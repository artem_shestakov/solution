#pragma  once
//-----------------------------------------------------------------------------
#include "ISCaratCoreBase.h"
//-----------------------------------------------------------------------------
class CTCoreMail : public ISCaratCoreBase
{
	Q_OBJECT

public:
	Q_INVOKABLE CTCoreMail(const QString &core_name, QObject *parent);
	virtual ~CTCoreMail();

protected:
	void Timeout();
	void SendMail(int ID, const QString &sender_mail, const QString &sender_password, const QString &server_mail, bool ssl, int port, const QString &recipient_mail, const QString &subject, const QString &text);
	void SendedDone(int ID);
	void SendedError(int ID, const QString &error_string);
};
//-----------------------------------------------------------------------------
