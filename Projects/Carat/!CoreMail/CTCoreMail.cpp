#include "StdAfx.h"
#include "CTCoreMail.h"
#include "ISQuery.h"
#include "ISDebug.h"
#include "ISLocalization.h"
#include "ISEmailAddress.h"
#include "ISSmtpClient.h"
#include "ISMimeMessage.h"
#include "ISMimeHtml.h"
//-----------------------------------------------------------------------------
static QString QS_CARAT_MAIL = PREPARE_QUERY("SELECT crml_id, crml_sendermail, crml_senderpassword, crml_servermail, crml_ssl, crml_port, crml_recipientmail, crml_subject, crml_text "
											 "FROM _caratmail "
											 "WHERE NOT crml_sended "
											 "ORDER BY crml_id");
//-----------------------------------------------------------------------------
static QString QU_SENDED_DONE = PREPARE_QUERY("UPDATE _caratmail SET "
											  "crml_sended = true "
											  "WHERE crml_id = :ObjectID");
//-----------------------------------------------------------------------------
static QString QU_SENDED_ERROR = PREPARE_QUERY("UPDATE _caratmail SET "
											   "crml_sended = false, "
											   "crml_errorstring = :ErrorString "
											   "WHERE crml_id = :ObjectID");
//-----------------------------------------------------------------------------
CTCoreMail::CTCoreMail(const QString &core_name, QObject *parent) : ISCaratCoreBase(core_name, parent)
{
	connect(GetTimer(), &QTimer::timeout, this, &CTCoreMail::Timeout);
	StartTimer(1000);
}
//-----------------------------------------------------------------------------
CTCoreMail::~CTCoreMail()
{

}
//-----------------------------------------------------------------------------
void CTCoreMail::Timeout()
{
	GetTimer()->stop();

	ISQuery qSelect(GetDB(), QS_CARAT_MAIL);
	if (qSelect.Execute())
	{
		while (qSelect.Next())
		{
			int ID = qSelect.ReadColumn("crml_id").toInt();
			QString SenderMail = qSelect.ReadColumn("crml_sendermail").toString();
			QString SenderPassword = qSelect.ReadColumn("crml_senderpassword").toString();
			QString ServerMail = qSelect.ReadColumn("crml_servermail").toString();
			bool SSL = qSelect.ReadColumn("crml_ssl").toBool();
			int Port = qSelect.ReadColumn("crml_port").toInt();
			QString RecipientMail = qSelect.ReadColumn("crml_recipientmail").toString();
			QString Subject = qSelect.ReadColumn("crml_subject").toString();
			QString Text = qSelect.ReadColumn("crml_text").toString();

			SendMail(ID, SenderMail, SenderPassword, ServerMail, SSL, Port, RecipientMail, Subject, Text);
		}
	}

	GetTimer()->start();
}
//-----------------------------------------------------------------------------
void CTCoreMail::SendMail(int ID, const QString &sender_mail, const QString &sender_password, const QString &server_mail, bool ssl, int port, const QString &recipient_mail, const QString &subject, const QString &text)
{
	ISDebug::ShowCaratString(this, LOCALIZATION("MailDispatcher.SendingMail").arg(recipient_mail));

	ISEmailAddress *AddressSender = new ISEmailAddress(sender_mail); //�����������
	ISEmailAddress *AddressRecipient = new ISEmailAddress(recipient_mail); //����������
	ISSmtpClient SmtpClient(server_mail, port, ssl ? ISSmtpClient::SslConnection : ISSmtpClient::TcpConnection);

	ISMimeMessage MimeMessage;
	MimeMessage.setSender(AddressSender);
	MimeMessage.setSubject(subject);
	MimeMessage.addRecipient(AddressRecipient);

	ISMimeHtml MimeHtml;
	MimeHtml.setHtml(text);
	MimeMessage.addPart(&MimeHtml);

	if (SmtpClient.connectToHost()) //���� ���������� � �������� ������ �������
	{
		if (SmtpClient.login(sender_mail, sender_password)) //���� ����������� ������ �������
		{
			bool Sended = SmtpClient.sendMail(MimeMessage);
			if (Sended)
			{
				SendedDone(ID);
				ISDebug::ShowCaratString(this, LOCALIZATION("MailDispatcher.SendedMail"));
			}
			else
			{
				SendedError(ID, "");
				ISDebug::ShowCaratString(this, LOCALIZATION("MailDispatcher.SendedFail"));
			}
		}
		else
		{
			SendedError(ID, SmtpClient.getErrorString());
		}

		SmtpClient.quit();
	}
	else
	{
		SendedError(ID, SmtpClient.getErrorString());
	}
}
//-----------------------------------------------------------------------------
void CTCoreMail::SendedDone(int ID)
{
	ISQuery qSended(GetDB(), QU_SENDED_DONE);
	qSended.BindValue(":ObjectID", ID);
	qSended.Execute();
}
//-----------------------------------------------------------------------------
void CTCoreMail::SendedError(int ID, const QString &error_string)
{
	ISQuery qSended(GetDB(), QU_SENDED_ERROR);
	qSended.BindValue(":ErrorString", error_string);
	qSended.BindValue(":ObjectID", ID);
	qSended.Execute();
}
//-----------------------------------------------------------------------------
