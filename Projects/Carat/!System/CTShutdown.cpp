#include "StdAfx.h"
#include "CTShutdown.h"
//-----------------------------------------------------------------------------
static CTShutdown *Shutdown;
//-----------------------------------------------------------------------------
CTShutdown::CTShutdown(QObject *parent) : QObject(parent)
{
	Shutdown = this;
	signal(SIGINT, &CTShutdown::Exit); //���������� Ctrl + C
	signal(SIGTERM, &(CTShutdown::Exit));
	signal(SIGILL, &(CTShutdown::Exit));
	signal(SIGFPE, &(CTShutdown::Exit));
	signal(SIGSEGV, &(CTShutdown::Exit));
	signal(SIGBREAK, &(CTShutdown::Exit)); //�������� ���� ������� (Alt + F4)
	signal(SIGABRT, &(CTShutdown::Exit));
	signal(SIGABRT_COMPAT, &(CTShutdown::Exit));
}
//-----------------------------------------------------------------------------
void CTShutdown::Exit(int SignalCode)
{
	QCoreApplication::quit();
}
//-----------------------------------------------------------------------------
