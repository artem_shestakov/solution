#include "StdAfx.h"
#include "CTThread.h"
#include "EXDefines.h"
#include "ISAssert.h"
//-----------------------------------------------------------------------------
CTThread::CTThread(const QString &core_name, const QString &core_class_name, QObject *parent) : QThread(parent)
{
	CoreName = core_name;
	CoreClassName = core_class_name;
	Core = nullptr;
	ThreadID = 0;
}
//-----------------------------------------------------------------------------
CTThread::~CTThread()
{
	
}
//-----------------------------------------------------------------------------
void CTThread::Shutdown()
{
	Core->Shutdown();
}
//-----------------------------------------------------------------------------
int CTThread::GetThreadID() const
{
	return ThreadID;
}
//-----------------------------------------------------------------------------
void CTThread::run()
{
	int ObjectType = QMetaType::type((CoreClassName + "*").toLocal8Bit().constData());
	IS_ASSERT(ObjectType, QString("Core \"%1\" is null.").arg(CoreClassName));

	const QMetaObject *MetaObject = QMetaType::metaObjectForType(ObjectType);
	Core = dynamic_cast<ISCaratCoreBase*>(MetaObject->newInstance(Q_ARG(const QString &, CoreName), Q_ARG(QObject *, nullptr)));
	IS_ASSERT(Core, QString("Core \"%1\" not dynamic cast.").arg(CoreClassName));

	ThreadID = int(CTThread::currentThreadId());

	emit StartedCore();
	exec();
	emit FinishedCore();
}
//-----------------------------------------------------------------------------
