#pragma once
//-----------------------------------------------------------------------------
#include "ISCaratCoreBase.h"
//-----------------------------------------------------------------------------
class CTThread : public QThread
{
	Q_OBJECT

signals:
	void StartedCore(); //������ � ������� ����
	void FinishedCore(); //������ �� ��������� ����

public:
	CTThread(const QString &core_name, const QString &core_class_name, QObject *parent = 0);
	virtual ~CTThread();

	void Shutdown(); //��������� ���� ������
	int GetThreadID() const; //�������� ������������� ������
	void run() override;

private:
	QString CoreName;
	QString CoreClassName;
	ISCaratCoreBase *Core;
	int ThreadID;
};
//-----------------------------------------------------------------------------
