#pragma once
//-----------------------------------------------------------------------------
#include <QObject>
//-----------------------------------------------------------------------------
class CTShutdown : public QObject
{
	Q_OBJECT

public:
	explicit CTShutdown(QObject *parent = 0);

private:
	static void Exit(int SignalCode);
};
//-----------------------------------------------------------------------------
