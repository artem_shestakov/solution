#include "StdAfx.h"
#include "CTService.h"
#include "ISQuery.h"
#include "ISDebug.h"
#include "ISCountingTime.h"
#include "ISLocalization.h"
//-----------------------------------------------------------------------------
static QString QS_CARAT_CORE = PREPARE_QUERY("SELECT core_name, core_classname "
											 "FROM _caratcore "
											 "WHERE NOT core_isdeleted "
											 "ORDER BY core_priority");
//-----------------------------------------------------------------------------
CTSerivce::CTSerivce(QObject *parent) : QObject(parent)
{
	
}
//-----------------------------------------------------------------------------
CTSerivce::~CTSerivce()
{

}
//-----------------------------------------------------------------------------
void CTSerivce::Start()
{
	ISQuery qSelect(QS_CARAT_CORE);
	if (qSelect.Execute())
	{
		ISDebug::ShowEmptyString();

		while (qSelect.Next())
		{
			ISCountingTime CountingTime;
			QString CoreName = qSelect.ReadColumn("core_name").toString();
			QString CoreClassName = qSelect.ReadColumn("core_classname").toString();

			ISDebug::ShowString(LOCALIZATION("CoreInitializing").arg(CoreName) + "...");

			QEventLoop EventLoop;

			CTThread *CaratThread = new CTThread(CoreName, CoreClassName, this);
			Threads.insert(CoreName, CaratThread);
			connect(CaratThread, &CTThread::StartedCore, &EventLoop, &QEventLoop::quit);
			CaratThread->start();

			EventLoop.exec();

			ISDebug::ShowString(LOCALIZATION("CoreInitialized").arg(CoreName).arg(CountingTime.GetElapsed()).arg(CaratThread->GetThreadID()));
			ISDebug::ShowEmptyString();
		}
	}
}
//-----------------------------------------------------------------------------
void CTSerivce::Stop()
{
	ISDebug::ShowString(LOCALIZATION("Shutdown") + "...");

	for (const auto &ThreadItem : Threads.toStdMap())
	{
		QString CoreName = ThreadItem.first;
		CTThread *CaratThread = ThreadItem.second;

		ISDebug::ShowString(LOCALIZATION("CoreShutdowning").arg(CoreName) + "...");

		CaratThread->Shutdown();

		QEventLoop EventLoop;
		connect(CaratThread, &CTThread::FinishedCore, &EventLoop, &QEventLoop::quit);
		CaratThread->quit();
		EventLoop.exec();

		ISDebug::ShowString(LOCALIZATION("CoreShutdowned").arg(CoreName));
	}

	Threads.clear();
}
//-----------------------------------------------------------------------------
