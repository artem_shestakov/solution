#include "StdAfx.h"
#include "CTShutdown.h"
#include "EXDefines.h"
#include "ISConfig.h"
#include "ISDebug.h"
#include "ISDatabase.h"
#include "ISQuery.h"
#include "ISPlugin.h"
#include "CTService.h"
#include "ISSystem.h"
#include "ISLocalization.h"
//-----------------------------------------------------------------------------
#include "CTCoreNotification.h"
#include "CTCoreMail.h"
#include "CTCoreTelegram.h"
//-----------------------------------------------------------------------------
bool IsRunningCarat()
{
	bool Result = false;

	{
		QSqlDatabase DB = QSqlDatabase::cloneDatabase(ISDatabase::GetInstance().GetDefaultDB(), "CheckStarted");
		if (DB.open(SYSTEM_USER_LOGIN, SYSTEM_USER_PASSWORD))
		{
			{
				ISQuery qSelect(DB, "SELECT COUNT(*) FROM pg_stat_activity WHERE application_name = 'Carat'");
				if (qSelect.ExecuteFirst())
				{
					int Count = qSelect.ReadColumn("count").toInt();
					if (Count)
					{
						Result = true;
					}
				}
			}

			DB.close();
		}
	}

	QSqlDatabase::removeDatabase("CheckStarted");
	return Result;
}
//-----------------------------------------------------------------------------
void RegisterMetaType()
{
	qRegisterMetaType<CTCoreNotification*>("CTCoreNotification");
	qRegisterMetaType<CTCoreMail*>("CTCoreMail");
	qRegisterMetaType<CTCoreTelegram*>("CTCoreTelegram");
}
//-----------------------------------------------------------------------------
void SetApplicationName()
{
	ISQuery qSetAppName;
	qSetAppName.Execute("SET application_name = 'Carat'");
}
//-----------------------------------------------------------------------------
int main(int argc, char *argv[])
{
	QCoreApplication CaratApplication(argc, argv);
	ISLocalization::GetInstance().LoadResourceFile(LOCALIZATION_FILE_CARAT);
	ISLocalization::GetInstance().LoadResourceFile(LOCALIZATION_FILE_CORE);

	if (IsRunningCarat()) //���� ����� ��� �������
	{
		ISDebug::ShowString(LOCALIZATION("AlreadyStarted"));
		ISSystem::SleepSeconds(3);
		return EXIT_CODE_NORMAL;
	}

	CTShutdown Shutdown;
	ISConfig::GetInstance().Initialize(CONFIG_FILE_PATH);
	RegisterMetaType();

	if (!ISPlugin::GetInstance().Load())
	{
		return EXIT_CODE_ERROR;
	}

	QString ErrorString;
	bool Connected = ISDatabase::GetInstance().ConnectToDefaultDB(SYSTEM_USER_LOGIN, SYSTEM_USER_PASSWORD, ErrorString);
	if (!Connected)
	{
		ISDebug::ShowCriticalString(ErrorString);
		return EXIT_CODE_ERROR;
	}

	SetApplicationName();

	ISDebug::ShowString(LOCALIZATION("Starting") + "...");
	CTSerivce Service;
	Service.Start();
	ISDebug::ShowString(LOCALIZATION("Started"));

	int Result = CaratApplication.exec();
	Service.Stop();
	ISDebug::ShowString(LOCALIZATION("ExitCarat") + "...");
	ISSystem::SleepSeconds(2);

	return Result;
}
//-----------------------------------------------------------------------------
