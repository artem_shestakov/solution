#pragma once
//-----------------------------------------------------------------------------
#include "ISCaratCoreBase.h"
#include "qttelegrambot.h"
#include "message.h"
//-----------------------------------------------------------------------------
class CTCoreTelegram : public ISCaratCoreBase
{
	Q_OBJECT

public:
	Q_INVOKABLE CTCoreTelegram(const QString &core_name, QObject *parent);
	virtual ~CTCoreTelegram();

protected:
	void NewMessage(Telegram::Message message);

private:
	Telegram::Bot *Bot;
};
//-----------------------------------------------------------------------------
