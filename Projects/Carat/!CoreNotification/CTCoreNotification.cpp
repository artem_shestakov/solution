#include "StdAfx.h"
#include "CTCoreNotification.h"
#include "EXDefines.h"
#include "ISQuery.h"
#include "ISSystem.h"
#include "ISDebug.h"
#include "ISLocalization.h"
//-----------------------------------------------------------------------------
static QString QS_USERS = PREPARE_QUERY("SELECT usrs_id, usrs_login FROM _users WHERE NOT usrs_isdeleted ORDER BY usrs_login");
//-----------------------------------------------------------------------------
static QString QS_NOTIFICATION = PREPARE_QUERY("SELECT pg_notify(:NotificationName, :Payload)");
//-----------------------------------------------------------------------------
static QString QS_NOTIFICATION_USER = PREPARE_QUERY("SELECT ntfu_id, ntfu_creationdate, ntfu_notification, ntfu_userfrom, ntfu_userto, ntfu_payload, ntfu_saved "
													"FROM _notificationuser "
													"WHERE NOT ntfu_sended "
													"ORDER BY ntfu_id");
//-----------------------------------------------------------------------------
static QString QU_NOTIFICATION_SENDED = PREPARE_QUERY("UPDATE _notificationuser SET ntfu_sended = true WHERE ntfu_id = :ObjectID");
//-----------------------------------------------------------------------------
static QString QD_NOTIFICATION_USER = PREPARE_QUERY("DELETE FROM _notificationuser WHERE ntfu_id = :ObjectID");
//-----------------------------------------------------------------------------
CTCoreNotification::CTCoreNotification(const QString &core_name, QObject *parent) : ISCaratCoreBase(core_name, parent)
{
	ISQuery qSelect(GetDB(), QS_USERS);
	if (qSelect.Execute())
	{
		while (qSelect.Next())
		{
			Users.insert(qSelect.ReadColumn("usrs_id").toInt(), qSelect.ReadColumn("usrs_login").toString());
		}
	}

	connect(GetTimer(), &QTimer::timeout, this, &CTCoreNotification::Timeout);
	StartTimer(100);
}
//-----------------------------------------------------------------------------
CTCoreNotification::~CTCoreNotification()
{
	
}
//-----------------------------------------------------------------------------
void CTCoreNotification::Timeout()
{
	ISQuery qSelect(QS_NOTIFICATION_USER);
	if (qSelect.Execute())
	{
		while (qSelect.Next())
		{
			int ID = qSelect.ReadColumn("ntfu_id").toInt();
			QDateTime DateTime = qSelect.ReadColumn("ntfu_creationdate").toDateTime(); //���� � ����� �����������
			ISUuid UID = qSelect.ReadColumn("ntfu_notification"); //������������� �����������
			int UserFrom = qSelect.ReadColumn("ntfu_userfrom").toInt(); //��� ��������
			int UserTo = qSelect.ReadColumn("ntfu_userto").toInt(); //���� ����������
			QString Payload = qSelect.ReadColumn("ntfu_payload").toString();
			bool Saved = qSelect.ReadColumn("ntfu_saved").toBool();

			if (UserTo) //���� ����������� ���������� ����������� ������������
			{
				SendNotification(Users.value(UserTo), UID, Payload, ID, DateTime, Saved);
			}
			else //���������� ���������� ���� �������������
			{
				for (const auto &User : Users.toStdMap()) //����� ������ �������������
				{
					int UserID = User.first;
					QString UserLogin = User.second;

					if (UserFrom != UserID)
					{
						SendNotification(UserLogin, UID, Payload, ID, DateTime, Saved);
					}
				}
			}

			if (Saved) //���� ����������� �� ����� ��������� � ����
			{
				ISQuery qSended(QU_NOTIFICATION_SENDED);
				qSended.BindValue(":ObjectID", ID);
				qSended.Execute();
			}
			else //�������� ������������
			{
				ISQuery qDelete(QD_NOTIFICATION_USER);
				qDelete.BindValue(":ObjectID", ID);
				qDelete.Execute();
			}
		}
	}
}
//-----------------------------------------------------------------------------
void CTCoreNotification::SendNotification(const QString &UserTo, const QString &NotificationUID, const QString &Payload, int ID, const QDateTime &DateTime, bool Saved)
{
	QVariantMap VariantMap;
	VariantMap.insert("NotificationUID", NotificationUID); //���� ���������� �����������
	VariantMap.insert("Payload", Payload); //���������� �� ������������
	VariantMap.insert("ID", ID); //������������� �����������
	VariantMap.insert("DateTime", DateTime); //���� � ����� �����������
	VariantMap.insert("Saved", Saved);

	QString PayloadString = ISSystem::VariantMapToJsonString(VariantMap);

	ISQuery qNotify(QS_NOTIFICATION);
	qNotify.BindValue(":NotificationName", UserTo);
	qNotify.BindValue(":Payload", PayloadString);
	if (qNotify.Execute())
	{
		ISDebug::ShowCaratString(this, LOCALIZATION("Notification.Sended").arg(UserTo));
	}
}
//-----------------------------------------------------------------------------
