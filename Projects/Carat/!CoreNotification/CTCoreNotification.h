#pragma once
//-----------------------------------------------------------------------------
#include "ISCaratCoreBase.h"
//-----------------------------------------------------------------------------
class CTCoreNotification : public ISCaratCoreBase
{
	Q_OBJECT

public:
	Q_INVOKABLE CTCoreNotification(const QString &core_name, QObject *parent);
	virtual ~CTCoreNotification();

protected:
	void Timeout();
	void SendNotification(const QString &UserTo, const QString &NotificationUID, const QString &Payload, int ID, const QDateTime &DateTime, bool Saved); //������� ����������� ������������

private:
	QMap<int, QString> Users;
};
//-----------------------------------------------------------------------------
