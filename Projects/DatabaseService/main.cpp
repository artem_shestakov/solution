#include "StdAfx.h"
#include "EXDefines.h"
#include "EXConstants.h"
#include "ISConfig.h"
#include "ISLocalization.h"
#include "ISProcess.h"
#include "ISLogger.h"
//-----------------------------------------------------------------------------
QString GetArguments();
//-----------------------------------------------------------------------------
int main(int argc, char *argv[])
{
	QCoreApplication Application(argc, argv);

	ISConfig::GetInstance().Initialize(CONFIG_FILE_PATH);
	ISLocalization::GetInstance().LoadResourceFile(LOCALIZATION_FILE_CORE);

	QEventLoop EventLoop;

	QProcessEnvironment ProcessEnvironment;
	ProcessEnvironment.insert("PGPASSWORD", ISConfig::GetInstance().GetValue(CONST_CONFIG_DATABASESERVICE_PASSWORD).toString());

	ISProcess Process(nullptr);
	Process.SetProcessEnvironment(ProcessEnvironment);
	QObject::connect(&Process, &ISProcess::Finished, &EventLoop, &QEventLoop::quit);

	QObject::connect(&Process, &ISProcess::Message, [=](const QString &Message)
	{
		QTextStream TextStream(stdout);
		TextStream.setCodec(TEXT_CODEC_IBM866);
		TextStream << Message << endl;

		IS_LOGGER(Message);
	});

	Process.Start("\"" + ISConfig::GetInstance().GetValue(CONST_CONFIG_DATABASESERVICE_FOLDERPOSTGRESBIN).toString() + "/" + PG_DUMP_FILE_NAME + "\"", GetArguments());
	EventLoop.exec();

	return EXIT_CODE_NORMAL;
}
//-----------------------------------------------------------------------------
QString GetArguments()
{
	QString DatabaseName = ISConfig::GetInstance().GetValue(CONST_CONFIG_DATABASESERVICE_DATABASE).toString();
	QString FileBackupPath = ISConfig::GetInstance().GetValue(CONST_CONFIG_DATABASESERVICE_FOLDERBACKUP).toString() + "/" + DatabaseName + "_" + QDate::currentDate().toString(DATE_FORMAT_STRING_V2) + "_" + QTime::currentTime().toString(TIME_FORMAT_STRING_V2) + ".dmp";

	QString Arguments;

	Arguments += "--host=" + ISConfig::GetInstance().GetValue(CONST_CONFIG_DATABASESERVICE_SERVER).toString() + " ";
	Arguments += "--port=" + ISConfig::GetInstance().GetValue(CONST_CONFIG_DATABASESERVICE_PORT).toString() + " ";
	Arguments += "--username=" + ISConfig::GetInstance().GetValue(CONST_CONFIG_DATABASESERVICE_LOGIN).toString() + " ";
	Arguments += "--format=c ";
	Arguments += "--encoding=UTF8 ";
	Arguments += "--compress=" + ISConfig::GetInstance().GetValue(CONST_CONFIG_DATABASESERVICE_COMPRESSION).toString() + " ";
	Arguments += "--inserts ";
	Arguments += "--column-inserts ";
	Arguments += "--role=postgres ";
	Arguments += "--exclude-table-data=_distfiles ";
	Arguments += "--exclude-table-data=_distfilesdata ";
	Arguments += "-v ";
	Arguments += "--file=" + FileBackupPath + " ";
	Arguments += DatabaseName;

	return Arguments;
}
//-----------------------------------------------------------------------------
