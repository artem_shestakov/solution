#pragma once
//-----------------------------------------------------------------------------
#include "ISAbstractMode.h"
#include "ISNamespace.h"
//-----------------------------------------------------------------------------
class ISModeUser : public ISAbstractMode
{
public:
	ISModeUser(const QString &entered_login);
	virtual ~ISModeUser();

	int Startup() override;

protected:
	int ExitNormal(); //��������� ���������
	int ExitError(); //��������� ��������� � �������
	void SetApplicationNameQuery(); //�������� ������������ ���������� ����� ���������� � ��
	bool CheckAccessDatabase(const QString &Login); //�������� ������� � ����
	bool CheckAccessAllowed(); //�������� ���������� ������� ������������
	bool CheckExistUserGroup(); //�������� ������� �������� ������������ � ������
	void InitializeTelephony(); //������������� ���������
	ISNamespace::UpdateResult CheckUpdate(); //�������� ����������
	bool CheckLicense(); //�������� ��������
};
//-----------------------------------------------------------------------------
