#pragma once
//-----------------------------------------------------------------------------
#include <QApplication>
//-----------------------------------------------------------------------------
class ISIntegralSystem : public QApplication
{
	Q_OBJECT

public:
	ISIntegralSystem(int &argc, char **argv);
	virtual ~ISIntegralSystem();

	void Initialize();
	bool notify(QObject *Object, QEvent *e);
};
//-----------------------------------------------------------------------------
