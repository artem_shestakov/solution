#include "StdAfx.h"
#include "ISCrashReport.h"
#include "EXDefines.h"
#include "ISDebug.h"
#include "ISStackWalker.h"
#include "ISProcessForm.h"
//-----------------------------------------------------------------------------
ISCrashReport::ISCrashReport() : QObject()
{

}
//-----------------------------------------------------------------------------
ISCrashReport::~ISCrashReport()
{

}
//-----------------------------------------------------------------------------
int ISCrashReport::ReportHook(int ReportType, char *Message, int *ReturnValue)
{
	CreateReport("CRASH", nullptr);
	return 0;
}
//-----------------------------------------------------------------------------
LONG ISCrashReport::ExceptionHandling(_EXCEPTION_POINTERS *ExceptionInfo)
{
	CreateReport("ASSERT", ExceptionInfo);
	return EXCEPTION_EXECUTE_HANDLER;
}
//-----------------------------------------------------------------------------
void ISCrashReport::CreateReport(const QString &FileName, _EXCEPTION_POINTERS *ExceptionInfo)
{
	ISProcessForm ProcessForm;
	ProcessForm.setWindowFlags(ProcessForm.windowFlags() & ~Qt::WindowStaysOnTopHint);
	ProcessForm.show();
	ProcessForm.SetText("Creating log");

	ISStackWalker SW;

	if (ExceptionInfo)
	{
		SW.ShowCallstack(GetCurrentThread(), ExceptionInfo->ContextRecord);
	}
	else
	{
		SW.ShowCallstack(GetCurrentThread());
	}

	QString StackString = SW.GetCallStack();

	QFile FileCrash(APPLICATION_LOGS_PATH + "/" + FileName + "-" + qAppName() + "_" + QDateTime::currentDateTime().toString("dd.MM.yyyy_hh.mm.ss") + ".txt");
	if (FileCrash.open(QIODevice::WriteOnly))
	{
		FileCrash.write("\n=========================================DEBUG=======================================\r\n");

		for (int i = 0; i < ISDebug::GetInstance().GetDebugMessages().count(); i++)
		{
			FileCrash.write(ISDebug::GetInstance().GetDebugMessages().at(i).toUtf8() + "\r\n");
		}

		FileCrash.write("\n=========================================STACK=======================================\r\n");
		FileCrash.write(StackString.toUtf8());
		FileCrash.close();
	}
}
//-----------------------------------------------------------------------------
