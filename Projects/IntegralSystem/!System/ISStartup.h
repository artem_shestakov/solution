#pragma once
//-----------------------------------------------------------------------------
#include <QObject>
//-----------------------------------------------------------------------------
class ISStartup : public QObject
{
	Q_OBJECT

public:
	ISStartup();
	virtual ~ISStartup();

	static bool CheckAdminRole(); //�������� ������� ���� ��������������
	static void CheckRequiredLibrary(); //�������� ������� ����������� ��� ������ ���������
	static bool InitializedModule(); //������������� ������
	static void InitializedMetaData(bool InitXSR); //�������������� ���� ������
};
//-----------------------------------------------------------------------------
