#pragma once
//-----------------------------------------------------------------------------
#include "ISAbstractMode.h"
//-----------------------------------------------------------------------------
class ISModeService : public ISAbstractMode
{
public:
	ISModeService(const QString &entered_login);
	virtual ~ISModeService();

	int Startup() override;

protected:
	bool CheckExistDatabase(); //�������� ������������� ���� ������
};
//-----------------------------------------------------------------------------
