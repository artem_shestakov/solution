#pragma once
//-----------------------------------------------------------------------------
class ISAbstractMode
{
public:
	ISAbstractMode(const QString &entered_login);
	virtual ~ISAbstractMode();

	virtual int Startup() = 0;

protected:
	QString GetLogin() const;

private:
	QString EnteredLogin;
};
//-----------------------------------------------------------------------------
