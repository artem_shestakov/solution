#include "StdAfx.h"
#include "ISAbstractMode.h"
//-----------------------------------------------------------------------------
ISAbstractMode::ISAbstractMode(const QString &entered_login)
{
	EnteredLogin = entered_login;
}
//-----------------------------------------------------------------------------
ISAbstractMode::~ISAbstractMode()
{

}
//-----------------------------------------------------------------------------
QString ISAbstractMode::GetLogin() const
{
	return EnteredLogin;
}
//-----------------------------------------------------------------------------
