#include "StdAfx.h"
#include "ISVersionInformer.h"
#include "ISVersion.h"
#include "ISSystem.h"
//-----------------------------------------------------------------------------
ISVersionInformer::ISVersionInformer()
{

}
//-----------------------------------------------------------------------------
ISVersionInformer::~ISVersionInformer()
{

}
//-----------------------------------------------------------------------------
QString ISVersionInformer::GetVersion()
{
	QString FullVersionString = ISVERSION_STRING;
	RemoveLastSpace(FullVersionString);
	return FullVersionString;
}
//-----------------------------------------------------------------------------
QString ISVersionInformer::GetRevision()
{
	QString RevisionString = REVISION_STRING;
	RemoveLastSpace(RevisionString);
	return RevisionString;
}
//-----------------------------------------------------------------------------
QString ISVersionInformer::GetBuild()
{
	QString BuildString = BUILD_STRING;
	RemoveLastSpace(BuildString);
	return BuildString;
}
//-----------------------------------------------------------------------------
QString ISVersionInformer::GetDate()
{
	QString DateFrom = VERSION_DATE_FROM;
	RemoveLastSpace(DateFrom);
	return DateFrom;
}
//-----------------------------------------------------------------------------
QString ISVersionInformer::GetHash()
{
	QString HashString = VERSION_HASH;
	RemoveLastSpace(HashString);
	return HashString;
}
//-----------------------------------------------------------------------------
void ISVersionInformer::RemoveLastSpace(QString &String)
{
	if (QString(String[String.length() - 1]) == " ")
	{
		ISSystem::RemoveLastSymbolFromString(String);
	}
}
//-----------------------------------------------------------------------------
