#include "StdAfx.h"
#include "ISModeUser.h"
#include "EXDefines.h"
#include "ISAssert.h"
#include "ISSplashScreen.h"
#include "ISLocalization.h"
#include "ISQuery.h"
#include "ISDatabase.h"
#include "ISMessageBox.h"
#include "ISMetaUser.h"
#include "ISSettingsDatabase.h"
#include "ISSystem.h"
#include "ISMetaSystemsEntity.h"
#include "ISParagraphEntity.h"
#include "ISPlugin.h"
#include "ISSettings.h"
#include "ISUpdate.h"
#include "ISUpdateDownloadForm.h"
#include "ISMetaData.h"
#include "ISPrintingEntity.h"
#include "ISUserRoleEntity.h"
#include "ISFavorites.h"
#include "ISSortingBuffer.h"
#include "ISColumnSizer.h"
#include "ISFastAccessEntity.h"
#include "ISFastAccessEntity.h"
#include "ISLicense.h"
#include "ISCrypterLicense.h"
#include "ISLicenseForm.h"
#include "ISExitForm.h"
#include "ISProtocol.h"
#include "ISBuffer.h"
#include "ISMainWindow.h"
#include "EXConstants.h"
#include "ISCore.h"
#include "ISTelephonyInterface.h"
#include "ISVersionInformer.h"
#include "ISNotify.h"
#include "ISIntegralSystem.h"
#include "ISAutoLocking.h"
#include "ISProperty.h"
//-----------------------------------------------------------------------------
static QString QS_USER_CHECK = PREPARE_QUERY("SELECT COUNT(*) FROM _users WHERE NOT usrs_isdeleted AND usrs_login = :Login");
//-----------------------------------------------------------------------------
static QString Q_SET_APPLICATION_NAME = "SET application_name = 'IntegralSystem %1'";
//-----------------------------------------------------------------------------
ISModeUser::ISModeUser(const QString &entered_login) : ISAbstractMode(entered_login)
{

}
//-----------------------------------------------------------------------------
ISModeUser::~ISModeUser()
{

}
//-----------------------------------------------------------------------------
int ISModeUser::Startup()
{
	//�������� ��������� ������ �������������
	ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Banner.Initialize.CheckCurrentUser"));
	ISQuery qSelectUser(QS_USER_CHECK);
	qSelectUser.BindValue(":Login", GetLogin());
	qSelectUser.ExecuteFirst();
	IS_ASSERT(qSelectUser.ReadColumn("count").toInt(), QString("Not found user %1 in table _users").arg(GetLogin()));

	//�������� ���� ��������
	ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Banner.Initialize.PrepareQueries"));
	ISQueryText::GetInstance().CheckAllQueries(ISDatabase::GetInstance().GetDefaultDB());

	//��������� ����� ���������� � ���� ������
	ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Banner.Initialize.SetApplicationNameDB"));
	SetApplicationNameQuery();

	//�������� ����-������ � ������������
	ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Banner.Initialize.MetaDataCurrentUser"));
	ISMetaUser::GetInstance().Initialize(GetLogin());

	//������������� �������� ���� ������
	ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Banner.Initialize.SettingsDatabase"));
	ISSettingsDatabase::GetInstance().Initialize();

	if (!CheckAccessDatabase(GetLogin())) //�������� ���������� ������� � ���� �������������
	{
		return ExitNormal();
	}

	if (!CheckAccessAllowed()) //�������� ���������� ������� ������������
	{
		return ExitNormal();
	}

	if (!CheckExistUserGroup()) //�������� ������� �������� ������������ � ������
	{
		return ExitNormal();
	}

	//�������� ����-������ � �������� � �����������
	ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Banner.Initialize.SystemsAndSubSystems"));
	ISMetaSystemsEntity::GetInstance();

	//������������� ����������
	ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Banner.Initialize.Paragraphs"));
	ISParagraphEntity::GetInstance();

	//����������� ����-����� ������������
	ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Banner.Initialize.RegistrationMetaTypes.Module").arg(ISPlugin::GetInstance().GetFileName()));
	ISPlugin::GetInstance().GetPluginInterface()->RegisterMetaTypes();

	//������������� ��������
	ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Banner.Initialize.SettingsUser"));
	ISSettings::GetInstance();

	ISNamespace::UpdateResult ResultUpdate = CheckUpdate();
	if (ResultUpdate == ISNamespace::UR_ExitApplication)
	{
		return EXIT_CODE_ERROR;
	}
	else if (ResultUpdate == ISNamespace::UR_ErrorStartUpdate)
	{
		ISMessageBox::ShowWarning(nullptr, LOCALIZATION("Message.Warning.ErrorStartUpdate"));
	}

	//������������� ������
	ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Banner.Initialize.Printing"));
	ISPrintingEntity::GetInstance();

	//������������� ���� �������
	ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Banner.Initialize.UserRoles"));
	ISUserRoleEntity::GetInstance().InitializeAccess();
	ISUserRoleEntity::GetInstance().InitializeAccessSpecial();

	if (!ISMetaUser::GetInstance().GetData()->System) //���� ������������ �� ���������
	{
		if (ISMetaUser::GetInstance().GetData()->GroupID) //���� ������������ �������� � ������
		{
			if (!ISMetaUser::GetInstance().GetData()->GroupFullAccess)
			{
				if (!ISUserRoleEntity::GetInstance().GetCountPermissions()) //���� � ������������ ��� ����
				{
					ISMessageBox::ShowWarning(nullptr, LOCALIZATION("Message.Warning.UserNotAccess"));
					return EXIT_CODE_ERROR;
				}
			}
		}
		else
		{
			ISMessageBox::ShowWarning(nullptr, LOCALIZATION("Message.Warning.UserGroupIsNull"));
			return EXIT_CODE_ERROR;
		}
	}

	if (ISMetaUser::GetInstance().GetData()->AccountLifetime) //���� ��� ������������ ��������� ����������� ����� �������� ������� ������
	{
		QDate DateStart = ISMetaUser::GetInstance().GetData()->AccountLifetimeStart;
		QDate DateEnd = ISMetaUser::GetInstance().GetData()->AccountLifetimeEnd;

		if (QDate::currentDate() < DateStart)
		{
			ISMessageBox::ShowWarning(nullptr, LOCALIZATION("Message.Warning.AccountLifetimeNotStarted"));
			return EXIT_CODE_ERROR;
		}
		else if (QDate::currentDate() == DateEnd) //���� ������� �������� ���� ��������
		{
			ISMessageBox::ShowWarning(nullptr, LOCALIZATION("Message.Warning.AccountLifetimeLeftLastDay"));
		}
		else if (QDate::currentDate().addDays(1) == DateEnd) //���� ������ �������� ���� ��������
		{
			ISMessageBox::ShowWarning(nullptr, LOCALIZATION("Message.Warning.AccountLifetimeLeftLastDayTwo"));
		}
		else if (QDate::currentDate() > DateEnd)
		{
			ISMessageBox::ShowWarning(nullptr, LOCALIZATION("Message.Warning.AccountLifetimeEnded"));
			return EXIT_CODE_ERROR;
		}
	}

	//������������� ���������
	ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Banner.Initialize.Telephony"));
	InitializeTelephony();

	//������������� �����������
	ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Banner.Initialize.Notification"));
	ISNotify::GetInstance().Initialize();

	//������������� ����������
	ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Banner.Initialize.Favorites"));
	ISFavorites::GetInstance().Initialize(CURRENT_USER_ID);

	//������������� ����������
	ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Banner.Initialize.UserSortings"));
	ISSortingBuffer::GetInstance();

	//������������� �������� �������
	ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Banner.Initialize.ColumnSizer"));
	ISColumnSizer::GetInstance().Initialize();

	//������������� ������� ������������
	ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Banner.Initialize.ExternalTools"));
	ISFastAccessEntity::GetInstance().LoadExternalTools();

	//������������� �������� �������
	ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Banner.Initialize.CreateRecords"));
	ISFastAccessEntity::GetInstance().LoadCreateRecords();

	//�������� ��������
	ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Banner.Initialize.License"));
	if (!CheckLicense())
	{
		return EXIT_CODE_ERROR;
	}

	//������������ ����������� ������
	ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Banner.Initialize.InitializeLocalizationModule"));
	ISLocalization::GetInstance().InitializeContent(ISPlugin::GetInstance().GetPluginInterface()->GetLocalization());

	//�������� ����� � ���������
	ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Banner.Initialize.RegisterEnterProtocol"));
	int EnterProtocolID = ISProtocol::EnterApplication();
	if (EnterProtocolID)
	{
		ISProtocol::EnterProtocol(EnterProtocolID);
		ISMetaUser::GetInstance().SetProtocolEnterID(EnterProtocolID);
	}

	if (CURRENT_USER_LOGIN != SYSTEM_USER_LOGIN)
	{
		if (!ISMetaUser::GetInstance().GetData()->Birthday.isNull())
		{
			if (ISMetaUser::GetInstance().GetData()->Birthday == QDate::currentDate())
			{
				ISSplashScreen::GetInstance().hide();
				QSound::play(BUFFER_AUDIO("HappyBirthday"));
				ISMessageBox::ShowInformation(nullptr, LOCALIZATION("HappyBirthday").arg(CURRENT_USER_FULL_NAME));
			}
		}
	}

	//�������� ������� �����
	ISSplashScreen::GetInstance().show();
	ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Banner.Initialize.OpeningMainWindow"));

	ISProperty::GetInstance().SetValue(IS_PROPERTY_LINE_EDIT_SELECTED_MENU, SETTING_BOOL(CONST_UID_SETTING_OTHER_SELECTED_MENU));

	ISMainWindow *MainWindow = new ISMainWindow();
	ISSplashScreen::GetInstance().hide();

	if (SETTING_BOOL(CONST_UID_SETTING_VIEW_MAINWINDOWMAXIMIZE))
	{
		if (SETTING_BOOL(CONST_UID_SETTING_VIEW_STARTMAINWINDOWANIMATED))
		{
			MainWindow->ShowAnimated(true);
		}
		else
		{
			MainWindow->showMaximized();
		}
	}
	else
	{
		if (SETTING_BOOL(CONST_UID_SETTING_VIEW_STARTMAINWINDOWANIMATED))
		{
			MainWindow->ShowAnimated();
		}
		else
		{
			MainWindow->show();
		}
	}

	if (SETTING_BOOL(CONST_UID_SETTING_SECURITY_AUTOLOCK))
	{
		ISAutoLocking::GetInstance().Initialize();
		ISIntegralSystem *Application = dynamic_cast<ISIntegralSystem*>(qApp);

		ISAutoLocking::GetInstance().SetInterval(SETTING_INT(CONST_UID_SETTING_SECURITY_AUTOLOCKINTERVAL) * 60000);
		ISAutoLocking::GetInstance().StartTimer();

		QObject::connect(MainWindow, &ISMainWindow::Unlocked, &ISAutoLocking::GetInstance(), &ISAutoLocking::UnlockApplication);
		QObject::connect(&ISAutoLocking::GetInstance(), &ISAutoLocking::AutoLock, MainWindow, &ISMainWindow::LockApplication);
	}

	MainWindow->raise();
	MainWindow->activateWindow();

	return EXIT_CODE_NORMAL;
}
//-----------------------------------------------------------------------------
int ISModeUser::ExitNormal()
{
	ISCore::ExitApplication();
	return EXIT_CODE_NORMAL;
}
//-----------------------------------------------------------------------------
int ISModeUser::ExitError()
{
	ISCore::ExitApplication();
	return EXIT_CODE_ERROR;
}
//-----------------------------------------------------------------------------
void ISModeUser::SetApplicationNameQuery()
{
	ISQuery qSetApplicationName;
	if (!qSetApplicationName.Execute(QString(Q_SET_APPLICATION_NAME).arg(ISVersionInformer::GetVersion())))
	{
		ISMessageBox::ShowCritical(nullptr, qSetApplicationName.GetSqlQuery().lastError().text());
	}
}
//-----------------------------------------------------------------------------
bool ISModeUser::CheckAccessDatabase(const QString &Login)
{
	if (ISMetaUser::GetInstance().GetData()->System)
	{
		return true;
	}

	bool AccessDatabase = ISSettingsDatabase::GetInstance().UserAccessDatabase;
	if (!AccessDatabase)
	{
		ISSplashScreen::GetInstance().hide();
		ISMessageBox::ShowWarning(nullptr, LOCALIZATION("Message.Information.NotAccessToDatabase"), LOCALIZATION("NotAccessToDatabase"));
	}

	return AccessDatabase;
}
//-----------------------------------------------------------------------------
bool ISModeUser::CheckAccessAllowed()
{
	bool Result = true;

	if (!ISMetaUser::GetInstance().GetData()->AccessAllowed) //���� � ������������ ��� ����� �������
	{
		ISSplashScreen::GetInstance().hide();
		ISMessageBox::ShowCritical(nullptr, LOCALIZATION("Message.Error.User.NotAccessAllowed"));

		Result = false;
	}

	return Result;
}
//-----------------------------------------------------------------------------
bool ISModeUser::CheckExistUserGroup()
{
	if (ISMetaUser::GetInstance().GetData()->System)
	{
		return true;
	}
	else
	{
		if (!ISMetaUser::GetInstance().GetData()->GroupID)
		{
			ISSplashScreen::GetInstance().hide();
			ISMessageBox::ShowCritical(nullptr, LOCALIZATION("Message.Error.User.NotLinkWithGroup"));

			return false;
		}
	}

	return true;
}
//-----------------------------------------------------------------------------
void ISModeUser::InitializeTelephony()
{
	QString TelephonyUID = ISMetaUser::GetInstance().GetData()->TelephonyType;
	if (TelephonyUID.length())
	{
		if (TelephonyUID != CONST_UID_TELEPHONY_NOT_USE)
		{
			QString TelephonyClass = ISMetaUser::GetInstance().GetData()->TelephonyClass;

			int ObjectType = QMetaType::type((TelephonyClass + "*").toLocal8Bit().constData());
			IS_ASSERT(ObjectType, "Telephony class is NULL. ClassName: " + TelephonyClass);

			const QMetaObject *MetaObject = QMetaType::metaObjectForType(ObjectType);
			IS_ASSERT(MetaObject, "Error opening telephony class.");

			ISTelephonyBase *TelephonyBase = dynamic_cast<ISTelephonyBase*>(MetaObject->newInstance());
			IS_ASSERT(TelephonyBase, "Error instance telephony.");
			
			if (TelephonyUID == CONST_UID_TELEPHONY_ASTERISK)
			{
				TelephonyBase->SetUserName(CURRENT_USER_FULL_NAME);
				TelephonyBase->SetServer(ISMetaUser::GetInstance().GetData()->AsteriskServer);
				TelephonyBase->SetPort(ISMetaUser::GetInstance().GetData()->AsteriskPort);
				TelephonyBase->SetLogin(ISMetaUser::GetInstance().GetData()->AsteriskLogin);
				TelephonyBase->SetPassword(ISMetaUser::GetInstance().GetData()->AsteriskPassword);
				TelephonyBase->SetPattern(ISMetaUser::GetInstance().GetData()->AsteriskPattern);
			}
			else if (TelephonyUID == CONST_UID_TELEPHONY_BEELINE)
			{
				TelephonyBase->SetPattern(ISMetaUser::GetInstance().GetData()->BeelinePattern);
			}

			if (!TelephonyBase->Initialize())
			{
				ISMessageBox::ShowWarning(nullptr, LOCALIZATION("Message.Warning.ErrorInitializeTelephony"), TelephonyBase->GetErrorString());
			}

			ISTelephonyInterface::GetInstance().SetPointer(TelephonyBase);
		}
	}
}
//-----------------------------------------------------------------------------
ISNamespace::UpdateResult ISModeUser::CheckUpdate() //�������� ����������
{
	bool CheckUpdateActivate = SETTING_BOOL(CONST_UID_SETTING_GENERAL_CHECKUPDATEATSTART);
	if (!CheckUpdateActivate)
	{
		return ISNamespace::UR_ContinueWork;
	}

	int FileID = 0;
	QString FileName;
	QString Version;

	ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Banner.Initialize.CheckUpdate"));
	bool UpdateExist = ISUpdate::GetInstance().CheckUpdate(FileID, FileName, Version);
	if (UpdateExist) //���� ���������� �������
	{
		ISSplashScreen::GetInstance().hide();

		ISUpdateDownloadForm UpdateDownloadForm(FileID, FileName, Version);
		if (UpdateDownloadForm.Exec())
		{
			bool Started = ISUpdate::GetInstance().StartInstallUpdate(FileName);
			if (Started)
			{
				return ISNamespace::UR_ExitApplication;
			}
			else
			{
				return ISNamespace::UR_ErrorStartUpdate;
			}
		}
		else
		{
			ISMessageBox::ShowWarning(nullptr, LOCALIZATION("Message.Warning.DownloadUpdateError"));
		}

		ISSplashScreen::GetInstance().show();
	}

	return ISNamespace::UR_ContinueWork;
}
//-----------------------------------------------------------------------------
bool ISModeUser::CheckLicense()
{
	bool Result = true;

	if (ISLicense::GetInstance().GetTableEmpty())
	{
		ISMessageBox::ShowWarning(nullptr, LOCALIZATION("Message.Warning.NotFoundRecordLicense"), LOCALIZATION("MustCreateLicenseOnServer"));
		return false;
	}

	ISLicense::GetInstance().Initialize();
	if (ISLicense::GetInstance().GetActivated()) //���� ������������ ���� ����������
	{
		QString LicenseKey = ISLicense::GetInstance().GetLicenseKey();
		LicenseKey = ISCrypterLicense::Decrypt(LicenseKey);
		if (LicenseKey != ISLicense::GetInstance().GetLicenseUID()) //���� ������������� ���� ��������
		{
			ISMessageBox::ShowCritical(nullptr, LOCALIZATION("Message.Error.LicenseInvalid"));
			ISLicense::GetInstance().ResetLicense();
			return false;
		}
	}
	else //���� ������������ ���� �� ����������
	{
		ISSplashScreen::GetInstance().hide();

		ISLicenseForm LicenseForm;
		LicenseForm.Exec();

		ISSplashScreen::GetInstance().show();

		ISNamespace::LicenseFormAction SelectedAction = LicenseForm.GetSelectedAction();
		if (SelectedAction == ISNamespace::LFA_TestingApplication)
		{
			ISLicense::GetInstance().ReduceCounter();
			return true;
		}
		else if (SelectedAction == ISNamespace::LFA_Exit)
		{
			return false;
		}
	}

	return Result;
}
//-----------------------------------------------------------------------------
