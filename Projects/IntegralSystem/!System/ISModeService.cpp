#include "StdAfx.h"
#include "ISModeService.h"
#include "EXDefines.h"
#include "EXConstants.h"
#include "ISLocalization.h"
#include "ISSplashScreen.h"
#include "ISServiceWindow.h"
#include "ISQuery.h"
#include "ISMessageBox.h"
#include "ISDatabase.h"
#include "ISConfiguratorCreate.h"
//-----------------------------------------------------------------------------
static QString QC_DATABASE = "CREATE DATABASE %1 WITH OWNER = %2 ENCODING = 'UTF8'";
//-----------------------------------------------------------------------------
ISModeService::ISModeService(const QString &entered_login) : ISAbstractMode(entered_login)
{

}
//-----------------------------------------------------------------------------
ISModeService::~ISModeService()
{

}
//-----------------------------------------------------------------------------
int ISModeService::Startup()
{
	if (!CheckExistDatabase())
	{
		return EXIT_CODE_ERROR;
	}

	ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Banner.Service.OpeningMainWindow"));

	ISServiceWindow *ServiceWindow = new ISServiceWindow();
	ISSplashScreen::GetInstance().hide();
	ServiceWindow->showMaximized();

	return EXIT_CODE_NORMAL;
}
//-----------------------------------------------------------------------------
bool ISModeService::CheckExistDatabase()
{
	QString ErrorString;
	bool Result = true;

	ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Configurator.Message.ConnectToSystemDB"));
	if (ISDatabase::GetInstance().ConnectToSystemDB(ErrorString))
	{
		QString Database = ISConfig::GetInstance().GetValue(CONST_CONFIG_CONNECTION_DATABASE).toString();
		ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Configurator.Message.CheckExistDatabase").arg(Database));
		bool Exist = ISDatabase::GetInstance().CheckExistDatabase(Database);

		if (!Exist) //���� ���� ������ �� ���������� - ������� �
		{
			if (ISMessageBox::ShowQuestion(nullptr, LOCALIZATION("Message.Question.DatabaseCreate").arg(Database)))
			{
				ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Configurator.Message.CreatingNewDatabase").arg(Database));

				ISCountingTime Time;
				QSqlQuery SqlQuery = ISDatabase::GetInstance().GetSystemDB().exec(QC_DATABASE.arg(Database).arg(SYSTEM_USER_LOGIN)); //���������� ������� �� �������� ���� ������
				int Elapsed = Time.GetElapsed();

				QSqlError SqlError = SqlQuery.lastError();
				if (SqlError.type() == QSqlError::NoError)
				{
					ISMessageBox::ShowInformation(nullptr, LOCALIZATION("Configurator.Message.CreatingNewDatabaseDone").arg(Database));
				}
				else
				{
					ISMessageBox::ShowCritical(nullptr, LOCALIZATION("Configurator.Message.DatabaseCreatingError").arg(Database), SqlError.text());
					Result = false;
				}
			}
			else
			{
				ISMessageBox::ShowWarning(nullptr, LOCALIZATION("Message.Warning.DatabaseNotCreated"));
				Result = false;
			}
		}

		if (Result) //���� ���� ������ �������
		{
			QString ErrorString;
			if (!ISDatabase::GetInstance().ConnectToDefaultDB(SYSTEM_USER_LOGIN, SYSTEM_USER_PASSWORD, ErrorString))
			{
				ISMessageBox::ShowWarning(nullptr, LOCALIZATION("Configurator.Message.NotConnectingDatabase").arg(Database), ErrorString);
				Result = false;
			}
		}

		ISDatabase::GetInstance().DisconnectFromSystemDB();
	}
	else
	{
		ISMessageBox::ShowCritical(nullptr, ErrorString);
		Result = false;
	}

	return Result;
}
//-----------------------------------------------------------------------------
