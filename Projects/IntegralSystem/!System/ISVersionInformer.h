#pragma once
//-----------------------------------------------------------------------------
#include <QtCore/QObject>
#include <QtCore/QString>
//-----------------------------------------------------------------------------
class ISVersionInformer : public QObject
{
	Q_OBJECT

public:
	ISVersionInformer();
	virtual ~ISVersionInformer();

	static QString GetVersion(); //�������� ������ ������
	static QString GetRevision(); //�������� ����� �������
	static QString GetBuild(); //�������� ����� ������
	static QString GetDate(); //�������� ���� ������
	static QString GetHash(); //�������� ��� ���������� �������

protected:
	static void RemoveLastSpace(QString &String); //������� ��������� ������ �� ������
};
//-----------------------------------------------------------------------------