#pragma once
//-----------------------------------------------------------------------------
#include <QObject>
//-----------------------------------------------------------------------------
class ISCrashReport : public QObject
{
	Q_OBJECT

public:
	ISCrashReport();
	virtual ~ISCrashReport();

	static int ReportHook(int ReportType, char *Message, int *ReturnValue);
	static LONG ExceptionHandling(_EXCEPTION_POINTERS *ExceptionInfo);

protected:
	static void CreateReport(const QString &FileName, _EXCEPTION_POINTERS *ExceptionInfo);
};
//-----------------------------------------------------------------------------
