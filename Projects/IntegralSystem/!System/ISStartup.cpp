#include "StdAfx.h"
#include "ISStartup.h"
#include "EXDefines.h"
#include "ISMissingLibraryForm.h"
#include "ISSplashScreen.h"
#include "ISPlugin.h"
#include "ISSystem.h"
#include "ISLocalization.h"
#include "ISMessageBox.h"
#include "ISDebug.h"
#include "ISMetaData.h"
//-----------------------------------------------------------------------------
ISStartup::ISStartup() : QObject()
{

}
//-----------------------------------------------------------------------------
ISStartup::~ISStartup()
{

}
//-----------------------------------------------------------------------------
bool ISStartup::CheckAdminRole()
{
	QFile File(APPLICATION_TEMP_PATH + "/CheckAdmin");
	bool Opened = File.open(QIODevice::WriteOnly);

	if (Opened)
	{
		File.close();
		File.remove();
	}
	else
	{
		if (File.error() == QFileDevice::OpenError)
		{
			QMessageBox::critical(nullptr, "Error", "Access denied! Run the program as an administrator.");
		}
		else
		{
			Opened = true;
		}
	}

	return Opened;
}
//-----------------------------------------------------------------------------
void ISStartup::CheckRequiredLibrary()
{
	QStringList StringList;

	QDomElement DomElement = ISSystem::GetDomElement(QFile(REQUIRED_LIBRARY));
	QDomNode DomNode = DomElement.firstChild();
	while (!DomNode.isNull())
	{
		QString Name = DomNode.attributes().namedItem("Name").nodeValue();
		QString Extension = DomNode.attributes().namedItem("ExtensionWindows").nodeValue();
		QString LibraryName = Name + "." + Extension;

		QString FullPath = APPLICATION_DIR_PATH + "/" + LibraryName;
		bool LibraryExist = QFile::exists(FullPath);
		if (!LibraryExist)
		{
			StringList.append(FullPath);
		}

		DomNode = DomNode.nextSibling();
	}

	if (StringList.count())
	{
		ISMissingLibraryForm MissingLibraryForm;
		MissingLibraryForm.SetLibraryList(StringList);
		MissingLibraryForm.Exec();
	}
}
//-----------------------------------------------------------------------------
bool ISStartup::InitializedModule()
{
	//������������� ������
	ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Banner.Initialize.InitializeModule"));
	if (!ISPlugin::GetInstance().Load())
	{
		ISMessageBox::ShowCritical(nullptr, LOCALIZATION("Message.Error.ModuleNotInitialized"), ISPlugin::GetInstance().GetErrorString());
		return false;
	}

	return true;
}
//-----------------------------------------------------------------------------
void ISStartup::InitializedMetaData(bool InitXSR)
{
	//������������� ����-������
	ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Banner.Initialize.MetaData"));
	ISMetaData::GetInstanse().Initialize(InitXSR);
}
//-----------------------------------------------------------------------------
