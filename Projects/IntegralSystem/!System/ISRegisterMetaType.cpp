#include "StdAfx.h"
#include "ISRegisterMetaType.h"
//-----------------------------------------------------------------------------
#include "ISDelegateBoolean.h"
#include "ISDelegateImage.h"
//-----------------------------------------------------------------------------
#include "ISConfiguratorCreate.h"
#include "ISConfiguratorUpdate.h"
#include "ISConfiguratorDelete.h"
#include "ISConfiguratorService.h"
#include "ISConfiguratorOther.h"
//-----------------------------------------------------------------------------
#include "ISListObjectForm.h"
#include "ISUsersListForm.h"
#include "ISUserObjectForm.h"
#include "ISProtocolObjectListForm.h"
#include "ISEditObjectListForm.h"
#include "ISDiscussionListForm.h"
#include "ISStatisticsDatabaseForm.h"
#include "ISStatisticTablesForm.h"
#include "ISProtocolEntersForm.h"
#include "ISStorageFilesListForm.h"
#include "ISMonitorActivityForm.h"
#include "ISDistFileListForm.h"
#include "ISCalendarObjectForm.h"
#include "ISSettingsDatabaseListForm.h"
#include "ISDesktopBaseForm.h"
#include "ISWorkspaceForm.h"
#include "ISCalendarForm.h"
#include "ISChatForm.h"
#include "ISTasksForm.h"
#include "ISTaskFilesListForm.h"
#include "ISTaskObjectForm.h"
#include "ISTasksStatusListForm.h"
#include "ISProtocolBaseListForm.h"
#include "ISTaskAttachedCardsForm.h"
#include "ISTasksAllListForm.h"
#include "ISTasksFromMeListForm.h"
#include "ISTasksMyListForm.h"
#include "ISApplicationsListForm.h"
#include "ISEMailListForm.h"
#include "ISEMailObjectForm.h"
#include "ISUserGroupAccessObjectForm.h"
//-----------------------------------------------------------------------------
#include "ISLineEdit.h"
#include "ISCheckEdit.h"
#include "ISIntegerEdit.h"
#include "ISDoubleEdit.h"
#include "ISTextEdit.h"
#include "ISDateTimeEdit.h"
#include "ISDateEdit.h"
#include "ISTimeEdit.h"
#include "ISImageEdit.h"
#include "ISListEdit.h"
#include "ISFontEdit.h"
#include "ISIPAddressEdit.h"
#include "ISPathEditFile.h"
#include "ISPathEditDir.h"
#include "ISPasswordEdit.h"
#include "ISColorEdit.h"
#include "ISPathEditDir.h"
#include "ISWeekDayEdit.h"
#include "ISINNEdit.h"
#include "ISOKPOEdit.h"
#include "ISKPPEdit.h"
#include "ISOGRNEdit.h"
#include "ISYearEdit.h"
#include "ISComboTimeEdit.h"
#include "ISPhoneEdit.h"
#include "ISUserEdit.h"
#include "ISDatabaseEdit.h"
#include "ISBIKEdit.h"
#include "ISICQEdit.h"
#include "ISTaskExecutorEdit.h"
#include "ISEMailEdit.h"
#include "ISPassportEdit.h"
#include "ISDivisionCodeEdit.h"
//-----------------------------------------------------------------------------
#include "ISComboSettingSelectionBehavior.h"
#include "ISComboSettingCalendarNoteType.h"
#include "ISComboSettingStartedParagraph.h"
//-----------------------------------------------------------------------------
#include "ISAsterisk.h"
#include "ISBeeline.h"
//-----------------------------------------------------------------------------
#include "ISTasksSqlModel.h"
#include "ISSqlModelView.h"
//-----------------------------------------------------------------------------
ISRegisterMetaType::ISRegisterMetaType() : QObject()
{

}
//-----------------------------------------------------------------------------
ISRegisterMetaType::~ISRegisterMetaType()
{

}
//-----------------------------------------------------------------------------
void ISRegisterMetaType::RegisterMetaType()
{
	qRegisterMetaType<QList<QSqlRecord>>("QList<QSqlRecord>");
	qRegisterMetaType<QSqlError>("QSqlError");

	qRegisterMetaType<ISConfiguratorCreate*>("ISConfiguratorCreate");
	qRegisterMetaType<ISConfiguratorUpdate*>("ISConfiguratorUpdate");
	qRegisterMetaType<ISConfiguratorDelete*>("ISConfiguratorDelete");
	qRegisterMetaType<ISConfiguratorService*>("ISConfiguratorService");
	qRegisterMetaType<ISConfiguratorOther*>("ISConfiguratorOther");

	qRegisterMetaType<ISListObjectForm*>("ISListObjectForm");
	qRegisterMetaType<ISUsersListForm*>("ISUsersListForm");
	qRegisterMetaType<ISUserObjectForm*>("ISUserObjectForm");
	qRegisterMetaType<ISProtocolObjectListForm*>("ISProtocolObjectListForm");
	qRegisterMetaType<ISEditObjectListForm*>("ISEditObjectListForm");
	qRegisterMetaType<ISStatisticsDatabaseForm*>("ISStatisticsDatabaseForm");
	qRegisterMetaType<ISStatisticTablesForm*>("ISStatisticTablesForm");
	qRegisterMetaType<ISProtocolEntersForm*>("ISProtocolEnterstForm");
	qRegisterMetaType<ISDiscussionListForm*>("ISDiscussionListForm");
	qRegisterMetaType<ISStorageFilesListForm*>("ISStorageFilesListForm");
	qRegisterMetaType<ISMonitorActivityForm*>("ISMonitorActivityForm");
	qRegisterMetaType<ISDistFileListForm*>("ISDistFileListForm");
	qRegisterMetaType<ISCalendarObjectForm*>("ISCalendarObjectForm");
	qRegisterMetaType<ISSettingsDatabaseListForm*>("ISSettingsDatabaseListForm");
	qRegisterMetaType<ISDesktopBaseForm*>("ISDesktopBaseForm");
	qRegisterMetaType<ISWorkspaceForm*>("ISWorkspaceForm");
	qRegisterMetaType<ISCalendarForm*>("ISCalendarForm");
	qRegisterMetaType<ISChatForm*>("ISChatForm");
	qRegisterMetaType<ISTasksForm*>("ISTasksForm");
	qRegisterMetaType<ISTaskFilesListForm*>("ISTaskFilesListForm");
	qRegisterMetaType<ISTaskObjectForm*>("ISTaskObjectForm");
	qRegisterMetaType<ISTasksStatusListForm*>("ISTasksStatusListForm");
	qRegisterMetaType<ISProtocolBaseListForm*>("ISProtocolBaseListForm");
	qRegisterMetaType<ISTaskAttachedCardsForm*>("ISTaskAttachedCardsForm");
	qRegisterMetaType<ISTasksAllListForm*>("ISTasksAllListForm");
	qRegisterMetaType<ISTasksFromMeListForm*>("ISTasksFromMeListForm");
	qRegisterMetaType<ISTasksMyListForm*>("ISTasksMyListForm");
	qRegisterMetaType<ISApplicationsListForm*>("ISApplicationsListForm");
	qRegisterMetaType<ISEMailListForm*>("ISEMailListForm");
	qRegisterMetaType<ISEMailObjectForm*>("ISEMailObjectForm");
	qRegisterMetaType<ISUserGroupAccessObjectForm*>("ISUserGroupAccessObjectForm");

	qRegisterMetaType<ISDelegateBoolean*>("ISDelegateBoolean");
	qRegisterMetaType<ISDelegateImage*>("ISDelegateImage");

	qRegisterMetaType<ISLineEdit*>("ISLineEdit");
	qRegisterMetaType<ISCheckEdit*>("ISCheckEdit"); 
	qRegisterMetaType<ISIntegerEdit*>("ISIntegerEdit");
	qRegisterMetaType<ISDoubleEdit*>("ISDoubleEdit");
	qRegisterMetaType<ISTextEdit*>("ISTextEdit");
	qRegisterMetaType<ISDateTimeEdit*>("ISDateTimeEdit");
	qRegisterMetaType<ISDateEdit*>("ISDateEdit");
	qRegisterMetaType<ISTimeEdit*>("ISTimeEdit");
	qRegisterMetaType<ISImageEdit*>("ISImageEdit");
	qRegisterMetaType<ISListEdit*>("ISListEdit");
	qRegisterMetaType<ISFontEdit*>("ISFontEdit");
	qRegisterMetaType<ISIPAddressEdit*>("ISIPAddressEdit");
	qRegisterMetaType<ISPathEditFile*>("ISPathEditFile");
	qRegisterMetaType<ISPathEditDir*>("ISPathEditDir"); 
	qRegisterMetaType<ISPasswordEdit*>("ISPasswordEdit");
	qRegisterMetaType<ISColorEdit*>("ISColorEdit");
	qRegisterMetaType<ISImageEdit*>("ISImageEdit");
	qRegisterMetaType<ISPathEditDir*>("ISPathEditDir");
	qRegisterMetaType<ISWeekDayEdit*>("ISWeekDayEdit");
	qRegisterMetaType<ISINNEdit*>("ISINNEdit");
	qRegisterMetaType<ISOKPOEdit*>("ISOKPOEdit");
	qRegisterMetaType<ISKPPEdit*>("ISKPPEdit");
	qRegisterMetaType<ISOGRNEdit*>("ISOGRNEdit");
	qRegisterMetaType<ISYearEdit*>("ISYearEdit");
	qRegisterMetaType<ISComboTimeEdit*>("ISComboTimeEdit");
	qRegisterMetaType<ISPhoneEdit*>("ISPhoneEdit");
	qRegisterMetaType<ISUserEdit*>("ISUserEdit");
	qRegisterMetaType<ISDatabaseEdit*>("ISDatabaseEdit");
	qRegisterMetaType<ISBIKEdit*>("ISBIKEdit");
	qRegisterMetaType<ISICQEdit*>("ISICQEdit");
	qRegisterMetaType<ISTaskExecutorEdit*>("ISTaskExecutorEdit");
	qRegisterMetaType<ISEMailEdit*>("ISEMailEdit");
	qRegisterMetaType<ISPassportEdit*>("ISPassportEdit");
	qRegisterMetaType<ISDivisionCodeEdit*>("ISDivisionCodeEdit");

	qRegisterMetaType<ISComboSettingSelectionBehavior*>("ISComboSettingSelectionBehavior");
	qRegisterMetaType<ISComboSettingCalendarNoteType*>("ISComboSettingCalendarNoteType");
	qRegisterMetaType<ISComboSettingStartedParagraph*>("ISComboSettingStartParagraph");
	
	qRegisterMetaType<ISAsterisk*>("ISAsterisk");
	qRegisterMetaType<ISBeeline*>("ISBeeline");

	qRegisterMetaType<ISTasksSqlModel*>("ISTasksSqlModel");
	qRegisterMetaType<ISSqlModelView*>("ISSqlModelView");
}
//-----------------------------------------------------------------------------
