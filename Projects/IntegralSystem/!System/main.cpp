#include "StdAfx.h"
#include "EXDefines.h"
#include "ISLocalization.h"
#include "ISStyleSheet.h"
#include "ISBuffer.h"
#include "ISCore.h"
#include "ISDebug.h"
#include "ISAssert.h"
#include "ISSplashScreen.h"
#include "ISSplashWidget.h"
#include "ISAuthorizationForm.h"
#include "ISConfig.h"
#include "ISRegisterMetaType.h"
#include "ISIntegralSystem.h"
#include "ISSystem.h"
#include "ISAbstractMode.h"
#include "ISModeService.h"
#include "ISModeUser.h"
#include "ISStartup.h"
#include "ISCrashReport.h"
#include "ISLibraryLoader.h"
//-----------------------------------------------------------------------------
int main(int argc, char *argv[])
{
	ISIntegralSystem IntegralApplication(argc, argv);
	ISDebug::ShowString(DEBUG_SPLITTER);
	ISDebug::ShowString("Starting system...");

	ISLibraryLoader LibraryLoader;
	LibraryLoader.Load();

	//�������� ������� ���� ��������������
	if (!ISStartup::CheckAdminRole())
	{
		return EXIT_CODE_ERROR;
	}
	
	IntegralApplication.Initialize();

	ISSplashWidget SplashWidget;
	SplashWidget.SetText("Starting system...");
	SplashWidget.show();
	ISSystem::ProcessEvents();

	SetUnhandledExceptionFilter((LPTOP_LEVEL_EXCEPTION_FILTER)ISCrashReport::ExceptionHandling);
	_CrtSetReportHook(&ISCrashReport::ReportHook);

	//�������� ����������������� �����
	SplashWidget.SetText("Loading config...");
	ISConfig::GetInstance().Initialize(CONFIG_FILE_PATH);

	//�������� �����������
	SplashWidget.SetText("Loading localization...");
	ISLocalization::GetInstance().LoadResourceFile(LOCALIZATION_FILE_CORE);
	ISLocalization::GetInstance().LoadResourceFile(LOCALIZATION_FILE_INTEGRAL_SYSTEM);
	ISLocalization::GetInstance().LoadResourceFile(LOCALIZATION_FILE_PLUGINS);

	//�������� ������ ����������
	SplashWidget.SetText("Loading styles...");
	ISStyleSheet::GetInstance();

	SplashWidget.close();

	//������
	ISSplashScreen::GetInstance().show();
	ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Banner.Initialize.System"));

	//����������� ����-�����
	ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Banner.Initialize.RegistrationMetaTypes"));
	ISRegisterMetaType::RegisterMetaType();

	//�������� ������
	ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Banner.Initialize.Buffer"));
	ISBuffer::GetInstance();

	//�������� ����������� ���������
	ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Banner.Initialize.CheckRequiredLibraries"));
	ISStartup::CheckRequiredLibrary();

	//�������� ����� �����������
	ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Banner.Initialize.ShowAuthorizationForm"));
	ISAuthorizationForm *AuthorizationForm = new ISAuthorizationForm();
	ISSplashScreen::GetInstance().hide();
	ISSplashScreen::GetInstance().clearMessage();

	bool Result = AuthorizationForm->ExecAnimated();
	if (!Result)
	{
		return EXIT_CODE_NORMAL;
	}

	ISSplashScreen::GetInstance().show();
	QString EnteredLogin = AuthorizationForm->GetEnteredLogin();
	ISNamespace::InputType SelectedInputType = AuthorizationForm->GetInputType();

	delete AuthorizationForm;

	ISAbstractMode *Input = nullptr;
	bool InitXSR = false;

	if (SelectedInputType == ISNamespace::IT_Client)
	{
		Input = new ISModeUser(EnteredLogin);
	}
	else if (SelectedInputType == ISNamespace::IT_Service)
	{
		Input = new ISModeService(EnteredLogin);
		InitXSR = true;
	}

	if (!ISStartup::InitializedModule())
	{
		ISSplashScreen::GetInstance().close(); //�������� ��� ��������� ������� (������ #3114)
		return EXIT_CODE_ERROR;
	}

	ISStartup::InitializedMetaData(InitXSR);

	int Startup = Input->Startup();

	delete Input;

	if (Startup != 0) //���� ��� ������� ��������� ������
	{
		ISCore::ExitApplication();
		return Startup;
	}

	ISSplashScreen::GetInstance().ResetPixmap();
	int Exec = IntegralApplication.exec();
	return Exec;
}
//-----------------------------------------------------------------------------
