#include "StdAfx.h"
#include "ISIntegralSystem.h"
#include "EXDefines.h"
#include "ISQueryException.h"
#include "ISMessageBox.h"
#include "ISLocalization.h"
#include "ISReconnectForm.h"
#include "ISStyleSheet.h"
#include "ISVersionInformer.h"
#include "ISBuffer.h"
#include "ISAutoLocking.h"
//-----------------------------------------------------------------------------
ISIntegralSystem::ISIntegralSystem(int &argc, char **argv) : QApplication(argc,	argv)
{
	
}
//-----------------------------------------------------------------------------
ISIntegralSystem::~ISIntegralSystem()
{
	
}
//-----------------------------------------------------------------------------
void ISIntegralSystem::Initialize()
{
	setEffectEnabled(Qt::UIEffect::UI_AnimateTooltip);
	setEffectEnabled(Qt::UIEffect::UI_FadeTooltip);
	setEffectEnabled(Qt::UIEffect::UI_AnimateMenu);
	setEffectEnabled(Qt::UIEffect::UI_FadeMenu);
	setEffectEnabled(Qt::UIEffect::UI_AnimateCombo);

	setStyleSheet(STYLE_SHEET("QToolTip"));
	setWindowIcon(BUFFER_ICONS("Logo"));
	setApplicationName("IntegralSystem");
	setApplicationVersion(ISVersionInformer::GetVersion());
	setFont(FONT_APPLICATION);

	QToolTip::setFont(FONT_APPLICATION);
}
//-----------------------------------------------------------------------------
bool ISIntegralSystem::notify(QObject *Object, QEvent *e)
{
	bool Result = false;

	try
	{
		Result = QApplication::notify(Object, e);
		
		if (ISAutoLocking::GetInstance().GetInitialized()) //���� ����� �������������� ���������������
		{
			if (!ISAutoLocking::GetInstance().GetLock()) //���� ��������� �� ������������� - ����������� �� �������
			{
				switch (e->type())
				{
				case QEvent::Move: ISAutoLocking::GetInstance().RestartTimer(); break;
				case QEvent::MouseMove: ISAutoLocking::GetInstance().RestartTimer(); break;
				case QEvent::Show: ISAutoLocking::GetInstance().RestartTimer(); break;
				case QEvent::Hide: ISAutoLocking::GetInstance().RestartTimer(); break;
				case QEvent::WindowActivate: ISAutoLocking::GetInstance().RestartTimer(); break;
				case QEvent::WindowDeactivate: ISAutoLocking::GetInstance().RestartTimer(); break;
				}
			}
		}
	}
	catch (ISQueryExceptionConnection &e)
	{
		if (ISMessageBox::ShowQuestion(nullptr, LOCALIZATION("Message.Question.Reconnect")))
		{
			ISReconnectForm::GetInstance().show();
			ISReconnectForm::GetInstance().StartReconnect(Result);
			if (Result)
			{
				ISMessageBox::ShowInformation(nullptr, LOCALIZATION("Message.Information.Reconnect.Done"));
			}
		}
	}

	return Result;
}
//-----------------------------------------------------------------------------
