#pragma once
//-----------------------------------------------------------------------------
#include <QWidget>
#include <QLabel>
//-----------------------------------------------------------------------------
class ISChatMessageWidget : public QWidget
{
	Q_OBJECT

public:
	ISChatMessageWidget(int message_id, const QString &Message, const QByteArray &ByteImage, const QString &FileName, const QString &UserFullName, const QDateTime &DateTime, QWidget *parent = 0);
	virtual ~ISChatMessageWidget();

	QString GetMessage() const;

protected:
	void DoubleClickedImage();
	void SaveImage();
	void SaveFile();

private:
	int MessageID;
	
	QLabel *LabelMessage;
};
//-----------------------------------------------------------------------------
