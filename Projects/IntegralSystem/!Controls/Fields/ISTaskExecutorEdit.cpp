#include "StdAfx.h"
#include "ISTaskExecutorEdit.h"
#include "EXDefines.h"
#include "ISPushButton.h"
#include "ISLocalization.h"
#include "ISMetaUser.h"
//-----------------------------------------------------------------------------
ISTaskExecutorEdit::ISTaskExecutorEdit(QObject *MetaField, QWidget *parent) : ISListEdit(MetaField, parent)
{
	ISPushButton *ButtonAppointMe = new ISPushButton(this);
	ButtonAppointMe->setText(LOCALIZATION("AppointMe"));
	ButtonAppointMe->setToolTip(LOCALIZATION("ClickFromAppointExecutedThisTask"));
	ButtonAppointMe->setSizePolicy(QSizePolicy::Maximum, ButtonAppointMe->sizePolicy().verticalPolicy());
	ButtonAppointMe->setCursor(CURSOR_POINTING_HAND);
	connect(ButtonAppointMe, &ISPushButton::clicked, this, &ISTaskExecutorEdit::AppointMe);
	AddWidgetToRight(ButtonAppointMe);
}
//-----------------------------------------------------------------------------
ISTaskExecutorEdit::ISTaskExecutorEdit(QWidget *parent) : ISTaskExecutorEdit(nullptr, parent)
{

}
//-----------------------------------------------------------------------------
ISTaskExecutorEdit::~ISTaskExecutorEdit()
{

}
//-----------------------------------------------------------------------------
void ISTaskExecutorEdit::AppointMe()
{
	SetValue(CURRENT_USER_ID);
}
//-----------------------------------------------------------------------------
