#include "StdAfx.h"
#include "ISDatabaseEdit.h"
#include "ISServiceButton.h"
#include "ISLocalization.h"
#include "ISDatabaseForm.h"
#include "ISSystem.h"
//-----------------------------------------------------------------------------
ISDatabaseEdit::ISDatabaseEdit(QObject *MetaField, QWidget *parent) : ISLineEdit(MetaField, parent)
{
	Port = -1;
	
#ifdef QT_DEBUG
	ISServiceButton *ButtonSelect = new ISServiceButton(this);
	ButtonSelect->setText("...");
	ButtonSelect->setToolTip(LOCALIZATION("SelectDatabase") + "...");
	connect(ButtonSelect, &ISServiceButton::clicked, this, &ISDatabaseEdit::SelectDatabase);
	AddWidgetToRight(ButtonSelect);
#endif
}
//-----------------------------------------------------------------------------
ISDatabaseEdit::ISDatabaseEdit(QWidget *parent) : ISDatabaseEdit(nullptr, parent)
{

}
//-----------------------------------------------------------------------------
ISDatabaseEdit::~ISDatabaseEdit()
{

}
//-----------------------------------------------------------------------------
void ISDatabaseEdit::SetServer(const QVariant &server)
{
	Server = server.toString();
}
//-----------------------------------------------------------------------------
void ISDatabaseEdit::SetPort(const QVariant &port)
{
	Port = port.toInt();
}
//-----------------------------------------------------------------------------
void ISDatabaseEdit::SelectDatabase()
{
	ISSystem::SetWaitGlobalCursor(true);
	ISDatabaseForm DatabaseForm(Server, Port, this);
	connect(&DatabaseForm, &ISDatabaseForm::Selected, this, &ISDatabaseEdit::SetValue);
	ISSystem::SetWaitGlobalCursor(false);

	DatabaseForm.Exec();
}
//-----------------------------------------------------------------------------
