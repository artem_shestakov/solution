#pragma once
//-----------------------------------------------------------------------------
#include "ISComboEdit.h"
//-----------------------------------------------------------------------------
class ISComboTimeEdit : public ISComboEdit
{
	Q_OBJECT

public:
	Q_INVOKABLE ISComboTimeEdit(QObject *MetaField, QWidget *parent);
	Q_INVOKABLE ISComboTimeEdit(QWidget *parent);
	virtual ~ISComboTimeEdit();
};
//-----------------------------------------------------------------------------
