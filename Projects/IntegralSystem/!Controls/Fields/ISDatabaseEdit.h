#pragma once
//-----------------------------------------------------------------------------
#include "ISLineEdit.h"
//-----------------------------------------------------------------------------
class ISDatabaseEdit : public ISLineEdit
{
	Q_OBJECT

public:
	Q_INVOKABLE ISDatabaseEdit(QObject *MetaField, QWidget *parent);
	Q_INVOKABLE ISDatabaseEdit(QWidget *parent);
	virtual ~ISDatabaseEdit();

	void SetServer(const QVariant &server);
	void SetPort(const QVariant &port);

protected:
	void SelectDatabase(); //����� ���� ������

private:
	QString Server;
	int Port;
};
//-----------------------------------------------------------------------------
