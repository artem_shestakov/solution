#pragma once
//-----------------------------------------------------------------------------
#include "ISListEdit.h"
//-----------------------------------------------------------------------------
class ISTaskExecutorEdit : public ISListEdit
{
	Q_OBJECT

public:
	Q_INVOKABLE ISTaskExecutorEdit(QObject *MetaField, QWidget *parent);
	Q_INVOKABLE ISTaskExecutorEdit(QWidget *parent);
	virtual ~ISTaskExecutorEdit();

protected:
	void AppointMe();
};
//-----------------------------------------------------------------------------
