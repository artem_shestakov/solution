#pragma once
//-----------------------------------------------------------------------------
#include <QToolButton>
//-----------------------------------------------------------------------------
class ISCalculatorButton : public QToolButton
{
    Q_OBJECT

public:
    explicit ISCalculatorButton(QWidget *parent = 0);

    QSize sizeHint() const override;
};
//-----------------------------------------------------------------------------
