#include "StdAfx.h"
#include "ISTasksSqlModel.h"
#include "ISQuery.h"
#include "ISSystem.h"
//-----------------------------------------------------------------------------
static QString QS_TASK_STATUS = PREPARE_QUERY("SELECT tsst_uid, tsst_name, tsst_color, tsst_defaultcolor FROM _taskstatus WHERE NOT tsst_isdeleted ORDER BY tsst_id");
//-----------------------------------------------------------------------------
ISTasksSqlModel::ISTasksSqlModel(PMetaClassTable *meta_table, QObject *parent) : ISSqlModelCore(meta_table, parent)
{
	ISQuery qSelect(QS_TASK_STATUS);
	if (qSelect.Execute())
	{
		while (qSelect.Next())
		{
			ISUuid UID = qSelect.ReadColumn("tsst_uid").toString();
			QString Name = qSelect.ReadColumn("tsst_name").toString();
			QString ColorString = qSelect.ReadColumn("tsst_color").toString();
			QString DefaultColor = qSelect.ReadColumn("tsst_defaultcolor").toString();

			QObject *Object = new QObject(this);
			Object->setProperty("UID", UID);

			if (ColorString.length())
			{
				Object->setProperty("Color", ISSystem::StringToColor(ColorString));
			}
			else
			{
				Object->setProperty("Color", ISSystem::StringToColor(DefaultColor));
			}

			Objects.insert(Name, Object);
		}
	}
}
//-----------------------------------------------------------------------------
ISTasksSqlModel::~ISTasksSqlModel()
{

}
//-----------------------------------------------------------------------------
QVariant ISTasksSqlModel::data(const QModelIndex &ModelIndex, int Role) const
{
	QVariant Value = ISSqlModelCore::data(ModelIndex, Role);

	if (Role == Qt::TextColorRole)
	{
		QString TaskStatus = GetRecord(ModelIndex.row()).value("Status").toString();

		if (Objects.value(TaskStatus)->property("UID").toString() != "")
		{
			if (GetRecord(ModelIndex.row()).value("DateOfDone").toDate() == QDate::currentDate().addDays(-1))
			{
				return qVariantFromValue(QColor(Qt::red));
			}
		}
		
		QColor Color = qvariant_cast<QColor>(Objects.value(TaskStatus)->property("Color"));
		if (Color.isValid())
		{
			return Color;
		}
	}

	return Value;
}
//-----------------------------------------------------------------------------
