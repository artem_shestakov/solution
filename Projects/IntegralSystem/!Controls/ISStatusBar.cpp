#include "StdAfx.h"
#include "ISStatusBar.h"
#include "ISLocalization.h"
#include "ISCore.h"
#include "ISBuffer.h"
#include "ISStyleSheet.h"
#include "ISConfig.h"
#include "EXDefines.h"
#include "ISSystem.h"
#include "ISSettings.h"
#include "EXConstants.h"
#include "ISMessageBox.h"
#include "ISServiceButton.h"
//-----------------------------------------------------------------------------
ISStatusBar::ISStatusBar(QWidget *parent) : QStatusBar(parent)
{
	ButtonDateTime = nullptr;

	bool ShowDateTime = SETTING_BOOL(CONST_UID_SETTING_STATUS_BAR_SHOWDATETIME);
	if (ShowDateTime)
	{
		CreateDateTime();
	}

	CreateConnectData();

	bool ShowWeather = SETTING_BOOL(CONST_UID_SETTING_VIEW_SHOWWEATHERLABEL);
	if (ShowWeather)
	{
		CreateWeatherLabel();
	}
}
//-----------------------------------------------------------------------------
ISStatusBar::~ISStatusBar()
{

}
//-----------------------------------------------------------------------------
void ISStatusBar::CreateDateTime()
{
	ButtonDateTime = new ISPushButton(this);
	ButtonDateTime->setText(GetCurrentDateTime());
	ButtonDateTime->setToolTip(LOCALIZATION("ClickFromGoOwerToCalendar"));
	ButtonDateTime->setCursor(CURSOR_POINTING_HAND);
	ButtonDateTime->setFont(Font);
	ButtonDateTime->setFlat(true);
	connect(ButtonDateTime, &ISPushButton::clicked, this, &ISStatusBar::DateTimeClicked);
	addWidget(ButtonDateTime);

	QTimer *Timer = new QTimer(this);
	Timer->setInterval(1000);
	connect(Timer, &QTimer::timeout, this, &ISStatusBar::TimerTick);
	Timer->start();
}
//-----------------------------------------------------------------------------
void ISStatusBar::CreateConnectData()
{
	ISServiceButton *ButtonConnection = new ISServiceButton(this);
	ButtonConnection->setToolTip(LOCALIZATION("ConnectionInfo"));
	ButtonConnection->setIcon(BUFFER_ICONS("DatabaseConnection"));
	ButtonConnection->setFlat(true);
	addWidget(ButtonConnection);
	connect(ButtonConnection, &ISPushButton::clicked, [=]
	{
		QString String;
		String += LOCALIZATION("Server") + ": " + ISConfig::GetInstance().GetValue(CONST_CONFIG_CONNECTION_SERVER).toString() + "\n";
		String += LOCALIZATION("Port") + ": " + ISConfig::GetInstance().GetValue(CONST_CONFIG_CONNECTION_PORT).toString() + "\n";
		String += LOCALIZATION("Database") + ": " + ISConfig::GetInstance().GetValue(CONST_CONFIG_CONNECTION_DATABASE).toString();

		ISMessageBox::ShowInformation(this, String);
	});
}
//-----------------------------------------------------------------------------
void ISStatusBar::CreateWeatherLabel()
{
	ButtonWeather = new ISButtonWeather(this);
	addWidget(ButtonWeather);
}
//-----------------------------------------------------------------------------
void ISStatusBar::TimerTick()
{
	ButtonDateTime->setText(GetCurrentDateTime());
}
//-----------------------------------------------------------------------------
QString ISStatusBar::GetCurrentDateTime() const
{
	return QString("%1: %2 %3").arg(LOCALIZATION("Today")).arg(ISSystem::GetCurrentDayOfWeekName()).arg(QDateTime::currentDateTime().toString(DATE_TIME_FORMAT_V3));
}
//-----------------------------------------------------------------------------
