#pragma once
//-----------------------------------------------------------------------------
#include "ISPushButton.h"
#include "ISButtonWeather.h"
//-----------------------------------------------------------------------------
class ISStatusBar : public QStatusBar
{
	Q_OBJECT

signals:
	void DateTimeClicked();

public:
	ISStatusBar(QWidget *parent = 0);
	virtual ~ISStatusBar();

protected:
	void CreateDateTime(); //������ � ������� ����� � ��������
	void CreateConnectData(); //������ � ������� ����������� � ��
	void CreateWeatherLabel(); //������ ������

	void TimerTick();
	QString GetCurrentDateTime() const;

private:
	QFont Font;	
	ISPushButton *ButtonDateTime;

	ISButtonWeather *ButtonWeather;
};
//-----------------------------------------------------------------------------
