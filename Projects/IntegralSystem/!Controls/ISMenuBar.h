#pragma once
//-----------------------------------------------------------------------------
#include "ISNamespace.h"
#include "ISMainMenu.h"
#include "ISParagraphButton.h"
#include "ISUuid.h"
//-----------------------------------------------------------------------------
class ISMenuBar : public QWidget
{
	Q_OBJECT

signals:
	void CreateRecords();
	void ExternalTools();
	void ParagraphClicked(const ISUuid &ParagraphUID);
	void Lock();
	void ChangeUser();
	void Exit();
	void Favorites();
	void History();
	void ChangePassword();
	void CreateLogToday();
	void Notifications();
	void Debug();
	void Settings();
	void Notebook();
	void Calculator();
	void HelpSystem();
	void AboutApplication();
	void AboutQt();
	void License();

public:
	ISMenuBar(QWidget *parent = 0);
	virtual ~ISMenuBar();

	QPushButton* CreateParagraphButton(const ISUuid &ParagraphUID, const QIcon &Icon, const QString &Text); //�������� ������
	void ButtonParagraphClicked(QAbstractButton *AbstractButton);
	void ButtonParagraphClicked(const ISUuid &ClickedParagraphUID);

	void ButtonNotifyStartBlink();
	void ButtonNotifyStopBlink();

protected:
	void CreateFastAccessMenu(); //�������� �������� �������
	void CreateMenuFile(); //�������� ���� "����"
	void CreateMenuService(); //�������� ���� "������"
	void CreateMenuAddition(); //�������� ���� "�������"
	void CreateMenuHelp(); //�������� ���� "�������"
	
	void CreateButtonsPanel(); //�������� ������ � ��������

	QToolButton* CreateButton(const QString &ToolTip, const QString &IconName);
	void ShowMainMenu();
	void HideMainMenu();

	void NotifyButtonTimeout();

private:
	QHBoxLayout *MainLayout;
	QHBoxLayout *LayoutMainButtons;
	QHBoxLayout *LayoutButtons;

	QButtonGroup *ButtonGroup;

	QToolButton *ButtonMenu;
	ISMainMenu *MainMenu;
	QAbstractButton *CurrentButton;
	QToolButton *ButtonNotify;

	QMap<QString, ISParagraphButton*> ParagraphButtons;

	QIcon IconNotifyNew;
	QIcon IconNotifyNull;

	QTimer *TimerNotify;
	bool IsNull;
};
//-----------------------------------------------------------------------------
