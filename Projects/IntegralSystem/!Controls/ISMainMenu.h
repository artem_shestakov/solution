#pragma once
//-----------------------------------------------------------------------------
#include <QMenu>
#include <QPropertyAnimation>
//-----------------------------------------------------------------------------
class ISMainMenu : public QMenu
{
	Q_OBJECT

public:
	ISMainMenu(QWidget *parent = 0);
	virtual ~ISMainMenu();

	void Show(const QPoint &pos);

private:
	QPropertyAnimation *PropertyAnimation;
};
//-----------------------------------------------------------------------------
