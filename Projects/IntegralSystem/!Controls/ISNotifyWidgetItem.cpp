#include "StdAfx.h"
#include "ISNotifyWidgetItem.h"
#include "EXDefines.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISServiceButton.h"
//-----------------------------------------------------------------------------
ISNotifyWidgetItem::ISNotifyWidgetItem(QWidget *parent, int notification_id, const QDateTime &notify_date_time, QListWidgetItem *list_widget_item, const QString &Message) : QWidget(parent)
{
	NotificationID = notification_id;
	ListWidgetItem = list_widget_item;

	QHBoxLayout *Layout = new QHBoxLayout();
	Layout->setContentsMargins(LAYOUT_MARGINS_5_PX);
	setLayout(Layout);

	QVBoxLayout *LayoutMessage = new QVBoxLayout();
	Layout->addLayout(LayoutMessage);

	QLabel *LabelDateTime = new QLabel(this);
	LabelDateTime->setText(notify_date_time.toString(DATE_TIME_FORMAT_V3) + ":");
	LabelDateTime->setFont(FONT_APPLICATION_BOLD);
	LayoutMessage->addWidget(LabelDateTime, 0, Qt::AlignLeft);
	
	QLabel *LabelMessage = new QLabel(this);
	LabelMessage->setText(Message);
	LabelMessage->setWordWrap(true);
	LayoutMessage->addWidget(LabelMessage);

	ISServiceButton *ButtonDelete = new ISServiceButton(this);
	ButtonDelete->setIcon(BUFFER_ICONS("Exit"));
	ButtonDelete->setToolTip(LOCALIZATION("DeleteNotification"));
	ButtonDelete->setFlat(true);
	connect(ButtonDelete, &ISServiceButton::clicked, this, &ISNotifyWidgetItem::DeleteClicked);
	Layout->addWidget(ButtonDelete);
}
//-----------------------------------------------------------------------------
ISNotifyWidgetItem::~ISNotifyWidgetItem()
{

}
//-----------------------------------------------------------------------------
void ISNotifyWidgetItem::DeleteClicked()
{
	emit Delete(NotificationID, ListWidgetItem);
}
//-----------------------------------------------------------------------------
