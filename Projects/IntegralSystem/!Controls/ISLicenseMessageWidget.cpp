#include "StdAfx.h"
#include "ISLicenseMessageWidget.h"
#include "EXDefines.h"
#include "ISLocalization.h"
#include "ISQLabel.h"
#include "ISLicense.h"
#include "ISMessageBox.h"
#include "ISCore.h"
#include "ISControls.h"
#include "ISPushButton.h"
//-----------------------------------------------------------------------------
ISLicenseMessageWidget::ISLicenseMessageWidget(QWidget *parent) : QWidget(parent)
{
	QVBoxLayout *Layout = new QVBoxLayout();
	setLayout(Layout);

	QHBoxLayout *LayoutLabel = new QHBoxLayout();
	Layout->addLayout(LayoutLabel);

	QFont Font("Arial", 12);
	Font.setBold(true);
	Font.setUnderline(true);

	ISQLabel *LabelLicense = new ISQLabel(this);
	LabelLicense->setFont(Font);
	LabelLicense->setCursor(CURSOR_POINTING_HAND);
	LabelLicense->setText(LOCALIZATION("License.Warning").arg(ISLicense::GetInstance().GetCounter()));
	LabelLicense->setToolTip(LOCALIZATION("ClickToViewHelp"));
	LabelLicense->setStyleSheet("color: red");
	connect(LabelLicense, &ISQLabel::Clicked, this, &ISLicenseMessageWidget::LabelClicked);
	LayoutLabel->addWidget(LabelLicense, 0, Qt::AlignHCenter);

	ISPushButton *ButtonHide = new ISPushButton(this);
	ButtonHide->setText(LOCALIZATION("Hide"));
	LayoutLabel->addWidget(ButtonHide);
	connect(ButtonHide, &ISPushButton::clicked, this, &QWidget::hide);

	LayoutLabel->addStretch();

	Layout->addWidget(ISControls::CreateHorizontalLine(this));
}
//-----------------------------------------------------------------------------
ISLicenseMessageWidget::~ISLicenseMessageWidget()
{

}
//-----------------------------------------------------------------------------
void ISLicenseMessageWidget::LabelClicked()
{
	ISMessageBox::ShowInformation(this, LOCALIZATION("License.ActivateInformation"));
}
//-----------------------------------------------------------------------------
