#pragma once
//-----------------------------------------------------------------------------
#include "ISSqlModelCore.h"
//-----------------------------------------------------------------------------
class ISTasksSqlModel : public ISSqlModelCore
{
	Q_OBJECT

public:
	Q_INVOKABLE ISTasksSqlModel(PMetaClassTable *meta_table, QObject *parent = 0);
	virtual ~ISTasksSqlModel();

	QVariant data(const QModelIndex &ModelIndex, int Role = Qt::DisplayRole) const;

private:
	QMap<QString, QObject*> Objects;
};
//-----------------------------------------------------------------------------
