#include "StdAfx.h"
#include "ISCalculatorButton.h"
//-----------------------------------------------------------------------------
ISCalculatorButton::ISCalculatorButton(QWidget *parent) : QToolButton(parent)
{
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
}
//-----------------------------------------------------------------------------
QSize ISCalculatorButton::sizeHint() const
{
    QSize Size = QToolButton::sizeHint();
    Size.rheight() += 20;
    Size.rwidth() = qMax(Size.width(), Size.height());
    return Size;
}
//-----------------------------------------------------------------------------
