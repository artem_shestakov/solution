#pragma once
//-----------------------------------------------------------------------------
#include <QWidget>
#include <QPushButton>
#include <QLabel>
//-----------------------------------------------------------------------------
class ISParagraphButton : public QWidget
{
	Q_OBJECT

public:
	ISParagraphButton(const QIcon &Icon, const QString &ToolTip, QWidget *parent = 0);
	virtual ~ISParagraphButton();

	QPushButton* GetButton();
	void SetVisibleLine(bool Visible);

private:
	QPushButton *PushButton;

	QWidget *LineLeft;
	QLabel *LineCenter;
	QWidget *LineRight;
};
//-----------------------------------------------------------------------------
