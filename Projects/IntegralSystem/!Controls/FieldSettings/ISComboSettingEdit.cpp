#include "StdAfx.h"
#include "ISComboSettingEdit.h"
//-----------------------------------------------------------------------------
ISComboSettingEdit::ISComboSettingEdit(QObject *MetaField, QWidget *parent) : ISComboEdit(MetaField, parent)
{
	SetEditable(false);
	SetVisibleClear(false);
}
//-----------------------------------------------------------------------------
ISComboSettingEdit::ISComboSettingEdit(QWidget *parent) : ISComboSettingEdit(nullptr, parent)
{

}
//-----------------------------------------------------------------------------
ISComboSettingEdit::~ISComboSettingEdit()
{

}
//-----------------------------------------------------------------------------
