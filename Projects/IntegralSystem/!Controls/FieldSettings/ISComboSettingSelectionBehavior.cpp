#include "StdAfx.h"
#include "ISComboSettingSelectionBehavior.h"
#include "ISLocalization.h"
//-----------------------------------------------------------------------------
ISComboSettingSelectionBehavior::ISComboSettingSelectionBehavior(QObject *MetaField, QWidget *parent) : ISComboSettingEdit(MetaField, parent)
{
	AddItem(LOCALIZATION("Settings.Table.SelectionBehavior.SelectItems"), "SelectItems");
	AddItem(LOCALIZATION("Settings.Table.SelectionBehavior.SelectRows"), "SelectRows");
	AddItem(LOCALIZATION("Settings.Table.SelectionBehavior.SelectColumns"), "SelectColumns");
}
//-----------------------------------------------------------------------------
ISComboSettingSelectionBehavior::ISComboSettingSelectionBehavior(QWidget *parent) : ISComboSettingSelectionBehavior(nullptr, parent)
{

}
//-----------------------------------------------------------------------------
ISComboSettingSelectionBehavior::~ISComboSettingSelectionBehavior()
{

}
//-----------------------------------------------------------------------------
