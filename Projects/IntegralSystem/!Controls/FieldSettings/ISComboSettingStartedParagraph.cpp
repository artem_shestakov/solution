#include "StdAfx.h"
#include "ISComboSettingStartedParagraph.h"
#include "ISQuery.h"
//-----------------------------------------------------------------------------
static QString QS_PARAFGRAPH = PREPARE_QUERY("SELECT prhs_localname, prhs_name "
											 "FROM _paragraphs "
											 "WHERE NOT prhs_isdeleted "
											 "ORDER BY prhs_orderid");
//-----------------------------------------------------------------------------
ISComboSettingStartedParagraph::ISComboSettingStartedParagraph(QObject *MetaField, QWidget *parent) : ISComboSettingEdit(MetaField, parent)
{
	ISQuery qSelect(QS_PARAFGRAPH);
	if (qSelect.Execute())
	{
		while (qSelect.Next())
		{
			AddItem(qSelect.ReadColumn("prhs_localname").toString(), qSelect.ReadColumn("prhs_name"));
		}
	}
}
//-----------------------------------------------------------------------------
ISComboSettingStartedParagraph::ISComboSettingStartedParagraph(QWidget *parent) : ISComboSettingStartedParagraph(nullptr, parent)
{

}
//-----------------------------------------------------------------------------
ISComboSettingStartedParagraph::~ISComboSettingStartedParagraph()
{

}
//-----------------------------------------------------------------------------
