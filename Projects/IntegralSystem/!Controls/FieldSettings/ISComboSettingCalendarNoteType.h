#pragma once
//-----------------------------------------------------------------------------
#include "ISComboSettingEdit.h"
//-----------------------------------------------------------------------------
class ISComboSettingCalendarNoteType : public ISComboSettingEdit
{
	Q_OBJECT

public:
	Q_INVOKABLE ISComboSettingCalendarNoteType(QObject *MetaField, QWidget *parent);
	Q_INVOKABLE ISComboSettingCalendarNoteType(QWidget *parent);
	virtual ~ISComboSettingCalendarNoteType();
};
//-----------------------------------------------------------------------------
