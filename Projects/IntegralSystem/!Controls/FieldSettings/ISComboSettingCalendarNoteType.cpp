#include "StdAfx.h"
#include "ISComboSettingCalendarNoteType.h"
#include "ISLocalization.h"
//-----------------------------------------------------------------------------
ISComboSettingCalendarNoteType::ISComboSettingCalendarNoteType(QObject *MetaField, QWidget *parent) : ISComboSettingEdit(MetaField, parent)
{
	AddItem(LOCALIZATION("Settings.CalendarEvents.NoteView.ToolTip"), "ToolTip");
	AddItem(LOCALIZATION("Settings.CalendarEvents.NoteView.List"), "List");
}
//-----------------------------------------------------------------------------
ISComboSettingCalendarNoteType::ISComboSettingCalendarNoteType(QWidget *parent) : ISComboSettingCalendarNoteType(nullptr, parent)
{

}
//-----------------------------------------------------------------------------
ISComboSettingCalendarNoteType::~ISComboSettingCalendarNoteType()
{

}
//-----------------------------------------------------------------------------
