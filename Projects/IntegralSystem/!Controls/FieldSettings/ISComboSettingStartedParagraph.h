#pragma once
//-----------------------------------------------------------------------------
#include "ISComboSettingEdit.h"
//-----------------------------------------------------------------------------
class ISComboSettingStartedParagraph : public ISComboSettingEdit
{
	Q_OBJECT

public:
	Q_INVOKABLE ISComboSettingStartedParagraph(QObject *MetaField, QWidget *parent);
	Q_INVOKABLE ISComboSettingStartedParagraph(QWidget *parent);
	virtual ~ISComboSettingStartedParagraph();
};
//-----------------------------------------------------------------------------
