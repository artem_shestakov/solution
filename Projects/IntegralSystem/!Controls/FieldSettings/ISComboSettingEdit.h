#pragma once
//-----------------------------------------------------------------------------
#include "ISComboEdit.h"
//-----------------------------------------------------------------------------
class ISComboSettingEdit : public ISComboEdit
{
	Q_OBJECT

public:
	Q_INVOKABLE ISComboSettingEdit(QObject *MetaField, QWidget *parent);
	Q_INVOKABLE ISComboSettingEdit(QWidget *parent);
	virtual ~ISComboSettingEdit();
};
//-----------------------------------------------------------------------------
