#pragma once
//-----------------------------------------------------------------------------
#include "ISComboSettingEdit.h"
//-----------------------------------------------------------------------------
class ISComboSettingSelectionBehavior : public ISComboSettingEdit
{
	Q_OBJECT

public:
	Q_INVOKABLE ISComboSettingSelectionBehavior(QObject *MetaField, QWidget *parent);
	Q_INVOKABLE ISComboSettingSelectionBehavior(QWidget *parent);
	virtual ~ISComboSettingSelectionBehavior();
};
//-----------------------------------------------------------------------------
