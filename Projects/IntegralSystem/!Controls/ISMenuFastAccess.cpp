#include "StdAfx.h"
#include "ISMenuFastAccess.h"
#include "EXDefines.h"
#include "ISLocalization.h"
#include "ISMessageBox.h"
#include "ISMetaData.h"
#include "ISCore.h"
#include "ISSystem.h"
//-----------------------------------------------------------------------------
ISMenuFastAccess::ISMenuFastAccess(QWidget *parent) : QMenu(parent)
{
	ActionGroupRecords = new QActionGroup(this);
	connect(ActionGroupRecords, &QActionGroup::triggered, this, &ISMenuFastAccess::CreateRecord);

	ActionGroupTools = new QActionGroup(this);
	connect(ActionGroupTools, &QActionGroup::triggered, this, &ISMenuFastAccess::StartExternalTool);

	QFont FontLabel = FONT_APPLICATION;
	FontLabel.setUnderline(true);

	QLabel *LabelCreateRecords = new QLabel(LOCALIZATION("CreateRecords") + ":", this);
	LabelCreateRecords->setFont(FontLabel);
	LabelCreateRecords->setAlignment(Qt::AlignVCenter | Qt::AlignLeft);

	QWidgetAction *WidgetActionCreateRecords = new QWidgetAction(this);
	WidgetActionCreateRecords->setDefaultWidget(LabelCreateRecords);
	addAction(WidgetActionCreateRecords);

	ActionCreateRecords = addAction(LOCALIZATION("Setting"), this, &ISMenuFastAccess::CreateRecords);

	QLabel *LabelExternalTools = new QLabel(LOCALIZATION("ExternalTools") + ":", this);
	LabelExternalTools->setFont(FontLabel);
	LabelExternalTools->setAlignment(Qt::AlignVCenter | Qt::AlignLeft);

	QWidgetAction *WidgetActionExternalTools = new QWidgetAction(this);
	WidgetActionExternalTools->setDefaultWidget(LabelExternalTools);
	addAction(WidgetActionExternalTools);

	ActionExternalTools = addAction(LOCALIZATION("Setting"), this, &ISMenuFastAccess::ExternalTools);

	connect(this, &ISMenuFastAccess::aboutToShow, this, &ISMenuFastAccess::AboutToShow);
}
//-----------------------------------------------------------------------------
ISMenuFastAccess::~ISMenuFastAccess()
{

}
//-----------------------------------------------------------------------------
void ISMenuFastAccess::AboutToShow()
{
	while (ActionGroupRecords->actions().count())
	{
		QAction *Action = ActionGroupRecords->actions().takeFirst();
		removeAction(Action);

		delete Action;
		Action = nullptr;
	}


	//Заполнение быстрого создания записей
	QVector<QString> VectorRecords = ISFastAccessEntity::GetInstance().GetCreateRecords();
	for (int i = 0; i < VectorRecords.count(); i++)
	{
		PMetaClassTable *MetaTable = ISMetaData::GetInstanse().GetMetaTable(VectorRecords.at(i));

		QAction *ActionRecord = new QAction(LOCALIZATION("CreateRecord") + ": " + MetaTable->GetLocalName(), this);
		ActionRecord->setFont(FONT_TAHOMA_10);
		ActionRecord->setData(MetaTable->GetName());
		insertAction(ActionCreateRecords, ActionRecord);
		ActionGroupRecords->addAction(ActionRecord);
	}

	while (ActionGroupTools->actions().count())
	{
		QAction *Action = ActionGroupTools->actions().takeFirst();
		removeAction(Action);

		delete Action;
		Action = nullptr;
	}

	//Заполнение внешних инструментов
	QVector<ISMetaExternalTool*> VectorTools = ISFastAccessEntity::GetInstance().GetExternalTools();
	for (int i = 0; i < VectorTools.count(); i++)
	{
		ISMetaExternalTool *MetaExternalTool = VectorTools.at(i);

		QAction *ActionExternalTool = new QAction(MetaExternalTool->GetIcon(), MetaExternalTool->GetLocalName(), this);
		ActionExternalTool->setFont(FONT_TAHOMA_10);
		ActionExternalTool->setData(MetaExternalTool->GetCommand());
		insertAction(ActionExternalTools, ActionExternalTool);
		ActionGroupTools->addAction(ActionExternalTool);
	}
}
//-----------------------------------------------------------------------------
void ISMenuFastAccess::CreateRecord(QAction *ActionTriggered)
{
	ISObjectFormBase *ObjectFormBase = ISCore::CreateObjectForm(ISNamespace::OFT_New, ISMetaData::GetInstanse().GetMetaTable(ActionTriggered->data().toString()));
	ObjectFormBase->show();
}
//-----------------------------------------------------------------------------
void ISMenuFastAccess::StartExternalTool(QAction *ActionTriggered)
{
	QString Command = ActionTriggered->data().toString();

	if (!QFile::exists(Command))
	{
		ISMessageBox::ShowWarning(nullptr, LOCALIZATION("Message.Error.NotFoundFile").arg(Command));
		return;
	}

	if (!ISSystem::OpenFile(Command))
	{
		ISMessageBox::ShowWarning(nullptr, LOCALIZATION("Message.Warning.NotStartedExternalTool").arg(ActionTriggered->text()));
	}
}
//-----------------------------------------------------------------------------
