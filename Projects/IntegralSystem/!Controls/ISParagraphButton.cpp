#include "StdAfx.h"
#include "ISParagraphButton.h"
#include "EXDefines.h"
#include "ISStyleSheet.h"
#include "ISBuffer.h"
#include "ISControls.h"
//-----------------------------------------------------------------------------
ISParagraphButton::ISParagraphButton(const QIcon &Icon, const QString &ToolTip, QWidget *parent) : QWidget(parent)
{
	setMaximumWidth(120);
	setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);

	QVBoxLayout *Layout = new QVBoxLayout();
	Layout->setSpacing(0);
	Layout->setContentsMargins(LAYOUT_MARGINS_NULL);
	setLayout(Layout);

	PushButton = new QPushButton(this);
	PushButton->setToolTip(ToolTip);
	PushButton->setIcon(Icon);
	PushButton->setIconSize(SIZE_32_32);
	PushButton->setCursor(CURSOR_POINTING_HAND);
	PushButton->setStyleSheet(STYLE_SHEET("ISMenuBarButton"));
	PushButton->setFont(FONT_APPLICATION_BOLD);
	Layout->addWidget(PushButton, 0, Qt::AlignHCenter);

	QHBoxLayout *LayoutLine = new QHBoxLayout();
	Layout->addLayout(LayoutLine, 0);

	LineLeft = new QWidget(this);
	LineLeft->setAutoFillBackground(true);
	LineLeft->setFixedHeight(5);
	LineLeft->setMaximumWidth(55);
	LineLeft->setSizePolicy(QSizePolicy::Minimum, LineLeft->sizePolicy().verticalPolicy());
	LayoutLine->addWidget(LineLeft, 0, Qt::AlignBottom);
	ISControls::SetBackgroundColorWidget(LineLeft, COLOR_WHITE);

	LineCenter = new QLabel(this);
	LineCenter->setPixmap(BUFFER_PIXMAPS("MainMenuButton.Bottom"));
	LineCenter->setFixedSize(LineCenter->pixmap()->size());
	LayoutLine->addWidget(LineCenter);

	LineRight = new QWidget(this);
	LineRight->setAutoFillBackground(true);
	LineRight->setFixedHeight(5);
	LineRight->setMaximumWidth(55);
	LineRight->setSizePolicy(QSizePolicy::Minimum, LineRight->sizePolicy().verticalPolicy());
	LayoutLine->addWidget(LineRight, 0, Qt::AlignBottom);
	ISControls::SetBackgroundColorWidget(LineRight, COLOR_WHITE);

	SetVisibleLine(false);
}
//-----------------------------------------------------------------------------
ISParagraphButton::~ISParagraphButton()
{

}
//-----------------------------------------------------------------------------
QPushButton* ISParagraphButton::GetButton()
{
	return PushButton;
}
//-----------------------------------------------------------------------------
void ISParagraphButton::SetVisibleLine(bool Visible)
{
	LineLeft->setVisible(Visible);
	LineCenter->setVisible(Visible);
	LineRight->setVisible(Visible);
}
//-----------------------------------------------------------------------------
