#pragma once
//-----------------------------------------------------------------------------
#include <QWidget>
//-----------------------------------------------------------------------------
class ISLicenseMessageWidget : public QWidget
{
	Q_OBJECT

public:
	ISLicenseMessageWidget(QWidget *parent = 0);
	virtual ~ISLicenseMessageWidget();

protected:
	void LabelClicked();
};
//-----------------------------------------------------------------------------
