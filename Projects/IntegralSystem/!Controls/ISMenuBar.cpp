#include "StdAfx.h"
#include "ISMenuBar.h"
#include "EXConstants.h"
#include "ISBuffer.h"
#include "ISLocalization.h"
#include "ISUserRoleEntity.h"
#include "EXDefines.h"
#include "ISStyleSheet.h"
#include "ISMetaUser.h"
#include "ISSettings.h"
#include "ISFastAccessEntity.h"
#include "ISMenuFastAccess.h"
//-----------------------------------------------------------------------------
ISMenuBar::ISMenuBar(QWidget *parent) : QWidget(parent)
{
	CurrentButton = nullptr;
	IconNotifyNew = BUFFER_ICONS("MainPanel.Notifications.New");
	IconNotifyNull = BUFFER_ICONS("MainPanel.Notifications.Null");
	IsNull = true;

	setAutoFillBackground(true);

	QPalette Palette(palette());
	Palette.setColor(QPalette::Background, COLOR_MAIN_MENU_BAR);
	setPalette(Palette);

	MainLayout = new QHBoxLayout();
	MainLayout->setContentsMargins(LAYOUT_MARGINS_NULL);
	setLayout(MainLayout);

	ButtonGroup = new QButtonGroup(this);
	connect(ButtonGroup, static_cast<void(QButtonGroup::*)(QAbstractButton*)>(&QButtonGroup::buttonClicked), this, static_cast<void(ISMenuBar::*)(QAbstractButton*)>(&ISMenuBar::ButtonParagraphClicked));

	ButtonMenu = new QToolButton(this);
	ButtonMenu->setText(LOCALIZATION("MainMenu"));
	ButtonMenu->setFont(FONT_TAHOMA_10);
	ButtonMenu->setAutoRaise(true);
	ButtonMenu->setCheckable(true);
	ButtonMenu->setIcon(BUFFER_ICONS("MainPanel.Menu"));
	ButtonMenu->setIconSize(SIZE_25_25);
	ButtonMenu->setCursor(CURSOR_POINTING_HAND);
	ButtonMenu->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
	ButtonMenu->setStyleSheet(STYLE_SHEET("QToolButtonMainMenu"));
	connect(ButtonMenu, &QToolButton::clicked, this, &ISMenuBar::ShowMainMenu);
	MainLayout->addWidget(ButtonMenu);

	if (SETTING_BOOL(CONST_UID_SETTING_VIEW_MENUFASTACCESS))
	{
		CreateFastAccessMenu();
	}

	QLabel *Label = new QLabel(this);
	Label->setVisible(false);
	Label->setText("IntegralSystem");
	Label->setStyleSheet("color: white;");
	Label->setFont(QFont("Tahoma", 14, QFont::Bold));
	MainLayout->addWidget(Label);

	MainMenu = new ISMainMenu(ButtonMenu);
	connect(MainMenu, &ISMainMenu::aboutToHide, this, &ISMenuBar::HideMainMenu);

	CreateMenuFile();
	CreateMenuService();
	CreateMenuAddition();
	CreateMenuHelp();

	//�������� ����������� ������� �������� ����
	for (QAction *ActionMenu : MainMenu->actions())
	{
		if (!ActionMenu->menu()->actions().count()) //���� � ����� ���� ��� ��������� - �������� ���� �����
		{
			ActionMenu->setVisible(false);
		}
	}

	//��������� ������ ��� ���� ��������� �������� ����
	for (QAction *ActionMainItem : MainMenu->actions())
	{
		ActionMainItem->setFont(FONT_TAHOMA_10);

		for (QAction *ActionChildItem : ActionMainItem->menu()->actions())
		{
			ActionChildItem->setFont(FONT_TAHOMA_10);
		}
	}

	//�����
	MainMenu->addSeparator();
	MainMenu->addAction(BUFFER_ICONS("Exit"), LOCALIZATION("Exit"), this, &ISMenuBar::Exit, QKeySequence(Qt::AltModifier + Qt::Key_F4));

	LayoutMainButtons = new QHBoxLayout();
	MainLayout->addLayout(LayoutMainButtons);

	MainLayout->addStretch();

	LayoutButtons = new QHBoxLayout();
	MainLayout->addLayout(LayoutButtons);

	CreateButtonsPanel();

	TimerNotify = new QTimer(this);
	TimerNotify->setInterval(1000);
	connect(TimerNotify, &QTimer::timeout, this, &ISMenuBar::NotifyButtonTimeout);
}
//-----------------------------------------------------------------------------
ISMenuBar::~ISMenuBar()
{
	
}
//-----------------------------------------------------------------------------
QPushButton* ISMenuBar::CreateParagraphButton(const ISUuid &ParagraphUID, const QIcon &Icon, const QString &Text)
{
	QVBoxLayout *Layout = new QVBoxLayout();
	Layout->setSpacing(0);
	Layout->setContentsMargins(LAYOUT_MARGINS_NULL);

	ISParagraphButton *ParagraphButton = new ISParagraphButton(Icon, Text, this);
	LayoutMainButtons->addWidget(ParagraphButton);

	ButtonGroup->addButton(ParagraphButton->GetButton());
	ParagraphButtons.insert(ParagraphUID, ParagraphButton);

	return ParagraphButton->GetButton();
}
//-----------------------------------------------------------------------------
void ISMenuBar::ButtonParagraphClicked(QAbstractButton *AbstractButton)
{
	if (CurrentButton == AbstractButton) //���� ���������� ����� �������� ���������
	{
		return;
	}

	for (const auto &Item : ParagraphButtons.toStdMap())
	{
		ISUuid ParagraphUID = Item.first;
		ISParagraphButton *ParagraphButton = Item.second;
		if (ParagraphButton->GetButton() == AbstractButton)
		{
			ParagraphButton->SetVisibleLine(true);
			emit ParagraphClicked(ParagraphUID);
		}
		else
		{
			ParagraphButton->SetVisibleLine(false);
		}
	}

	CurrentButton = AbstractButton;
}
//-----------------------------------------------------------------------------
void ISMenuBar::ButtonParagraphClicked(const ISUuid &ClickedParagraphUID)
{
	for (const auto &Item : ParagraphButtons.toStdMap())
	{
		ISUuid ParagraphUID = Item.first;
		ISParagraphButton *ParagraphButton = Item.second;
		if (ClickedParagraphUID == ParagraphUID)
		{
			ButtonParagraphClicked(ParagraphButton->GetButton());
			break;
		}
	}
}
//-----------------------------------------------------------------------------
void ISMenuBar::ButtonNotifyStartBlink()
{
	ButtonNotify->setIcon(IconNotifyNew);
	TimerNotify->start();
}
//-----------------------------------------------------------------------------
void ISMenuBar::ButtonNotifyStopBlink()
{
	QIcon Icon = BUFFER_ICONS("MainPanel.Notifications.Null");
	Icon.addPixmap(BUFFER_ICONS("MainPanel.Notifications.Null.Active").pixmap(SIZE_25_25), QIcon::Active);

	ButtonNotify->setIcon(Icon);
	TimerNotify->stop();
}
//-----------------------------------------------------------------------------
void ISMenuBar::CreateFastAccessMenu()
{
	QToolButton *ButtonFastMenu = new QToolButton(this);
	ButtonFastMenu->setText(LOCALIZATION("FastAccess"));
	ButtonFastMenu->setFont(FONT_TAHOMA_10);
	ButtonFastMenu->setAutoRaise(true);
	ButtonFastMenu->setIcon(BUFFER_ICONS("MainPanel.FastAccess"));
	ButtonFastMenu->setIconSize(SIZE_25_25);
	ButtonFastMenu->setCursor(CURSOR_POINTING_HAND);
	ButtonFastMenu->setPopupMode(QToolButton::InstantPopup);
	ButtonFastMenu->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
	ButtonFastMenu->setStyleSheet(STYLE_SHEET("QToolButtonMainMenu"));
	MainLayout->addWidget(ButtonFastMenu);

	ISMenuFastAccess *MenuFastAccess = new ISMenuFastAccess(ButtonFastMenu);
	connect(MenuFastAccess, &ISMenuFastAccess::CreateRecords, this, &ISMenuBar::CreateRecords);
	connect(MenuFastAccess, &ISMenuFastAccess::ExternalTools, this, &ISMenuBar::ExternalTools);
	ButtonFastMenu->setMenu(MenuFastAccess);
}
//-----------------------------------------------------------------------------
void ISMenuBar::CreateMenuFile()
{
	
}
//-----------------------------------------------------------------------------
void ISMenuBar::CreateMenuService()
{
	QMenu *MenuService = MainMenu->addMenu(BUFFER_ICONS("MainMenuItem"), LOCALIZATION("Service"));

	//���������
	MenuService->addAction(BUFFER_ICONS("Favorites"), LOCALIZATION("Favorites"), this, &ISMenuBar::Favorites);

	//�������
	MenuService->addAction(BUFFER_ICONS("History"), LOCALIZATION("History"), this, &ISMenuBar::History);
	MenuService->addSeparator();

	//����� ������
	if (ISUserRoleEntity::GetInstance().CheckPermissionSpecial(CONST_UID_PERMISSION_SPECIAL_CHANGE_PASSWORD))
	{
		MenuService->addAction(LOCALIZATION("ChangePassword") + "...", this, &ISMenuBar::ChangePassword);
	}

	//������������ ����
	MenuService->addAction(BUFFER_ICONS("Log"), LOCALIZATION("CreateLogToday"), this, &ISMenuBar::CreateLogToday);
}
//-----------------------------------------------------------------------------
void ISMenuBar::CreateMenuAddition()
{
	QMenu *MenuAddition = MainMenu->addMenu(BUFFER_ICONS("MainMenuItem"), LOCALIZATION("Additionally"));

	//�������
	MenuAddition->addAction(BUFFER_ICONS("Note"), LOCALIZATION("Notebook"), this, &ISMenuBar::Notebook);

	MenuAddition->addAction(BUFFER_ICONS("Calculator"), LOCALIZATION("Calculator"), this, &ISMenuBar::Calculator);
}
//-----------------------------------------------------------------------------
void ISMenuBar::CreateMenuHelp()
{
	QMenu *MenuHelp = MainMenu->addMenu(BUFFER_ICONS("MainMenuItem"), LOCALIZATION("Help"));

	//���������� �������
	//Menu->addAction(BUFFER_ICONS("Reference"), LOCALIZATION("HelpSystem"), this, &ISMenuBar::HelpSystem);
	//Menu->addSeparator();

	//� ���������
	MenuHelp->addAction(BUFFER_ICONS("About"), LOCALIZATION("AboutForm.AboutApplication"), this, &ISMenuBar::AboutApplication);

	//Qt
	MenuHelp->addAction(BUFFER_ICONS("Qt"), LOCALIZATION("AboutFrameworkDeveloped"), this, &ISMenuBar::AboutQt);
	MenuHelp->addSeparator();

	//��������
	MenuHelp->addAction(LOCALIZATION("License"), this, &ISMenuBar::License);
}
//-----------------------------------------------------------------------------
void ISMenuBar::CreateButtonsPanel()
{
	//�������
	if (SETTING_BOOL(CONST_UID_SETTING_DEBUG_ACCESSTODEBUGVIEW))
	{
		QToolButton *ButtonDebug = CreateButton(LOCALIZATION("Debug"), "MainPanel.Debug");
		connect(ButtonDebug, &QToolButton::clicked, this, &ISMenuBar::Debug);
	}

	//���������
	QToolButton *ButtonSettings = CreateButton(LOCALIZATION("Settings"), "MainPanel.Settings");
	connect(ButtonSettings, &QToolButton::clicked, this, &ISMenuBar::Settings);

	//����� ������������
	QToolButton *ButtonChangeUser = CreateButton(LOCALIZATION("ChangeUser"), "MainPanel.ChangeUser");
	connect(ButtonChangeUser, &QToolButton::clicked, this, &ISMenuBar::ChangeUser);

	//����������
	QToolButton *ButtonLook = CreateButton(LOCALIZATION("Block"), "MainPanel.Lock");
	connect(ButtonLook, &QToolButton::clicked, this, &ISMenuBar::Lock);

	//�����������
	ButtonNotify = CreateButton(LOCALIZATION("Notifications"), "MainPanel.Notifications.Null");
	connect(ButtonNotify, &QToolButton::clicked, this, &ISMenuBar::Notifications);
}
//-----------------------------------------------------------------------------
QToolButton* ISMenuBar::CreateButton(const QString &ToolTip, const QString &IconName)
{
	QIcon Icon = BUFFER_ICONS(IconName);
	Icon.addPixmap(BUFFER_ICONS(IconName + ".Active").pixmap(SIZE_25_25), QIcon::Active);

	QToolButton *ToolButton = new QToolButton(this);
	ToolButton->setToolTip(ToolTip);
	ToolButton->setIcon(Icon);
	ToolButton->setAutoRaise(true);
	ToolButton->setToolButtonStyle(Qt::ToolButtonIconOnly);
	ToolButton->setIconSize(SIZE_25_25);
	ToolButton->setCursor(CURSOR_POINTING_HAND);
	LayoutButtons->addWidget(ToolButton);

	return ToolButton;
}
//-----------------------------------------------------------------------------
void ISMenuBar::ShowMainMenu()
{
	ButtonMenu->setChecked(true);
	MainMenu->Show(ButtonMenu->mapToGlobal(QPoint()));
}
//-----------------------------------------------------------------------------
void ISMenuBar::HideMainMenu()
{
	ButtonMenu->setChecked(false);
}
//-----------------------------------------------------------------------------
void ISMenuBar::NotifyButtonTimeout()
{
	if (IsNull)
	{
		ButtonNotify->setIcon(IconNotifyNew);
	}
	else
	{
		ButtonNotify->setIcon(IconNotifyNull);
	}

	IsNull = !IsNull;
}
//-----------------------------------------------------------------------------
