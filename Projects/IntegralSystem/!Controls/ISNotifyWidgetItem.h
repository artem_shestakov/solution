#pragma once
//-----------------------------------------------------------------------------
#include <QWidget>
//-----------------------------------------------------------------------------
class ISNotifyWidgetItem : public QWidget
{
	Q_OBJECT

signals:
	void Delete(int NotificationUserID, QListWidgetItem *ListWidgetItem);

public:
	ISNotifyWidgetItem(QWidget *parent, int notification_id, const QDateTime &notify_date_time, QListWidgetItem *list_widget_item, const QString &Message);
	virtual ~ISNotifyWidgetItem();

protected:
	void DeleteClicked();

private:
	int NotificationID;
	QListWidgetItem *ListWidgetItem;
};
//-----------------------------------------------------------------------------
