#include "StdAfx.h"
#include "ISChatMessageWidget.h"
#include "EXDefines.h"
#include "ISBuffer.h"
#include "ISQLabel.h"
#include "ISQuery.h"
#include "ISImageViewForm.h"
#include "ISSystem.h"
#include "ISLocalization.h"
#include "ISFileDialog.h"
#include "ISMessageBox.h"
#include "ISPushButton.h"
//-----------------------------------------------------------------------------
static QString QS_MESSAGE_IMAGE = PREPARE_QUERY("SELECT chat_image FROM _chatmessages WHERE chat_id = :MessageID");
//-----------------------------------------------------------------------------
static QString QS_MESSAGE_FILE = PREPARE_QUERY("SELECT chat_file, chat_filename FROM _chatmessages WHERE chat_id = :MessageID");
//-----------------------------------------------------------------------------
ISChatMessageWidget::ISChatMessageWidget(int message_id, const QString &Message, const QByteArray &ByteImage, const QString &FileName, const QString &UserFullName, const QDateTime &DateTime, QWidget *parent) : QWidget(parent)
{
	MessageID = message_id;

	QVBoxLayout *MainLayout = new QVBoxLayout();
	setLayout(MainLayout);

	QHBoxLayout *LayoutTop = new QHBoxLayout();
	MainLayout->addLayout(LayoutTop);

	QLabel *LabelIcon = new QLabel(this);
	LabelIcon->setPixmap(BUFFER_ICONS("Chat.Message").pixmap(SIZE_16_16));
	LayoutTop->addWidget(LabelIcon);

	QLabel *LabelUser = new QLabel(UserFullName + ":", this);
	LabelUser->setFont(FONT_APPLICATION_BOLD);
	LayoutTop->addWidget(LabelUser);

	LayoutTop->addStretch();

	QLabel *LabelDateTime = new QLabel(DateTime.toString(DATE_TIME_FORMAT_V3), this);
	LayoutTop->addWidget(LabelDateTime);
	
	QHBoxLayout *LayoutBottom = new QHBoxLayout();
	MainLayout->addLayout(LayoutBottom);

	LabelMessage = new QLabel(this);
	LabelMessage->setText(Message);
	LabelMessage->setWordWrap(true);
	LayoutBottom->addWidget(LabelMessage);

	if (!ByteImage.isNull()) //���� � ��������� ����������� �����������
	{
		QPixmap Pixmap;
		if (Pixmap.loadFromData(ByteImage))
		{
			QHBoxLayout *Layout = new QHBoxLayout();
			MainLayout->addLayout(Layout);

			ISQLabel *LabelImage = new ISQLabel(this);
			LabelImage->setPixmap(Pixmap.scaled(SIZE_200_200, Qt::KeepAspectRatio));
			LabelImage->setCursor(CURSOR_POINTING_HAND);
			connect(LabelImage, &ISQLabel::DoubleClicked, this, &ISChatMessageWidget::DoubleClickedImage);
			Layout->addWidget(LabelImage);

			ISPushButton *ButtonSave = new ISPushButton(this);
			ButtonSave->setText(LOCALIZATION("Save"));
			ButtonSave->setToolTip(LOCALIZATION("Save"));
			ButtonSave->setIcon(BUFFER_ICONS("Save"));
			connect(ButtonSave, &ISPushButton::clicked, this, &ISChatMessageWidget::SaveImage);
			Layout->addWidget(ButtonSave);

			Layout->addStretch();
		}
	}

	if (FileName.length()) //���� � ��������� ���������� ����
	{
		ISQLabel *Label = new ISQLabel(LOCALIZATION("File") + ": " + FileName, this);
		Label->setToolTip(LOCALIZATION("ClickFromSaveFile"));
		Label->setProperty("FileName", FileName);
		Label->setCursor(CURSOR_POINTING_HAND);
		Label->setSizePolicy(QSizePolicy::Maximum, Label->sizePolicy().verticalPolicy());
		connect(Label, &ISQLabel::Clicked, this, &ISChatMessageWidget::SaveFile);
		MainLayout->addWidget(Label);

		QFont Font = Label->font();
		Font.setUnderline(true);
		Label->setFont(Font);
	}
}
//-----------------------------------------------------------------------------
ISChatMessageWidget::~ISChatMessageWidget()
{

}
//-----------------------------------------------------------------------------
QString ISChatMessageWidget::GetMessage() const
{
	return LabelMessage->text();
}
//-----------------------------------------------------------------------------
void ISChatMessageWidget::DoubleClickedImage()
{
	ISQLabel *Label = dynamic_cast<ISQLabel*>(sender());
	if (Label)
	{
		ISSystem::SetWaitGlobalCursor(true);

		ISQuery qSelectImage(QS_MESSAGE_IMAGE);
		qSelectImage.BindValue(":MessageID", MessageID);
		if (qSelectImage.ExecuteFirst())
		{
			QPixmap Pixmap;
			if (Pixmap.loadFromData(qSelectImage.ReadColumn("chat_image").toByteArray()))
			{
				ISImageViewForm *ImageViewForm = new ISImageViewForm(Pixmap);
				ImageViewForm->show();
			}
		}

		ISSystem::SetWaitGlobalCursor(false);
	}
}
//-----------------------------------------------------------------------------
void ISChatMessageWidget::SaveImage()
{
	QString ImagePath = ISFileDialog::GetSaveFileNameImage(this);
	if (ImagePath.length())
	{
		ISSystem::SetWaitGlobalCursor(true);

		ISQuery qSelectImage(QS_MESSAGE_IMAGE);
		qSelectImage.BindValue(":MessageID", MessageID);
		if (qSelectImage.ExecuteFirst())
		{
			QPixmap Pixmap;
			if (Pixmap.loadFromData(qSelectImage.ReadColumn("chat_image").toByteArray()))
			{
				if (Pixmap.save(ImagePath))
				{
					ISSystem::SetWaitGlobalCursor(false);
					ISMessageBox::ShowInformation(nullptr, LOCALIZATION("Message.Information.FileSaved"));
				}
			}
		}

		ISSystem::SetWaitGlobalCursor(false);
	}
}
//-----------------------------------------------------------------------------
void ISChatMessageWidget::SaveFile()
{
	ISQLabel *Label = dynamic_cast<ISQLabel*>(sender());
	if (Label)
	{
		QString FilePath = ISFileDialog::GetSaveFileName(this, QString(), Label->property("FileName").toString());
		if (FilePath.length())
		{
			ISSystem::SetWaitGlobalCursor(true);

			ISQuery qSelectFile(QS_MESSAGE_FILE);
			qSelectFile.BindValue(":MessageID", MessageID);
			if (qSelectFile.ExecuteFirst())
			{
				QByteArray ByteFile = qSelectFile.ReadColumn("chat_file").toByteArray();

				QFile File(FilePath);
				if (File.open(QIODevice::WriteOnly))
				{
					File.write(ByteFile);
					File.close();

					ISSystem::SetWaitGlobalCursor(false);
					if (ISMessageBox::ShowQuestion(nullptr, LOCALIZATION("Message.Question.FileSaved")))
					{
						ISSystem::OpenFile(FilePath);
					}
				}
			}

			ISSystem::SetWaitGlobalCursor(false);
		}
	}
}
//-----------------------------------------------------------------------------
