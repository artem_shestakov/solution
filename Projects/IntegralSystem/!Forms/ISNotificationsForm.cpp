#include "StdAfx.h"
#include "ISNotificationsForm.h"
#include "EXDefines.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISControls.h"
#include "ISNotifyWidgetItem.h"
#include "ISMessageBox.h"
#include "ISQuery.h"
#include "ISDatabase.h"
#include "ISNotificationService.h"
#include "ISNotify.h"
//-----------------------------------------------------------------------------
static QString QS_NOTIFICATION = PREPARE_QUERY("SELECT ntfu_id, ntfu_creationdate, ntfu_notification "
											   "FROM _notificationuser "
											   "WHERE ntfu_userto = ongetcurrentuserid() "
											   "ORDER BY ntfu_id DESC")
//-----------------------------------------------------------------------------
static QString QD_NOTIFICATION = PREPARE_QUERY("DELETE FROM _notificationuser WHERE ntfu_id = :NotificationUserID");
//-----------------------------------------------------------------------------
static QString QD_NOTIFICATION_CLEAR = PREPARE_QUERY("DELETE FROM _notificationuser WHERE ntfu_userto = ongetcurrentuserid()");
//-----------------------------------------------------------------------------
static QString QS_NOTIFICATION_COUNT = PREPARE_QUERY("SELECT COUNT(*) FROM _notificationuser WHERE ntfu_userto = ongetcurrentuserid()");
//-----------------------------------------------------------------------------
ISNotificationsForm::ISNotificationsForm(QWidget *parent) : ISInterfaceForm(parent)
{
	setWindowFlags(Qt::Popup);
	setAttribute(Qt::WA_DeleteOnClose, false);
	
	connect(this, &ISNotificationsForm::NotifyCountSignal, this, &ISNotificationsForm::NotifyCount);
	connect(this, &ISNotificationsForm::NotifyAdd, this, &ISNotificationsForm::NewNotification);
	connect(&ISNotify::GetInstance(), static_cast<void (ISNotify::*)(const QVariantMap &)>(&ISNotify::Notify), this, &ISNotificationsForm::NewNotification2);

	QVBoxLayout *LayoutFrame = new QVBoxLayout();
	LayoutFrame->setContentsMargins(LAYOUT_MARGINS_5_PX);

	QFrame *Frame = new QFrame(this);
	Frame->setFrameShape(QFrame::Box);
	Frame->setFrameShadow(QFrame::Raised);
	Frame->setLayout(LayoutFrame);
	GetMainLayout()->addWidget(Frame);

	LayoutFrame->addWidget(new QLabel(LOCALIZATION("Notifications") + ":", this));

	ListWidget = new ISListWidget(this);
	LayoutFrame->addWidget(ListWidget);

	LayoutFrame->addWidget(ISControls::CreateHorizontalLine(this));

	ButtonClear = new ISPushButton(this);
	ButtonClear->setEnabled(false);
	ButtonClear->setText(LOCALIZATION("Clear"));
	connect(ButtonClear, &ISPushButton::clicked, this, &ISNotificationsForm::Clear);
	LayoutFrame->addWidget(ButtonClear, 0, Qt::AlignRight);

	LabelEmpty = new QLabel(this);
	LabelEmpty->setVisible(false);
	LabelEmpty->setText(LOCALIZATION("NotificationEmpty"));
	LabelEmpty->setFont(FONT_APPLICATION_BOLD);
	LabelEmpty->adjustSize();

	FutureWatcher = new QFutureWatcher<void>(this);
}
//-----------------------------------------------------------------------------
ISNotificationsForm::~ISNotificationsForm()
{

}
//-----------------------------------------------------------------------------
void ISNotificationsForm::StartCheckExist()
{
	QFuture<void> Future = QtConcurrent::run(this, &ISNotificationsForm::CheckExistNotifications);
	FutureWatcher->setFuture(Future);
}
//-----------------------------------------------------------------------------
void ISNotificationsForm::paintEvent(QPaintEvent *e)
{
	QRect Rect = ListWidget->frameGeometry();
	QPoint CenterPoint = Rect.center();
	CenterPoint.setX(CenterPoint.x() - (LabelEmpty->width() / 2));
	CenterPoint.setY(CenterPoint.y() - (LabelEmpty->height() / 2));
	LabelEmpty->move(CenterPoint);
}
//-----------------------------------------------------------------------------
void ISNotificationsForm::CheckExistNotifications()
{
	{
		QSqlDatabase DB = QSqlDatabase::cloneDatabase(ISDatabase::GetInstance().GetDefaultDB(), "CheckNotifyUser");
		if (DB.open())
		{
			{
				ISQuery qSelect(DB, QS_NOTIFICATION);
				if (qSelect.Execute())
				{
					int Count = qSelect.GetCountResultRows();
					if (Count)
					{
						while (qSelect.Next())
						{
							int NotificationUserID = qSelect.ReadColumn("ntfu_id").toInt();
							QDateTime DateTime = qSelect.ReadColumn("ntfu_creationdate").toDateTime();
							ISUuid NotificationUserUID = qSelect.ReadColumn("ntfu_notification");

							emit NotifyAdd(NotificationUserID, DateTime, ISNotify::GetInstance().GetMetaNotify(NotificationUserUID));
						}

						emit NotifyCountSignal(Count);
					}
				}
			}

			DB.close();
		}
	}

	QSqlDatabase::removeDatabase("CheckNotifyUser");
}
//-----------------------------------------------------------------------------
void ISNotificationsForm::NotifyCount(int Count)
{
	emit NotifyNew();
	QApplication::beep();
	ISNotificationService::ShowNotification(LOCALIZATION("NewNotificationsCurrentUser").arg(Count));
}
//-----------------------------------------------------------------------------
void ISNotificationsForm::NewNotification(int NotificationUserID, const QDateTime &NotifyDateTime, ISMetaNotify *MetaNotify)
{
	QListWidgetItem *ListWidgetItem = new QListWidgetItem();
	ListWidget->insertItem(0, ListWidgetItem);

	ISNotifyWidgetItem *NotifyWidgetItem = new ISNotifyWidgetItem(ListWidget, NotificationUserID, NotifyDateTime, ListWidgetItem, MetaNotify->GetMessage());
	NotifyWidgetItem->adjustSize();	
	connect(NotifyWidgetItem, &ISNotifyWidgetItem::Delete, this, &ISNotificationsForm::DeleteItem);
	ListWidget->setItemWidget(ListWidgetItem, NotifyWidgetItem);
	ListWidgetItem->setSizeHint(NotifyWidgetItem->sizeHint());

	CountChanged();
}
//-----------------------------------------------------------------------------
void ISNotificationsForm::NewNotification2(const QVariantMap &VariantMap)
{
	int ObjectID = 0;
	
	if (VariantMap.value("Saved").toBool())
	{
		ObjectID = VariantMap.value("ID").toInt();
	}

	NewNotification(ObjectID, VariantMap.value("DateTime").toDateTime(), ISNotify::GetInstance().GetMetaNotify(VariantMap.value("NotificationUID").toString()));
}
//-----------------------------------------------------------------------------
void ISNotificationsForm::DeleteItem(int NotificationUserID, QListWidgetItem *ListWidgetItem)
{
	if (NotificationUserID)
	{
		ISQuery qDeleteNotificationUser(QD_NOTIFICATION);
		qDeleteNotificationUser.BindValue(":NotificationUserID", NotificationUserID);
		if (qDeleteNotificationUser.Execute())
		{
			ListWidget->RemoveItem(ListWidgetItem);
			CountChanged();
		}
	}
	else
	{
		ListWidget->RemoveItem(ListWidgetItem);
		CountChanged();
	}
}
//-----------------------------------------------------------------------------
void ISNotificationsForm::Clear()
{
	if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.ClearListNotifications")))
	{
		ISQuery qDeleteNotifications(QD_NOTIFICATION_CLEAR);
		if (qDeleteNotifications.Execute())
		{
			ListWidget->Clear();
			CountChanged();
			hide();
		}
	}
}
//-----------------------------------------------------------------------------
void ISNotificationsForm::CountChanged()
{
	if (ListWidget->count())
	{
		emit NotifyNew();
		ButtonClear->setEnabled(true);
		LabelEmpty->setVisible(false);
	}
	else
	{
		emit NotifyEmpty();
		ButtonClear->setEnabled(false);
		LabelEmpty->setVisible(true);
	}
}
//-----------------------------------------------------------------------------
