#pragma once
//-----------------------------------------------------------------------------
#include "ISInterfaceDialogForm.h"
//-----------------------------------------------------------------------------
class ISEMailInfoForm : public ISInterfaceDialogForm
{
	Q_OBJECT

public:
	ISEMailInfoForm(int mail_id, QWidget *parent = 0);
	virtual ~ISEMailInfoForm();

protected:
	void AddRow(const QString &LabelText, const QVariant &Value);

private:
	QFormLayout *FormLayout;
};
//-----------------------------------------------------------------------------
