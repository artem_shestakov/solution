#include "StdAfx.h"
#include "ISTasksBaseListForm.h"
#include "EXDefines.h"
#include "EXConstants.h"
#include "ISMetaData.h"
#include "ISLocalization.h"
#include "ISQuery.h"
#include "ISMessageBox.h"
#include "ISTasks.h"
//-----------------------------------------------------------------------------
static QString QS_TASK_STATUS = PREPARE_QUERY("SELECT tsst_uid, tsst_name FROM _taskstatus WHERE NOT tsst_isdeleted ORDER BY tsst_name");
//-----------------------------------------------------------------------------
static QString QU_TASK_STATUS = PREPARE_QUERY("UPDATE _tasks SET task_status = :TaskStatus WHERE task_id = :TaskID");
//-----------------------------------------------------------------------------
ISTasksBaseListForm::ISTasksBaseListForm(QWidget *parent) : ISListBaseForm(ISMetaData::GetInstanse().GetMetaTable("_Tasks"), parent)
{
	GetAction(ISNamespace::AT_Print)->setVisible(false);
	CreateComboStatus();
}
//-----------------------------------------------------------------------------
ISTasksBaseListForm::~ISTasksBaseListForm()
{

}
//-----------------------------------------------------------------------------
void ISTasksBaseListForm::CreateCopy()
{
	QString StatusCurrentTask = ISUuid(GetCurrentRecordValueDB("Status"));
	if (StatusCurrentTask == CONST_UID_TASK_STATUS_CONSIDERATION)
	{
		ISListBaseForm::CreateCopy();
	}
	else
	{
		ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.CreateCopyTask"));
	}
}
//-----------------------------------------------------------------------------
void ISTasksBaseListForm::Edit()
{
	QString StatusCurrentTask = ISUuid(GetCurrentRecordValueDB("Status"));
	if (StatusCurrentTask == CONST_UID_TASK_STATUS_CONSIDERATION)
	{
		ISListBaseForm::Edit();
	}
	else
	{
		ISTasks::ShowTaskForm(GetObjectID());
	}
}
//-----------------------------------------------------------------------------
void ISTasksBaseListForm::CreateComboStatus()
{
	GetToolBar()->addWidget(new QLabel(LOCALIZATION("Task.Status") + ":", this));

	ComboStatus = new ISComboEdit(this);
	ComboStatus->SetEditable(false);
	ComboStatus->SetVisibleClear(false);
	ComboStatus->SetCursor(CURSOR_POINTING_HAND);
	ComboStatus->SetToolTip(LOCALIZATION("Task.Status.ToolTip"));
	ComboStatus->AddItem(LOCALIZATION("Task.Status.Any"), QString());

	ISQuery qSelectStatus(QS_TASK_STATUS);
	if (qSelectStatus.Execute())
	{
		while (qSelectStatus.Next())
		{
			ComboStatus->AddItem(qSelectStatus.ReadColumn("tsst_name").toString(), qSelectStatus.ReadColumn("tsst_uid"));
		}
	}

	connect(ComboStatus, &ISComboEdit::ValueChange, this, &ISTasksBaseListForm::SetFilterStatus);
	GetToolBar()->addWidget(ComboStatus);
}
//-----------------------------------------------------------------------------
void ISTasksBaseListForm::AfterShowEvent()
{
	ISInterfaceMetaForm::AfterShowEvent();
}
//-----------------------------------------------------------------------------
void ISTasksBaseListForm::FilterChanged()
{
	QString FilterString;

	if (FilterStatus.length())
	{
		FilterString += QString("task_status = '%1'").arg(FilterStatus);
	}

	if (FilterStatus.length() && FilterUser.length())
	{
		FilterString += " AND ";
	}

	if (FilterUser.length())
	{
		FilterString += QString("task_executor = %1").arg(FilterUser);
	}

	if (FilterString.length())
	{
		GetQueryModel()->SetClassFilter(FilterString);
	}
	else
	{
		GetQueryModel()->ClearClassFilter();
	}
	
	Update();
}
//-----------------------------------------------------------------------------
bool ISTasksBaseListForm::SetTaskStatus(const QString &TaskStatusUID)
{
	ISQuery qTaskStatus(QU_TASK_STATUS);
	qTaskStatus.BindValue(":TaskStatus", TaskStatusUID);
	qTaskStatus.BindValue(":TaskID", GetObjectID());
	return qTaskStatus.Execute();
}
//-----------------------------------------------------------------------------
void ISTasksBaseListForm::SetFilterStatus(const QVariant &filter_status)
{
	FilterStatus = filter_status.toString();
	FilterChanged();
}
//-----------------------------------------------------------------------------
void ISTasksBaseListForm::SetFilterUser(const QVariant &filter_user)
{
	FilterUser = filter_user.toString();
	FilterChanged();
}
//-----------------------------------------------------------------------------
