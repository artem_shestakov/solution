#include "StdAfx.h"
#include "ISConnectionForm.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISMessageBox.h"
#include "EXConstants.h"
#include "ISCore.h"
#include "ISConfig.h"
#include "EXDefines.h"
#include "ISSystem.h"
#include "ISDatabase.h"
#include "ISControls.h"
//-----------------------------------------------------------------------------
ISConnectionForm::ISConnectionForm(QWidget *parent) : ISInterfaceDialogForm(parent)
{
	CheckAutoboot = nullptr;

	setWindowTitle(LOCALIZATION("ConnectionSettings"));

	GetMainLayout()->setContentsMargins(LAYOUT_MARGINS_10_PX);
	ForbidResize();

	QFormLayout *FormLayout = new QFormLayout();
	GetMainLayout()->addLayout(FormLayout);

	EditServer = new ISIPAddressEdit(this);
	EditServer->SetValue(ISConfig::GetInstance().GetValue(CONST_CONFIG_CONNECTION_SERVER));
	FormLayout->addRow(LOCALIZATION("Server") + ":", EditServer);

	EditPort = new ISIntegerEdit(this);
	EditPort->SetValue(ISConfig::GetInstance().GetValue(CONST_CONFIG_CONNECTION_PORT));
	FormLayout->addRow(LOCALIZATION("Port") + ":", EditPort);

	EditDatabase = new ISDatabaseEdit(this);
	EditDatabase->SetValue(ISConfig::GetInstance().GetValue(CONST_CONFIG_CONNECTION_DATABASE));
	EditDatabase->SetPlaceholderText(LOCALIZATION("EnterDatabaseName"));
	EditDatabase->SetServer(EditServer->GetValue());
	EditDatabase->SetPort(EditPort->GetValue());
	FormLayout->addRow(LOCALIZATION("DatabaseName") + ":", EditDatabase);

	connect(EditServer, &ISIPAddressEdit::ValueChange, EditDatabase, &ISDatabaseEdit::SetServer);
	connect(EditPort, &ISIntegerEdit::ValueChange, EditDatabase, &ISDatabaseEdit::SetPort);

	EditModule = new ISPathEditFile(this);
	EditModule->SetValue(ISConfig::GetInstance().GetValue(CONST_CONFIG_OTHER_MODULE));
	EditModule->SetFilterFile(LOCALIZATION("File.Filter.Dll"));
	FormLayout->addRow(LOCALIZATION("Module") + ":", EditModule);

	QFormLayout *FormLayoutInput = new QFormLayout();

	GroupBoxInput = new QGroupBox(LOCALIZATION("AutoInput"), this);
	GroupBoxInput->setCheckable(true);
	GroupBoxInput->setChecked(ISConfig::GetInstance().GetValue(CONST_CONFIG_AUTOINPUT_INCLUDED).toBool());
	GroupBoxInput->setLayout(FormLayoutInput);
	GetMainLayout()->addWidget(GroupBoxInput);

	EditLogin = new ISLineEdit(GroupBoxInput);
	EditLogin->SetValue(ISConfig::GetInstance().GetValue(CONST_CONFIG_AUTOINPUT_LOGIN));
	FormLayoutInput->addRow(LOCALIZATION("Login") + ":", EditLogin);

	EditPassword = new ISPasswordEdit(GroupBoxInput);
	EditPassword->SetValue(ISConfig::GetInstance().GetValue(CONST_CONFIG_AUTOINPUT_PASSWORD));
	FormLayoutInput->addRow(LOCALIZATION("Password") + ":", EditPassword);

#ifdef Q_OS_WIN32
	CheckAutoboot = new ISCheckEdit(this);
	CheckAutoboot->SetText(LOCALIZATION("AutobootStartup"));
	CheckAutoboot->SetToolTip(LOCALIZATION("AutobootStartup.ToolTip"));
	CheckAutoboot->SetValue(ISConfig::GetInstance().GetValue(CONST_CONFIG_OTHER_AUTOBOOT));
	GetMainLayout()->addWidget(CheckAutoboot);
#endif

	QHBoxLayout *LayoutButton = new QHBoxLayout();
	GetMainLayout()->addLayout(LayoutButton);

	ButtonTestConnection = new ISPushButton(this);
	ButtonTestConnection->setText(LOCALIZATION("TestConnection"));
	ButtonTestConnection->setIcon(BUFFER_ICONS("Reconnect"));
	connect(ButtonTestConnection, &ISPushButton::clicked, this, &ISConnectionForm::TestConnection);
	LayoutButton->addWidget(ButtonTestConnection);

	LayoutButton->addStretch();

	ButtonSave = new ISPushButton(this);
	ButtonSave->setText(LOCALIZATION("Save"));
	ButtonSave->setIcon(BUFFER_ICONS("Save"));
	connect(ButtonSave, &ISPushButton::clicked, this, &ISConnectionForm::SaveSettings);
	LayoutButton->addWidget(ButtonSave);
}
//-----------------------------------------------------------------------------
ISConnectionForm::~ISConnectionForm()
{

}
//-----------------------------------------------------------------------------
void ISConnectionForm::AfterShowEvent()
{
	EditServer->SetFocus();
}
//-----------------------------------------------------------------------------
void ISConnectionForm::EnterClicked()
{
	SaveSettings();
}
//-----------------------------------------------------------------------------
void ISConnectionForm::SaveSettings()
{
	if (!CheckFields())
	{
		return;
	}

	ISConfig::GetInstance().SetValue(CONST_CONFIG_CONNECTION_SERVER, EditServer->GetValue());
	ISConfig::GetInstance().SetValue(CONST_CONFIG_CONNECTION_PORT, EditPort->GetValue());
	ISConfig::GetInstance().SetValue(CONST_CONFIG_CONNECTION_DATABASE, EditDatabase->GetValue());
	ISConfig::GetInstance().SetValue(CONST_CONFIG_OTHER_MODULE, EditModule->GetValue());
	ISConfig::GetInstance().SetValue(CONST_CONFIG_OTHER_AUTOBOOT, CheckAutoboot->GetValue());
	ISConfig::GetInstance().SetValue(CONST_CONFIG_AUTOINPUT_INCLUDED, GroupBoxInput->isChecked());
	ISConfig::GetInstance().SetValue(CONST_CONFIG_AUTOINPUT_LOGIN, EditLogin->GetValue());
	ISConfig::GetInstance().SetValue(CONST_CONFIG_AUTOINPUT_PASSWORD, EditPassword->GetValue());

	if (CheckAutoboot)
	{
		QSettings settings("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", QSettings::NativeFormat);

		if (CheckAutoboot->GetValue().toBool())
		{
			settings.setValue(APPLICATION_NAME, QDir::toNativeSeparators(QCoreApplication::applicationFilePath()));
			settings.sync();
		}
		else
		{
			settings.remove(APPLICATION_NAME);
		}
	}

	close();
}
//-----------------------------------------------------------------------------
void ISConnectionForm::TestConnection()
{
	ISSystem::SetWaitGlobalCursor(true);
	
	ButtonTestConnection->setText(LOCALIZATION("Checking") + "...");
	ISSystem::RepaintWidget(ButtonTestConnection);

	QString ErrorConnection;
	QString Login;
	QString Password;

	if (GroupBoxInput->isChecked())
	{
		Login = EditLogin->GetValue().toString();
		Password = EditPassword->GetValue().toString();
	}
	else
	{
		Login = SYSTEM_USER_LOGIN;
		Password = SYSTEM_USER_PASSWORD;
	}

	if (ISDatabase::GetInstance().ConnectToTestDB(EditServer->GetValue().toString(), EditPort->GetValue().toInt(), Login, Password, EditDatabase->GetValue().toString(), ErrorConnection))
	{
		ISSystem::SetWaitGlobalCursor(false);
		ButtonTestConnection->setText(LOCALIZATION("TestConnection"));

		ISMessageBox::ShowInformation(this, LOCALIZATION("Message.Information.Connection.Created.Complete"));
		ISDatabase::GetInstance().DisconnectFromTestDB();
	}
	else
	{
		ISSystem::SetWaitGlobalCursor(false);
		ButtonTestConnection->setText(LOCALIZATION("TestConnection"));
		ISMessageBox::ShowCritical(this, LOCALIZATION("Message.Error.Connection.NotCreated").arg(EditDatabase->GetValue().toString()).arg(EditServer->GetValue().toString()), ErrorConnection);
	}
}
//-----------------------------------------------------------------------------
bool ISConnectionForm::CheckFields()
{
	if (EditServer->GetValue().toString().isEmpty())
	{
		ISMessageBox::ShowCritical(this, LOCALIZATION("Message.Error.Field.NullValue").arg(LOCALIZATION("Server")));
		return false;
	}
	else if (EditPort->GetValue().toString().isEmpty())
	{
		ISMessageBox::ShowCritical(this, LOCALIZATION("Message.Error.Field.NullValue").arg(LOCALIZATION("Port")));
		return false;
	}
	else if (EditDatabase->GetValue().toString().isEmpty())
	{
		ISMessageBox::ShowCritical(this, LOCALIZATION("Message.Error.Field.NullValue").arg(LOCALIZATION("DatabaseName")));
		return false;
	}

	return true;
}
//-----------------------------------------------------------------------------
