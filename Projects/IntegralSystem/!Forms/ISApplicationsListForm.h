#pragma once
//-----------------------------------------------------------------------------
#include "ISListBaseForm.h"
//-----------------------------------------------------------------------------
class ISApplicationsListForm : public ISListBaseForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISApplicationsListForm(QWidget *parent = 0);
	virtual ~ISApplicationsListForm();
};
//-----------------------------------------------------------------------------
