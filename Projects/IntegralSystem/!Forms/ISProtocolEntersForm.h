#pragma once
//-----------------------------------------------------------------------------
#include "ISListBaseForm.h"
//-----------------------------------------------------------------------------
class ISProtocolEntersForm : public ISListBaseForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISProtocolEntersForm(QWidget *parent = 0);
	virtual ~ISProtocolEntersForm();

protected:
	void UpdatedProtocol();
	void Update() override;
	void LoadDataAfterEvent() override;
	void SelectedRowEvent(const QItemSelection &ItemSelected, const QItemSelection &ItemDeSelected) override;

private:
	ISListBaseForm *ProtocolListForm;
};
//-----------------------------------------------------------------------------
