#include "StdAfx.h"
#include "ISChatForm.h"
#include "EXDefines.h"
#include "ISQuery.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISMetaUser.h"
#include "ISChatMessageWidget.h"
#include "ISSystem.h"
#include "ISFileDialog.h"
#include "ISMessageBox.h"
#include "EXConstants.h"
#include "ISNotify.h"
#include "ISParagraphEntity.h"
//-----------------------------------------------------------------------------
static QString QS_CHAT_MESSAGES = PREPARE_QUERY("SELECT chat_id "
												"FROM _chatmessages "
												"WHERE NOT chat_isdeleted "
												"AND chat_id > ((SELECT max(chat_id) FROM _chatmessages) - :Limit) "
												"ORDER BY chat_id");
//-----------------------------------------------------------------------------
static QString QS_CHAT_MESSAGE = PREPARE_QUERY("SELECT chat_creationdate, concat(usrs_surname, ' ', usrs_name, ' ', usrs_patronymic), chat_message, chat_image, chat_file, chat_filename "
											   "FROM _chatmessages "
											   "LEFT JOIN _users ON chat_user = usrs_id "
											   "WHERE chat_id = :MessageID");
//-----------------------------------------------------------------------------
static QString QS_COUNT_MESSAGES = PREPARE_QUERY("SELECT COUNT(*) FROM _chatmessages");
//-----------------------------------------------------------------------------
static QString QI_CHAT_MESSAGE = PREPARE_QUERY("INSERT INTO _chatmessages(chat_message, chat_image, chat_file, chat_filename) "
											   "VALUES(:Message, :Image, :File, :FileName) "
											   "RETURNING chat_id");
//-----------------------------------------------------------------------------
ISChatForm::ISChatForm(QWidget *parent) : ISParagraphBaseForm(parent)
{
	CurrentAttach = ISNamespace::ACT_NoAttach;
	CountMessage = 0;
	IconChat = BUFFER_ICONS("MainPanel.Chat");
	IconNewMessage = BUFFER_ICONS("MainPanel.Chat.NewMessage");
	connect(&ISNotify::GetInstance(), &ISNotify::NewChatMessage, this, &ISChatForm::NotifyMessage);

	QHBoxLayout *MainLayout = new QHBoxLayout();
	setLayout(MainLayout);

	QVBoxLayout *LayoutCenter = new QVBoxLayout();
	MainLayout->addLayout(LayoutCenter);

	QHBoxLayout *LayoutTitle = new QHBoxLayout();
	LayoutCenter->addLayout(LayoutTitle);

	LayoutTitle->addWidget(new QLabel(LOCALIZATION("ShowMessages") + ":", this));

	QRadioButton *ButtonShowAll = new QRadioButton(LOCALIZATION("AllMessages"), this);
	LayoutTitle->addWidget(ButtonShowAll);

	QRadioButton *ButtonLast10 = new QRadioButton(LOCALIZATION("Last10Messages"), this);
	LayoutTitle->addWidget(ButtonLast10);

	QRadioButton *ButtonLast20 = new QRadioButton(LOCALIZATION("Last20Messages"), this);
	LayoutTitle->addWidget(ButtonLast20);

	QRadioButton *ButtonLast100 = new QRadioButton(LOCALIZATION("Last100Messages"), this);
	LayoutTitle->addWidget(ButtonLast100);

	LayoutTitle->addStretch();

	ButtonGroup = new QButtonGroup(this);
	ButtonGroup->addButton(ButtonShowAll, 0);
	ButtonGroup->addButton(ButtonLast10, 10);
	ButtonGroup->addButton(ButtonLast20, 20);
	ButtonGroup->addButton(ButtonLast100, 100);
	connect(ButtonGroup, static_cast<void(QButtonGroup::*)(int)>(&QButtonGroup::buttonClicked), this, &ISChatForm::ReloadMessages);

	QVBoxLayout *LayouGroupBox = new QVBoxLayout();
	LayouGroupBox->setContentsMargins(QMargins(0, 1, 0, 1));

	QGroupBox *GroupBox = new QGroupBox(LOCALIZATION("SharedChat"), this);
	GroupBox->setLayout(LayouGroupBox);
	LayoutCenter->addWidget(GroupBox);

	ListWidget = new ISListWidget(this);
	ListWidget->setFrameShape(QFrame::NoFrame);
	ListWidget->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
	ListWidget->setAlternatingRowColors(true);
	ListWidget->setContextMenuPolicy(Qt::ActionsContextMenu);
	GroupBox->layout()->addWidget(ListWidget);

	ActionCopyText = new QAction(ListWidget);
	ActionCopyText->setText(LOCALIZATION("CopyText"));
	connect(ActionCopyText, &QAction::triggered, this, &ISChatForm::CopyText);
	ListWidget->addAction(ActionCopyText);

	QHBoxLayout *LayoutBar = new QHBoxLayout();
	LayoutCenter->addLayout(LayoutBar);
	
	LabelLoading = new QLabel(this);
	LabelLoading->setText(LOCALIZATION("LoadingMesaages") + "...");
	LabelLoading->setVisible(false);
	LayoutBar->addWidget(LabelLoading);
	
	LayoutBar->addStretch();

	LabelAttachPath = new QLabel(this);
	LayoutBar->addWidget(LabelAttachPath);

	ISPushButton *ButtonAttach = new ISPushButton(this);
	ButtonAttach->setText(LOCALIZATION("Attach"));
	ButtonAttach->setMenu(new QMenu(ButtonAttach));
	LayoutBar->addWidget(ButtonAttach);

	ActionAttachImage = new QAction(ButtonAttach->menu());
	ActionAttachImage->setText(LOCALIZATION("Image"));
	connect(ActionAttachImage, &QAction::triggered, this, &ISChatForm::AttachImage);
	ButtonAttach->menu()->addAction(ActionAttachImage);

	ActionAttachFile = new QAction(ButtonAttach->menu());
	ActionAttachFile->setText(LOCALIZATION("File"));
	connect(ActionAttachFile, &QAction::triggered, this, &ISChatForm::AttachFile);
	ButtonAttach->menu()->addAction(ActionAttachFile);

	ButtonAttach->menu()->addSeparator();

	ActionClearAttach = new QAction(ButtonAttach->menu());
	ActionClearAttach->setText(LOCALIZATION("ClearAttach"));
	ActionClearAttach->setEnabled(false);
	ButtonAttach->menu()->addAction(ActionClearAttach);
	connect(ActionClearAttach, &QAction::triggered, this, &ISChatForm::ClearAttach);

	ButtonSend = new ISPushButton(this);
	ButtonSend->setText(LOCALIZATION("Send"));
	ButtonSend->setToolTip(LOCALIZATION("SendMessageToChat"));
	ButtonSend->setIcon(BUFFER_ICONS("Chat.SendMessage"));
	ButtonSend->setCursor(CURSOR_POINTING_HAND);
	connect(ButtonSend, &ISPushButton::clicked, this, &ISChatForm::SendMessage);
	LayoutBar->addWidget(ButtonSend);

	TextEdit = new ISTextEdit(this);
	TextEdit->SetPlaceholderText(LOCALIZATION("InputMessage") + "...");
	TextEdit->SetExecuteEnter(false);
	TextEdit->setMaximumHeight(80);
	connect(TextEdit, &ISTextEdit::KeyPressEnter, this, &ISChatForm::SendMessage);
	LayoutCenter->addWidget(TextEdit);

	SoundSend = new QSound(BUFFER_AUDIO("Chat.SendMessage"), this);

	ButtonLast10->click();
}
//-----------------------------------------------------------------------------
ISChatForm::~ISChatForm()
{

}
//-----------------------------------------------------------------------------
void ISChatForm::showEvent(QShowEvent *e)
{
	ISParagraphBaseForm::showEvent(e);
	
	CountMessage = 0;
	GetButtonParagraph()->setText(QString());
	GetButtonParagraph()->setToolTip(ISParagraphEntity::GetInstance().GetParagraph(CONST_UID_PARAGRAPH_CHAT)->GetToolTip());
	GetButtonParagraph()->setIcon(IconChat);
	GetButtonParagraph()->setCursor(CURSOR_POINTING_HAND);

	QTimer::singleShot(50, TextEdit, &ISTextEdit::SetFocus);
}
//-----------------------------------------------------------------------------
void ISChatForm::LoadMessages(int Limit)
{
	LabelLoading->setVisible(true);
	ISSystem::RepaintWidget(LabelLoading);
	ISSystem::SetWaitGlobalCursor(true);

	ISQuery qSelectMessages(QS_CHAT_MESSAGES);

	if (Limit)
	{
		qSelectMessages.BindValue(":Limit", Limit);
	}
	else
	{
		ISQuery qCountMessages(QS_COUNT_MESSAGES);
		qCountMessages.ExecuteFirst();
		qSelectMessages.BindValue(":Limit", qCountMessages.ReadColumn("count"));
	}

	if (qSelectMessages.Execute())
	{
		while (qSelectMessages.Next())
		{
			int MessageID = qSelectMessages.ReadColumn("chat_id").toInt();
			CreateItemWidget(MessageID);
		}
	}

	LabelLoading->setVisible(false);
	ISSystem::RepaintWidget(LabelLoading);
	ISSystem::SetWaitGlobalCursor(false);
}
//-----------------------------------------------------------------------------
void ISChatForm::ReloadMessages(int Limit)
{
	ListWidget->Clear();
	LoadMessages(Limit);
}
//-----------------------------------------------------------------------------
void ISChatForm::SendMessage()
{
	QString Message = TextEdit->GetValue().toString();
	if (Message.length() || CurrentAttach == ISNamespace::ACT_Image || CurrentAttach == ISNamespace::ACT_File)
	{
		ButtonSend->setText(LOCALIZATION("Sending") + "...");
		ISSystem::RepaintWidget(ButtonSend);
		ISSystem::SetWaitGlobalCursor(true);

		QVariant VariantImage = GetImage();
		QVariant VariantFile = GetFile();
		QString FileName = GetFileName();

		ISQuery qInsertMessage(QI_CHAT_MESSAGE);
		qInsertMessage.BindValue(":Message", Message);
		qInsertMessage.BindValue(":Image", VariantImage);
		qInsertMessage.BindValue(":File", VariantFile);
		qInsertMessage.BindValue(":FileName", FileName);
		if (qInsertMessage.ExecuteFirst())
		{
			int MessageID = qInsertMessage.ReadColumn("chat_id").toInt();

			CreateItemWidget(MessageID, Message, VariantImage.toByteArray(), FileName, CURRENT_USER_FULL_NAME, QDateTime::currentDateTime());

			ISNotify::GetInstance().SendNotification(CONST_UID_NOTIFY_NEW_CHAT_MESSAGE, MessageID, false);

			TextEdit->Clear();
			ClearAttach();

			SoundSend->play();
		}

		ButtonSend->setText(LOCALIZATION("Send"));
		ISSystem::SetWaitGlobalCursor(false);
	}
	else
	{
		TextEdit->SetFocus();
	}
}
//-----------------------------------------------------------------------------
void ISChatForm::AttachImage()
{
	QString ImagePath = ISFileDialog::GetOpenFileNameImage(this);
	if (ImagePath.length())
	{
		qint64 Kbytes = ISSystem::GetFileSize(ImagePath) / 1024;
		if (Kbytes > 5120) //5mb
		{
			ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.ChatImageSizeConstraint"));
		}
		else
		{
			LabelAttachPath->setText(ImagePath);
			ActionClearAttach->setEnabled(true);

			CurrentAttach = ISNamespace::ACT_Image;
		}
	}
}
//-----------------------------------------------------------------------------
void ISChatForm::AttachFile()
{
	QString FilePath = ISFileDialog::GetOpenFileName(this);
	if (FilePath.length())
	{
		qint64 Kbytes = ISSystem::GetFileSize(FilePath) / 1024;
		if (Kbytes > 15360) //15mb
		{
			ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.ChatFileSizeConstraint"));
		}
		else
		{
			LabelAttachPath->setText(FilePath);
			ActionClearAttach->setEnabled(true);

			CurrentAttach = ISNamespace::ACT_File;
		}
	}
}
//-----------------------------------------------------------------------------
void ISChatForm::ClearAttach()
{
	LabelAttachPath->clear();
	ActionClearAttach->setEnabled(false);

	CurrentAttach = ISNamespace::ACT_NoAttach;
}
//-----------------------------------------------------------------------------
void ISChatForm::CreateItemWidget(int MessageID)
{
	ISQuery qSelectMessage(QS_CHAT_MESSAGE);
	qSelectMessage.BindValue(":MessageID", MessageID);
	if (qSelectMessage.ExecuteFirst())
	{
		QString Message = qSelectMessage.ReadColumn("chat_message").toString();
		QByteArray ByteArrayImage = qSelectMessage.ReadColumn("chat_image").toByteArray();
		QString FileName = qSelectMessage.ReadColumn("chat_filename").toString();
		QString UserName = qSelectMessage.ReadColumn("concat").toString();
		QDateTime DateTime = qSelectMessage.ReadColumn("chat_creationdate").toDateTime();

		CreateItemWidget(MessageID, Message, ByteArrayImage, FileName, UserName, DateTime);
	}
}
//-----------------------------------------------------------------------------
void ISChatForm::CreateItemWidget(int MessageID, const QString &Message, const QByteArray &ByteImage, const QString &FileName, const QString &UserFullName, const QDateTime &DateTime)
{
	QListWidgetItem *ListWidgetItem = new QListWidgetItem(ListWidget);

	ISChatMessageWidget *ChatMessageWidget = new ISChatMessageWidget(MessageID, Message, ByteImage, FileName, UserFullName, DateTime, ListWidget);
	ListWidget->setItemWidget(ListWidgetItem, ChatMessageWidget);

	ChatMessageWidget->adjustSize();
	ListWidgetItem->setSizeHint(ChatMessageWidget->sizeHint());
	ListWidgetItem->setSelected(true);
	ListWidget->scrollToBottom();
}
//-----------------------------------------------------------------------------
QVariant ISChatForm::GetImage() const
{
	if (CurrentAttach == ISNamespace::ACT_Image)
	{
		QString ImagePath = LabelAttachPath->text();

		QPixmap Pixmap(ImagePath);
		QByteArray ByteArray;
		QBuffer Buffer(&ByteArray);
		Buffer.open(QIODevice::WriteOnly);
		Pixmap.save(&Buffer, QImageReader(ImagePath).format());
		return ByteArray;
	}

	return QVariant();
}
//-----------------------------------------------------------------------------
QVariant ISChatForm::GetFile() const
{
	if (CurrentAttach == ISNamespace::ACT_File)
	{
		QFile File(LabelAttachPath->text());
		if (File.open(QIODevice::ReadOnly))
		{
			QByteArray ByteArray = File.readAll();
			return ByteArray;
		}
	}

	return QVariant();
}
//-----------------------------------------------------------------------------
QString ISChatForm::GetFileName() const
{
	if (CurrentAttach == ISNamespace::ACT_File)
	{
		return ISSystem::GetFileName(LabelAttachPath->text());
	}

	return QString();
}
//-----------------------------------------------------------------------------
void ISChatForm::CopyText()
{
	ISChatMessageWidget *MessageWidget = dynamic_cast<ISChatMessageWidget*>(ListWidget->itemWidget(ListWidget->currentItem()));
	QApplication::clipboard()->setText(MessageWidget->GetMessage());
}
//-----------------------------------------------------------------------------
void ISChatForm::NotifyMessage(const QVariantMap &VariantMap)
{
	CreateItemWidget(VariantMap.value("Payload").toInt());

	if (parent()->parent()->property("CurrentParagraphUID").toString() != CONST_UID_PARAGRAPH_CHAT)
	{
		CountMessage++;
		GetButtonParagraph()->setText(QString("(+%1)").arg(CountMessage));
		GetButtonParagraph()->setToolTip(LOCALIZATION("ThereAreUnreadMessages"));
		GetButtonParagraph()->setIcon(IconNewMessage);
		GetButtonParagraph()->setCursor(CURSOR_WHATS_THIS);
	}
}
//-----------------------------------------------------------------------------
