#include "StdAfx.h"
#include "ISMainWindow.h"
#include "EXDefines.h"
#include "ISTrace.h"
#include "ISSystem.h"
#include "ISCore.h"
#include "ISLocalization.h"
#include "ISProtocol.h"
#include "ISBuffer.h"
#include "ISMessageBox.h"
#include "ISSortingBuffer.h"
#include "ISLicense.h"
#include "ISMetaUser.h"
#include "ISSettingsForm.h"
#include "ISSettings.h"
#include "ISFavoritesForm.h"
#include "ISAboutForm.h"
#include "ISSettingsDatabase.h"
#include "ISControls.h"
#include "ISNoteForm.h"
#include "ISDatabase.h"
#include "ISSplashScreen.h"
#include "ISUserRoleEntity.h"
#include "ISMetaData.h"
#include "ISAssert.h"
#include "ISHistory.h"
#include "EXConstants.h"
#include "ISMetaSystemsEntity.h"
#include "ISLicenseForm.h"
#include "ISLockForm.h"
#include "ISExitForm.h"
#include "ISMemoryObjects.h"
#include "ISColumnSizer.h"
#include "ISHistoryForm.h"
#include "ISSplitter.h"
#include "ISCalculatorForm.h"
#include "ISLicenseMessageWidget.h"
#include "ISListBaseForm.h"
#include "ISFastCreateObjectForm.h"
#include "ISExternalToolsForm.h"
#include "ISInputDialog.h"
#include "ISUserPasswordForm.h"
#include "ISParagraphEntity.h"
#include "ISPlugin.h"
#include "ISNotify.h"
#include "ISFileDialog.h"
#include "ISNotificationService.h"
#include "ISCreatedObjectsEntity.h"
//-----------------------------------------------------------------------------
ISMainWindow::ISMainWindow(QWidget *parent) : ISInterfaceForm(parent)
{
	CloseEvent = true;

	ISMemoryObjects::GetInstance().SetMainWindow(this);

	setWindowIcon(BUFFER_PIXMAPS("Logo"));
	setWindowTitle(QString("IntegralSystem - %1 : %2").arg(LOCALIZATION(ISPlugin::GetInstance().GetLocalName())).arg(ISMetaUser::GetInstance().GetData()->FullName));
	resize(SIZE_MAIN_WINDOW);
	setMinimumSize(SIZE_MAIN_WINDOW_MINIMUM);
	GetMainLayout()->setSpacing(0);

	CreateMenuBar();
	CreateLicenseMessage();
	CreateInformationMessage();
	CreateStackWidget();
	CreateStatusBar();

	LabelShadow = new QLabel(this);
	LabelShadow->setVisible(false);

	CreateNotificationsForm();

	//������ ������� ������ ����������� ��� ����� �����������
	connect(&ISNotify::GetInstance(), static_cast<void (ISNotify::*)(void)>(&ISNotify::Notify), MenuBar, &ISMenuBar::ButtonNotifyStartBlink);

	//���������� ������������
	connect(&ISNotify::GetInstance(), &ISNotify::TermianteUser, this, &ISMainWindow::TerminateMe);

	//�������� ����������
	connect(&ISNotify::GetInstance(), &ISNotify::UpdateAviable, this, &ISMainWindow::UpdateAviable);
}
//-----------------------------------------------------------------------------
ISMainWindow::~ISMainWindow()
{

}
//-----------------------------------------------------------------------------
void ISMainWindow::closeEvent(QCloseEvent *e)
{
	if (CloseEvent)
	{
		if (ISCreatedObjectsEntity::GetInstance().CheckExistForms())
		{
			if (SETTING_BOOL(CONST_UID_SETTING_GENERAL_CONFIRMEXITAPPLICATION))
			{
				SetVisibleShadow(true);
				raise();
				activateWindow();

				ISExitForm ExitForm;
				if (ExitForm.Exec())
				{
					ISNamespace::ExitFormAction SelectedAction = ExitForm.GetSelectedAction();

					switch (SelectedAction)
					{
					case ISNamespace::EF_Action_Exit:
						BeforeClose();
						ISCore::ExitApplication();
						break;

					case ISNamespace::EF_Action_ChangeUser:
						e->ignore();
						BeforeClose();
						ISCore::ChangeUser();
						break;

					case ISNamespace::EF_Action_Lock:
						e->ignore();
						LockApplication();
						break;
					}
				}
				else
				{
					e->ignore();
				}

				SetVisibleShadow(false);
			}
			else
			{
				BeforeClose();
				ISCore::ExitApplication();
			}
		}
		else
		{
			e->ignore();
		}
	}
}
//-----------------------------------------------------------------------------
QString ISMainWindow::GetCurrentParagraphUID() const
{
	return CurrentParagraphUID;
}
//-----------------------------------------------------------------------------
void ISMainWindow::SetCloseEventFlag(bool close_event)
{
	CloseEvent = close_event;
}
//-----------------------------------------------------------------------------
bool ISMainWindow::GetCloseEventFlag() const
{
	return CloseEvent;
}
//-----------------------------------------------------------------------------
void ISMainWindow::LockApplication()
{
	HideAnimation();

	ISLockForm *LockForm = new ISLockForm();
	connect(LockForm, &ISLockForm::Unlocked, [=]
	{
		show();
		ShowAnimated();
		raise();
		activateWindow();
		setFocus();

		emit Unlocked();
	});
	LockForm->ExecAnimated();
}
//-----------------------------------------------------------------------------
void ISMainWindow::AfterShowEvent()
{
	ISInterfaceForm::AfterShowEvent();

	if (SETTING_BOOL(CONST_UID_SETTING_VIEW_FULLSCREEN))
	{
		setWindowState(Qt::WindowFullScreen);
	}
}
//-----------------------------------------------------------------------------
void ISMainWindow::CreateMenuBar()
{
	MenuBar = new ISMenuBar(this);
	connect(MenuBar, &ISMenuBar::ParagraphClicked, this, &ISMainWindow::ParagraphClicked);
	connect(MenuBar, &ISMenuBar::CreateRecords, this, &ISMainWindow::ShowCreateRecords);
	connect(MenuBar, &ISMenuBar::ExternalTools, this, &ISMainWindow::ShowExternalTools);
	connect(MenuBar, &ISMenuBar::Lock, this, &ISMainWindow::LockApplication);
	connect(MenuBar, &ISMenuBar::ChangeUser, this, &ISMainWindow::ChangeUser);
	connect(MenuBar, &ISMenuBar::Exit, this, &ISMainWindow::close);
	connect(MenuBar, &ISMenuBar::Favorites, this, &ISMainWindow::ShowFavoritesForm);
	connect(MenuBar, &ISMenuBar::History, this, &ISMainWindow::ShowHistoryForm);
	connect(MenuBar, &ISMenuBar::ChangePassword, this, &ISMainWindow::ShowChangePasswordForm);
	connect(MenuBar, &ISMenuBar::CreateLogToday, this, &ISMainWindow::CreateLogToday);
	connect(MenuBar, &ISMenuBar::Notifications, this, &ISMainWindow::ShowNotifications);
	connect(MenuBar, &ISMenuBar::Debug, this, &ISMainWindow::ShowDebugApplication);
	connect(MenuBar, &ISMenuBar::Settings, this, &ISMainWindow::ShowSettingsForm);
	connect(MenuBar, &ISMenuBar::Notebook, this, &ISMainWindow::ShowNoteForm);
	connect(MenuBar, &ISMenuBar::Calculator, this, &ISMainWindow::ShowCalculator);
	connect(MenuBar, &ISMenuBar::AboutApplication, this, &ISMainWindow::ShowAboutForm);
	connect(MenuBar, &ISMenuBar::AboutQt, this, &ISMainWindow::ShowAboutQt);
	connect(MenuBar, &ISMenuBar::License, this, &ISMainWindow::ShowLicenseForm);
	GetMainLayout()->addWidget(MenuBar);
}
//-----------------------------------------------------------------------------
void ISMainWindow::CreateLicenseMessage()
{
	if (!ISLicense::GetInstance().GetActivated())
	{
		ISLicenseMessageWidget *LicenseMessageWidget = new ISLicenseMessageWidget(this);
		GetMainLayout()->addWidget(LicenseMessageWidget);
	}
}
//-----------------------------------------------------------------------------
void ISMainWindow::CreateInformationMessage()
{
	QString InformationMessage = ISSettingsDatabase::GetInstance().InformationMessage;
	if (InformationMessage.length())
	{
		QLabel *Label = new QLabel(this);
		Label->setFont(FONT_APPLICATION_BOLD);
		Label->setText(InformationMessage);
		Label->setWordWrap(true);
		Label->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Maximum);
		GetMainLayout()->addWidget(Label, 0, Qt::AlignCenter);

		GetMainLayout()->addWidget(ISControls::CreateHorizontalLine(this));

		QColor ColorMessage = ISSettingsDatabase::GetInstance().InformationMessageColor;

		QPalette Palette = Label->palette();
		Palette.setColor(QPalette::WindowText, ColorMessage);
		Label->setPalette(Palette);
	}
}
//-----------------------------------------------------------------------------
void ISMainWindow::CreateStackWidget()
{
	StackedWidget = new QStackedWidget(this);
	GetMainLayout()->addWidget(StackedWidget);

	for (int i = 0; i < ISParagraphEntity::GetInstance().GetParagraphs().count(); i++)
	{
		ISMetaParagraph *MetaParagraph = ISParagraphEntity::GetInstance().GetParagraphs().at(i);
		ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Banner.Initialize.OpeningMainWindow.CreateParagparh").arg(MetaParagraph->GetLocalName()));
		IS_TRACE_MSG(QString("Creating paragraph: %1").arg(MetaParagraph->GetName()));

		ISUuid ParagraphUID = MetaParagraph->GetUID();

		QPushButton *PushButton = MenuBar->CreateParagraphButton(ParagraphUID, BUFFER_ICONS(MetaParagraph->GetIcon()), MetaParagraph->GetToolTip());

		int ObjectType = QMetaType::type((MetaParagraph->GetClassName() + "*").toLocal8Bit().constData());
		IS_ASSERT(ObjectType, QString("Invalid object type from paragraph: %1").arg(MetaParagraph->GetName()));

		const QMetaObject *MetaObject = QMetaType::metaObjectForType(ObjectType);
		IS_ASSERT(MetaObject, QString("Invalid meta object from paragraph: %1").arg(MetaParagraph->GetName()));

		ISParagraphBaseForm *ParagraphBaseForm = dynamic_cast<ISParagraphBaseForm*>(MetaObject->newInstance(Q_ARG(QWidget *, this)));
		IS_ASSERT(ParagraphBaseForm, QString("Invalid class object from paragraph: %1").arg(MetaParagraph->GetName()));
		ParagraphBaseForm->SetButtonParagraph(PushButton);
		int ParagraphIndex = StackedWidget->addWidget(ParagraphBaseForm);
		
		Paragraphs.insert(ParagraphUID, ParagraphIndex);

		if (MetaParagraph->GetDefault())
		{
			if (!CurrentParagraphUID.length())
			{
				MenuBar->ButtonParagraphClicked(PushButton);
				CurrentParagraphUID = ParagraphUID;
			}
		}

		QString StartedParagraph = SETTING_STRING(CONST_UID_SETTING_VIEW_STARTEDPARAGRAPH);
		if (StartedParagraph == MetaParagraph->GetName())
		{
			if (ParagraphUID != CurrentParagraphUID)
			{
				MenuBar->ButtonParagraphClicked(PushButton);
				CurrentParagraphUID = ParagraphUID;
			}
		}
	}
}
//-----------------------------------------------------------------------------
void ISMainWindow::CreateStatusBar()
{
	if (SETTING_BOOL(CONST_UID_SETTING_STATUS_BAR_SHOWSTATUSBAR))
	{
		QFrame *Frame = ISControls::CreateHorizontalLine(this);
		GetMainLayout()->addWidget(Frame);

		QPalette Palette = Frame->palette();
		Palette.setColor(QPalette::Dark, COLOR_MAIN_MENU_BAR);
		Frame->setPalette(Palette);

		StatusBar = new ISStatusBar(this);
		GetMainLayout()->addWidget(StatusBar);
		connect(StatusBar, &ISStatusBar::DateTimeClicked, [=]
		{
			MenuBar->ButtonParagraphClicked(CONST_UID_PARAGRAPH_CALENDAR);
		});
	}
}
//-----------------------------------------------------------------------------
void ISMainWindow::CreateNotificationsForm()
{
	NotificationsForm = new ISNotificationsForm(this);
	connect(NotificationsForm, &ISNotificationsForm::NotifyNew, MenuBar, &ISMenuBar::ButtonNotifyStartBlink);
	connect(NotificationsForm, &ISNotificationsForm::NotifyEmpty, MenuBar, &ISMenuBar::ButtonNotifyStopBlink);
	NotificationsForm->StartCheckExist();
}
//-----------------------------------------------------------------------------
void ISMainWindow::ShowCreateRecords()
{
	ISFastCreateRecordsForm FastCreateRecordsForm;
	FastCreateRecordsForm.Exec();
}
//-----------------------------------------------------------------------------
void ISMainWindow::ShowExternalTools()
{
	ISExternalToolsForm FastAccessSettingsForm;
	FastAccessSettingsForm.Exec();
}
//-----------------------------------------------------------------------------
void ISMainWindow::ParagraphClicked(const ISUuid &ParagraphUID)
{
	ISSystem::SetWaitGlobalCursor(true);

	int ParagraphIndex = Paragraphs.value(ParagraphUID);
	ISParagraphBaseForm *ParagraphWidget = dynamic_cast<ISParagraphBaseForm*>(StackedWidget->widget(ParagraphIndex));
	StackedWidget->setCurrentWidget(ParagraphWidget);
	ParagraphWidget->Invoke();
	
	ParagraphWidget->setFocus();
	CurrentParagraphUID = ParagraphUID;

	ISProtocol::Insert(true, CONST_UID_PROTOCOL_OPEN_PARAGRAPH, QString(), QString(), QVariant(), ISParagraphEntity::GetInstance().GetParagraph(ParagraphUID)->GetLocalName());

	ISSystem::SetWaitGlobalCursor(false);
}
//-----------------------------------------------------------------------------
void ISMainWindow::SetVisibleShadow(bool Visible)
{
	if (Visible)
	{
		QPixmap Pixmap = grab(rect());
		QImage Image(Pixmap.size(), QImage::Format_ARGB32);

		MenuBar->setVisible(false);

		QPainter Painter;
		Painter.begin(&Image);
		Painter.drawPixmap(0, 0, Pixmap);
		Painter.fillRect(Image.rect(), QColor(0, 0, 0, 130));
		Painter.end();

		LabelShadow->setPixmap(QPixmap::fromImage(Image));
		LabelShadow->move(0, 0);
		LabelShadow->resize(width(), height());
		LabelShadow->setVisible(true);
	}
	else
	{
		MenuBar->setVisible(true);

		LabelShadow->clear();
		LabelShadow->resize(SIZE_NULL);
		LabelShadow->setVisible(false);
	}
}
//-----------------------------------------------------------------------------
void ISMainWindow::TerminateMe(const QVariantMap &VariantMap)
{
	activateWindow();
	raise();
	ISSystem::ProcessEvents();
	ISMessageBox::ShowInformation(this, LOCALIZATION("Message.Information.TerminateMe"));
	ISCore::ExitApplication();
}
//-----------------------------------------------------------------------------
void ISMainWindow::UpdateAviable(const QVariantMap &VariantMap)
{
	if (SETTING_BOOL(CONST_UID_SETTING_GENERAL_CHECKUPDATEATSTART))
	{
		if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.NotifyUpdateAviable")))
		{
			if (ISCreatedObjectsEntity::GetInstance().CheckExistForms())
			{
				ISCore::ChangeUser();
			}
		}
	}
}
//-----------------------------------------------------------------------------
void ISMainWindow::BeforeClose()
{
	hide();

	ISSplashScreen::GetInstance().DefaultPixmap();
	ISSplashScreen::GetInstance().show();
	ISSystem::ProcessEvents();

	ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Banner.CloseApplication.FixingExitToProtocol"));
	ISProtocol::ExitApplication();
	ISProtocol::ExitProtocol();

	ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Banner.CloseApplication.SaveUserSortings"));
	ISSortingBuffer::GetInstance().SaveSortings();

	if (SETTING_BOOL(CONST_UID_SETTING_TABLES_REMEMBERCOLUMNSIZE))
	{
		ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Banner.CloseApplication.ColumnSize.Save"));
		ISColumnSizer::GetInstance().Save();
	}
	else
	{
		ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Banner.CloseApplication.ColumnSize.Clear"));
		ISColumnSizer::GetInstance().Clear();
	}

	ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Banner.CloseApplication.DisconnectFromDatabase"));
	ISDatabase::GetInstance().DisconnectFromDefaultDB();

	ISSplashScreen::GetInstance().SetMessage(LOCALIZATION("Banner.CloseApplication.ExitApplication"));
}
//-----------------------------------------------------------------------------
void ISMainWindow::OpenHistoryObject(const QString &TableName, int ObjectID)
{
	PMetaClassTable *MetaTable = ISMetaData::GetInstanse().GetMetaTable(TableName);
	ISObjectFormBase *ObjectForm = ISCore::CreateObjectForm(ISNamespace::ObjectFormType::OFT_Edit, MetaTable, ObjectID);
	if (ObjectForm)
	{
		WorkspaceForm->AddObjectForm(ObjectForm);
	}
}
//-----------------------------------------------------------------------------
void ISMainWindow::EscapeClicked()
{
	close();
}
//-----------------------------------------------------------------------------
void ISMainWindow::LockClicked()
{
	if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.Lock")))
	{
		LockApplication();
	}
}
//-----------------------------------------------------------------------------
void ISMainWindow::ChangeUser()
{
	if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.ChangeUser")))
	{
		BeforeClose();
		ISCore::ChangeUser();
	}
}
//-----------------------------------------------------------------------------
void ISMainWindow::ShowFavoritesForm()
{
	ISSystem::SetWaitGlobalCursor(true);

	ISFavoritesForm *FavoritesForm = new ISFavoritesForm();
	connect(FavoritesForm, &ISFavoritesForm::AddFormFromTab, WorkspaceForm, &ISWorkspaceForm::AddObjectForm);
	FavoritesForm->show();

	ISSystem::SetWaitGlobalCursor(false);
}
//-----------------------------------------------------------------------------
void ISMainWindow::ShowHistoryForm()
{
	ISSystem::SetWaitGlobalCursor(true);

	ISHistoryForm *HistoryForm = new ISHistoryForm();
	connect(HistoryForm, &ISHistoryForm::OpenObject, this, &ISMainWindow::OpenHistoryObject);
	HistoryForm->show();

	ISSystem::SetWaitGlobalCursor(false);
}
//-----------------------------------------------------------------------------
void ISMainWindow::ShowChangePasswordForm()
{
	ISUserPasswordForm UserPasswordForm(CURRENT_USER_ID, ISMetaUser::GetInstance().GetData()->Login, ISMetaUser::GetInstance().GetData()->FullName);
	UserPasswordForm.Exec();
}
//-----------------------------------------------------------------------------
void ISMainWindow::CreateLogToday()
{
	QString LogPath = ISSystem::GetLogFileName();
	QString FileName = ISSystem::GetFileName(LogPath);
	QString FilePath = ISFileDialog::GetSaveFileName(this, LOCALIZATION("File.Filter.Log"), FileName);
	if (FilePath.length())
	{
		QFile FileLog(LogPath);
		if (!FileLog.copy(FilePath))
		{
			ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.NotCreateLogFile"), FileLog.errorString());
		}
	}
}
//-----------------------------------------------------------------------------
void ISMainWindow::ShowNoteForm()
{
	ISSystem::SetWaitGlobalCursor(true);

	ISNoteForm *NoteListForm = new ISNoteForm();
	NoteListForm->show();

	ISSystem::SetWaitGlobalCursor(false);
}
//-----------------------------------------------------------------------------
void ISMainWindow::ShowCalculator()
{
	ISCalculatorForm *CalculatorForm = new ISCalculatorForm();
	CalculatorForm->show();
}
//-----------------------------------------------------------------------------
void ISMainWindow::ShowNotifications()
{
	ISSystem::SetWaitGlobalCursor(true);

	NotificationsForm->adjustSize();

	QPoint Point = MenuBar->mapToGlobal(QPoint());
	Point.setX((Point.x() + MenuBar->width()) - NotificationsForm->width() - 10);
	Point.setY(Point.y() + MenuBar->height() - 5);

	NotificationsForm->move(Point);
	NotificationsForm->show();

	ISSystem::SetWaitGlobalCursor(false);
}
//-----------------------------------------------------------------------------
void ISMainWindow::ShowDebugApplication()
{
	ISSystem::SetWaitGlobalCursor(true);
	ISSystem::OpenFile(DEBUG_VIEW_PATH);
	ISSystem::SetWaitGlobalCursor(false);
}
//-----------------------------------------------------------------------------
void ISMainWindow::ShowSettingsForm()
{
	if (SETTING_BOOL(CONST_UID_SETTING_SECURITY_PASSWORDSETTINGSSHOW))
	{
		QString InputPassword = ISInputDialog::GetPassword(this, LOCALIZATION("Security"), QString()).toString();
		if (InputPassword.length())
		{
			if (InputPassword != SETTING_STRING(CONST_UID_SETTING_SECURITY_PASSWORDSETTINGS))
			{
				ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Password.Error"));
				return;
			}
		}
	}

	ISSystem::SetWaitGlobalCursor(true);
	ISSettingsForm SettingsForm(nullptr);
	ISSystem::SetWaitGlobalCursor(false);
	SettingsForm.Exec();
}
//-----------------------------------------------------------------------------
void ISMainWindow::ShowHelpSystem()
{
	
}
//-----------------------------------------------------------------------------
void ISMainWindow::ShowAboutForm()
{
	ISAboutForm AboutForm;
	AboutForm.Exec();
}
//-----------------------------------------------------------------------------
void ISMainWindow::ShowAboutQt()
{
	QApplication::aboutQt();
}
//-----------------------------------------------------------------------------
void ISMainWindow::ShowLicenseForm()
{
	ISLicenseForm LicenseForm;
	LicenseForm.Exec();
}
//-----------------------------------------------------------------------------
