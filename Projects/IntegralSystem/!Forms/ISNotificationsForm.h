#pragma once
//-----------------------------------------------------------------------------
#include "ISInterfaceForm.h"
#include "ISPushButton.h"
#include "ISListWidget.h"
#include "ISMetaNotify.h"
//-----------------------------------------------------------------------------
class ISNotificationsForm : public ISInterfaceForm
{
	Q_OBJECT

signals:
	void NotifyCountSignal(int Count);
	void NotifyAdd(int NotificationUserID, const QDateTime &NotificationDateTime, ISMetaNotify *MetaNotify);
	void NotifyNew();
	void NotifyEmpty();

public:
	ISNotificationsForm(QWidget *parent = 0);
	virtual ~ISNotificationsForm();

	void StartCheckExist(); //������ �������� ������� �����������

protected:
	void paintEvent(QPaintEvent *e);
	void CheckExistNotifications(); //�������� ������� �����������
	void NotifyCount(int Count); //����������� � ������� �����������
	void NewNotification(int NotificationUserID, const QDateTime &NotifyDateTime, ISMetaNotify *MetaNotify);
	void NewNotification2(const QVariantMap &VariantMap);
	void DeleteItem(int NotificationUserID, QListWidgetItem *ListWidgetItem);
	void Clear();
	void CountChanged();

private:
	ISListWidget *ListWidget;
	ISPushButton *ButtonClear;
	QLabel *LabelEmpty;

	QFutureWatcher<void> *FutureWatcher;
};
//-----------------------------------------------------------------------------
