#include "StdAfx.h"
#include "ISUserGroupAccessObjectForm.h"
#include "EXConstants.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISUuid.h"
//-----------------------------------------------------------------------------
ISUserGroupAccessObjectForm::ISUserGroupAccessObjectForm(ISNamespace::ObjectFormType form_type, PMetaClassTable *meta_table, QWidget *parent, int object_id) : ISObjectFormBase(form_type, meta_table, parent, object_id)
{
	EditTable = dynamic_cast<ISListEdit*>(GetFieldWidget("Table"));
	EditTable->SetSqlFilter("clss_issystem = false");

	connect(GetFieldWidget("Type"), &ISFieldEditBase::ValueChange, this, &ISUserGroupAccessObjectForm::TypeChanged);

	QAction *ActionSelectAll = new QAction(this);
	ActionSelectAll->setText(LOCALIZATION("SelectAllAccess"));
	ActionSelectAll->setToolTip(LOCALIZATION("SelectAllAccess"));
	ActionSelectAll->setIcon(BUFFER_ICONS("CheckAll"));
	connect(ActionSelectAll, &QAction::triggered, [=] {	SetCheckAccess(true); });
	AddAction(ActionSelectAll, false);

	QAction *ActionDeselectAll = new QAction(this);
	ActionDeselectAll->setText(LOCALIZATION("DeselectAllAccess"));
	ActionDeselectAll->setToolTip(LOCALIZATION("DeselectAllAccess"));
	ActionDeselectAll->setIcon(BUFFER_ICONS("CheckNotAll"));
	connect(ActionDeselectAll, &QAction::triggered, [=] { SetCheckAccess(false); });
	AddAction(ActionDeselectAll, false);
}
//-----------------------------------------------------------------------------
ISUserGroupAccessObjectForm::~ISUserGroupAccessObjectForm()
{

}
//-----------------------------------------------------------------------------
void ISUserGroupAccessObjectForm::AfterShowEvent()
{
	ISObjectFormBase::AfterShowEvent();

	if (GetFormType() == ISNamespace::OFT_New)
	{
		SetVisibleField("SubSystem", false);
		SetVisibleField("Table", false);
	}
	else
	{
		TypeChanged(GetFieldValue("Type"));
	}
}
//-----------------------------------------------------------------------------
void ISUserGroupAccessObjectForm::SetCheckAccess(bool Checked)
{
	for (int i = 0; i < GetMetaTable()->GetFields().count(); i++)
	{
		PMetaClassField *MetaField = GetMetaTable()->GetFields().at(i);
		if (MetaField->GetType() == ISNamespace::FT_Bool)
		{
			GetFieldWidget(MetaField->GetName())->SetValue(Checked);
		}
	}
}
//-----------------------------------------------------------------------------
void ISUserGroupAccessObjectForm::TypeChanged(const QVariant &value)
{
	ISUuid TypeUID = value;
	if (TypeUID == CONST_UID_USERGROUPACCESSTYPE_SUBSYSTEMS)
	{
		SetVisibleField("SubSystem", true);
		SetVisibleField("Table", false);
		
		GetFieldWidget("Table")->Clear();
	}
	else if (TypeUID == CONST_UID_USERGROUPACCESSTYPE_TABLES)
	{
		SetVisibleField("SubSystem", false);
		SetVisibleField("Table", true);

		GetFieldWidget("SubSystem")->Clear();
	}

	SetModificationFlag(false);
}
//-----------------------------------------------------------------------------
