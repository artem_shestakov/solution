#pragma once
//-----------------------------------------------------------------------------
#include "ISParagraphBaseForm.h"
//-----------------------------------------------------------------------------
class ISDesktopBaseForm : public ISParagraphBaseForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISDesktopBaseForm(QWidget *parent = 0);
	virtual ~ISDesktopBaseForm();

private:
	QVBoxLayout *MainLayout;
};
//-----------------------------------------------------------------------------
