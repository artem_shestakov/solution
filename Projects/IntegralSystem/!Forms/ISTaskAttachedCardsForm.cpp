#include "StdAfx.h"
#include "ISTaskAttachedCardsForm.h"
#include "EXDefines.h"
#include "ISQuery.h"
#include "ISObjectFormBase.h"
#include "ISCore.h"
#include "ISMetaData.h"
#include "ISSystem.h"
#include "ISControls.h"
#include "ISTasks.h"
//-----------------------------------------------------------------------------
static QString QS_TASK_ATTACHED_CARDS = PREPARE_QUERY("SELECT tatc_tablename, tatc_objectid "
													  "FROM _taskattachedcards "
													  "WHERE tatc_task = :ParentTaskID "
													  "ORDER BY tatc_id");
//-----------------------------------------------------------------------------
ISTaskAttachedCardsForm::ISTaskAttachedCardsForm(QWidget *parent) : ISInterfaceMetaForm(parent)
{
	QToolBar *ToolBar = new QToolBar(this);
	ToolBar->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
	GetMainLayout()->addWidget(ToolBar);

	QAction *ActionDetachCard = ISControls::CreateActionDetachTask(ToolBar);
	connect(ActionDetachCard, &QAction::triggered, this, &ISTaskAttachedCardsForm::Detach);
	ToolBar->addAction(ActionDetachCard);

	ListWidget = new ISListWidget(this);
	ListWidget->setCursor(CURSOR_POINTING_HAND);
	connect(ListWidget, &QListWidget::itemDoubleClicked, this, &ISTaskAttachedCardsForm::ItemDoubleClicked);
	GetMainLayout()->addWidget(ListWidget);
}
//-----------------------------------------------------------------------------
ISTaskAttachedCardsForm::~ISTaskAttachedCardsForm()
{

}
//-----------------------------------------------------------------------------
void ISTaskAttachedCardsForm::LoadData()
{
	ISQuery qSelectAttached(QS_TASK_ATTACHED_CARDS);
	qSelectAttached.BindValue(":ParentTaskID", GetParentObjectID());
	if (qSelectAttached.Execute())
	{
		while (qSelectAttached.Next())
		{
			QString TableName = qSelectAttached.ReadColumn("tatc_tablename").toString();
			int ObjectID = qSelectAttached.ReadColumn("tatc_objectid").toInt();

			PMetaClassTable *MetaTable = ISMetaData::GetInstanse().GetMetaTable(TableName);

			QListWidgetItem *ListWidgetItem = new QListWidgetItem(ListWidget);
			ListWidgetItem->setText(MetaTable->GetLocalName() + ": " + ISCore::GetObjectName(MetaTable, ObjectID));
			ListWidgetItem->setCheckState(Qt::Unchecked);
			ListWidgetItem->setData(Qt::UserRole, TableName);
			ListWidgetItem->setData(Qt::UserRole * 2, ObjectID);
			ListWidgetItem->setSizeHint(QSize(ListWidgetItem->sizeHint().width(), 30));
		}
	}
}
//-----------------------------------------------------------------------------
void ISTaskAttachedCardsForm::Detach()
{
	for (int i = 0; i < ListWidget->count(); i++)
	{
		QListWidgetItem *ListWidgetItem = ListWidget->item(i);
		if (ListWidgetItem->checkState() == Qt::Checked)
		{
			QString TableName = ListWidgetItem->data(Qt::UserRole).toString();
			int ObjectID = ListWidgetItem->data(Qt::UserRole * 2).toInt();
			ISTasks::DetachRecord(GetParentObjectID(), TableName, ObjectID);
		}
	}
}
//-----------------------------------------------------------------------------
void ISTaskAttachedCardsForm::ItemDoubleClicked(QListWidgetItem *ListWidgetItem)
{
	OpenObject(ListWidgetItem->data(Qt::UserRole).toString(), ListWidgetItem->data(Qt::UserRole * 2).toInt());
}
//-----------------------------------------------------------------------------
void ISTaskAttachedCardsForm::OpenObject(const QString &TableName, int ObjectID)
{
	ISObjectFormBase *ObjectFormBase = ISCore::CreateObjectForm(ISNamespace::OFT_Edit, ISMetaData::GetInstanse().GetMetaTable(TableName), ObjectID);
	ObjectFormBase->show();
}
//-----------------------------------------------------------------------------
