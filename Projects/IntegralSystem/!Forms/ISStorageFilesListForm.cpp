#include "StdAfx.h"
#include "ISStorageFilesListForm.h"
#include "EXDefines.h"
#include "ISSystem.h"
#include "ISCore.h"
#include "ISLocalization.h"
#include "ISQuery.h"
#include "ISProgressForm.h"
#include "EXConstants.h"
#include "ISMessageBox.h"
#include "ISMetaUser.h"
#include "ISFileDialog.h"
#include "ISControls.h"
#include "ISAttachFileForm.h"
#include "ISInputDialog.h"
#include "ISMetaData.h"
#include "ISBuffer.h"
//-----------------------------------------------------------------------------
static QString QI_FILE_COPY = PREPARE_QUERY("INSERT INTO _storagefiles(sgfs_owneruser, sgfs_name, sgfs_expansion, sgfs_size) "
											"SELECT sgfs_owneruser, sgfs_name, sgfs_expansion, sgfs_size "
											"FROM _storagefiles "
											"WHERE sgfs_id = :ObjectID "
											"RETURNING sgfs_id");
//-----------------------------------------------------------------------------
static QString QI_FILE_COPY_DATA = PREPARE_QUERY("INSERT INTO _storagefilesdata(sgfd_storagefile, sgfd_data) "
												 "SELECT :StorageFile, sgfd_data "
												 "FROM _storagefilesdata "
												 "WHERE sgfd_storagefile = :StorageFile "
												 "ORDER BY sgfd_id");
//-----------------------------------------------------------------------------
static QString QS_FILE = PREPARE_QUERY("SELECT sgfd_data FROM _storagefilesdata WHERE sgfd_StorageFile = :StorageFile ORDER BY sgfd_id");
//-----------------------------------------------------------------------------
ISStorageFilesListForm::ISStorageFilesListForm(QWidget *parent) : ISListBaseForm(ISMetaData::GetInstanse().GetMetaTable("_StorageFiles"), parent)
{
	GetQueryModel()->SetClassFilter("NOT sgfs_private");

	ActionSetText(ISNamespace::AT_Create, LOCALIZATION("Addition"));

	QAction *ActionOpen = new QAction(GetToolBar());
	ActionOpen->setText(LOCALIZATION("Open"));
	ActionOpen->setToolTip(LOCALIZATION("Open"));
	ActionOpen->setIcon(BUFFER_ICONS("File.Open"));
	connect(ActionOpen, &QAction::triggered, this, &ISStorageFilesListForm::OpenFile);
	InsertAction(ActionOpen, GetAction(ISNamespace::AT_Edit));

	QAction *ActionSave = ISControls::CreateActionSave(GetToolBar());
	connect(ActionSave, &QAction::triggered, this, &ISStorageFilesListForm::SaveFile);
	InsertAction(ActionSave, GetAction(ISNamespace::AT_Edit));

	RadioAll = new QRadioButton(this);
	RadioAll->setText(LOCALIZATION("AllFiles"));
	RadioAll->setChecked(true);
	connect(RadioAll, &QRadioButton::clicked, this, &ISStorageFilesListForm::FilterChanged);
	GetToolBar()->addWidget(RadioAll);

	RadioPrivate = new QRadioButton(this);
	RadioPrivate->setText(LOCALIZATION("MyPrivateFiles"));
	connect(RadioPrivate, &QRadioButton::clicked, this, &ISStorageFilesListForm::FilterChanged);
	GetToolBar()->addWidget(RadioPrivate);
}
//-----------------------------------------------------------------------------
ISStorageFilesListForm::~ISStorageFilesListForm()
{

}
//-----------------------------------------------------------------------------
void ISStorageFilesListForm::Create()
{
	ISAttachFileForm AttachmentFileForm;
	connect(&AttachmentFileForm, &ISAttachFileForm::UpdateList, this, &ISStorageFilesListForm::Update);
	AttachmentFileForm.Exec();
}
//-----------------------------------------------------------------------------
void ISStorageFilesListForm::CreateCopy()
{
	QString FileName = GetCurrentRecordValue("Name").toString();

	if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.CreateCopyFile").arg(FileName)))
	{
		FileName = ISInputDialog::GetString(this, LOCALIZATION("Named"), LOCALIZATION("FileName"), FileName).toString();

		if (FileName.length())
		{
			ISQuery qInsertCopy(QI_FILE_COPY);
			qInsertCopy.BindValue(":ObjectID", GetObjectID());
			if (qInsertCopy.ExecuteFirst())
			{
				ISQuery qInsertData(QI_FILE_COPY_DATA);
				qInsertData.BindValue(":StorageFile", qInsertCopy.ReadColumn("sgfs_id").toInt());
				qInsertData.Execute();

				ISListBaseForm::Update();
			}
		}
		else
		{
			ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.FileNameIsNull"));
		}
	}
}
//-----------------------------------------------------------------------------
void ISStorageFilesListForm::OpenFile()
{
	QFile File(APPLICATION_TEMP_PATH + "/" + QUuid::createUuid().toString() + "." + GetCurrentRecordValue("Expansion").toString());
	qDebug() << File.fileName();
	if (!File.open(QIODevice::WriteOnly))
	{
		ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Error.NotOpenedFile").arg(File.fileName()), File.errorString());
		return;
	}

	ISQuery qSelectFile(QS_FILE);
	qSelectFile.BindValue(":StorageFile", GetObjectID());
	if (qSelectFile.Execute())
	{
		while (qSelectFile.Next())
		{
			QByteArray ByteArray = qSelectFile.ReadColumn("sgfd_data").toByteArray();
			File.write(ByteArray);
		}

		File.close();

		ISSystem::OpenFile(File.fileName());
	}
}
//-----------------------------------------------------------------------------
void ISStorageFilesListForm::SaveFile()
{
	QString FileExpansion = GetCurrentRecordValue("Expansion").toString();
	QString FileName = GetCurrentRecordValue("Name").toString();

	if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.SaveFile").arg(FileName + "." + FileExpansion)))
	{
		QString FilePath = ISFileDialog::GetSaveFileName(this, LOCALIZATION("File.Filter.File").arg(FileExpansion), FileName);
		if (!FilePath.length())
		{
			return;
		}

		QFile File(FilePath);
		if (!File.open(QIODevice::WriteOnly))
		{
			ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Error.NotOpenedFile").arg(FilePath), File.errorString());
			return;
		}

		ISQuery qSelectFile(QS_FILE);
		qSelectFile.BindValue(":StorageFile", GetObjectID());
		if (qSelectFile.Execute())
		{
			while (qSelectFile.Next())
			{
				QByteArray ByteArray = qSelectFile.ReadColumn("sgfd_data").toByteArray();
				File.write(qUncompress(ByteArray));
			}

			File.close();

			if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.File.SavedToPath").arg(FilePath)))
			{
				ISSystem::OpenFile(FilePath);
			}
		}
	}
}
//-----------------------------------------------------------------------------
void ISStorageFilesListForm::FilterChanged()
{
	QString ClassFilter;

	if (RadioAll->isChecked())
	{
		GetQueryModel()->SetClassFilter("NOT sgfs_private");
	}
	else if (RadioPrivate->isChecked())
	{
		GetQueryModel()->SetClassFilter("sgfs_owneruser = ongetcurrentuserid() AND sgfs_private");
	}

	GetQueryModel()->SetClassFilter(ClassFilter);
	Update();
}
//-----------------------------------------------------------------------------
