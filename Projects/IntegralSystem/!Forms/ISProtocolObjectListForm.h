#pragma once
//-----------------------------------------------------------------------------
#include "ISProtocolBaseListForm.h"
//-----------------------------------------------------------------------------
class ISProtocolObjectListForm : public ISProtocolBaseListForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISProtocolObjectListForm(QWidget *parent = 0);
	virtual ~ISProtocolObjectListForm();

protected:
	virtual void LoadDataAfterEvent() override;
	void LoadData() override;
};
//-----------------------------------------------------------------------------
