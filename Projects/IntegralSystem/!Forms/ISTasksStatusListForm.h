#pragma once
//-----------------------------------------------------------------------------
#include "ISListBaseForm.h"
//-----------------------------------------------------------------------------
class ISTasksStatusListForm : public ISListBaseForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISTasksStatusListForm(QWidget *parent = 0);
	virtual ~ISTasksStatusListForm();

protected:
	void SelectColor();
	void ResetColor();
};
//-----------------------------------------------------------------------------
