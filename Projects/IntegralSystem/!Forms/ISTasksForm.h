#pragma once
//-----------------------------------------------------------------------------
#include "ISParagraphBaseForm.h"
#include "ISTasksBaseListForm.h"
#include "ISCalendarWidget.h"
//-----------------------------------------------------------------------------
class ISTasksForm : public ISParagraphBaseForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISTasksForm(QWidget *parent = 0);
	virtual ~ISTasksForm();

protected:
	void GroupBoxToggled(bool On);
	void CalendarDateChanged(const QDate &Date);
	void ActionTriggered(QAction *ActionClicked);
	void CreateTaskObjectForm(QWidget *TasksObjectForm);
	void AddAction(const QString &Text, const QString &ToolTip, const QIcon &Icon, const QString &ListFormName);

private:
	QGroupBox *GroupBox;
	ISCalendarWidget *CalendarWidget;
	QActionGroup *ActionGroup;
	QToolBar *ToolBar;
	QVBoxLayout *CentralLayout;
	ISTasksBaseListForm *TasksListForm;
	QAction *ActionCurrent;
};
//-----------------------------------------------------------------------------
