#include "StdAfx.h"
#include "ISExitForm.h"
#include "EXDefines.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISMemoryObjects.h"
//-----------------------------------------------------------------------------
ISExitForm::ISExitForm(QWidget *parent) : ISInterfaceDialogForm(parent)
{
	SelectedAction = ISNamespace::ExitFormAction::EF_Action_Unknown;

	setWindowTitle(LOCALIZATION("Exit"));
	setWindowFlags(Qt::WindowStaysOnTopHint | Qt::WindowTitleHint | Qt::CustomizeWindowHint);

	GetMainLayout()->setContentsMargins(LAYOUT_MARGINS_10_PX);
	ForbidResize();

	QLabel *Label = new QLabel(this);
	Label->setText(LOCALIZATION("SelectAction") + ":");
	GetMainLayout()->addWidget(Label);

	RadioExit = new QRadioButton(this);
	RadioExit->setCursor(CURSOR_POINTING_HAND);
	RadioExit->setText(LOCALIZATION("ExitToApplication"));
	RadioExit->setIcon(BUFFER_ICONS("Exit"));
	RadioExit->setChecked(true);
	GetMainLayout()->addWidget(RadioExit);

	RadioChangeUser = new QRadioButton(this);
	RadioChangeUser->setCursor(CURSOR_POINTING_HAND);
	RadioChangeUser->setText(LOCALIZATION("ChangeUser"));
	RadioChangeUser->setIcon(BUFFER_ICONS("UserChange"));
	GetMainLayout()->addWidget(RadioChangeUser);

	RadioLock = new QRadioButton(this);
	RadioLock->setCursor(CURSOR_POINTING_HAND);
	RadioLock->setText(LOCALIZATION("Block"));
	RadioLock->setIcon(BUFFER_ICONS("Lock"));
	GetMainLayout()->addWidget(RadioLock);

	QHBoxLayout *LayoutBottom = new QHBoxLayout();
	GetMainLayout()->addLayout(LayoutBottom);

	ButtonOK = new ISPushButton(this);
	ButtonOK->setText("OK");
	ButtonOK->setCursor(CURSOR_POINTING_HAND);
	connect(ButtonOK, &ISPushButton::clicked, this, &ISExitForm::Select);
	LayoutBottom->addWidget(ButtonOK);

	ButtonClose = new ISPushButton(this);
	ButtonClose->setText(LOCALIZATION("Cancel"));
	ButtonClose->setCursor(CURSOR_POINTING_HAND);
	ButtonClose->setFocus();
	connect(ButtonClose, &ISPushButton::clicked, this, &ISExitForm::Close);
	LayoutBottom->addWidget(ButtonClose);
}
//-----------------------------------------------------------------------------
ISExitForm::~ISExitForm()
{

}
//-----------------------------------------------------------------------------
ISNamespace::ExitFormAction ISExitForm::GetSelectedAction()
{
	return SelectedAction;
}
//-----------------------------------------------------------------------------
void ISExitForm::showEvent(QShowEvent *e)
{
	QRect Rect = ISMemoryObjects::GetInstance().GetMainWindow()->property("geometry").toRect();
	
	int x = Rect.width() / 2;
	x = Rect.x() + x;
	x = x - (width() / 2);

	int y = Rect.height() / 2;
	y = Rect.y() + y;
	y = y - (height() / 2);

	move(x, y);

	ISAdditionalForm::showEvent(e);
}
//-----------------------------------------------------------------------------
void ISExitForm::Select()
{
	if (RadioExit->isChecked())
	{
		SelectedAction = ISNamespace::ExitFormAction::EF_Action_Exit;
	}
	else if (RadioChangeUser->isChecked())
	{
		SelectedAction = ISNamespace::ExitFormAction::EF_Action_ChangeUser;
	}
	else if (RadioLock->isChecked())
	{
		SelectedAction = ISNamespace::ExitFormAction::EF_Action_Lock;
	}

	SetResult(true);
	close();
}
//-----------------------------------------------------------------------------
void ISExitForm::Close()
{
	SetResult(false);
	close();
}
//-----------------------------------------------------------------------------
void ISExitForm::EnterClicked()
{
	if (ButtonOK->hasFocus())
	{
		Select();
	}
	else if (ButtonClose->hasFocus())
	{
		close();
	}
}
//-----------------------------------------------------------------------------
