#pragma once
//-----------------------------------------------------------------------------
#include "ISInterfaceForm.h"
#include "ISDesktopBaseForm.h"
#include "ISMenuBar.h"
#include "ISStatusBar.h"
#include "ISCalendarForm.h"
#include "ISChatForm.h"
#include "ISSystemsPanel.h"
#include "ISWorkspaceForm.h"
#include "ISNotificationsForm.h"
//-----------------------------------------------------------------------------
class ISMainWindow : public ISInterfaceForm
{
	Q_OBJECT

	Q_PROPERTY(QString CurrentParagraphUID READ GetCurrentParagraphUID)
	Q_PROPERTY(bool CloseEvent READ GetCloseEventFlag WRITE SetCloseEventFlag)

signals:
	void Unlocked();

public:
	ISMainWindow(QWidget *parent = 0);
	virtual ~ISMainWindow();
	
	void closeEvent(QCloseEvent *e);
	
	QString GetCurrentParagraphUID() const;

	void SetCloseEventFlag(bool close_event);
	bool GetCloseEventFlag() const;

	void LockApplication(); //���������� ���������

protected:
	void AfterShowEvent() override;
	void CreateMenuBar(); //�������� �������� �������
	void CreateLicenseMessage(); //������ � ���������� � �������������� ���������
	void CreateInformationMessage(); //�������� ��������������� ���������
	void CreateStackWidget(); //�������� ��������� �������� � ������� �������
	void CreateStatusBar(); //�������� ������-����
	void CreateNotificationsForm(); //�������� ����� �����������

	void ShowCreateRecords(); //�������� ����� �������� �������� �������� �������
	void ShowExternalTools(); //�������� ����� �������� �������� �������
	void ParagraphClicked(const ISUuid &ParagraphUID); //������� ������� �� ������-��������
	void SetVisibleShadow(bool Visible); //�������� ��������� ����
	
	void TerminateMe(const QVariantMap &VariantMap); //������ �� ���������� ������������
	void UpdateAviable(const QVariantMap &VariantMap); //������ � ������� ����������

	void BeforeClose(); //�������� ����� ����������� ���������
	void OpenHistoryObject(const QString &TableName, int ObjectID);
	void EscapeClicked() override;
	void LockClicked(); //���������� ���������
	void ChangeUser(); //����� ������������
	void ShowStyleFactoryForm(); //�������� ����� ������ ������� �����
	void ShowFavoritesForm(); //������� ����� ����������
	void ShowHistoryForm(); //������� ����� �������
	void ShowChangePasswordForm(); //������� ����� ����� ������
	void CreateLogToday(); //������������ ��� ����� �� ������� ����
	void ShowNoteForm(); //������� ����� "�������"
	void ShowCalculator(); //������� �����������
	void ShowNotifications(); //������� ����� "�����������"
	void ShowDebugApplication(); //������� ��������
	void ShowSettingsForm(); //������� ����� "���������"
	void ShowHelpSystem(); //������� ���������� �������
	void ShowAboutForm(); //������� ����� "� ���������"
	void ShowAboutQt(); //������� ����� "� Qt"
	void ShowLicenseForm(); //������� ����� "��������"

private:
	QLabel *LabelShadow;
	ISMenuBar *MenuBar;

	QMap<ISUuid, int> Paragraphs;
	QStackedWidget *StackedWidget; //����-������� ��������
	ISDesktopBaseForm *WidgetDesktop; //������ �������� �����
	ISWorkspaceForm *WorkspaceForm; //������ ������� �������
	ISCalendarForm *CalendarForm; //������ ���������
	ISChatForm *ChatForm; //������ ����
	ISNotificationsForm *NotificationsForm;
	ISStatusBar *StatusBar;
	
	QString CurrentParagraphUID;
	bool CloseEvent;
};
//-----------------------------------------------------------------------------
