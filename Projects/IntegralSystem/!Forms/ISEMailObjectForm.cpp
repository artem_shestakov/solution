#include "StdAfx.h"
#include "ISEMailObjectForm.h"
#include "ISLocalization.h"
#include "ISMessageBox.h"
#include "ISEMail.h"
//-----------------------------------------------------------------------------
ISEMailObjectForm::ISEMailObjectForm(ISNamespace::ObjectFormType form_type, PMetaClassTable *meta_table, QWidget *parent, int object_id) : ISObjectFormBase(form_type, meta_table, parent, object_id)
{

}
//-----------------------------------------------------------------------------
ISEMailObjectForm::~ISEMailObjectForm()
{

}
//-----------------------------------------------------------------------------
void ISEMailObjectForm::SetRecipient(const QVariant &Recipient)
{
	ISFieldEditBase *EditRecipient = GetFieldWidget("Recipient");
	EditRecipient->SetValue(Recipient);
	EditRecipient->setEnabled(false);
}
//-----------------------------------------------------------------------------
bool ISEMailObjectForm::Save()
{
	bool Result = ISObjectFormBase::Save();
	if (Result)
	{
		if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.SendMailAfterSave")))
		{
			ISEMail::SendMail(GetObjectID(), GetFieldValue("Subject").toString(), GetFieldValue("Recipient").toInt());
		}
	}

	return Result;
}
//-----------------------------------------------------------------------------
