#include "StdAfx.h"
#include "ISDesktopBaseForm.h"
#include "EXDefines.h"
#include "EXConstants.h"
#include "ISUserRoleEntity.h"
#include "ISPlugin.h"
#include "ISAssert.h"
#include "ISBuffer.h"
#include "ISLocalization.h"
//-----------------------------------------------------------------------------
ISDesktopBaseForm::ISDesktopBaseForm(QWidget *parent) : ISParagraphBaseForm(parent)
{
	MainLayout = new QVBoxLayout();
	setLayout(MainLayout);

	QString DesktopFormName = ISPlugin::GetInstance().GetDesktopForm();
	if (DesktopFormName.length())
	{
		if (ISUserRoleEntity::GetInstance().CheckPermissionSpecial(CONST_UID_PERMISSION_SPECIAL_DESKTOP))
		{
			int ObjectType = QMetaType::type((DesktopFormName + "*").toLocal8Bit().constData());
			IS_ASSERT(ObjectType, QString("Class for desktop form is NULL. DesktopFormName: %1").arg(DesktopFormName));

			const QMetaObject *MetaObject = QMetaType::metaObjectForType(ObjectType);
			IS_ASSERT(MetaObject, "Error opening desktop widget.");

			QWidget *DesktopWidget = dynamic_cast<QWidget*>(MetaObject->newInstance(Q_ARG(QWidget *, this)));
			IS_ASSERT(DesktopWidget, QString("Error instance desktop widget. DesktopFormName: %1").arg(DesktopFormName));
			MainLayout->addWidget(DesktopWidget);
		}
		else
		{
			QLabel *Label = new QLabel(this);
			Label->setText(LOCALIZATION("NotPermissionSpecialDesktop"));
			Label->setFont(FONT_TAHOMA_12_BOLD);
			MainLayout->addWidget(Label, 0, Qt::AlignCenter);
		}
	}
	else
	{
		QLabel *LabelLogo = new QLabel(this);
		LabelLogo->setPixmap(BUFFER_PIXMAPS("DesktopLogo"));
		MainLayout->addWidget(LabelLogo, 0, Qt::AlignCenter);
	}
}
//-----------------------------------------------------------------------------
ISDesktopBaseForm::~ISDesktopBaseForm()
{

}
//-----------------------------------------------------------------------------
