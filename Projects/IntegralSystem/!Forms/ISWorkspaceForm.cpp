#include "StdAfx.h"
#include "ISWorkspaceForm.h"
#include "EXDefines.h"
#include "ISMemoryObjects.h"
#include "ISMetaSystemsEntity.h"
#include "ISUserRoleEntity.h"
#include "ISSystemsPanel.h"
#include "ISProtocol.h"
#include "ISSystem.h"
#include "ISAssert.h"
#include "ISListBaseForm.h"
#include "ISMetaData.h"
#include "ISObjectFormBase.h"
//-----------------------------------------------------------------------------
ISWorkspaceForm::ISWorkspaceForm(QWidget *parent) : ISParagraphBaseForm(parent)
{
	CentralForm = nullptr;

	ISMemoryObjects::GetInstance().SetWorkspaceForm(this);

	Layout = new QVBoxLayout();
	Layout->setContentsMargins(LAYOUT_MARGINS_NULL);
	Layout->setSpacing(0);
	setLayout(Layout);

	CreateSystems();
	CreateTabWidget();
}
//-----------------------------------------------------------------------------
ISWorkspaceForm::~ISWorkspaceForm()
{

}
//-----------------------------------------------------------------------------
void ISWorkspaceForm::AddObjectForm(QWidget *ObjectFormWidget)
{
	ISObjectFormBase *ObjectForm = dynamic_cast<ISObjectFormBase*>(ObjectFormWidget);

	//��������� ������������ ��������� ������� ����� �������
	connect(ObjectForm, &ISObjectFormBase::windowTitleChanged, [=](const QString &WindowTitle)
	{
		TabWidget->setTabText(TabWidget->indexOf(ObjectForm), WindowTitle);
	});

	//��������� ������ ��������� ������� ����� �������
	connect(ObjectForm, &ISObjectFormBase::windowIconChanged, [=](const QIcon &WindowIcon)
	{
		TabWidget->setTabIcon(TabWidget->indexOf(ObjectForm), WindowIcon);
	});

	connect(ObjectForm, &ISObjectFormBase::CurrentObjectTab, [=]
	{
		TabWidget->setCurrentWidget(ObjectFormWidget);
	});

	ObjectForm->setParent(this);

	TabWidget->addTab(ObjectForm, ObjectForm->windowIcon(), ObjectForm->windowTitle());
	TabWidget->setCurrentWidget(ObjectForm);
}
//-----------------------------------------------------------------------------
void ISWorkspaceForm::CreateSystems()
{
	QVector<ISMetaSystem*> VectorMetaSystems; //��������� ����������

	for (int i = 0; i < ISMetaSystemsEntity::GetInstance().GetCountSystems(); i++) //����� ������
	{
		ISMetaSystem *MetaSystem = ISMetaSystemsEntity::GetInstance().GetSystems().at(i);
		bool Assess = ISUserRoleEntity::GetInstance().CheckAllSubSystemsForSystem(MetaSystem->GetUID());
		if (Assess)
		{
			VectorMetaSystems.append(MetaSystem);
		}
	}

	ISSystemsPanel *SystemsPanel = new ISSystemsPanel(this);
	connect(SystemsPanel, &ISSystemsPanel::ClickedSubSystem, this, &ISWorkspaceForm::ClickedSubSystem);
	Layout->addWidget(SystemsPanel);

	//���������� ������ � �������
	for (int i = 0; i < VectorMetaSystems.count(); i++)
	{
		SystemsPanel->AddSystem(VectorMetaSystems.at(i));
	}
}
//-----------------------------------------------------------------------------
void ISWorkspaceForm::CreateTabWidget()
{
	TabWidget = new ISTabWidgetMain(this);
	TabWidget->setSizePolicy(QSizePolicy::Ignored, TabWidget->sizePolicy().verticalPolicy());
	connect(TabWidget, &ISTabWidgetMain::Duplicate, this, &ISWorkspaceForm::AddObjectForm);
	Layout->addWidget(TabWidget);
}
//-----------------------------------------------------------------------------
void ISWorkspaceForm::ClickedSubSystem(const QString &SubSystemUID, const QIcon &IconSubSystem)
{
	if (SubSystemUID == CurrentSubSystemUID)
	{
		return;
	}
	else
	{
		CurrentSubSystemUID = SubSystemUID;
	}

	ISSystem::SetWaitGlobalCursor(true);

	ISMetaSubSystem *MetaSubSystem = ISMetaSystemsEntity::GetInstance().GetSubSystem(SubSystemUID);

	if (CentralForm) //���� ����������� ����� ���� ������� - ������� �
	{
		delete CentralForm;
		CentralForm = nullptr;
	}

	QString TableName = MetaSubSystem->GetTableName();
	QString ClassName = MetaSubSystem->GetClassName();

	TabWidget->tabBar()->setTabIcon(0, IconSubSystem);
	TabWidget->tabBar()->setTabText(0, MetaSubSystem->GetLocalName());
	TabWidget->tabBar()->setCurrentIndex(0);

	PMetaClassTable *MetaTable = nullptr;
	if (TableName.length()) //�������� �������
	{
		MetaTable = ISMetaData::GetInstanse().GetMetaTable(TableName);
		ISProtocol::OpenSubSystem(MetaTable->GetName(), MetaTable->GetLocalListName());

		ISListBaseForm *ListBaseForm = new ISListBaseForm(MetaTable, this);
		CentralForm = ListBaseForm;
	}
	else if (ClassName.length()) //�������� ������ (�������)
	{
		ISProtocol::OpenSubSystem(QString(), MetaSubSystem->GetLocalName());

		int ObjectType = QMetaType::type((ClassName + "*").toLocal8Bit().constData());
		IS_ASSERT(ObjectType, QString("Class for SybSystem is NULL. ClassName: %1").arg(ClassName));

		const QMetaObject *MetaObject = QMetaType::metaObjectForType(ObjectType);
		IS_ASSERT(MetaObject, "Error opening subsystem widget.");

		CentralForm = dynamic_cast<ISInterfaceMetaForm*>(MetaObject->newInstance(Q_ARG(QWidget *, this)));
		IS_ASSERT(CentralForm, QString("Error instance subsystem. ClassName: %1").arg(ClassName));
	}

	CentralForm->SetUID(SubSystemUID);
	connect(CentralForm, &ISListBaseForm::AddFormFromTab, this, &ISWorkspaceForm::AddObjectForm);
	TabWidget->GetMainTab()->layout()->addWidget(CentralForm);

	ISSystem::SetWaitGlobalCursor(false);
	ISSystem::RepaintWidget(CentralForm);

	QTimer::singleShot(600, Qt::PreciseTimer, CentralForm, &ISInterfaceMetaForm::LoadData);
}
//-----------------------------------------------------------------------------
