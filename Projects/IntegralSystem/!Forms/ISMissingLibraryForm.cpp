#include "StdAfx.h"
#include "ISMissingLibraryForm.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "EXDefines.h"
#include "ISCore.h"
#include "ISControls.h"
#include "ISPushButton.h"
//-----------------------------------------------------------------------------
ISMissingLibraryForm::ISMissingLibraryForm(QWidget *parent) : ISInterfaceDialogForm(parent)
{
	setWindowTitle(LOCALIZATION("MissingLibrary"));

	GetMainLayout()->setContentsMargins(LAYOUT_MARGINS_10_PX);
	ForbidResize();

	QLabel *Label = new QLabel(this);
	Label->setText(LOCALIZATION("MissingLibraryText"));
	GetMainLayout()->addWidget(Label, 0, Qt::AlignLeft);

	ListWidget = new ISListWidget(this);
	GetMainLayout()->addWidget(ListWidget);

	GetMainLayout()->addWidget(ISControls::CreateHorizontalLine(this));

	ISPushButton *ButtonClose = new ISPushButton(this);
	ButtonClose->setText(LOCALIZATION("Close"));
	ButtonClose->setIcon(BUFFER_ICONS("Close"));
	connect(ButtonClose, &ISPushButton::clicked, this, &ISMissingLibraryForm::close);
	GetMainLayout()->addWidget(ButtonClose, 0, Qt::AlignRight);
}
//-----------------------------------------------------------------------------
ISMissingLibraryForm::~ISMissingLibraryForm()
{

}
//-----------------------------------------------------------------------------
void ISMissingLibraryForm::SetLibraryList(const QStringList &StringList)
{
	for (int i = 0; i < StringList.count(); i++)
	{
		OnAddLibrary(StringList.at(i));
	}
}
//-----------------------------------------------------------------------------
void ISMissingLibraryForm::OnAddLibrary(const QString &Library)
{
	QListWidgetItem *ListWidgetItem = new QListWidgetItem(ListWidget);
	ListWidgetItem->setText(Library);
	ListWidgetItem->setIcon(BUFFER_ICONS("LibraryDll"));
	ListWidgetItem->setSizeHint(QSize(ListWidgetItem->sizeHint().width(), 30));
}
//-----------------------------------------------------------------------------
