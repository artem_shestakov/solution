#include "StdAfx.h"
#include "ISStatisticsDatabaseForm.h"
#include "EXConstants.h"
#include "ISScrollArea.h"
#include "ISSystem.h"
#include "EXDefines.h"
#include "ISLocalization.h"
#include "ISDatabase.h"
//-----------------------------------------------------------------------------
ISStatisticsDatabaseForm::ISStatisticsDatabaseForm(QWidget *parent) : ISInterfaceMetaForm(parent)
{
	Layout = new QVBoxLayout();

	ISScrollArea *ScrollArea = new ISScrollArea(this);
	ScrollArea->widget()->setLayout(Layout);
	GetMainLayout()->addWidget(ScrollArea);

	//������ ���� ������
	QLabel *LabelDatabaseSize = new QLabel(this);
	LabelDatabaseSize->setText(LOCALIZATION("DatabaseSize") + ": " + ISDatabase::GetInstance().GetCurrentDatabaseSize());
	Layout->addWidget(LabelDatabaseSize);

	//������ PostgreSQL
	QLabel *LabelPostgreSQLVersion = new QLabel(this);
	LabelPostgreSQLVersion->setText(LOCALIZATION("VersionPostgreSQL") + ": " + ISDatabase::GetInstance().GetVersionPostgres());
	Layout->addWidget(LabelPostgreSQLVersion);

	//����� ������� �������
	QLabel *LabelStartTimeServer = new QLabel(this);
	LabelStartTimeServer->setText(LOCALIZATION("ServerStartTime") + ": " + ISDatabase::GetInstance().GetStartTimeServer());
	Layout->addWidget(LabelStartTimeServer);

	//������ ���������� ����������
	QLabel *LabelInetClientAddress = new QLabel(this);
	LabelInetClientAddress->setText(LOCALIZATION("InetClientAddress") + ": " + ISDatabase::GetInstance().GetInetClientAddress());
	Layout->addWidget(LabelInetClientAddress);

	//����� ���������� ����������
	QLabel *LabelInetServerAddress = new QLabel(this);
	LabelInetServerAddress->setText(LOCALIZATION("InetServerAddress") + ": " + ISDatabase::GetInstance().GetInetServerAddress());
	Layout->addWidget(LabelInetServerAddress);

	Layout->addStretch();
}
//-----------------------------------------------------------------------------
ISStatisticsDatabaseForm::~ISStatisticsDatabaseForm()
{

}
//-----------------------------------------------------------------------------
void ISStatisticsDatabaseForm::LoadData()
{

}
//-----------------------------------------------------------------------------
