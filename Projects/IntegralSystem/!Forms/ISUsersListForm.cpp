#include "StdAfx.h"
#include "ISUsersListForm.h"
#include "ISMetaData.h"
#include "EXDefines.h"
#include "ISControls.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISMessageBox.h"
#include "ISMetaUser.h"
#include "ISUserPasswordForm.h"
//-----------------------------------------------------------------------------
ISUsersListForm::ISUsersListForm(QWidget *parent) : ISListBaseForm(ISMetaData::GetInstanse().GetMetaTable("_Users"), parent)
{
	QAction *ActionChangePassword = ISControls::CreateActionPasswordChange(this);
	connect(ActionChangePassword, &QAction::triggered, this, &ISUsersListForm::ChangePassword);
	AddAction(ActionChangePassword, true, true);
}
//-----------------------------------------------------------------------------
ISUsersListForm::~ISUsersListForm()
{

}
//-----------------------------------------------------------------------------
void ISUsersListForm::CreateCopy()
{
	if (CheckThisUser())
	{
		ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.NotCreateCopyThisUser"));
	}
	else
	{
		ISListBaseForm::CreateCopy();
	}
}
//-----------------------------------------------------------------------------
void ISUsersListForm::Edit()
{
	if (CheckThisUser())
	{
		ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.NotEditThisUser"));
	}
	else
	{
		ISListBaseForm::Edit();
	}
}
//-----------------------------------------------------------------------------
void ISUsersListForm::ChangePassword()
{
	if (CheckThisUser())
	{
		ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.NotEditThisUser"));
		return;
	}

	if (CheckPostgres())
	{
		ISMessageBox::ShowWarning(this, LOCALIZATION("Message.User.ChangePassword.Postgres"));
		return;
	}

	QString FullName = GetCurrentRecordValue("Surname").toString() + " " + GetCurrentRecordValue("Name").toString() + " " + GetCurrentRecordValue("Patronymic").toString();
	ISUserPasswordForm UserPasswordForm(GetCurrentRecordValue("ID").toInt(), GetCurrentRecordValue("Login").toString(), FullName);
	if (UserPasswordForm.Exec())
	{
		ISMessageBox::ShowInformation(this, LOCALIZATION("Message.Information.ChangePasswordUser").arg(FullName));
	}
}
//-----------------------------------------------------------------------------
bool ISUsersListForm::CheckPostgres()
{
	QString Login = GetCurrentRecordValue("Login").toString();
	if (Login == SYSTEM_USER_LOGIN)
	{
		return true;
	}

	return false;
}
//-----------------------------------------------------------------------------
bool ISUsersListForm::CheckThisUser()
{
	if (GetObjectID() == CURRENT_USER_ID)
	{
		return true;
	}

	return false;
}
//-----------------------------------------------------------------------------
