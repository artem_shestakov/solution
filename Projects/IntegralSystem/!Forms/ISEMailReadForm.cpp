#include "StdAfx.h"
#include "ISEMailReadForm.h"
#include "EXDefines.h"
#include "ISAssert.h"
#include "ISQuery.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISLineEdit.h"
#include "ISLineEdit.h"
#include "ISCheckEdit.h"
#include "ISTextEdit.h"
#include "ISPushButton.h"
#include "ISMessageBox.h"
#include "ISEMailObjectForm.h"
#include "ISCore.h"
#include "ISMetaData.h"
//-----------------------------------------------------------------------------
static QString QS_MAIL_INCOMING = PREPARE_QUERY("SELECT concat(usrs_surname, ' ', usrs_name, ' ', usrs_patronymic), mail_departuredatetime, mail_recipient, mail_subject, mail_important, mail_message "
												"FROM _email "
												"LEFT JOIN _users ON usrs_id = mail_sender "
												"WHERE mail_id = :MailID");
//-----------------------------------------------------------------------------
ISEMailReadForm::ISEMailReadForm(int mail_id, QWidget *parent) : ISInterfaceForm(parent)
{
	setWindowTitle(LOCALIZATION("IncomingMail"));
	setWindowIcon(BUFFER_ICONS("EMail.ListForm.Incoming"));
	resize(SIZE_MAIN_WINDOW_MINIMUM);

	GetMainLayout()->setContentsMargins(LAYOUT_MARGINS_10_PX);

	QFormLayout *FormLayout = new QFormLayout();
	GetMainLayout()->addLayout(FormLayout);

	ISQuery qSelectMail(QS_MAIL_INCOMING);
	qSelectMail.BindValue(":MailID", mail_id);
	IS_ASSERT(qSelectMail.ExecuteFirst(), qSelectMail.GetSqlQuery().lastError().text());

	Recipient = qSelectMail.ReadColumn("mail_recipient").toInt();

	ISLineEdit *EditSender = new ISLineEdit(this);
	EditSender->SetVisibleClear(false);
	EditSender->SetReadOnly(true);
	EditSender->SetValue(qSelectMail.ReadColumn("concat"));
	FormLayout->addRow(LOCALIZATION("Sender") + ":", EditSender);

	ISLineEdit *EditDepartureDateTime = new ISLineEdit(this);
	EditDepartureDateTime->SetVisibleClear(false);
	EditDepartureDateTime->SetReadOnly(true);
	EditDepartureDateTime->SetValue(qSelectMail.ReadColumn("mail_departuredatetime").toDateTime().toString(DATE_TIME_FORMAT_V1));
	FormLayout->addRow(LOCALIZATION("DepartureDateTime") + ":", EditDepartureDateTime);

	ISLineEdit *EditSubject = new ISLineEdit(this);
	EditSubject->SetVisibleClear(false);
	EditSubject->SetReadOnly(true);
	EditSubject->SetValue(qSelectMail.ReadColumn("mail_subject"));
	FormLayout->addRow(LOCALIZATION("Subject") + ":", EditSubject);

	ISCheckEdit *CheckEditImportant = new ISCheckEdit(this);
	CheckEditImportant->SetReadOnly(true);
	CheckEditImportant->SetValue(qSelectMail.ReadColumn("mail_important"));
	FormLayout->addRow(LOCALIZATION("Important") + ":", CheckEditImportant);

	ISTextEdit *TextEdit = new ISTextEdit(this);
	TextEdit->SetVisibleClear(false);
	TextEdit->SetReadOnly(true);
	TextEdit->SetValue(qSelectMail.ReadColumn("mail_message"));
	GetMainLayout()->addWidget(TextEdit);

	QHBoxLayout *LayoutBottom = new QHBoxLayout();
	LayoutBottom->addStretch();
	GetMainLayout()->addLayout(LayoutBottom);

	ISPushButton *ButtonReply = new ISPushButton(this);
	ButtonReply->setText(LOCALIZATION("Reply"));
	connect(ButtonReply, &ISPushButton::clicked, this, &ISEMailReadForm::Reply);
	LayoutBottom->addWidget(ButtonReply);

	ISPushButton *ButtonClose = new ISPushButton(this);
	ButtonClose->setText(LOCALIZATION("Close"));
	ButtonClose->setIcon(BUFFER_ICONS("Close"));
	connect(ButtonClose, &ISPushButton::clicked, this, &ISEMailReadForm::close);
	LayoutBottom->addWidget(ButtonClose);
}
//-----------------------------------------------------------------------------
ISEMailReadForm::~ISEMailReadForm()
{

}
//-----------------------------------------------------------------------------
void ISEMailReadForm::EscapeClicked()
{
	close();
}
//-----------------------------------------------------------------------------
void ISEMailReadForm::Reply()
{
	if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.ReplyEMail")))
	{
		ISEMailObjectForm *EMailObjectForm = dynamic_cast<ISEMailObjectForm*>(ISCore::CreateObjectForm(ISNamespace::OFT_New, ISMetaData::GetInstanse().GetMetaTable("_EMail")));
		EMailObjectForm->SetRecipient(Recipient);
		EMailObjectForm->show();
		close();
	}
}
//-----------------------------------------------------------------------------
