#pragma once
//-----------------------------------------------------------------------------
#include "ISInterfaceForm.h"
//-----------------------------------------------------------------------------
class ISEMailReadForm : public ISInterfaceForm
{
	Q_OBJECT

public:
	ISEMailReadForm(int mail_id, QWidget *parent = 0);
	virtual ~ISEMailReadForm();

protected:
	void EscapeClicked() override;
	void Reply();

private:
	int Recipient;
};
//-----------------------------------------------------------------------------
