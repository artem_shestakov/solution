#include "StdAfx.h"
#include "ISMonitorActivityForm.h"
#include "EXConstants.h"
#include "ISMetaData.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISNotify.h"
#include "ISQuery.h"
#include "ISMessageBox.h"
//-----------------------------------------------------------------------------
static QString QS_USERS = PREPARE_QUERY("SELECT DISTINCT usrs_id "
										"FROM pg_stat_activity "
										"LEFT JOIN _users ON usrs_login = usename "
										"WHERE usrs_login != 'postgres'");
//-----------------------------------------------------------------------------
ISMonitorActivityForm::ISMonitorActivityForm(QWidget *parent) : ISListViewForm(ISMetaData::GetInstanse().GetMetaQuery("MonitorActivity"), parent)
{
	ActionTerminateCurrentUser = new QAction(this);
	ActionTerminateCurrentUser->setText(LOCALIZATION("TerminateUser"));
	ActionTerminateCurrentUser->setToolTip(LOCALIZATION("TerminateUser"));
	ActionTerminateCurrentUser->setIcon(BUFFER_ICONS("UserTerminate"));
	connect(ActionTerminateCurrentUser, &QAction::triggered, this, &ISMonitorActivityForm::TerminateSelectedUser);
	AddAction(ActionTerminateCurrentUser, true);

	ActionTerminateAllUsers = new QAction(this);
	ActionTerminateAllUsers->setText(LOCALIZATION("TerminateAllUsers"));
	ActionTerminateAllUsers->setToolTip(LOCALIZATION("TerminateAllUsers"));
	ActionTerminateAllUsers->setIcon(BUFFER_ICONS("UsersTerminate"));
	connect(ActionTerminateAllUsers, &QAction::triggered, this, &ISMonitorActivityForm::TerminateAllUsers);
	AddAction(ActionTerminateAllUsers, true);
}
//-----------------------------------------------------------------------------
ISMonitorActivityForm::~ISMonitorActivityForm()
{

}
//-----------------------------------------------------------------------------
void ISMonitorActivityForm::LoadDataAfterEvent()
{
	ISListViewForm::LoadDataAfterEvent();
	
	ActionTerminateCurrentUser->setVisible(true);
	ActionTerminateAllUsers->setVisible(true);
}
//-----------------------------------------------------------------------------
void ISMonitorActivityForm::TerminateAllUsers()
{
	if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.TerminateAllUsers")))
	{
		ISQuery qSelect(QS_USERS);
		if (qSelect.Execute())
		{
			int Count = qSelect.GetCountResultRows();
			while (qSelect.Next())
			{
				int UserID = qSelect.ReadColumn("usrs_id").toInt();
				TerminateUser(UserID);
			}

			ISMessageBox::ShowInformation(this, LOCALIZATION("Message.Information.QueryTerminateAllUsers").arg(Count));
		}
	}
}
//-----------------------------------------------------------------------------
void ISMonitorActivityForm::TerminateSelectedUser()
{
	QSqlRecord SqlRecord = GetSqlModel()->GetRecord(GetCurrentRowIndex());
	QString UserName = SqlRecord.value(GetSqlModel()->GetFieldIndex("User")).toString();
	int UserID = SqlRecord.value(GetSqlModel()->GetFieldIndex("UserID")).toInt();

	if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.TerminateUser").arg(UserName)))
	{
		TerminateUser(UserID);
		ISMessageBox::ShowInformation(this, LOCALIZATION("Message.Information.QueryTerminateUser").arg(UserName));
	}
}
//-----------------------------------------------------------------------------
void ISMonitorActivityForm::TerminateUser(int UserID)
{
	ISNotify::GetInstance().SendNotification(CONST_UID_NOTIFY_TERMINATE_USER, QVariant(), false, UserID);
}
//-----------------------------------------------------------------------------
