#include "StdAfx.h"
#include "ISTaskObjectForm.h"
#include "EXConstants.h"
#include "ISMessageBox.h"
#include "ISLocalization.h"
#include "ISMetaUser.h"
#include "ISNotify.h"
//-----------------------------------------------------------------------------
ISTaskObjectForm::ISTaskObjectForm(ISNamespace::ObjectFormType form_type, PMetaClassTable *meta_table, QWidget *parent, int object_id) : ISObjectFormBase(form_type, meta_table, parent, object_id)
{
	
}
//-----------------------------------------------------------------------------
ISTaskObjectForm::~ISTaskObjectForm()
{

}
//-----------------------------------------------------------------------------
bool ISTaskObjectForm::Save()
{
	if (GetFieldValue("DateLimit").toDate() < QDate::currentDate()) //���� ��������� ������������ ������
	{
		ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.NotCreateExpiredTask"));
		GetFieldWidget("DateLimit")->BlinkRed();
		return false;
	}
	
	bool IsNew = true;

	if (GetFormType() == ISNamespace::OFT_Edit)
	{
		IsNew = false;
	}

	bool Result = ISObjectFormBase::Save();
	if (Result)
	{
		int ExecutorID = GetFieldValue("Executor").toInt();
		QString TaskName = GetFieldValue("Name").toString();

		if (IsNew) //���� ������ ���� �������
		{
			ISNotify::GetInstance().SendNotification(CONST_UID_NOTIFY_NEW_TASK, TaskName, true, ExecutorID);
		}
		else //������ ���� ���������������
		{
			ISNotify::GetInstance().SendNotification(CONST_UID_NOTIFY_EDIT_TASK, TaskName, true, ExecutorID);
		}
	}

	return Result;
}
//-----------------------------------------------------------------------------
