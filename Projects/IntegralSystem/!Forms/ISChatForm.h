#pragma once
//-----------------------------------------------------------------------------
#include "ISParagraphBaseForm.h"
#include "ISTextEdit.h"
#include "ISNamespace.h"
#include "ISPushButton.h"
#include "ISListWidget.h"
//-----------------------------------------------------------------------------
class ISChatForm : public ISParagraphBaseForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISChatForm(QWidget *parent = 0);
	virtual ~ISChatForm();

protected:
	void showEvent(QShowEvent *e);
	void LoadMessages(int Limit); //�������� ���������
	void ReloadMessages(int Limit); //������������ ���������
	void SendMessage(); //�������� ���������
	void AttachImage(); //������������ ����������� � ���������
	void AttachFile(); //������������ ����� � ���������
	void ClearAttach(); //������� ������������� �������
	void CreateItemWidget(int MessageID);
	void CreateItemWidget(int MessageID, const QString &Message, const QByteArray &ByteImage, const QString &FileName, const QString &UserFullName, const QDateTime &DateTime);
	
	QVariant GetImage() const;
	QVariant GetFile() const;
	QString GetFileName() const;

	void CopyText(); //����������� ����� � ����� ������

	void NotifyMessage(const QVariantMap &VariantMap);

private:
	QButtonGroup *ButtonGroup;
	ISListWidget *ListWidget;
	QAction *ActionCopyText;
	ISTextEdit *TextEdit;
	QLabel *LabelLoading;
	QLabel *LabelAttachPath;

	QAction *ActionAttachImage;
	QAction *ActionAttachFile;
	QAction *ActionClearAttach;
	ISPushButton *ButtonSend;

	ISNamespace::AttachChatType CurrentAttach;
	QSound *SoundSend;
	int CountMessage;

	QIcon IconChat;
	QIcon IconNewMessage;
};
//-----------------------------------------------------------------------------
