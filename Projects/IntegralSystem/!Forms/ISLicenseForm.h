#pragma once
//-----------------------------------------------------------------------------
#include "ISInterfaceDialogForm.h"
#include "ISNamespace.h"
//-----------------------------------------------------------------------------
class ISLicenseForm : public ISInterfaceDialogForm
{
	Q_OBJECT

public:
	ISLicenseForm(QWidget *parent = 0);
	virtual ~ISLicenseForm();

	ISNamespace::LicenseFormAction GetSelectedAction();

protected:
	void Activate();
	void ExtendTesting();
	void Testing();
	void Exit();

private:
	ISNamespace::LicenseFormAction SelectedAction;
};
//-----------------------------------------------------------------------------
