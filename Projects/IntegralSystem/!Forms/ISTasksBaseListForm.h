#pragma once
//-----------------------------------------------------------------------------
#include "ISListBaseForm.h"
#include "ISComboEdit.h"
#include "ISUuid.h"
//-----------------------------------------------------------------------------
class ISTasksBaseListForm : public ISListBaseForm
{
	Q_OBJECT

public:
	ISTasksBaseListForm(QWidget *parent = 0);
	virtual ~ISTasksBaseListForm();

	void CreateCopy() override;
	void Edit() override;

protected:
	void CreateComboStatus();
	void AfterShowEvent() override;
	void FilterChanged();
	bool SetTaskStatus(const QString &TaskStatusUID);

	void SetFilterStatus(const QVariant &filter_status);
	void SetFilterUser(const QVariant &filter_user);

private:
	ISComboEdit *ComboStatus;

	ISUuid FilterStatus;
	ISUuid FilterUser;
};
//-----------------------------------------------------------------------------
