#include "StdAfx.h"
#include "ISLockForm.h"
#include "EXDefines.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISMetaUser.h"
#include "ISMessageBox.h"
#include "ISSystem.h"
//-----------------------------------------------------------------------------
ISLockForm::ISLockForm(QWidget *parent) : ISInterfaceDialogForm(parent)
{
	Close = false;

	setWindowIcon(BUFFER_ICONS("Lock"));
	ForbidResize();

	connect(this, &ISLockForm::AnimationFinished, this, &ISLockForm::close);

	QLabel *LabelLogo = new QLabel(this);
	LabelLogo->setPixmap(BUFFER_PIXMAPS("BannerText").scaled(SIZE_450_450, Qt::KeepAspectRatio, Qt::SmoothTransformation));
	GetMainLayout()->addWidget(LabelLogo);

	QVBoxLayout *Layout = new QVBoxLayout();
	Layout->setContentsMargins(LAYOUT_MARGINS_5_PX);
	GetMainLayout()->addLayout(Layout);

	QLabel *LabelLock = new QLabel(this);
	LabelLock->setWordWrap(true);
	LabelLock->setFont(FONT_APPLICATION_BOLD);
	LabelLock->setText(LOCALIZATION("ApplicationLock"));
	Layout->addWidget(LabelLock);

	QFormLayout *FormLayout = new QFormLayout();

	QGroupBox *GroupBox = new QGroupBox(this);
	GroupBox->setLayout(FormLayout);
	Layout->addWidget(GroupBox);

	QLabel *LabelUser = new QLabel(this);
	LabelUser->setFont(FONT_APPLICATION_BOLD);
	LabelUser->setText(CURRENT_USER_FULL_NAME);
	FormLayout->addRow(LOCALIZATION("User") + ":", LabelUser);

	EditPassword = new ISPasswordEdit(this);
	EditPassword->SetVisibleGenerate(false);
	EditPassword->SetPasswordCheckeder(false);
	EditPassword->SetVisibleClear(false);
	EditPassword->SetFocus();
	FormLayout->addRow(LOCALIZATION("Password") + ":", EditPassword);

	ISPushButton *ButtonUnlock = new ISPushButton(this);
	ButtonUnlock->setText(LOCALIZATION("Unlock"));
	ButtonUnlock->setIcon(BUFFER_ICONS("Arrow.Right"));
	connect(ButtonUnlock, &ISPushButton::clicked, this, &ISLockForm::Unlock);
	Layout->addWidget(ButtonUnlock, 0, Qt::AlignRight);
}
//-----------------------------------------------------------------------------
ISLockForm::~ISLockForm()
{

}
//-----------------------------------------------------------------------------
void ISLockForm::closeEvent(QCloseEvent *e)
{
	if (Close)
	{
		ISInterfaceDialogForm::closeEvent(e);
	}
	else
	{
		ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.NotCloseLockForm"));
		e->ignore();
	}
}
//-----------------------------------------------------------------------------
void ISLockForm::Unlock()
{
	QString EnterPassword = EditPassword->GetValue().toString();
	if (!EnterPassword.length())
	{
		ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Error.Password.IsEmpty"));
		EditPassword->BlinkRed();
		return;
	}
	
	if (ISMetaUser::GetInstance().CheckPassword(EnterPassword))
	{
		SetResult(true);
		Close = true;
		
		HideAnimation();
		emit Unlocked();
	}
	else
	{
		ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Password.Error"));
		EditPassword->SetFocus();
	}
}
//-----------------------------------------------------------------------------
void ISLockForm::EnterClicked()
{
	Unlock();
}
//-----------------------------------------------------------------------------
