#include "StdAfx.h"
#include "ISTasksMyListForm.h"
#include "EXConstants.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISQuery.h"
#include "ISMessageBox.h"
#include "ISInputDialog.h"
//-----------------------------------------------------------------------------
static QString QU_DONE_TASK = PREPARE_QUERY("UPDATE _tasks SET "
											"task_status = :TaskStatus, "
											"task_dateofdone = CURRENT_DATE, "
											"task_resolution = :Resolution "
											"WHERE task_id = :TaskID");
//-----------------------------------------------------------------------------
ISTasksMyListForm::ISTasksMyListForm(QWidget *parent) : ISTasksBaseListForm(parent)
{
	GetQueryModel()->SetClassFilter("task_executor = ongetcurrentuserid()");

	QAction *ActionDoneTask = new QAction(this);
	ActionDoneTask->setText(LOCALIZATION("Task.Done"));
	ActionDoneTask->setToolTip(LOCALIZATION("Task.Done"));
	ActionDoneTask->setIcon(BUFFER_ICONS("Tasks.Done"));
	connect(ActionDoneTask, &QAction::triggered, this, &ISTasksMyListForm::DoneTask);
	AddAction(ActionDoneTask);
}
//-----------------------------------------------------------------------------
ISTasksMyListForm::~ISTasksMyListForm()
{

}
//-----------------------------------------------------------------------------
void ISTasksMyListForm::DoneTask()
{
	QString CurrentStatusUID = ISUuid(GetCurrentRecordValueDB("Status"));
	if (CurrentStatusUID == CONST_UID_TASK_STATUS_OPENED)
	{
		if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.DoneTask").arg(GetCurrentRecordValue("Name").toString())))
		{
			QString Resolution = ISInputDialog::GetText(this, LOCALIZATION("Task.Done.Title"), LOCALIZATION("Task.Done.Label") + ":").toString();

			ISQuery qOpenTask(QU_DONE_TASK);
			qOpenTask.BindValue(":TaskStatus", CONST_UID_TASK_STATUS_DONE);
			qOpenTask.BindValue(":TaskID", GetObjectID());

			if (Resolution.length())
			{
				qOpenTask.BindValue(":Resolution", Resolution);
			}

			if (qOpenTask.Execute())
			{
				ISMessageBox::ShowInformation(this, LOCALIZATION("Message.Information.DoneTask").arg(GetCurrentRecordValue("Name").toString()));
				Update();
			}
		}
	}
	else
	{
		ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.DoneTaskOnlyOpened"));
	}
}
//-----------------------------------------------------------------------------
