#pragma once
//-----------------------------------------------------------------------------
#include "ISListViewForm.h"
//-----------------------------------------------------------------------------
class ISStatisticTablesForm : public ISListViewForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISStatisticTablesForm(QWidget *parent = 0);
	virtual ~ISStatisticTablesForm();
};
//-----------------------------------------------------------------------------
