#pragma once
//-----------------------------------------------------------------------------
#include "ISInterfaceDialogForm.h"
#include "ISNamespace.h"
#include "ISPushButton.h"
//-----------------------------------------------------------------------------
class ISExitForm : public ISInterfaceDialogForm
{
	Q_OBJECT

public:
	ISExitForm(QWidget *parent = 0);
	~ISExitForm();

	ISNamespace::ExitFormAction GetSelectedAction();

protected:
	void showEvent(QShowEvent *e);
	void Select();
	void Close();
	void EnterClicked() override;

private:
	ISNamespace::ExitFormAction SelectedAction;

	QRadioButton *RadioExit;
	QRadioButton *RadioChangeUser;
	QRadioButton *RadioLock;

	ISPushButton *ButtonOK;
	ISPushButton *ButtonClose;
};
//-----------------------------------------------------------------------------
