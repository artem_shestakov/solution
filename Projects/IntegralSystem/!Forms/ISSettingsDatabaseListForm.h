#pragma once
//-----------------------------------------------------------------------------
#include "ISListBaseForm.h"
//-----------------------------------------------------------------------------
class ISSettingsDatabaseListForm : public ISListBaseForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISSettingsDatabaseListForm(QWidget *parent = 0);
	virtual ~ISSettingsDatabaseListForm();

protected:
	void ActiveSetting();
};
//-----------------------------------------------------------------------------
