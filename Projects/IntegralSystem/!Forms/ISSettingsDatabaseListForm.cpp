#include "StdAfx.h"
#include "ISSettingsDatabaseListForm.h"
#include "ISMetaData.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISMessageBox.h"
#include "ISQuery.h"
//-----------------------------------------------------------------------------
static QString QU_SETTING = PREPARE_QUERY("UPDATE _settingsdatabase SET sgdb_active = false");
//-----------------------------------------------------------------------------
static QString QU_SETTING_ACTIVE = PREPARE_QUERY("UPDATE _settingsdatabase SET sgdb_active = true WHERE sgdb_id = :SettingID");
//-----------------------------------------------------------------------------
ISSettingsDatabaseListForm::ISSettingsDatabaseListForm(QWidget *parent) : ISListBaseForm(ISMetaData::GetInstanse().GetMetaTable("_SettingsDatabase"), parent)
{
	QAction *ActionActive = new QAction(GetToolBar());
	ActionActive->setText(LOCALIZATION("ActiveSetting"));
	ActionActive->setToolTip(LOCALIZATION("ActiveSetting"));
	ActionActive->setIcon(BUFFER_ICONS("Apply.Blue"));
	connect(ActionActive, &QAction::triggered, this, &ISSettingsDatabaseListForm::ActiveSetting);
	AddAction(ActionActive, true, true);
}
//-----------------------------------------------------------------------------
ISSettingsDatabaseListForm::~ISSettingsDatabaseListForm()
{

}
//-----------------------------------------------------------------------------
void ISSettingsDatabaseListForm::ActiveSetting()
{
	bool Active = GetCurrentRecordValue("Active").toBool();
	if (Active)
	{
		ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.SettingDatabaseAlreadyActive"));
	}
	else
	{
		if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.ActiveSettingDatabase")))
		{
			ISQuery qDeactive(QU_SETTING);
			if (qDeactive.Execute())
			{
				ISQuery qActive(QU_SETTING_ACTIVE);
				qActive.BindValue(":SettingID", GetObjectID());
				if (qActive.Execute())
				{
					Update();
				}
			}
		}
	}
}
//-----------------------------------------------------------------------------
