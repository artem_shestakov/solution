#pragma once
//-----------------------------------------------------------------------------
#include "ISListViewForm.h"
//-----------------------------------------------------------------------------
class ISMonitorActivityForm : public ISListViewForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISMonitorActivityForm(QWidget *parent = 0);
	virtual ~ISMonitorActivityForm();

protected:
	void LoadDataAfterEvent() override;
	void TerminateAllUsers();
	void TerminateSelectedUser();
	void TerminateUser(int UserID);

private:
	QAction *ActionTerminateCurrentUser;
	QAction *ActionTerminateAllUsers;
};
//-----------------------------------------------------------------------------
