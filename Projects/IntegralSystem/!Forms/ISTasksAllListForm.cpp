#include "StdAfx.h"
#include "ISTasksAllListForm.h"
#include "EXDefines.h"
#include "EXConstants.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISQuery.h"
#include "ISMessageBox.h"
#include "ISUserEdit.h"
//-----------------------------------------------------------------------------
ISTasksAllListForm::ISTasksAllListForm(QWidget *parent) : ISTasksBaseListForm(parent)
{
	CreateComboUser();

	QAction *ActionOpenTask = new QAction(this);
	ActionOpenTask->setText(LOCALIZATION("Task.Open"));
	ActionOpenTask->setToolTip(LOCALIZATION("Task.Open"));
	ActionOpenTask->setIcon(BUFFER_ICONS("Tasks.Open"));
	connect(ActionOpenTask, &QAction::triggered, this, &ISTasksAllListForm::OpenTask);
	AddAction(ActionOpenTask);

	QAction *ActionCloseTask = new QAction(this);
	ActionCloseTask->setText(LOCALIZATION("Task.Close"));
	ActionCloseTask->setToolTip(LOCALIZATION("Task.Close"));
	ActionCloseTask->setIcon(BUFFER_ICONS("Tasks.Close"));
	connect(ActionCloseTask, &QAction::triggered, this, &ISTasksAllListForm::CloseTask);
	AddAction(ActionCloseTask);
}
//-----------------------------------------------------------------------------
ISTasksAllListForm::~ISTasksAllListForm()
{

}
//-----------------------------------------------------------------------------
void ISTasksAllListForm::CreateComboUser()
{
	GetToolBar()->addWidget(new QLabel(LOCALIZATION("Task.Executor") + ":", this));

	ISUserEdit *UserEdit = new ISUserEdit(this);
	UserEdit->SetToolTip(LOCALIZATION("Task.Executor.ToolTip"));
	connect(UserEdit, &ISComboEdit::ValueChange, this, &ISTasksAllListForm::SetFilterUser);
	GetToolBar()->addWidget(UserEdit);
}
//-----------------------------------------------------------------------------
void ISTasksAllListForm::OpenTask()
{
	ISUuid CurrentStatusUID = GetCurrentRecordValueDB("Status").toString();
	if (CurrentStatusUID == CONST_UID_TASK_STATUS_CONSIDERATION)
	{
		if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.OpenTask").arg(GetCurrentRecordValue("Name").toString()).arg(GetCurrentRecordValue("Executor").toString())))
		{
			if (SetTaskStatus(CONST_UID_TASK_STATUS_OPENED))
			{
				ISMessageBox::ShowInformation(this, LOCALIZATION("Message.Information.OpenTask").arg(GetCurrentRecordValue("Name").toString()));
				Update();
			}
		}
	}
	else
	{
		ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.OpenTaskOnlyConsideration"));
	}
}
//-----------------------------------------------------------------------------
void ISTasksAllListForm::CloseTask()
{
	ISUuid CurrentStatusUID = GetCurrentRecordValueDB("Status").toString();
	if (CurrentStatusUID == CONST_UID_TASK_STATUS_DONE)
	{
		if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.CloseTask").arg(GetCurrentRecordValue("Name").toString())))
		{
			if (SetTaskStatus(CONST_UID_TASK_STATUS_CLOSED))
			{
				ISMessageBox::ShowInformation(this, LOCALIZATION("Message.Information.CloseTask").arg(GetCurrentRecordValue("Name").toString()));
				Update();
			}
		}
	}
	else
	{
		ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.CloseOnlyDoneTasks"));
	}
}
//-----------------------------------------------------------------------------
