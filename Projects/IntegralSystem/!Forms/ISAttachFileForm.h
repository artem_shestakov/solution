#pragma once
//-----------------------------------------------------------------------------
#include "ISInterfaceDialogForm.h"
#include "ISPushButton.h"
#include "ISListWidget.h"
//-----------------------------------------------------------------------------
class ISAttachFileForm : public ISInterfaceDialogForm
{
	Q_OBJECT

signals:
	void RemoveBeginItem();
	void UpdateList();

public:
	ISAttachFileForm(QWidget *parent = 0);
	virtual ~ISAttachFileForm();

protected:
	void UpdateButtons();
	void SelectFiles();
	void Clean();
	void Delete();

	void StartDownload();
	void StopDownload();

private:
	ISListWidget *ListWidget;
	QLabel *LabelFiles;
	QProgressBar *ProgressFiles;
	QProgressBar *ProgressFile;

	ISPushButton *ButtonSelect;
	ISPushButton *ButtonClean;
	ISPushButton *ButtonStart;
	ISPushButton *ButtonStop;
	ISPushButton *ButtonClose;
};
//-----------------------------------------------------------------------------
