#include "StdAfx.h"
#include "ISEMailInfoForm.h"
#include "EXDefines.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISQuery.h"
#include "ISCheckEdit.h"
#include "ISLineEdit.h"
#include "ISTextEdit.h"
#include "ISPushButton.h"
//-----------------------------------------------------------------------------
static QString QS_EMAIL_MESSAGE = PREPARE_QUERY("SELECT "
												"mail_id, "
												"concat(u1.usrs_surname, ' ', u1.usrs_name, ' ', u1.usrs_patronymic) AS sender, "
												"concat(u2.usrs_surname, ' ', u2.usrs_name, ' ', u2.usrs_patronymic) AS recipient, "
												"mail_read, "
												"mail_readdatetime, "
												"mail_departuredatetime, "
												"mail_subject, "
												"mail_important, "
												"mail_message "
												"FROM _email "
												"LEFT JOIN _users u1 ON u1.usrs_id = mail_sender "
												"LEFT JOIN _users u2 ON u2.usrs_id = mail_recipient "
												"WHERE mail_id = :MessageID");
//-----------------------------------------------------------------------------
ISEMailInfoForm::ISEMailInfoForm(int mail_id, QWidget *parent) : ISInterfaceDialogForm(parent)
{
	setWindowTitle(LOCALIZATION("EMailMessageInfo"));
	setWindowIcon(BUFFER_ICONS("EMail.ListForm.All"));

	GetMainLayout()->setContentsMargins(LAYOUT_MARGINS_10_PX);

	FormLayout = new QFormLayout();
	GetMainLayout()->addLayout(FormLayout);

	ISQuery qSelect(QS_EMAIL_MESSAGE);
	qSelect.BindValue(":MessageID", mail_id);
	if (qSelect.ExecuteFirst())
	{
		AddRow(LOCALIZATION("SystemField.ID"), qSelect.ReadColumn("mail_id"));
		AddRow(LOCALIZATION("Sender"), qSelect.ReadColumn("sender"));
		AddRow(LOCALIZATION("Recipient"), qSelect.ReadColumn("recipient"));
		AddRow(LOCALIZATION("Readed"), qSelect.ReadColumn("mail_read"));
		AddRow(LOCALIZATION("ReadedDateTime"), qSelect.ReadColumn("mail_readdatetime").toDateTime().toString(DATE_TIME_FORMAT_V1));
		AddRow(LOCALIZATION("DepartureDateTime"), qSelect.ReadColumn("mail_departuredatetime").toDateTime().toString(DATE_TIME_FORMAT_V1));
		AddRow(LOCALIZATION("Subject"), qSelect.ReadColumn("mail_subject"));
		AddRow(LOCALIZATION("Important"), qSelect.ReadColumn("mail_important"));

		ISTextEdit *TextEdit = new ISTextEdit(this);
		TextEdit->SetReadOnly(true);
		TextEdit->SetVisibleClear(false);
		TextEdit->SetValue(qSelect.ReadColumn("mail_message"));
		FormLayout->addRow(LOCALIZATION("Message") + ":", TextEdit);
	}

	ISPushButton *ButtonClose = new ISPushButton(this);
	ButtonClose->setText(LOCALIZATION("Close"));
	ButtonClose->setIcon(BUFFER_ICONS("Close"));
	connect(ButtonClose, &ISPushButton::clicked, this, &ISEMailInfoForm::close);
	GetMainLayout()->addWidget(ButtonClose, 0, Qt::AlignRight);
}
//-----------------------------------------------------------------------------
ISEMailInfoForm::~ISEMailInfoForm()
{

}
//-----------------------------------------------------------------------------
void ISEMailInfoForm::AddRow(const QString &LabelText, const QVariant &Value)
{
	ISFieldEditBase *FieldEditBase = nullptr;

	if (Value.type() == QVariant::Bool)
	{
		ISCheckEdit *CheckEdit = new ISCheckEdit(this);
		CheckEdit->SetValue(Value);
		CheckEdit->SetReadOnly(true);
		FieldEditBase = CheckEdit;
	}
	else
	{
		ISLineEdit *LineEdit = new ISLineEdit(this);
		LineEdit->SetVisibleClear(false);
		LineEdit->SetReadOnly(true);
		LineEdit->SetValue(Value);
		FieldEditBase = LineEdit;
	}
	
	FormLayout->addRow(LabelText + ":", FieldEditBase);
}
//-----------------------------------------------------------------------------
