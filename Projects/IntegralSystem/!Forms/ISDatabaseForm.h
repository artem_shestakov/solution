#pragma once
//-----------------------------------------------------------------------------
#include "ISInterfaceDialogForm.h"
#include "ISListWidget.h"
//-----------------------------------------------------------------------------
class ISDatabaseForm : public ISInterfaceDialogForm
{
	Q_OBJECT

signals:
	void Selected(const QVariant &DatabaseName);

public:
	ISDatabaseForm(const QString &server, int port, QWidget *parent = 0);
	virtual ~ISDatabaseForm();

protected:
	void ItemDoubleClicked(QListWidgetItem *ListWidgetItem);

private:
	ISListWidget *ListWidget;
};
//-----------------------------------------------------------------------------
