#include "StdAfx.h"
#include "ISTasksStatusListForm.h"
#include "ISMetaData.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISInputDialog.h"
#include "ISQuery.h"
#include "ISMessageBox.h"
//-----------------------------------------------------------------------------
static QString QU_COLOR = PREPARE_QUERY("UPDATE _taskstatus SET tsst_color = :Color WHERE tsst_id = :StatusID");
//-----------------------------------------------------------------------------
static QString QU_COLOR_RESET = PREPARE_QUERY("UPDATE _taskstatus SET tsst_color = NULL WHERE tsst_id = :StatusID");
//-----------------------------------------------------------------------------
ISTasksStatusListForm::ISTasksStatusListForm(QWidget *parent) : ISListBaseForm(ISMetaData::GetInstanse().GetMetaTable("_TaskStatus"), parent)
{
	QAction *ActionSelectColor = new QAction(this);
	ActionSelectColor->setText(LOCALIZATION("SelectColor"));
	ActionSelectColor->setToolTip(LOCALIZATION("SelectColor"));
	ActionSelectColor->setIcon(BUFFER_ICONS("Color"));
	connect(ActionSelectColor, &QAction::triggered, this, &ISTasksStatusListForm::SelectColor);
	AddAction(ActionSelectColor);

	QAction *ActionResetColor = new QAction(this);
	ActionResetColor->setText(LOCALIZATION("ResetColor"));
	ActionResetColor->setToolTip(LOCALIZATION("ResetColor"));
	ActionResetColor->setIcon(BUFFER_ICONS("ResetColor"));
	connect(ActionResetColor, &QAction::triggered, this, &ISTasksStatusListForm::ResetColor);
	AddAction(ActionResetColor);
}
//-----------------------------------------------------------------------------
ISTasksStatusListForm::~ISTasksStatusListForm()
{

}
//-----------------------------------------------------------------------------
void ISTasksStatusListForm::SelectColor()
{
	QString ColorString = ISInputDialog::GetColor(this, LOCALIZATION("SelectColor"), LOCALIZATION("SelectColorFont") + ":", GetCurrentRecordValue("Color")).toString();
	if (ColorString.length())
	{
		ISQuery qUpdate(QU_COLOR);
		qUpdate.BindValue(":Color", ColorString);
		qUpdate.BindValue(":StatusID", GetObjectID());
		qUpdate.Execute();

		Update();
	}
}
//-----------------------------------------------------------------------------
void ISTasksStatusListForm::ResetColor()
{
	if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.ResetColorTaskStatus")))
	{
		ISQuery qUpdate(QU_COLOR_RESET);
		qUpdate.BindValue(":StatusID", GetObjectID());
		if (qUpdate.Execute())
		{
			ISMessageBox::ShowInformation(this, LOCALIZATION("Message.Information.ResetColorTaskStatus"));
			Update();
		}
	}
}
//-----------------------------------------------------------------------------
