#pragma once
//-----------------------------------------------------------------------------
#include "ISObjectFormBase.h"
#include "ISListEdit.h"
//-----------------------------------------------------------------------------
class ISUserGroupAccessObjectForm : public ISObjectFormBase
{
	Q_OBJECT

public:
	Q_INVOKABLE ISUserGroupAccessObjectForm(ISNamespace::ObjectFormType form_type, PMetaClassTable *meta_table, QWidget *parent, int object_id = 0);
	virtual ~ISUserGroupAccessObjectForm();

protected:
	void AfterShowEvent() override;
	void SetCheckAccess(bool Checked);
	void TypeChanged(const QVariant &value);

private:
	ISListEdit *EditTable;
};
//-----------------------------------------------------------------------------
