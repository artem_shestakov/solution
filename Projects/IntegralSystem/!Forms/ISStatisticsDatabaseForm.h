#pragma once
//-----------------------------------------------------------------------------
#include "ISInterfaceMetaForm.h"
//-----------------------------------------------------------------------------
class ISStatisticsDatabaseForm : public ISInterfaceMetaForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISStatisticsDatabaseForm(QWidget *parent = 0);
	virtual ~ISStatisticsDatabaseForm();

	void LoadData() override;

private:
	QVBoxLayout *Layout;
};
//-----------------------------------------------------------------------------
