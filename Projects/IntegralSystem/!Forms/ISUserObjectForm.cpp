#include "StdAfx.h"
#include "ISUserObjectForm.h"
#include "EXConstants.h"
#include "ISControls.h"
#include "ISUsersListForm.h"
#include "ISLocalization.h"
#include "ISMessageBox.h"
#include "ISQuery.h"
#include "ISBuffer.h"
#include "ISUserPasswordForm.h"
#include "ISNotify.h"
//-----------------------------------------------------------------------------
static QString QS_USER = PREPARE_QUERY("SELECT COUNT(*) FROM pg_user WHERE usename = :UserLogin");
//-----------------------------------------------------------------------------
static QString QC_USER = "CREATE ROLE \"%1\" SUPERUSER NOINHERIT LOGIN CONNECTION LIMIT 1";
//-----------------------------------------------------------------------------
static QString QA_LOGIN = "ALTER ROLE %1 RENAME TO %2";
//-----------------------------------------------------------------------------
ISUserObjectForm::ISUserObjectForm(ISNamespace::ObjectFormType form_type, PMetaClassTable *meta_table, QWidget *parent, int object_id) : ISObjectFormBase(form_type, meta_table, parent, object_id)
{
	QAction *ActionChangePassword = ISControls::CreateActionPasswordChange(this);
	connect(ActionChangePassword, &QAction::triggered, this, &ISUserObjectForm::PasswordChange);
	AddAction(ActionChangePassword, true);

	EditLogin = GetFieldWidget("Login");
}
//-----------------------------------------------------------------------------
ISUserObjectForm::~ISUserObjectForm()
{

}
//-----------------------------------------------------------------------------
void ISUserObjectForm::AfterShowEvent()
{
	ISObjectFormBase::AfterShowEvent();

	LoginOld = EditLogin->GetValue().toString();
}
//-----------------------------------------------------------------------------
bool ISUserObjectForm::Save()
{
	bool Result = true;

	if (GetFormType() == ISNamespace::OFT_New)
	{
		Result = ISObjectFormBase::Save();
		if (Result)
		{
			QString UserFullName = GetFieldValue("Surname").toString() + " " + GetFieldValue("Name").toString() + " " + GetFieldValue("Patronymic").toString();
			if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.CreatePasswordUser").arg(UserFullName)))
			{
				QString FullName = GetFieldValue("Surname").toString() + " " + GetFieldValue("Name").toString() + " " + GetFieldValue("Patronymic").toString();
				ISUserPasswordForm UserPasswordForm(GetObjectID(), GetFieldValue("Login").toString(), FullName);
				if (UserPasswordForm.Exec())
				{
					ISMessageBox::ShowInformation(this, LOCALIZATION("Message.Information.CreatedPasswordUser"));
				}
			}
		}
	}
	else
	{
		Result = ISObjectFormBase::Save();
	}

	if (Result)
	{
		ISNotify::GetInstance().SendNotification(CONST_UID_NOTIFY_USER_CHANGED, QVariant(), false, GetObjectID());
	}

	return Result;
}
//-----------------------------------------------------------------------------
void ISUserObjectForm::SaveEvent()
{
	if (GetFormType() == ISNamespace::OFT_New || GetFormType() == ISNamespace::OFT_Copy) //���� ���������� �������� ��� �������� ����� ������������
	{
		ISQuery qCreate;
		if (qCreate.Execute(QC_USER.arg(EditLogin->GetValue().toString())))
		{
			LoginOld = EditLogin->GetValue().toString();
		}
	}
	else if (GetFormType() == ISNamespace::OFT_Edit) //���� ���������� �������������� ������������
	{
		if (LoginOld != EditLogin->GetValue().toString()) //���� ����� ������������ ���������
		{
			ISQuery qAlterLogin;
			if (qAlterLogin.Execute(QA_LOGIN.arg(LoginOld).arg(EditLogin->GetValue().toString())))
			{
				LoginOld = EditLogin->GetValue().toString();
			}
		}
	}
}
//-----------------------------------------------------------------------------
void ISUserObjectForm::PasswordChange()
{
	QString FullName = GetFieldValue("Surname").toString() + " " + GetFieldValue("Name").toString() + " " + GetFieldValue("Patronymic").toString();
	ISUserPasswordForm UserPasswordForm(GetObjectID(), GetFieldValue("Login").toString(), FullName);
	if (UserPasswordForm.Exec())
	{
		ISMessageBox::ShowInformation(this, LOCALIZATION("Message.Information.ChangePasswordUser").arg(FullName));
	}
}
//-----------------------------------------------------------------------------
