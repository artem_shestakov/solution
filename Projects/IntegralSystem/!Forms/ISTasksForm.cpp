#include "StdAfx.h"
#include "ISTasksForm.h"
#include "EXDefines.h"
#include "EXConstants.h"
#include "ISStyleSheet.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISUserRoleEntity.h"
#include "ISTasksAllListForm.h"
#include "ISTasksMyListForm.h"
#include "ISTasksFromMeListForm.h"
#include "ISSystem.h"
#include "ISAssert.h"
#include "ISControls.h"
//-----------------------------------------------------------------------------
ISTasksForm::ISTasksForm(QWidget *parent) : ISParagraphBaseForm(parent)
{
	TasksListForm = nullptr;
	ActionCurrent = nullptr;
	ActionGroup = new QActionGroup(this);

	QHBoxLayout *MainLayout = new QHBoxLayout();
	setLayout(MainLayout);

	QVBoxLayout *LayoutLeft = new QVBoxLayout();
	MainLayout->addLayout(LayoutLeft);

	GroupBox = new QGroupBox(this);
	GroupBox->setTitle(LOCALIZATION("Task.DateLimitFilter"));
	GroupBox->setCheckable(true);
	GroupBox->setChecked(false);
	GroupBox->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
	GroupBox->setLayout(new QVBoxLayout());
	connect(GroupBox, &QGroupBox::toggled, this, &ISTasksForm::GroupBoxToggled);
	LayoutLeft->addWidget(GroupBox);

	CalendarWidget = new ISCalendarWidget(this);
	connect(CalendarWidget, &ISCalendarWidget::DateChanged, this, &ISTasksForm::CalendarDateChanged);
	GroupBox->layout()->addWidget(CalendarWidget);
	
	LayoutLeft->addStretch();

	QVBoxLayout *LayoutRight = new QVBoxLayout();
	MainLayout->addLayout(LayoutRight);

	ToolBar = new QToolBar(this);
	ToolBar->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
	connect(ToolBar, &QToolBar::actionTriggered, this, &ISTasksForm::ActionTriggered);
	LayoutRight->addWidget(ToolBar);

	LayoutRight->addWidget(ISControls::CreateHorizontalLine(this));

	CentralLayout = new QVBoxLayout();
	CentralLayout->addStretch();
	LayoutRight->addLayout(CentralLayout);

	//��� ������
	if (ISUserRoleEntity::GetInstance().CheckPermissionSpecial(CONST_UID_PERMISSION_SPECIAL_TASKS_ALL))
	{
		AddAction(LOCALIZATION("Tasks.All"), LOCALIZATION("Tasks.All"), BUFFER_ICONS("Tasks.All"), "ISTasksAllListForm");
	}

	//��� ������
	if (ISUserRoleEntity::GetInstance().CheckPermissionSpecial(CONST_UID_PERMISSION_SPECIAL_TASKS_MY))
	{
		AddAction(LOCALIZATION("Tasks.ForMe"), LOCALIZATION("Tasks.ForMe"), BUFFER_ICONS("Tasks.ForMe"), "ISTasksMyListForm");
	}

	//������ �� ����
	if (ISUserRoleEntity::GetInstance().CheckPermissionSpecial(CONST_UID_PERMISSION_SPECIAL_TASKS_FROM_ME))
	{
		AddAction(LOCALIZATION("Tasks.FromMe"), LOCALIZATION("Tasks.FromMe"), BUFFER_ICONS("Tasks.FromMe"), "ISTasksFromMeListForm");
	}
}
//-----------------------------------------------------------------------------
ISTasksForm::~ISTasksForm()
{

}
//-----------------------------------------------------------------------------
void ISTasksForm::GroupBoxToggled(bool On)
{
	if (On)
	{
		TasksListForm->GetQueryModel()->SetClassFilter("task_datelimit = :DateLimit");
		TasksListForm->GetQueryModel()->AddCondition(":DateLimit", CalendarWidget->selectedDate());
	}
	else
	{
		TasksListForm->GetQueryModel()->ClearClassFilter();
	}

	TasksListForm->Update();
}
//-----------------------------------------------------------------------------
void ISTasksForm::CalendarDateChanged(const QDate &Date)
{
	TasksListForm->GetQueryModel()->AddCondition(":DateLimit", Date);
	TasksListForm->Update();
}
//-----------------------------------------------------------------------------
void ISTasksForm::ActionTriggered(QAction *ActionClicked)
{
	if (ActionCurrent == ActionClicked)
	{
		return;
	}
	else
	{
		ActionCurrent = ActionClicked;
	}

	ISSystem::SetWaitGlobalCursor(true);

	CentralLayout->removeItem(CentralLayout->itemAt(0));
	if (TasksListForm)
	{
		delete TasksListForm;
		TasksListForm = nullptr;
	}

	QString ListFormName = ActionClicked->data().toString();

	int ObjectType = QMetaType::type((ListFormName + "*").toLocal8Bit().constData());
	IS_ASSERT(ObjectType, "TasksListForm is NULL.");

	const QMetaObject *MetaObject = QMetaType::metaObjectForType(ObjectType);
	TasksListForm = dynamic_cast<ISTasksBaseListForm*>(MetaObject->newInstance(Q_ARG(QWidget *, this)));
	IS_ASSERT(TasksListForm, "TasksListForm not dynamic cast");

	connect(TasksListForm, &ISTasksBaseListForm::AddFormFromTab, this, &ISTasksForm::CreateTaskObjectForm);
	CentralLayout->addWidget(TasksListForm);

	ISSystem::SetWaitGlobalCursor(false);

	TasksListForm->LoadData();
}
//-----------------------------------------------------------------------------
void ISTasksForm::CreateTaskObjectForm(QWidget *TasksObjectForm)
{
	TasksObjectForm->setParent(nullptr);
	TasksObjectForm->show();
	
	ISSystem::MoveWidgetToCenter(TasksObjectForm);
}
//-----------------------------------------------------------------------------
void ISTasksForm::AddAction(const QString &Text, const QString &ToolTip, const QIcon &Icon, const QString &ListFormName)
{
	QAction *Action = new QAction(ToolBar);
	Action->setText(Text);
	Action->setToolTip(ToolTip);
	Action->setIcon(Icon);
	Action->setData(ListFormName);
	Action->setCheckable(true);
	ToolBar->addAction(Action);

	ActionGroup->addAction(Action);
}
//-----------------------------------------------------------------------------
