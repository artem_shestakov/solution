#pragma once
//-----------------------------------------------------------------------------
#include "ISListBaseForm.h"
//-----------------------------------------------------------------------------
class ISTaskFilesListForm : public ISListBaseForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISTaskFilesListForm(QWidget *parent = 0);
	virtual ~ISTaskFilesListForm();

	void LoadData() override;

protected:
	void Create() override;
	void CreateCopy() override;
	void SaveFile();
};
//-----------------------------------------------------------------------------
