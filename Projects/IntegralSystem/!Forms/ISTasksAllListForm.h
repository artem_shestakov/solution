#pragma once
//-----------------------------------------------------------------------------
#include "ISTasksBaseListForm.h"
//-----------------------------------------------------------------------------
class ISTasksAllListForm : public ISTasksBaseListForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISTasksAllListForm(QWidget *parent = 0);
	virtual ~ISTasksAllListForm();

protected:
	void CreateComboUser();

	void OpenTask(); //������� ������
	void CloseTask(); //������� ������
};
//-----------------------------------------------------------------------------
