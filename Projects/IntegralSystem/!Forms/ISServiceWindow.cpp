#include "StdAfx.h"
#include "ISServiceWindow.h"
#include "EXDefines.h"
#include "ISDebug.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISStyleSheet.h"
#include "ISSystem.h"
#include "ISMessageBox.h"
#include "ISConfiguratorBase.h"
#include "EXConstants.h"
#include "ISQueryException.h"
//-----------------------------------------------------------------------------
ISServiceWindow::ISServiceWindow(QWidget *parent) : ISInterfaceForm(parent)
{
	setWindowTitle("IntegralSystem - " + LOCALIZATION("ServiceMode"));
	setWindowIcon(BUFFER_ICONS("ServiceWindow"));

	CreateToolBar();
	CreateTabWidget();
	CreateStatusBar();
}
//-----------------------------------------------------------------------------
ISServiceWindow::~ISServiceWindow()
{

}
//-----------------------------------------------------------------------------
void ISServiceWindow::closeEvent(QCloseEvent *e)
{
	if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.ServiceWindowClose")))
	{
		ISInterfaceForm::closeEvent(e);
	}
	else
	{
		e->ignore();
	}
}
//-----------------------------------------------------------------------------
void ISServiceWindow::CreateToolBar()
{
	ToolBar = new QToolBar(this);
	ToolBar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
	GetMainLayout()->addWidget(ToolBar);

	QDomElement DomElement = ISSystem::GetDomElement(QFile(SERVICE_MODE_SCHEME));
	QDomNode NodeSections = DomElement.firstChild();

	while (!NodeSections.isNull()) //����� ��������
	{
		QString SectionLocalName = NodeSections.attributes().namedItem("LocalName").nodeValue();
		QString SectionIcon = NodeSections.attributes().namedItem("Icon").nodeValue();
		QString SectionClassName = NodeSections.attributes().namedItem("ClassName").nodeValue();

		QAction *ActionSection = AddActionToolBar(SectionLocalName, SectionLocalName, BUFFER_ICONS(SectionIcon));
		ActionSection->setMenu(new QMenu(this));

		QDomNode NodeFunctions = NodeSections.firstChild();
		while (!NodeFunctions.isNull())
		{
			QString FunctionLocalName = NodeFunctions.attributes().namedItem("LocalName").nodeValue();
			QString FunctionName = NodeFunctions.attributes().namedItem("FunctionName").nodeValue();
			QString Description = NodeFunctions.attributes().namedItem("Description").nodeValue();

			QAction *ActionFunction = new QAction(ActionSection->menu());
			ActionFunction->setText(FunctionLocalName);
			ActionFunction->setIcon(BUFFER_ICONS("MainMenuItem"));
			ActionFunction->setProperty("ClassName", SectionClassName);
			ActionFunction->setProperty("FunctionName", FunctionName);
			ActionFunction->setProperty("Description", Description);
			connect(ActionFunction, &QAction::triggered, this, &ISServiceWindow::BeforeExecute);
			connect(ActionFunction, &QAction::triggered, this, &ISServiceWindow::FunctionExecute);
			connect(ActionFunction, &QAction::triggered, this, &ISServiceWindow::AfterExecute);
			ActionSection->menu()->addAction(ActionFunction);

			NodeFunctions = NodeFunctions.nextSibling();
		}

		NodeSections = NodeSections.nextSibling();
	}
}
//-----------------------------------------------------------------------------
void ISServiceWindow::CreateTabWidget()
{
	TabWidget = new QTabWidget(this);
	GetMainLayout()->addWidget(TabWidget);

	TextOutput = new ISTextEdit(TabWidget);
	TextOutput->SetReadOnly(true);
	TextOutput->SetVisibleClear(false);
	TabWidget->addTab(TextOutput, LOCALIZATION("Output"));
}
//-----------------------------------------------------------------------------
void ISServiceWindow::CreateStatusBar()
{
	QWidget *WidgetBottomBar = new QWidget(this);
	WidgetBottomBar->setLayout(new QHBoxLayout());
	GetMainLayout()->addWidget(WidgetBottomBar);

	LabelCurrentTask = new QLabel(WidgetBottomBar);
	LabelCurrentTask->setText(LOCALIZATION("ModeStandby"));
	LabelCurrentTask->setSizePolicy(QSizePolicy::Maximum, LabelCurrentTask->sizePolicy().verticalPolicy());
	WidgetBottomBar->layout()->addWidget(LabelCurrentTask);

	ProgressBar = new QProgressBar(WidgetBottomBar);
	ProgressBar->setSizePolicy(QSizePolicy::Minimum, ProgressBar->sizePolicy().verticalPolicy());
	ProgressBar->setMaximum(100);
	WidgetBottomBar->layout()->addWidget(ProgressBar);

	QStatusBar *StatusBar = new QStatusBar(this);
	GetMainLayout()->addWidget(StatusBar);

	StatusBar->addWidget(new QLabel(LOCALIZATION("Server") + ": " + ISConfig::GetInstance().GetValue(CONST_CONFIG_CONNECTION_SERVER).toString(), this));
	StatusBar->addWidget(new QLabel(LOCALIZATION("Port") + ": " + ISConfig::GetInstance().GetValue(CONST_CONFIG_CONNECTION_PORT).toString(), this));
	StatusBar->addWidget(new QLabel(LOCALIZATION("Database") + ": " + ISConfig::GetInstance().GetValue(CONST_CONFIG_CONNECTION_DATABASE).toString(), this));
}
//-----------------------------------------------------------------------------
QAction* ISServiceWindow::AddActionToolBar(const QString &Text, const QString &ToolTip, const QIcon &Icon)
{
	QAction *Action = new QAction(ToolBar);
	Action->setText(Text);
	Action->setToolTip(ToolTip);
	Action->setIcon(Icon);
	ToolBar->addAction(Action);

	QToolButton *ToolButton = dynamic_cast<QToolButton*>(ToolBar->widgetForAction(Action));
	ToolButton->setCursor(CURSOR_POINTING_HAND);
	ToolButton->setPopupMode(QToolButton::InstantPopup);
	ToolButton->setStyleSheet(STYLE_SHEET("QToolButtonMenu"));

	return Action;
}
//-----------------------------------------------------------------------------
void ISServiceWindow::ProgressChanged(int Step, int StepCount)
{
	ProgressBar->setMaximum(StepCount);
	ProgressBar->setValue(Step);
}
//-----------------------------------------------------------------------------
void ISServiceWindow::BeforeExecute()
{
	connect(&ISDebug::GetInstance(), &ISDebug::Output, TextOutput, &ISTextEdit::AddText);
	
	TextOutput->Clear();
	ToolBar->setEnabled(false);
}
//-----------------------------------------------------------------------------
void ISServiceWindow::FunctionExecute()
{
	QAction *ActionClicked = dynamic_cast<QAction*>(sender());
	if (!ActionClicked)
	{
		ISMessageBox::ShowCritical(this, LOCALIZATION("Message.Error.NotExecuteFunction"));
		return;
	}

	if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.ExecuteFunction").arg(ActionClicked->text()), ActionClicked->property("Description").toString()))
	{
		QString ClassName = ActionClicked->property("ClassName").toString();

		int ObjectType = QMetaType::type(ClassName.toLocal8Bit().constData());
		IS_ASSERT(ObjectType, QString("Invalid object type from class %1").arg(ClassName));

		const QMetaObject *MetaObject = QMetaType::metaObjectForType(ObjectType);
		IS_ASSERT(MetaObject, QString("Not created meta object from class %1").arg(ClassName));

		ISConfiguratorBase *CommandBase = dynamic_cast<ISConfiguratorBase*>(MetaObject->newInstance());
		connect(CommandBase, &ISConfiguratorBase::ProgressSignal, this, &ISServiceWindow::ProgressChanged);
		IS_ASSERT(CommandBase, QString("Invalid instance from class %1").arg(ClassName));

		QString FunctionName = ActionClicked->property("FunctionName").toString();
		QString FunctionLocalName = ActionClicked->text();
		
		LabelCurrentTask->setText(LOCALIZATION("ExecutingTask").arg(FunctionLocalName));
		ISSystem::RepaintWidget(LabelCurrentTask);
		ISSystem::ProcessEvents();

		ISCountingTime TimeWorking;
		bool Invoked = false;

		try
		{
			Invoked = QMetaObject::invokeMethod(CommandBase, FunctionName.toLocal8Bit().constData());
		}
		catch (ISQueryException &e)
		{
			ISMessageBox::ShowCritical(nullptr, QString::fromLocal8Bit(e.what()));
		}

		QString WorkingTime = ISSystem::MillisecondsToString(TimeWorking.GetElapsed());

		if (Invoked)
		{
			ISMessageBox::ShowInformation(this, LOCALIZATION("Configurator.Message.InvokedFunctionDone").arg(FunctionLocalName).arg(WorkingTime));
		}
		else
		{
			ISMessageBox::ShowInformation(this, LOCALIZATION("Configurator.Message.InvokedFunctionError").arg(FunctionLocalName));
		}

		delete CommandBase;
	}
}
//-----------------------------------------------------------------------------
void ISServiceWindow::AfterExecute()
{
	disconnect(&ISDebug::GetInstance(), &ISDebug::Output, TextOutput, &ISTextEdit::AddText);

	LabelCurrentTask->setText(LOCALIZATION("ModeStandby"));
	ProgressBar->setMaximum(100);
	ProgressBar->setValue(0);
	ToolBar->setEnabled(true);
}
//-----------------------------------------------------------------------------
