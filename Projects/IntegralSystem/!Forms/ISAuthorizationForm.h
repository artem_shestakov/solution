#pragma once
//-----------------------------------------------------------------------------
#include "ISInterfaceDialogForm.h"
#include "ISLineEdit.h"
#include "ISPasswordEdit.h"
//-----------------------------------------------------------------------------
class ISAuthorizationForm : public ISInterfaceDialogForm
{
	Q_OBJECT

public:
	ISAuthorizationForm(QWidget *parent = 0);
	virtual ~ISAuthorizationForm();
	
	QString GetEnteredLogin() const; //�������� ��������� �����
	ISNamespace::InputType GetInputType() const;

protected:
	void keyPressEvent(QKeyEvent *e);
	void AfterShowEvent() override;
	void EnterClicked() override;
	void EscapeClicked() override;
	void RadioClicked();

	void ShowConnectionForm(); //�������� ����� �������� �����������
	void ShowAboutForm(); //������� ����� "� ���������"

	void Input(); //���� � ���������
	void InputClient(); //���������� �����
	void InputService(); //��������� �����
	bool Check(); //��������
	void SetConnecting(bool Connecting); //�������� ������� ����������

private:
	ISLineEdit *EditLogin;
	ISPasswordEdit *EditPassword;
	QRadioButton *RadioTypeClient;
	QRadioButton *RadioTypeService;
	QLabel *LabelConnectToDatabase;
	ISPushButton *ButtonInput;
	ISPushButton *ButtonExit;

	ISNamespace::InputType CurrentInputType;
};
//-----------------------------------------------------------------------------
