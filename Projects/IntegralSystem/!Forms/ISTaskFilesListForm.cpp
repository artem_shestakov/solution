#include "StdAfx.h"
#include "ISTaskFilesListForm.h"
#include "ISMetaData.h"
#include "ISFileDialog.h"
#include "ISQuery.h"
#include "ISSystem.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISMessageBox.h"
#include "ISInputDialog.h"
//-----------------------------------------------------------------------------
static QString QI_FILE = PREPARE_QUERY("INSERT INTO _taskfiles(tsfs_task, tsfs_name, tsfs_expansion, tsfs_data) "
									   "VALUES(:TaskID, :Name, :Expansion, :Data)");
//-----------------------------------------------------------------------------
static QString QI_FILE_COPY = PREPARE_QUERY("INSERT INTO _taskfiles(tsfs_task, tsfs_name, tsfs_expansion, tsfs_data) "
											"SELECT tsfs_task, :Name, tsfs_expansion, tsfs_data "
											"FROM _taskfiles "
											"WHERE tsfs_id = :ObjectID "
											"RETURNING tsfs_id");
//-----------------------------------------------------------------------------
static QString QS_FILE = PREPARE_QUERY("SELECT tsfs_data FROM _taskfiles WHERE tsfs_id = :ObjectID");
//-----------------------------------------------------------------------------
ISTaskFilesListForm::ISTaskFilesListForm(QWidget *parent) : ISListBaseForm(ISMetaData::GetInstanse().GetMetaTable("_TaskFiles"), parent)
{
	QAction *ActionSaveFile = new QAction(this);
	ActionSaveFile->setText(LOCALIZATION("SaveFile"));
	ActionSaveFile->setToolTip(LOCALIZATION("SaveFile"));
	ActionSaveFile->setIcon(BUFFER_ICONS("Save"));
	connect(ActionSaveFile, &QAction::triggered, this, &ISTaskFilesListForm::SaveFile);
	AddAction(ActionSaveFile);
}
//-----------------------------------------------------------------------------
ISTaskFilesListForm::~ISTaskFilesListForm()
{

}
//-----------------------------------------------------------------------------
void ISTaskFilesListForm::LoadData()
{
	GetQueryModel()->SetClassFilter(QString("tsfs_task = %1").arg(GetParentObjectID()));
	ISListBaseForm::LoadData();
}
//-----------------------------------------------------------------------------
void ISTaskFilesListForm::Create()
{
	QString FilePath = ISFileDialog::GetOpenFileName(this, QString(), LOCALIZATION("File.Filter.All"));
	if (!FilePath.length())
	{
		return;
	}

	qint64 Kbytes = ISSystem::GetFileSize(FilePath) / 1024;
	if (Kbytes > 30720) //5mb
	{
		ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.TaskFileSizeConstraint"));
		return;
	}


	QFile File(FilePath);
	if (File.open(QIODevice::ReadOnly))
	{
		QByteArray ByteArray = File.readAll();
		File.close();

		ISQuery qInsert(QI_FILE);
		qInsert.BindValue(":TaskID", GetParentObjectID());
		qInsert.BindValue(":Name", ISSystem::GetCompleteBaseFileName(FilePath));
		qInsert.BindValue(":Expansion", QFileInfo(FilePath).suffix());
		qInsert.BindValue(":Data", ByteArray);
		if (qInsert.Execute())
		{
			Update();
		}
	}
	else
	{
		ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Error.NotOpenedFile").arg(FilePath), File.errorString());
	}
}
//-----------------------------------------------------------------------------
void ISTaskFilesListForm::CreateCopy()
{
	if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.CreateCopyTaskFile").arg(GetCurrentRecordValue("Name").toString())))
	{
		QString NewFileName = ISInputDialog::GetString(this, LOCALIZATION("Named"), LOCALIZATION("FileName") + ":", GetCurrentRecordValue("Name")).toString();

		ISQuery qInsert(QI_FILE_COPY);
		qInsert.BindValue(":Name", NewFileName);
		qInsert.BindValue(":ObjectID", GetObjectID());
		if (qInsert.ExecuteFirst())
		{
			SetSelectObjectAfterUpdate(qInsert.ReadColumn("tsfs_id").toInt());
			ISMessageBox::ShowInformation(this, LOCALIZATION("Message.Information.CreateCopyTaskFileDone"));
			Update();
		}
	}
}
//-----------------------------------------------------------------------------
void ISTaskFilesListForm::SaveFile()
{
	if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.SaveFile").arg(GetCurrentRecordValue("Name").toString())))
	{
		QString FilePath = ISFileDialog::GetSaveFileName(this, "*." + GetCurrentRecordValue("Expansion").toString(), GetCurrentRecordValue("Name").toString());
		if (FilePath.length())
		{
			QFile File(FilePath);

			if (File.exists())
			{
				if (!File.remove())
				{
					ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.NotReplacedFile").arg(FilePath), File.errorString());
				}
			}

			if (File.open(QIODevice::WriteOnly))
			{
				ISQuery qSelectFile(QS_FILE);
				qSelectFile.BindValue(":ObjectID", GetObjectID());
				if (qSelectFile.ExecuteFirst())
				{
					QByteArray ByteArray = qSelectFile.ReadColumn("tsfs_data").toByteArray();
					File.write(ByteArray);
					File.close();
				}
			}
			else
			{
				ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Error.NotOpenedFile").arg(FilePath), File.errorString());
			}
		}
	}
}
//-----------------------------------------------------------------------------
