#pragma once
//-----------------------------------------------------------------------------
#include "ISListBaseForm.h"
#include "ISProgressForm.h"
//-----------------------------------------------------------------------------
class ISDistFileListForm : public ISListBaseForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISDistFileListForm(QWidget *parent = 0);
	~ISDistFileListForm();

protected:
	void Create() override;
	bool DeleteCascade() override;
	bool InsertData(int FileID, const QString &Data);
	
	void Actual();
	void Save();
};
//-----------------------------------------------------------------------------
