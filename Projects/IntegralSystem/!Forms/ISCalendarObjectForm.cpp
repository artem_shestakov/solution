#include "StdAfx.h"
#include "ISCalendarObjectForm.h"
#include "ISMetaUser.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISCore.h"
#include "ISSystem.h"
#include "ISMetaData.h"
//-----------------------------------------------------------------------------
ISCalendarObjectForm::ISCalendarObjectForm(ISNamespace::ObjectFormType form_type, PMetaClassTable *meta_table, QWidget *parent, int object_id) : ISObjectFormBase(form_type, meta_table, parent, object_id)
{
	SetVisibleNavigationBar(false);
	SetVisibleSearchPanel(false);
	SetVisibleFieldID(false);
	SetVisibleFavorites(false);
	SetVisibleDelete(false);
	SetVisibleSearch(false);
	SetVisibleReRead(false);

	GetTabWidget()->tabBar()->setAutoHide(true);

	if (GetFieldValue("TableName").isValid())
	{
		QAction *ActionCard = new QAction(this);
		ActionCard->setText(LOCALIZATION("Card"));
		ActionCard->setToolTip(LOCALIZATION("Card"));
		ActionCard->setIcon(BUFFER_ICONS("Document"));
		connect(ActionCard, &QAction::triggered, this, &ISCalendarObjectForm::OpenCard);
		AddAction(ActionCard);
	}
}
//-----------------------------------------------------------------------------
ISCalendarObjectForm::~ISCalendarObjectForm()
{

}
//-----------------------------------------------------------------------------
void ISCalendarObjectForm::SetDate(const QDate &Date)
{
	GetFieldWidget("Date")->SetValue(Date);
}
//-----------------------------------------------------------------------------
void ISCalendarObjectForm::AfterShowEvent()
{
	ISObjectFormBase::AfterShowEvent();
	SetModificationFlag(false);
}
//-----------------------------------------------------------------------------
void ISCalendarObjectForm::EscapeClicked()
{
	close();
}
//-----------------------------------------------------------------------------
void ISCalendarObjectForm::OpenCard()
{
	QString TableName = GetFieldValue("TableName").toString();
	int ObjectID = GetFieldValue("ObjectID").toInt();

	ISObjectFormBase *OrganizationObjectForm = ISCore::CreateObjectForm(ISNamespace::OFT_Edit, ISMetaData::GetInstanse().GetMetaTable(TableName), ObjectID);
	OrganizationObjectForm->show();
}
//-----------------------------------------------------------------------------
