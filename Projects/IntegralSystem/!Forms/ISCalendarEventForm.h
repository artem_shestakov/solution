#pragma once
//-----------------------------------------------------------------------------
#include "ISInterfaceForm.h"
#include "ISComboTimeEdit.h"
//-----------------------------------------------------------------------------
class ISCalendarEventForm : public ISInterfaceForm
{
	Q_OBJECT

signals:
	void ClosedEvent();

public:
	ISCalendarEventForm(int ID, const QString &Name, const QDate &Date, const QTime &Time, const QString &Text, const QString &table_name = QString(), int object_id = 0, QWidget *parent = 0);
	virtual ~ISCalendarEventForm();

protected:
	void closeEvent(QCloseEvent *e);
	void Defer();
	void OpenCard();
	void EventClose(); //Завершение события
	void Flashing();

private:
	int CalendarID;
	QString TableName;
	int ObjectID;

	ISComboTimeEdit *ComboEdit;

	QSound *Sound;
	QTimer *TimerSound;

	bool CloseEvent;
	bool FlashingFlag;
};
//-----------------------------------------------------------------------------
