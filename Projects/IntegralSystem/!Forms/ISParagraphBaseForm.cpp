#include "StdAfx.h"
#include "ISParagraphBaseForm.h"
//-----------------------------------------------------------------------------
ISParagraphBaseForm::ISParagraphBaseForm(QWidget *parent) : QWidget(parent)
{
	PushButton = nullptr;
}
//-----------------------------------------------------------------------------
ISParagraphBaseForm::~ISParagraphBaseForm()
{

}
//-----------------------------------------------------------------------------
void ISParagraphBaseForm::SetButtonParagraph(QPushButton *push_button)
{
	PushButton = push_button;
}
//-----------------------------------------------------------------------------
QPushButton* ISParagraphBaseForm::GetButtonParagraph()
{
	return PushButton;
}
//-----------------------------------------------------------------------------
void ISParagraphBaseForm::Invoke()
{

}
//-----------------------------------------------------------------------------
