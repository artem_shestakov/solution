#include "StdAfx.h"
#include "ISAttachFileForm.h"
#include "EXDefines.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISControls.h"
#include "ISFileDialog.h"
#include "ISAttachFileItem.h"
#include "ISSystem.h"
#include "ISStorageFileLoader.h"
//-----------------------------------------------------------------------------
ISAttachFileForm::ISAttachFileForm(QWidget *parent) : ISInterfaceDialogForm(parent)
{
	setWindowTitle(LOCALIZATION("AddingFiles"));
	resize(SIZE_MAIN_WINDOW_MINIMUM);

	GetMainLayout()->setContentsMargins(LAYOUT_MARGINS_10_PX);

	QHBoxLayout *Layout = new QHBoxLayout();
	GetMainLayout()->addLayout(Layout);

	ButtonSelect = new ISPushButton(this);
	ButtonSelect->setText(LOCALIZATION("ListForm.Select") + "...");
	connect(ButtonSelect, &ISPushButton::clicked, this, &ISAttachFileForm::SelectFiles);
	Layout->addWidget(ButtonSelect);

	ButtonClean = new ISPushButton(this);
	ButtonClean->setEnabled(false);
	ButtonClean->setText(LOCALIZATION("Clear"));
	connect(ButtonClean, &ISPushButton::clicked, this, &ISAttachFileForm::Clean);
	Layout->addWidget(ButtonClean);

	Layout->addStretch();

	ListWidget = new ISListWidget(this);
	GetMainLayout()->addWidget(ListWidget);

	LabelFiles = new QLabel(this);
	LabelFiles->setText(LOCALIZATION("FilesCount") + ": 0");
	GetMainLayout()->addWidget(LabelFiles, 0, Qt::AlignLeft);

	GetMainLayout()->addWidget(ISControls::CreateHorizontalLine(this));

	ProgressFiles = new QProgressBar(this);
	ProgressFiles->setValue(0);
	GetMainLayout()->addWidget(ProgressFiles);

	ProgressFile = new QProgressBar(this);
	ProgressFile->setValue(0);
	GetMainLayout()->addWidget(ProgressFile);

	GetMainLayout()->addWidget(ISControls::CreateHorizontalLine(this));

	QHBoxLayout *LayoutBottom = new QHBoxLayout();
	LayoutBottom->addStretch();
	GetMainLayout()->addLayout(LayoutBottom);

	ButtonStart = new ISPushButton(this);
	ButtonStart->setEnabled(false);
	ButtonStart->setText(LOCALIZATION("Download"));
	connect(ButtonStart, &ISPushButton::clicked, this, &ISAttachFileForm::StartDownload);
	LayoutBottom->addWidget(ButtonStart);

	ButtonStop = new ISPushButton(this);
	ButtonStop->setEnabled(false);
	ButtonStop->setText(LOCALIZATION("Stop"));
	connect(ButtonStop, &ISPushButton::clicked, this, &ISAttachFileForm::StopDownload);
	LayoutBottom->addWidget(ButtonStop);

	ButtonClose = new ISPushButton(this);
	ButtonClose->setText(LOCALIZATION("Close"));
	ButtonClose->setIcon(BUFFER_ICONS("Close"));
	connect(ButtonClose, &ISPushButton::clicked, this, &ISAttachFileForm::close);
	LayoutBottom->addWidget(ButtonClose);

	connect(this, &ISAttachFileForm::RemoveBeginItem, ListWidget, &ISListWidget::RemoveBeginItem);
}
//-----------------------------------------------------------------------------
ISAttachFileForm::~ISAttachFileForm()
{

}
//-----------------------------------------------------------------------------
void ISAttachFileForm::UpdateButtons()
{
	int Count = ListWidget->count();
	if (Count)
	{
		ButtonClean->setEnabled(true);
		ButtonStart->setEnabled(true);

		ProgressFiles->setMaximum(Count);
	}
	else
	{
		ButtonClean->setEnabled(false);
		ButtonStart->setEnabled(false);
	}

	LabelFiles->setText(LOCALIZATION("FilesCount") + ": " + QString::number(Count));
}
//-----------------------------------------------------------------------------
void ISAttachFileForm::SelectFiles()
{
	QStringList StringList = ISFileDialog::GetOpenFilesName(this);

	ISSystem::SetWaitGlobalCursor(true);

	for (int i = 0; i < StringList.count(); i++)
	{
		ISAttachFileItem *FileItem = new ISAttachFileItem(this);
		FileItem->SetFilePath(StringList.at(i));
		connect(FileItem, &ISAttachFileItem::Delete, this, &ISAttachFileForm::Delete);

		QListWidgetItem *ListWidgetItem = new QListWidgetItem(ListWidget);
		ListWidgetItem->setSizeHint(FileItem->sizeHint());
		ListWidget->setItemWidget(ListWidgetItem, FileItem);

		FileItem->SetItemIndex(ListWidget->row(ListWidgetItem));

		ISSystem::ProcessEvents();
	}

	UpdateButtons();

	ISSystem::SetWaitGlobalCursor(false);
}
//-----------------------------------------------------------------------------
void ISAttachFileForm::Clean()
{
	ISSystem::SetWaitGlobalCursor(true);

	while (ListWidget->count())
	{
		emit RemoveBeginItem();
	}

	UpdateButtons();
	ISSystem::SetWaitGlobalCursor(false);
}
//-----------------------------------------------------------------------------
void ISAttachFileForm::Delete()
{
	ISAttachFileItem *FileItem = dynamic_cast<ISAttachFileItem*>(sender());
	
	QListWidgetItem *ListWidgetItem = ListWidget->item(FileItem->GetItemIndex());
	ListWidget->setItemWidget(ListWidgetItem, nullptr);

	delete FileItem;
	FileItem = nullptr;

	delete ListWidgetItem;
	ListWidgetItem = nullptr;

	UpdateButtons();
}
//-----------------------------------------------------------------------------
void ISAttachFileForm::StartDownload()
{
	ButtonStart->setEnabled(false);
	ButtonStop->setEnabled(true);
	ButtonClose->setEnabled(false);

	while (ListWidget->count())
	{
		QListWidgetItem *ListWidgetItem = ListWidget->item(0);
		ISAttachFileItem *FileItem = dynamic_cast<ISAttachFileItem*>(ListWidget->itemWidget(ListWidgetItem));
		QString FilePath = FileItem->GetFilePath();
		FileItem->SetFilePath(FileItem->GetFilePath() + " " + LOCALIZATION("Loaded") + "...");
		FileItem->SetFont(FONT_APPLICATION_BOLD);
		ISSystem::ProcessEvents();

		ISStorageFileLoader Loader(FilePath, this);
		connect(&Loader, &ISStorageFileLoader::ChangeProgressFileMaximum, ProgressFile, &QProgressBar::setMaximum);
		connect(&Loader, &ISStorageFileLoader::LoadingFile, [=]	{ ProgressFile->setValue(ProgressFile->value() + 1); });
		Loader.Load();

		emit RemoveBeginItem();

		ProgressFiles->setValue(ProgressFiles->value() + 1);
		ISSystem::ProcessEvents();
	}

	ButtonStop->setEnabled(false);
	ButtonClose->setEnabled(true);
	
	ProgressFile->setValue(0);
	ProgressFiles->setValue(0);
	UpdateButtons();

	emit UpdateList();
}
//-----------------------------------------------------------------------------
void ISAttachFileForm::StopDownload()
{

}
//-----------------------------------------------------------------------------
