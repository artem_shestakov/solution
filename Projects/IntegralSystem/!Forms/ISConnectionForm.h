#pragma once
//-----------------------------------------------------------------------------
#include "ISInterfaceDialogForm.h"
#include "ISIPAddressEdit.h"
#include "ISIntegerEdit.h"
#include "ISDatabaseEdit.h"
#include "ISPathEditFile.h"
#include "ISCheckEdit.h"
#include "ISPasswordEdit.h"
#include "ISPushButton.h"
//-----------------------------------------------------------------------------
class ISConnectionForm : public ISInterfaceDialogForm
{
	Q_OBJECT

public:
	ISConnectionForm(QWidget *parent = 0);
	virtual ~ISConnectionForm();

protected:
	void AfterShowEvent() override;
	void EnterClicked() override;
	void SaveSettings(); //����������� ���������� ��������
	void TestConnection(); //�������� ����������
	bool CheckFields(); //�������� �� ���������� �����

private:
	ISIPAddressEdit *EditServer;
	ISIntegerEdit *EditPort;
	ISDatabaseEdit *EditDatabase;
	ISPathEditFile *EditModule;
	
	QGroupBox *GroupBoxInput;
	ISLineEdit *EditLogin;
	ISPasswordEdit *EditPassword;

	ISCheckEdit *CheckAutoboot;

	ISPushButton *ButtonTestConnection;
	ISPushButton *ButtonSave;
};
//-----------------------------------------------------------------------------
