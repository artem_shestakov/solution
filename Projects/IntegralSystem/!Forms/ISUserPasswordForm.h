#pragma once
//-----------------------------------------------------------------------------
#include "ISInterfaceDialogForm.h"
#include "ISLineEdit.h"
#include "ISButtonDialog.h"
//-----------------------------------------------------------------------------
class ISUserPasswordForm : public ISInterfaceDialogForm
{
	Q_OBJECT

public:
	ISUserPasswordForm(int user_id, const QString &login, const QString &full_name, QWidget *parent = 0);
	virtual ~ISUserPasswordForm();

protected:
	void EnterClicked() override;
	void ChangePassword();
	void PasswordChecked();
	void SetIconAndTextHeader(const QIcon &Icon, const QString &Text);

private:
	QLabel *LabelIcon;
	QLabel *LabelText;

	ISLineEdit *EditPassword;
	ISLineEdit *EditPasswordCheck;
	ISButtonDialog *ButtonDialog;

	int UserID;
	QString Login;
};
//-----------------------------------------------------------------------------
