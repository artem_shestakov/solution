#include "StdAfx.h"
#include "ISCalendarForm.h"
#include "EXDefines.h"
#include "ISBuffer.h"
#include "ISLocalization.h"
#include "ISControls.h"
#include "ISSystem.h"
#include "ISMetaData.h"
#include "ISCore.h"
#include "ISCalendarObjectForm.h"
#include "ISQuery.h"
#include "ISMetaUser.h"
#include "ISMessageBox.h"
#include "ISCalendarEventForm.h"
#include "ISInputDialog.h"
#include "ISDatabase.h"
#include "ISAssert.h"
#include "ISMemoryObjects.h"
//-----------------------------------------------------------------------------
static QString QS_CALENDAR = PREPARE_QUERY("SELECT cldr_id, cldr_date, cldr_timealert, cldr_name, cldr_text, cldr_closed "
										   "FROM _calendar "
										   "WHERE NOT cldr_isdeleted "
										   "AND cldr_user = :UserID "
										   "AND cldr_date = :Date "
										   "ORDER BY cldr_id DESC");
//-----------------------------------------------------------------------------
static QString QD_CALENDAR = PREPARE_QUERY("DELETE FROM _calendar WHERE cldr_id = :CalendarID");
//-----------------------------------------------------------------------------
static QString QS_CALENDAR_EVENT = PREPARE_QUERY("SELECT cldr_id, cldr_date, cldr_timealert, cldr_name, cldr_text, cldr_tablename, cldr_objectid "
												 "FROM _calendar "
												 "WHERE NOT cldr_isdeleted "
												 "AND cldr_user = :UserID "
												 "AND cldr_date = CURRENT_DATE "
												 "AND NOT cldr_closed")
//-----------------------------------------------------------------------------
static QString QS_CALENDAR_OVERDUE = PREPARE_QUERY("SELECT cldr_id, cldr_name, cldr_date, cldr_timealert, cldr_text, cldr_tablename, cldr_objectid "
												   "FROM _calendar "
												   "WHERE cldr_user = :UserID "
												   "AND NOT cldr_closed "
												   "AND cldr_date <= now()::DATE "
												   "AND cldr_timealert < now()::TIME "
												   "ORDER BY cldr_id");
//-----------------------------------------------------------------------------
static QString QS_CALENDAR_SEARCH = PREPARE_QUERY("SELECT cldr_id, cldr_date, cldr_timealert, cldr_name, cldr_text, cldr_closed "
												  "FROM _calendar "
												  "WHERE NOT cldr_isdeleted "
												  "AND cldr_user = :UserID "
												  "ORDER BY cldr_id DESC");
//-----------------------------------------------------------------------------
static QString QS_CALENDAR_EVENT_DATE = PREPARE_QUERY("SELECT cldr_date FROM _calendar WHERE cldr_id = :CalendarID");
//-----------------------------------------------------------------------------
ISCalendarForm::ISCalendarForm(QWidget *parent) : ISParagraphBaseForm(parent)
{
	FutureWatcher = new QFutureWatcher<void>(this);

	ISMemoryObjects::GetInstance().SetCalendarForm(this);
	connect(this, &ISCalendarForm::ShowEvent, this, &ISCalendarForm::ShowEventForm);

	MainLayout = new QHBoxLayout();
	setLayout(MainLayout);

	QVBoxLayout *LayoutCentral = new QVBoxLayout();
	MainLayout->addLayout(LayoutCentral);

	QToolBar *ToolBar = new QToolBar(this);
	ToolBar->setToolButtonStyle(Qt::ToolButtonStyle::ToolButtonTextBesideIcon);
	LayoutCentral->addWidget(ToolBar);

	QWidget *Spacer = new QWidget(ToolBar);
	Spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	ToolBar->addWidget(Spacer);

	ActionCreate = ISControls::CreateActionCreate(ToolBar);
	ActionCreate->setText(LOCALIZATION("CreateEvent"));
	ActionCreate->setToolTip(LOCALIZATION("CreateNewEventCalendar"));
	connect(ActionCreate, &QAction::triggered, this, &ISCalendarForm::Create);
	ToolBar->addAction(ActionCreate);

	ActionDateTo = new QAction(ToolBar);
	ActionDateTo->setText(LOCALIZATION("CalendarDateTo"));
	ActionDateTo->setToolTip(LOCALIZATION("CalendarDateTo"));
	ActionDateTo->setIcon(BUFFER_ICONS("CalendarMain.DateTo"));
	connect(ActionDateTo, &QAction::triggered, this, &ISCalendarForm::DateTo);
	ToolBar->addAction(ActionDateTo);

	ActionToday = new QAction(ToolBar);
	ActionToday->setText(LOCALIZATION("ToCurrentDate"));
	ActionToday->setToolTip(LOCALIZATION("ToCurrentDate"));
	ActionToday->setIcon(BUFFER_ICONS("CalendarMain.ToCurrentDate"));
	connect(ActionToday, &QAction::triggered, this, &ISCalendarForm::ToCurrentDate);
	ToolBar->addAction(ActionToday);

	ActionEdit = ISControls::CreateActionEdit(this);
	ActionEdit->setEnabled(false);
	connect(ActionEdit, &QAction::triggered, this, &ISCalendarForm::Edit);

	ActionDelete = ISControls::CreateActionDelete(this);
	ActionDelete->setText(LOCALIZATION("Delete"));
	ActionDelete->setToolTip(LOCALIZATION("Delete"));
	ActionDelete->setEnabled(false);
	connect(ActionDelete, &QAction::triggered, this, &ISCalendarForm::Delete);

	ActionDateEvent = new QAction(this);
	ActionDateEvent->setText(LOCALIZATION("OnDateEvent"));
	ActionDateEvent->setIcon(BUFFER_ICONS("Arrow.Left"));
	ActionDateEvent->setEnabled(false);
	connect(ActionDateEvent, &QAction::triggered, this, &ISCalendarForm::DateEvent);

	CalendarPanel = new ISCalendarPanel(this);
	connect(CalendarPanel, &ISCalendarPanel::selectionChanged, this, &ISCalendarForm::SelectedDateChanged);
	LayoutCentral->addWidget(CalendarPanel);

	QVBoxLayout *LayoutRight = new QVBoxLayout();
	QWidget *Panel = new QWidget(this);
	Panel->setLayout(LayoutRight);
	MainLayout->addWidget(Panel);

	SelectedDayWidget = new ISCalendarDayWidget(Panel);
	LayoutRight->addWidget(SelectedDayWidget);

	EditSearch = new ISLineEdit(this);
	EditSearch->SetVisibleClearStandart(true);
	EditSearch->SetVisibleClear(false);
	EditSearch->SetPlaceholderText(LOCALIZATION("EnteringSearchQuery"));
	connect(EditSearch, &ISLineEdit::ValueChange, this, &ISCalendarForm::SearchChanged);
	LayoutRight->addWidget(EditSearch);

	GroupBox = new QGroupBox(this);
	GroupBox->setTitle(LOCALIZATION("Events").arg(CalendarPanel->selectedDate().toString(DATE_FORMAT_STRING_V1)));
	GroupBox->setLayout(new QVBoxLayout());
	LayoutRight->addWidget(GroupBox);

	ListWidget = new ISListWidget(GroupBox);
	ListWidget->setFrameShape(QFrame::NoFrame);
	ListWidget->setContextMenuPolicy(Qt::ActionsContextMenu);
	connect(ListWidget, &QListWidget::itemDoubleClicked, this, &ISCalendarForm::ItemDoubleClicked);
	connect(ListWidget, &QListWidget::itemSelectionChanged, this, &ISCalendarForm::ItemSelectionChanged);
	GroupBox->layout()->addWidget(ListWidget);

	ListWidget->addAction(ActionEdit);
	ListWidget->addAction(ActionDelete);
	ListWidget->addAction(ActionDateEvent);

	CalendarPanel->selectionChanged();

	QTimer::singleShot(2000, this, &ISCalendarForm::ShowOverdueEvents);

	Database = QSqlDatabase::cloneDatabase(ISDatabase::GetInstance().GetDefaultDB(), "CalendarDB");

	Timer = new QTimer(this);
	Timer->setInterval(30000);
	connect(Timer, &QTimer::timeout, this, &ISCalendarForm::TimerTick);
	Timer->start();
}
//-----------------------------------------------------------------------------
ISCalendarForm::~ISCalendarForm()
{

}
//-----------------------------------------------------------------------------
void ISCalendarForm::UpdateCurrentMount()
{
	CalendarPanel->UpdateCells();
	SelectedDateChanged();
}
//-----------------------------------------------------------------------------
void ISCalendarForm::ShowOverdueEvents()
{
	ISQuery qSelect(QS_CALENDAR_OVERDUE);
	qSelect.BindValue(":UserID", CURRENT_USER_ID);
	if (qSelect.Execute())
	{
		while (qSelect.Next())
		{
			int CalendarID = qSelect.ReadColumn("cldr_id").toInt();
			QString Name = qSelect.ReadColumn("cldr_name").toString();
			QDate Date = qSelect.ReadColumn("cldr_date").toDate();
			QTime TimeAlert = qSelect.ReadColumn("cldr_timealert").toTime();
			QString Text = qSelect.ReadColumn("cldr_text").toString();
			QString TableName = qSelect.ReadColumn("cldr_tablename").toString();
			int ObjectID = qSelect.ReadColumn("cldr_objectid").toInt();

			ShowEventForm(CalendarID, Name, Date, TimeAlert, Text, TableName, ObjectID);
		}
	}
}
//-----------------------------------------------------------------------------
void ISCalendarForm::SelectedDateChanged()
{
	QDate Date = CalendarPanel->selectedDate();
	SelectedDayWidget->SetSelectedDate(Date);

	EditSearch->Clear();
	ListWidget->Clear();

	ISQuery qSelect(QS_CALENDAR);
	qSelect.BindValue(":UserID", CURRENT_USER_ID);
	qSelect.BindValue(":Date", Date);
	if (qSelect.Execute())
	{
		while (qSelect.Next())
		{
			int ID = qSelect.ReadColumn("cldr_id").toInt();
			QTime TimeAlert = qSelect.ReadColumn("cldr_timealert").toTime();
			QString Name = qSelect.ReadColumn("cldr_name").toString();
			QString Text = qSelect.ReadColumn("cldr_text").toString();
			bool Closed = qSelect.ReadColumn("cldr_closed").toBool();

			AddEventItem(ID, Name, Text, TimeAlert, Closed);
		}
	}

	GroupBox->setTitle(LOCALIZATION("Events").arg(CalendarPanel->selectedDate().toString(DATE_FORMAT_STRING_V1)));
}
//-----------------------------------------------------------------------------
void ISCalendarForm::Create()
{
	ISCalendarObjectForm *CalendarObjectForm = dynamic_cast<ISCalendarObjectForm*>(ISCore::CreateObjectForm(ISNamespace::OFT_New, ISMetaData::GetInstanse().GetMetaTable("_Calendar")));
	CalendarObjectForm->SetDate(CalendarPanel->selectedDate());
	connect(CalendarObjectForm, &ISCalendarObjectForm::UpdateList, this, &ISCalendarForm::SelectedDateChanged);
	connect(CalendarObjectForm, &ISCalendarObjectForm::UpdateList, CalendarPanel, &ISCalendarPanel::UpdateCells);
	CalendarObjectForm->show();
}
//-----------------------------------------------------------------------------
void ISCalendarForm::DateTo()
{
	QVariant Date = ISInputDialog::GetDate(this, LOCALIZATION("CalendarDateTo"), LOCALIZATION("SelectDate"));
	if (Date.isValid())
	{
		CalendarPanel->setSelectedDate(Date.toDate());
	}
}
//-----------------------------------------------------------------------------
void ISCalendarForm::ToCurrentDate()
{
	CalendarPanel->Today();
}
//-----------------------------------------------------------------------------
void ISCalendarForm::Edit()
{
	ISCalendarEventItem *EventItem = dynamic_cast<ISCalendarEventItem*>(ListWidget->itemWidget(ListWidget->currentItem()));
	if (EventItem)
	{
		if (!EventItem->GetClosed())
		{
			EditEvent(EventItem->GetCalendarID());
		}
	}
}
//-----------------------------------------------------------------------------
void ISCalendarForm::Delete()
{
	ISCalendarEventItem *EventItem = dynamic_cast<ISCalendarEventItem*>(ListWidget->itemWidget(ListWidget->currentItem()));
	if (EventItem)
	{
		DeleteEvent(EventItem->GetCalendarID());
	}
}
//-----------------------------------------------------------------------------
void ISCalendarForm::DateEvent()
{
	ISCalendarEventItem *EventItem = dynamic_cast<ISCalendarEventItem*>(ListWidget->itemWidget(ListWidget->currentItem()));
	if (EventItem)
	{
		ISQuery qSelect(QS_CALENDAR_EVENT_DATE);
		qSelect.BindValue(":CalendarID", EventItem->GetCalendarID());
		if (qSelect.ExecuteFirst())
		{
			QDate Date = qSelect.ReadColumn("cldr_date").toDate();
			CalendarPanel->setSelectedDate(Date);
		}
	}
}
//-----------------------------------------------------------------------------
void ISCalendarForm::ItemDoubleClicked(QListWidgetItem *ListWidgetItem)
{
	ISCalendarEventItem *EventItem = dynamic_cast<ISCalendarEventItem*>(ListWidget->itemWidget(ListWidgetItem));
	if (!EventItem->GetClosed())
	{
		EditEvent(EventItem->GetCalendarID());
	}
}
//-----------------------------------------------------------------------------
void ISCalendarForm::ItemSelectionChanged()
{
	ISCalendarEventItem *EventItem = dynamic_cast<ISCalendarEventItem*>(ListWidget->itemWidget(ListWidget->currentItem()));
	if (EventItem)
	{
		ActionEdit->setEnabled(!EventItem->GetClosed());
		ActionDelete->setEnabled(true);
		ActionDateEvent->setEnabled(true);
	}
	else
	{
		ActionEdit->setEnabled(false);
		ActionDelete->setEnabled(false);
		ActionDateEvent->setEnabled(false);
	}
}
//-----------------------------------------------------------------------------
void ISCalendarForm::EditEvent(int CalendarID)
{
	ISCalendarObjectForm *CalendarObjectForm = dynamic_cast<ISCalendarObjectForm*>(ISCore::CreateObjectForm(ISNamespace::OFT_Edit, ISMetaData::GetInstanse().GetMetaTable("_Calendar"), CalendarID));
	connect(CalendarObjectForm, &ISCalendarObjectForm::UpdateList, this, &ISCalendarForm::SelectedDateChanged);
	connect(CalendarObjectForm, &ISCalendarObjectForm::UpdateList, CalendarPanel, &ISCalendarPanel::UpdateCells);
	CalendarObjectForm->show();
}
//-----------------------------------------------------------------------------
void ISCalendarForm::DeleteEvent(int CalendarID)
{
	if (ISMessageBox::ShowQuestion(CalendarPanel, LOCALIZATION("Message.Question.DeleteCalendarEvent")))
	{
		ISQuery qDelete(QD_CALENDAR);
		qDelete.BindValue(":CalendarID", CalendarID);
		if (qDelete.Execute())
		{
			CalendarPanel->selectionChanged();
			CalendarPanel->UpdateCells();
		}
	}
}
//-----------------------------------------------------------------------------
void ISCalendarForm::SearchChanged(const QVariant &value)
{
	ListWidget->Clear();

	if (value.isValid())
	{
		GroupBox->setTitle(LOCALIZATION("SearchResults"));
		QString SearchValue = value.toString();

		ISQuery qSelect(QS_CALENDAR_SEARCH);
		qSelect.BindValue(":UserID", CURRENT_USER_ID);
		if (qSelect.Execute())
		{
			while (qSelect.Next())
			{
				int ID = qSelect.ReadColumn("cldr_id").toInt();
				QDate Date = qSelect.ReadColumn("cldr_date").toDate();
				QTime TimeAlert = qSelect.ReadColumn("cldr_timealert").toTime();
				QString Name = qSelect.ReadColumn("cldr_name").toString().toLower();
				QString Text = qSelect.ReadColumn("cldr_text").toString().toLower();
				bool Closed = qSelect.ReadColumn("cldr_closed").toBool();

				if (Name.contains(SearchValue) || Text.contains(SearchValue))
				{
					AddEventItem(ID, Name, Text, TimeAlert, Closed)->SetVisibleDate(Date);
				}
			}
		}
	}
	else
	{
		CalendarPanel->selectionChanged();
	}
}
//-----------------------------------------------------------------------------
void ISCalendarForm::TimerTick()
{
	QFuture<void> Future = QtConcurrent::run(this, &ISCalendarForm::CheckEvents);
	FutureWatcher->setFuture(Future);
}
//-----------------------------------------------------------------------------
void ISCalendarForm::CheckEvents()
{
	IS_ASSERT(!Database.isOpen(), "Connection CalendarDB already opened.");

	if (Database.open())
	{
		ISQuery qSelect(Database, QS_CALENDAR_EVENT);
		qSelect.BindValue(":UserID", CURRENT_USER_ID);
		if (qSelect.Execute())
		{
			while (qSelect.Next())
			{
				QString EventTime = qSelect.ReadColumn("cldr_timealert").toTime().toString(TIME_FORMAT_STRING_V1);
				QString CurrentTime = QTime::currentTime().toString(TIME_FORMAT_STRING_V1);

				if (EventTime == CurrentTime)
				{
					int CalendarID = qSelect.ReadColumn("cldr_id").toInt();
					QDate Date = qSelect.ReadColumn("cldr_date").toDate();
					QTime TimeAlert = qSelect.ReadColumn("cldr_timealert").toTime();
					QString Name = qSelect.ReadColumn("cldr_name").toString();
					QString Text = qSelect.ReadColumn("cldr_text").toString();
					QString TableName = qSelect.ReadColumn("cldr_tablename").toString();
					int ObjectID = qSelect.ReadColumn("cldr_objectid").toInt();

					emit ShowEvent(CalendarID, Name, Date, TimeAlert, Text, TableName, ObjectID);
				}
			}
		}
	}

	if (Database.isOpen())
	{
		Database.close();
	}
}
//-----------------------------------------------------------------------------
ISCalendarEventItem* ISCalendarForm::AddEventItem(int CalendarID, const QString &Name, const QString &Text, const QTime &Time, bool Closed)
{
	ISCalendarEventItem *EventItem = new ISCalendarEventItem(CalendarID, Name, Text, Time, Closed, ListWidget);

	QListWidgetItem *ListWidgetItem = new QListWidgetItem(ListWidget);
	ListWidgetItem->setSizeHint(EventItem->sizeHint());
	ListWidget->setItemWidget(ListWidgetItem, EventItem);

	connect(EventItem, &ISCalendarEventItem::SizeHintChanged, [=]
	{
		ListWidgetItem->setSizeHint(EventItem->sizeHint());
	});

	return EventItem;
}
//-----------------------------------------------------------------------------
void ISCalendarForm::ShowEventForm(int CalendarID, const QString &Name, const QDate &Date, const QTime &Time, const QString &Text, const QString &TableName, int ObjectID)
{
	ISCalendarEventForm *CalendarEventForm = new ISCalendarEventForm(CalendarID, Name, Date, Time, Text, TableName, ObjectID);
	CalendarEventForm->show();
	CalendarEventForm->activateWindow();
	connect(CalendarEventForm, &ISCalendarEventForm::ClosedEvent, [=]
	{
		UpdateCurrentMount();
	});
}
//-----------------------------------------------------------------------------
