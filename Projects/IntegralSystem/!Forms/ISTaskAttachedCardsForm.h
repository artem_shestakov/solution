#pragma once
//-----------------------------------------------------------------------------
#include "ISInterfaceMetaForm.h"
#include "ISListWidget.h"
//-----------------------------------------------------------------------------
class ISTaskAttachedCardsForm : public ISInterfaceMetaForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISTaskAttachedCardsForm(QWidget *parent = 0);
	virtual ~ISTaskAttachedCardsForm();

	void LoadData();

protected:
	void Detach();
	void ItemDoubleClicked(QListWidgetItem *ListWidgetItem);
	void OpenObject(const QString &TableName, int ObjectID);

private:
	ISListWidget *ListWidget;
};
//-----------------------------------------------------------------------------
