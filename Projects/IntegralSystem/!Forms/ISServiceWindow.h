#pragma once
//-----------------------------------------------------------------------------
#include "ISInterfaceForm.h"
#include "ISTextEdit.h"
//-----------------------------------------------------------------------------
class ISServiceWindow : public ISInterfaceForm
{
	Q_OBJECT

public:
	ISServiceWindow(QWidget *parent = 0);
	virtual ~ISServiceWindow();

protected:
	void closeEvent(QCloseEvent *e);
	void CreateToolBar();
	void CreateTabWidget();
	void CreateStatusBar();

	QAction* AddActionToolBar(const QString &Text, const QString &ToolTip, const QIcon &Icon);
	void ProgressChanged(int Step, int StepCount);

	void BeforeExecute(); //�������� ����� ����������� �������
	void FunctionExecute(); //���������� �������
	void AfterExecute(); //�������� ����� ���������� �������

private:
	QToolBar *ToolBar;
	QTabWidget *TabWidget;
	ISTextEdit *TextOutput;
	
	QLabel *LabelCurrentTask;
	QProgressBar *ProgressBar;
};
//-----------------------------------------------------------------------------
