﻿#include "StdAfx.h"
#include "ISAuthorizationForm.h"
#include "ISCore.h"
#include "ISSystem.h"
#include "EXConstants.h"
#include "ISDatabase.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISAboutForm.h"
#include "ISSplashScreen.h"
#include "ISQuery.h"
#include "ISMetaUser.h"
#include "ISConfig.h"
#include "EXDefines.h"
#include "ISAssert.h"
#include "ISConnectionForm.h"
#include "ISMessageBox.h"
#include "ISControls.h"
#include "ISStyleSheet.h"
//-----------------------------------------------------------------------------
ISAuthorizationForm::ISAuthorizationForm(QWidget *parent) : ISInterfaceDialogForm(parent)
{
	CurrentInputType = ISNamespace::IT_Unknown;

	setWindowTitle(LOCALIZATION("InputInSystem"));
	ForbidResize();
	GetMainLayout()->setContentsMargins(LAYOUT_MARGINS_NULL);

	QLabel *LableImage = new QLabel(this);
	LableImage->setScaledContents(true);
	LableImage->setPixmap(BUFFER_PIXMAPS("BannerLogoAuth"));
	GetMainLayout()->addWidget(LableImage);

	QVBoxLayout *Layout = new QVBoxLayout();
	Layout->setContentsMargins(LAYOUT_MARGINS_10_PX);
	GetMainLayout()->addLayout(Layout);

	Layout->addWidget(new QLabel(LOCALIZATION("InputLoginAndPassword") + ":", this));

	EditLogin = new ISLineEdit(this);
	EditLogin->SetPlaceholderText(LOCALIZATION("Login"));
	EditLogin->SetIcon(BUFFER_ICONS("Auth.Login"));
	EditLogin->SetVisibleClear(false);
	Layout->addWidget(EditLogin);

	EditPassword = new ISPasswordEdit(this);
	EditPassword->SetPlaceholderText(LOCALIZATION("Password"));
	EditPassword->SetVisibleGenerate(false);
	EditPassword->SetPasswordCheckeder(false);
	EditPassword->SetIcon(BUFFER_ICONS("Auth.Password"));
	EditPassword->SetVisibleClear(false);
	EditPassword->SetVisibleCheckBox(false);
	Layout->addWidget(EditPassword);

	QHBoxLayout *LayoutType = new QHBoxLayout();
	Layout->addLayout(LayoutType);

	LayoutType->addWidget(new QLabel(LOCALIZATION("Mode") + ":", this));

	RadioTypeClient = new QRadioButton(LOCALIZATION("TypeStartup.User"), this);
	RadioTypeClient->setChecked(true);
	connect(RadioTypeClient, &QRadioButton::clicked, this, &ISAuthorizationForm::RadioClicked);
	LayoutType->addWidget(RadioTypeClient);

	RadioTypeService = new QRadioButton(LOCALIZATION("TypeStartup.Service"), this);
	connect(RadioTypeService, &QRadioButton::clicked, this, &ISAuthorizationForm::RadioClicked);
	LayoutType->addWidget(RadioTypeService);

	LayoutType->addStretch();

	Layout->addSpacerItem(new QSpacerItem(0, 40));

	LabelConnectToDatabase = new QLabel(this);
	Layout->addWidget(LabelConnectToDatabase, 0, Qt::AlignRight);

	QHBoxLayout *LayoutBottom = new QHBoxLayout();
	Layout->addLayout(LayoutBottom);

	ISServiceButton *ButtonMenu = new ISServiceButton(this);
	ButtonMenu->setToolTip(LOCALIZATION("Additionally"));
	ButtonMenu->setIcon(BUFFER_ICONS("Auth.Additionally"));
	ButtonMenu->setCursor(CURSOR_POINTING_HAND);
	ButtonMenu->setMenu(new QMenu(ButtonMenu));
	LayoutBottom->addWidget(ButtonMenu);

	ButtonMenu->menu()->addAction(LOCALIZATION("Form.Authorization.ConnectionSettings"), this, &ISAuthorizationForm::ShowConnectionForm, Qt::Key_F9);
	ButtonMenu->menu()->addAction(LOCALIZATION("Form.Authorization.About"), this, &ISAuthorizationForm::ShowAboutForm, Qt::Key_F1);

	LayoutBottom->addStretch();

	ButtonInput = new ISPushButton(BUFFER_ICONS("Apply.Blue"), LOCALIZATION("Input"), this);
	ButtonInput->setCursor(CURSOR_POINTING_HAND);
	connect(ButtonInput, &ISPushButton::clicked, this, &ISAuthorizationForm::Input);
	LayoutBottom->addWidget(ButtonInput);

	ButtonExit = new ISPushButton(BUFFER_ICONS("Auth.Exit"), LOCALIZATION("Exit"), this);
	ButtonExit->setCursor(CURSOR_POINTING_HAND);
	connect(ButtonExit, &ISPushButton::clicked, this, &ISAuthorizationForm::close);
	LayoutBottom->addWidget(ButtonExit);
}
//-----------------------------------------------------------------------------
ISAuthorizationForm::~ISAuthorizationForm()
{
	
}
//-----------------------------------------------------------------------------
QString ISAuthorizationForm::GetEnteredLogin() const
{
	return EditLogin->GetValue().toString();
}
//-----------------------------------------------------------------------------
ISNamespace::InputType ISAuthorizationForm::GetInputType() const
{
	return CurrentInputType;
}
//-----------------------------------------------------------------------------
void ISAuthorizationForm::keyPressEvent(QKeyEvent *e)
{
	ISInterfaceDialogForm::keyPressEvent(e);

	if (e->key() == Qt::Key_F2)
	{
		RadioTypeClient->setChecked(true);
	}
	else if (e->key() == Qt::Key_F3)
	{
		RadioTypeService->setChecked(true);
	}
}
//-----------------------------------------------------------------------------
void ISAuthorizationForm::AfterShowEvent()
{
	ISInterfaceDialogForm::AfterShowEvent();

	if (ISConfig::GetInstance().GetValue(CONST_CONFIG_AUTOINPUT_INCLUDED).toBool())
	{
		EditLogin->SetValue(ISConfig::GetInstance().GetValue(CONST_CONFIG_AUTOINPUT_LOGIN));
		EditPassword->SetValue(ISConfig::GetInstance().GetValue(CONST_CONFIG_AUTOINPUT_PASSWORD));

		ButtonInput->setFocus();
	}
	else
	{
		EditLogin->SetFocus();
	}
}
//-----------------------------------------------------------------------------
void ISAuthorizationForm::EnterClicked()
{
	Input();
}
//-----------------------------------------------------------------------------
void ISAuthorizationForm::EscapeClicked()
{
	close();
}
//-----------------------------------------------------------------------------
void ISAuthorizationForm::RadioClicked()
{
	QRadioButton *RadioButton = dynamic_cast<QRadioButton*>(sender());
	if (RadioButton)
	{
		if (RadioButton == RadioTypeClient)
		{
			EditLogin->setEnabled(true);
			EditPassword->setEnabled(true);
		}
		else if (RadioButton == RadioTypeService)
		{
			EditLogin->setEnabled(false);
			EditPassword->setEnabled(false);
		}
	}
}
//-----------------------------------------------------------------------------
void ISAuthorizationForm::ShowConnectionForm()
{
	ISConnectionForm ConnectionForm;
	ConnectionForm.Exec();
}
//-----------------------------------------------------------------------------
void ISAuthorizationForm::ShowAboutForm()
{
	ISAboutForm AboutForm;
	AboutForm.Exec();
}
//-----------------------------------------------------------------------------
void ISAuthorizationForm::Input()
{
	if (RadioTypeClient->isChecked())
	{
		InputClient();
	}
	else if (RadioTypeService->isChecked())
	{
		InputService();
	}
}
//-----------------------------------------------------------------------------
void ISAuthorizationForm::InputClient()
{
	ISSystem::SetWaitGlobalCursor(true);

	if (!Check())
	{
		return;
	}

	QString EnteredLogin = EditLogin->GetValue().toString();
	QString EnteredPassword = EditPassword->GetValue().toString();
	QString ErrorConnection;

	SetConnecting(true);

	bool Connected = ISDatabase::GetInstance().ConnectToDefaultDB(EnteredLogin, EnteredPassword, ErrorConnection);
	if (!Connected) //Ошибка подключения к базе данных
	{
		ISSystem::SetWaitGlobalCursor(false);
		SetConnecting(false);

		ISMessageBox MessageBox(QMessageBox::Warning, LOCALIZATION("ErrorConnectionDatabase"), LOCALIZATION("Message.Question.ConnectionError.Reconnect"), QMessageBox::Yes | QMessageBox::No | QMessageBox::Close, this);
		MessageBox.setDetailedText(ErrorConnection);
		MessageBox.setButtonText(QMessageBox::Close, LOCALIZATION("Exit"));
		MessageBox.setDefaultButton(QMessageBox::No);
		MessageBox.adjustSize();
		ISSystem::MoveWidgetToCenter(&MessageBox);
		QMessageBox::StandardButtons ClickedButton = MessageBox.Exec();

		if (ClickedButton == QMessageBox::Yes) //Предложить повтор попытки соединения
		{
			Input();
		}
		else if (ClickedButton == QMessageBox::No) //Отмена
		{
			return;
		}
		else if (ClickedButton == QMessageBox::Close) //Выход из программы
		{
			SetResult(false);
			close();
			return;
		}
	}

	CurrentInputType = ISNamespace::IT_Client;
	ISSystem::SetWaitGlobalCursor(false);
	SetConnecting(false);
	hide();
	SetResult(true);
	close();
}
//-----------------------------------------------------------------------------
void ISAuthorizationForm::InputService()
{
	CurrentInputType = ISNamespace::IT_Service;
	hide();
	SetResult(true);
	close();
}
//-----------------------------------------------------------------------------
bool ISAuthorizationForm::Check()
{
	if (ISConfig::GetInstance().GetValue(CONST_CONFIG_CONNECTION_SERVER).toString().isEmpty())
	{
		ISSystem::SetWaitGlobalCursor(false);
		ISMessageBox::ShowCritical(this, LOCALIZATION("Message.Error.ConnectionSetting.Input.ServerEmpty"));
		return false;
	}

	if (ISConfig::GetInstance().GetValue(CONST_CONFIG_CONNECTION_PORT).toString().isEmpty())
	{
		ISSystem::SetWaitGlobalCursor(false);
		ISMessageBox::ShowCritical(this, LOCALIZATION("Message.Error.ConnectionSetting.Input.PortEmpty"));
		return false;
	}

	if (ISConfig::GetInstance().GetValue(CONST_CONFIG_CONNECTION_DATABASE).toString().isEmpty())
	{
		ISSystem::SetWaitGlobalCursor(false);
		ISMessageBox::ShowCritical(this, LOCALIZATION("Message.Error.ConnectionSetting.Input.DatabaseNameEmpty"));
		return false;
	}

	if (!EditLogin->GetValue().toString().length())
	{
		ISSystem::SetWaitGlobalCursor(false);
		ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Error.Input.LoginEmpty"));
		EditLogin->BlinkRed();
		return false;
	}

	if (!EditPassword->GetValue().toString().length())
	{
		ISSystem::SetWaitGlobalCursor(false);
		ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Error.Input.PasswordEmpty"));
		EditPassword->BlinkRed();
		return false;
	}

	return true;
}
//-----------------------------------------------------------------------------
void ISAuthorizationForm::SetConnecting(bool Connecting)
{
	if (Connecting)
	{
		LabelConnectToDatabase->setText(LOCALIZATION("ConnectToDatabase") + "...");
	}
	else
	{
		LabelConnectToDatabase->setText(QString());
	}

	ButtonInput->setEnabled(!Connecting);
	ISSystem::RepaintWidget(ButtonInput);

	ButtonExit->setEnabled(!Connecting);
	ISSystem::RepaintWidget(ButtonExit);

	ISSystem::RepaintWidget(LabelConnectToDatabase);
	ISSystem::ProcessEvents();
}
//-----------------------------------------------------------------------------
