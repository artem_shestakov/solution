#include "StdAfx.h"
#include "ISTasksFromMeListForm.h"
//-----------------------------------------------------------------------------
ISTasksFromMeListForm::ISTasksFromMeListForm(QWidget *parent) : ISTasksBaseListForm(parent)
{
	GetQueryModel()->SetClassFilter("task_owner = ongetcurrentuserid()");
}
//-----------------------------------------------------------------------------
ISTasksFromMeListForm::~ISTasksFromMeListForm()
{

}
//-----------------------------------------------------------------------------
