#pragma once
//-----------------------------------------------------------------------------
#include "ISMenuBar.h"
//-----------------------------------------------------------------------------
class ISParagraphBaseForm : public QWidget
{
	Q_OBJECT

public:
	ISParagraphBaseForm(QWidget *parent = 0);
	virtual ~ISParagraphBaseForm();

	void SetButtonParagraph(QPushButton *push_button);
	QPushButton *GetButtonParagraph();
	
	virtual void Invoke();

private:
	QPushButton *PushButton;
};
//-----------------------------------------------------------------------------
