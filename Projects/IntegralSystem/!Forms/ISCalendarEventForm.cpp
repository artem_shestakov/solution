#include "StdAfx.h"
#include "ISCalendarEventForm.h"
#include "EXDefines.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISStyleSheet.h"
#include "ISTextEdit.h"
#include "ISQuery.h"
#include "ISPushButton.h"
#include "ISSettings.h"
#include "EXConstants.h"
#include "ISMessageBox.h"
#include "ISControls.h"
#include "ISCore.h"
#include "ISMetaData.h"
#include "ISSystem.h"
//-----------------------------------------------------------------------------
static QString QU_CALENDAR = PREPARE_QUERY("UPDATE _calendar SET cldr_closed = true WHERE cldr_id = :CalendarID");
//-----------------------------------------------------------------------------
ISCalendarEventForm::ISCalendarEventForm(int ID, const QString &Name, const QDate &Date, const QTime &Time, const QString &Text, const QString &table_name, int object_id, QWidget *parent) : ISInterfaceForm(parent)
{
	CalendarID = ID;
	TableName = table_name;
	ObjectID = object_id;
	CloseEvent = true;
	FlashingFlag = false;

	setWindowTitle(LOCALIZATION("Reminder"));
	setWindowIcon(BUFFER_ICONS("Bell"));
	setWindowFlags(Qt::WindowStaysOnTopHint);

	GetMainLayout()->setContentsMargins(LAYOUT_MARGINS_10_PX);

	QHBoxLayout *LayoutInfo = new QHBoxLayout();
	GetMainLayout()->addLayout(LayoutInfo);

	QLabel *LabelImage = new QLabel(this);
	LabelImage->setPixmap(BUFFER_ICONS("Calendar").pixmap(SIZE_32_32));
	LayoutInfo->addWidget(LabelImage);

	QVBoxLayout *Layout = new QVBoxLayout();
	LayoutInfo->addLayout(Layout);

	QLabel *LabelName = new QLabel(this);
	LabelName->setText(Name);
	LabelName->setFont(FONT_TAHOMA_12_BOLD);
	LabelName->setStyleSheet(STYLE_SHEET("QLabel.Color.Gray"));
	Layout->addWidget(LabelName);

	QLabel *LabelDate = new QLabel(this);
	LabelDate->setText(QString("%1: %2 %3 %4").arg(LOCALIZATION("Date")).arg(Date.day()).arg(QDate::longMonthName(Date.month())).arg(Date.year()));
	Layout->addWidget(LabelDate);

	QLabel *LabelTime = new QLabel(this);
	LabelTime->setText(LOCALIZATION("Time") + ": " + Time.toString(TIME_FORMAT_STRING_V1));
	Layout->addWidget(LabelTime);

	LayoutInfo->addStretch();

	QGroupBox *GroupBox = new QGroupBox(this);
	GroupBox->setTitle(LOCALIZATION("Text"));
	GroupBox->setLayout(new QVBoxLayout());
	GetMainLayout()->addWidget(GroupBox);

	ISTextEdit *TextEdit = new ISTextEdit(GroupBox);
	TextEdit->SetValue(Text);
	TextEdit->SetReadOnly(true);
	TextEdit->SetVisibleClear(false);
	GroupBox->layout()->addWidget(TextEdit);

	QHBoxLayout *LayoutBottom = new QHBoxLayout();
	GetMainLayout()->addLayout(LayoutBottom);

	ComboEdit = new ISComboTimeEdit(this);
	LayoutBottom->addWidget(ComboEdit);

	ISPushButton *ButtonDefer = new ISPushButton(this);
	ButtonDefer->setText(LOCALIZATION("Defer"));
	connect(ButtonDefer, &ISPushButton::clicked, this, &ISCalendarEventForm::Defer);
	LayoutBottom->addWidget(ButtonDefer);

	if (TableName.length()) //���� ������� ���� �������
	{
		ISPushButton *ButtonCard = new ISPushButton(this);
		ButtonCard->setText(LOCALIZATION("Card"));
		ButtonCard->setIcon(BUFFER_ICONS("Document"));
		connect(ButtonCard, &ISPushButton::clicked, this, &ISCalendarEventForm::OpenCard);
		LayoutBottom->addWidget(ButtonCard);
	}

	LayoutBottom->addStretch();

	ISPushButton *ButtonClose = new ISPushButton(this);
	ButtonClose->setText(LOCALIZATION("CalendarCloseEvent"));
	ButtonClose->setIcon(BUFFER_ICONS("Close"));
	connect(ButtonClose, &ISPushButton::clicked, this, &ISCalendarEventForm::EventClose);
	LayoutBottom->addWidget(ButtonClose);

	Sound = new QSound(BUFFER_AUDIO("CalendarEvent"), this);
	
	TimerSound = new QTimer(this);
	TimerSound->setInterval(CALENDAR_AUDIO_TIMEOUT);

	if (SETTING_BOOL(CONST_UID_SETTING_SOUND_CALENDAREVENT))
	{
		connect(TimerSound, &QTimer::timeout, Sound, static_cast<void(QSound::*)(void)>(&QSound::play));
		Sound->play();
	}

	TimerSound->start();

	QTimer *TimerFlashing = new QTimer(this);
	TimerFlashing->setInterval(1000);
	connect(TimerFlashing, &QTimer::timeout, this, &ISCalendarEventForm::Flashing);
	TimerFlashing->start();
}
//-----------------------------------------------------------------------------
ISCalendarEventForm::~ISCalendarEventForm()
{

}
//-----------------------------------------------------------------------------
void ISCalendarEventForm::closeEvent(QCloseEvent *e)
{
	if (CloseEvent)
	{
		if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.CalendarEventFormClose")))
		{
			EventClose();
		}
		else
		{
			e->ignore();
			return;
		}
	}

	ISInterfaceForm::closeEvent(e);
}
//-----------------------------------------------------------------------------
void ISCalendarEventForm::Defer()
{
	int Minute = ComboEdit->GetValue().toInt();
	QTime Time = QTime::currentTime().addSecs(60 * Minute);

	ISQuery qUpdate("UPDATE _calendar SET cldr_timealert = :Time WHERE cldr_id = :CalendarID");
	qUpdate.BindValue(":Time", Time);
	qUpdate.BindValue(":CalendarID", CalendarID);
	if (qUpdate.Execute())
	{
		CloseEvent = false;
		emit ClosedEvent();
		close();
	}
}
//-----------------------------------------------------------------------------
void ISCalendarEventForm::OpenCard()
{
	ISObjectFormBase *OrganizationObjectForm = ISCore::CreateObjectForm(ISNamespace::OFT_Edit, ISMetaData::GetInstanse().GetMetaTable(TableName), ObjectID);
	OrganizationObjectForm->show();
}
//-----------------------------------------------------------------------------
void ISCalendarEventForm::EventClose()
{
	ISQuery qClose(QU_CALENDAR);
	qClose.BindValue(":CalendarID", CalendarID);
	if (qClose.Execute())
	{
		CloseEvent = false;
		emit ClosedEvent();
		close();
	}
}
//-----------------------------------------------------------------------------
void ISCalendarEventForm::Flashing()
{
	FlashingFlag = !FlashingFlag;

	if (FlashingFlag)
	{
		ISControls::SetBackgroundColorWidget(this, COLOR_CALENDAR_EVENT_FORM_FLASH);
	}
	else
	{
		ISControls::SetBackgroundColorWidget(this, COLOR_WHITE);
	}
}
//-----------------------------------------------------------------------------
