#include "StdAfx.h"
#include "ISEMailListForm.h"
#include "EXDefines.h"
#include "EXConstants.h"
#include "ISMetaData.h"
#include "ISQuery.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISMessageBox.h"
#include "ISEMailInfoForm.h"
#include "ISEMailReadForm.h"
#include "ISEMail.h"
//-----------------------------------------------------------------------------
static QString QS_MAIL_STATUS = PREPARE_QUERY("SELECT mlst_uid, mlst_name, mlst_icon FROM _emailstatus WHERE NOT mlst_isdeleted ORDER BY mlst_order");
//-----------------------------------------------------------------------------
static QString QS_MAIL_READ = PREPARE_QUERY("SELECT mail_read FROM _email WHERE mail_id = :MailID");
//-----------------------------------------------------------------------------
static QString QU_MAIL_READ = PREPARE_QUERY("UPDATE _email SET mail_read = true, mail_readdatetime = now() WHERE mail_id = :MailID");
//-----------------------------------------------------------------------------
ISEMailListForm::ISEMailListForm(QWidget *parent) : ISListBaseForm(ISMetaData::GetInstanse().GetMetaTable("_EMail"), parent)
{
	ActionSend = new QAction(this);
	ActionSend->setVisible(false);
	ActionSend->setText(LOCALIZATION("Send"));
	ActionSend->setToolTip(LOCALIZATION("Send"));
	ActionSend->setIcon(BUFFER_ICONS("EMail.ListForm.Outgoing"));
	connect(ActionSend, &QAction::triggered, this, &ISEMailListForm::SendMail);
	AddAction(ActionSend, true, true);

	ActionRead = new QAction(this);
	ActionRead->setVisible(false);
	ActionRead->setText(LOCALIZATION("ReadMail"));
	ActionRead->setToolTip(LOCALIZATION("ReadMail"));
	ActionRead->setIcon(BUFFER_ICONS("EMail.Read"));
	connect(ActionRead, &QAction::triggered, this, &ISEMailListForm::ReadMail);
	AddAction(ActionRead, true, true);

	CreateStatusWidget();
}
//-----------------------------------------------------------------------------
ISEMailListForm::~ISEMailListForm()
{

}
//-----------------------------------------------------------------------------
void ISEMailListForm::CreateCopy()
{
	if (GetCurrentStatus() != CONST_UID_EMAIL_STATUS_DRAFTS)
	{
		ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.CreateCopyMessageNotDrafts"));
	}
	else
	{
		ISListBaseForm::CreateCopy();
	}
}
//-----------------------------------------------------------------------------
void ISEMailListForm::Edit()
{
	if (GetCurrentStatus() != CONST_UID_EMAIL_STATUS_DRAFTS)
	{
		if (CurrentStatusUID == CONST_UID_EMAIL_STATUS_INCOMING)
		{
			ISEMailReadForm *EMailReadForm = new ISEMailReadForm(GetObjectID());
			EMailReadForm->show();
		}
		else if (CurrentStatusUID == CONST_UID_EMAIL_STATUS_OUTGOING)
		{
			ISEMailInfoForm EMailInfoForm(GetObjectID());
			EMailInfoForm.Exec();
		}
	}
	else
	{
		ISListBaseForm::Edit();
	}
}
//-----------------------------------------------------------------------------
void ISEMailListForm::Delete()
{
	if (GetCurrentStatus() != CONST_UID_EMAIL_STATUS_DRAFTS)
	{
		ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.DeleteMessageNotDrafts"));
	}
	else
	{
		ISListBaseForm::Delete();
	}
}
//-----------------------------------------------------------------------------
bool ISEMailListForm::DeleteCascade()
{
	if (GetCurrentStatus() != CONST_UID_EMAIL_STATUS_DRAFTS)
	{
		ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.DeleteCascadeMessageNotDrafts"));
		return false;
	}
	
	return ISListBaseForm::DeleteCascade();
}
//-----------------------------------------------------------------------------
void ISEMailListForm::AfterShowEvent()
{
	ISListBaseForm::AfterShowEvent();
	HideField("Read");
	HideField("ReadDateTime");
	HideField("DepartureDateTime");
}
//-----------------------------------------------------------------------------
void ISEMailListForm::CreateStatusWidget()
{
	ListWidgetStatus = new ISListWidget(this);
	ListWidgetStatus->setCursor(CURSOR_POINTING_HAND);
	ListWidgetStatus->setSizePolicy(QSizePolicy::Maximum, ListWidgetStatus->sizePolicy().verticalPolicy());
	GetLayoutTableView()->insertWidget(0, ListWidgetStatus);

	ISQuery qSelectStatus(QS_MAIL_STATUS);
	if (qSelectStatus.Execute())
	{
		if (qSelectStatus.GetCountResultRows())
		{
			while (qSelectStatus.Next())
			{
				ISUuid UID = qSelectStatus.ReadColumn("mlst_uid");
				QString Name = qSelectStatus.ReadColumn("mlst_name").toString();
				QString IconName = qSelectStatus.ReadColumn("mlst_icon").toString();

				QListWidgetItem *ListWidgetItem = new QListWidgetItem(ListWidgetStatus);
				ListWidgetItem->setText(Name);
				ListWidgetItem->setData(Qt::UserRole, UID);
				ListWidgetItem->setIcon(BUFFER_ICONS(IconName));
				ListWidgetItem->setSizeHint(QSize(ListWidgetItem->sizeHint().width(), 30));
			}

			QListWidgetItem *BeginItemWidget = ListWidgetStatus->item(0);
			BeginItemWidget->setFont(FONT_APPLICATION_BOLD);
			ListWidgetStatus->setCurrentItem(BeginItemWidget);
		}
	}

	connect(ListWidgetStatus, &QListWidget::itemSelectionChanged, this, &ISEMailListForm::StatusSelectionChanged);
}
//-----------------------------------------------------------------------------
void ISEMailListForm::StatusSelectionChanged()
{
	for (int i = 0; i < ListWidgetStatus->count(); i++)
	{
		ListWidgetStatus->item(i)->setFont(FONT_APPLICATION);
	}

	QListWidgetItem *CurrentItem = ListWidgetStatus->currentItem();
	CurrentItem->setFont(FONT_APPLICATION_BOLD);

	CurrentStatusUID = CurrentItem->data(Qt::UserRole).toString();
	if (CurrentStatusUID == CONST_UID_EMAIL_STATUS_ALL) //��� ���������
	{
		GetQueryModel()->ClearClassFilter();

		ActionSend->setVisible(false);
		ActionRead->setVisible(false);
		HideField("Read");
		HideField("ReadDateTime");
		HideField("DepartureDateTime");
	}
	else if (CurrentStatusUID == CONST_UID_EMAIL_STATUS_INCOMING) //��������
	{
		GetQueryModel()->SetClassFilter(QString("mail_Recipient = ongetcurrentuserid() AND mail_status = \"%1\"").arg(CONST_UID_EMAIL_STATUS_OUTGOING));

		ActionSend->setVisible(false);
		ActionRead->setVisible(true);
		HideField("Read");
		HideField("ReadDateTime");
		HideField("DepartureDateTime");
	}
	else if (CurrentStatusUID == CONST_UID_EMAIL_STATUS_OUTGOING) //���������
	{
		GetQueryModel()->SetClassFilter(QString("mail_sender = ongetcurrentuserid() AND mail_status = \"%1\"").arg(CurrentStatusUID));

		ActionSend->setVisible(false);
		ActionRead->setVisible(false);
		ShowField("Read");
		ShowField("ReadDateTime");
		ShowField("DepartureDateTime");
	}
	else if (CurrentStatusUID == CONST_UID_EMAIL_STATUS_DRAFTS) //���������
	{
		GetQueryModel()->SetClassFilter(QString("mail_sender = ongetcurrentuserid() AND mail_status = \"%1\"").arg(CurrentStatusUID));

		ActionSend->setVisible(true);
		ActionRead->setVisible(false);
		HideField("Read");
		HideField("ReadDateTime");
		HideField("DepartureDateTime");
	}

	Update();
}
//-----------------------------------------------------------------------------
ISUuid ISEMailListForm::GetTypeDispatch()
{
	return ISUuid(GetCurrentRecordValueDB("TypeDispatch"));
}
//-----------------------------------------------------------------------------
ISUuid ISEMailListForm::GetCurrentStatus()
{
	return ISUuid(GetCurrentRecordValueDB("Status"));
}
//-----------------------------------------------------------------------------
void ISEMailListForm::SendMail()
{
	if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.SendEMailMessage").arg(GetCurrentRecordValue("Recipient").toString())))
	{
		if (ISEMail::SendMail(GetObjectID(), GetCurrentRecordValue("Subject").toString(), GetCurrentRecordValueDB("Recipient").toInt()))
		{
			Update();
		}
	}
}
//-----------------------------------------------------------------------------
void ISEMailListForm::ReadMail()
{
	ISQuery qSelectRead(QS_MAIL_READ);
	qSelectRead.BindValue(":MailID", GetObjectID());
	if (qSelectRead.ExecuteFirst())
	{
		if (!qSelectRead.ReadColumn("mail_read").toBool())
		{
			ISQuery qUpdateReadMail(QU_MAIL_READ);
			qUpdateReadMail.BindValue(":MailID", GetObjectID());
			qUpdateReadMail.Execute();
		}

		ISEMailReadForm *EMailReadForm = new ISEMailReadForm(GetObjectID());
		EMailReadForm->show();
	}
}
//-----------------------------------------------------------------------------
