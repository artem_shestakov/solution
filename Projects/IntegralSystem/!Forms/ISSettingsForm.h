#pragma once
//-----------------------------------------------------------------------------
#include "ISInterfaceDialogForm.h"
#include "ISMetaSettingsGroup.h"
#include "ISFieldEditBase.h"
#include "ISButtonDialog.h"
#include "ISScrollArea.h"
#include "ISListWidget.h"
//-----------------------------------------------------------------------------
class ISSettingsForm : public ISInterfaceDialogForm
{
	Q_OBJECT

public:
	ISSettingsForm(const QString &SettingGroup = QString(), QWidget *parent = 0);
	virtual ~ISSettingsForm();

protected:
	void CreateNavigationPanel(); //�������� ������ ���������
	void CreateCentralPanel(); //�������� ����������� ������
	void CreateSettings();
	void Save();
	void DefaultSettings(); //���������� ��������� �� ���������
	void Export();
	void Import();
	void Restart();
	QListWidgetItem* CreateItemGroup(ISMetaSettingsGroup *MetaGroup);
	void ItemSelectionChanged();
	void LabelRowClicked();
	void DataChanged();

private:
	QHBoxLayout *Layout;
	ISListWidget *ListWidget;
	QVBoxLayout *LayoutCentral;
	QLabel *LabelCurrentGroup;
	QLabel *LabelCurrentGroupHint;
	QTabWidget *TabWidget;
	ISButtonDialog *ButtonDialog;

	QMap<QListWidgetItem*, ISMetaSettingsGroup*> Groups;
	QMap<QString, ISFieldEditBase*> Fields;
};
//-----------------------------------------------------------------------------
