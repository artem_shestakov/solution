#include "StdAfx.h"
#include "ISAboutForm.h"
#include "ISVersionInformer.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "EXDefines.h"
#include "ISSystem.h"
#include "ISConfig.h"
#include "ISMessageBox.h"
#include "ISFileDialog.h"
#include "ISMetaData.h"
#include "ISPlugin.h"
#include "ISPushButton.h"
#include "ISTextEdit.h"
//-----------------------------------------------------------------------------
ISAboutForm::ISAboutForm(QWidget *parent) : ISInterfaceDialogForm(parent)
{
	setWindowTitle(LOCALIZATION("AboutForm.AboutApplication"));
	setMinimumSize(SIZE_MAIN_WINDOW_MINIMUM);
	GetMainLayout()->setContentsMargins(LAYOUT_MARGINS_5_PX);

	QHBoxLayout *Layout = new QHBoxLayout();
	GetMainLayout()->addLayout(Layout);

	QPixmap PixmapLogo = BUFFER_PIXMAPS("Logo");
	QPixmap NewPixmap = PixmapLogo.scaled(QSize(200, 200), Qt::IgnoreAspectRatio, Qt::FastTransformation);
	
	QLabel *LabelImage = new QLabel(this);
	LabelImage->setPixmap(NewPixmap);
	Layout->addWidget(LabelImage);

	LayoutRight = new QVBoxLayout();
	LayoutRight->setContentsMargins(LAYOUT_MARGINS_10_PX);
	Layout->addLayout(LayoutRight);

	TabWidget = new QTabWidget(this);
	LayoutRight->addWidget(TabWidget);

	ISPushButton *ButtonClose = new ISPushButton(this);
	ButtonClose->setText(LOCALIZATION("Close"));
	ButtonClose->setIcon(BUFFER_ICONS("Close"));
	connect(ButtonClose, &ISPushButton::clicked, this, &ISAboutForm::close);
	LayoutRight->addWidget(ButtonClose, 0, Qt::AlignRight);

	CreateCommonTab();
	CreateContactsTab();
	CreateVersionTab();
	CreateModuleTab();
	CreateDescriptionTab();
	CreateLicenseTab();
	CreateOtherTab();
}
//-----------------------------------------------------------------------------
ISAboutForm::~ISAboutForm()
{

}
//-----------------------------------------------------------------------------
void ISAboutForm::CreateCommonTab()
{
	QVBoxLayout *LayoutCommon = new QVBoxLayout();

	QWidget *TabCommon = new QWidget(TabWidget);
	TabCommon->setLayout(LayoutCommon);
	TabWidget->addTab(TabCommon, LOCALIZATION("AboutForm.Tab.Common"));

	AddLabel(TabCommon, LOCALIZATION("AboutForm.Tab.Common.ProductName"), LOCALIZATION("IntegralSystem"));
	AddLabel(TabCommon, LOCALIZATION("AboutForm.Tab.Common.Cofiguration"), ISSystem::GetConfiguration());

	LayoutCommon->addStretch();
}
//-----------------------------------------------------------------------------
void ISAboutForm::CreateContactsTab()
{
	QVBoxLayout *LayoutContacts = new QVBoxLayout();

	QWidget *TabContacts = new QWidget(TabWidget);
	TabContacts->setLayout(LayoutContacts);
	TabWidget->addTab(TabContacts, LOCALIZATION("AboutForm.Tab.Contacts"));

	AddLabel(TabContacts, LOCALIZATION("AboutForm.Tab.Contacts.ProductAuthor"), LOCALIZATION("AboutForm.Tab.Contacts.ProductAuthor.FullName"));
	AddLabel(TabContacts, LOCALIZATION("AboutForm.Tab.Contacts.ProductAuthorPhone"), "8-903-416-10-03");
	AddLabel(TabContacts, LOCALIZATION("AboutForm.Tab.Contacts.Telegram"), "@artem_shestakov");
	AddLabel(TabContacts, LOCALIZATION("AboutForm.Tab.Contacts.WhatsApp"), "8-903-416-10-03");
	AddLabel(TabContacts, LOCALIZATION("AboutForm.Tab.Contacts.ProductAuthorMail"), "artemshestakov@outlook.com");
	AddLabel(TabContacts, LOCALIZATION("AboutForm.Tab.Contacts.Address"), LOCALIZATION("AboutForm.Tab.Contacts.Address.Text"));

	LayoutContacts->addStretch();

	ISPushButton *ButtonSaveInfo = new ISPushButton(TabContacts);
	ButtonSaveInfo->setText(LOCALIZATION("AboutForm.Tab.Contacts.SaveAuthorInformation"));
	ButtonSaveInfo->setIcon(BUFFER_ICONS("Save"));
	connect(ButtonSaveInfo, &ISPushButton::clicked, this, &ISAboutForm::SaveAuthorInfo);
	LayoutContacts->addWidget(ButtonSaveInfo, 0, Qt::AlignRight);
}
//-----------------------------------------------------------------------------
void ISAboutForm::CreateVersionTab()
{
	QVBoxLayout *LayoutVersion = new QVBoxLayout();

	QWidget *TabVersion = new QWidget(TabWidget);
	TabVersion->setLayout(LayoutVersion);
	TabWidget->addTab(TabVersion, LOCALIZATION("AboutForm.Tab.Version"));

	AddLabel(TabVersion, LOCALIZATION("AboutForm.Tab.Version.ProductVersion"), ISVersionInformer::GetVersion());
	AddLabel(TabVersion, LOCALIZATION("AboutForm.Tab.Version.ReleaseDate"), ISVersionInformer::GetDate());
	AddLabel(TabVersion, LOCALIZATION("AboutForm.Tab.Version.ProductVersionRevision"), ISVersionInformer::GetRevision());
	AddLabel(TabVersion, LOCALIZATION("AboutForm.Tab.Version.ProductVersionBuild"), ISVersionInformer::GetBuild());
	AddLabel(TabVersion, LOCALIZATION("AboutForm.Tab.Version.ProductVersionHash"), ISVersionInformer::GetHash());
	AddLabel(TabVersion, LOCALIZATION("AboutForm.Tab.Version.QtVersion"), qVersion());

	LayoutVersion->addStretch();
}
//-----------------------------------------------------------------------------
void ISAboutForm::CreateModuleTab()
{
	if (!ISMetaData::GetInstanse().GetInitialized())
	{
		return;
	}

	QVBoxLayout *LayoutModule = new QVBoxLayout();

	QWidget *TabModule = new QWidget(TabWidget);
	TabModule->setLayout(LayoutModule);
	TabWidget->addTab(TabModule, LOCALIZATION("AboutForm.Tab.Module"));

	AddLabel(TabModule, LOCALIZATION("AboutForm.Tab.Module.Name"), ISPlugin::GetInstance().GetLocalName());
	AddLabel(TabModule, LOCALIZATION("AboutForm.Tab.Module.Path"), ISPlugin::GetInstance().GetPath());
	AddLabel(TabModule, LOCALIZATION("AboutForm.Tab.Module.Size"), ISSystem::FileSizeFromString(ISSystem::GetFileSize(ISPlugin::GetInstance().GetPath())));

	LayoutModule->addStretch();
}
//-----------------------------------------------------------------------------
void ISAboutForm::CreateDescriptionTab()
{
	QVBoxLayout *LayoutDescription = new QVBoxLayout();

	QWidget *TabDescription = new QWidget(TabWidget);
	TabDescription->setLayout(LayoutDescription);
	TabWidget->addTab(TabDescription, LOCALIZATION("AboutForm.Tab.Description"));

	ISTextEdit *TextEdit = new ISTextEdit(TabDescription);
	TextEdit->SetValue(LOCALIZATION("DescriptionApplication"));
	TextEdit->SetReadOnly(true);
	TextEdit->SetVisibleClear(false);
	TabDescription->layout()->addWidget(TextEdit);
}
//-----------------------------------------------------------------------------
void ISAboutForm::CreateLicenseTab()
{
	QVBoxLayout *LayoutLicense = new QVBoxLayout();

	QWidget *TabLicense = new QWidget(TabWidget);
	TabLicense->setLayout(LayoutLicense);
	TabWidget->addTab(TabLicense, LOCALIZATION("AboutForm.Tab.License"));

	ISTextEdit *TextEdit = new ISTextEdit(TabLicense);
	TextEdit->SetReadOnly(true);
	TextEdit->SetVisibleClear(false);
	TabLicense->layout()->addWidget(TextEdit);

	QFile FileLicense(":Licenses/IntegralSystem.txt");
	if (FileLicense.open(QIODevice::ReadOnly))
	{
		TextEdit->SetValue(QString::fromLocal8Bit(FileLicense.readAll()));
		FileLicense.close();
	}
}
//-----------------------------------------------------------------------------
void ISAboutForm::CreateOtherTab()
{
	QVBoxLayout *LayoutOther = new QVBoxLayout();

	QWidget *TabOther = new QWidget(TabWidget);
	TabOther->setLayout(LayoutOther);
	TabWidget->addTab(TabOther, LOCALIZATION("AboutForm.Tab.Other"));

	AddLabel(TabOther, LOCALIZATION("AboutForm.Tab.Other.SizeTemp"), ISSystem::GetSizeDir(APPLICATION_TEMP_PATH));
	AddLabel(TabOther, LOCALIZATION("AboutForm.Tab.Other.SizeLogger"), ISSystem::GetSizeDir(APPLICATION_LOGS_PATH));
	AddLabel(TabOther, LOCALIZATION("AboutForm.Tab.Other.ApplicationDir"), APPLICATION_DIR_PATH);

	LayoutOther->addStretch();
}
//-----------------------------------------------------------------------------
void ISAboutForm::AddLabel(QWidget *parent, const QString &LabelText, const QString &Text)
{
	QHBoxLayout *LayoutRow = new QHBoxLayout();
	LayoutRow->setContentsMargins(LAYOUT_MARGINS_NULL);

	QWidget *WidgetRow = new QWidget(parent);
	WidgetRow->setLayout(LayoutRow);

	QLabel *LabelRow = new QLabel(parent);
	LabelRow->setFont(FONT_APPLICATION_BOLD);
	LabelRow->setText(LabelText + ":");
	LayoutRow->addWidget(LabelRow);

	QLabel *Label = new QLabel(parent);
	Label->setFont(FONT_APPLICATION_BOLD);
	Label->setText(Text);
	LayoutRow->addWidget(Label);

	LayoutRow->addStretch();

	parent->layout()->addWidget(WidgetRow);
}
//-----------------------------------------------------------------------------
void ISAboutForm::SaveAuthorInfo()
{
	QString FilePath = ISFileDialog::GetSaveFileName(this, LOCALIZATION("File.Filter.Txt"));
	if (FilePath.length())
	{
		QFile File(FilePath);
		if (File.open(QIODevice::WriteOnly | QIODevice::Text))
		{
			QString String;
			String += LOCALIZATION("AboutForm.Tab.Contacts.ProductAuthor") + ": " + LOCALIZATION("AboutForm.Tab.Contacts.ProductAuthor.FullName") + "\n";
			String += LOCALIZATION("AboutForm.Tab.Contacts.ProductAuthorPhone") + ": 8-903-416-10-03\n";
			String += LOCALIZATION("AboutForm.Tab.Contacts.Telegram") + ": @artem_shestakov\n";
			String += LOCALIZATION("AboutForm.Tab.Contacts.WhatsApp") + ": 8-903-416-10-03\n";
			String += LOCALIZATION("AboutForm.Tab.Contacts.ProductAuthorMail") + ": artemshestakov@outlook.com";

			File.write(String.toUtf8());
			File.close();

			ISMessageBox::ShowInformation(this, LOCALIZATION("Message.Information.FileSaved"));
		}
	}
}
//-----------------------------------------------------------------------------
