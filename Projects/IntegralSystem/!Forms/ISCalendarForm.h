#pragma once
//-----------------------------------------------------------------------------
#include "ISParagraphBaseForm.h"
#include "ISCalendarPanel.h"
#include "ISCalendarDayWidget.h"
#include "ISCalendarEventItem.h"
#include "ISLineEdit.h"
#include "ISListWidget.h"
//-----------------------------------------------------------------------------
class ISCalendarForm : public ISParagraphBaseForm
{
	Q_OBJECT

signals:
	void ShowEvent(int CalendarID, const QString &Name, const QDate &Date, const QTime &Time, const QString &Text, const QString &TableName, int ObjectID);

public:
	Q_INVOKABLE ISCalendarForm(QWidget *parent = 0);
	virtual ~ISCalendarForm();

public slots:
	void UpdateCurrentMount(); //�������� ������ �������� ������

protected:
	void ShowOverdueEvents(); //����� ������������ �����
	void SelectedDateChanged(); //������� ����� ��������� ����

	void Create(); //�������� ������ ������� ���������
	void DateTo(); //������� �� ���� ���������
	void ToCurrentDate(); //������� �� ������� ����
	void Edit(); //��������� ������� ���������
	void Delete(); //�������� ������� ���������
	void DateEvent(); //������� �� ���� ���������� �������
	
	void ItemDoubleClicked(QListWidgetItem *ListWidgetItem); //������� ������� �� �������
	void ItemSelectionChanged();

	void EditEvent(int CalendarID);
	void DeleteEvent(int CalendarID);

	void SearchChanged(const QVariant &value);
	void TimerTick();
	void CheckEvents();
	ISCalendarEventItem* AddEventItem(int CalendarID, const QString &Name, const QString &Text, const QTime &Time, bool Closed); //�������� ������� ������� � ������
	void ShowEventForm(int CalendarID, const QString &Name, const QDate &Date, const QTime &Time, const QString &Text, const QString &TableName = QString(), int ObjectID = 0); //�������� ����� �������

private:
	QHBoxLayout *MainLayout;
	
	ISCalendarPanel *CalendarPanel;
	ISCalendarDayWidget *SelectedDayWidget;

	QAction *ActionCreate;
	QAction *ActionDateTo;
	QAction *ActionToday;
	QAction *ActionEdit;
	QAction *ActionDelete;
	QAction *ActionDateEvent;

	ISLineEdit *EditSearch;
	QGroupBox *GroupBox;
	ISListWidget *ListWidget;

	QFutureWatcher<void> *FutureWatcher;
	QSqlDatabase Database;
	QTimer *Timer;
};
//-----------------------------------------------------------------------------
