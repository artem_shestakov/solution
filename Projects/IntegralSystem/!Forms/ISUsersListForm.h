#pragma once
//-----------------------------------------------------------------------------
#include "ISListBaseForm.h"
#include "ISProcessForm.h"
//-----------------------------------------------------------------------------
class ISUsersListForm : public ISListBaseForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISUsersListForm(QWidget *parent = 0);
	virtual ~ISUsersListForm();

protected:
	void CreateCopy() override;
	void Edit() override;
	void ChangePassword();

private:
	bool CheckPostgres(); //Проверка редактирования системного пользователя
	bool CheckThisUser(); //Проверка редактирования текущего пользователя
};
//-----------------------------------------------------------------------------
