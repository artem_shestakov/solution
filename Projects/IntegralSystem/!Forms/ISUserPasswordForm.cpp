#include "StdAfx.h"
#include "ISUserPasswordForm.h"
#include "EXDefines.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISMessageBox.h"
#include "ISQuery.h"
#include "ISMetaUser.h"
//-----------------------------------------------------------------------------
static QString QA_PASSWORD = "ALTER ROLE %1 PASSWORD '%2'";
//-----------------------------------------------------------------------------
static QString QI_USER_PASSWORD_CHANGED = PREPARE_QUERY("INSERT INTO _userpasswordchanged(upcg_user, upcg_whouser) "
														"VALUES(:User, :WhoUser)");
//-----------------------------------------------------------------------------
ISUserPasswordForm::ISUserPasswordForm(int user_id, const QString &login, const QString &full_name, QWidget *parent) : ISInterfaceDialogForm(parent)
{
	UserID = user_id;
	Login = login;

	setWindowTitle(LOCALIZATION("ChangePassword"));

	ForbidResize();
	GetMainLayout()->setContentsMargins(LAYOUT_MARGINS_5_PX);

	GetMainLayout()->addWidget(new QLabel(LOCALIZATION("User") + ": " + full_name, this));

	QHBoxLayout *Layout = new QHBoxLayout();
	GetMainLayout()->addLayout(Layout);

	LabelIcon = new QLabel(this);
	LabelIcon->setPixmap(BUFFER_ICONS("Keyboard").pixmap(SIZE_25_25));
	Layout->addWidget(LabelIcon);

	LabelText = new QLabel(LOCALIZATION("EnterThePassword"), this);
	Layout->addWidget(LabelText);

	Layout->addStretch();

	QFormLayout *FormLayout = new QFormLayout();
	GetMainLayout()->addLayout(FormLayout);

	EditPassword = new ISLineEdit(this);
	EditPassword->SetEchoMode(QLineEdit::Password);
	connect(EditPassword, &ISLineEdit::DataChanged, this, &ISUserPasswordForm::PasswordChecked);
	FormLayout->addRow(LOCALIZATION("Password") + ":", EditPassword);

	EditPasswordCheck = new ISLineEdit(this);
	EditPasswordCheck->SetEchoMode(QLineEdit::Password);
	connect(EditPasswordCheck, &ISLineEdit::DataChanged, this, &ISUserPasswordForm::PasswordChecked);
	FormLayout->addRow(LOCALIZATION("PasswordCheck") + ":", EditPasswordCheck);

	ButtonDialog = new ISButtonDialog(this);
	ButtonDialog->SetApplyEnabled(false);
	connect(ButtonDialog, &ISButtonDialog::Apply, this, &ISUserPasswordForm::ChangePassword);
	connect(ButtonDialog, &ISButtonDialog::Close, this, &ISUserPasswordForm::close);
	GetMainLayout()->addWidget(ButtonDialog);
}
//-----------------------------------------------------------------------------
ISUserPasswordForm::~ISUserPasswordForm()
{

}
//-----------------------------------------------------------------------------
void ISUserPasswordForm::EnterClicked()
{
	ChangePassword();
}
//-----------------------------------------------------------------------------
void ISUserPasswordForm::ChangePassword()
{
	ISQuery qAlterPassword;
	if (qAlterPassword.Execute(QA_PASSWORD.arg(Login).arg(EditPasswordCheck->GetValue().toString())))
	{
		ISQuery qInsertPasswordChange(QI_USER_PASSWORD_CHANGED);
		qInsertPasswordChange.BindValue(":User", UserID);
		qInsertPasswordChange.BindValue(":WhoUser", CURRENT_USER_ID);
		if (qInsertPasswordChange.Execute())
		{
			SetResult(true);
			close();
		}
	}
}
//-----------------------------------------------------------------------------
void ISUserPasswordForm::PasswordChecked()
{
	QString Password = EditPassword->GetValue().toString();
	QString PasswordCheck = EditPasswordCheck->GetValue().toString();

	if (Password.length() && PasswordCheck.length())
	{
		if (EditPassword->GetValue().toString() == EditPasswordCheck->GetValue().toString())
		{
			ButtonDialog->SetApplyEnabled(true);
			SetIconAndTextHeader(BUFFER_ICONS("Apply.Green"), LOCALIZATION("PasswordsMatch"));
		}
		else
		{
			ButtonDialog->SetApplyEnabled(false);
			SetIconAndTextHeader(BUFFER_ICONS("Exit"), LOCALIZATION("PasswordsNotMatch"));
		}
	}
	else if (!Password.length() && !PasswordCheck.length())
	{
		ButtonDialog->SetApplyEnabled(false);
		SetIconAndTextHeader(BUFFER_ICONS("Keyboard"), LOCALIZATION("EnterThePassword"));
	}
	else
	{
		ButtonDialog->SetApplyEnabled(false);
		SetIconAndTextHeader(BUFFER_ICONS("Exit"), LOCALIZATION("OneFieldPasswordIsEmpty"));
	}
}
//-----------------------------------------------------------------------------
void ISUserPasswordForm::SetIconAndTextHeader(const QIcon &Icon, const QString &Text)
{
	LabelIcon->setPixmap(Icon.pixmap(SIZE_25_25));
	LabelText->setText(Text);
}
//-----------------------------------------------------------------------------
