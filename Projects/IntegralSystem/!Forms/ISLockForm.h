#pragma once
//-----------------------------------------------------------------------------
#include "ISInterfaceDialogForm.h"
#include "ISPasswordEdit.h"
#include "ISCountingTime.h"
//-----------------------------------------------------------------------------
class ISLockForm : public ISInterfaceDialogForm
{
	Q_OBJECT

signals:
	void Unlocked();

public:
	ISLockForm(QWidget *parent = 0);
	virtual ~ISLockForm();

	void closeEvent(QCloseEvent *e);

protected:
	void Unlock();
	void EnterClicked() override;

private:
	bool Close;
	ISPasswordEdit *EditPassword;
};
//-----------------------------------------------------------------------------
