#include "StdAfx.h"
#include "ISDatabaseForm.h"
#include "EXDefines.h"
#include "ISLocalization.h"
#include "ISDatabase.h"
#include "ISQuery.h"
#include "ISMessageBox.h"
//-----------------------------------------------------------------------------
static QString QS_DATABASES = PREPARE_QUERY("SELECT datname FROM pg_database ORDER BY datname");
//-----------------------------------------------------------------------------
ISDatabaseForm::ISDatabaseForm(const QString &server, int port, QWidget *parent) : ISInterfaceDialogForm(parent)
{
	setWindowTitle(LOCALIZATION("SelectDatabase"));

	ForbidResize();
	GetMainLayout()->setContentsMargins(LAYOUT_MARGINS_5_PX);

	ListWidget = new ISListWidget(this);
	connect(ListWidget, &QListWidget::itemDoubleClicked, this, &ISDatabaseForm::ItemDoubleClicked);
	GetMainLayout()->addWidget(ListWidget);

	QString ErrorString;
	if (ISDatabase::GetInstance().ConnectToSystemDB(ErrorString))
	{
		ISQuery qSelect(ISDatabase::GetInstance().GetSystemDB(), QS_DATABASES);
		if (qSelect.Execute())
		{
			while (qSelect.Next())
			{
				QListWidgetItem *ListWidgetItem = new QListWidgetItem(ListWidget);
				ListWidgetItem->setText(qSelect.ReadColumn("datname").toString());
			}
		}

		ISDatabase::GetInstance().DisconnectFromSystemDB();
	}
	else
	{
		ISMessageBox::ShowWarning(this, ErrorString);
	}
}
//-----------------------------------------------------------------------------
ISDatabaseForm::~ISDatabaseForm()
{

}
//-----------------------------------------------------------------------------
void ISDatabaseForm::ItemDoubleClicked(QListWidgetItem *ListWidgetItem)
{
	emit Selected(ListWidgetItem->text());
	close();
}
//-----------------------------------------------------------------------------
