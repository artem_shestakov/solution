#include "StdAfx.h"
#include "ISProtocolEntersForm.h"
#include "ISLocalization.h"
#include "ISMetaData.h"
//-----------------------------------------------------------------------------
ISProtocolEntersForm::ISProtocolEntersForm(QWidget *parent) : ISListBaseForm(ISMetaData::GetInstanse().GetMetaTable("_ProtocolEnters"), parent)
{
	setSizePolicy(QSizePolicy::Expanding, QSizePolicy::MinimumExpanding);

	GetAction(ISNamespace::AT_SystemInfo)->setVisible(false);
	GetAction(ISNamespace::AT_ShowActual)->setVisible(false);

	QGroupBox *GroupBox = new QGroupBox(this);
	GroupBox->setContentsMargins(10, 0, 0, 0);
	GroupBox->setTitle(LOCALIZATION("Details"));
	GroupBox->setLayout(new QVBoxLayout());
	GetMainLayout()->addWidget(GroupBox);

	ProtocolListForm = new ISListBaseForm(ISMetaData::GetInstanse().GetMetaTable("_Protocol"), this);
	ProtocolListForm->setEnabled(false);
	connect(ProtocolListForm, &ISListBaseForm::Updated, this, &ISProtocolEntersForm::UpdatedProtocol);
	GroupBox->layout()->addWidget(ProtocolListForm);
}
//-----------------------------------------------------------------------------
ISProtocolEntersForm::~ISProtocolEntersForm()
{

}
//-----------------------------------------------------------------------------
void ISProtocolEntersForm::UpdatedProtocol()
{
	GetTableView()->setFocus();
}
//-----------------------------------------------------------------------------
void ISProtocolEntersForm::Update()
{
	ISListBaseForm::Update();
	ProtocolListForm->setEnabled(false);
}
//-----------------------------------------------------------------------------
void ISProtocolEntersForm::LoadDataAfterEvent()
{
	ISListBaseForm::LoadDataAfterEvent();
	ISListBaseForm::HideField("EnterProtocol");
}
//-----------------------------------------------------------------------------
void ISProtocolEntersForm::SelectedRowEvent(const QItemSelection &ItemSelected, const QItemSelection &ItemDeSelected)
{
	ProtocolListForm->setEnabled(true);
	ProtocolListForm->GetQueryModel()->SetClassFilter("prtc_session = (SELECT prtc_session FROM _protocol WHERE prtc_id = (SELECT petr_enterprotocol FROM _protocolenters WHERE petr_id = " + QString::number(GetObjectID()) + "))");
	ProtocolListForm->LoadData();
}
//-----------------------------------------------------------------------------
