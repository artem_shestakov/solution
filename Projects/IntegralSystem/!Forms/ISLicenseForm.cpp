#include "StdAfx.h"
#include "ISLicenseForm.h"
#include "EXDefines.h"
#include "ISLocalization.h"
#include "ISLicense.h"
#include "ISLineEdit.h"
#include "ISInputDialog.h"
#include "ISCrypterLicense.h"
#include "ISMessageBox.h"
#include "ISBaseTableView.h"
#include "ISMetaQuery.h"
#include "ISDatabase.h"
//-----------------------------------------------------------------------------
ISLicenseForm::ISLicenseForm(QWidget *parent) : ISInterfaceDialogForm(parent)
{
	SelectedAction = ISNamespace::LFA_Unknown;

	setWindowTitle(LOCALIZATION("License.Assistant"));
	setWindowFlags(Qt::WindowStaysOnTopHint | Qt::WindowTitleHint | Qt::CustomizeWindowHint);

	GetMainLayout()->setContentsMargins(LAYOUT_MARGINS_10_PX);
	resize(SIZE_750_400);
	ForbidResize();
	
	QLabel *LabelTitle = new QLabel(LOCALIZATION("License.Assistant.TitleText"), this);
	LabelTitle->setFont(FONT_TAHOMA_10_BOLD);
	LabelTitle->setWordWrap(true);
	GetMainLayout()->addWidget(LabelTitle);

	QGroupBox *GroupBoxAction = new QGroupBox(this);
	GroupBoxAction->setTitle(LOCALIZATION("License.SelectAction"));
	GroupBoxAction->setLayout(new QVBoxLayout());
	GetMainLayout()->addWidget(GroupBoxAction);

	QCommandLinkButton *ButtonActivate = new QCommandLinkButton(GroupBoxAction);
	ButtonActivate->setText(LOCALIZATION("License.Activate"));
	ButtonActivate->setDescription(LOCALIZATION("License.Activate.Description"));
	connect(ButtonActivate, &QCommandLinkButton::clicked, this, &ISLicenseForm::Activate);
	GroupBoxAction->layout()->addWidget(ButtonActivate);

	QCommandLinkButton *ButtonExtendTesting = new QCommandLinkButton(GroupBoxAction);
	ButtonExtendTesting->setText(LOCALIZATION("License.ExtendTesting"));
	ButtonExtendTesting->setDescription(LOCALIZATION("License.ExtendTesting.Description"));
	connect(ButtonExtendTesting, &QCommandLinkButton::clicked, this, &ISLicenseForm::ExtendTesting);
	GroupBoxAction->layout()->addWidget(ButtonExtendTesting);

	QCommandLinkButton *ButtonTesting = new QCommandLinkButton(GroupBoxAction);
	ButtonTesting->setText(LOCALIZATION("License.Testing"));
	ButtonTesting->setDescription(LOCALIZATION("License.Testing.Description"));
	connect(ButtonTesting, &QCommandLinkButton::clicked, this, &ISLicenseForm::Testing);
	GroupBoxAction->layout()->addWidget(ButtonTesting);

	if (!ISLicense::GetInstance().GetCounter())
	{
		ButtonTesting->setEnabled(false);
	}

	QCommandLinkButton *ButtonExit = new QCommandLinkButton(GroupBoxAction);
	ButtonExit->setText(LOCALIZATION("ExitToApplication"));
	connect(ButtonExit, &QCommandLinkButton::clicked, this, &ISLicenseForm::Exit);
	GroupBoxAction->layout()->addWidget(ButtonExit);

	QTabWidget *TabWidget = new QTabWidget(this);
	GetMainLayout()->addWidget(TabWidget);
	
	QFormLayout *FormLayout = new QFormLayout();

	QWidget *WidgetLicenseInfo = new QWidget(TabWidget);
	WidgetLicenseInfo->setLayout(FormLayout);
	TabWidget->addTab(WidgetLicenseInfo, LOCALIZATION("License.LicenseInformation"));

	//������������� �������� (UID)
	ISLineEdit *EditLicenseUID = new ISLineEdit(WidgetLicenseInfo);
	EditLicenseUID->SetValue(ISLicense::GetInstance().GetLicenseUID());
	EditLicenseUID->SetReadOnly(true);
	EditLicenseUID->SetVisibleClear(false);
	FormLayout->addRow(LOCALIZATION("License.SerialNumber") + ":", EditLicenseUID);

	QLabel *LabelLicenseType = nullptr;

	if (ISLicense::GetInstance().GetActivated())
	{
		ButtonActivate->setEnabled(false);
		ButtonExtendTesting->setEnabled(false);
		ButtonTesting->setEnabled(false);
		ButtonExit->setEnabled(false);

		//������������ ����
		ISLineEdit *EditLicenseKey = new ISLineEdit(WidgetLicenseInfo);
		EditLicenseKey->SetValue(ISLicense::GetInstance().GetLicenseKey());
		EditLicenseKey->SetReadOnly(true);
		EditLicenseKey->SetVisibleClear(false);
		FormLayout->addRow(LOCALIZATION("License.LicenseKey") + ":", EditLicenseKey);

		LabelLicenseType = new QLabel(LOCALIZATION("License.Type.Constant"), WidgetLicenseInfo);
	}
	else
	{
		//�������
		ISLineEdit *EditCounter = new ISLineEdit(WidgetLicenseInfo);
		EditCounter->SetValue(QString::number(ISLicense::GetInstance().GetCounter()));
		EditCounter->SetReadOnly(true);
		EditCounter->SetVisibleClear(false);
		FormLayout->addRow(LOCALIZATION("License.Counter") + ":", EditCounter);

		LabelLicenseType = new QLabel(LOCALIZATION("License.Type.Temporary"), WidgetLicenseInfo);
	}

	FormLayout->addRow(LOCALIZATION("License.Type") + ":", LabelLicenseType);

	QSqlQueryModel *Model = new QSqlQueryModel(this);
	Model->setQuery(ISMetaQuery::GetQueryText("LicenseExtend"), ISDatabase::GetInstance().GetDefaultDB());

	ISBaseTableView *TableLicenseExtend = new ISBaseTableView(TabWidget);
	TableLicenseExtend->verticalHeader()->setVisible(false);
	TableLicenseExtend->setModel(Model);
	TabWidget->addTab(TableLicenseExtend, LOCALIZATION("License.HistoryExtend"));
}
//-----------------------------------------------------------------------------
ISLicenseForm::~ISLicenseForm()
{

}
//-----------------------------------------------------------------------------
ISNamespace::LicenseFormAction ISLicenseForm::GetSelectedAction()
{
	return SelectedAction;
}
//-----------------------------------------------------------------------------
void ISLicenseForm::Activate()
{
	QVariant Value = ISInputDialog::GetText(this, LOCALIZATION("License.Activating"), LOCALIZATION("License.EnterLicenseKey"));
	if (Value.isValid())
	{
		QString DecryptValue = ISCrypterLicense::Decrypt(Value.toString());
		if (ISLicense::GetInstance().GetLicenseUID() == DecryptValue)
		{
			ISLicense::GetInstance().Activate(Value.toString());
			ISMessageBox::ShowInformation(this, LOCALIZATION("Message.Information.LicenseActivated"));

			SelectedAction = ISNamespace::LFA_Activated;
			close();
		}
		else
		{
			ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.EnteredCodeInvalid"));
		}
	}
}
//-----------------------------------------------------------------------------
void ISLicenseForm::ExtendTesting()
{
	QVariant Value = ISInputDialog::GetText(this, LOCALIZATION("License.ExpandingTesting"), LOCALIZATION("License.EnterExpandingKey"));
	if (Value.isValid())
	{
		QString Counter = ISCrypterLicense::Decrypt(Value.toString());
		if (Counter.toInt())
		{
			if (ISLicense::GetInstance().CheckExtend(Value))
			{
				ISLicense::GetInstance().Extend(Counter.toInt(), Value);
				Testing();
				
				ISMessageBox::ShowInformation(this, LOCALIZATION("Message.Information.ExpandedTesting").arg(Counter));
			}
			else
			{
				ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.NotExtendTesting"));
			}
		}
		else
		{
			ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.EnteredCodeInvalid"));
		}
	}
}
//-----------------------------------------------------------------------------
void ISLicenseForm::Testing()
{
	SelectedAction = ISNamespace::LFA_TestingApplication;
	close();
}
//-----------------------------------------------------------------------------
void ISLicenseForm::Exit()
{
	SelectedAction = ISNamespace::LFA_Exit;
	close();
}
//-----------------------------------------------------------------------------
