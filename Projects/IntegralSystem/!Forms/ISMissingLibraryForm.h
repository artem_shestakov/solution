#pragma once
//-----------------------------------------------------------------------------
#include "ISInterfaceDialogForm.h"
#include "ISListWidget.h"
//-----------------------------------------------------------------------------
class ISMissingLibraryForm : public ISInterfaceDialogForm
{
	Q_OBJECT

public:
	ISMissingLibraryForm(QWidget *parent = 0);
	virtual ~ISMissingLibraryForm();

	void SetLibraryList(const QStringList &StringList);

protected:
	void OnAddLibrary(const QString &Library);

private:
	ISListWidget *ListWidget;
};
//-----------------------------------------------------------------------------
