#pragma once
//-----------------------------------------------------------------------------
#include "ISObjectFormBase.h"
//-----------------------------------------------------------------------------
class ISUserObjectForm : public ISObjectFormBase
{
	Q_OBJECT

public:
	Q_INVOKABLE ISUserObjectForm(ISNamespace::ObjectFormType form_type, PMetaClassTable *meta_table, QWidget *parent, int object_id = 0);
	virtual ~ISUserObjectForm();
	
protected:
	void AfterShowEvent() override;
	bool Save() override;
	void SaveEvent() override;
	void PasswordChange();

private:
	ISFieldEditBase *EditLogin;

	QString LoginOld;
};
//-----------------------------------------------------------------------------
