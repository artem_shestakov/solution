#pragma once
//-----------------------------------------------------------------------------
#include "ISObjectFormBase.h"
//-----------------------------------------------------------------------------
class ISEMailObjectForm : public ISObjectFormBase
{
	Q_OBJECT

public:
	Q_INVOKABLE ISEMailObjectForm(ISNamespace::ObjectFormType form_type, PMetaClassTable *meta_table, QWidget *parent, int object_id = 0);
	virtual ~ISEMailObjectForm();

	void SetRecipient(const QVariant &Recipient); //�������� ����������

protected:
	bool Save() override;
};
//-----------------------------------------------------------------------------
