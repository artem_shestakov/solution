#pragma once
//-----------------------------------------------------------------------------
#include "ISTasksBaseListForm.h"
//-----------------------------------------------------------------------------
class ISTasksFromMeListForm : public ISTasksBaseListForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISTasksFromMeListForm(QWidget *parent = 0);
	virtual ~ISTasksFromMeListForm();
};
//-----------------------------------------------------------------------------
