#pragma once
//-----------------------------------------------------------------------------
#include "ISListBaseForm.h"
#include "ISComboEdit.h"
#include "ISListWidget.h"
#include "ISUuid.h"
//-----------------------------------------------------------------------------
class ISEMailListForm : public ISListBaseForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISEMailListForm(QWidget *parent = 0);
	virtual ~ISEMailListForm();

protected:
	void CreateCopy() override;
	void Edit() override;
	void Delete() override;
	bool DeleteCascade() override;
	void AfterShowEvent() override;

	void CreateStatusWidget();
	void StatusSelectionChanged();
	ISUuid GetTypeDispatch(); //�������� ��� �������� ���������
	ISUuid GetCurrentStatus(); //�������� ������ �������� ���������

protected:
	void SendMail();
	void ReadMail();

private:
	QAction *ActionSend;
	QAction *ActionRead;

	ISListWidget *ListWidgetStatus;
	QString CurrentStatusUID;
};
//-----------------------------------------------------------------------------
