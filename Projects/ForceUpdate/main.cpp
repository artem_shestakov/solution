#include "StdAfx.h"
#include "EXDefines.h"
#include "ISUpdate.h"
#include "ISDatabase.h"
#include "ISConfig.h"
#include "ISDebug.h"
#include "ISLocalization.h"
//-----------------------------------------------------------------------------
int main(int argc, char *argv[])
{
	QCoreApplication Application(argc, argv);

	QTextCodec::setCodecForLocale(QTextCodec::codecForName("IBM 866"));

	ISConfig::GetInstance().Initialize(CONFIG_FILE_PATH);
	ISLocalization::GetInstance().LoadResourceFile(LOCALIZATION_FILE_INTEGRAL_SYSTEM);
	ISLocalization::GetInstance().LoadResourceFile(LOCALIZATION_FILE_CORE);

	int MaximumProgress = 0;
	int FileID = 0;
	QString FileName;
	QString Version;

	bool Connected = ISDatabase::GetInstance().ConnectToDefaultDB(SYSTEM_USER_LOGIN, SYSTEM_USER_PASSWORD);
	if (Connected)
	{
		QObject::connect(&ISUpdate::GetInstance(), &ISUpdate::Message, [=](const QString &Message)
		{
			QTextStream Stream(stdout);
			Stream << Message << endl;
		});
	
		QObject::connect(&ISUpdate::GetInstance(), &ISUpdate::ProgressValue, [=](int Value)
		{
			QTextStream Stream(stdout);
			Stream << LOCALIZATION("LoadingUpdateProgress").arg(Value) << endl;
		});

		bool Result = ISUpdate::GetInstance().CheckUpdate(FileID, FileName, Version);
		if (Result)
		{
			Result = ISUpdate::GetInstance().LoadUpdate(FileID, FileName);
			if (Result)
			{
				ISUpdate::GetInstance().StartInstallUpdate(FileName);
			}
		}
		else
		{
			ISDebug::ShowEmptyString(false);
			ISDebug::ShowString(LOCALIZATION("UpdateNotRequired"));
			system("PAUSE");
		}
	}
}
//-----------------------------------------------------------------------------
