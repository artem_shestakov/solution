@ECHO off

SET /a ERROR_COUNT=0

MSBuild ..\Library\ISCarat\ISCarat.vcxproj /p:Configuration=%1 /p:Platform=%2 /p:OutDir=..\..\Deploy\%1-%2\ /p:PlatformToolset=%3 /m
IF ERRORLEVEL 1 GOTO ERROR
GOTO OK
:ERROR
SET /a ERROR_COUNT+=1
:OK

MSBuild ..\Library\ISConfigurator\ISConfigurator.vcxproj /p:Configuration=%1 /p:Platform=%2 /p:OutDir=..\..\Deploy\%1-%2\ /p:PlatformToolset=%3 /m
IF ERRORLEVEL 1 GOTO ERROR
GOTO OK
:ERROR
SET /a ERROR_COUNT+=1
:OK

MSBuild ..\Library\ISControls\ISControls.vcxproj /p:Configuration=%1 /p:Platform=%2 /p:OutDir=..\..\Deploy\%1-%2\ /p:PlatformToolset=%3 /m
IF ERRORLEVEL 1 GOTO ERROR
GOTO OK
:ERROR
SET /a ERROR_COUNT+=1
:OK

MSBuild ..\Library\ISCore\ISCore.vcxproj /p:Configuration=%1 /p:Platform=%2 /p:OutDir=..\..\Deploy\%1-%2\ /p:PlatformToolset=%3 /m
IF ERRORLEVEL 1 GOTO ERROR
GOTO OK
:ERROR
SET /a ERROR_COUNT+=1
:OK

MSBuild ..\Library\ISFields\ISFields.vcxproj /p:Configuration=%1 /p:Platform=%2 /p:OutDir=..\..\Deploy\%1-%2\ /p:PlatformToolset=%3 /m
IF ERRORLEVEL 1 GOTO ERROR
GOTO OK
:ERROR
SET /a ERROR_COUNT+=1
:OK

MSBuild ..\Library\ISMetadata\ISMetadata.vcxproj /p:Configuration=%1 /p:Platform=%2 /p:OutDir=..\..\Deploy\%1-%2\ /p:PlatformToolset=%3 /m
IF ERRORLEVEL 1 GOTO ERROR
GOTO OK
:ERROR
SET /a ERROR_COUNT+=1
:OK

MSBuild ..\Library\ISNetwork\ISNetwork.vcxproj /p:Configuration=%1 /p:Platform=%2 /p:OutDir=..\..\Deploy\%1-%2\ /p:PlatformToolset=%3 /m
IF ERRORLEVEL 1 GOTO ERROR
GOTO OK
:ERROR
SET /a ERROR_COUNT+=1
:OK

MSBuild ..\Library\ISPlugin\ISPlugin.vcxproj /p:Configuration=%1 /p:Platform=%2 /p:OutDir=..\..\Deploy\%1-%2\ /p:PlatformToolset=%3 /m
IF ERRORLEVEL 1 GOTO ERROR
GOTO OK
:ERROR
SET /a ERROR_COUNT+=1
:OK

MSBuild ..\Library\ISPostgres\ISPostgres.vcxproj /p:Configuration=%1 /p:Platform=%2 /p:OutDir=..\..\Deploy\%1-%2\ /p:PlatformToolset=%3 /m
IF ERRORLEVEL 1 GOTO ERROR
GOTO OK
:ERROR
SET /a ERROR_COUNT+=1
:OK

MSBuild ..\Library\ISPrinting\ISPrinting.vcxproj /p:Configuration=%1 /p:Platform=%2 /p:OutDir=..\..\Deploy\%1-%2\ /p:PlatformToolset=%3 /m
IF ERRORLEVEL 1 GOTO ERROR
GOTO OK
:ERROR
SET /a ERROR_COUNT+=1
:OK

MSBuild ..\Library\ISProcess\ISProcess.vcxproj /p:Configuration=%1 /p:Platform=%2 /p:OutDir=..\..\Deploy\%1-%2\ /p:PlatformToolset=%3 /m
IF ERRORLEVEL 1 GOTO ERROR
GOTO OK
:ERROR
SET /a ERROR_COUNT+=1
:OK

MSBuild ..\Library\ISResources\ISResources.vcxproj /p:Configuration=%1 /p:Platform=%2 /p:OutDir=..\..\Deploy\%1-%2\ /p:PlatformToolset=%3 /m
IF ERRORLEVEL 1 GOTO ERROR
GOTO OK
:ERROR
SET /a ERROR_COUNT+=1
:OK

MSBuild ..\Library\ISSystem\ISSystem.vcxproj /p:Configuration=%1 /p:Platform=%2 /p:OutDir=..\..\Deploy\%1-%2\ /p:PlatformToolset=%3 /m
IF ERRORLEVEL 1 GOTO ERROR
GOTO OK
:ERROR
SET /a ERROR_COUNT+=1
:OK

MSBuild ..\Library\ISTelephony\ISTelephony.vcxproj /p:Configuration=%1 /p:Platform=%2 /p:OutDir=..\..\Deploy\%1-%2\ /p:PlatformToolset=%3 /m
IF ERRORLEVEL 1 GOTO ERROR
GOTO OK
:ERROR
SET /a ERROR_COUNT+=1
:OK

IF %ERROR_COUNT% GTR 0 BuildLibrary %1 %2 ...