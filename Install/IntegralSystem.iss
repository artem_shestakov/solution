[Setup]
AppId={{757D746C-0676-404E-BC4C-40246AB5E38}
AppName=IntegralSystem
AppVerName=IntegralSystem
AppVersion=%Version%
VersionInfoVersion=%Version%
DefaultDirName={pf}\IntegralSystem
DefaultGroupName=IntegralSystem
OutputDir=..\Output
OutputBaseFilename=IntegralSystem_%Object%_%Configuration%_%PlatformName%_%Version%
AlwaysShowDirOnReadyPage=yes
ShowLanguageDialog=yes
DisableProgramGroupPage=no
AppPublisher=Shestakov Artem
CreateUninstallRegKey=yes
DirExistsWarning=auto
LicenseFile=..\Resources\Licenses\IntegralSystem.txt
WizardImageStretch=yes
SetupIconFile=..\Resources\Icons\IntegralSystem.ico
FlatComponentsList=no
ShowComponentSizes=yes
InfoBeforeFile=..\Resources\SourceInstaller\InformationBefore.txt
InfoAfterFile=..\Resources\SourceInstaller\InformationAfter.txt
Compression=lzma2/ultra64
SolidCompression=yes
PrivilegesRequired=admin
UninstallDisplayIcon={app}\IntegralSystem.ico
UninstallDisplayName=IntegralSystem
DisableWelcomePage=no

[Languages]
Name: "Russian"; MessagesFile: "compiler:Languages\Russian.isl";

[Registry]
Root: HKLM; Subkey: "Software\Microsoft\Windows\CurrentVersion\Run"; ValueType: string; ValueName: "Carat"; ValueData: "{app}\Carat.exe"; Components: Server;

[Dirs]
Name: "{app}\Logs";
Name: "{app}\Temp";
Name: "{app}\ProcDump";

[Types]
Name: Installation; Description: "Тип установки"; Flags: iscustom;

[Components]
Name: Server; Description: "Сервер"; Types: Installation; Flags: exclusive;
Name: Client; Description: "Клиент"; Types: Installation; Flags: exclusive;

[Icons]
Name: "{userdesktop}\Carat"; Filename: "{app}\Carat.exe"; WorkingDir: "{app}"; Tasks: DesktopIconServer; Comment: "Серверный процесс Carat";
Name: "{userdesktop}\Integral System"; Filename: "{app}\IntegralSystem.exe"; WorkingDir: "{app}"; Tasks: DesktopIconClient; Comment: "Система управления данными «IntegralSystem»";
Name: "{group}\Carat"; Filename: "{app}\Carat.exe"; WorkingDir: "{app}"; Tasks: DesktopIconServer; Comment: "Серверный процесс Carat";
Name: "{group}\Integral System"; Filename: "{app}\IntegralSystem.exe"; WorkingDir: "{app}"; Comment: "Система управления данными IntegralSystem";
Name: "{group}\Принудительное обновление"; Filename: "{app}\ForceUpdate.exe"; WorkingDir: "{app}"; Comment: "Принудительное обновление IntegralSystem";
Name: "{group}\DebugView"; Filename: "{app}\DebugView.exe"; WorkingDir: "{app}\Utility";
Name: "{group}\Документация"; Filename: "{app}\IntegralSystem.doc"; WorkingDir: "{app}"; Comment: "Документация";
Name: "{group}\Удалить Integral System"; Filename: "{app}\unins000.exe"; WorkingDir: "{app}"; Comment: "Удаление Integral System";

[Tasks]
Name: "DesktopIconServer"; Description: "Создать значок «Carat» на рабочем столе"; GroupDescription: "{cm:AdditionalIcons}"; Components: Server;
Name: "DesktopIconClient"; Description: "Создать значок «IntegralSystem» на рабочем столе"; GroupDescription: "{cm:AdditionalIcons}"; Components: Server Client;

[Run]
Filename: "{app}\Carat.exe"; Description: "Запустить сервер после выхода из установщика"; Flags: postinstall shellexec skipifsilent; Components: Server;
Filename: "{app}\IntegralSystem.exe"; Description: "Запустить программу после выхода из установщика"; Flags: postinstall shellexec skipifsilent; Components: Server Client;

[INI]
Filename: {app}\Config.ini; Section: Connection; Key: Server; String: ; Flags: createkeyifdoesntexist;
Filename: {app}\Config.ini; Section: Connection; Key: Port; String: ; Flags: createkeyifdoesntexist;
Filename: {app}\Config.ini; Section: Connection; Key: Database; String: ; Flags: createkeyifdoesntexist;
Filename: {app}\Config.ini; Section: AutoInput; Key: Included; String: false; Flags: createkeyifdoesntexist;
Filename: {app}\Config.ini; Section: AutoInput; Key: Login; String: ; Flags: createkeyifdoesntexist;
Filename: {app}\Config.ini; Section: AutoInput; Key: Password; String: ; Flags: createkeyifdoesntexist;
Filename: {app}\Config.ini; Section: Other; Key: Module; String: "{code:GetCompletePath}/IS%Object%.dll"; Flags: createkeyifdoesntexist;
Filename: {app}\Config.ini; Section: Other; Key: Autoboot; String: false; Flags: createkeyifdoesntexist;
Filename: {app}\Config.ini; Section: DatabaseService; Key: Server; String: ; Flags: createkeyifdoesntexist;
Filename: {app}\Config.ini; Section: DatabaseService; Key: Port; String: ; Flags: createkeyifdoesntexist;
Filename: {app}\Config.ini; Section: DatabaseService; Key: Database; String: ; Flags: createkeyifdoesntexist;
Filename: {app}\Config.ini; Section: DatabaseService; Key: Login; String: ; Flags: createkeyifdoesntexist;
Filename: {app}\Config.ini; Section: DatabaseService; Key: Password; String: ; Flags: createkeyifdoesntexist;
Filename: {app}\Config.ini; Section: DatabaseService; Key: FolderBackup; String: ; Flags: createkeyifdoesntexist;
Filename: {app}\Config.ini; Section: DatabaseService; Key: FolderPostgresBin; String: ; Flags: createkeyifdoesntexist;
Filename: {app}\Config.ini; Section: DatabaseService; Key: Compression; String: 0; Flags: createkeyifdoesntexist;

[Files]
Source: ..\Components\%Configuration%-%Platform%\*; DestDir: {app}; Flags: ignoreversion;
Source: ..\Components\OpenSSL\%Platform%\*; DestDir: {app}; Flags: ignoreversion;
Source: ..\Components\Utility\cmdow.exe; DestDir: {win}; Flags: ignoreversion;
Source: ..\Components\Utility\DebugView.exe; DestDir: {app}; Flags: ignoreversion;
Source: ..\Components\Utility\Procdump\%Platform%\*; DestDir: {app}; Flags: ignoreversion;
Source: ..\Components\VersionLib\%Platform%\*; DestDir: {app}; Flags: ignoreversion;
Source: ..\Documentation\IntegralSystem.doc; DestDir: {app}; Flags: ignoreversion;
Source: ..\Resources\Licenses\*; DestDir: {app}\Licenses; Flags: ignoreversion;

Source: ..\Deploy\%Configuration%-%Platform%\DatabaseService.exe; DestDir: {app}; Flags: ignoreversion; Components: Server;
Source: ..\Deploy\%Configuration%-%Platform%\DatabaseService.pdb; DestDir: {app}; Flags: ignoreversion; Components: Server;

Source: ..\Deploy\%Configuration%-%Platform%\Carat.exe; DestDir: {app}; Flags: ignoreversion; Components: Server;
Source: ..\Deploy\%Configuration%-%Platform%\Carat.pdb; DestDir: {app}; Flags: ignoreversion; Components: Server;

Source: ..\Deploy\%Configuration%-%Platform%\IntegralSystem.exe; DestDir: {app}; Flags: ignoreversion; Components: Server Client;
Source: ..\Deploy\%Configuration%-%Platform%\IntegralSystem.pdb; DestDir: {app}; Flags: ignoreversion; Components: Server Client;
Source: ..\Resources\Icons\IntegralSystem.ico; DestDir: {app}; Flags: ignoreversion; Components: Server Client;
Source: ..\Resources\SourceInstaller\Update.cmd; DestDir: {app}; Flags: ignoreversion; Components: Server Client;

Source: ..\Deploy\%Configuration%-%Platform%\ForceUpdate.exe; DestDir: {app}; Flags: ignoreversion; Components: Server Client;
Source: ..\Deploy\%Configuration%-%Platform%\ForceUpdate.pdb; DestDir: {app}; Flags: ignoreversion; Components: Server Client;

Source: ..\Deploy\%Configuration%-%Platform%\*; Excludes: "*.exp, *.ilk, *.lib, *.exe"; DestDir: {app}; Flags: ignoreversion recursesubdirs;

[Code]
function GetCompletePath(String: String): String;
var
    AppPath: String;
begin
    AppPath := ExpandConstant('{app}');
    StringChange(AppPath, '\', '/')
    Result := AppPath;
end;

procedure InitializeWizard();
begin
  WizardForm.WelcomeLabel2.Caption := WizardForm.WelcomeLabel2.Caption + #13#10#13#10 + 
  'Наименование: %Object%' + #13#10 +
  'Конфигурация: %Configuration%' + #13#10 +
  'Платформа: %Platform%' + #13#10 +
  'Версия устанавливаемой программы: %Version%';
end;