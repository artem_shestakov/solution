@ECHO off

REM ����� ��������� ��������
call Scripts\ChangeQt.cmd
call Scripts\vcvars32.bat
call WinDeployClear.cmd
call UpdateVersion.cmd

REM ������������ ������
SET /p MAJOR=<..\Major.txt
SET /p MINOR=<..\Minor.txt
SET /p REVISION=<..\Revision.txt
SET /p BUILD=<..\Build.txt

REM ������ ������
MSBuild IntegralSystem.proj /p:Object=%1 /p:Configuration=%2 /p:Platform=%3 /p:PlatformToolset=%4 /p:Version=%MAJOR%.%MINOR%.%REVISION%.%BUILD%
IF ERRORLEVEL 1 GOTO ERROR
GOTO OK
:ERROR
PAUSE
:OK