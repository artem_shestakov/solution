@ECHO off

SET Configuration=%1
SET Platform=%2
SET Folder=%Configuration%-%Platform%

setlocal enabledelayedexpansion

FOR /F "usebackq" %%a IN (`DIR /B ..\Deploy\%Folder%\*.dll ..\Deploy\%Folder%\*.exe`) DO (
SET Result=%%a
SET Path=%cd..%..\Deploy\%1-%2\!Result!

%QT_PATH%\bin\windeployqt.exe !Path! --force --no-angle --no-opengl-sw --no-system-d3d-compiler --no-compiler-runtime --pdb --%Configuration% --libdir ..\Deploy\%Folder% --dir ..\Deploy\%Folder%
)