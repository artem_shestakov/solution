#include <QtCore/QCoreApplication>
#include <QtCore/QStringList>
#include <QtCore/QDebug>
#include "ISCrypterLicense.h"
//-----------------------------------------------------------------------------
int KeyIndex(int Index, const QString &EncryptionKey);
void ShowHelp();
//-----------------------------------------------------------------------------
int main(int argc, char *argv[])
{
	QCoreApplication Licenser(argc, argv);

	QStringList StringList = Licenser.arguments();

	if (StringList.count() == 3)
	{
		QString Argument = StringList.at(1);

		if (Argument == "license") //�������� ������������� �����
		{
			QString LicenseUID = StringList.at(2); //������������� ��������
			QString LicenseKey = ISCrypterLicense::Crypt(LicenseUID);
			qDebug().noquote() << "License key:" << LicenseKey;
		}
		else if (StringList.at(1) == "expand") //�������� ����� ���������
		{
			QString Expand = StringList.at(2);
			if (Expand.toInt())
			{
				Expand = ISCrypterLicense::Crypt(Expand);
				qDebug().noquote() << "Expand:" << Expand;
			}
			else
			{
				qDebug().noquote() << "Invalid expand!";
				ShowHelp();
			}
		}
	}
	else
	{
		qDebug().noquote() << "Invalid arguments!";
		ShowHelp();
	}

	return Licenser.exec();
}
//-----------------------------------------------------------------------------
int KeyIndex(int Index, const QString &EncryptionKey)
{
	int Lenght = EncryptionKey.length();
	int Multiple = Index / Lenght;
	if (Index >= Lenght)
	{
		return Index - (Lenght * Multiple);
	}

	return Index;
}
//-----------------------------------------------------------------------------
void ShowHelp()
{
	qDebug().noquote() << "Using: Licenser license <LicenseUID>";
	qDebug().noquote() << "Using: Licenser expand <Count>";
}
//-----------------------------------------------------------------------------
