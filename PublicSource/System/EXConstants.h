#pragma once
//-----------------------------------------------------------------------------
#define CONST_UID_USER_POSTGRES_UID "{F400ECFA-185E-4587-8961-8731DEAC3B5E}" //������������� ������������ postgres
//-----------------------------------------------------------------------------
#define CONST_UID_PARAGRAPH_DESKTOP "{9A36AD64-5A2F-4B62-9DA7-4FC545B6AB5F}" //������� ����
#define CONST_UID_PARAGRAPH_WORKSPACE "{18A90F8B-1D99-420E-BA60-C3EBF60D595E}" //������� �������
#define CONST_UID_PARAGRAPH_CALENDAR "{1BA7F467-32AA-4719-B8F0-BCD8BC678CAB}" //���������
#define CONST_UID_PARAGRAPH_CHAT "{C8C7F423-39F5-42FE-B902-501A160AC2DC}" //���
#define CONST_UID_PARAGRAPH_TASKS "{588A7DBA-CC77-4EA2-9691-7F800C629D7F}" //������
//-----------------------------------------------------------------------------
#define CONST_UID_PERMISSION_SPECIAL_DESKTOP "{8738DEFC-3603-4205-BDBC-6232DD9EE1CE}" //������� ����
#define CONST_UID_PERMISSION_SPECIAL_EDIT_PRINT_FORM "{88AE9796-7631-4ED3-BA71-93526630423A}" //�������������� �������� ����
#define CONST_UID_PERMISSION_SPECIAL_CHANGE_PASSWORD "{D0CB0DB0-6D8D-4A13-9076-3E0003E8718C}" //����� ������
#define CONST_UID_PERMISSION_SPECIAL_TASKS_ALL "{BBD7170A-94E6-41DD-9427-AFB538C07728}" //������ �����: ��� ������
#define CONST_UID_PERMISSION_SPECIAL_TASKS_MY "{D561EBDC-5F89-4DB0-9171-49EA7AD3AFF8}" //������ �����: ��� ������
#define CONST_UID_PERMISSION_SPECIAL_TASKS_FROM_ME "{9A237DD5-856C-460A-8417-20B756747496}" //������ �����: ������ �� ����
//-----------------------------------------------------------------------------
#define CONST_UID_PROTOCOL_ENTER_APPLICATION "{2614CB96-EC54-4D4F-B1A6-06D2444962CA}"
#define CONST_UID_PROTOCOL_EXIT_APPLICATION "{E9461667-E1CA-4C22-907B-FDC350A931C4}"
#define CONST_UID_PROTOCOL_CREATE_OBJECT "{D1348312-298F-4A7C-B584-9BA8C4952CD3}"
#define CONST_UID_PROTOCOL_CREATE_COPY_OBJECT "{EFA8FE45-1174-4D2E-BBE6-4940380961D4}"
#define CONST_UID_PROTOCOL_EDIT_OBJECT "{0361643D-0A62-4F51-84BD-313F53115EFD}"
#define CONST_UID_PROTOCOL_SHOW_OBJECT "{117E8972-97DC-4E72-93AC-DC3BB50D11CF}"
#define CONST_UID_PROTOCOL_SHOW_SYSTEM_INFO_OBJECT "{2937C918-2E34-431A-86EF-52AEE9BF83AA}"
#define CONST_UID_PROTOCOL_OPEN_PARAGRAPH "{C9C18C76-AC52-4B3E-B89D-5040F06AE3CE}"
#define CONST_UID_PROTOCOL_OPEN_SYSTEM "{66E93262-02C6-49E3-B47E-FF20ED8DDDC5}"
#define CONST_UID_PROTOCOL_OPEN_SUB_SYSTEM "{5F3EBAED-0970-4E0E-883C-535E6F084133}"
#define CONST_UID_PROTOCOL_DELETE_OBJECT "{275EDA32-F757-4EAA-BBD4-6C48AFD5DA9B}"
#define CONST_UID_PROTOCOL_DELETE_CASCADE_OBJECT "{B6EEFB63-8197-4F3A-8373-5C3B2FC409F3}"
#define CONST_UID_PROTOCOL_RECOVERY_OBJECT "{C0000A7D-EDA1-4C06-9D7C-1DC942EE438E}"
#define CONST_UID_PROTOCOL_EXPORT_TABLE "{1C50A667-B1C4-4F2C-8F39-90106ABB6774}"
#define CONST_UID_PROTOCOL_PRINT "{916A2119-5D8D-41C9-8358-AE587F365898}"
//-----------------------------------------------------------------------------
#define CONST_UID_USERGROUPACCESSTYPE_SUBSYSTEMS "{79581A54-802A-4C5D-87BE-9C4A80FCFDEA}"
#define CONST_UID_USERGROUPACCESSTYPE_TABLES "{B622DD43-7065-42A1-B9A8-677C78E71BDD}"
//-----------------------------------------------------------------------------
#define CONST_UID_TELEPHONY_NOT_USE "{B916A611-9C2C-44D5-9F02-5CC3628B2AC0}"
#define CONST_UID_TELEPHONY_ASTERISK "{AAF1708E-B224-441C-B4E9-7569267F3B55}"
#define CONST_UID_TELEPHONY_BEELINE "{CA82CCF7-D66B-4415-BE7B-DC02006DFABF}"
//-----------------------------------------------------------------------------
#define CONST_UID_NOTIFY_NEW_TASK "{6D29E4DA-7BC0-44AD-BF03-6897C3C403F9}"
#define CONST_UID_NOTIFY_EDIT_TASK "{1C674715-8C13-40BD-A21F-857857277394}"
#define CONST_UID_NOTIFY_NEW_CHAT_MESSAGE "{77A232D0-6A44-4A9B-833E-2C55E73E0FA2}"
#define CONST_UID_NOTIFY_MAIL_NEW "{809E3CB2-EBBD-4A1D-8A99-A514F02A6907}"
#define CONST_UID_NOTIFY_USER_CHANGED "{C1141CF5-8E95-4793-841A-2E1E8270A2F3}"
#define CONST_UID_NOTIFY_TERMINATE_USER "{50CD9037-7809-4059-B20C-B18900E8BE34}"
#define CONST_UID_NOTIFY_UPDATE_AVAILABLE "{168CA167-EA0B-48DA-918D-C617FFE2BA35}"
//-----------------------------------------------------------------------------
#define CONST_UID_TASK_STATUS_CONSIDERATION "{AB4DF72F-8114-4A72-AEEB-1661862286F5}" //�� ������������
#define CONST_UID_TASK_STATUS_OPENED "{3B3A2D43-0910-4D0E-9FC2-B90DC7491E91}" //�������
#define CONST_UID_TASK_STATUS_DONE "{FCD1F0B7-462D-4BA2-8496-D5ADADCAB183}" //���������
#define CONST_UID_TASK_STATUS_CLOSED "{9A043F22-3B0F-410D-8C79-D106ABA1A872}" //�������
//-----------------------------------------------------------------------------
#define CONST_UID_EMAIL_STATUS_ALL "{6A568846-FC62-43EF-9C1A-5C0E561C41E3}" //���
#define CONST_UID_EMAIL_STATUS_INCOMING "{E0CD48D8-689F-4FDD-938A-A0AFD6D5F817}" //��������
#define CONST_UID_EMAIL_STATUS_OUTGOING "{34060FAC-009D-4FEE-B47A-9195BCAF0D72}" //���������
#define CONST_UID_EMAIL_STATUS_DRAFTS "{F0932617-A3A4-44AE-A43D-1FB2743620C2}" //���������
//-----------------------------------------------------------------------------
#define CONST_UID_SETTING_GENERAL_SHOWNOTIFICATIONFORM "{98F5F993-7647-422A-8C81-340C065F58FE}"
#define CONST_UID_SETTING_GENERAL_CONFIRMEXITAPPLICATION "{39FDDD7F-3E4C-441C-96E9-75C4CB6380AE}"
#define CONST_UID_SETTING_GENERAL_CHECKUPDATEATSTART "{2431F355-4615-42F8-996A-462E1A7C823D}"
#define CONST_UID_SETTING_VIEW_MAINWINDOWMAXIMIZE "{6A0103BE-8CDA-4EB9-BA28-304CC7033D57}"
#define CONST_UID_SETTING_VIEW_FULLSCREEN "{6AB8CD71-06BA-4059-A795-F9BCA5255634}"
#define CONST_UID_SETTING_VIEW_STARTMAINWINDOWANIMATED "{26F3985C-5404-42A7-9140-48BE784BC424}"
#define CONST_UID_SETTING_VIEW_MENUFASTACCESS "{B276B94D-9167-4E5A-8A6B-991FA6DFC40A}"
#define CONST_UID_SETTING_VIEW_STARTEDPARAGRAPH "{33171C9C-A536-426D-B74F-91F174DF00C6}"
#define CONST_UID_SETTING_VIEW_SHOWWEATHERLABEL "{B27F703A-C2BA-4CE5-A93D-F5FBD828D874}"
#define CONST_UID_SETTING_VIEW_WEATHERTIMEOUT "{2EFCB5E2-4CDF-44F8-B5E7-9D45135DD8D3}"
#define CONST_UID_SETTING_SECURITY_AUTOLOCK "{4AF0990B-B17F-4B71-AFB4-BE42CFA0A71C}"
#define CONST_UID_SETTING_SECURITY_AUTOLOCKINTERVAL "{E5EBC475-FC14-4D8F-98AD-91271006855F}"
#define CONST_UID_SETTING_SECURITY_PASSWORDSETTINGSSHOW "{322387EB-A40B-4404-A638-532A20759F6A}"
#define CONST_UID_SETTING_SECURITY_PASSWORDSETTINGS "{7EDD7BA7-E7AC-4092-B2CB-220C65B49F80}"
#define CONST_UID_SETTING_TABLES_HIGHLIGHTINGODDROWS "{9CBB5512-9559-4740-A7E5-A44D94BEC434}"
#define CONST_UID_SETTING_TABLES_MINIMIZEHEIGHTROWS "{77BA2566-D78D-467A-92CE-DFD987F21C03}"
#define CONST_UID_SETTING_TABLES_SELECTIONBEHAVIOR "{E2C337EE-688E-47EC-B0A9-3EE10BC4DFC2}"
#define CONST_UID_SETTING_TABLES_SHOWHORIZONTALHEADER "{22C5FF3B-83DE-4C8D-BC2B-8A456C8C57D4}"
#define CONST_UID_SETTING_TABLES_SHOWVERTICALHEADER "{6D14F262-B98E-4FBD-B9CC-A1A8E7B52D9F}"
#define CONST_UID_SETTING_TABLES_SHOWCOUNTRECORD "{EFD021B1-5267-4DFA-8D11-A4FF4A419536}"
#define CONST_UID_SETTING_TABLES_REMEMBERSORTING "{3F05B53E-3B86-480F-A5F9-3394F15DB790}"
#define CONST_UID_SETTING_TABLES_REMEMBERCOLUMNSIZE "{D9999522-2BD0-464A-9714-F7399B20490F}"
#define CONST_UID_SETTING_TABLES_SHOWTOOLTIP "{1635F5D1-5575-4D3E-A93E-757D28511E50}"
#define CONST_UID_SETTING_TABLES_SHOWNAVIGATION "{1A5C6B4E-CD33-4478-A516-BF35E2DA01EE}"
#define CONST_UID_SETTING_TABLES_PAGE_NAVIGATION "{9F2A193A-9EFA-42A9-954F-5BC49B17C102}"
#define CONST_UID_SETTING_TABLES_PAGE_NAVIGATION_LIMIT "{6F5CA447-F94A-4CCF-8241-C82F6E8BD82B}"
#define CONST_UID_SETTING_OBJECTS_VISIBLESYSTEMINFO "{2876F177-B30F-41A0-8643-F2BCA583726F}"
#define CONST_UID_SETTING_TABS_WHEELSCROLLINGTABS "{3398D347-6B46-465E-BB58-2B93B92C85EF}"
#define CONST_UID_SETTING_TABS_VIEWUSESSCROLLBUTTONS "{307693A2-BD35-41AE-8F83-016CD340AD43}"
#define CONST_UID_SETTING_STATUS_BAR_SHOWSTATUSBAR "{90EEF535-752B-4632-A3FC-F1A41B6D9F34}"
#define CONST_UID_SETTING_STATUS_BAR_SHOWDATETIME "{9114BF67-E006-4FF6-BC08-09CF13E9C106}"
#define CONST_UID_SETTING_FONTS_SUBSYSTEMPANEL "{D0352FB9-5FD6-444A-BA7B-BEE54897EC25}"
#define CONST_UID_SETTING_FONTS_TABLEVIEW "{A1633FD5-34C6-4E63-9087-DEC2BBDBED3A}"
#define CONST_UID_SETTING_FONTS_FIELDEDITBASE "{0B0F57E7-EC14-4F5F-9CD0-ABF45CDEF6F3}"
#define CONST_UID_SETTING_SOUND_CHATMESSAGE "{D2CACFDF-F703-4351-824D-34FDC05A3F17}"
#define CONST_UID_SETTING_SOUND_CALENDAREVENT "{70680EED-160D-45E9-8F79-D6CBDE8AA875}"
#define CONST_UID_SETTING_CALENDAR_NOTE_VIEW_TYPE "{B03FBAC5-F793-4565-92A3-821C87AE3B56}"
#define CONST_UID_SETTING_DEBUG_ACCESSTODEBUGVIEW "{DC11DFE1-5CF8-405D-AF0B-FBB63E7D76AA}"
#define CONST_UID_SETTING_DEBUG_TIMESHOWLISTFORM "{5332D8E9-6D80-4901-A720-33132AA51F37}"
#define CONST_UID_SETTING_DEBUG_TIMESHOWOBJECTFORM "{3AB3621F-EC12-40C9-8E93-9E5BFFB73173}"
#define CONST_UID_SETTING_EMAIL_EMAIL "{CEF6588C-51E0-4BE6-8B1F-1F5D585448EB}"
#define CONST_UID_SETTING_EMAIL_PASSWORD "{2278A79B-40A3-41EC-A017-6BCD730DC8D4}"
#define CONST_UID_SETTING_EMAIL_INCOMING_SERVER "{C32F11A9-4EC3-4BC5-845B-B57C3FC082B1}"
#define CONST_UID_SETTING_EMAIL_INCOMING_PORT "{A58F7F1C-DB02-4B30-AEAC-06F9ADC763E5}"
#define CONST_UID_SETTING_EMAIL_OUTGOING_SERVER "{2955EDBB-0F0C-4FFB-8B9D-F827081E0BC5}"
#define CONST_UID_SETTING_EMAIL_OUTGOING_PORT "{1607E289-2F82-44E9-AF4C-2B4E1C9298D3}"
#define CONST_UID_SETTING_EMAIL_SSL "{60F2DB1F-977B-472C-AF6C-16F36407A7CA}"
#define CONST_UID_SETTING_OTHER_MAXIMUMVIEWHISTORY "{97D72002-CEF4-4D2A-B00F-7A3E9F1B8DA2}"
#define CONST_UID_SETTING_OTHER_AUTOCLEANTEMPDIR "{118A02D4-4437-496C-BC89-BB81B6CD2391}"
#define CONST_UID_SETTING_OTHER_SELECTED_MENU "{3AD4888F-EDC7-4D69-A97F-F9678B6AAC44}"
//-----------------------------------------------------------------------------
#define CONST_CONFIG_CONNECTION_SERVER "Connection/Server"
#define CONST_CONFIG_CONNECTION_PORT "Connection/Port"
#define CONST_CONFIG_CONNECTION_DATABASE "Connection/Database"
#define CONST_CONFIG_OTHER_MODULE "Other/Module"
#define CONST_CONFIG_OTHER_AUTOBOOT "Other/Autoboot"
#define CONST_CONFIG_AUTOINPUT_INCLUDED "AutoInput/Included"
#define CONST_CONFIG_AUTOINPUT_LOGIN "AutoInput/Login"
#define CONST_CONFIG_AUTOINPUT_PASSWORD "AutoInput/Password"
#define CONST_CONFIG_DATABASESERVICE_SERVER "DatabaseService/Server"
#define CONST_CONFIG_DATABASESERVICE_PORT "DatabaseService/Port"
#define CONST_CONFIG_DATABASESERVICE_DATABASE "DatabaseService/Database"
#define CONST_CONFIG_DATABASESERVICE_LOGIN "DatabaseService/Login"
#define CONST_CONFIG_DATABASESERVICE_PASSWORD "DatabaseService/Password"
#define CONST_CONFIG_DATABASESERVICE_FOLDERBACKUP "DatabaseService/FolderBackup"
#define CONST_CONFIG_DATABASESERVICE_FOLDERPOSTGRESBIN "DatabaseService/FolderPostgresBin"
#define CONST_CONFIG_DATABASESERVICE_COMPRESSION "DatabaseService/Compression"
//-----------------------------------------------------------------------------
