#pragma once
//-----------------------------------------------------------------------------

//All
#define APPLICATION_NAME QApplication::applicationName()
#define APPLICATION_DIR_PATH QApplication::applicationDirPath()
#define APPLICATION_FILE_PATH QApplication::applicationFilePath()
#define APPLICATION_LOGS_PATH QString(APPLICATION_DIR_PATH + "/Logs")
#define APPLICATION_ASSISTANT_DIR QString(APPLICATION_DIR_PATH + "/Assistant")
#define APPLICATION_TEMP_PATH QString(APPLICATION_DIR_PATH + "/Temp")
#define APPLICATION_TRANSLATIOS_DIR QString(APPLICATION_DIR_PATH + "/translations")
#define APPLICATION_INTEGRAL_SYSTEM_NAME "IntegralSystem"
#define CONFIG_FILE_PATH QString(APPLICATION_DIR_PATH + "/Config.ini")
#define DEBUG_VIEW_PATH QString(APPLICATION_DIR_PATH + "/DebugView.exe")
#define SCHEMA_FUNCTIONS_PATH ":Other/Functions.xml"
#define SCHEMA_TEMPLATE_FIELDS_PATH ":Other/ClassTemplateFields.xml"
#define SERVICE_MODE_SCHEME ":Other/ServiceModeScheme.xml"
#define REQUIRED_LIBRARY ":Other/RequiredLibraries.xml"

//DATABASE SERVICE
#define PG_DUMP_FILE_NAME "pg_dump.exe"
#define PG_RESTORE_FILE_NAME "pg_restore.exe"

//PROPERTY
#define IS_PROPERTY_LINE_EDIT_SELECTED_MENU "LineEditSelectedMenu"

//Thread Milliseconds Values
#define ONE_SECOND_TO_MILLISECOND 1000
#define TWO_SECOND_TO_MILLISECOND 2000
#define FIVE_SECOND_TO_MILLISECOND 5000
#define ONE_MINUTE_TO_MILLISECOND 60000
#define TWO_MINUTE_TO_MILLISECOND ONE_MINUTE_TO_MILLISECOND * 2

//INTERFACE
#define SIZE_MINIMUM_HEIGHT_EDIT_FIELD 24 //����������� ������ ���� ����� ����� ����������
#define SIZE_FIXED_HEIGHT_IMAGE_EDIT 500
#define LAYOUT_MARGINS_NULL QMargins(0, 0, 0, 0)
#define LAYOUT_MARGINS_1_PX QMargins(1, 1, 1, 1)
#define LAYOUT_MARGINS_2_PX QMargins(2, 2, 2, 2)
#define LAYOUT_MARGINS_5_PX QMargins(5, 5, 5, 5)
#define LAYOUT_MARGINS_10_PX QMargins(10, 10, 10, 10)
#define ISPUSHBUTTON_MINIMUM_WIDTH 75
#define ISPUSHBUTTON_MINIMUM_HEIGHT 24

//SIZES
#define SIZE_MAIN_WINDOW QSize(800, 559) //������ �������� ����
#define SIZE_MAIN_WINDOW_MINIMUM QSize(700, 500) //����������� ������ �������� ����
#define SIZE_AUTHORIZATION_FORM QSize(350, 450) //������ ����� �����������
#define SIZE_PROGRESS_FORM QSize(550, 100) //������ ����� ���������
#define SIZE_SYSTEM_ICON SIZE_32_32 //������ ������ ������� �� ������
#define SIZE_BUTTON_TITLE QSize(30, 30)
#define SIZE_750_400 QSize(750, 400)
#define SIZE_700_500 QSize(700, 500)
#define SIZE_640_480 QSize(640, 480)
#define SIZE_500_90 QSize(500, 90)
#define SIZE_500_65 QSize(500, 65)
#define SIZE_450_450 QSize(450, 450)
#define SIZE_390_230 QSize(390, 230)
#define SIZE_350_215 QSize(350, 215)
#define SIZE_320_60 QSize(320, 60)
#define SIZE_300_300 QSize(300, 300)
#define SIZE_256_256 QSize(256, 256)
#define SIZE_200_200 QSize(200, 200)
#define SIZE_100_100 QSize(100, 100)
#define SIZE_64_64 QSize(64, 64)
#define SIZE_50_35 QSize(50, 35)
#define SIZE_40_40 QSize(40, 40)
#define SIZE_35_35 QSize(35, 35)
#define SIZE_32_32 QSize(32, 32)
#define SIZE_25_25 QSize(25, 25)
#define SIZE_22_22 QSize(22, 22)
#define SIZE_20_20 QSize(20, 20)
#define SIZE_18_18 QSize(18, 18)
#define SIZE_16_16 QSize(16, 16)
#define SIZE_NULL QSize(0, 0)

//FONTS
#define FONT_APPLICATION_STRING QApplication::font().toString()
#define FONT_APPLICATION FONT_TAHOMA_9
#define FONT_APPLICATION_BOLD FONT_TAHOMA_9_BOLD

#define FONT_COURIER_12 QFont("Courier", 12)
#define FONT_TAHOMA_35 QFont("Tahoma", 35)
#define FONT_TAHOMA_18 QFont("Tahoma", 18)
#define FONT_TAHOMA_15 QFont("Tahoma", 15)
#define FONT_TAHOMA_14 QFont("Tahoma", 14)
#define FONT_TAHOMA_14_BOLD QFont("Tahoma", 14, QFont::Bold)
#define FONT_TAHOMA_13_BOLD QFont("Tahoma", 13, QFont::Bold)
#define FONT_TAHOMA_12 QFont("Tahoma", 12)
#define FONT_TAHOMA_12_BOLD QFont("Tahoma", 12, QFont::Bold)
#define FONT_TAHOMA_11 QFont("Tahoma", 11)
#define FONT_TAHOMA_10_BOLD QFont("Tahoma", 10, QFont::Bold)
#define FONT_TAHOMA_10 QFont("Tahoma", 10)
#define FONT_TAHOMA_9 QFont("Tahoma", 9)
#define FONT_TAHOMA_9_BOLD QFont("Tahoma", 9, QFont::Bold)
#define FONT_TAHOMA_8 QFont("Tahoma", 8)

//CURSORS
#define CURSOR_ARROW Qt::ArrowCursor
#define CURSOR_POINTING_HAND Qt::PointingHandCursor
#define CURSOR_WHATS_THIS Qt::WhatsThisCursor
#define CURSOR_WAIT Qt::WaitCursor
#define CURSOR_SIZE_ALL Qt::SizeAllCursor
#define CURSOR_BUSY Qt::BusyCursor
#define CURSOR_FORBIDDEN Qt::ForbiddenCursor
#define CURSOR_OPEN_HAND Qt::OpenHandCursor
#define CURSOR_SIZE_HORIZONTAL Qt::SizeHorCursor
#define CURSOR_SIZE_VERTICAL Qt::SizeVerCursor
#define CURSOR_SIZE_F_DIAG Qt::SizeFDiagCursor
#define CURSOR_SIZE_B_DIAG Qt::SizeBDiagCursor

//LOCALIZATION
#define LOCALIZATION_FILE_CARAT "Carat"
#define LOCALIZATION_FILE_CORE "Core"
#define LOCALIZATION_FILE_INTEGRAL_SYSTEM "IntegralSystem"
#define LOCALIZATION_FILE_PLUGINS "Plugins"
#define LOCALIZATION_FILE_CARAT_CONTROL "CaratControl"

//SYMBOLS
#define SYMBOL_CIRCLE QString(QChar(9679)) //������ "����"
#define SYMBOL_OBJECT_CHANGED QString(" [*]")
#define SYMBOL_SPACE " " //������ �������
#define SYMBOL_NUMBER "�" //������ "�����"
#define SYMBOL_SPACE_HIDE QString::fromLocal8Bit("�") //�������� ������ (�� �������� ����������� �������� �������)
#define SYMBOL_PLUS "+" //������ "����"
#define SYMBOL_MINUS "-" //������ "�����"

//COLORS
#define EDIT_WIDGET_COLOR_RED QColor(236, 99, 99) //���� ����� ������ �������������� ���� �������������� ����������
#define COLOR_BACKGROUND_INTERFACE QColor(230, 230, 230) //���� ���� ���� ����������
#define COLOR_WHITE QColor(Qt::white) //���� ���� ������� ����� ����������
#define COLOR_MAIN_MENU_BAR QColor(81, 145, 189)
#define COLOR_BACKGROUND_CALENDAR QColor(215, 229, 240)
#define COLOR_WIDGET_BOX_TITLE QColor(235, 235, 235)
#define COLOR_CALENDAR_EVENT_FORM_FLASH QColor(223, 225, 255)
#define COLOR_STANDART "#000000"

//SYSTEM
#define SYSTEM_USER_LOGIN "postgres"
#define SYSTEM_DATABASE_NAME "postgres"
#define SYSTEM_USER_PASSWORD "sys"
#define FOREIGN_VIEW_NAME_FIELD_SPLIT " "
#define SOURCE_LINE __LINE__
#define SOURCE_FILE __FILE__
#define FIGURES_STRING "0123456789"
#define EXIT_CODE_NORMAL EXIT_SUCCESS
#define EXIT_CODE_ERROR EXIT_FAILURE
#define SQL_DRIVER_QPSQL "QPSQL"
#define XOR64_ITERATION_COUNT 15
#define MAX_QUERY_TIME 50 //����������� ����� ���������� SQL-������� (� �������������)
#define EMERGENCY_EXIT_TIMEOUT 10
#define DEBUG_SPLITTER "================================================================================"
#define CONFIGURATOR_PAUSE_BEFORE_COMMAND 5 //�������� ����� ������� ���������� ������� �������������� (� ��������)
#define INTEGER_MAXIMUM INT_MAX //������������ ����� �����
#define DOUBLE_MAXIMUM DBL_MAX
#define VIVRTUAL_KEYBOARD_PATH "C:/Windows/system32/osk.exe"
#define CURRENT_PID QApplication::applicationPid()
#define AVIABLE_IMAGE_EXTENSION QStringList() << EXTENSION_PNG << EXTENSION_JPEG  << EXTENSION_JPG << EXTENSION_DDS << EXTENSION_GIF << EXTENSION_ICNS << EXTENSION_ICO << EXTENSION_SVG << EXTENSION_TGA << EXTENSION_TIFF << EXTENSION_WBMP << EXTENSION_BMP << EXTENSION_WEBP
#define CALENDAR_AUDIO_TIMEOUT 20000
#define LOCAL_HOST_ADDESS "127.0.0.1"
#define BORDER_HEIGHT 6
#define BORDER_WIDTH 6
#define BORDER_SQUARE QSize(6, 6)
#define WINDOW_TITLE_HEIGTH 30
#define DURATION_SHOW_HIDE_ANIMATION 700
#define CLASS_IS_LIST_EDIT "ISListEdit"
#define LICENSE_COUNTER 20

//TOKENS
#define TOKEN_DA_DATA_TOKEN "f2c204cd6ba093b294324acec5d01b25fad632cf"
#define TOKEN_OPEN_WEATHER_MAP_TOKEN "d4e703421760e0ca1ca5944ac1b13692"
#define TOKEN_TELEGRAM "504082486:AAE1PIdf1Oqdi5jkVeHjMsL_za5hl3UEeOo"

//REPORT TYPE
#define REPORT_TYPE_HTML "Html"
#define REPORT_TYPE_WORD "Word"

//TEXT CODECS
#define TEXT_CODEC_IBM866 "IBM 866"

//RegExp's
#define REG_EXP_IP_ADDRESS "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$" //IP-������
#define REG_EXP_EMAIL "\\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b" //����� ����������� �����
#define REG_EXP_NUMBER_COUNT_8 "^[0-9]{8}$" //8 ����
#define REG_EXP_NUMBER_COUNT_9 "^[0-9]{9}$" //9 ����
#define REG_EXP_NUMBER_COUNT_10 "^[0-9]{10}$" //10 ����
#define REG_EXP_NUMBER_COUNT_12 "^[0-9]{12}$" //12 ����
#define REG_EXP_NUMBER_COUNT_13 "^[0-9]{13}$" //13 ����
#define REG_EXP_NUMBER_COUNT_17 "^[0-9]{17}$" //17 ����

//DateTimes Formats
#define DATE_FORMAT_STRING_V1 "d MMMM yyyy"
#define DATE_FORMAT_STRING_V2 "dd.MM.yyyy"
#define TIME_FORMAT_STRING_V1 "hh:mm"
#define TIME_FORMAT_STRING_V2 "hh.mm"
#define TIME_FORMAT_STRING_V3 "hh:mm:ss"
#define DATE_TIME_FORMAT_V1 "d MMMM yyyy hh:mm"
#define DATE_TIME_FORMAT_V2 "dd.MM.yyyy hh:mm"
#define DATE_TIME_FORMAT_V3 "d MMMM yyyy hh:mm:ss"
#define DATE_TIME_FORMAT_V4 "dd.MM.yyyy hh:mm:ss.zzz"
#define DATE_TIME_FORMAT_V5 "d MMMM yy hh:mm:ss.zzz"

//FileExtensions
#define EXTENSION_DLL "dll"
#define EXTENSION_XLS "xls"
#define EXTENSION_XLSX "xlsx"
#define EXTENSION_CSV "csv"
#define EXTENSION_TXT "txt"
#define EXTENSION_HTML "html"
#define EXTENSION_HTM "htm"
#define EXTENSION_DBF "dbf"
#define EXTENSION_PDF "pdf"
#define EXTENSION_LOG "log"
#define EXTENSION_PNG "png"
#define EXTENSION_JPEG "jpeg"
#define EXTENSION_JPG "jpg"
#define EXTENSION_DDS "dds"
#define EXTENSION_GIF "gif"
#define EXTENSION_ICNS "icns"
#define EXTENSION_ICO "ico"
#define EXTENSION_SVG "svg"
#define EXTENSION_TGA "tga"
#define EXTENSION_TIFF "tiff"
#define EXTENSION_WBMP "wbmp"
#define EXTENSION_BMP "bmp"
#define EXTENSION_WEBP "webp"
#define EXTENSION_LANG "lang"
