#pragma once
//-----------------------------------------------------------------------------
#include <QString>
#include <QByteArray>
//-----------------------------------------------------------------------------
class ISCrypterLicense
{
public:
	ISCrypterLicense();
	virtual ~ISCrypterLicense();

	static QString Crypt(const QString &String);
	static QString Decrypt(const QString &String);
};
//-----------------------------------------------------------------------------
