#include "StdAfx.h"
#include "ISCrypterLicense.h"
//-----------------------------------------------------------------------------
ISCrypterLicense::ISCrypterLicense()
{

}
//-----------------------------------------------------------------------------
ISCrypterLicense::~ISCrypterLicense()
{

}
//-----------------------------------------------------------------------------
QString ISCrypterLicense::Crypt(const QString &String)
{
	QByteArray ByteArray(String.toUtf8());
	QString Base64 = ByteArray.toBase64(QByteArray::OmitTrailingEquals);

	QString Result;
	for (const QString &Symbol : Base64)
	{
		Result.insert(0, Symbol);
	}

	return Result;
}
//-----------------------------------------------------------------------------
QString ISCrypterLicense::Decrypt(const QString &String)
{
	QString Base64;
	for (const QString &Symbol : String)
	{
		Base64.insert(0, Symbol);
	}

	Base64 = QByteArray::fromBase64(Base64.toUtf8(), QByteArray::OmitTrailingEquals);
	return Base64;
}
//-----------------------------------------------------------------------------
