#pragma once
//-----------------------------------------------------------------------------
#include "ISVersionMajorInteger.h"
#include "ISVersionMajorString.h"

#include "ISVersionMinorInteger.h"
#include "ISVersionMinorString.h"

#include "ISVersionRevisionInteger.h"
#include "ISVersionRevisionString.h"

#include "ISVersionBuildInteger.h"
#include "ISVersionBuildString.h"

#include "ISVersionDate.h"
#include "ISVersionHash.h"
//-----------------------------------------------------------------------------
#define ISVERSION_INT MAJOR_INTEGER,MINOR_INTEGER,REVISION_INTEGER,BUILD_INTEGER
#define ISVERSION_STRING MAJOR_STRING "." MINOR_STRING "." REVISION_STRING "." BUILD_STRING
//-----------------------------------------------------------------------------
