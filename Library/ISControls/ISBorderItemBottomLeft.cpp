#include "StdAfx.h"
#include "ISBorderItemBottomLeft.h"
#include "EXDefines.h"
//-----------------------------------------------------------------------------
ISBorderItemBottomLeft::ISBorderItemBottomLeft(ISNamespace::BorderItemType BorderType, QWidget *parent) : ISBorderItemBase(BorderType, parent)
{
	setFixedSize(BORDER_SQUARE);
	setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
	setCursor(CURSOR_SIZE_B_DIAG);
}
//-----------------------------------------------------------------------------
ISBorderItemBottomLeft::~ISBorderItemBottomLeft()
{

}
//-----------------------------------------------------------------------------
