#include "StdAfx.h"
#include "ISQTextEdit.h"
#include "ISContextMenuText.h"
#include "ISStyleSheet.h"
//-----------------------------------------------------------------------------
ISQTextEdit::ISQTextEdit(QWidget *parent) : QTextEdit(parent)
{
	Wheel = false;
	ExecEnter = true;

	setStyleSheet(STYLE_SHEET("ISTextEdit"));
}
//-----------------------------------------------------------------------------
ISQTextEdit::~ISQTextEdit()
{

}
//-----------------------------------------------------------------------------
void ISQTextEdit::SetExecuteEnter(bool Enable)
{
	ExecEnter = Enable;
}
//-----------------------------------------------------------------------------
void ISQTextEdit::contextMenuEvent(QContextMenuEvent *e)
{
	ISContextMenuText ContextMenu(this, isReadOnly(), document()->isUndoAvailable(), document()->isRedoAvailable(), textCursor().hasSelection(), QLineEdit::Normal, toPlainText().isEmpty(), textCursor().selectedText().count());
	connect(&ContextMenu, &ISContextMenuText::Delete, [=] { textCursor().removeSelectedText(); });
	connect(&ContextMenu, &ISContextMenuText::UppercaseText, [=]
	{
		int SelectionStart = textCursor().selectionStart();
		int SelectionEnd = textCursor().selectionEnd();

		QString Text = toPlainText();
		QString SelectedText = textCursor().selectedText();
		QString UpperText = SelectedText.toUpper();

		Text.replace(SelectionStart, UpperText.length(), UpperText);
		setText(Text);

		QTextCursor TextCursor = textCursor();
		TextCursor.setPosition(SelectionStart);
		TextCursor.setPosition(SelectionEnd, QTextCursor::KeepAnchor);
		setTextCursor(TextCursor);
	});
	connect(&ContextMenu, &ISContextMenuText::LowercaseText, [=]
	{
		int SelectionStart = textCursor().selectionStart();
		int SelectionEnd = textCursor().selectionEnd();

		QString Text = toPlainText();
		QString SelectedText = textCursor().selectedText();
		QString LowerText = SelectedText.toLower();

		Text.replace(SelectionStart, LowerText.length(), LowerText);
		setText(Text);

		QTextCursor TextCursor = textCursor();
		TextCursor.setPosition(SelectionStart);
		TextCursor.setPosition(SelectionEnd, QTextCursor::KeepAnchor);
		setTextCursor(TextCursor);
	});
	ContextMenu.exec(e->globalPos());
}
//-----------------------------------------------------------------------------
void ISQTextEdit::wheelEvent(QWheelEvent *e)
{
	if (Wheel)
	{
		if (e->delta() > NULL)
		{
			zoomIn();
		}
		else
		{
			zoomOut();
		}
	}
	else
	{
		QTextEdit::wheelEvent(e);
	}
}
//-----------------------------------------------------------------------------
void ISQTextEdit::keyPressEvent(QKeyEvent *e)
{
	if (ExecEnter)
	{
		QTextEdit::keyPressEvent(e);

		if (e->modifiers() == Qt::CTRL)
		{
			Wheel = true;
		}
	}
	else
	{
		if (e->key() == Qt::Key_Enter || e->key() == Qt::Key_Return)
		{
			e->ignore();
			emit KeyPressEnter();
		}
		else
		{
			QTextEdit::keyPressEvent(e);
		}
	}
}
//-----------------------------------------------------------------------------
void ISQTextEdit::keyReleaseEvent(QKeyEvent *e)
{
	QTextEdit::keyReleaseEvent(e);

	Wheel = false;
}
//-----------------------------------------------------------------------------
void ISQTextEdit::mousePressEvent(QMouseEvent *e)
{
	QTextEdit::mousePressEvent(e);

	if (e->button() == Qt::MiddleButton)
	{
		if (!isReadOnly())
		{
			paste();
		}
	}
}
//-----------------------------------------------------------------------------
