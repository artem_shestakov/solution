#pragma once
//-----------------------------------------------------------------------------
#include "iscontrols_global.h"
#include "ISPushButton.h"
//-----------------------------------------------------------------------------
class ISCONTROLS_EXPORT ISBaseTableView : public QTableView
{
	Q_OBJECT

signals:
	void WheelUp();
	void WheelDown();
	void CornerClicked();

public:
	ISBaseTableView(QWidget *parent = 0);
	virtual ~ISBaseTableView();

	void SetSelectionScroll(bool selection_scroll);
	void SetCornerText(const QString &text);
	void SetCornerToolTip(const QString &tool_tip);
	void SetVisibleVerticalHeader(bool visible);

protected:
	void wheelEvent(QWheelEvent *e);
	void paintEvent(QPaintEvent *e);
	
private:
	bool SelectionScroll;
	ISPushButton *ButtonCorner;
};
//-----------------------------------------------------------------------------
