#pragma once
//-----------------------------------------------------------------------------
#include "iscontrols_global.h"
//-----------------------------------------------------------------------------
class ISTitleWidget : public QWidget
{
	Q_OBJECT

signals:
	void Curtail();
	void RestoreDeploy();
	void Close();

public:
	ISTitleWidget(QWidget *parent = 0);
	virtual ~ISTitleWidget();

	void SetIcon(const QIcon &Icon);
	void SetText(const QString &Text);
	void RestoreDeployChange(const QIcon &Icon, const QString &ToolTip);
	void SetVisibleCurtail(bool Visible);
	void SetVisibleRestoreDeploy(bool Visible);
	void SetEnableMoving(bool Enable);
	void SetEnableResize(bool Enable);

protected:
	void mousePressEvent(QMouseEvent *e);
	void mouseReleaseEvent(QMouseEvent *e);
	void mouseMoveEvent(QMouseEvent *e);
	void mouseDoubleClickEvent(QMouseEvent *e);

private:
	QWidget *ParentWidget;
	QPoint PointClick;
	bool MoveEvent;
	bool Resizeble;
	bool Moving;

	QLabel *LabelIcon;
	QLabel *LabelText;

	QPushButton *ButtonCurtail;
	QPushButton *ButtonRestoreDeploy;
	QPushButton *ButtonClose;
};
//-----------------------------------------------------------------------------
