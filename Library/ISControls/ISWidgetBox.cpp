#include "StdAfx.h"
#include "ISWidgetBox.h"
#include "EXDefines.h"
#include "ISAssert.h"
#include "ISBuffer.h"
//-----------------------------------------------------------------------------
ISWidgetBox::ISWidgetBox(const QString &Title, QWidget *parent) : QFrame(parent)
{
	LastHeight = 0;

	setFrameShadow(QFrame::Sunken);
	setFrameShape(QFrame::Box);
	
	MainLayout = new QVBoxLayout();
	MainLayout->setContentsMargins(LAYOUT_MARGINS_NULL);
	MainLayout->setSpacing(0);
	setLayout(MainLayout);

	WidgetBoxTitle = new ISWidgetBoxTitle(this);
	WidgetBoxTitle->SetIcon(BUFFER_ICONS("WidgetBox.Visible.True"));
	WidgetBoxTitle->SetText(Title);
	WidgetBoxTitle->setSizePolicy(WidgetBoxTitle->sizePolicy().horizontalPolicy(), QSizePolicy::Maximum);
	connect(WidgetBoxTitle, &ISWidgetBoxTitle::Clicked, this, &ISWidgetBox::ClickedTitle);
	MainLayout->addWidget(WidgetBoxTitle);

	WidgetCentral = new QWidget(this);
	WidgetCentral->setSizePolicy(WidgetCentral->sizePolicy().horizontalPolicy(), QSizePolicy::Maximum);
	MainLayout->addWidget(WidgetCentral);

	PropertyAnimation = new QPropertyAnimation(WidgetCentral, "maximumHeight", this);
	PropertyAnimation->setDuration(700);
}
//-----------------------------------------------------------------------------
ISWidgetBox::ISWidgetBox(QWidget *parent) : ISWidgetBox(QString(), parent)
{

}
//-----------------------------------------------------------------------------
ISWidgetBox::~ISWidgetBox()
{

}
//-----------------------------------------------------------------------------
void ISWidgetBox::SetTitle(const QString &Title)
{
	WidgetBoxTitle->SetText(Title);
}
//-----------------------------------------------------------------------------
void ISWidgetBox::SetLayout(QLayout *Layout)
{
	IS_ASSERT(!WidgetCentral->layout(), "CentralLayout already exist.");
	WidgetCentral->setLayout(Layout);
}
//-----------------------------------------------------------------------------
QLayout* ISWidgetBox::GetLayout()
{
	return WidgetCentral->layout();
}
//-----------------------------------------------------------------------------
void ISWidgetBox::showEvent(QShowEvent *e)
{
	QWidget::showEvent(e);
	adjustSize();
	LastHeight = WidgetCentral->height();
}
//-----------------------------------------------------------------------------
void ISWidgetBox::ClickedTitle()
{
	if (WidgetCentral->height())
	{
		PropertyAnimation->setStartValue(WidgetCentral->height());
		PropertyAnimation->setEndValue(0);

		WidgetBoxTitle->SetIcon(BUFFER_ICONS("WidgetBox.Visible.False"));
	}
	else
	{
		PropertyAnimation->setStartValue(0);
		PropertyAnimation->setEndValue(LastHeight);

		WidgetBoxTitle->SetIcon(BUFFER_ICONS("WidgetBox.Visible.True"));
	}

	PropertyAnimation->start();
}
//-----------------------------------------------------------------------------
