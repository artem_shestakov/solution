#include "StdAfx.h"
#include "ISQSpinBox.h"
#include "EXDefines.h"
#include "ISContextMenuInteger.h"
#include "ISQLineEdit.h"
//-----------------------------------------------------------------------------
ISQSpinBox::ISQSpinBox(QWidget *parent) : QSpinBox(parent)
{
	setMaximum(INTEGER_MAXIMUM);
	setLineEdit(new ISQLineEdit(this));
	connect(lineEdit(), &QLineEdit::textChanged, this, &ISQSpinBox::TextChanged);
}
//-----------------------------------------------------------------------------
ISQSpinBox::~ISQSpinBox()
{

}
//-----------------------------------------------------------------------------
void ISQSpinBox::contextMenuEvent(QContextMenuEvent *e)
{
	ISContextMenuInteger ContextMenu(lineEdit(), lineEdit()->isReadOnly(), lineEdit()->isUndoAvailable(), lineEdit()->isRedoAvailable(), lineEdit()->hasSelectedText(), lineEdit()->echoMode(), lineEdit()->text().isEmpty(), value(), minimum(), maximum());
	connect(&ContextMenu, &ISContextMenuInteger::Delete, lineEdit(), &QLineEdit::del);
	connect(&ContextMenu, &ISContextMenuInteger::StepUp, this, &ISQSpinBox::stepUp);
	connect(&ContextMenu, &ISContextMenuInteger::StepDown, this, &ISQSpinBox::stepDown);
	ContextMenu.exec(e->globalPos());
}
//-----------------------------------------------------------------------------
