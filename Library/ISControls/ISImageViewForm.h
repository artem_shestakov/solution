#pragma  once
//-----------------------------------------------------------------------------
#include "iscontrols_global.h"
#include "ISAdditionalForm.h"
//-----------------------------------------------------------------------------
class ISCONTROLS_EXPORT ISImageViewForm : public ISAdditionalForm
{
	Q_OBJECT

public:
	ISImageViewForm(const QPixmap &Pixmap, QWidget *parent = 0);
	virtual ~ISImageViewForm();

protected:
	void CreateToolBar();
	void CreateStatusBar();

	void Print();
	void ZoomIn();
	void ZoomOut();
	void NormalSize();
	void FitToWindow();

	void ScaleImage(double Factor);
	void AdjustScrollBar(QScrollBar *ScrollBar, double Factor);

private:
	QLabel *LabelImage;
	QScrollArea *ScrollArea;
	double ScaleFactor;
	QPrinter Printer;

	QAction *ActionPrint;
	QAction *ActionZoomIn;
	QAction *ActionZoomOut;
	QAction *ActionNormalSize;
	QAction *ActionFitToWindow;
};
//-----------------------------------------------------------------------------
