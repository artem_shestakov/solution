#include "StdAfx.h"
#include "ISTitleWidget.h"
#include "EXDefines.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
//-----------------------------------------------------------------------------
ISTitleWidget::ISTitleWidget(QWidget *parent) : QWidget(parent)
{
	ParentWidget = parent;
	MoveEvent = false;
	Resizeble = true;
	Moving = true;

	setFixedHeight(WINDOW_TITLE_HEIGTH);
	setMouseTracking(true);
	setAutoFillBackground(true);

	QPalette Palette = palette();
	Palette.setColor(backgroundRole(), Qt::darkGray);
	setPalette(Palette);

	QHBoxLayout *MainLayout = new QHBoxLayout();
	MainLayout->setContentsMargins(LAYOUT_MARGINS_NULL);
	setLayout(MainLayout);

	QHBoxLayout *LayoutLeft = new QHBoxLayout();
	LayoutLeft->setContentsMargins(LAYOUT_MARGINS_2_PX);
	MainLayout->addLayout(LayoutLeft);

	MainLayout->addStretch();

	LabelIcon = new QLabel(this);
	LayoutLeft->addWidget(LabelIcon);

	LabelText = new QLabel(this);
	LabelText->setStyleSheet("color: white");
	LayoutLeft->addWidget(LabelText);

	QHBoxLayout *LayoutRight = new QHBoxLayout();
	LayoutRight->setContentsMargins(LAYOUT_MARGINS_NULL);
	LayoutRight->setSpacing(0);
	MainLayout->addLayout(LayoutRight);

	ButtonCurtail = new QPushButton(this);
	ButtonCurtail->setToolTip(LOCALIZATION("Border.ToCurtail"));
	ButtonCurtail->setIcon(BUFFER_ICONS("WidgetTitle.Curtail"));
	ButtonCurtail->setFixedSize(SIZE_BUTTON_TITLE);
	ButtonCurtail->setFlat(true);
	connect(ButtonCurtail, &QPushButton::clicked, this, &ISTitleWidget::Curtail);
	LayoutRight->addWidget(ButtonCurtail, 0, Qt::AlignTop);

	ButtonRestoreDeploy = new QPushButton(this);
	ButtonRestoreDeploy->setToolTip(LOCALIZATION("Border.Restore"));
	ButtonRestoreDeploy->setIcon(BUFFER_ICONS("WidgetTitle.Restore"));
	ButtonRestoreDeploy->setFixedSize(SIZE_BUTTON_TITLE);
	ButtonRestoreDeploy->setFlat(true);
	connect(ButtonRestoreDeploy, &QPushButton::clicked, this, &ISTitleWidget::RestoreDeploy);
	LayoutRight->addWidget(ButtonRestoreDeploy, 0, Qt::AlignTop);

	ButtonClose = new QPushButton(this);
	ButtonClose->setToolTip(LOCALIZATION("Border.Close"));
	ButtonClose->setIcon(BUFFER_ICONS("WidgetTitle.Close"));
	ButtonClose->setFixedSize(SIZE_BUTTON_TITLE);
	ButtonClose->setFlat(true);
	connect(ButtonClose, &QPushButton::clicked, this, &ISTitleWidget::Close);
	LayoutRight->addWidget(ButtonClose, 0, Qt::AlignTop);
}
//-----------------------------------------------------------------------------
ISTitleWidget::~ISTitleWidget()
{

}
//-----------------------------------------------------------------------------
void ISTitleWidget::SetIcon(const QIcon &Icon)
{
	LabelIcon->setPixmap(Icon.pixmap(SIZE_16_16));
}
//-----------------------------------------------------------------------------
void ISTitleWidget::SetText(const QString &Text)
{
	LabelText->setText(Text);
}
//-----------------------------------------------------------------------------
void ISTitleWidget::RestoreDeployChange(const QIcon &Icon, const QString &ToolTip)
{
	ButtonRestoreDeploy->setIcon(Icon);
	ButtonRestoreDeploy->setToolTip(ToolTip);
}
//-----------------------------------------------------------------------------
void ISTitleWidget::SetVisibleCurtail(bool Visible)
{
	ButtonCurtail->setVisible(Visible);
}
//-----------------------------------------------------------------------------
void ISTitleWidget::SetVisibleRestoreDeploy(bool Visible)
{
	ButtonRestoreDeploy->setVisible(Visible);
}
//-----------------------------------------------------------------------------
void ISTitleWidget::SetEnableMoving(bool Enable)
{
	Moving = Enable;
}
//-----------------------------------------------------------------------------
void ISTitleWidget::SetEnableResize(bool Enable)
{
	Resizeble = Enable;
}
//-----------------------------------------------------------------------------
void ISTitleWidget::mousePressEvent(QMouseEvent *e)
{
	QWidget::mousePressEvent(e);
	
	if (e->button() == Qt::LeftButton)
	{
		MoveEvent = true;
		PointClick = e->pos();
		setCursor(CURSOR_OPEN_HAND);
	}
}
//-----------------------------------------------------------------------------
void ISTitleWidget::mouseReleaseEvent(QMouseEvent *e)
{
	QWidget::mouseReleaseEvent(e);
	
	MoveEvent = false;
	setCursor(CURSOR_ARROW);
}
//-----------------------------------------------------------------------------
void ISTitleWidget::mouseMoveEvent(QMouseEvent *e)
{
	QWidget::mouseMoveEvent(e);

	if (Moving)
	{
		if (MoveEvent)
		{
			int x = e->pos().x() - PointClick.x();
			int y = e->pos().y() - PointClick.y();

			ParentWidget->move(ParentWidget->pos().x() + x, ParentWidget->pos().y() + y);
		}
	}
}
//-----------------------------------------------------------------------------
void ISTitleWidget::mouseDoubleClickEvent(QMouseEvent *e)
{
	QWidget::mouseDoubleClickEvent(e);

	if (Resizeble)
	{
		emit RestoreDeploy();
	}
}
//-----------------------------------------------------------------------------
