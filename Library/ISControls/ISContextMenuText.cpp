#include "StdAfx.h"
#include "ISContextMenuText.h"
#include "ISControls.h"
//-----------------------------------------------------------------------------
ISContextMenuText::ISContextMenuText(QWidget *ParentEdit, bool ReadOnly, bool UndoAvailable, bool RedoAvailable, bool HasSelectedText, QLineEdit::EchoMode EchoMode, bool Empty, int SelectedTextCount)
	: ISContextMenuBase(ParentEdit, ReadOnly, UndoAvailable, RedoAvailable, HasSelectedText, EchoMode, Empty)
{
	QAction *ActionToUpper = ISControls::GetActionContextToUpper(this);
	ActionToUpper->setEnabled(!ReadOnly && SelectedTextCount);
	connect(ActionToUpper, &QAction::triggered, this, &ISContextMenuText::UppercaseText);
	addAction(ActionToUpper);

	QAction *ActionToLower = ISControls::GetActionContextToLower(this);
	ActionToLower->setEnabled(!ReadOnly && SelectedTextCount);
	connect(ActionToLower, &QAction::triggered, this, &ISContextMenuText::LowercaseText);
	addAction(ActionToLower);
}
//-----------------------------------------------------------------------------
ISContextMenuText::~ISContextMenuText()
{

}
//-----------------------------------------------------------------------------
