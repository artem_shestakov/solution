#include "StdAfx.h"
#include "ISLabelLink.h"
#include "ISStyleSheet.h"
#include "EXDefines.h"
//-----------------------------------------------------------------------------
ISLabelLink::ISLabelLink(QWidget *parent) : ISQLabel(parent)
{
	setFocusPolicy(Qt::StrongFocus);
	setStyleSheet(STYLE_SHEET("ISLabelLink"));
	setCursor(CURSOR_POINTING_HAND);
}
//-----------------------------------------------------------------------------
ISLabelLink::~ISLabelLink()
{

}
//-----------------------------------------------------------------------------
void ISLabelLink::enterEvent(QEvent *e)
{
	ISQLabel::enterEvent(e);

	QFont Font = font();
	Font.setUnderline(true);
	setFont(Font);
}
//-----------------------------------------------------------------------------
void ISLabelLink::leaveEvent(QEvent *e)
{
	ISQLabel::leaveEvent(e);

	QFont Font = font();
	Font.setUnderline(false);
	setFont(Font);
}
//-----------------------------------------------------------------------------
