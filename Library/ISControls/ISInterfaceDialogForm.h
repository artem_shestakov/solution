#pragma once
//-----------------------------------------------------------------------------
#include "iscontrols_global.h"
#include "ISAdditionalForm.h"
//-----------------------------------------------------------------------------
class ISCONTROLS_EXPORT ISInterfaceDialogForm : public ISAdditionalForm
{
	Q_OBJECT

public:
	ISInterfaceDialogForm(QWidget *parent = 0, Qt::WindowFlags Flags = 0);
	virtual ~ISInterfaceDialogForm();
	
	virtual void showEvent(QShowEvent *e);
	virtual void closeEvent(QCloseEvent *e);

	virtual bool Exec();
	virtual bool ExecAnimated();

	bool GetResult() const;

protected:
	void DestroyEventLoop();
	void SetResult(bool result);
	virtual void SetWindowState();

private:
	QEventLoop *EventLoopDialog;
	bool Result;
};
//-----------------------------------------------------------------------------
