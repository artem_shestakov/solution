#pragma once
//-----------------------------------------------------------------------------
#include "iscontrols_global.h"
#include "ISContextMenuBase.h"
//-----------------------------------------------------------------------------
//����������� ���� ��� ����� �������������� ������ (��������������� ISLineEdit � ISTextEdit)
class ISCONTROLS_EXPORT ISContextMenuText : public ISContextMenuBase
{
	Q_OBJECT

signals:
	void UppercaseText();
	void LowercaseText();

public:
	ISContextMenuText(QWidget *ParentEdit, bool ReadOnly, bool UndoAvailable, bool RedoAvailable, bool HasSelectedText, QLineEdit::EchoMode EchoMode, bool Empty, int SelectedTextCount);
	virtual ~ISContextMenuText();
};
//-----------------------------------------------------------------------------
