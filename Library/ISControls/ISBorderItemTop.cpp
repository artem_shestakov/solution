#include "StdAfx.h"
#include "ISBorderItemTop.h"
#include "EXDefines.h"
//-----------------------------------------------------------------------------
ISBorderItemTop::ISBorderItemTop(ISNamespace::BorderItemType BorderType, QWidget *parent) : ISBorderItemBase(BorderType, parent)
{
	setFixedHeight(BORDER_HEIGHT);
	setSizePolicy(QSizePolicy::Minimum, sizePolicy().verticalPolicy());
	setCursor(CURSOR_SIZE_VERTICAL);
}
//-----------------------------------------------------------------------------
ISBorderItemTop::~ISBorderItemTop()
{

}
//-----------------------------------------------------------------------------
void ISBorderItemTop::mouseMoveEvent(QMouseEvent *e)
{
	ISBorderItemBase::mouseMoveEvent(e);

	if (GetMoveEnable())
	{
		if (GetMoving())
		{
			QSize Size = GetWidgetForm()->size();

			int x = GetWidgetForm()->pos().x();
			int y = e->pos().y() - GetMouseClick().y();
			int w = GetWidgetForm()->width();
			int h = GetWidgetForm()->size().height() + y;

			qDebug() << x << y << w << h;
			GetWidgetForm()->setGeometry(x, y, Size.width(), Size.height());
		}
	}
}
//-----------------------------------------------------------------------------
