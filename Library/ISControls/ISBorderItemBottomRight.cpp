#include "StdAfx.h"
#include "ISBorderItemBottomRight.h"
#include "EXDefines.h"
//-----------------------------------------------------------------------------
ISBorderItemBottomRight::ISBorderItemBottomRight(ISNamespace::BorderItemType BorderType, QWidget *parent) : ISBorderItemBase(BorderType, parent)
{
	setFixedSize(BORDER_SQUARE);
	setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
	setCursor(CURSOR_SIZE_F_DIAG);
}
//-----------------------------------------------------------------------------
ISBorderItemBottomRight::~ISBorderItemBottomRight()
{

}
//-----------------------------------------------------------------------------
void ISBorderItemBottomRight::mouseMoveEvent(QMouseEvent *e)
{
	ISBorderItemBase::mouseMoveEvent(e);

	if (GetMoveEnable())
	{
		if (GetMoving())
		{
			QSize Size = GetWidgetForm()->size();
			int x = e->windowPos().x() - Size.width();
			int y = e->windowPos().y() - Size.height();
			Size.setWidth(Size.width() + x);
			Size.setHeight(Size.height() + y);

			GetWidgetForm()->resize(Size);
		}
	}
}
//-----------------------------------------------------------------------------
