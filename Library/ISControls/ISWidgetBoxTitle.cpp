#include "StdAfx.h"
#include "ISWidgetBoxTitle.h"
#include "EXDefines.h"
#include "ISControls.h"
//-----------------------------------------------------------------------------
ISWidgetBoxTitle::ISWidgetBoxTitle(QWidget *parent) : QWidget(parent)
{
	setAutoFillBackground(true);
	setMouseTracking(true);
	setCursor(CURSOR_POINTING_HAND);

	ISControls::SetBackgroundColorWidget(this, COLOR_WIDGET_BOX_TITLE);

	QHBoxLayout *Layout = new QHBoxLayout();
	Layout->setContentsMargins(LAYOUT_MARGINS_2_PX);
	setLayout(Layout);

	LabelIcon = new QLabel(this);
	Layout->addWidget(LabelIcon);

	LabelText = new QLabel(this);
	LabelText->setFont(FONT_APPLICATION_BOLD);
	Layout->addWidget(LabelText);

	Layout->addStretch();
}
//-----------------------------------------------------------------------------
ISWidgetBoxTitle::~ISWidgetBoxTitle()
{

}
//-----------------------------------------------------------------------------
void ISWidgetBoxTitle::SetIcon(const QIcon &Icon)
{
	LabelIcon->setPixmap(Icon.pixmap(SIZE_16_16));
}
//-----------------------------------------------------------------------------
void ISWidgetBoxTitle::SetText(const QString &Text)
{
	LabelText->setText(Text);
}
//-----------------------------------------------------------------------------
void ISWidgetBoxTitle::mousePressEvent(QMouseEvent *e)
{
	QWidget::mousePressEvent(e);
	emit Clicked();
}
//-----------------------------------------------------------------------------
