#include "StdAfx.h"
#include "ISBaseTableView.h"
#include "EXDefines.h"
#include "ISStyleSheet.h"
//-----------------------------------------------------------------------------
ISBaseTableView::ISBaseTableView(QWidget *parent) : QTableView(parent)
{
	SelectionScroll = false;

	setSortingEnabled(true);
	setContextMenuPolicy(Qt::CustomContextMenu);
	setSelectionMode(QAbstractItemView::ExtendedSelection);
	setCornerButtonEnabled(false);
	setStyleSheet(STYLE_SHEET("ISBaseTableView"));

	verticalHeader()->setMinimumWidth(40);
	verticalScrollBar()->setContextMenuPolicy(Qt::NoContextMenu);
	horizontalScrollBar()->setContextMenuPolicy(Qt::NoContextMenu);
	horizontalHeader()->setSortIndicator(0, Qt::AscendingOrder);
	horizontalHeader()->setTextElideMode(Qt::ElideRight);

	ButtonCorner = new ISPushButton(this);
	ButtonCorner->setCursor(CURSOR_POINTING_HAND);
	ButtonCorner->setFlat(true);
	ButtonCorner->setFont(FONT_TAHOMA_8);
	connect(ButtonCorner, &QPushButton::clicked, this, &ISBaseTableView::CornerClicked);
}
//-----------------------------------------------------------------------------
ISBaseTableView::~ISBaseTableView()
{

}
//-----------------------------------------------------------------------------
void ISBaseTableView::SetSelectionScroll(bool selection_scroll)
{
	SelectionScroll = selection_scroll;
}
//-----------------------------------------------------------------------------
void ISBaseTableView::SetCornerText(const QString &text)
{
	ButtonCorner->setText(text);
}
//-----------------------------------------------------------------------------
void ISBaseTableView::SetCornerToolTip(const QString &tool_tip)
{
	ButtonCorner->setToolTip(tool_tip);
}
//-----------------------------------------------------------------------------
void ISBaseTableView::SetVisibleVerticalHeader(bool visible)
{
	verticalHeader()->setVisible(visible);
	ButtonCorner->setVisible(visible);
}
//-----------------------------------------------------------------------------
void ISBaseTableView::wheelEvent(QWheelEvent *e)
{
	if (SelectionScroll)
	{
		if (e->delta() > 0)
		{
			emit WheelUp();
		}
		else
		{
			emit WheelDown();
		}
	}
	else
	{
		QTableView::wheelEvent(e);
	}
}
//-----------------------------------------------------------------------------
void ISBaseTableView::paintEvent(QPaintEvent *e)
{
	QTableView::paintEvent(e);

	ButtonCorner->setFixedWidth(verticalHeader()->width());
	ButtonCorner->setFixedHeight(horizontalHeader()->height());
}
//-----------------------------------------------------------------------------
