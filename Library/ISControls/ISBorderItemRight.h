#pragma once
//-----------------------------------------------------------------------------
#include "iscontrols_global.h"
#include "ISBorderItemBase.h"
//-----------------------------------------------------------------------------
class ISBorderItemRight : public ISBorderItemBase
{
	Q_OBJECT

public:
	ISBorderItemRight(ISNamespace::BorderItemType BorderType, QWidget *parent = 0);
	virtual ~ISBorderItemRight();

protected:
	void mouseMoveEvent(QMouseEvent *e);
};
//-----------------------------------------------------------------------------
