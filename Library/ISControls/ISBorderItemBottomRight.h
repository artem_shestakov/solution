#pragma once
//-----------------------------------------------------------------------------
#include "iscontrols_global.h"
#include "ISBorderItemBase.h"
//-----------------------------------------------------------------------------
class ISBorderItemBottomRight : public ISBorderItemBase
{
	Q_OBJECT

public:
	ISBorderItemBottomRight(ISNamespace::BorderItemType BorderType, QWidget *parent = 0);
	virtual ~ISBorderItemBottomRight();

protected:
	void mouseMoveEvent(QMouseEvent *e);
};
//-----------------------------------------------------------------------------
