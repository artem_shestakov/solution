#pragma once
//-----------------------------------------------------------------------------
#include "iscontrols_global.h"
#include "ISBorderItemBase.h"
//-----------------------------------------------------------------------------
class ISBorderItemBottom : public ISBorderItemBase
{
	Q_OBJECT

public:
	ISBorderItemBottom(ISNamespace::BorderItemType BorderType, QWidget *parent = 0);
	virtual ~ISBorderItemBottom();

protected:
	void mouseMoveEvent(QMouseEvent *e);
};
//-----------------------------------------------------------------------------
