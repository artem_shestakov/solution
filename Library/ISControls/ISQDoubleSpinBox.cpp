#include "StdAfx.h"
#include "ISQDoubleSpinBox.h"
#include "EXDefines.h"
#include "ISContextMenuDouble.h"
#include "ISQLineEdit.h"
//-----------------------------------------------------------------------------
ISQDoubleSpinBox::ISQDoubleSpinBox(QWidget *parent) : QDoubleSpinBox(parent)
{
	setMaximum(DOUBLE_MAXIMUM);
	setLineEdit(new ISQLineEdit(this));
	connect(lineEdit(), &QLineEdit::textChanged, this, &ISQDoubleSpinBox::TextChanged);
}
//-----------------------------------------------------------------------------
ISQDoubleSpinBox::~ISQDoubleSpinBox()
{

}
//-----------------------------------------------------------------------------
void ISQDoubleSpinBox::contextMenuEvent(QContextMenuEvent *e)
{
	ISContextMenuDouble ContextMenu(lineEdit(), lineEdit()->isReadOnly(), lineEdit()->isUndoAvailable(), lineEdit()->isRedoAvailable(), lineEdit()->hasSelectedText(), lineEdit()->echoMode(), lineEdit()->text().isEmpty(), value(), minimum(), maximum());
	connect(&ContextMenu, &ISContextMenuDouble::Delete, lineEdit(), &QLineEdit::del);
	connect(&ContextMenu, &ISContextMenuDouble::StepUp, this, &ISQDoubleSpinBox::stepUp);
	connect(&ContextMenu, &ISContextMenuDouble::StepDown, this, &ISQDoubleSpinBox::stepDown);
	ContextMenu.exec(e->globalPos());
}
//-----------------------------------------------------------------------------
