#pragma once
//-----------------------------------------------------------------------------
#include "iscontrols_global.h"
//-----------------------------------------------------------------------------
class ISCONTROLS_EXPORT ISScrollArea : public QScrollArea
{
	Q_OBJECT

public:
	ISScrollArea(QWidget *parent = 0);
	virtual ~ISScrollArea();

private:
	QWidget *Widget;
};
//-----------------------------------------------------------------------------
