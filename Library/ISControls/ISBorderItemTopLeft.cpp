#include "StdAfx.h"
#include "ISBorderItemTopLeft.h"
#include "EXDefines.h"
//-----------------------------------------------------------------------------
ISBorderItemTopLeft::ISBorderItemTopLeft(ISNamespace::BorderItemType BorderType, QWidget *parent) : ISBorderItemBase(BorderType, parent)
{
	setFixedSize(BORDER_SQUARE);
	setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
	setCursor(CURSOR_SIZE_F_DIAG);
}
//-----------------------------------------------------------------------------
ISBorderItemTopLeft::~ISBorderItemTopLeft()
{

}
//-----------------------------------------------------------------------------
void ISBorderItemTopLeft::mouseMoveEvent(QMouseEvent *e)
{
	ISBorderItemBase::mouseMoveEvent(e);
}
//-----------------------------------------------------------------------------
