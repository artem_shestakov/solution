#pragma once
//-----------------------------------------------------------------------------
#include "iscontrols_global.h"
#include "ISBorderItemBase.h"
//-----------------------------------------------------------------------------
class ISCONTROLS_EXPORT ISBorderItemTop : public ISBorderItemBase
{
	Q_OBJECT

public:
	ISBorderItemTop(ISNamespace::BorderItemType BorderType, QWidget *parent = 0);
	virtual ~ISBorderItemTop();

protected:
	void mouseMoveEvent(QMouseEvent *e);
};
//-----------------------------------------------------------------------------
