#include "StdAfx.h"
#include "ISBorderItemLeft.h"
#include "EXDefines.h"
//-----------------------------------------------------------------------------
ISBorderItemLeft::ISBorderItemLeft(ISNamespace::BorderItemType BorderType, QWidget *parent) : ISBorderItemBase(BorderType, parent)
{
	setFixedWidth(BORDER_WIDTH);
	setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Minimum);
	setCursor(CURSOR_SIZE_HORIZONTAL);
}
//-----------------------------------------------------------------------------
ISBorderItemLeft::~ISBorderItemLeft()
{

}
//-----------------------------------------------------------------------------
void ISBorderItemLeft::mouseMoveEvent(QMouseEvent *e)
{
	ISBorderItemBase::mouseMoveEvent(e);
}
//-----------------------------------------------------------------------------
