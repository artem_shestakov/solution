#pragma once
//-----------------------------------------------------------------------------
#include "iscontrols_global.h"
//-----------------------------------------------------------------------------
class ISCONTROLS_EXPORT ISQDoubleSpinBox : public QDoubleSpinBox
{
	Q_OBJECT

signals:
	void TextChanged(const QString &Text);

public:
	ISQDoubleSpinBox(QWidget *parent = 0);
	virtual ~ISQDoubleSpinBox();

protected:
	void contextMenuEvent(QContextMenuEvent *e);
};
//-----------------------------------------------------------------------------
