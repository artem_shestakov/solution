#include "StdAfx.h"
#include "ISBorderItemBase.h"
#include "EXDefines.h"
//-----------------------------------------------------------------------------
ISBorderItemBase::ISBorderItemBase(ISNamespace::BorderItemType BorderType, QWidget *parent) : QWidget(parent)
{
	WidgetForm = parent->parentWidget();
	Moving = false;

	setAutoFillBackground(true);
	setMouseTracking(true);

	QPalette Palette = palette();
	Palette.setColor(backgroundRole(), Qt::darkGray);
	setPalette(Palette);
}
//-----------------------------------------------------------------------------
ISBorderItemBase::~ISBorderItemBase()
{

}
//-----------------------------------------------------------------------------
void ISBorderItemBase::DisableResize()
{
	MoveEnable = false;
	setMouseTracking(false);
	setCursor(CURSOR_ARROW);
}
//-----------------------------------------------------------------------------
void ISBorderItemBase::mousePressEvent(QMouseEvent *e)
{
	QWidget::mousePressEvent(e);

	Moving = true;
	
	MouseClick.setX(e->windowPos().x());
	MouseClick.setY(e->windowPos().y());
}
//-----------------------------------------------------------------------------
void ISBorderItemBase::mouseReleaseEvent(QMouseEvent *e)
{
	QWidget::mouseReleaseEvent(e);

	Moving = false;
}
//-----------------------------------------------------------------------------
QPoint ISBorderItemBase::GetMouseClick() const
{
	return MouseClick;
}
//-----------------------------------------------------------------------------
bool ISBorderItemBase::GetMoving() const
{
	return Moving;
}
//-----------------------------------------------------------------------------
bool ISBorderItemBase::GetMoveEnable() const
{
	return MoveEnable;
}
//-----------------------------------------------------------------------------
QWidget* ISBorderItemBase::GetWidgetForm()
{
	return WidgetForm;
}
//-----------------------------------------------------------------------------
