#include "StdAfx.h"
#include "ISBorderItemRight.h"
#include "EXDefines.h"
//-----------------------------------------------------------------------------
ISBorderItemRight::ISBorderItemRight(ISNamespace::BorderItemType BorderType, QWidget *parent) : ISBorderItemBase(BorderType, parent)
{
	setFixedWidth(BORDER_WIDTH);
	setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Minimum);
	setCursor(CURSOR_SIZE_HORIZONTAL);
}
//-----------------------------------------------------------------------------
ISBorderItemRight::~ISBorderItemRight()
{

}
//-----------------------------------------------------------------------------
void ISBorderItemRight::mouseMoveEvent(QMouseEvent *e)
{
	ISBorderItemBase::mouseMoveEvent(e);

	if (GetMoveEnable())
	{
		if (GetMoving())
		{
			QSize Size = GetWidgetForm()->size();
			int x = e->windowPos().x() - Size.width();
			Size.setWidth(Size.width() + x);

			GetWidgetForm()->resize(Size);
		}
	}
}
//-----------------------------------------------------------------------------
