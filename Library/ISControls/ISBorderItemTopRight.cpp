#include "StdAfx.h"
#include "ISBorderItemTopRight.h"
#include "EXDefines.h"
//-----------------------------------------------------------------------------
ISBorderItemTopRight::ISBorderItemTopRight(ISNamespace::BorderItemType BorderType, QWidget *parent) : ISBorderItemBase(BorderType, parent)
{
	setFixedSize(BORDER_SQUARE);
	setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
	setCursor(CURSOR_SIZE_B_DIAG);
}
//-----------------------------------------------------------------------------
ISBorderItemTopRight::~ISBorderItemTopRight()
{

}
//-----------------------------------------------------------------------------
void ISBorderItemTopRight::mouseMoveEvent(QMouseEvent *e)
{
	ISBorderItemBase::mouseMoveEvent(e);
}
//-----------------------------------------------------------------------------
