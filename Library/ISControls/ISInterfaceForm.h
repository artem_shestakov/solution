#pragma once
//-----------------------------------------------------------------------------
#include "iscontrols_global.h"
#include "ISBorderWidget.h"
#include "ISTitleWidget.h"
//-----------------------------------------------------------------------------
//!������� ����� ����� ����������
class ISCONTROLS_EXPORT ISInterfaceForm : public QWidget
{
	Q_OBJECT

signals:
	void AnimationFinished();

public:
	ISInterfaceForm(QWidget *parent = 0, Qt::WindowFlags Flags = 0);
	virtual ~ISInterfaceForm();

	virtual void showEvent(QShowEvent *e);

	void ShowAnimated(bool maximized = false, int duration = 0);
	void HideAnimation(int duration = 0);

	void ResetPalette(); //�������� ���
	QString GetFormUID() const; //�������� ���������� ������������� �����

protected:
	QVBoxLayout* GetMainLayout(); //�������� ��������� �� ������� ����������� �����
	ISBorderWidget* GetBorderWidget(); //�������� ��������� �� ������ �����
	ISTitleWidget* GetTitleWidget(); //�������� ��������� �� ������ ��������� ����

	void ForbidResize(); //��������� �������� ������ �����

	virtual void AfterShowEvent();
	virtual void SetWindowState();
	virtual void EscapeClicked();
	virtual void EnterClicked();

private:
	QVBoxLayout *MainLayout;
	ISBorderWidget *BorderWidget;
	ISTitleWidget *TitleWidget;

	QPropertyAnimation *AnimationShow;
	QPropertyAnimation *AnimationHide;
	bool Showed;
	bool BorderUsed;
	QString FormUID;
};
//-----------------------------------------------------------------------------
