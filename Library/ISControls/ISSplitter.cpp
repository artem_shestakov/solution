#include "StdAfx.h"
#include "ISSplitter.h"
#include "ISStyleSheet.h"
//-----------------------------------------------------------------------------
ISSplitter::ISSplitter(QWidget* parent, int animationDuration, const QEasingCurve& curve) 
	: QSplitter(parent)
    , Animation(new QPropertyAnimation(this, "splitterHandlePosition"))
    , m_splitterHandlePosition(0)
{
	setHandleWidth(8);
	setStyleSheet(STYLE_SHEET("QSplitter"));

    connect(Animation, &QPropertyAnimation::finished, this, &ISSplitter::animationFinished);

    Animation->setDuration(animationDuration);
    Animation->setEasingCurve(curve);
}

void ISSplitter::moveHandleAt(int position)
{
    Animation->stop();

    Animation->setStartValue(QSplitter::sizes()[0]);
    Animation->setEndValue(position);

    Animation->start();
}

void ISSplitter::setAnimationDuration(int msec)
{
    Animation->setDuration(msec);
}

int ISSplitter::animationDuration()
{
    return Animation->duration();
}

void ISSplitter::setHandleAt(int position)
{
    m_splitterHandlePosition = position;
    moveSplitter(m_splitterHandlePosition, 1);
}

int ISSplitter::handlePosition() const
{
    return m_splitterHandlePosition;
}
