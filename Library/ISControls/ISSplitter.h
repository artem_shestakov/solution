#pragma once
//-----------------------------------------------------------------------------
#include "iscontrols_global.h"
//-----------------------------------------------------------------------------
class ISCONTROLS_EXPORT ISSplitter : public QSplitter
{
	Q_OBJECT

    Q_PROPERTY(int splitterHandlePosition READ handlePosition WRITE setHandleAt)

public:
	ISSplitter(QWidget* parent, int animationDuration = 500, const QEasingCurve& curve = QEasingCurve::InOutQuad);

public:
    int handlePosition() const;
    int animationDuration();
    void moveHandleAt(int position);
    void setHandleAt(int position);

    void setAnimationDuration(int msec);

signals:
    void animationFinished();

private:
    QPropertyAnimation* Animation;
    int m_splitterHandlePosition;
};
//-----------------------------------------------------------------------------
