#include "StdAfx.h"
#include "ISImageViewForm.h"
#include "ISControls.h"
#include "ISSystem.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
//-----------------------------------------------------------------------------
ISImageViewForm::ISImageViewForm(const QPixmap &Pixmap, QWidget *parent) : ISAdditionalForm(parent)
{
	ScaleFactor = 1.0;

	setWindowTitle(LOCALIZATION("ViewImage"));
	setWindowIcon(BUFFER_ICONS("Image"));

	CreateToolBar();

	LabelImage = new QLabel(this);
	LabelImage->setBackgroundRole(QPalette::Base);
	LabelImage->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
	LabelImage->setScaledContents(true);

	if (!Pixmap.isNull())
	{
		LabelImage->setPixmap(Pixmap);
	}

	ScrollArea = new QScrollArea(this);
	ScrollArea->setBackgroundRole(QPalette::Dark);
	ScrollArea->setWidget(LabelImage);
	GetMainLayout()->addWidget(ScrollArea);

	CreateStatusBar();
}
//-----------------------------------------------------------------------------
ISImageViewForm::~ISImageViewForm()
{

}
//-----------------------------------------------------------------------------
void ISImageViewForm::CreateToolBar()
{
	QToolBar *ToolBar = new QToolBar(this);
	ToolBar->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
	GetMainLayout()->addWidget(ToolBar);

	GetMainLayout()->addWidget(ISControls::CreateHorizontalLine(this));

	ActionPrint = ISControls::CreateActionPrint(ToolBar);
	connect(ActionPrint, &QAction::triggered, this, &ISImageViewForm::Print);
	ToolBar->addAction(ActionPrint);

	ActionZoomIn = ISControls::CreateActionZoomIn(ToolBar);
	connect(ActionZoomIn, &QAction::triggered, this, &ISImageViewForm::ZoomIn);
	ToolBar->addAction(ActionZoomIn);

	ActionZoomOut = ISControls::CreateActionZoomOut(ToolBar);
	connect(ActionZoomOut, &QAction::triggered, this, &ISImageViewForm::ZoomOut);
	ToolBar->addAction(ActionZoomOut);

	ActionNormalSize = ISControls::CreateActionNormalSize(ToolBar);
	connect(ActionNormalSize, &QAction::triggered, this, &ISImageViewForm::NormalSize);
	ToolBar->addAction(ActionNormalSize);

	ActionFitToWindow = ISControls::CreateActionFitToWindow(ToolBar);
	ActionFitToWindow->setCheckable(true);
	connect(ActionFitToWindow, &QAction::triggered, this, &ISImageViewForm::FitToWindow);
	ToolBar->addAction(ActionFitToWindow);
}
//-----------------------------------------------------------------------------
void ISImageViewForm::CreateStatusBar()
{
	GetMainLayout()->addWidget(ISControls::CreateHorizontalLine(this));

	QStatusBar *StatusBar = new QStatusBar(this);
	StatusBar->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Maximum);
	GetMainLayout()->addWidget(StatusBar);

	QLabel *LabelImageSize = new QLabel(StatusBar);
	LabelImageSize->setText(LOCALIZATION("ImageSize") + ": " + ISSystem::SizeToString(LabelImage->pixmap()->size()));
	StatusBar->addWidget(LabelImageSize);
}
//-----------------------------------------------------------------------------
void ISImageViewForm::Print()
{
	QPrintDialog PrintDialog(&Printer, this);
	ISSystem::MoveWidgetToCenter(&PrintDialog);
	if (PrintDialog.exec())
	{
		QPainter painter(&Printer);
		QRect rect = painter.viewport();
		QSize size = LabelImage->pixmap()->size();
		size.scale(rect.size(), Qt::KeepAspectRatio);
		painter.setViewport(rect.x(), rect.y(), size.width(), size.height());
		painter.setWindow(LabelImage->pixmap()->rect());
		painter.drawPixmap(0, 0, *LabelImage->pixmap());
	}
}
//-----------------------------------------------------------------------------
void ISImageViewForm::ZoomIn()
{
	ScaleImage(1.25);
}
//-----------------------------------------------------------------------------
void ISImageViewForm::ZoomOut()
{
	ScaleImage(0.8);
}
//-----------------------------------------------------------------------------
void ISImageViewForm::NormalSize()
{
	LabelImage->adjustSize();
	ScaleFactor = 1.0;
}
//-----------------------------------------------------------------------------
void ISImageViewForm::FitToWindow()
{
	bool Fit = ActionFitToWindow->isChecked();
	ScrollArea->setWidgetResizable(Fit);
	if (Fit)
	{
		ActionZoomIn->setEnabled(false);
		ActionZoomOut->setEnabled(false);
		ActionNormalSize->setEnabled(false);
	}
	else
	{
		ActionZoomIn->setEnabled(true);
		ActionZoomOut->setEnabled(true);
		ActionNormalSize->setEnabled(true);

		NormalSize();
	}
}
//-----------------------------------------------------------------------------
void ISImageViewForm::ScaleImage(double Factor)
{
	ScaleFactor *= Factor;
	LabelImage->resize(ScaleFactor * LabelImage->pixmap()->size());

	AdjustScrollBar(ScrollArea->horizontalScrollBar(), Factor);
	AdjustScrollBar(ScrollArea->verticalScrollBar(), Factor);
}
//-----------------------------------------------------------------------------
void ISImageViewForm::AdjustScrollBar(QScrollBar *ScrollBar, double Factor)
{
	ScrollBar->setValue(int(Factor * ScrollBar->value() + ((Factor - 1) * ScrollBar->pageStep() / 2)));
}
//-----------------------------------------------------------------------------
