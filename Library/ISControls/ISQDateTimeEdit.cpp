#include "StdAfx.h"
#include "ISQDateTimeEdit.h"
#include "ISStyleSheet.h"
#include "ISContextMenuDateTime.h"
#include "ISQLineEdit.h"
//-----------------------------------------------------------------------------
ISQDateTimeEdit::ISQDateTimeEdit(QWidget *parent) : QDateTimeEdit(parent)
{
	setStyleSheet(STYLE_SHEET("ISDateTimeEdit"));
	setLineEdit(new ISQLineEdit(parent));
}
//-----------------------------------------------------------------------------
ISQDateTimeEdit::~ISQDateTimeEdit()
{

}
//-----------------------------------------------------------------------------
void ISQDateTimeEdit::contextMenuEvent(QContextMenuEvent *e)
{
	ISContextMenuDateTime ContextMenu(lineEdit(), lineEdit()->isReadOnly(), lineEdit()->isUndoAvailable(), lineEdit()->isRedoAvailable(), lineEdit()->hasSelectedText(), lineEdit()->echoMode(), lineEdit()->text().isEmpty());
	connect(&ContextMenu, &ISContextMenuDateTime::Delete, lineEdit(), &QLineEdit::del);
	connect(&ContextMenu, &ISContextMenuDateTime::StepUp, this, &ISQDateTimeEdit::stepUp);
	connect(&ContextMenu, &ISContextMenuDateTime::StepDown, this, &ISQDateTimeEdit::stepDown);
	ContextMenu.exec(e->globalPos());
}
//-----------------------------------------------------------------------------
