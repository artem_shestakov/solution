#include "StdAfx.h"
#include "ISScrollArea.h"
#include "EXDefines.h"
#include "ISStyleSheet.h"
//-----------------------------------------------------------------------------
ISScrollArea::ISScrollArea(QWidget *parent) : QScrollArea(parent)
{
	Widget = new QWidget(this);
	Widget->setObjectName("WidgetScrollArea");
	Widget->setStyleSheet(STYLE_SHEET("ISScrollArea"));
	setWidget(Widget);

	setFrameShape(QFrame::NoFrame);
	setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
	verticalScrollBar()->setCursor(CURSOR_POINTING_HAND);
	horizontalScrollBar()->setCursor(CURSOR_POINTING_HAND);
	setWidgetResizable(true);
}
//-----------------------------------------------------------------------------
ISScrollArea::~ISScrollArea()
{

}
//-----------------------------------------------------------------------------
