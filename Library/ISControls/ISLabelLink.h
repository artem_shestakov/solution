#pragma once
//-----------------------------------------------------------------------------
#include "iscontrols_global.h"
#include "ISQLabel.h"
//-----------------------------------------------------------------------------
class ISCONTROLS_EXPORT ISLabelLink : public ISQLabel
{
	Q_OBJECT

public:
	ISLabelLink(QWidget *parent = 0);
	virtual ~ISLabelLink();

protected:
	void enterEvent(QEvent *e); //������� ��������� ����
	void leaveEvent(QEvent *e); //������� ����� ���� �������� ������� �������
};
//-----------------------------------------------------------------------------
