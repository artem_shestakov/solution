#include "StdAfx.h"
#include "ISCalendarPopup.h"
#include "EXDefines.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
//-----------------------------------------------------------------------------
ISCalendarPopup::ISCalendarPopup(QWidget *parent) : ISCalendarWidget(parent)
{
	setWindowFlags(Qt::Popup);

	QToolButton *ButtonToday = new QToolButton(this);
	ButtonToday->setAutoRaise(true);
	ButtonToday->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
	ButtonToday->setText(LOCALIZATION("ToCurrentDate"));
	ButtonToday->setIcon(BUFFER_ICONS("Today"));
	ButtonToday->setCursor(CURSOR_POINTING_HAND);
	connect(ButtonToday, &QToolButton::clicked, this, &ISCalendarWidget::Today);
	GetLayout()->addWidget(ButtonToday, 0, Qt::AlignRight);
}
//-----------------------------------------------------------------------------
ISCalendarPopup::~ISCalendarPopup()
{

}
//-----------------------------------------------------------------------------
