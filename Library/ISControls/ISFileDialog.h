#pragma once
//-----------------------------------------------------------------------------
#include "iscontrols_global.h"
//-----------------------------------------------------------------------------
class ISCONTROLS_EXPORT ISFileDialog : public QFileDialog
{
	Q_OBJECT

public:
	ISFileDialog(QWidget *parent = 0);
	virtual ~ISFileDialog();

	static QString GetOpenFileNameImage(QWidget *parent); //�������� ���� � ������������ �����������
	static QString GetSaveFileNameImage(QWidget *parent); //�������� ���� � ������������ �����������
	static QString GetOpenFileName(QWidget *parent, const QString &FilePath = QString(), const QString &FilterName = QString()); //�������� ���� � ������������ �����
	static QString GetSaveFileName(QWidget *parent, const QString &FilterName, const QString &FileName = QString()); //�������� ���� � ������������ �����
	static QString GetDirectoryPath(QWidget *parent); //�������� ���� � ����������
	static QStringList GetOpenFilesName(QWidget *parent, const QString &FilterName = QString()); //�������� ���� � ����������� ������
};
//-----------------------------------------------------------------------------
