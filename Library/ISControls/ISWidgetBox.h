#pragma once
//-----------------------------------------------------------------------------
#include "iscontrols_global.h"
#include "ISWidgetBoxTitle.h"
//-----------------------------------------------------------------------------
class ISCONTROLS_EXPORT ISWidgetBox : public QFrame
{
	Q_OBJECT

public:
	ISWidgetBox(const QString &Title, QWidget *parent = 0);
	ISWidgetBox(QWidget *parent = 0);
	virtual ~ISWidgetBox();

	void SetTitle(const QString &Title);
	
	void SetLayout(QLayout *Layout);
	QLayout* GetLayout();

protected:
	void showEvent(QShowEvent *e);
	void ClickedTitle();

private:
	QVBoxLayout *MainLayout;
	ISWidgetBoxTitle *WidgetBoxTitle;
	QWidget *WidgetCentral;

	QPropertyAnimation *PropertyAnimation;
	int LastHeight;
};
//-----------------------------------------------------------------------------
