#include "StdAfx.h"
#include "ISInterfaceDialogForm.h"
#include "ISSystem.h"
//-----------------------------------------------------------------------------
ISInterfaceDialogForm::ISInterfaceDialogForm(QWidget *parent, Qt::WindowFlags Flags) 
	: ISAdditionalForm(parent, Flags | ((Flags & Qt::WindowType_Mask) == 0 ? Qt::Dialog : Qt::WindowType(0)))
{
	EventLoopDialog = nullptr;
	Result = false;

	setAttribute(Qt::WA_DeleteOnClose, false);
	setAttribute(Qt::WA_ShowModal, true);

	if (GetTitleWidget())
	{
		GetTitleWidget()->SetVisibleCurtail(false);
		GetTitleWidget()->SetVisibleRestoreDeploy(false);
	}
}
//-----------------------------------------------------------------------------
ISInterfaceDialogForm::~ISInterfaceDialogForm()
{
	
}
//-----------------------------------------------------------------------------
void ISInterfaceDialogForm::showEvent(QShowEvent *e)
{
	ISAdditionalForm::showEvent(e);

	QRect Rect = frameGeometry();
	Rect.moveCenter(QDesktopWidget().availableGeometry().center());
	move(Rect.topLeft());
}
//-----------------------------------------------------------------------------
void ISInterfaceDialogForm::closeEvent(QCloseEvent *e)
{
	DestroyEventLoop();
	ISAdditionalForm::closeEvent(e);
}
//-----------------------------------------------------------------------------
bool ISInterfaceDialogForm::Exec()
{
	show();
	
	QEventLoop EventLoop;
	EventLoopDialog = &EventLoop;
	
	(void) EventLoop.exec(QEventLoop::DialogExec);
	
	EventLoopDialog = nullptr;

	return Result;
}
//-----------------------------------------------------------------------------
bool ISInterfaceDialogForm::ExecAnimated()
{
	ShowAnimated();
	return Exec();
}
//-----------------------------------------------------------------------------
bool ISInterfaceDialogForm::GetResult() const
{
	return Result;
}
//-----------------------------------------------------------------------------
void ISInterfaceDialogForm::DestroyEventLoop()
{
	if (EventLoopDialog)
	{
		EventLoopDialog->exit();
		EventLoopDialog = nullptr;
	}
}
//-----------------------------------------------------------------------------
void ISInterfaceDialogForm::SetResult(bool result)
{
	Result = result;
}
//-----------------------------------------------------------------------------
void ISInterfaceDialogForm::SetWindowState()
{

}
//-----------------------------------------------------------------------------
