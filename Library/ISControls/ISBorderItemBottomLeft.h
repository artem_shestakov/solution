#pragma once
//-----------------------------------------------------------------------------
#include "iscontrols_global.h"
#include "ISBorderItemBase.h"
//-----------------------------------------------------------------------------
class ISBorderItemBottomLeft : public ISBorderItemBase
{
	Q_OBJECT

public:
	ISBorderItemBottomLeft(ISNamespace::BorderItemType BorderType, QWidget *parent = 0);
	~ISBorderItemBottomLeft();
};
//-----------------------------------------------------------------------------
