#pragma once
//-----------------------------------------------------------------------------
#include "iscontrols_global.h"
#include "ISBorderItemBase.h"
//-----------------------------------------------------------------------------
class ISCONTROLS_EXPORT ISBorderItemTopRight : public ISBorderItemBase
{
	Q_OBJECT

public:
	ISBorderItemTopRight(ISNamespace::BorderItemType BorderType, QWidget *parent = 0);
	virtual ~ISBorderItemTopRight();

protected:
	void mouseMoveEvent(QMouseEvent *e);
};
//-----------------------------------------------------------------------------
