#pragma once
//-----------------------------------------------------------------------------
#include "iscontrols_global.h"
#include "ISInterfaceForm.h"
//-----------------------------------------------------------------------------
class ISCONTROLS_EXPORT ISAdditionalForm : public ISInterfaceForm
{
	Q_OBJECT

public:
	ISAdditionalForm(QWidget *parent = 0, Qt::WindowFlags Flags = 0);
	virtual ~ISAdditionalForm();

protected:
	void EscapeClicked() override;
};
//-----------------------------------------------------------------------------
