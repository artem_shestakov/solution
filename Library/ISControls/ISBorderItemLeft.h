#pragma once
//-----------------------------------------------------------------------------
#include "iscontrols_global.h"
#include "ISBorderItemBase.h"
//-----------------------------------------------------------------------------
class ISCONTROLS_EXPORT ISBorderItemLeft : public ISBorderItemBase
{
	Q_OBJECT

public:
	ISBorderItemLeft(ISNamespace::BorderItemType BorderType, QWidget *parent = 0);
	virtual ~ISBorderItemLeft();

protected:
	void mouseMoveEvent(QMouseEvent *e);
};
//-----------------------------------------------------------------------------
