#pragma once
//-----------------------------------------------------------------------------
#include "iscontrols_global.h"
//-----------------------------------------------------------------------------
class ISCONTROLS_EXPORT ISWidgetBoxTitle : public QWidget
{
	Q_OBJECT

signals:
	void Clicked();

public:
	ISWidgetBoxTitle(QWidget *parent = 0);
	virtual ~ISWidgetBoxTitle();

	void SetIcon(const QIcon &Icon);
	void SetText(const QString &Text);

protected:
	void mousePressEvent(QMouseEvent *e);

private:
	QLabel *LabelIcon;
	QLabel *LabelText;
};
//-----------------------------------------------------------------------------
