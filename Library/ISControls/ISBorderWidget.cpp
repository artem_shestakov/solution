#include "StdAfx.h"
#include "ISBorderWidget.h"
#include "EXDefines.h"
#include "ISBorderItemBase.h"
//-----------------------------------------------------------------------------
ISBorderWidget::ISBorderWidget(QWidget *parent) : QWidget(parent)
{
	LayoutCommon = new QVBoxLayout();
	LayoutCommon->setContentsMargins(LAYOUT_MARGINS_NULL);
	LayoutCommon->setSpacing(0);
	setLayout(LayoutCommon);

	CreateTop();
	CreateCenter();
	CreateBottom();
}
//-----------------------------------------------------------------------------
ISBorderWidget::~ISBorderWidget()
{

}
//-----------------------------------------------------------------------------
void ISBorderWidget::SetCentralLayout(QBoxLayout *Layout)
{
	CentralLayout->addLayout(Layout);
}
//-----------------------------------------------------------------------------
void ISBorderWidget::SetTitleWidget(ISTitleWidget *title_widget)
{
	LayoutTitle->addWidget(title_widget);
}
//-----------------------------------------------------------------------------
void ISBorderWidget::SetVisibleBorder(bool Visible)
{
	ItemTopLeft->setVisible(Visible);
	ItemTop->setVisible(Visible);
	ItemTopRight->setVisible(Visible);

	ItemLeft->setVisible(Visible);
	ItemRight->setVisible(Visible);

	ItemBottomLeft->setVisible(Visible);
	ItemBottom->setVisible(Visible);
	ItemBottomRight->setVisible(Visible);
}
//-----------------------------------------------------------------------------
void ISBorderWidget::DisableResize()
{
	ItemTopLeft->DisableResize();
	ItemTop->DisableResize();
	ItemTopRight->DisableResize();

	ItemLeft->DisableResize();
	ItemRight->DisableResize();

	ItemBottomLeft->DisableResize();
	ItemBottom->DisableResize();
	ItemBottomRight->DisableResize();
}
//-----------------------------------------------------------------------------
void ISBorderWidget::CreateTop()
{
	LayoutTop = new QHBoxLayout();
	LayoutTop->setContentsMargins(LAYOUT_MARGINS_NULL);
	LayoutCommon->addLayout(LayoutTop);

	ItemTopLeft = new ISBorderItemTopLeft(ISNamespace::BIT_TopLeft, this);
	LayoutTop->addWidget(ItemTopLeft);

	ItemTop = new ISBorderItemTop(ISNamespace::BIT_Top, this);
	LayoutTop->addWidget(ItemTop);

	ItemTopRight = new ISBorderItemTopRight(ISNamespace::BIT_TopRight, this);
	LayoutTop->addWidget(ItemTopRight);
}
//-----------------------------------------------------------------------------
void ISBorderWidget::CreateCenter()
{
	LayoutCenter = new QHBoxLayout();
	LayoutCenter->setContentsMargins(LAYOUT_MARGINS_NULL);
	LayoutCommon->addLayout(LayoutCenter);

	ItemLeft = new ISBorderItemLeft(ISNamespace::BIT_Left, this);
	LayoutCenter->addWidget(ItemLeft);

	QVBoxLayout *Layout = new QVBoxLayout();
	LayoutCenter->addLayout(Layout);

	LayoutTitle = new QVBoxLayout();
	Layout->addLayout(LayoutTitle);

	CentralLayout = new QVBoxLayout();
	CentralLayout->setSpacing(6);
	Layout->addLayout(CentralLayout);

	ItemRight = new ISBorderItemRight(ISNamespace::BIT_Right, this);
	LayoutCenter->addWidget(ItemRight);
}
//-----------------------------------------------------------------------------
void ISBorderWidget::CreateBottom()
{
	LayoutBottom = new QHBoxLayout();
	LayoutBottom->setContentsMargins(LAYOUT_MARGINS_NULL);
	LayoutCommon->addLayout(LayoutBottom);

	ItemBottomLeft = new ISBorderItemBottomLeft(ISNamespace::BIT_BottomLeft, this);
	LayoutBottom->addWidget(ItemBottomLeft);

	ItemBottom = new ISBorderItemBottom(ISNamespace::BIT_Bottom, this);
	LayoutBottom->addWidget(ItemBottom);

	ItemBottomRight = new ISBorderItemBottomRight(ISNamespace::BIT_BottomRight, this);
	LayoutBottom->addWidget(ItemBottomRight);
}
//-----------------------------------------------------------------------------
