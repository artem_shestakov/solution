#include "StdAfx.h"
#include "ISImageWidget.h"
#include "EXDefines.h"
#include "ISContextMenuImage.h"
#include "ISStyleSheet.h"
#include "ISFileDialog.h"
#include "ISSystem.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISImageViewForm.h"
//-----------------------------------------------------------------------------
ISImageWidget::ISImageWidget(QWidget *parent) : QLabel(parent)
{
	setStyleSheet(STYLE_SHEET("ISLabelImage"));
	setAlignment(Qt::AlignCenter);
	setMouseTracking(true);
	setAutoFillBackground(true);
	setAcceptDrops(true);
	SetBackgroundImage(BUFFER_ICONS("ImageEditBackgroup").pixmap(SIZE_64_64));
}
//-----------------------------------------------------------------------------
ISImageWidget::~ISImageWidget()
{

}
//-----------------------------------------------------------------------------
void ISImageWidget::contextMenuEvent(QContextMenuEvent *e)
{
	ISContextMenuImage ContextMenu(this, CurrentPixmap.isNull());
	connect(&ContextMenu, &ISContextMenuImage::Select, this, &ISImageWidget::OnSelect);
	connect(&ContextMenu, &ISContextMenuImage::Cut, this, &ISImageWidget::OnCut);
	connect(&ContextMenu, &ISContextMenuImage::Copy, this, &ISImageWidget::OnCopy);
	connect(&ContextMenu, &ISContextMenuImage::Paste, this, &ISImageWidget::OnPaste);
	connect(&ContextMenu, &ISContextMenuImage::Save, this, &ISImageWidget::OnSave);
	connect(&ContextMenu, &ISContextMenuImage::OpenView, this, &ISImageWidget::OnOpenView);
	ContextMenu.exec(e->globalPos());
}
//-----------------------------------------------------------------------------
void ISImageWidget::dragEnterEvent(QDragEnterEvent *e)
{
	const QMimeData *MimeData = e->mimeData();
	if (MimeData->hasFormat("text/uri-list"))
	{
		if (MimeData->urls().count() > 1) //���� ������������ ������������� ����� ������ �����
		{
			return;
		}

		QString FilePath = MimeData->urls()[0].toString();
		if (!QStringList(AVIABLE_IMAGE_EXTENSION).contains(QFileInfo(FilePath).suffix().toLower())) //���� ���������� �������������� �� �����������
		{
			return;
		}

		e->acceptProposedAction();
	}
}
//-----------------------------------------------------------------------------
void ISImageWidget::dropEvent(QDropEvent *e)
{
	const QMimeData* MimeData = e->mimeData();
	if (MimeData->hasUrls())
	{
		QString FilePath = MimeData->urls()[0].toString(QUrl::PreferLocalFile);
		SetPixmap(FilePath);
	}
}
//-----------------------------------------------------------------------------
void ISImageWidget::dragMoveEvent(QDragMoveEvent *e)
{
	const QMimeData *MimeData = e->mimeData();
	if (MimeData->hasUrls())
	{
		e->acceptProposedAction();
	}
}
//-----------------------------------------------------------------------------
void ISImageWidget::mouseDoubleClickEvent(QMouseEvent *e)
{
	QLabel::mouseDoubleClickEvent(e);

	if (e->button() == Qt::LeftButton)
	{
		if (CurrentPixmap.isNull())
		{
			OnSelect();
		}
		else
		{
			OnOpenView();
		}
	}
}
//-----------------------------------------------------------------------------
void ISImageWidget::SetBackgroundImage(const QPixmap &Pixmap)
{
	setPixmap(Pixmap);
}
//-----------------------------------------------------------------------------
QPixmap ISImageWidget::GetPixmap() const
{
	return CurrentPixmap;
}
//-----------------------------------------------------------------------------
void ISImageWidget::SetPixmap(const QPixmap &Pixmap)
{
	Clear();
	CurrentPixmap = Pixmap;
	QPixmap CompletePixmap = Pixmap.scaled(SIZE_200_200, Qt::KeepAspectRatio);
	setPixmap(CompletePixmap);
	emit ImageChanged();

	if (!Pixmap.isNull())
	{
		setFocus();
	}
}
//-----------------------------------------------------------------------------
void ISImageWidget::SetPixmap(const QString &Path)
{
	QPixmap Pixmap;
	if (Pixmap.load(Path))
	{
		SetPixmap(Pixmap);
	}
}
//-----------------------------------------------------------------------------
void ISImageWidget::Clear()
{
	CurrentPixmap = QPixmap();
	setPixmap(QPixmap());
	emit ImageChanged();
	SetBackgroundImage(BUFFER_ICONS("ImageEditBackgroup").pixmap(SIZE_64_64));
}
//-----------------------------------------------------------------------------
void ISImageWidget::OnSelect()
{
	QString Path = ISFileDialog::GetOpenFileNameImage(this);
	if (Path.length())
	{
		SetPixmap(Path);
	}
}
//-----------------------------------------------------------------------------
void ISImageWidget::OnCut()
{
	OnCopy();
	Clear();
}
//-----------------------------------------------------------------------------
void ISImageWidget::OnCopy()
{
	QApplication::clipboard()->setPixmap(CurrentPixmap);
}
//-----------------------------------------------------------------------------
void ISImageWidget::OnPaste()
{
	ISSystem::SetWaitGlobalCursor(true);
	SetPixmap(QApplication::clipboard()->pixmap());
	ISSystem::SetWaitGlobalCursor(false);
}
//-----------------------------------------------------------------------------
void ISImageWidget::OnSave()
{
	QString Path = ISFileDialog::GetSaveFileNameImage(this);
	if (Path.length())
	{
		bool Saved = CurrentPixmap.save(Path);
	}
}
//-----------------------------------------------------------------------------
void ISImageWidget::OnOpenView()
{
	ISSystem::SetWaitGlobalCursor(true);

	ISImageViewForm *ImageViewForm = new ISImageViewForm(CurrentPixmap);
	ImageViewForm->show();

	ISSystem::SetWaitGlobalCursor(false);
}
//-----------------------------------------------------------------------------
