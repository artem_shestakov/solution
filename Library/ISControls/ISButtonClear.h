#pragma once
//-----------------------------------------------------------------------------
#include "iscontrols_global.h"
#include <QPushButton>
//-----------------------------------------------------------------------------
class ISCONTROLS_EXPORT ISButtonClear : public QPushButton
{
	Q_OBJECT

public:
	ISButtonClear(QWidget *parent = 0);
	virtual ~ISButtonClear();
};
//-----------------------------------------------------------------------------
