#include "StdAfx.h"
#include "ISCalendarWidget.h"
#include "ISPushButton.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "EXDefines.h"
#include "ISStyleSheet.h"
//-----------------------------------------------------------------------------
ISCalendarWidget::ISCalendarWidget(QWidget * parent) : QCalendarWidget(parent)
{
	setGridVisible(true);
	setVerticalHeaderFormat(QCalendarWidget::NoVerticalHeader);
	setHorizontalHeaderFormat(QCalendarWidget::ShortDayNames);
	setCursor(CURSOR_POINTING_HAND);
	setDateEditEnabled(true);
	setFirstDayOfWeek(Qt::Monday);

	connect(this, &ISCalendarWidget::selectionChanged, this, &ISCalendarWidget::SelectionChanged);

	Layout = dynamic_cast<QVBoxLayout*>(layout());
	NavigationBar = findChild<QWidget*>("qt_calendar_navigationbar");

	QPalette Palette = NavigationBar->palette();
	Palette.setColor(NavigationBar->backgroundRole(), COLOR_MAIN_MENU_BAR);
	NavigationBar->setPalette(Palette);
	
	ButtonPrevMouth = findChild<QToolButton*>("qt_calendar_prevmonth");
	ButtonPrevMouth->setIcon(BUFFER_ICONS("CalendarWidget.Button.Left"));
	ButtonPrevMouth->setToolTip(LOCALIZATION("PrevMouth"));
	ButtonPrevMouth->setCursor(CURSOR_POINTING_HAND);

	ButtonMonth = findChild<QToolButton*>("qt_calendar_monthbutton");
	ButtonMonth->setStyleSheet(STYLE_SHEET("ISCalendarWidget.ButtonMoth"));
	connect(ButtonMonth->menu(), &QMenu::triggered, this, &ISCalendarWidget::MonthClicked);

	for (QAction *Action : ButtonMonth->menu()->actions())
	{
		Action->setCheckable(true);

		if (Action->data().toInt() == QDate::currentDate().month())
		{
			Action->setChecked(true);
			Action->setFont(FONT_APPLICATION_BOLD);
		}
	}

	ButtonYear = findChild<QToolButton*>("qt_calendar_yearbutton");
	ButtonYear->setStyleSheet(STYLE_SHEET("ISCalendarWidget.ButtonYear"));

	ButtonNextMouth = findChild<QToolButton*>("qt_calendar_nextmonth");
	ButtonNextMouth->setIcon(BUFFER_ICONS("CalendarWidget.Button.Right"));
	ButtonNextMouth->setToolTip(LOCALIZATION("NextMouth"));
	ButtonNextMouth->setCursor(CURSOR_POINTING_HAND);

	//������ ���������
	QTextCharFormat TextCharFormatHeader;
	TextCharFormatHeader.setBackground(QBrush(COLOR_BACKGROUND_INTERFACE));
	TextCharFormatHeader.setFontCapitalization(QFont::Capitalize);
	setWeekdayTextFormat(Qt::Monday, TextCharFormatHeader);
	setWeekdayTextFormat(Qt::Tuesday, TextCharFormatHeader);
	setWeekdayTextFormat(Qt::Wednesday, TextCharFormatHeader);
	setWeekdayTextFormat(Qt::Thursday, TextCharFormatHeader);
	setWeekdayTextFormat(Qt::Friday, TextCharFormatHeader);
	setWeekdayTextFormat(Qt::Saturday, TextCharFormatHeader);
	setWeekdayTextFormat(Qt::Sunday, TextCharFormatHeader);

	//������ ���������
	QTextCharFormat HeaderFormat;
	HeaderFormat.setFontWeight(QFont::Bold);
	setHeaderTextFormat(HeaderFormat);
}
//-----------------------------------------------------------------------------
ISCalendarWidget::~ISCalendarWidget()
{

}
//-----------------------------------------------------------------------------
QWidget* ISCalendarWidget::GetNavigationBar()
{
	return NavigationBar;
}
//-----------------------------------------------------------------------------
QToolButton* ISCalendarWidget::GetButtonPrevMonth()
{
	return ButtonPrevMouth;
}
//-----------------------------------------------------------------------------
QToolButton* ISCalendarWidget::GetButtonNextMonth()
{
	return ButtonNextMouth;
}
//-----------------------------------------------------------------------------
QToolButton* ISCalendarWidget::GetButtonMonth()
{
	return ButtonMonth;
}
//-----------------------------------------------------------------------------
QVBoxLayout* ISCalendarWidget::GetLayout()
{
	return Layout;
}
//-----------------------------------------------------------------------------
void ISCalendarWidget::Today()
{
	setSelectedDate(QDate::currentDate());
	emit clicked(QDate::currentDate());
}
//-----------------------------------------------------------------------------
void ISCalendarWidget::SelectionChanged()
{
	emit DateChanged(selectedDate());
}
//-----------------------------------------------------------------------------
void ISCalendarWidget::paintCell(QPainter *Painter, const QRect &Rect, const QDate &Date) const
{
	Painter->save();

	if (Date == QDate::currentDate()) //���� �������� ������� ����
	{
		Painter->fillRect(Rect, COLOR_BACKGROUND_CALENDAR); //���������� ����

		//��������� �����
		Painter->setBrush(QBrush(Qt::transparent, Qt::SolidPattern));
		Painter->setPen(QPen(COLOR_MAIN_MENU_BAR, 2, Qt::SolidLine));
		Painter->drawRect(QRect(Rect.x(), Rect.y(), Rect.width(), Rect.height()));
	}
	else
	{
		if (Date == selectedDate()) //��� �������� ���������� ������������ ����
		{
			Painter->fillRect(Rect, COLOR_BACKGROUND_CALENDAR); //���������� ����
		}
		else
		{
			Painter->fillRect(Rect, Qt::white); //���������� ����
		}
	}

	if (Date.month() != monthShown()) //���� �������� ��� �� �������� ������
	{
		Painter->fillRect(Rect, QColor(245, 245, 245)); //���������� ����
	}

	QString Text = QString::number(Date.day()); //�����
	
	QRect RectText(Rect.x() + 3, Rect.y() + 2, Rect.width(), Rect.height()); //������������ ������
	Painter->drawText(RectText, Text); //��������� ������ � �����

	Painter->restore();
}
//-----------------------------------------------------------------------------
void ISCalendarWidget::hideEvent(QHideEvent *e)
{
	QCalendarWidget::hideEvent(e);
	emit Hide();
}
//-----------------------------------------------------------------------------
void ISCalendarWidget::MonthClicked(QAction *ActionClicked)
{
	for (QAction *Action : ButtonMonth->menu()->actions())
	{
		Action->setFont(FONT_APPLICATION);
		Action->setChecked(false);
	}

	ActionClicked->setFont(FONT_APPLICATION_BOLD);
	ActionClicked->setChecked(true);
}
//-----------------------------------------------------------------------------
