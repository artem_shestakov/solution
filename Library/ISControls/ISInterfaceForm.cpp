#include "StdAfx.h"
#include "ISInterfaceForm.h"
#include "EXDefines.h"
#include "ISControls.h"
#include "ISSystem.h"
#include "ISBorderWidget.h"
#include "ISTitleWidget.h"
#include "ISBuffer.h"
#include "ISLocalization.h"
//-----------------------------------------------------------------------------
ISInterfaceForm::ISInterfaceForm(QWidget *parent, Qt::WindowFlags Flags) : QWidget(parent, Flags)
{
	AnimationShow = nullptr;
	Showed = false;
	BorderWidget = nullptr;
	TitleWidget = nullptr;
	FormUID = QUuid::createUuid().toString();

	setAttribute(Qt::WA_DeleteOnClose, true);
	setAutoFillBackground(true);

	MainLayout = new QVBoxLayout();
	MainLayout->setContentsMargins(LAYOUT_MARGINS_NULL);

	bool Border = false;
	if (Border) //��������� ���������������� �����
	{
		if (!parent)
		{
			setWindowFlags(Qt::FramelessWindowHint);

			QVBoxLayout *LayoutBorder = new QVBoxLayout();
			LayoutBorder->setContentsMargins(LAYOUT_MARGINS_NULL);
			setLayout(LayoutBorder);

			BorderWidget = new ISBorderWidget(this);
			BorderWidget->SetCentralLayout(MainLayout);
			LayoutBorder->addWidget(BorderWidget);

			TitleWidget = new ISTitleWidget(this);
			TitleWidget->SetIcon(windowIcon());
			connect(TitleWidget, &ISTitleWidget::Curtail, this, &ISInterfaceForm::showMinimized);
			connect(TitleWidget, &ISTitleWidget::RestoreDeploy, this, &ISInterfaceForm::SetWindowState);
			connect(TitleWidget, &ISTitleWidget::Close, this, &ISInterfaceForm::close);
			connect(this, &QWidget::windowIconChanged, TitleWidget, &ISTitleWidget::SetIcon);
			connect(this, &QWidget::windowTitleChanged, TitleWidget, &ISTitleWidget::SetText);
			BorderWidget->SetTitleWidget(TitleWidget);
		}
		else
		{
			setLayout(MainLayout);
			BorderUsed = false;
		}
	}
	else
	{
		setLayout(MainLayout);
		BorderUsed = false;
	}

	ISControls::SetBackgroundColorWidget(this, COLOR_WHITE);

	QAction *ActionWindowState = new QAction(this);
	ActionWindowState->setShortcut(Qt::Key_F11);
	connect(ActionWindowState, &QAction::triggered, this, &ISInterfaceForm::SetWindowState);
	addAction(ActionWindowState);

	QAction *ActionEscape = new QAction(this);
	ActionEscape->setShortcut(Qt::Key_Escape);
	connect(ActionEscape, &QAction::triggered, this, &ISInterfaceForm::EscapeClicked);
	addAction(ActionEscape);

	QAction *ActionEnter = new QAction(this);
	ActionEnter->setShortcut(Qt::Key_Enter);
	connect(ActionEnter, &QAction::triggered, this, &ISInterfaceForm::EnterClicked);
	addAction(ActionEnter);

	QAction *ActionReturn = new QAction(this);
	ActionReturn->setShortcut(Qt::Key_Return);
	connect(ActionReturn, &QAction::triggered, this, &ISInterfaceForm::EnterClicked);
	addAction(ActionReturn);

	AnimationShow = new QPropertyAnimation(this, "windowOpacity", this);
	connect(AnimationShow, &QPropertyAnimation::finished, this, &ISInterfaceForm::show);

	AnimationHide = new QPropertyAnimation(this, "windowOpacity", this);
	connect(AnimationHide, &QPropertyAnimation::finished, this, &ISInterfaceForm::hide);
}
//-----------------------------------------------------------------------------
ISInterfaceForm::~ISInterfaceForm()
{
	
}
//-----------------------------------------------------------------------------
void ISInterfaceForm::showEvent(QShowEvent *e)
{
	QWidget::showEvent(e);

	if (!Showed)
	{
		Showed = true;
		AfterShowEvent();
	}

	if (BorderUsed)
	{
		if (isMaximized())
		{
			BorderWidget->SetVisibleBorder(false);
			TitleWidget->SetEnableMoving(false);
		}
		else
		{
			TitleWidget->RestoreDeployChange(BUFFER_ICONS("WidgetTitle.Deploy"), LOCALIZATION("Border.Deploy"));
		}
	}
}
//-----------------------------------------------------------------------------
void ISInterfaceForm::ShowAnimated(bool maximized, int duration)
{
	setWindowOpacity(0.0);
	
	if (maximized)
	{
		showMaximized();
	}
	else
	{
		show();
	}

	AnimationShow->setStartValue(windowOpacity());
	AnimationShow->setEndValue(1.0);

	if (duration)
	{
		AnimationShow->setDuration(duration);
	}
	else
	{
		AnimationShow->setDuration(DURATION_SHOW_HIDE_ANIMATION);
	}

	AnimationShow->start();
}
//-----------------------------------------------------------------------------
void ISInterfaceForm::HideAnimation(int duration)
{
	setWindowOpacity(1.0);

	AnimationHide->setStartValue(windowOpacity());
	AnimationHide->setEndValue(0.0);

	if (duration)
	{
		AnimationHide->setDuration(duration);
	}
	else
	{
		AnimationHide->setDuration(DURATION_SHOW_HIDE_ANIMATION);
	}

	AnimationHide->start();
}
//-----------------------------------------------------------------------------
void ISInterfaceForm::ResetPalette()
{
	setPalette(QPalette());
}
//-----------------------------------------------------------------------------
QString ISInterfaceForm::GetFormUID() const
{
	return FormUID;
}
//-----------------------------------------------------------------------------
QVBoxLayout* ISInterfaceForm::GetMainLayout()
{
	return MainLayout;
}
//-----------------------------------------------------------------------------
ISBorderWidget* ISInterfaceForm::GetBorderWidget()
{
	return BorderWidget;
}
//-----------------------------------------------------------------------------
ISTitleWidget* ISInterfaceForm::GetTitleWidget()
{
	return TitleWidget;
}
//-----------------------------------------------------------------------------
void ISInterfaceForm::ForbidResize()
{
	setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint | Qt::MSWindowsFixedSizeDialogHint);

	if (BorderUsed)
	{
		BorderWidget->DisableResize();
		TitleWidget->SetEnableResize(false);
	}
}
//-----------------------------------------------------------------------------
void ISInterfaceForm::AfterShowEvent()
{
	
}
//-----------------------------------------------------------------------------
void ISInterfaceForm::SetWindowState()
{
	Qt::WindowStates State = windowState();

	switch (State)
	{
	case Qt::WindowNoState:
		setWindowState(Qt::WindowMaximized);
		
		if (BorderUsed)
		{
			BorderWidget->SetVisibleBorder(false);
			TitleWidget->SetEnableMoving(false);
			TitleWidget->RestoreDeployChange(BUFFER_ICONS("WidgetTitle.Restore"), LOCALIZATION("Border.Restore"));
		}
		break;

	case Qt::WindowMaximized:
		setWindowState(Qt::WindowNoState);

		if (BorderUsed)
		{
			BorderWidget->SetVisibleBorder(true);
			TitleWidget->SetEnableMoving(true);
			TitleWidget->RestoreDeployChange(BUFFER_ICONS("WidgetTitle.Deploy"), LOCALIZATION("Border.Deploy"));
		}
		break;
	}
}
//-----------------------------------------------------------------------------
void ISInterfaceForm::EscapeClicked()
{

}
//-----------------------------------------------------------------------------
void ISInterfaceForm::EnterClicked()
{

}
//-----------------------------------------------------------------------------
