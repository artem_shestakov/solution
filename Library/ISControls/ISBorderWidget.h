#pragma once
//-----------------------------------------------------------------------------
#include "iscontrols_global.h"
#include "ISTitleWidget.h"
//-----------------------------------------------------------------------------
#include "ISBorderItemTop.h"
#include "ISBorderItemTopLeft.h"
#include "ISBorderItemTopRight.h"
#include "ISBorderItemLeft.h"
#include "ISBorderItemRight.h"
#include "ISBorderItemBottomLeft.h"
#include "ISBorderItemBottom.h"
#include "ISBorderItemBottomRight.h"
//-----------------------------------------------------------------------------
class ISCONTROLS_EXPORT ISBorderWidget : public QWidget
{
	Q_OBJECT

public:
	ISBorderWidget(QWidget *parent = 0);
	virtual ~ISBorderWidget();

	void SetCentralLayout(QBoxLayout *Layout);
	void SetTitleWidget(ISTitleWidget *title_widget);
	void SetVisibleBorder(bool Visible);
	void DisableResize();

protected:
	void CreateTop();
	void CreateCenter();
	void CreateBottom();

private:
	QVBoxLayout *LayoutCommon;
	QVBoxLayout *LayoutTitle;
	QHBoxLayout *LayoutTop;
	QHBoxLayout *LayoutCenter;
	QHBoxLayout *LayoutBottom;
	
	QVBoxLayout *CentralLayout;

	ISBorderItemTopLeft *ItemTopLeft;
	ISBorderItemTop *ItemTop;
	ISBorderItemTopRight *ItemTopRight;
	
	ISBorderItemLeft *ItemLeft;
	ISBorderItemRight *ItemRight;

	ISBorderItemBottomLeft *ItemBottomLeft;
	ISBorderItemBottom *ItemBottom;
	ISBorderItemBottomRight *ItemBottomRight;
};
//-----------------------------------------------------------------------------
