#include "StdAfx.h"
#include "ISBorderItemBottom.h"
#include "EXDefines.h"
//-----------------------------------------------------------------------------
ISBorderItemBottom::ISBorderItemBottom(ISNamespace::BorderItemType BorderType, QWidget *parent) : ISBorderItemBase(BorderType, parent)
{
	setFixedHeight(BORDER_HEIGHT);
	setSizePolicy(QSizePolicy::Minimum, sizePolicy().verticalPolicy());
	setCursor(CURSOR_SIZE_VERTICAL);
}
//-----------------------------------------------------------------------------
ISBorderItemBottom::~ISBorderItemBottom()
{

}
//-----------------------------------------------------------------------------
void ISBorderItemBottom::mouseMoveEvent(QMouseEvent *e)
{
	ISBorderItemBase::mouseMoveEvent(e);

	if (GetMoveEnable())
	{
		if (GetMoving())
		{
			QSize Size = GetWidgetForm()->size();
			int y = e->windowPos().y() - Size.height();
			Size.setHeight(Size.height() + y);

			GetWidgetForm()->resize(Size);
		}
	}
}
//-----------------------------------------------------------------------------
