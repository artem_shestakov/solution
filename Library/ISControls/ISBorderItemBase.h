#pragma once
//-----------------------------------------------------------------------------
#include "iscontrols_global.h"
#include "ISNamespace.h"
//-----------------------------------------------------------------------------
class ISCONTROLS_EXPORT ISBorderItemBase : public QWidget
{
	Q_OBJECT

public:
	ISBorderItemBase(ISNamespace::BorderItemType BorderType, QWidget *parent = 0);
	virtual ~ISBorderItemBase();

	void DisableResize();

protected:
	void mousePressEvent(QMouseEvent *e);
	void mouseReleaseEvent(QMouseEvent *e);
	
	QPoint GetMouseClick() const;
	bool GetMoving() const;
	bool GetMoveEnable() const;
	QWidget* GetWidgetForm();

private:
	QWidget *WidgetForm;
	QPoint MouseClick;
	bool Moving;
	bool MoveEnable;
};
//-----------------------------------------------------------------------------
