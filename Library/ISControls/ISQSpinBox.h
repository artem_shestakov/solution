#pragma once
//-----------------------------------------------------------------------------
#include "iscontrols_global.h"
//-----------------------------------------------------------------------------
class ISCONTROLS_EXPORT ISQSpinBox : public QSpinBox
{
	Q_OBJECT

signals:
	void TextChanged(const QString &Text);

public:
	ISQSpinBox(QWidget *parent = 0);
	virtual ~ISQSpinBox();

protected:
	virtual void contextMenuEvent(QContextMenuEvent *e);
};
//-----------------------------------------------------------------------------
