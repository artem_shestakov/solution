#pragma once
//-----------------------------------------------------------------------------
#include "iscontrols_global.h"
#include "ISBorderItemBase.h"
//-----------------------------------------------------------------------------
class ISCONTROLS_EXPORT ISBorderItemTopLeft : public ISBorderItemBase
{
	Q_OBJECT

public:
	ISBorderItemTopLeft(ISNamespace::BorderItemType BorderType, QWidget *parent = 0);
	virtual ~ISBorderItemTopLeft();

protected:
	void mouseMoveEvent(QMouseEvent *e);
};
//-----------------------------------------------------------------------------
