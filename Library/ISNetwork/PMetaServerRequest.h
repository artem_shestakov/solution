#pragma once
//-----------------------------------------------------------------------------
#include "isnetwork_global.h"
//-----------------------------------------------------------------------------
class ISNETWORK_EXPORT PMetaServerRequest : public QObject
{
	Q_OBJECT

public:
	PMetaServerRequest();
	virtual ~PMetaServerRequest();

	void SetID(int id);
	int GetID() const;

	void SetCountMessages(int count_messages);
	int OnGetCountMessages() const;

	void SetCost(double cost);
	double GetCost() const;

	void SetBalance(double balance);
	double GetBalance() const;

private:
	int ID; //�������������
	int CountMessages; //���������� ��� ���������
	double Cost; //��������� ������������� ���������
	double Balance; //���������� ������
};
//-----------------------------------------------------------------------------
