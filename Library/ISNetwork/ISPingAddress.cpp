#include "StdAfx.h"
#include "ISPingAddress.h"
//-----------------------------------------------------------------------------
ISPingAddress::ISPingAddress(const QString &ip_address, QObject *parent) : QObject(parent)
{
	IPAddress = ip_address;
	Running = false;
	LastElapsed = -1;
	CountingTime = nullptr;
}
//-----------------------------------------------------------------------------
ISPingAddress::~ISPingAddress()
{

}
//-----------------------------------------------------------------------------
QString ISPingAddress::GetErrorString() const
{
	return ErrorString;
}
//-----------------------------------------------------------------------------
bool ISPingAddress::IsRunning() const
{
	return Running;
}
//-----------------------------------------------------------------------------
int ISPingAddress::GetLastElapsed() const
{
	return LastElapsed;
}
//-----------------------------------------------------------------------------
void ISPingAddress::SetAddress(const QString &ip_address)
{
	IPAddress = ip_address;
}
//-----------------------------------------------------------------------------
QString ISPingAddress::GetAddress() const
{
	return IPAddress;
}
//-----------------------------------------------------------------------------
void ISPingAddress::Ping()
{
	Running = true;

	if (CountingTime)
	{
		CountingTime->Restart();
	}
	else
	{
		CountingTime = new ISCountingTime(this);
	}
	
	QHostInfo::lookupHost(IPAddress, this, SLOT(PingResult(const QHostInfo &)));
}
//-----------------------------------------------------------------------------
void ISPingAddress::PingResult(const QHostInfo &HostInfo)
{
	Running = false;
	LastElapsed = CountingTime->GetElapsed();

	if (HostInfo.error() == QHostInfo::NoError)
	{
		ErrorString.clear();
	}
	else
	{
		ErrorString = HostInfo.errorString();
	}

	emit Finished();
}
//-----------------------------------------------------------------------------
