#include "StdAfx.h"
#include "ISSMSHelper.h"
//-----------------------------------------------------------------------------
ISSMSHelper::ISSMSHelper()
{

}
//-----------------------------------------------------------------------------
ISSMSHelper::~ISSMSHelper()
{

}
//-----------------------------------------------------------------------------
PMetaServerRequest* ISSMSHelper::OnConvertJsonToSMSRequest(const QByteArray &ServerRequest)
{
	QJsonDocument JsonDocument = QJsonDocument::fromJson(ServerRequest);
	if (!JsonDocument.isEmpty())
	{
		QVariantMap VariantMap = JsonDocument.object().toVariantMap();
		if (VariantMap.count())
		{
			PMetaServerRequest *ServerRequest = new PMetaServerRequest();
			ServerRequest->SetID(VariantMap.value("id").toInt());
			ServerRequest->SetCountMessages(VariantMap.value("cnt").toInt());
			ServerRequest->SetCost(VariantMap.value("cost").toDouble());
			ServerRequest->SetBalance(VariantMap.value("balance").toDouble());
			return ServerRequest;
		}
	}

	return nullptr;
}
//-----------------------------------------------------------------------------
