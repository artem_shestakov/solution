#include "StdAfx.h"
#include "ISSMS.h"
#include "EXDefines.h"
//#include "ISSettingsDatabase.h"
#include "ISAssert.h"
#include "ISNetwork.h"
//-----------------------------------------------------------------------------
ISSMS::ISSMS(QObject *parent) : QObject(parent)
{
	this->Number = QString();
	this->MessageText = QString();
	this->ErrorText = QString();
}
//-----------------------------------------------------------------------------
ISSMS::~ISSMS()
{

}
//-----------------------------------------------------------------------------
void ISSMS::SetNumber(const QString &Number)
{
	this->Number = Number;
}
//-----------------------------------------------------------------------------
void ISSMS::SetMessageText(const QString &MessageText)
{
	this->MessageText = MessageText;
}
//-----------------------------------------------------------------------------
bool ISSMS::SendSMS()
{
	IS_ASSERT(Number.length(), "Number not valid");
	IS_ASSERT(MessageText.length(), "MessageText not valid");
	
	ErrorText.clear();
	ReplyData.clear();

	bool Result = false;

	QString QueryText = GenerateQuery(Number, MessageText);

	QEventLoop EventLoop;
	QNetworkAccessManager NetworkAccessManager;
	connect(&NetworkAccessManager, SIGNAL(finished(QNetworkReply *)), &EventLoop, SLOT(quit()));

	QUrl Url(QueryText);
	QNetworkRequest NetworkRequest(Url);
	QNetworkReply *NetworkReply = NetworkAccessManager.get(NetworkRequest);
	if (NetworkReply)
	{
		EventLoop.exec();

		QNetworkReply::NetworkError ReplyError = NetworkReply->error();
		if (ReplyError == QNetworkReply::NoError)
		{
			ReplyData = NetworkReply->readAll();
			Result = true;
		}
		else
		{
			ErrorText = NetworkReply->errorString();
		}

		delete NetworkReply;
		NetworkReply = nullptr;
	}

	Number.clear();

	return Result;
}
//-----------------------------------------------------------------------------
QString ISSMS::GetErrorText() const
{
	return ErrorText;
}
//-----------------------------------------------------------------------------
QByteArray ISSMS::GetReplyData() const
{
	return ReplyData;
}
//-----------------------------------------------------------------------------
QString ISSMS::GenerateQuery(const QString &Number, const QString &Message)
{
	QString Query = QString();

	//QString Login = ISSettingsDatabase::GetInstance().GetValue(DefinesSettingsDatabase::NameSMS, DefinesSettingsDatabase::SMS::Login).toString();
	//QString Password = ISSettingsDatabase::GetInstance().GetValue(DefinesSettingsDatabase::NameSMS, DefinesSettingsDatabase::SMS::Password).toString();

	Query += "https://smsc.ru/sys/send.php?";
	//Query += QString("login=%1&").arg(Login);
	//Query += QString("psw=%1&").arg(Password);
	Query += QString("phones=%1&").arg(Number);
	Query += QString("mes=%1&").arg(Message);
	Query += QString("cost=3&");
	Query += QString("charset=utf-8&");
	Query += QString("fmt=3");

	return Query;
}
//-----------------------------------------------------------------------------
