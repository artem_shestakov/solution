#pragma once
//-----------------------------------------------------------------------------
#include "isnetwork_global.h"
#include "ISCountingTime.h"
//-----------------------------------------------------------------------------
class ISNETWORK_EXPORT ISPingAddress : public QObject
{
	Q_OBJECT

signals:
	void Finished();

public:
	ISPingAddress(const QString &ip_address, QObject *parent = 0);
	virtual ~ISPingAddress();

	QString GetErrorString() const; //�������� ����� ������
	bool IsRunning() const; //�������� ������ ������
	int GetLastElapsed() const; //�������� ����� ����������� �� ����
	void SetAddress(const QString &ip_address); //�������� �����
	QString GetAddress() const; //�������� �����

	void Ping(); //���� ������

protected slots:
	void PingResult(const QHostInfo &HostInfo); //��������� �����

private:
	QString IPAddress; //����� �����
	bool Running; //������ ������
	int LastElapsed; //����������� ����� �� ����
	QString ErrorString; //����� ������
	ISCountingTime *CountingTime;
};
//-----------------------------------------------------------------------------
