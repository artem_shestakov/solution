#include "StdAfx.h"
#include "PMetaServerRequest.h"
//-----------------------------------------------------------------------------
PMetaServerRequest::PMetaServerRequest()
{
	this->ID = -1;
	this->CountMessages = -1;
	this->Cost = -1;
	this->Balance = -1;
}
//-----------------------------------------------------------------------------
PMetaServerRequest::~PMetaServerRequest()
{

}
//-----------------------------------------------------------------------------
void PMetaServerRequest::SetID(int id)
{
	ID = id;
}
//-----------------------------------------------------------------------------
int PMetaServerRequest::GetID() const
{
	return ID;
}
//-----------------------------------------------------------------------------
void PMetaServerRequest::SetCountMessages(int count_messages)
{
	CountMessages = count_messages;
}
//-----------------------------------------------------------------------------
int PMetaServerRequest::OnGetCountMessages() const
{
	return CountMessages;
}
//-----------------------------------------------------------------------------
void PMetaServerRequest::SetCost(double cost)
{
	Cost = cost;
}
//-----------------------------------------------------------------------------
double PMetaServerRequest::GetCost() const
{
	return Cost;
}
//-----------------------------------------------------------------------------
void PMetaServerRequest::SetBalance(double balance)
{
	Balance = balance;
}
//-----------------------------------------------------------------------------
double PMetaServerRequest::GetBalance() const
{
	return Balance;
}
//-----------------------------------------------------------------------------
