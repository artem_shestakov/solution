#include "StdAfx.h"
#include "ISEmailAddress.h"
//-----------------------------------------------------------------------------
ISEmailAddress::ISEmailAddress(const QString & address, const QString & name)
{
    this->address = address;
    this->name = name;
}
//-----------------------------------------------------------------------------
ISEmailAddress::~ISEmailAddress()
{

}
//-----------------------------------------------------------------------------
void ISEmailAddress::setName(const QString & name)
{
    this->name = name;
}
//-----------------------------------------------------------------------------
void ISEmailAddress::setAddress(const QString & address)
{
    this->address = address;
}
//-----------------------------------------------------------------------------
const QString & ISEmailAddress::getName() const
{
    return name;
}
//-----------------------------------------------------------------------------
const QString & ISEmailAddress::getAddress() const
{
    return address;
}
//-----------------------------------------------------------------------------
