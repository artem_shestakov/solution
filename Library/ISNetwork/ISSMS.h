#pragma once
//-----------------------------------------------------------------------------
#include "isnetwork_global.h"
//-----------------------------------------------------------------------------
class ISNETWORK_EXPORT ISSMS : public QObject
{
	Q_OBJECT

public:
	ISSMS(QObject *parent = 0);
	virtual ~ISSMS();

	void SetNumber(const QString &Number); //�������� ����� ���������� ���
	void SetMessageText(const QString &MessageText); //�������� ����� ���
	bool SendSMS(); //�������� ���
	QString GetErrorText() const; //�������� ����� ������
	QByteArray GetReplyData() const;

protected:
	QString GenerateQuery(const QString &Number, const QString &Message); //��������� ���������� ������� �� �������� ���

private:
	QString Number; //����� ���������� ���
	QString MessageText; //����� ��� ���������
	QString ErrorText; //����� ������
	QByteArray ReplyData; //�����
};
//-----------------------------------------------------------------------------
