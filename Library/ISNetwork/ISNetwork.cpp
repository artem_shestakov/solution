#include "StdAfx.h"
#include "ISNetwork.h"
#include "EXDefines.h"
#include "ISDebug.h"
//-----------------------------------------------------------------------------
ISNetwork::ISNetwork(QObject *parent) : QObject(parent)
{

}
//-----------------------------------------------------------------------------
ISNetwork::~ISNetwork()
{

}
//-----------------------------------------------------------------------------
bool ISNetwork::CheckAccessInternet()
{
	bool Result = false;

	QNetworkAccessManager Manager;
	QNetworkRequest Request(QUrl("http://google.com"));
	QNetworkReply *Reply = Manager.get(Request);
	
	QEventLoop EventLoop;
	connect(Reply, &QNetworkReply::finished, &EventLoop, &QEventLoop::quit);
	EventLoop.exec();

	if (Reply->error() == QNetworkReply::NoError)
	{
		Result = true;
	}
	else
	{
		ISDebug::ShowWarningString(Reply->errorString());
	}
	
	if (Reply)
	{
		delete Reply;
		Reply = nullptr;
	}

	return Result;
}
//-----------------------------------------------------------------------------
