#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISStorageFileLoader : public QObject
{
	Q_OBJECT

signals:
	void ChangeProgressFileMaximum(int Maximum);
	void LoadingFile();

public:
	ISStorageFileLoader(const QString &file_path, QObject *parent = 0);
	virtual ~ISStorageFileLoader();

	QString GetErrorString() const;
	int GetStorageFileID() const;

	bool Load();

private:
	QString ErrorString;
	QString FilePath;
	int StorageFileID;
};
//-----------------------------------------------------------------------------
