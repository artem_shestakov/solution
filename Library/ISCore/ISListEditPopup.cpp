#include "StdAfx.h"
#include "ISListEditPopup.h"
#include "EXDefines.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISQuery.h"
#include "ISMetaDataHelper.h"
#include "PMetaClassForeign.h"
#include "ISSystem.h"
#include "ISPushButton.h"
//-----------------------------------------------------------------------------
ISListEditPopup::ISListEditPopup(QWidget *ComboBox) : ISInterfaceForm(ComboBox)
{
	MetaForeign = nullptr;

	setWindowFlags(Qt::Popup);
	setAttribute(Qt::WA_DeleteOnClose, false);

	QVBoxLayout *LayoutFrame = new QVBoxLayout();
	LayoutFrame->setContentsMargins(LAYOUT_MARGINS_5_PX);

	QFrame *Frame = new QFrame(this);
	Frame->setFrameShape(QFrame::Box);
	Frame->setFrameShadow(QFrame::Raised);
	Frame->setLayout(LayoutFrame);
	GetMainLayout()->addWidget(Frame);

	LineEdit = new ISLineEdit(this);
	LineEdit->SetPlaceholderText(LOCALIZATION("Search") + "...");
	LineEdit->SetIcon(BUFFER_ICONS("Search"));
	connect(LineEdit, &ISLineEdit::ValueChange, this, &ISListEditPopup::Search);
	LayoutFrame->addWidget(LineEdit);

	ListWidget = new ISListWidget(this);
	ListWidget->setCursor(CURSOR_POINTING_HAND);
	connect(ListWidget, &QListWidget::itemClicked, this, &ISListEditPopup::ItemClicked);
	LayoutFrame->addWidget(ListWidget);

	QStatusBar *StatusBar = new QStatusBar(this);
	LayoutFrame->addWidget(StatusBar);

	LabelCountRow = new QLabel(StatusBar);
	StatusBar->addWidget(LabelCountRow);

	LabelSearch = new QLabel(StatusBar);
	LabelSearch->setVisible(false);
	StatusBar->addWidget(LabelSearch);

	ISPushButton *ButtonHide = new ISPushButton(StatusBar);
	ButtonHide->setText(LOCALIZATION("Hide"));
	connect(ButtonHide, &ISPushButton::clicked, this, &ISListEditPopup::hide);
	StatusBar->addPermanentWidget(ButtonHide);
}
//-----------------------------------------------------------------------------
ISListEditPopup::~ISListEditPopup()
{

}
//-----------------------------------------------------------------------------
void ISListEditPopup::showEvent(QShowEvent *e)
{
	ISSystem::SetWaitGlobalCursor(true);
	
	LoadDataFromQuery();
	LineEdit->SetFocus();
	ISInterfaceForm::showEvent(e);

	ISSystem::SetWaitGlobalCursor(false);
}
//-----------------------------------------------------------------------------
void ISListEditPopup::hideEvent(QHideEvent *e)
{
	ISSystem::SetWaitGlobalCursor(true);

	ListWidget->Clear();
	LineEdit->Clear();
	CurrentValue.clear();
	ISInterfaceForm::hideEvent(e);

	ISSystem::SetWaitGlobalCursor(false);

	emit Hided();
}
//-----------------------------------------------------------------------------
void ISListEditPopup::SetMetaForeign(QObject *meta_foreign)
{
	MetaForeign = meta_foreign;
}
//-----------------------------------------------------------------------------
void ISListEditPopup::SetCurrentValue(const QVariant &current_value)
{
	CurrentValue = current_value;
}
//-----------------------------------------------------------------------------
QString ISListEditPopup::GetSqlFilter() const
{
	return SqlFilter;
}
//-----------------------------------------------------------------------------
void ISListEditPopup::SetSqlFilter(const QString &sql_filter)
{
	SqlFilter = sql_filter;
}
//-----------------------------------------------------------------------------
void ISListEditPopup::ClearSqlFilter()
{
	SqlFilter.clear();
}
//-----------------------------------------------------------------------------
void ISListEditPopup::Search(const QVariant &value)
{
	ISSystem::SetWaitGlobalCursor(true);

	QString SearchValue = value.toString().toLower();
	if (SearchValue.length())
	{
		int Founded = 0;
		int LastIndex = 0;
		for (int i = 0; i < ListWidget->count(); i++)
		{
			QListWidgetItem *ListWidgetItem = ListWidget->item(i);
			QString ItemText = ListWidgetItem->text().toLower();
			if (ItemText.contains(SearchValue))
			{
				Founded++;
				LastIndex = i;
				ListWidget->setItemHidden(ListWidgetItem, false);
			}
			else
			{
				ListWidget->setItemHidden(ListWidgetItem, true);
			}
		}

		if (Founded == 1)
		{
			QListWidgetItem *ListWidgetItem = ListWidget->item(LastIndex);
			ListWidget->setItemSelected(ListWidgetItem, true);
			ListWidget->setCurrentItem(ListWidgetItem);
			ListWidget->setFocus();
		}

		LabelSearch->setVisible(true);
		LabelSearch->setText(LOCALIZATION("Founded") + ": " + QString::number(Founded));
	}
	else
	{
		for (int i = 0; i < ListWidget->count(); i++)
		{
			ListWidget->setItemHidden(ListWidget->item(i), false);
		}

		LabelSearch->setVisible(false);
		LabelSearch->clear();
	}

	ISSystem::SetWaitGlobalCursor(false);
}
//-----------------------------------------------------------------------------
void ISListEditPopup::ItemClicked(QListWidgetItem *ListWidgetItem)
{
	emit Selected(ListWidgetItem->data(Qt::UserRole), ListWidgetItem->text());
	hide();
}
//-----------------------------------------------------------------------------
void ISListEditPopup::LoadDataFromQuery()
{
	QString QueryText = MetaForeign->property("SqlQuery").toString();
	if (SqlFilter.length())
	{
		QueryText = ISMetaDataHelper::GenerateSqlQueryFromForeign(dynamic_cast<PMetaClassForeign*>(MetaForeign), SqlFilter);
	}

	ISQuery qSelect(QueryText);
	if (qSelect.Execute())
	{
		QListWidgetItem *CurrentItem = nullptr;

		while (qSelect.Next())
		{
			QVariant ID = qSelect.ReadColumn("ID");
			QString Value = qSelect.ReadColumn("Value").toString();

			QListWidgetItem *ListWidgetItem = new QListWidgetItem(ListWidget);
			ListWidgetItem->setText(Value);
			ListWidgetItem->setData(Qt::UserRole, ID);
			ListWidgetItem->setSizeHint(QSize(ListWidgetItem->sizeHint().width(), 25));

			if (CurrentValue == ID)
			{
				CurrentItem = ListWidgetItem;
			}
		}

		if (CurrentItem)
		{
			ListWidget->setItemSelected(CurrentItem, true);
			ListWidget->scrollToItem(CurrentItem, QAbstractItemView::ScrollHint::PositionAtCenter);
			CurrentItem->setFont(FONT_APPLICATION_BOLD);
			CurrentItem->setSelected(true);
		}

		LabelCountRow->setText(LOCALIZATION("RecordsCount") + ": " + QString::number(ListWidget->count()));
	}
}
//-----------------------------------------------------------------------------
void ISListEditPopup::EnterClicked()
{
	QListWidgetItem *ListWidgetItem = ListWidget->currentItem();
	if (ListWidgetItem)
	{
		ItemClicked(ListWidgetItem);
	}
}
//-----------------------------------------------------------------------------
