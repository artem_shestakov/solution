#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISTabBarMain : public QTabBar
{
	Q_OBJECT

signals:
	void MidButtonClicked(int Index);
	void DuplicateWindow(int Index);
	void SeparateWindow(int Index);
	void FixedTab(int Index);

public:
	ISTabBarMain(QWidget *parent = 0);
	virtual ~ISTabBarMain();

	void AddFixedTab(const QString &TabUID); //��������� �������
	void RemoveFixedTab(const QString &TabUID); //��������� �������
	bool CheckFixedTab(const QString &TabUID); //�������� - ���������� �� �������

protected:
	void mousePressEvent(QMouseEvent *e);
	void mouseMoveEvent(QMouseEvent *e);
	void mouseReleaseEvent(QMouseEvent *e);
	void wheelEvent(QWheelEvent *e);
	void CreateContextMenu();
	void SetVisibleContextActions(bool Visible); //���������� ��� ������ ������������ ����
	
	void CloseAllTabs();
	void CloseRightTabs();

private:
	QPointF PressPos;
	bool MouseClick;
	
	QMenu *ContextMenu;
	QAction *ActionFixedTab;
	QAction *ActionCloseTab;
	QAction *ActionCloseRightTabs;
	QAction *ActionCloseAllTabs;
	QAction *ActionDuplicate;
	QAction *ActionSeparateWindow;

	int MouseRightClickTabIndex;

	QList<QString> FixedTabs;
};
//-----------------------------------------------------------------------------
