#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
#include "ISFieldEditBase.h"
#include "PMetaClassTable.h"
#include "ISPushButton.h"
#include "ISListEditPopup.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISListEdit : public ISFieldEditBase
{
	Q_OBJECT

public:
	Q_INVOKABLE ISListEdit(QObject *MetaField, QWidget *parent);
	Q_INVOKABLE ISListEdit(QWidget *parent);
	virtual ~ISListEdit();

	void SetValue(const QVariant &value) override; //�������� �������������
	QVariant GetValue() const override; //���������� �������������
	void Clear() override;

	QString GetCurrentText() const; //�������� ��������� ������������ �������
	void SetEnabled(bool Enabled);
	
	void SetSqlFilter(const QString &sql_filter); //�������� ������
	void ClearSqlFilter(); //�������� SQL-������

public slots:
	void SetReadOnly(bool read_only);

protected:
	void SelectedValue(const QVariant &id, const QString &text);
	
	void ShowPopup();
	void HidedPopup();
	void ShowListForm();
	
	void CreateObject();
	void EditObject();

private:
	PMetaClassTable *MetaTable;

	ISPushButton *ButtonMain;
	QToolButton *ButtonList;
	QAction *ActionCreate;
	QAction *ActionEdit;
	ISListEditPopup *ListEditPopup;

	QVariant ID;
};
//-----------------------------------------------------------------------------
