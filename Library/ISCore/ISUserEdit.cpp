#include "StdAfx.h"
#include "ISUserEdit.h"
#include "EXDefines.h"
#include "ISQuery.h"
#include "ISLocalization.h"
//-----------------------------------------------------------------------------
static QString QS_USERS = PREPARE_QUERY("SELECT usrs_id, concat(usrs_surname, ' ', usrs_name, ' ', usrs_patronymic) FROM _users WHERE NOT usrs_isdeleted ORDER BY concat");
//-----------------------------------------------------------------------------
ISUserEdit::ISUserEdit(QObject *MetaField, QWidget *parent) : ISComboEdit(MetaField, parent)
{
	SetEditable(false);
	SetVisibleClear(false);
	SetCursor(CURSOR_POINTING_HAND);

	ISQuery qSelect(QS_USERS);
	if (qSelect.Execute())
	{
		if (qSelect.GetCountResultRows())
		{
			AddItem(LOCALIZATION("All"), QVariant());

			while (qSelect.Next())
			{
				AddItem(qSelect.ReadColumn("concat").toString(), qSelect.ReadColumn("usrs_id").toInt());
			}
		}
	}
}
//-----------------------------------------------------------------------------
ISUserEdit::ISUserEdit(QWidget *parent) : ISUserEdit(nullptr, parent)
{

}
//-----------------------------------------------------------------------------
ISUserEdit::~ISUserEdit()
{

}
//-----------------------------------------------------------------------------
