#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISDelegateColor : public QStyledItemDelegate
{
	Q_OBJECT

public:
	ISDelegateColor(QObject *parent);
	virtual ~ISDelegateColor();

protected:
	void paint(QPainter *Painter, const QStyleOptionViewItem &Option, const QModelIndex &Index) const;
};
//-----------------------------------------------------------------------------
