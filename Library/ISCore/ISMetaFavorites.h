#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISMetaFavorites : public QObject
{
	Q_OBJECT

public:
	ISMetaFavorites(QObject *parent = 0);
	virtual ~ISMetaFavorites();

	void AddObjectID(int ObjectID);
	bool ContainsObject(int ObjectID);
	void DeleteObjectID(int ObjectID);

private:
	QVector<int> Objects;
};
//-----------------------------------------------------------------------------
