#include "StdAfx.h"
#include "ISListObjectForm.h"
//-----------------------------------------------------------------------------
ISListObjectForm::ISListObjectForm(PMetaClassTable *MetaTable, int ParentObjectID, QWidget *parent) : ISListBaseForm(MetaTable, parent)
{
	SetParentObjectID(ParentObjectID);
	GetQueryModel()->SetParentObjectIDClassFilter(ParentObjectID);
}
//-----------------------------------------------------------------------------
ISListObjectForm::~ISListObjectForm()
{

}
//-----------------------------------------------------------------------------
