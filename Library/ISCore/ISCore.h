#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
#include "ISFieldEditBase.h"
#include "ISObjectFormBase.h"
#include "PMetaClassTable.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISCore : public QObject
{
	Q_OBJECT

public:
	ISCore();
	virtual ~ISCore();

	static ISFieldEditBase* CreateColumnForField(QWidget *ParentWidget, PMetaClassField *MetaField, QString &ControlWidgetName = QString());
	static ISFieldEditBase* CreateColumnForField(QWidget *ParentWidget, ISNamespace::FieldType DataType, QString &ControlWidgetName = QString());

	static ISObjectFormBase* CreateObjectForm(ISNamespace::ObjectFormType FormType, PMetaClassTable *MetaTable, int ObjectID = 0, QWidget *parent = 0);
	static bool DeleteOrRecoveryObject(ISNamespace::DeleteRecoveryObject DeleteOrRecovery, const QString &TableName, const QString &TableAlias, int ID, const QString &LocalListName); //�������/������������ ������
	static bool DeleteCascadeObject(const QString &TableName, const QString &TableAlias, int ObjectID); //������� ������ ��������

	static void ChangeUser(); //����� ������������
	static void RestartApplication(); //���������� ���������
	static void ExitApplication(); //����� �� ���������

	static QString GetObjectName(PMetaClassTable *MetaTable, int ObjectID); //�������� ������������ �������
};
//-----------------------------------------------------------------------------
