#include "StdAfx.h"
#include "ISDelegateBoolean.h"
//-----------------------------------------------------------------------------
ISDelegateBoolean::ISDelegateBoolean(QObject *parent) : QStyledItemDelegate(parent)
{

}
//-----------------------------------------------------------------------------
ISDelegateBoolean::~ISDelegateBoolean()
{

}
//-----------------------------------------------------------------------------
void ISDelegateBoolean::paint(QPainter *Painter, const QStyleOptionViewItem &Option, const QModelIndex &Index) const
{
	QStyleOptionViewItem OptionViewItem = Option;
	initStyleOption(&OptionViewItem, Index);
	OptionViewItem.text.clear();

	const QWidget *Widget = OptionViewItem.widget;
	QStyle *Style = Widget ? Widget->style() : QApplication::style();
	Style->drawControl(QStyle::CE_ItemViewItem, &OptionViewItem, Painter, Widget);

	QStyleOptionButton ButtonStyle;
	*(QStyleOption*)&ButtonStyle = OptionViewItem;

	ButtonStyle.rect = QApplication::style()->subElementRect(QStyle::SE_CheckBoxIndicator, &ButtonStyle, Widget);
	ButtonStyle.rect.moveCenter(Option.rect.center());

	QVariant Value = Index.model()->data(Index, Qt::DisplayRole);
	if (Value.toBool())
	{
		ButtonStyle.state = QStyle::State_On;
	}
	else
	{
		ButtonStyle.state = QStyle::State_Off;
	}

	ButtonStyle.state |= QStyle::State_Enabled;

	QApplication::style()->drawControl(QStyle::CE_CheckBox, &ButtonStyle, Painter, Widget);
}
//-----------------------------------------------------------------------------
