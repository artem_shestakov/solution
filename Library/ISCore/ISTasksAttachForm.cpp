#include "StdAfx.h"
#include "ISTasksAttachForm.h"
#include "EXDefines.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISButtonDialog.h"
#include "ISQuery.h"
//-----------------------------------------------------------------------------
static QString QS_TASKS = PREPARE_QUERY("SELECT task_id, task_name FROM _tasks WHERE NOT task_isdeleted ORDER BY task_id DESC");
//-----------------------------------------------------------------------------
ISTasksAttachForm::ISTasksAttachForm(QWidget *parent) : ISInterfaceDialogForm(parent)
{
	setWindowTitle(LOCALIZATION("Task.Select"));
	setWindowIcon(BUFFER_ICONS("Task.Attach"));

	GetMainLayout()->setContentsMargins(LAYOUT_MARGINS_5_PX);

	GetMainLayout()->addWidget(new QLabel(LOCALIZATION("Tasks"), this));

	ListWidget = new ISListWidget(this);
	GetMainLayout()->addWidget(ListWidget);

	ISButtonDialog *ButtonDialog = new ISButtonDialog(this);
	connect(ButtonDialog, &ISButtonDialog::Apply, this, &ISTasksAttachForm::Select);
	connect(ButtonDialog, &ISButtonDialog::Close, this, &ISTasksAttachForm::close);
	GetMainLayout()->addWidget(ButtonDialog);

	LoadTasks();
}
//-----------------------------------------------------------------------------
ISTasksAttachForm::~ISTasksAttachForm()
{

}
//-----------------------------------------------------------------------------
int ISTasksAttachForm::GetSelectedTaskID() const
{
	return ListWidget->currentItem()->data(Qt::UserRole).toInt();
}
//-----------------------------------------------------------------------------
QString ISTasksAttachForm::GetSelectedTaskName() const
{
	return ListWidget->currentItem()->text();
}
//-----------------------------------------------------------------------------
void ISTasksAttachForm::LoadTasks()
{
	ISQuery qSelectTasks(QS_TASKS);
	if (qSelectTasks.Execute())
	{
		if (qSelectTasks.GetCountResultRows())
		{
			while (qSelectTasks.Next())
			{
				QListWidgetItem *ListWidgetItem = new QListWidgetItem(ListWidget);
				ListWidgetItem->setText(qSelectTasks.ReadColumn("task_name").toString());
				ListWidgetItem->setSizeHint(QSize(ListWidgetItem->sizeHint().width(), 30));
				ListWidgetItem->setData(Qt::UserRole, qSelectTasks.ReadColumn("task_id").toString());
			}

			ListWidget->setItemSelected(ListWidget->item(0), true);
		}
	}
}
//-----------------------------------------------------------------------------
void ISTasksAttachForm::Select()
{
	if (ListWidget->currentItem())
	{
		SetResult(true);
		close();
	}
}
//-----------------------------------------------------------------------------
