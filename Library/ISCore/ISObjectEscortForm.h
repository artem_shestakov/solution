#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISObjectEscortForm : public QWidget
{
	Q_OBJECT

public:
	ISObjectEscortForm(int ParentObjectID, QWidget *parent = 0);
	virtual ~ISObjectEscortForm();
};
//-----------------------------------------------------------------------------
