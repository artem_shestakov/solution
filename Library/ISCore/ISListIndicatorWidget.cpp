#include "StdAfx.h"
#include "ISListIndicatorWidget.h"
#include "EXDefines.h"
#include "ISStyleSheet.h"
//-----------------------------------------------------------------------------
ISListIndicatorWidget::ISListIndicatorWidget(QWidget *parent) : QWidget(parent)
{
	setVisible(false);
	setCursor(CURSOR_WAIT);

	QHBoxLayout *Layout = new QHBoxLayout();
	setLayout(Layout);

	LabelImage = new QLabel(this);
	Layout->addWidget(LabelImage);

	LabelText = new QLabel(this);
	LabelText->setStyleSheet(STYLE_SHEET("QLabel.Color.Gray"));
	LabelText->setFont(FONT_TAHOMA_14_BOLD);
	Layout->addWidget(LabelText);
}
//-----------------------------------------------------------------------------
ISListIndicatorWidget::~ISListIndicatorWidget()
{

}
//-----------------------------------------------------------------------------
void ISListIndicatorWidget::hideEvent(QHideEvent *e)
{
	QWidget::hideEvent(e);

	QMovie *Movie = LabelImage->movie();
	if (Movie)
	{
		delete Movie;
		Movie = nullptr;
	}

	LabelImage->setMovie(nullptr);
	LabelImage->setPixmap(QPixmap());
	LabelText->clear();
	setToolTip(QString());
}
//-----------------------------------------------------------------------------
void ISListIndicatorWidget::SetVisibleAnimation(bool Visible, QMovie *Movie)
{
	if (Visible)
	{
		Movie->start();
	}
	else
	{
		Movie->stop();

		QMovie *OldMovie = LabelImage->movie();
		if (OldMovie)
		{
			delete OldMovie;
			OldMovie = nullptr;
		}
	}

	LabelImage->setMovie(Movie);
	adjustSize();
}
//-----------------------------------------------------------------------------
void ISListIndicatorWidget::SetPixmap(const QPixmap &Pixmap)
{
	LabelImage->setPixmap(Pixmap.scaled(SIZE_32_32, Qt::KeepAspectRatio));
	adjustSize();
}
//-----------------------------------------------------------------------------
void ISListIndicatorWidget::SetText(const QString &Text)
{
	LabelText->setText(Text);
	adjustSize();
}
//-----------------------------------------------------------------------------
