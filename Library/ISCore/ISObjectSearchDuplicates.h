#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
#include "PMetaClassTable.h"
#include "ISLineEdit.h"
#include "ISComboEdit.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISObjectSearchDuplicates : public QWidget
{
	Q_OBJECT

signals:
	void Search(const QString &SqlText);
	void Hided();

public:
	ISObjectSearchDuplicates(PMetaClassTable *meta_table, QWidget *parent = 0);
	virtual ~ISObjectSearchDuplicates();
	
	void Hide();
	void Visible();

protected:
	void CreateQuery();

private:
	PMetaClassTable *MetaTable;

	QGroupBox *GroupBox;
	ISLineEdit *LineEdit;
	ISComboEdit *ComboEdit;
	QToolButton *ButtonResult;

	bool VisibleFlag;
	int Heigth;
	QPropertyAnimation *PropertyAnimation;
};
//-----------------------------------------------------------------------------

