#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
#include "ISInterfaceDialogForm.h"
#include "ISNamespace.h"
#include "PMetaClassTable.h"
#include "ISComboEdit.h"
#include "ISFieldEditBase.h"
#include "ISPushButton.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISSearchForm : public ISInterfaceDialogForm
{
	Q_OBJECT

public:
	ISSearchForm(PMetaClassTable *meta_table, QWidget *parent = 0);
	virtual ~ISSearchForm();

	void showEvent(QShowEvent *e);

	ISNamespace::SearchFormResult GetSearchResult();
	QString GetField() const;
	QVariant GetValue() const;
	ISNamespace::SearchHow GetSearchHow();

protected:
	void LoadFields(); //���������� �����
	void FieldChanged(const QVariant &Value);
	void EnabledSearch(); //�������� ��������� ������ "�����"
	void EnterClicked() override;
	void EscapeClicked() override;

	void Search(); //�����
	void Clear(); //������� �����������
	void Close(); //�������� �����

private:
	PMetaClassTable *MetaTable;
	ISNamespace::SearchFormResult SearchResult;

	ISComboEdit *ComboFields;
	ISFieldEditBase *EditValue;
	QHBoxLayout *LayoutValue;
	QWidget *WidgetHowSearch;
	QRadioButton *RadioBeginString;
	QRadioButton *RadioEndString;
	QRadioButton *RadioPartString;
	QRadioButton *RadioExactMatch;
	
	ISPushButton *ButtonSearch;
};
//-----------------------------------------------------------------------------
