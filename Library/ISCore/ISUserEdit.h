#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
#include "ISComboEdit.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISUserEdit : public ISComboEdit
{
	Q_OBJECT

public:
	Q_INVOKABLE ISUserEdit(QObject *MetaField, QWidget *parent);
	Q_INVOKABLE ISUserEdit(QWidget *parent);
	virtual ~ISUserEdit();
};
//-----------------------------------------------------------------------------
