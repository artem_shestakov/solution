#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
#include "ISExportWorker.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISExportExcel : public ISExportWorker
{
	Q_OBJECT

public:
	ISExportExcel(QObject *parent = 0);
	virtual ~ISExportExcel();

	bool Prepare();
	bool Export();

private:
	QAxObject* ExcelApplication;
	QAxObject *Workbooks;
	QAxObject *Workbook;
	QAxObject *Sheets;
	QAxObject *Sheet;
};
//-----------------------------------------------------------------------------
