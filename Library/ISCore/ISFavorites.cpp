#include "StdAfx.h"
#include "ISFavorites.h"
#include "ISQuery.h"
#include "ISCountingTime.h"
#include "ISDebug.h"
#include "ISSystem.h"
#include "ISMetaUser.h"
//-----------------------------------------------------------------------------
static QString QS_FAVORITES = PREPARE_QUERY("SELECT fvts_tablename, fvts_objectid FROM _favorites WHERE fvts_user = :CurrentUserID");
//-----------------------------------------------------------------------------
static QString QI_FAVORITE = PREPARE_QUERY("INSERT INTO _favorites(fvts_user, fvts_tablename, fvts_tablelocalname, fvts_objectname, fvts_objectid) "
										   "VALUES(:User, :TableName, :TableLocalName, :ObjectName, :ObjectID)");
//-----------------------------------------------------------------------------
static QString QD_FAVORITE = PREPARE_QUERY("DELETE FROM _favorites WHERE fvts_tablename = :TableName AND fvts_objectid = :ObjectID");
//-----------------------------------------------------------------------------
static QString QD_ALL_FAVORITES = PREPARE_QUERY("DELETE FROM _favorites WHERE fvts_user = :CurrentUserID");
//-----------------------------------------------------------------------------
ISFavorites::ISFavorites() : QObject()
{

}
//-----------------------------------------------------------------------------
ISFavorites::~ISFavorites()
{

}
//-----------------------------------------------------------------------------
ISFavorites& ISFavorites::GetInstance()
{
	static ISFavorites Favorites;
	return Favorites;
}
//-----------------------------------------------------------------------------
void ISFavorites::Initialize(int UserID)
{
	ISCountingTime Time;

	ISQuery qSelectFavorites(QS_FAVORITES);
	qSelectFavorites.BindValue(":CurrentUserID", UserID);
	if (qSelectFavorites.Execute())
	{
		while (qSelectFavorites.Next())
		{
			QString TableName = qSelectFavorites.ReadColumn("fvts_tablename").toString();
			int ObjectID = qSelectFavorites.ReadColumn("fvts_objectid").toInt();

			if (FavoritesMap.contains(TableName))
			{
				FavoritesMap.value(TableName)->AddObjectID(ObjectID);
			}
			else
			{
				ISMetaFavorites *MetaFavortes = new ISMetaFavorites(this);
				MetaFavortes->AddObjectID(ObjectID);

				FavoritesMap.insert(TableName, MetaFavortes);
			}
		}
	}

	QString TimeInitialized = ISSystem::MillisecondsToString(Time.GetElapsed());
	ISDebug::ShowDebugString("Initialize Favorites time: " + TimeInitialized);
}
//-----------------------------------------------------------------------------
void ISFavorites::AddFavorite(const QString &TableName, const QString &TableLocalName, const QString &ObjectName, int ObjectID)
{
	ISQuery qInsertFavorite(QI_FAVORITE);
	qInsertFavorite.BindValue(":User", CURRENT_USER_ID);
	qInsertFavorite.BindValue(":TableName", TableName);
	qInsertFavorite.BindValue(":TableLocalName", TableLocalName);
	qInsertFavorite.BindValue(":ObjectName", ObjectName);
	qInsertFavorite.BindValue(":ObjectID", ObjectID);
	if (qInsertFavorite.Execute())
	{
		if (FavoritesMap.contains(TableName))
		{
			FavoritesMap.value(TableName)->AddObjectID(ObjectID);
		}
		else
		{
			ISMetaFavorites *MetaFavortes = new ISMetaFavorites();
			MetaFavortes->AddObjectID(ObjectID);
			
			FavoritesMap.insert(TableName, MetaFavortes);
		}
	}
}
//-----------------------------------------------------------------------------
bool ISFavorites::DeleteFavorite(const QString &TableName, int ObjectID)
{
	ISQuery qDeleteFavorite(QD_FAVORITE);
	qDeleteFavorite.BindValue(":TableName", TableName);
	qDeleteFavorite.BindValue(":ObjectID", ObjectID);
	bool Executed = qDeleteFavorite.Execute();

	if (Executed)
	{
		FavoritesMap.value(TableName)->DeleteObjectID(ObjectID);
	}

	return Executed;
}
//-----------------------------------------------------------------------------
void ISFavorites::DeleteAllFavorites(int CurrentUserID)
{
	ISQuery qDeleteFavorites(QD_ALL_FAVORITES);
	qDeleteFavorites.BindValue(":CurrentUserID", CurrentUserID);
	if (qDeleteFavorites.Execute())
	{
		FavoritesMap.clear();
	}
}
//-----------------------------------------------------------------------------
bool ISFavorites::CheckExistFavoriteObject(const QString &TableName, int ObjectID)
{
	if (FavoritesMap.contains(TableName))
	{
		bool ok = FavoritesMap.value(TableName)->ContainsObject(ObjectID);
		return ok;
	}

	return false;
}
//-----------------------------------------------------------------------------
