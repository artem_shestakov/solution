#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISEMail : public QObject
{
	Q_OBJECT

public:
	ISEMail();
	virtual ~ISEMail();

	static bool SendMail(int MailID, const QString &Subject, int Recipient); //�������� ������
	static bool Send(const QString &sender_mail, const QString &sender_password, const QString &server_mail, bool ssl, int port, const QString &recipient_mail, const QString &subject, const QString &text);
};
//-----------------------------------------------------------------------------
