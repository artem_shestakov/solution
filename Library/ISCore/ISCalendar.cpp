#include "StdAfx.h"
#include "ISCalendar.h"
#include "EXDefines.h"
#include "ISQuery.h"
#include "ISNotificationService.h"
#include "ISMessageBox.h"
#include "ISLocalization.h"
#include "ISAssert.h"
//-----------------------------------------------------------------------------
static QString QI_CALENDAR = PREPARE_QUERY("INSERT INTO _calendar(cldr_date, cldr_timealert, cldr_name, cldr_text, cldr_tablename, cldr_objectid) "
										   "VALUES(:Date, :TimeAlert, :Name, :Text, :TableName, :ObjectID)");
//-----------------------------------------------------------------------------
ISCalendar::ISCalendar() : QObject()
{

}
//-----------------------------------------------------------------------------
ISCalendar::~ISCalendar()
{

}
//-----------------------------------------------------------------------------
void ISCalendar::Insert(const QDate &Date, const QTime &TimeAlert, const QString &Name, const QVariant &Text, const QString &TableName, int ObjectID)
{
	ISQuery qInsert(QI_CALENDAR);
	qInsert.BindValue(":Date", Date);
	qInsert.BindValue(":TimeAlert", TimeAlert);
	qInsert.BindValue(":Name", Name);
	qInsert.BindValue(":Text", Text);

	if (TableName.length())
	{
		IS_ASSERT(ObjectID, "Invalid object id");

		qInsert.BindValue(":TableName", TableName);
		qInsert.BindValue(":ObjectID", ObjectID);
	}

	if (qInsert.Execute())
	{
		ISNotificationService::ShowNotification(LOCALIZATION("NotifyInsertedDone"));
	}
	else
	{
		ISMessageBox::ShowWarning(nullptr, LOCALIZATION("Message.Warning.NotifyNotInserted"));
	}
}
//-----------------------------------------------------------------------------
