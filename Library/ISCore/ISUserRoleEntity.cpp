#include "StdAfx.h"
#include "ISUserRoleEntity.h"
#include "ISQuery.h"
#include "ISMetaUser.h"
#include "ISSystem.h"
#include "ISMetaSystemsEntity.h"
#include "EXDefines.h"
#include "ISAssert.h"
#include "ISCountingTime.h"
#include "ISDebug.h"
//-----------------------------------------------------------------------------
static QString QS_USERGROUP_ACCESS = PREPARE_QUERY("SELECT usga_type, usga_subsystem, usga_table, usga_show, usga_create, usga_createcopy, usga_edit, usga_delete, usga_deletecascade, usga_updatelist, usga_showdeleted, usga_search, usga_export, usga_print, usga_systeminformation, usga_attachtask, usga_tablenavigation "
												  "FROM _usergroupaccess "
												  "WHERE NOT usga_isdeleted "
												  "AND usga_group = :GroupID");
//-----------------------------------------------------------------------------
static QString QS_USERGROUP_ACCESS_SPECIAL = PREPARE_QUERY("SELECT uast_uid "
														   "FROM _usergroupaccessspecial "
														   "LEFT JOIN _usergroupaccessspecialtype ON uast_id = ugas_accesstype "
														   "WHERE NOT ugas_isdeleted "
														   "AND ugas_group = :GroupID");
//-----------------------------------------------------------------------------
ISUserRoleEntity::ISUserRoleEntity() : QObject()
{
	
}
//-----------------------------------------------------------------------------
ISUserRoleEntity::~ISUserRoleEntity()
{

}
//-----------------------------------------------------------------------------
ISUserRoleEntity& ISUserRoleEntity::GetInstance()
{
	static ISUserRoleEntity UserRoleEntity;
	return UserRoleEntity;
}
//-----------------------------------------------------------------------------
void ISUserRoleEntity::InitializeAccess()
{
	if (ISMetaUser::GetInstance().GetData()->System)
	{
		return;
	}

	ISCountingTime Time;

	ISQuery qSelect(QS_USERGROUP_ACCESS);
	qSelect.BindValue(":GroupID", ISMetaUser::GetInstance().GetData()->GroupID);
	if (qSelect.Execute())
	{
		while (qSelect.Next())
		{
			ISUuid Type = qSelect.ReadColumn("usga_type");
			ISUuid SubSystemUID = qSelect.ReadColumn("usga_subsystem");
			ISUuid TableUID = qSelect.ReadColumn("usga_table");
			bool Show = qSelect.ReadColumn("usga_show").toBool();
			bool Create = qSelect.ReadColumn("usga_create").toBool();
			bool CreateCopy = qSelect.ReadColumn("usga_createcopy").toBool();
			bool Edit = qSelect.ReadColumn("usga_edit").toBool();
			bool Delete = qSelect.ReadColumn("usga_delete").toBool();
			bool DeleteCascade = qSelect.ReadColumn("usga_deletecascade").toBool();
			bool UpdateList = qSelect.ReadColumn("usga_updatelist").toBool();
			bool ShowDeleted = qSelect.ReadColumn("usga_showdeleted").toBool();
			bool Search = qSelect.ReadColumn("usga_search").toBool();
			bool Export = qSelect.ReadColumn("usga_export").toBool();
			bool Print = qSelect.ReadColumn("usga_print").toBool();
			bool SystemInformation = qSelect.ReadColumn("usga_systeminformation").toBool();
			bool AttachTask = qSelect.ReadColumn("usga_attachtask").toBool();
			bool TableNavigation = qSelect.ReadColumn("usga_tablenavigation").toBool();

			if (SubSystemUID.length() && TableUID.length())
			{
				IS_ASSERT(false, "Permission error");
			}

			PMetaUserPermission *UserPermission = new PMetaUserPermission(this);
			UserPermission->SetSubSystemUID(SubSystemUID);
			UserPermission->SetTableUID(TableUID);
			UserPermission->SetShow(Show);
			UserPermission->SetCreate(Create);
			UserPermission->SetCreateCopy(CreateCopy);
			UserPermission->SetEdit(Edit);
			UserPermission->SetDelete(Delete);
			UserPermission->SetDeleteCascade(DeleteCascade);
			UserPermission->SetUpdateList(UpdateList);
			UserPermission->SetShowDeleted(ShowDeleted);
			UserPermission->SetSearch(Search);
			UserPermission->SetExport(Export);
			UserPermission->SetPrint(Print);
			UserPermission->SetSystemInformation(SystemInformation);
			UserPermission->SetAttachTask(AttachTask);
			UserPermission->SetTableNavigation(TableNavigation);

			Permissions.append(UserPermission);
		}
	}

	QString TimeInitialized = ISSystem::MillisecondsToString(Time.GetElapsed());
	ISDebug::ShowDebugString("Initialize UserRole time: " + TimeInitialized);
}
//-----------------------------------------------------------------------------
void ISUserRoleEntity::InitializeAccessSpecial()
{
	if (ISMetaUser::GetInstance().GetData()->System)
	{
		return;
	}

	ISQuery qSelect(QS_USERGROUP_ACCESS_SPECIAL);
	qSelect.BindValue(":GroupID", ISMetaUser::GetInstance().GetData()->GroupID);
	if (qSelect.Execute())
	{
		while (qSelect.Next())
		{
			ISUuid PermissionSpecialUID = qSelect.ReadColumn("uast_uid");
			PermissionsSpecial.append(PermissionSpecialUID);
		}
	}
}
//-----------------------------------------------------------------------------
bool ISUserRoleEntity::CheckPermission(const QString &UID, ISNamespace::PermissionsType Type)
{
	//���� ������������ ��������� ��� � ������, � ������� ������� ������������ ������ ������
	if (ISMetaUser::GetInstance().GetData()->System || ISMetaUser::GetInstance().GetData()->GroupFullAccess)
	{
		return true;
	}

	bool Result = false;

	for (int i = 0; i < Permissions.count(); i++)
	{
		PMetaUserPermission *MetaPermission = Permissions.at(i);
		
		ISUuid TempUID;
		if (MetaPermission->GetSubSystemUID().length())
		{
			TempUID = MetaPermission->GetSubSystemUID();
		}
		else if (MetaPermission->GetTableUID().length())
		{
			TempUID = MetaPermission->GetTableUID();
		}

		if (TempUID == UID)
		{
			switch (Type)
			{
			case ISNamespace::PermissionsType::PT_Show: Result = MetaPermission->GetShow(); break;
			case ISNamespace::PermissionsType::PT_Create: Result = MetaPermission->GetCreate(); break;
			case ISNamespace::PermissionsType::PT_CreateCopy: Result = MetaPermission->GetCreateCopy(); break;
			case ISNamespace::PermissionsType::PT_Edit: Result = MetaPermission->GetEdit(); break;
			case ISNamespace::PermissionsType::PT_Delete: Result = MetaPermission->GetDelete(); break;
			case ISNamespace::PermissionsType::PT_DeleteCascade: Result = MetaPermission->GetDeleteCascade(); break;
			case ISNamespace::PermissionsType::PT_UpdateList: Result = MetaPermission->GetUpdateList(); break;
			case ISNamespace::PermissionsType::PT_ShowDeleted: Result = MetaPermission->GetShowDeleted(); break;
			case ISNamespace::PermissionsType::PT_ShowAll: Result = MetaPermission->GetShowAll(); break;
			case ISNamespace::PermissionsType::PT_Search: Result = MetaPermission->GetSearch(); break;
			case ISNamespace::PermissionsType::PT_Export: Result = MetaPermission->GetExport(); break;
			case ISNamespace::PermissionsType::PT_Print: Result = MetaPermission->GetPrint(); break;
			case ISNamespace::PermissionsType::PT_SystemInformation: Result = MetaPermission->GetSystemInformation(); break;
			case ISNamespace::PermissionsType::PT_TableNavigation: Result = MetaPermission->GetTableNavigation(); break;
			case ISNamespace::PermissionsType::PT_Unknown: IS_ASSERT(false, QString("Permission unknown for uid: %1").arg(UID)); break;
			}

			break;
		}
	}

	return Result;
}
//-----------------------------------------------------------------------------
bool ISUserRoleEntity::CheckPermissionSpecial(const QString &PermissionSpecialUID)
{
	//���� ������������ ��������� ��� � ������ ������������ ������ ������
	if (ISMetaUser::GetInstance().GetData()->System || ISMetaUser::GetInstance().GetData()->GroupFullAccess)
	{
		return true;
	}

	bool Check = PermissionsSpecial.contains(PermissionSpecialUID);
	return Check;
}
//-----------------------------------------------------------------------------
bool ISUserRoleEntity::CheckAllSubSystemsForSystem(const QString &SystemUID)
{
	bool Result = false;
	int Count = 0;
	
	ISMetaSystem *System = ISMetaSystemsEntity::GetInstance().GetSystem(SystemUID);
	if (System)
	{
		for (int i = 0; i < System->GetSubSystems().count(); i++)
		{
			ISMetaSubSystem *SubSystem = System->GetSubSystems().at(i);
			bool Access = CheckPermission(SubSystem->GetUID(), ISNamespace::PermissionsType::PT_Show);
			if (Access)
			{
				Count++;
			}
		}
	}

	if (Count)
	{
		Result = true;
	}

	return Result;
}
//-----------------------------------------------------------------------------
int ISUserRoleEntity::GetCountPermissions()
{
	return Permissions.count();
}
//-----------------------------------------------------------------------------
