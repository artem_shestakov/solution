#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISLicense : public QObject
{
	Q_OBJECT

public:
	ISLicense(const ISLicense &) = delete;
	ISLicense(ISLicense &&) = delete;
	ISLicense &operator=(const ISLicense &) = delete;
	ISLicense &operator=(ISLicense &&) = delete;
	~ISLicense();

	static ISLicense& GetInstance(); //�������� ��������

	void Initialize(); //�������������
	void ReduceCounter(); //��������� �������
	void Activate(const QString &key); //���������
	void Extend(int counter, const QVariant &CounterValue); //��������� ��������
	void ResetLicense(); //����� ��������

	QString GetErrorString() const; //�������� ������ � �������
	bool GetActivated() const; //�������� ���� ���������
	QString GetLicenseUID() const; //�������� ������������� ��������
	QString GetLicenseKey() const; //�������� ������������ ����
	int GetCounter() const; //�������� �������� ��������
	bool GetTableEmpty() const; //�������� ������� �������� � ����
	bool CheckExtend(const QVariant &EnteredCounterCode) const; //�������� ����������� �������� �������� ������

private:
	ISLicense();

	QString LicenseUID; //������������� ��������
	QString LicenseKey; //������������ ����
	int Counter; //������� ���������� ������ � ���������


	QString ErrorString;
};
//-----------------------------------------------------------------------------
