#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
#include "ISUuid.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISMetaUserData : public QObject
{
	Q_OBJECT

public:
	ISMetaUserData(QObject *parent = 0);
	virtual ~ISMetaUserData();

	bool System; //���������
	int ID; //������������� ������������
	QString Surname; //�������
	QString Name; //���
	QString Patronymic; //��������
	QDate Birthday; //���� ��������
	QString Login; //�����
	QString FullName; //���
	bool AccessAllowed; //������ ��������
	bool AccountLifetime; //������������ ���� �������� ������� ������
	QDate AccountLifetimeStart; //������ �������� ������� ������
	QDate AccountLifetimeEnd; //��������� �������� ������� ������
	
	ISUuid TelephonyType; //��� ���������
	QString TelephonyClass; //����� ���������

	QString AsteriskServer; //����� ������� Asterisk
	int AsteriskPort; //���� ������� Asterisk
	QString AsteriskLogin; //����� ������������ ������� Asterisk
	QString AsteriskPassword; //������ ������������ ������� Asterisk
	int AsteriskPattern; //���������� ����� ������������ ������� Asterisk

	int BeelinePattern; //���������� ����� ������
	
	int GroupID; //������������� ������ � ������� ������� ������������
	QString GroupName; //������������ ������ ������������
	bool GroupFullAccess; //������ ������
	QVector<QString> ExclusionProtocolType; //������ ����������� ������� ���������
};
//-----------------------------------------------------------------------------
