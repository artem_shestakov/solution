#include "StdAfx.h"
#include "ISSelectBaseListForm.h"
#include "EXDefines.h"
#include "ISLocalization.h"
//-----------------------------------------------------------------------------
ISSelectBaseListForm::ISSelectBaseListForm(PMetaClassTable *MetaTable, QWidget *parent) : ISListBaseForm(MetaTable, parent)
{
	resize(SIZE_640_480);
	setWindowTitle(LOCALIZATION("List") + ": " + MetaTable->GetLocalListName());
	GetMainLayout()->setContentsMargins(LAYOUT_MARGINS_10_PX);
	connect(this, &ISListBaseForm::AddFormFromTab, this, &ISSelectBaseListForm::CreateObjectForm);
}
//-----------------------------------------------------------------------------
ISSelectBaseListForm::~ISSelectBaseListForm()
{

}
//-----------------------------------------------------------------------------
void ISSelectBaseListForm::SetSqlFilter(const QString &SqlFilter)
{
	GetQueryModel()->SetClassFilter(SqlFilter);
}
//-----------------------------------------------------------------------------
void ISSelectBaseListForm::CreateObjectForm(QWidget *ObjectForm)
{
	ObjectForm->show();
}
//-----------------------------------------------------------------------------
void ISSelectBaseListForm::DoubleClickedTable(const QModelIndex &ModelIndex)
{
	Select();
}
//-----------------------------------------------------------------------------
void ISSelectBaseListForm::EnterClicked()
{
	Select();
}
//-----------------------------------------------------------------------------
void ISSelectBaseListForm::EscapeClicked()
{
	close();
}
//-----------------------------------------------------------------------------
