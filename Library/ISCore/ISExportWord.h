#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
#include "ISExportWorker.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISExportWord : public ISExportWorker
{
	Q_OBJECT

public:
	ISExportWord(QObject *parent = 0);
	virtual ~ISExportWord();

	bool Prepare();
	bool Export();

private:
	QAxObject *WordApplication;
};
//-----------------------------------------------------------------------------
