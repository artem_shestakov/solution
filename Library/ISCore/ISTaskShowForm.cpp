#include "StdAfx.h"
#include "ISTaskShowForm.h"
#include "EXDefines.h"
#include "ISAssert.h"
#include "ISQuery.h"
#include "ISLocalization.h"
#include "ISLineEdit.h"
#include "ISTextEdit.h"
//-----------------------------------------------------------------------------
static QString QS_TASK = PREPARE_QUERY("SELECT task_name, "
									   "concat(ow.usrs_surname, ' ', ow.usrs_name, ' ', ow.usrs_patronymic) AS owner, "
									   "tspr_name, "
									   "concat(ex.usrs_surname, ' ', ex.usrs_name, ' ', ex.usrs_patronymic) AS executor, "
									   "tsst_name, "
									   "tstp_name, "
									   "task_datelimit, "
									   "task_description "
									   "FROM _tasks "
									   "LEFT JOIN _users ow ON task_owner = ow.usrs_id "
									   "LEFT JOIN _taskpriority ON task_priority = tspr_uid "
									   "LEFT JOIN _users ex ON task_executor = ex.usrs_id "
									   "LEFT JOIN _taskstatus ON task_status = tsst_uid "
									   "LEFT JOIN _tasktype ON task_type = tstp_id "
									   "WHERE task_id = :TaskID");
//-----------------------------------------------------------------------------
ISTaskShowForm::ISTaskShowForm(int task_id, QWidget *parent) : ISInterfaceForm(parent)
{
	TaskID = task_id;

	ISQuery qSelect(QS_TASK);
	qSelect.BindValue(":TaskID", TaskID);
	IS_ASSERT(qSelect.ExecuteFirst(), qSelect.GetSqlQuery().lastError().text());

	QString TaskName = qSelect.ReadColumn("task_name").toString();
	QString TaskOwner = qSelect.ReadColumn("owner").toString();
	QString TaskPriority = qSelect.ReadColumn("tspr_name").toString();
	QString TaskExecutor = qSelect.ReadColumn("executor").toString();
	QString TaskStatus = qSelect.ReadColumn("tsst_name").toString();
	QString TaskType = qSelect.ReadColumn("tstp_name").toString();
	QString TaskDatelimit = qSelect.ReadColumn("task_datelimit").toDate().toString(DATE_FORMAT_STRING_V1);
	QString TaskDescription = qSelect.ReadColumn("task_description").toString();

	setWindowTitle(LOCALIZATION("Task") + ": " + TaskName);

	GetMainLayout()->setContentsMargins(LAYOUT_MARGINS_10_PX);

	QFormLayout *FormLayout = new QFormLayout();
	GetMainLayout()->addLayout(FormLayout);

	ISLineEdit *EditName = new ISLineEdit(this);
	EditName->SetValue(TaskName);
	EditName->SetReadOnly(true);
	FormLayout->addRow(LOCALIZATION("Named") + ":", EditName);

	ISLineEdit *EditOwner = new ISLineEdit(this);
	EditOwner->SetValue(TaskOwner);
	EditOwner->SetReadOnly(true);
	FormLayout->addRow(LOCALIZATION("Task.Author") + ":", EditOwner);

	ISLineEdit *EditPriority = new ISLineEdit(this);
	EditPriority->SetValue(TaskPriority);
	EditPriority->SetReadOnly(true);
	FormLayout->addRow(LOCALIZATION("Task.Priority") + ":", EditPriority);

	ISLineEdit *EditExecutor = new ISLineEdit(this);
	EditExecutor->SetValue(TaskExecutor);
	EditExecutor->SetReadOnly(true);
	FormLayout->addRow(LOCALIZATION("Task.Executor") + ":", EditExecutor);

	ISLineEdit *EditStatus = new ISLineEdit(this);
	EditStatus->SetValue(TaskStatus);
	EditStatus->SetReadOnly(true);
	FormLayout->addRow(LOCALIZATION("Task.Status") + ":", EditStatus);

	ISLineEdit *EditType = new ISLineEdit(this);
	EditType->SetValue(TaskType);
	EditType->SetReadOnly(true);
	FormLayout->addRow(LOCALIZATION("Task.Type") + ":", EditType);

	ISLineEdit *EditDateLimit = new ISLineEdit(this);
	EditDateLimit->SetValue(TaskDatelimit);
	EditDateLimit->SetReadOnly(true);
	FormLayout->addRow(LOCALIZATION("Task.DateLimit") + ":", EditDateLimit);

	ISTextEdit *EditDescription = new ISTextEdit(this);
	EditDescription->SetValue(TaskDescription);
	EditDescription->SetReadOnly(true);
	FormLayout->addRow(LOCALIZATION("Description") + ":", EditDescription);
}
//-----------------------------------------------------------------------------
ISTaskShowForm::~ISTaskShowForm()
{

}
//-----------------------------------------------------------------------------
void ISTaskShowForm::EscapeClicked()
{
	close();
}
//-----------------------------------------------------------------------------
