#include "StdAfx.h"
#include "ISSortingMetaTable.h"
#include "EXDefines.h"
//-----------------------------------------------------------------------------
ISSortingMetaTable::ISSortingMetaTable()
{
	
}
//-----------------------------------------------------------------------------
ISSortingMetaTable::~ISSortingMetaTable()
{

}
//-----------------------------------------------------------------------------
void ISSortingMetaTable::SetTableName(const QString &table_name)
{
	this->TableName = table_name;
}
//-----------------------------------------------------------------------------
QString ISSortingMetaTable::GetTableName() const
{
	return TableName;
}
//-----------------------------------------------------------------------------
void ISSortingMetaTable::SetFieldName(const QString &field_name)
{
	FieldName = field_name;
}
//-----------------------------------------------------------------------------
QString ISSortingMetaTable::GetFieldName() const
{
	return FieldName;
}
//-----------------------------------------------------------------------------
void ISSortingMetaTable::SetSortingType(Qt::SortOrder sorting)
{
	Sorting = sorting;
}
//-----------------------------------------------------------------------------
Qt::SortOrder ISSortingMetaTable::GetSortingType() const
{
	return Sorting;
}
//-----------------------------------------------------------------------------
