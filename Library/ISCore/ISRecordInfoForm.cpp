#include "StdAfx.h"
#include "ISRecordInfoForm.h"
#include "ISBuffer.h"
#include "ISLocalization.h"
#include "EXDefines.h"
#include "ISFieldEditBase.h"
#include "ISCore.h"
#include "ISQuery.h"
//-----------------------------------------------------------------------------
ISRecordInfoForm::ISRecordInfoForm(PMetaClassTable *MetaTable, int ObjectID, QWidget *parent) : ISInterfaceDialogForm(parent)
{
	setWindowIcon(BUFFER_ICONS("RecordInformation"));
	setWindowTitle(MetaTable->GetLocalName());

	ForbidResize();

	QFormLayout *FormLayout = new QFormLayout();
	FormLayout->setContentsMargins(LAYOUT_MARGINS_10_PX);
	GetMainLayout()->addLayout(FormLayout);

	QLabel *LabelTitle = new QLabel(this);
	LabelTitle->setText(LOCALIZATION("SystemInformation") + ":");
	LabelTitle->setFont(FONT_TAHOMA_12);
	LabelTitle->setStyleSheet("color: gray");
	FormLayout->addRow(LabelTitle, new QWidget(this));

	QFont Font = LabelTitle->font();
	Font.setUnderline(true);
	Font.setBold(true);
	LabelTitle->setFont(Font);

	for (int i = 0; i < MetaTable->GetSystemFields().count(); i++)
	{
		PMetaClassField *MetaField = MetaTable->GetSystemFields().at(i);

		QLabel *Label = new QLabel(this);
		Label->setFont(FONT_APPLICATION_BOLD);
		Label->setText(MetaField->GetLabelName() + ":");

		ISFieldEditBase *FieldEditBase = ISCore::CreateColumnForField(this, MetaField, MetaField->GetControlWidget());
		FieldEditBase->SetVisibleClear(false);
		FieldEditBase->SetReadOnly(true);
		FormLayout->addRow(Label, FieldEditBase);

		ISQuery qSelect("SELECT " + MetaTable->GetAlias() + "_" + MetaField->GetName() + " FROM " + MetaTable->GetName() + " WHERE " + MetaTable->GetAlias() + "_id = :ObjectID");
		qSelect.BindValue(":ObjectID", ObjectID);
		if (qSelect.ExecuteFirst())
		{
			QVariant Value = qSelect.ReadColumn(MetaTable->GetAlias() + "_" + MetaField->GetName());
			FieldEditBase->SetValue(Value);
		}
	}

	setFocus();
}
//-----------------------------------------------------------------------------
ISRecordInfoForm::~ISRecordInfoForm()
{

}
//-----------------------------------------------------------------------------
