#include "StdAfx.h"
#include "ISListViewForm.h"
#include "ISControls.h"
#include "ISMetaQuery.h"
//-----------------------------------------------------------------------------
ISListViewForm::ISListViewForm(PMetaClassTable *meta_table, QWidget *parent) : ISListBaseForm(meta_table, parent)
{
	GetToolBar()->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
	GetToolBar()->setVisible(false);
	GetTableView()->setContextMenuPolicy(Qt::NoContextMenu);

	connect(this, &ISListViewForm::DoubleClicked, this, &ISListViewForm::DoubleClickedList);
}
//-----------------------------------------------------------------------------
ISListViewForm::~ISListViewForm()
{

}
//-----------------------------------------------------------------------------
void ISListViewForm::AddCondition(const QString &Parameter, const QVariant &Value)
{
	Conditions.insert(Parameter, Value);
}
//-----------------------------------------------------------------------------
void ISListViewForm::RemoveCondition(const QString &Parameter)
{
	Conditions.remove(Parameter);
}
//-----------------------------------------------------------------------------
void ISListViewForm::SetClassFilter(const QString &class_filter)
{
	ClassFilter = class_filter;
}
//-----------------------------------------------------------------------------
void ISListViewForm::ClearClassFilter()
{
	ClassFilter.clear();
}
//-----------------------------------------------------------------------------
void ISListViewForm::LoadData()
{
	QTimer::singleShot(600, Qt::PreciseTimer, this, &ISListViewForm::Update);
}
//-----------------------------------------------------------------------------
void ISListViewForm::Update()
{
	GetSqlModel()->Clear();
	GetModelThread()->Execute(ISMetaQuery::GetQueryText(GetMetaTable()->GetName(), ClassFilter), Conditions);
}
//-----------------------------------------------------------------------------
void ISListViewForm::LoadDataAfterEvent()
{
	ISListBaseForm::LoadDataAfterEvent();

	for (QAction *Action : GetToolBar()->actions()) //����� ���� ������-�������� �� ������� � �� �������
	{
		Action->setVisible(false);
	}

	GetToolBar()->setVisible(true);
	GetAction(ISNamespace::AT_Update)->setVisible(true);
}
//-----------------------------------------------------------------------------
void ISListViewForm::DoubleClickedList(const QModelIndex &ModelIndex)
{

}
//-----------------------------------------------------------------------------
