#include "StdAfx.h"
#include "ISPickingObjectsListForm.h"
//-----------------------------------------------------------------------------
ISPickingObjectsListForm::ISPickingObjectsListForm(PMetaClassTable *MetaTable, QWidget *parent) : ISSelectBaseListForm(MetaTable, parent)
{
	GetAction(ISNamespace::AT_Picking)->setVisible(true);
}
//-----------------------------------------------------------------------------
ISPickingObjectsListForm::~ISPickingObjectsListForm()
{

}
//-----------------------------------------------------------------------------
void ISPickingObjectsListForm::Picking()
{
	QVector<int> Objects = GetSelectedIDs();
	if (Objects.count())
	{
		emit Matched(Objects);
	}

	close();
}
//-----------------------------------------------------------------------------
