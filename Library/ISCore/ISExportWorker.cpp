#include "StdAfx.h"
#include "ISExportWorker.h"
#include "ISLocalization.h"
//-----------------------------------------------------------------------------
ISExportWorker::ISExportWorker(QObject *parent) : QObject(parent)
{
	ErrorString = LOCALIZATION("Export.Error.NoError");
	Model = nullptr;
	Header = false;
	Canceled = false;
}
//-----------------------------------------------------------------------------
ISExportWorker::~ISExportWorker()
{

}
//-----------------------------------------------------------------------------
void ISExportWorker::Cancel()
{
	Canceled = true;
}
//-----------------------------------------------------------------------------
void ISExportWorker::SetTableName(const QString &LocalName)
{
	TableName = LocalName;
}
//-----------------------------------------------------------------------------
void ISExportWorker::SetModel(ISSqlModelCore *model)
{
	Model = model;
}
//-----------------------------------------------------------------------------
void ISExportWorker::SetFields(const QList<QString> &fields)
{
	Fields = fields;
}
//-----------------------------------------------------------------------------
void ISExportWorker::SetHeader(bool header)
{
	Header = header;
}
//-----------------------------------------------------------------------------
void ISExportWorker::SetSelectedRows(const QVector<int> &selected_rows)
{
	SelectedRows = selected_rows;
}
//-----------------------------------------------------------------------------
QString ISExportWorker::GetErrorString() const
{
	return ErrorString;
}
//-----------------------------------------------------------------------------
QVariant ISExportWorker::PrepareValue(const QVariant &Value) const
{
	QVariant Result;

	if (Value.type() == QVariant::Bool)
	{
		if (Value.toBool())
		{
			Result = LOCALIZATION("Yes");
		}
		else
		{
			Result = LOCALIZATION("No");
		}
	}
	else
	{
		Result = Value;
	}

	return Result;
}
//-----------------------------------------------------------------------------
