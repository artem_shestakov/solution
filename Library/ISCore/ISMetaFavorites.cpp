#include "StdAfx.h"
#include "ISMetaFavorites.h"
//-----------------------------------------------------------------------------
ISMetaFavorites::ISMetaFavorites(QObject *parent) : QObject(parent)
{
	
}
//-----------------------------------------------------------------------------
ISMetaFavorites::~ISMetaFavorites()
{

}
//-----------------------------------------------------------------------------
void ISMetaFavorites::AddObjectID(int ObjectID)
{
	Objects.append(ObjectID);
}
//-----------------------------------------------------------------------------
bool ISMetaFavorites::ContainsObject(int ObjectID)
{
	bool Contains = Objects.contains(ObjectID);
	return Contains;
}
//-----------------------------------------------------------------------------
void ISMetaFavorites::DeleteObjectID(int ObjectID)
{
	int Index = Objects.indexOf(ObjectID);
	Objects.remove(Index);
}
//-----------------------------------------------------------------------------
