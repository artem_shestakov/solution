#include "StdAfx.h"
#include "ISColumnSizer.h"
#include "ISQuery.h"
#include "ISMetaUser.h"
//-----------------------------------------------------------------------------
static QString QS_COLUMN_SIZE = PREPARE_QUERY("SELECT clsz_tablename, clsz_fieldname, clsz_size "
											  "FROM _columnsize "
											  "WHERE clsz_user = :UserID "
											  "ORDER BY clsz_id");
//-----------------------------------------------------------------------------
static QString QS_COLUMN_SIZE_COUNT = PREPARE_QUERY("SELECT COUNT(*) "
													"FROM _columnsize "
													"WHERE clsz_user = :UserID "
													"AND clsz_tablename = :TableName "
													"AND clsz_fieldname = :FieldName");
//-----------------------------------------------------------------------------
static QString QU_COLUMN_SIZE = PREPARE_QUERY("UPDATE _columnsize SET "
											  "clsz_size = :Size "
											  "WHERE clsz_user = :UserID "
											  "AND clsz_tablename = :TableName "
											  "AND clsz_fieldname = :FieldName");
//-----------------------------------------------------------------------------
static QString QI_COLUMN_SIZE = PREPARE_QUERY("INSERT INTO _columnsize(clsz_user, clsz_tablename, clsz_fieldname, clsz_size) "
											  "VALUES (:UserID, :TableName, :FieldName, :Size)");
//-----------------------------------------------------------------------------
static QString QD_COLUMN_SIZE = PREPARE_QUERY("DELETE FROM _columnsize WHERE clsz_user = :UserID");
//-----------------------------------------------------------------------------
ISColumnSizer::ISColumnSizer() : QObject()
{

}
//-----------------------------------------------------------------------------
ISColumnSizer::~ISColumnSizer()
{

}
//-----------------------------------------------------------------------------
ISColumnSizer& ISColumnSizer::GetInstance()
{
	static ISColumnSizer ColumnSizer;
	return ColumnSizer;
}
//-----------------------------------------------------------------------------
void ISColumnSizer::Initialize()
{
	ISQuery qSelect(QS_COLUMN_SIZE);
	qSelect.BindValue(":UserID", CURRENT_USER_ID);
	if (qSelect.Execute())
	{
		while (qSelect.Next())
		{
			QString TableName = qSelect.ReadColumn("clsz_tablename").toString();
			QString FieldName = qSelect.ReadColumn("clsz_fieldname").toString();
			int Size = qSelect.ReadColumn("clsz_size").toInt();

			SetColumnSize(TableName, FieldName, Size);
		}
	}
}
//-----------------------------------------------------------------------------
void ISColumnSizer::Save()
{
	for (int i = 0; i < Tables.count(); i++)
	{
		ISMetaColumnSizeTable *ColumnSizeTable = Tables.at(i);
		for (const auto Field : ColumnSizeTable->GetFields().toStdMap())
		{
			QString FieldName = Field.first;
			int Size = Field.second;

			ISQuery qSelect(QS_COLUMN_SIZE_COUNT);
			qSelect.BindValue(":UserID", CURRENT_USER_ID);
			qSelect.BindValue(":TableName", ColumnSizeTable->GetTableName());
			qSelect.BindValue(":FieldName", FieldName);
			if (qSelect.ExecuteFirst())
			{
				int Count = qSelect.ReadColumn("count").toInt();
				if (Count)
				{
					ISQuery qUpdate(QU_COLUMN_SIZE);
					qUpdate.BindValue(":Size", Size);
					qUpdate.BindValue(":UserID", CURRENT_USER_ID);
					qUpdate.BindValue(":TableName", ColumnSizeTable->GetTableName());
					qUpdate.BindValue(":FieldName", FieldName);
					qUpdate.Execute();
				}
				else
				{
					ISQuery qInsert(QI_COLUMN_SIZE);
					qInsert.BindValue(":UserID", CURRENT_USER_ID);
					qInsert.BindValue(":TableName", ColumnSizeTable->GetTableName());
					qInsert.BindValue(":FieldName", FieldName);
					qInsert.BindValue(":Size", Size);
					qInsert.Execute();
				}
			}
		}
	}
}
//-----------------------------------------------------------------------------
void ISColumnSizer::Clear()
{
	ISQuery qDelete(QD_COLUMN_SIZE);
	qDelete.BindValue(":UserID", CURRENT_USER_ID);
	qDelete.Execute();
}
//-----------------------------------------------------------------------------
void ISColumnSizer::SetColumnSize(const QString &TableName, const QString &FieldName, int Size)
{
	if (Tables.count())
	{
		bool Found = false;

		for (int i = 0; i < Tables.count(); i++)
		{
			ISMetaColumnSizeTable *ColumnSizeTable = Tables.at(i);
			if (ColumnSizeTable->GetTableName() == TableName)
			{
				ColumnSizeTable->SetFieldSize(FieldName, Size);
				Found = true;
				break;
			}
		}

		if (!Found)
		{
			AddTable(TableName, FieldName, Size);
		}
	}
	else
	{
		AddTable(TableName, FieldName, Size);
	}
}
//-----------------------------------------------------------------------------
int ISColumnSizer::GetColumnSize(const QString &TableName, const QString &FieldName) const
{
	int Result = 0;

	for (int i = 0; i < Tables.count(); i++)
	{
		ISMetaColumnSizeTable *ColumnSizeTable = Tables.at(i);
		if (ColumnSizeTable->GetTableName() == TableName)
		{
			Result = ColumnSizeTable->GetFieldSize(FieldName);
			break;
		}
	}

	return Result;
}
//-----------------------------------------------------------------------------
void ISColumnSizer::AddTable(const QString &TableName, const QString &FieldName, int FieldSize)
{
	ISMetaColumnSizeTable *ColumnSizeTable = new ISMetaColumnSizeTable(this);
	ColumnSizeTable->SetTableName(TableName);
	ColumnSizeTable->SetFieldSize(FieldName, FieldSize);
	Tables.append(ColumnSizeTable);
}
//-----------------------------------------------------------------------------
