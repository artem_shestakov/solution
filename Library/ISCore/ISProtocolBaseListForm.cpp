#include "StdAfx.h"
#include "ISProtocolBaseListForm.h"
#include "ISMetaData.h"
#include "ISCore.h"
//-----------------------------------------------------------------------------
ISProtocolBaseListForm::ISProtocolBaseListForm(QWidget *parent) : ISListBaseForm(ISMetaData::GetInstanse().GetMetaTable("_Protocol"), parent)
{

}
//-----------------------------------------------------------------------------
ISProtocolBaseListForm::~ISProtocolBaseListForm()
{

}
//-----------------------------------------------------------------------------
void ISProtocolBaseListForm::DoubleClickedTable(const QModelIndex &ModelIndex)
{
	QString TableName = GetCurrentRecordValueDB("TableName").toString();
	int ObjectID = GetCurrentRecordValueDB("ObjectID").toInt();
	ISCore::CreateObjectForm(ISNamespace::OFT_Edit, ISMetaData::GetInstanse().GetMetaTable(TableName), ObjectID)->show();
}
//-----------------------------------------------------------------------------
