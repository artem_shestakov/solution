#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
#include "ISInterfaceForm.h"
#include "ISLineEdit.h"
#include "ISListWidget.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISListEditPopup : public ISInterfaceForm
{
	Q_OBJECT

signals:
	void Selected(const QVariant &ID, const QString &Text);
	void Hided();

public:
	ISListEditPopup(QWidget *ComboBox = 0);
	virtual ~ISListEditPopup();

	void showEvent(QShowEvent *e);
	void hideEvent(QHideEvent *e);

	void SetMetaForeign(QObject *meta_foreign);
	void SetCurrentValue(const QVariant &current_value);

	QString GetSqlFilter() const; //�������� ������
	void SetSqlFilter(const QString &sql_filter); //�������� ������
	void ClearSqlFilter(); //�������� ������

protected:
	void Search(const QVariant &value); //�����
	void ItemClicked(QListWidgetItem *ListWidgetItem); //����� �������
	void LoadDataFromQuery(); //�������� �����������
	void EnterClicked() override;

private:
	ISLineEdit *LineEdit;
	ISListWidget *ListWidget;
	QLabel *LabelCountRow;
	QLabel *LabelSearch;

	QString SqlFilter;
	QObject *MetaForeign;
	QVariant CurrentValue;
};
//-----------------------------------------------------------------------------
