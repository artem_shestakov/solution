#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISSettingsHelper : public QObject
{
	Q_OBJECT

public:
	ISSettingsHelper();
	virtual ~ISSettingsHelper();

	static bool CheckExistSettng(const QString &SettingUID);
	static void InsertSetting(const QString &SettingUID, const QVariant &Value);
	static QVariant GetUserValue(const QString &SettingUID);
	static bool SaveValue(const QString &SettingUID, const QVariant &Value);
};
//-----------------------------------------------------------------------------
