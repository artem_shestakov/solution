#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISSettingsDatabase : public QObject
{
	Q_OBJECT

public:
	ISSettingsDatabase(const ISSettingsDatabase &) = delete;
	ISSettingsDatabase(ISSettingsDatabase &&) = delete;
	ISSettingsDatabase &operator=(const ISSettingsDatabase &) = delete;
	ISSettingsDatabase &operator=(ISSettingsDatabase &&) = delete;
	~ISSettingsDatabase();

	static ISSettingsDatabase& GetInstance();

	void Initialize();

public:
	QString SettingName;
	bool Active;
	bool UserAccessDatabase;

	QString OrganizationNameFull;
	QString OrganizationNameSmall;
	QString GeneralPrincipal;
	QString ChiefAccountant;
	QString Storekeeper;
	QString LegalAddress;
	QString ActualAddress;
	QString INN;
	QString KPP;
	QString OGRN;
	QDate DateRegistration;
	QString OKVED;
	QString OKPO;
	QString CheckingAccount;
	QString FullNameBank;
	QString BIK;
	int AccountCurrency;
	
	QString PhoneMain;
	QString PhoneOther;
	QString WebSite;
	QString EMail;
	QString ICQ;
	QString ContactPerson;
	QString PhoneContactPerson;

	QString SMSLogin;
	QString SMSPassword;
	int SMSEncoding;

	QByteArray Logo;
	QByteArray LogoSmall;

	QString InformationMessage;
	QColor InformationMessageColor;

private:
	ISSettingsDatabase();
};
//-----------------------------------------------------------------------------
