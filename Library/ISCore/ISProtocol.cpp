#include "StdAfx.h"
#include "ISProtocol.h"
#include "ISAssert.h"
#include "ISSystem.h"
#include "ISQuery.h"
#include "ISMetaUser.h"
#include "EXDefines.h"
#include "EXConstants.h"
#include "ISQueryPool.h"
//-----------------------------------------------------------------------------
static QString QI_PROTOCOL = PREPARE_QUERY("INSERT INTO _protocol(prtc_tablename, prtc_tablelocalname, prtc_protocoltype, prtc_objectid, prtc_information, prtc_session) "
										   "VALUES(:TableName, :TableLocalName, :ProtocolTypeUID, :ObjectID, :Information, :Session) "
										   "RETURNING prtc_id");
//-----------------------------------------------------------------------------
static QString QI_ENTER = PREPARE_QUERY("INSERT INTO _protocolenters(petr_enterprotocol) "
										"VALUES(:EnterProtocol)");
//-----------------------------------------------------------------------------
static QString QU_ENTER = PREPARE_QUERY("UPDATE _protocolenters SET "
										"petr_timeexit = now(), "
										"petr_timeinsystem = (SELECT now()::TIME - petr_datetime::TIME FROM _protocolenters WHERE petr_enterprotocol = :EnterProtocol) "
										"WHERE petr_enterprotocol = :EnterProtocol");
//-----------------------------------------------------------------------------
static QString QU_USER_LAST_ENTER = PREPARE_QUERY("UPDATE _users SET "
												  "usrs_lastenter = now() "
												  "WHERE usrs_id = ongetcurrentuserid()");
//-----------------------------------------------------------------------------
static QString QU_ENTER_TIME = PREPARE_QUERY("UPDATE _protocolenters SET petr_timeinsystem = :TimeInSystem WHERE petr_enterprotocol = :EnterProtocol");
//-----------------------------------------------------------------------------
ISProtocol::ISProtocol()
{

}
//-----------------------------------------------------------------------------
ISProtocol::~ISProtocol()
{

}
//-----------------------------------------------------------------------------
int ISProtocol::EnterApplication()
{
	int ProtocolID = Insert(false, CONST_UID_PROTOCOL_ENTER_APPLICATION, QString(), QString(), QVariant());

	if (ProtocolID)
	{
		ISQuery qLastEnter(QU_USER_LAST_ENTER);
		qLastEnter.Execute();
	}

	return ProtocolID;
}
//-----------------------------------------------------------------------------
void ISProtocol::ExitApplication()
{
	Insert(false, CONST_UID_PROTOCOL_EXIT_APPLICATION, QString(), QString(), QString());
}
//-----------------------------------------------------------------------------
void ISProtocol::OpenSubSystem(const QString &TableName, const QString &LocalListName)
{
	Insert(true, CONST_UID_PROTOCOL_OPEN_SUB_SYSTEM, TableName, LocalListName, QVariant());
}
//-----------------------------------------------------------------------------
void ISProtocol::CreateObject(const QString &TableName, const QString &LocalListName, const QVariant &ObjectID, const QString &ObjectName)
{
	Insert(true, CONST_UID_PROTOCOL_CREATE_OBJECT, TableName, LocalListName, ObjectID, ObjectName);
}
//-----------------------------------------------------------------------------
void ISProtocol::CreateCopyObject(const QString &TableName, const QString &LocalListName, const QVariant &ObjectID, const QString &ObjectName)
{
	Insert(true, CONST_UID_PROTOCOL_CREATE_COPY_OBJECT, TableName, LocalListName, ObjectID, ObjectName);
}
//-----------------------------------------------------------------------------
void ISProtocol::EditObject(const QString &TableName, const QString &LocalListName, const QVariant &ObjectID, const QString &ObjectName)
{
	Insert(true, CONST_UID_PROTOCOL_EDIT_OBJECT, TableName, LocalListName, ObjectID, ObjectName);
}
//-----------------------------------------------------------------------------
void ISProtocol::ShowObject(const QString &TableName, const QString &LocalListName, const QVariant &ObjectID, const QString &ObjectName)
{
	Insert(true, CONST_UID_PROTOCOL_SHOW_OBJECT, TableName, LocalListName, ObjectID, ObjectName);
}
//-----------------------------------------------------------------------------
void ISProtocol::DeleteObject(const QString &TableName, const QString &LocalListName, const QVariant &ObjectID)
{
	Insert(true, CONST_UID_PROTOCOL_DELETE_OBJECT, TableName, LocalListName, ObjectID);
}
//-----------------------------------------------------------------------------
void ISProtocol::RecoveryObject(const QString &TableName, const QString &LocalListName, const QVariant &ObjectID)
{
	Insert(true, CONST_UID_PROTOCOL_RECOVERY_OBJECT, TableName, LocalListName, ObjectID);
}
//-----------------------------------------------------------------------------
void ISProtocol::DeleteCascadeObject(const QString &TableName, const QString &LocalListName, const QVariant &ObjectID)
{
	Insert(true, CONST_UID_PROTOCOL_DELETE_CASCADE_OBJECT, TableName, LocalListName, ObjectID);
}
//-----------------------------------------------------------------------------
void ISProtocol::EnterProtocol(int ProtocolID)
{
	ISQuery qInsert(QI_ENTER);
	qInsert.BindValue(":EnterProtocol", ProtocolID);
	qInsert.Execute();
}
//-----------------------------------------------------------------------------
void ISProtocol::ExitProtocol()
{
	ISQuery qUpdateEnter(QU_ENTER);
	qUpdateEnter.BindValue(":EnterProtocol", ISMetaUser::GetInstance().GetProtocolEnterID());
	qUpdateEnter.Execute();
}
//-----------------------------------------------------------------------------
int ISProtocol::Insert(bool Thread, const QString &ProtocolTypeUID, const QString &TableName, const QString &LocalListName, const QVariant &ObjectID, const QString &Information)
{
	int Result = 0;

	if (ISMetaUser::GetInstance().GetData()->ExclusionProtocolType.contains(ProtocolTypeUID))
	{
		return Result;
	}

	if (Thread)
	{
		QVariantMap Parameters;
		Parameters.insert(":ProtocolTypeUID", ProtocolTypeUID);
		Parameters.insert(":Session", ISMetaUser::GetInstance().GetSessionUID());
		Parameters.insert(":TableName", TableName);
		Parameters.insert(":ObjectID", ObjectID);
		Parameters.insert(":TableLocalName", LocalListName);
		Parameters.insert(":Information", Information);
		
		ISQueryPool::GetInstance().AddQuery(QI_PROTOCOL, Parameters);
	}
	else
	{
		ISQuery qInsertProtocol(QI_PROTOCOL);
		qInsertProtocol.BindValue(":TableName", TableName);
		qInsertProtocol.BindValue(":ProtocolTypeUID", ProtocolTypeUID);
		qInsertProtocol.BindValue(":ObjectID", ObjectID);
		qInsertProtocol.BindValue(":TableLocalName", LocalListName);
		qInsertProtocol.BindValue(":Information", Information);
		qInsertProtocol.BindValue(":Session", ISMetaUser::GetInstance().GetSessionUID());
		if (qInsertProtocol.ExecuteFirst())
		{
			Result = qInsertProtocol.ReadColumn("prtc_id").toInt();
		}
		else
		{
			IS_ASSERT(false, "Not executed query: " + qInsertProtocol.GetSqlText());
		}
	}

	return Result;
}
//-----------------------------------------------------------------------------
