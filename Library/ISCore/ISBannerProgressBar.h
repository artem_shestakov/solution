#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISBannerProgressBar : public QProgressBar
{
	Q_OBJECT

public:
	ISBannerProgressBar(QWidget *parent = 0);
	virtual ~ISBannerProgressBar();
};
//-----------------------------------------------------------------------------
