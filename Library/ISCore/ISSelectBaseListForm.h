#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
#include "ISListBaseForm.h"
//-----------------------------------------------------------------------------
//!������� ����� ������ ������ �������
class ISCORE_EXPORT ISSelectBaseListForm : public ISListBaseForm
{
	Q_OBJECT

public:
	ISSelectBaseListForm(PMetaClassTable *MetaTable, QWidget *parent = 0);
	virtual ~ISSelectBaseListForm();

	void SetSqlFilter(const QString &SqlFilter);

protected:
	void CreateObjectForm(QWidget *ObjectForm);
	void DoubleClickedTable(const QModelIndex &ModelIndex) override;
	void EnterClicked() override;
	void EscapeClicked() override;
};
//-----------------------------------------------------------------------------
