#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISListIndicatorWidget : public QWidget
{
	Q_OBJECT

public:
	ISListIndicatorWidget(QWidget *parent);
	virtual ~ISListIndicatorWidget();

	void hideEvent(QHideEvent *e);

	void SetVisibleAnimation(bool Visible, QMovie *Movie = nullptr); //�������� �������� � � ���������
	void SetPixmap(const QPixmap &Pixmap); //�������� ��������
	void SetText(const QString &Text); //�������� �����

private:
	QLabel *LabelImage;
	QLabel *LabelText;
};
//-----------------------------------------------------------------------------
