#include "StdAfx.h"
#include "ISMetaColumnSizeTable.h"
//-----------------------------------------------------------------------------
ISMetaColumnSizeTable::ISMetaColumnSizeTable(QObject *parent) : QObject(parent)
{
	
}
//-----------------------------------------------------------------------------
ISMetaColumnSizeTable::~ISMetaColumnSizeTable()
{

}
//-----------------------------------------------------------------------------
void ISMetaColumnSizeTable::SetTableName(const QString &table_name)
{
	TableName = table_name;
}
//-----------------------------------------------------------------------------
QString ISMetaColumnSizeTable::GetTableName() const
{
	return TableName;
}
//-----------------------------------------------------------------------------
void ISMetaColumnSizeTable::SetFieldSize(const QString &FieldName, int Size)
{
	Fields.insert(FieldName, Size);
}
//-----------------------------------------------------------------------------
int ISMetaColumnSizeTable::GetFieldSize(const QString &FieldName)
{
	return Fields.value(FieldName);
}
//-----------------------------------------------------------------------------
QMap<QString, int> ISMetaColumnSizeTable::GetFields()
{
	return Fields;
}
//-----------------------------------------------------------------------------
