#include "StdAfx.h"
#include "ISFavoritesForm.h"
#include "ISLocalization.h"
#include "ISFavorites.h"
#include "EXDefines.h"
#include "ISBuffer.h"
#include "ISMessageBox.h"
#include "ISListWidgetItemFavorite.h"
#include "ISQueryModel.h"
#include "ISQuery.h"
#include "ISMetaUser.h"
#include "ISCore.h"
#include "ISMetaData.h"
#include "ISPushButton.h"
//-----------------------------------------------------------------------------
static QString QS_FAVORITES = PREPARE_QUERY("SELECT fvts_id, fvts_tablename, fvts_tablelocalname, fvts_objectname, fvts_objectid FROM _favorites WHERE fvts_user = :CurrentUserID ORDER BY fvts_id DESC");
//-----------------------------------------------------------------------------
static QString QS_FAVORITES_TABLE = PREPARE_QUERY("SELECT fvts_id, fvts_tablename, fvts_tablelocalname, fvts_objectname, fvts_objectid FROM _favorites WHERE fvts_user = :CurrentUserID AND fvts_tablename = :TableName ORDER BY fvts_id DESC");
//-----------------------------------------------------------------------------
ISFavoritesForm::ISFavoritesForm(QWidget *parent, PMetaClassTable *MetaTable) : ISInterfaceForm(parent)
{
	this->MetaTable = MetaTable;

	if (MetaTable)
	{
		setWindowTitle(LOCALIZATION("FavoritesForTable") + ": " + MetaTable->GetLocalListName());
	}
	else
	{
		setWindowTitle(LOCALIZATION("Favorites"));
	}

	setWindowIcon(BUFFER_ICONS("Favorites"));

	GetMainLayout()->setContentsMargins(LAYOUT_MARGINS_10_PX);

	ToolBar = new QToolBar(this);
	ToolBar->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
	GetMainLayout()->addWidget(ToolBar);

	ActionOpen = new QAction(ToolBar);
	ActionOpen->setText(LOCALIZATION("Open"));
	ActionOpen->setToolTip(LOCALIZATION("Open"));
	ActionOpen->setIcon(BUFFER_ICONS("Select"));
	ActionOpen->setEnabled(false);
	connect(ActionOpen, &QAction::triggered, this, &ISFavoritesForm::OpenFavorite);
	ToolBar->addAction(ActionOpen);

	ActionDelete = new QAction(ToolBar);
	ActionDelete->setText(LOCALIZATION("Delete"));
	ActionDelete->setToolTip(LOCALIZATION("Delete"));
	ActionDelete->setIcon(BUFFER_ICONS("Delete"));
	ActionDelete->setEnabled(false);
	connect(ActionDelete, &QAction::triggered, this, &ISFavoritesForm::DeleteFavorite);
	ToolBar->addAction(ActionDelete);

	ActionClearFavorites = new QAction(ToolBar);
	ActionClearFavorites->setText(LOCALIZATION("ClearFavorites"));
	ActionClearFavorites->setToolTip(LOCALIZATION("ClearFavorites"));
	ActionClearFavorites->setIcon(BUFFER_ICONS("Clear"));
	connect(ActionClearFavorites, &QAction::triggered, this, &ISFavoritesForm::ClearFavorites);
	ToolBar->addAction(ActionClearFavorites);

	ListWidget = new ISListWidget(this);
	connect(ListWidget, &QListWidget::itemDoubleClicked, this, &ISFavoritesForm::ListWidgetDoubleClicked);
	connect(ListWidget, &QListWidget::itemClicked, this, &ISFavoritesForm::ItemClicked);
	GetMainLayout()->addWidget(ListWidget);

	ISPushButton *ButtonClose = new ISPushButton(this);
	ButtonClose->setText(LOCALIZATION("Close"));
	ButtonClose->setIcon(BUFFER_ICONS("Close"));
	connect(ButtonClose, &ISPushButton::clicked, this, &ISFavoritesForm::close);
	GetMainLayout()->addWidget(ButtonClose, 0, Qt::AlignRight);

	LoadFavorites();
}
//-----------------------------------------------------------------------------
ISFavoritesForm::~ISFavoritesForm()
{

}
//-----------------------------------------------------------------------------
void ISFavoritesForm::LoadFavorites()
{
	ISQuery qSelect;

	if (MetaTable)
	{
		qSelect = ISQuery(QS_FAVORITES_TABLE);
		qSelect.BindValue(":TableName", MetaTable->GetName());
	}
	else
	{
		qSelect = ISQuery(QS_FAVORITES);
	}

	qSelect.BindValue(":CurrentUserID", CURRENT_USER_ID);
	if (qSelect.Execute())
	{
		while (qSelect.Next())
		{
			int FavoriteID = qSelect.ReadColumn("fvts_id").toInt();
			QString TableName = qSelect.ReadColumn("fvts_tablename").toString();
			QString TableLocalName = qSelect.ReadColumn("fvts_tablelocalname").toString();
			QString ObjectName = qSelect.ReadColumn("fvts_objectname").toString();
			int ObjectID = qSelect.ReadColumn("fvts_objectid").toInt();

			ISListWidgetItemFavorite *Item = new ISListWidgetItemFavorite(ListWidget);
			Item->setText(TableLocalName + ": " + ObjectName);
			Item->setSizeHint(QSize(Item->sizeHint().width(), 30));
			Item->SetFavoriteID(FavoriteID);
			Item->SetTableName(TableName);
			Item->SetObjectID(ObjectID);
		}
	}
}
//-----------------------------------------------------------------------------
void ISFavoritesForm::ReloadFavorites()
{
	ListWidget->Clear();
	LoadFavorites();
}
//-----------------------------------------------------------------------------
void ISFavoritesForm::OpenFavorite()
{
	ISListWidgetItemFavorite *Item = dynamic_cast<ISListWidgetItemFavorite*>(ListWidget->currentItem());
	if (Item)
	{
		PMetaClassTable *MetaTable = ISMetaData::GetInstanse().GetMetaTable(Item->GetTableName());
		ISObjectFormBase *ObjectForm = ISCore::CreateObjectForm(ISNamespace::ObjectFormType::OFT_Edit, MetaTable, Item->GetObjectID());
		emit AddFormFromTab(ObjectForm);
		
		close();
	}
}
//-----------------------------------------------------------------------------
void ISFavoritesForm::DeleteFavorite()
{
	if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.DeleteFavorite")))
	{
		ISListWidgetItemFavorite *Item = dynamic_cast<ISListWidgetItemFavorite*>(ListWidget->currentItem());
		if (Item)
		{
			if (ISFavorites::GetInstance().DeleteFavorite(Item->GetTableName(), Item->GetObjectID()))
			{
				ReloadFavorites();
			}
		}
	}
}
//-----------------------------------------------------------------------------
void ISFavoritesForm::ClearFavorites()
{
	if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.DeleteFavorites")))
	{
		ISFavorites::GetInstance().DeleteAllFavorites(CURRENT_USER_ID);
		ReloadFavorites();
	}
}
//-----------------------------------------------------------------------------
void ISFavoritesForm::EscapeClicked()
{
	close();
}
//-----------------------------------------------------------------------------
void ISFavoritesForm::ListWidgetDoubleClicked(QListWidgetItem *Item)
{
	OpenFavorite();
}
//-----------------------------------------------------------------------------
void ISFavoritesForm::ItemClicked(QListWidgetItem *Item)
{
	ActionOpen->setEnabled(true);
	ActionDelete->setEnabled(true);
}
//-----------------------------------------------------------------------------
