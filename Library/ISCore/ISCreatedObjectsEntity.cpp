#include "StdAfx.h"
#include "ISCreatedObjectsEntity.h"
#include "EXDefines.h"
#include "ISMessageBox.h"
#include "ISLocalization.h"
#include "ISSystem.h"
//-----------------------------------------------------------------------------
ISCreatedObjectsEntity::ISCreatedObjectsEntity() : QObject()
{

}
//-----------------------------------------------------------------------------
ISCreatedObjectsEntity::~ISCreatedObjectsEntity()
{

}
//-----------------------------------------------------------------------------
ISCreatedObjectsEntity& ISCreatedObjectsEntity::GetInstance()
{
	static ISCreatedObjectsEntity CreatedObjectsEntity;
	return CreatedObjectsEntity;
}
//-----------------------------------------------------------------------------
void ISCreatedObjectsEntity::RegisterForm(ISObjectFormBase *ObjectForm)
{
	ObjectForms.insert(ObjectForm->GetFormUID(), ObjectForm);
}
//-----------------------------------------------------------------------------
void ISCreatedObjectsEntity::UnregisterForm(const QString &FormUID)
{
	ObjectForms.remove(FormUID);
}
//-----------------------------------------------------------------------------
bool ISCreatedObjectsEntity::CheckExistForms()
{
	QList<ISObjectFormBase*> Forms = ObjectForms.values();
	int CountNotSaved = 0;
	QString DetailedText;

	for (int i = 0; i < Forms.count(); i++)
	{
		ISObjectFormBase *ObjectFormBase = Forms.at(i);
		if (ObjectFormBase->GetModificationFlag())
		{
			CountNotSaved++;
			DetailedText += QString::number(i + 1) + ") " + ObjectFormBase->windowTitle().remove(SYMBOL_OBJECT_CHANGED) + ";\n";
		}
	}

	if (CountNotSaved)
	{
		ISSystem::RemoveLastSymbolFromString(DetailedText, 2);
		DetailedText += ".";
		ISMessageBox::ShowInformation(nullptr, LOCALIZATION("Message.Information.NotSavedObjects").arg(CountNotSaved), DetailedText);
		return false;
	}

	return true;
}
//-----------------------------------------------------------------------------
