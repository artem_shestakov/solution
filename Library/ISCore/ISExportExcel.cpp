#include "StdAfx.h"
#include "ISExportExcel.h"
#include "EXDefines.h"
#include "ISLocalization.h"
#include "ISMessageBox.h"
#include "ISAssert.h"
#include "ISSystem.h"
//-----------------------------------------------------------------------------
ISExportExcel::ISExportExcel(QObject *parent) : ISExportWorker(parent)
{
	ExcelApplication = nullptr;
	Workbooks = nullptr;
	Workbook = nullptr;
	Sheets = nullptr;
	Sheet = nullptr;
}
//-----------------------------------------------------------------------------
ISExportExcel::~ISExportExcel()
{
	delete Sheet;
	delete Sheets;
	delete Workbook;
	delete Workbooks;
	delete ExcelApplication;
}
//-----------------------------------------------------------------------------
bool ISExportExcel::Prepare()
{
	ExcelApplication = new QAxObject("Excel.Application", this);
	if (!ExcelApplication)
	{
		ErrorString = LOCALIZATION("Export.Error.Excel.NotInitialized.Excel");
		return false;
	}

	ExcelApplication->setProperty("DisplayAlerts", "0");

	Workbooks = ExcelApplication->querySubObject("Workbooks");
	if (!Workbooks)
	{
		ErrorString = LOCALIZATION("Export.Error.Excel.NotInitialized.Workbooks");
		return false;
	}

	Workbook = Workbooks->querySubObject("Add()");
	if (!Workbook)
	{
		ErrorString = LOCALIZATION("Export.Error.Excel.NotInitialized.Workbook");
		return false;
	}

	Sheets = Workbook->querySubObject("Sheets");
	if (!Sheets)
	{
		ErrorString = LOCALIZATION("Export.Error.Excel.NotInitialized.Sheets");
		return false;
	}

	Sheet = Sheets->querySubObject("Item(const QVariant &)", 1);
	if (!Sheet)
	{
		ErrorString = LOCALIZATION("Export.Error.Excel.NotInitialized.Sheet");
		return false;
	}

	if (!Sheet->setProperty("Name", TableName))
	{
		ErrorString = LOCALIZATION("Export.Error.Excel.NotRenameSheet");
		return false;
	}

	return true;
}
//-----------------------------------------------------------------------------
bool ISExportExcel::Export()
{
	int StartRow = 1;
	if (Header) //���� � �������������� ���� ����� �������� ��������� �������
	{
		for (int i = 0; i < Fields.count(); i++) //����� ��������� ��� �������� �����
		{
			QString FieldName = Fields.at(i);
			if (Fields.contains(FieldName))
			{
				QString LocalName = Model->GetFieldLocalName(FieldName);

				QAxObject *HeaderCell = Sheet->querySubObject("Cells(QVariant, QVariant)", StartRow, i + 1);
				HeaderCell->setProperty("Value", LocalName);
				
				ISSystem::Excel_SetBoldCell(HeaderCell, true);
				ISSystem::Excel_SetBackgrounCell(HeaderCell, QColor("lightGray"));
				ISSystem::Excel_CreateBorderCell(HeaderCell);

				delete HeaderCell;
			}
		}

		StartRow = 2;
	}

	for (int Row = 0; Row < Model->rowCount(); Row++) //����� �����
	{
		if (Canceled) //���� ���� ������ ������ "����������"
		{
			if (ISMessageBox::ShowQuestion(nullptr, LOCALIZATION("Export.Process.Cancel"), LOCALIZATION("Export.Process.Cancel.DetailedText")))
			{
				ExcelApplication->dynamicCall("Quit()");
				return true;
			}
			else
			{
				Canceled = false;
			}
		}

		if (SelectedRows.count()) //���� ���� ���������� ������
		{
			if (!SelectedRows.contains(Row))
			{
				continue;
			}
		}

		QSqlRecord SqlRecord = Model->GetRecord(Row); //������� ������

		for (int Column = 0; Column < Fields.count(); Column++) //����� �������
		{
			QVariant Value = SqlRecord.value(Fields.at(Column)).toString();
			Value = PrepareValue(Value);

			QAxObject *Cell = Sheet->querySubObject("Cells(QVariant, QVariant)", StartRow, Column + 1);
			bool Set = Cell->setProperty("Value", Value);
			IS_ASSERT(Set, "Not setProperty \"Value\"");

			ISSystem::Excel_CreateBorderCell(Cell);

			delete Cell;
		}

		StartRow++;
		emit ExportedRow();
		emit Message(LOCALIZATION("Export.Process.Process").arg(Row + 1).arg(Model->rowCount()) + "...");
	}

	Sheet->dynamicCall("Activate()");
	ExcelApplication->setProperty("Visible", true);
	return true;
}
//-----------------------------------------------------------------------------
