#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
#include "ISMetaSystem.h"
#include "ISMetaSubSystem.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISMetaSystemsEntity : public QObject
{
	Q_OBJECT

public:
	ISMetaSystemsEntity(const ISMetaSystemsEntity &) = delete;
	ISMetaSystemsEntity(ISMetaSystemsEntity &&) = delete;
	ISMetaSystemsEntity &operator=(const ISMetaSystemsEntity &) = delete;
	ISMetaSystemsEntity &operator=(ISMetaSystemsEntity &&) = delete;
	~ISMetaSystemsEntity();

	static ISMetaSystemsEntity& GetInstance();

	void AppendSystem(ISMetaSystem *MetaSystem); //�������� ������� � ������ ������
	QVector<ISMetaSystem*> GetSystems(); //�������� ������ ���� ������
	QVector<ISMetaSubSystem*> GetAllSubSystems(); //�������� ������ ���� ���������
	ISMetaSystem* GetSystem(const QString &SystemUID); //�������� ������� �� � ��������������
	ISMetaSubSystem* GetSubSystem(const QString &SubSystemUID); //�������� ���������� �� � ��������������
	int GetCountSystems(); //������� ���������� ���� ������

protected:
	void Initialize(); //�������������

	QIcon GetSystemIcon(ISMetaSystem *MetaSystem) const; //�������� ������ ��� �������
	QIcon GetSubSystemIcon(ISMetaSubSystem *MetaSubSystem) const; //�������� ������ ��� ����������

	QString GetSystemToolTip(ISMetaSystem *MetaSystem) const; //�������� ��������� ��� �������
	QString GetSubSystemToolTip(ISMetaSubSystem *MetaSubSystem) const; //�������� ��������� ��� ����������

private:
	ISMetaSystemsEntity();

	QVector<ISMetaSystem*> Systems;
	QVector<ISMetaSubSystem*> SubSystems;
};
//-----------------------------------------------------------------------------
