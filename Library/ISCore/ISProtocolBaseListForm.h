#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
#include "ISListBaseForm.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISProtocolBaseListForm : public ISListBaseForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISProtocolBaseListForm(QWidget *parent = 0);
	~ISProtocolBaseListForm();

protected:
	void DoubleClickedTable(const QModelIndex &ModelIndex);
};
//-----------------------------------------------------------------------------
