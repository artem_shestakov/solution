#include "StdAfx.h"
#include "ISHistory.h"
#include "ISQueryPool.h"
#include "ISMetaUser.h"
#include "ISQueryText.h"
//-----------------------------------------------------------------------------
static QString QI_HISTORY = PREPARE_QUERY("INSERT INTO _history(htry_user, htry_tablename, htry_tablelocalname, htry_objectname, htry_objectid) "
										  "VALUES(:CurrentUserID, :TableName, :TableLocalName, :ObjectName, :ObjectID)");
//-----------------------------------------------------------------------------
static QString QD_HISTORY = PREPARE_QUERY("DELETE FROM _history WHERE htry_user = :CurrentUserID");
//-----------------------------------------------------------------------------
ISHistory::ISHistory()
{

}
//-----------------------------------------------------------------------------
ISHistory::~ISHistory()
{

}
//-----------------------------------------------------------------------------
void ISHistory::AddHistory(const QString &TableName, const QString &LocalListName, const QString &ObjectName, int ObjectID)
{
	QVariantMap Parameters;
	Parameters.insert(":CurrentUserID", CURRENT_USER_ID);
	Parameters.insert(":TableName", TableName);
	Parameters.insert(":TableLocalName", LocalListName);
	Parameters.insert(":ObjectName", ObjectName);
	Parameters.insert(":ObjectID", ObjectID);

	ISQueryPool::GetInstance().AddQuery(QI_HISTORY, Parameters);
}
//-----------------------------------------------------------------------------
void ISHistory::ClearHistory()
{
	QVariantMap Parameters;
	Parameters.insert(":CurrentUserID", CURRENT_USER_ID);

	ISQueryPool::GetInstance().AddQuery(QD_HISTORY, Parameters);
}
//-----------------------------------------------------------------------------
