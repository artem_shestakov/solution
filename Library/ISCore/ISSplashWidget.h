#pragma  once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISSplashWidget : public QFrame
{
	Q_OBJECT

public:
	ISSplashWidget(QString &Text = QString(), QWidget *parent = 0);
	virtual ~ISSplashWidget();

	void showEvent(QShowEvent *e);
	void SetText(const QString &Text);

private:
	QLabel *LabelText;
};
//-----------------------------------------------------------------------------
