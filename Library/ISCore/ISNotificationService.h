#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
#include "ISNamespace.h"
#include "ISPopupMessage.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISNotificationService : public QObject
{
	Q_OBJECT

public:
	ISNotificationService();
	virtual ~ISNotificationService();

	static void ShowNotification(ISNamespace::NotificationFormType NotificationType, const QString &MetaObjectName = QString(), const QString &ObjectName = QString());
	static void ShowNotification(const QString &Title, const QString &Message);
	static ISPopupMessage* ShowNotification(const QString &Mesage);
};
//-----------------------------------------------------------------------------
