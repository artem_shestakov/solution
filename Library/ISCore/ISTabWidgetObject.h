#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISTabWidgetObject : public QTabWidget
{
	Q_OBJECT

public:
	ISTabWidgetObject(QWidget *parent = 0);
	virtual ~ISTabWidgetObject();

protected:
	void tabInserted(int Index);
	void CloseTab();
};
//-----------------------------------------------------------------------------
