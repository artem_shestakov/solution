#include "StdAfx.h"
#include "ISSettingsHelper.h"
#include "ISQuery.h"
#include "EXDefines.h"
#include "ISAssert.h"
#include "ISSettings.h"
//-----------------------------------------------------------------------------
static QString QS_USER_SETTING = PREPARE_QUERY("SELECT COUNT(*) FROM _usersettings WHERE usst_user = ongetcurrentuserid() AND usst_setting = :SettingUID");
//-----------------------------------------------------------------------------
static QString QI_USER_SETTING = PREPARE_QUERY("INSERT INTO _usersettings(usst_user, usst_setting, usst_value) "
											   "VALUES(ongetcurrentuserid(), :SettingUID, :Value)");
//-----------------------------------------------------------------------------
static QString QS_USER_VALUE = PREPARE_QUERY("SELECT usst_value FROM _usersettings WHERE usst_user = ongetcurrentuserid() AND usst_setting = :SettingUID");
//-----------------------------------------------------------------------------
static QString QU_USER_SETTING_VALUE = PREPARE_QUERY("UPDATE _usersettings SET "
													 "usst_value = :Value "
													 "WHERE usst_user = ongetcurrentuserid() "
													 "AND usst_setting = :SettingUID");
//-----------------------------------------------------------------------------
ISSettingsHelper::ISSettingsHelper()
{

}
//-----------------------------------------------------------------------------
ISSettingsHelper::~ISSettingsHelper()
{

}
//-----------------------------------------------------------------------------
bool ISSettingsHelper::CheckExistSettng(const QString &SettingUID)
{
	ISQuery qSelectSetting(QS_USER_SETTING);
	qSelectSetting.BindValue(":SettingUID", SettingUID);
	if (qSelectSetting.ExecuteFirst())
	{
		int Count = qSelectSetting.ReadColumn("count").toInt();
		if (Count == 1)
		{
			return true;
		}
	}

	return false;
}
//-----------------------------------------------------------------------------
void ISSettingsHelper::InsertSetting(const QString &SettingUID, const QVariant &Value)
{
	ISQuery qInsertSetting(QI_USER_SETTING);
	qInsertSetting.BindValue(":SettingUID", SettingUID);
	qInsertSetting.BindValue(":Value", Value);
	bool Executed = qInsertSetting.Execute();
	IS_ASSERT(Executed, "Query for initialize new settings not executed. Query text: " + qInsertSetting.GetSqlText());
}
//-----------------------------------------------------------------------------
QVariant ISSettingsHelper::GetUserValue(const QString &SettingUID)
{
	ISQuery qSelectValue(QS_USER_VALUE);
	qSelectValue.BindValue(":SettingUID", SettingUID);
	if (qSelectValue.ExecuteFirst())
	{
		QVariant Value = qSelectValue.ReadColumn("usst_value");
		return Value;
	}

	IS_ASSERT(false, qSelectValue.GetSqlQuery().lastError().text());
	return QVariant();
}
//-----------------------------------------------------------------------------
bool ISSettingsHelper::SaveValue(const QString &SettingUID, const QVariant &Value)
{
	ISQuery qUpdateValue(QU_USER_SETTING_VALUE);
	qUpdateValue.BindValue(":Value", Value);
	qUpdateValue.BindValue(":SettingUID", SettingUID);
	bool Updated = qUpdateValue.Execute();
	if (Updated)
	{
		ISSettings::GetInstance().SetValue(SettingUID, Value);
	}

	return Updated;
}
//-----------------------------------------------------------------------------
