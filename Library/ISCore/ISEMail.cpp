#include "StdAfx.h"
#include "ISEMail.h"
#include "EXConstants.h"
#include "ISQuery.h"
#include "ISNotify.h"
//-----------------------------------------------------------------------------
static QString QU_MAIL_STATUS = PREPARE_QUERY("UPDATE _email SET mail_status = :StatusUID, mail_departuredatetime = now() WHERE mail_id = :MailID");
//-----------------------------------------------------------------------------
static QString QI_CARAT_MAIL = PREPARE_QUERY("INSERT INTO _caratmail(crml_sendermail, crml_senderpassword, crml_servermail, crml_ssl, crml_port, crml_recipientmail, crml_subject, crml_text) "
											 "VALUES(:SenderMail, :SenderPassword, :ServerMail, :SSL, :Port, :RecipientMail, :Subject, :Text)");
//-----------------------------------------------------------------------------
ISEMail::ISEMail() : QObject()
{

}
//-----------------------------------------------------------------------------
ISEMail::~ISEMail()
{

}
//-----------------------------------------------------------------------------
bool ISEMail::SendMail(int MailID, const QString &Subject, int Recipient)
{
	ISQuery qUpdateStatus(QU_MAIL_STATUS);
	qUpdateStatus.BindValue(":StatusUID", CONST_UID_EMAIL_STATUS_OUTGOING);
	qUpdateStatus.BindValue(":MailID", MailID);
	if (qUpdateStatus.Execute())
	{
		ISNotify::GetInstance().SendNotification(CONST_UID_NOTIFY_MAIL_NEW, Subject, true, Recipient);
		return true;
	}

	return false;
}
//-----------------------------------------------------------------------------
bool ISEMail::Send(const QString &sender_mail, const QString &sender_password, const QString &server_mail, bool ssl, int port, const QString &recipient_mail, const QString &subject, const QString &text)
{
	ISQuery qInsert(QI_CARAT_MAIL);
	qInsert.BindValue(":SenderMail", sender_mail);
	qInsert.BindValue(":SenderPassword", sender_password);
	qInsert.BindValue(":ServerMail", server_mail);
	qInsert.BindValue(":SSL", ssl);
	qInsert.BindValue(":Port", port);
	qInsert.BindValue(":RecipientMail", recipient_mail);
	qInsert.BindValue(":Subject", subject);
	qInsert.BindValue(":Text", text);
	return qInsert.Execute();
}
//-----------------------------------------------------------------------------
