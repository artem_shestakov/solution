#include "StdAfx.h"
#include "ISNoteObjectForm.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "EXDefines.h"
#include "ISPushButton.h"
#include "ISQuery.h"
//-----------------------------------------------------------------------------
static QString QS_NOTE_OBJECT = PREPARE_QUERY("SELECT COUNT(*) FROM _noteobject WHERE nobj_tablename = :TableName AND nobj_objectid = :ObjectID");
//-----------------------------------------------------------------------------
static QString QS_NOTE = PREPARE_QUERY("SELECT nobj_note FROM _noteobject WHERE nobj_tablename = :TableName AND nobj_objectid = :ObjectID");
//-----------------------------------------------------------------------------
static QString QU_NOTE = PREPARE_QUERY("UPDATE _noteobject SET nobj_note = :Note WHERE nobj_tablename = :TableName AND nobj_objectid = :ObjectID");
//-----------------------------------------------------------------------------
static QString QI_NOTE = PREPARE_QUERY("INSERT INTO _noteobject(nobj_tablename, nobj_objectid, nobj_note) VALUES(:TableName, :ObjectID, :Note)");
//-----------------------------------------------------------------------------
ISNoteObjectForm::ISNoteObjectForm(const QString &table_name, int object_id, QWidget *parent) : ISInterfaceDialogForm(parent)
{
	TableName = table_name;
	ObjectID = object_id;

	setWindowTitle(LOCALIZATION("Note"));
	setWindowIcon(BUFFER_ICONS("NoteObject"));

	ForbidResize();

	GetMainLayout()->setContentsMargins(LAYOUT_MARGINS_10_PX);

	TextEdit = new ISTextEdit(this);
	GetMainLayout()->addWidget(TextEdit);

	QHBoxLayout *Layout = new QHBoxLayout();
	Layout->addStretch();
	GetMainLayout()->addLayout(Layout);

	ISPushButton *ButtonSave = new ISPushButton(this);
	ButtonSave->setText(LOCALIZATION("Save"));
	ButtonSave->setIcon(BUFFER_ICONS("Save"));
	connect(ButtonSave, &ISPushButton::clicked, this, &ISNoteObjectForm::Save);
	Layout->addWidget(ButtonSave);

	ISPushButton *ButtonClose = new ISPushButton(this);
	ButtonClose->setText(LOCALIZATION("Close"));
	ButtonClose->setIcon(BUFFER_ICONS("Close"));
	connect(ButtonClose, &ISPushButton::clicked, this, &ISNoteObjectForm::close);
	Layout->addWidget(ButtonClose);

	LoadNote();
}
//-----------------------------------------------------------------------------
ISNoteObjectForm::~ISNoteObjectForm()
{

}
//-----------------------------------------------------------------------------
void ISNoteObjectForm::Save()
{
	ISQuery qSelectCount(QS_NOTE_OBJECT);
	qSelectCount.BindValue(":TableName", TableName);
	qSelectCount.BindValue(":ObjectID", ObjectID);
	if (qSelectCount.ExecuteFirst())
	{
		int Count = qSelectCount.ReadColumn("count").toInt();
		if (Count)
		{
			ISQuery qUpdate(QU_NOTE);
			qUpdate.BindValue(":Note", TextEdit->GetValue());
			qUpdate.BindValue(":TableName", TableName);
			qUpdate.BindValue(":ObjectID", ObjectID);
			if (qUpdate.Execute())
			{
				close();
			}
		}
		else
		{
			ISQuery qInsert(QI_NOTE);
			qInsert.BindValue(":Note", TextEdit->GetValue());
			qInsert.BindValue(":TableName", TableName);
			qInsert.BindValue(":ObjectID", ObjectID);
			if (qInsert.Execute())
			{
				close();
			}
		}
	}
}
//-----------------------------------------------------------------------------
void ISNoteObjectForm::LoadNote()
{
	ISQuery qSelectCount(QS_NOTE_OBJECT);
	qSelectCount.BindValue(":TableName", TableName);
	qSelectCount.BindValue(":ObjectID", ObjectID);
	if (qSelectCount.ExecuteFirst())
	{
		int Count = qSelectCount.ReadColumn("count").toInt();
		if (Count)
		{
			ISQuery qSelectNote(QS_NOTE);
			qSelectNote.BindValue(":TableName", TableName);
			qSelectNote.BindValue(":ObjectID", ObjectID);
			if (qSelectNote.ExecuteFirst())
			{
				QString Note = qSelectNote.ReadColumn("nobj_note").toString();
				TextEdit->SetValue(Note);
			}
		}
	}
}
//-----------------------------------------------------------------------------
