#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
#include "ISMetaParagraph.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISParagraphEntity : public QObject
{
	Q_OBJECT

public:
	ISParagraphEntity(const ISParagraphEntity &) = delete;
	ISParagraphEntity(ISParagraphEntity &&) = delete;
	ISParagraphEntity &operator=(const ISParagraphEntity &) = delete;
	ISParagraphEntity &operator=(ISParagraphEntity &&) = delete;
	~ISParagraphEntity();

	static ISParagraphEntity& GetInstance();
	
	ISMetaParagraph* GetParagraph(const QString &ParagraphUID); //�������� ��������
	QVector<ISMetaParagraph*> GetParagraphs(); //�������� ������ ����������

private:
	ISParagraphEntity();

	QVector<ISMetaParagraph*> Paragraphs;
};
//-----------------------------------------------------------------------------
