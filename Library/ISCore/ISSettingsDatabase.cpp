#include "StdAfx.h"
#include "ISSettingsDatabase.h"
#include "ISQuery.h"
#include "ISQueryText.h"
#include "ISSystem.h"
//-----------------------------------------------------------------------------
static QString QS_SETTINGS_DATABASE = PREPARE_QUERY("SELECT * FROM _settingsdatabase WHERE NOT sgdb_isdeleted AND sgdb_active");
//-----------------------------------------------------------------------------
ISSettingsDatabase::ISSettingsDatabase() : QObject()
{
	Active = false;
	UserAccessDatabase = true;
}
//-----------------------------------------------------------------------------
ISSettingsDatabase::~ISSettingsDatabase()
{

}
//-----------------------------------------------------------------------------
ISSettingsDatabase& ISSettingsDatabase::GetInstance()
{
	static ISSettingsDatabase SettingsDatabase;
	return SettingsDatabase;
}
//-----------------------------------------------------------------------------
void ISSettingsDatabase::Initialize()
{
	ISQuery qSelect(QS_SETTINGS_DATABASE);
	if (qSelect.ExecuteFirst())
	{
		SettingName = qSelect.ReadColumn("sgdb_settingname").toString();
		Active = qSelect.ReadColumn("sgdb_active").toBool();
		UserAccessDatabase = qSelect.ReadColumn("sgdb_useraccessdatabase").toBool();

		OrganizationNameFull = qSelect.ReadColumn("sgdb_organizationnamefull").toString();
		OrganizationNameSmall = qSelect.ReadColumn("sgdb_organizationnamesmall").toString();
		GeneralPrincipal = qSelect.ReadColumn("sgdb_generalprincipal").toString();
		ChiefAccountant = qSelect.ReadColumn("sgdb_chiefaccountant").toString();
		Storekeeper = qSelect.ReadColumn("sgdb_storekeeper").toString();
		LegalAddress = qSelect.ReadColumn("sgdb_legaladdress").toString();
		ActualAddress = qSelect.ReadColumn("sgdb_actualaddress").toString();
		INN = qSelect.ReadColumn("sgdb_inn").toString();
		KPP = qSelect.ReadColumn("sgdb_kpp").toString();
		OGRN = qSelect.ReadColumn("sgdb_ogrn").toString();
		DateRegistration = qSelect.ReadColumn("sgdb_dateregistration").toDate();
		OKVED = qSelect.ReadColumn("sgdb_okved").toString();
		OKPO = qSelect.ReadColumn("sgdb_okpo").toString();
		CheckingAccount = qSelect.ReadColumn("sgdb_checkingaccount").toString();
		FullNameBank = qSelect.ReadColumn("sgdb_fullnamebank").toString();
		BIK = qSelect.ReadColumn("sgdb_bik").toString();
		AccountCurrency = qSelect.ReadColumn("sgdb_accountcurrency").toInt();

		PhoneMain = qSelect.ReadColumn("sgdb_phonemain").toString();
		PhoneOther = qSelect.ReadColumn("sgdb_phoneother").toString();
		WebSite = qSelect.ReadColumn("sgdb_website").toString();
		EMail = qSelect.ReadColumn("sgdb_email").toString();
		ICQ = qSelect.ReadColumn("sgdb_icq").toString();
		ContactPerson = qSelect.ReadColumn("sgdb_contactperson").toString();
		PhoneContactPerson = qSelect.ReadColumn("sgdb_phonecontactperson").toString();

		SMSLogin = qSelect.ReadColumn("sgdb_smslogin").toString();
		SMSPassword = qSelect.ReadColumn("sgdb_smspassword").toString();
		SMSEncoding = qSelect.ReadColumn("sgdb_smsencoding").toInt();

		Logo = qSelect.ReadColumn("sgdb_logo").toByteArray();
		LogoSmall = qSelect.ReadColumn("sgdb_logosmall").toByteArray();
		
		InformationMessage = qSelect.ReadColumn("sgdb_informationmessage").toString();
		InformationMessageColor = ISSystem::StringToColor(qSelect.ReadColumn("sgdb_informationmessagecolor").toString());
	}
}
//-----------------------------------------------------------------------------
