#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
#include "ISListBaseForm.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISListViewForm : public ISListBaseForm
{
	Q_OBJECT

public:
	ISListViewForm(PMetaClassTable *meta_table, QWidget *parent = 0);
	virtual ~ISListViewForm();

	void AddCondition(const QString &Parameter, const QVariant &Value); //�������� �������� � ������ �������
	void RemoveCondition(const QString &Parameter); //������� �������� �� ������ �������
	
	void SetClassFilter(const QString &class_filter); //�������� ������
	void ClearClassFilter(); //�������� ������

	virtual void LoadData() override;
	virtual void Update() override;

protected:
	virtual void LoadDataAfterEvent() override;
	virtual void DoubleClickedList(const QModelIndex &ModelIndex);

private:
	QVariantMap Conditions;
	QString ClassFilter;
};
//-----------------------------------------------------------------------------
