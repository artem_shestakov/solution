#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
#include "ISMetaSettingsGroup.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISSettings : public QObject
{
	Q_OBJECT

public:
	ISSettings(const ISSettings &) = delete;
	ISSettings(ISSettings &&) = delete;
	ISSettings &operator=(const ISSettings &) = delete;
	ISSettings &operator=(ISSettings &&) = delete;
	~ISSettings();

	static ISSettings& GetInstance();

	bool GetValueBool(const QString &SettingUID);
	QString GetValueString(const QString &SettingUID);
	int GetValueInt(const QString &SettingUID);
	
	QVariant GetValue(const QString &SettingUID); //�������� �������� ����-���������
	void SetValue(const QString &SettingUID, const QVariant &Value); //�������� �������� ����-���������
	
	QVector<ISMetaSettingsGroup*> GetSettingGroups(); //�������� ��� ������ ��������

	ISMetaSetting* GetMetaSetting(const QString &SettingUID); //�������� ����-��������� �� � ��������������

protected:
	void Initialize(); //�������������

private:
	ISSettings();

	QVector<ISMetaSettingsGroup*> SettingGroups;
	int CountAllSettings;
};
//-----------------------------------------------------------------------------
#define SETTING_VALUE(SETTING_UID) ISSettings::GetInstance().GetValue(SETTING_UID)
#define SETTING_BOOL(SETTING_UID) ISSettings::GetInstance().GetValueBool(SETTING_UID)
#define SETTING_STRING(SETTING_UID) ISSettings::GetInstance().GetValueString(SETTING_UID)
#define SETTING_INT(SETTING_UID) ISSettings::GetInstance().GetValueInt(SETTING_UID)
//-----------------------------------------------------------------------------
