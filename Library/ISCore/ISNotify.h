#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
#include "ISMetaNotify.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISNotify : public QObject
{
	Q_OBJECT

//������������ ������� ��� ����������� (��� � �.�.)
signals:
	void NewChatMessage(const QVariantMap &VariantMap); //������ � ����� ��������� � ����
	void TermianteUser(const QVariantMap &VariantMap); //������ � �������������� ���������� �� �������
	void UpdateAviable(const QVariantMap &VariantMap); //������ � ������� ����������

//������� �������
signals:
	void Notify(const QVariantMap &VariantMap);
	void Notify();

public:
	ISNotify(const ISNotify &) = delete;
	ISNotify(ISNotify &&) = delete;
	ISNotify &operator=(const ISNotify &) = delete;
	ISNotify &operator=(ISNotify &&) = delete;
	~ISNotify();

	static ISNotify& GetInstance();

	void Initialize(); //�������������
	ISMetaNotify* GetMetaNotify(const QString &NotificationUID);

	void SendNotification(const QString &NotificationUID, const QVariant &Payload = QVariant(), bool Saved = true, int UserTo = 0); //������� �����������

protected:
	void Notification(const QString &NotificationName, QSqlDriver::NotificationSource Source, const QVariant &Payload); //������� �����������

private:
	ISNotify();

	QMap<QString, ISMetaNotify*> Notifications;
	QSqlDriver *SqlDriver;
	QSound *Sound;
};
//-----------------------------------------------------------------------------
