#include "StdAfx.h"
#include "ISCore.h"
#include "ISAssert.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "EXDefines.h"
#include "ISMetaData.h"
#include "ISQuery.h"
#include "ISConfig.h"
#include "ISQueryPool.h"
#include "ISProtocol.h"
#include "ISSystem.h"
#include "ISMemoryObjects.h"
//-----------------------------------------------------------------------------
static QString Q_DELETE_OR_RECOVERY_OBJECT = "UPDATE %1 SET %2_isdeleted = :IsDeleted WHERE %2_id = :ObjectID";
//-----------------------------------------------------------------------------
static QString QD_OBJECT_CASCADE = "DELETE FROM %1 WHERE %2_id = :ObjectID";
//-----------------------------------------------------------------------------
static QString QU_OBJECT = "UPDATE %1 SET %2_deletiondate = now(), %2_deletionuser = CURRENT_USER WHERE %2_id = %3";
//-----------------------------------------------------------------------------
ISCore::ISCore()
{

}
//-----------------------------------------------------------------------------
ISCore::~ISCore()
{

}
//-----------------------------------------------------------------------------
ISFieldEditBase* ISCore::CreateColumnForField(QWidget *ParentWidget, PMetaClassField *MetaField, QString &ControlWidgetName)
{
	ISFieldEditBase *FieldEditBase = nullptr;

	if (MetaField->GetControlWidget().length())
	{
		ControlWidgetName = MetaField->GetControlWidget();
	}

	if (ControlWidgetName.length())
	{
		int ObjectType = QMetaType::type((ControlWidgetName + "*").toLocal8Bit().constData());
		IS_ASSERT(ObjectType, QString("ObjectType for field edit \"%1\" NULL.").arg(ControlWidgetName));

		const QMetaObject *MetaObject = QMetaType::metaObjectForType(ObjectType);
		QObject *Object = MetaObject->newInstance(Q_ARG(QObject *, MetaField), (Q_ARG(QWidget *, ParentWidget)));
		IS_ASSERT(Object, QString("Not created QObject for ClassName: %1").arg(ControlWidgetName));

		FieldEditBase = dynamic_cast<ISFieldEditBase*>(Object);
		IS_ASSERT(FieldEditBase, QString("Not created ISFieldEditBase for ClassName: %1").arg(ControlWidgetName));
	}
	else
	{
		if (MetaField->GetForeign())
		{
			ControlWidgetName = CLASS_IS_LIST_EDIT;
		}
		else
		{
			ControlWidgetName = ISMetaData::GetInstanse().GetAssociationTypes().GetControlWidgetFromType(MetaField->GetType());
		}

		FieldEditBase = CreateColumnForField(ParentWidget, MetaField, ControlWidgetName);
	}

	return FieldEditBase;
}
//-----------------------------------------------------------------------------
ISFieldEditBase* ISCore::CreateColumnForField(QWidget *ParentWidget, ISNamespace::FieldType DataType, QString &ControlWidgetName)
{
	ISFieldEditBase *FieldEditBase = nullptr;

	if (ControlWidgetName.length())
	{
		int ObjectType = QMetaType::type((ControlWidgetName + "*").toLocal8Bit().constData());
		IS_ASSERT(ObjectType, QString("ObjectType for field edit \"%1\" NULL.").arg(ControlWidgetName));

		const QMetaObject *MetaObject = QMetaType::metaObjectForType(ObjectType);
		QObject *Object = MetaObject->newInstance(Q_ARG(QObject *, nullptr), (Q_ARG(QWidget *, ParentWidget)));
		IS_ASSERT(Object, QString("Not created QObject for ClassName: %1").arg(ControlWidgetName));

		FieldEditBase = dynamic_cast<ISFieldEditBase*>(Object);
		IS_ASSERT(FieldEditBase, QString("Not created ISFieldEditBase for ClassName: %1").arg(ControlWidgetName));
	}
	else
	{
		ControlWidgetName = ISMetaData::GetInstanse().GetAssociationTypes().GetControlWidgetFromType(DataType);
		FieldEditBase = CreateColumnForField(ParentWidget, DataType, ControlWidgetName);
	}

	return FieldEditBase;
}
//-----------------------------------------------------------------------------
ISObjectFormBase* ISCore::CreateObjectForm(ISNamespace::ObjectFormType FormType, PMetaClassTable *MetaTable, int ObjectID, QWidget *parent)
{
	ISSystem::SetWaitGlobalCursor(true);

	ISObjectFormBase *ObjectForm = nullptr;

	if (MetaTable->GetObjectForm().length())
	{
		int ObjectType = QMetaType::type((MetaTable->GetObjectForm() + "*").toLocal8Bit().constData());
		IS_ASSERT(ObjectType, QString("ObjectForm for table \"%1\" is null.").arg(MetaTable->GetObjectForm()));

		const QMetaObject *MetaObject = QMetaType::metaObjectForType(ObjectType);
		ObjectForm = dynamic_cast<ISObjectFormBase*>(MetaObject->newInstance(Q_ARG(ISNamespace::ObjectFormType, FormType), Q_ARG(PMetaClassTable *, MetaTable), Q_ARG(QWidget *, parent), Q_ARG(int, ObjectID)));
		IS_ASSERT(ObjectForm, "ObjectForm not dynamic cast.");
	}
	else
	{
		ObjectForm = new ISObjectFormBase(FormType, MetaTable, parent, ObjectID);
	}

	ISSystem::SetWaitGlobalCursor(false);
	return ObjectForm;
}
//-----------------------------------------------------------------------------
bool ISCore::DeleteOrRecoveryObject(ISNamespace::DeleteRecoveryObject DeleteOrRecovery, const QString &TableName, const QString &TableAlias, int ID, const QString &LocalListName)
{
	bool Result = false;

	QString QueryText = Q_DELETE_OR_RECOVERY_OBJECT.arg(TableName).arg(TableAlias);

	ISQuery qDeleteOrRecovery(QueryText);
	qDeleteOrRecovery.BindValue(":ObjectID", ID);

	if (DeleteOrRecovery == ISNamespace::DeleteRecoveryObject::DRO_Delete)
	{
		qDeleteOrRecovery.BindValue(":IsDeleted", true);
	}
	else if (ISNamespace::DeleteRecoveryObject::DRO_Recovery)
	{
		qDeleteOrRecovery.BindValue(":IsDeleted", false);
	}

	Result = qDeleteOrRecovery.Execute();

	if (Result && DeleteOrRecovery == ISNamespace::DRO_Delete)
	{
		QString UpdateQuery = QU_OBJECT;
		UpdateQuery = UpdateQuery.arg(TableName);
		UpdateQuery = UpdateQuery.arg(TableAlias);
		UpdateQuery = UpdateQuery.arg(ID);

		ISQueryPool::GetInstance().AddQuery(UpdateQuery);
	}

	if (DeleteOrRecovery == ISNamespace::DRO_Delete)
	{
		ISProtocol::DeleteObject(TableName, LocalListName, ID);
	}
	else if (DeleteOrRecovery == ISNamespace::DRO_Recovery)
	{
		ISProtocol::RecoveryObject(TableName, LocalListName, ID);
	}

	return Result;
}
//-----------------------------------------------------------------------------
bool ISCore::DeleteCascadeObject(const QString &TableName, const QString &TableAlias, int ObjectID)
{
	bool Result = false;

	QString QueryText = QD_OBJECT_CASCADE.arg(TableName).arg(TableAlias);
	ISQuery qDeleteCascade(QueryText);
	qDeleteCascade.BindValue(":ObjectID", ObjectID);
	if (qDeleteCascade.Execute())
	{
		Result = true;
	}

	return Result;
}
//-----------------------------------------------------------------------------
void ISCore::ChangeUser()
{
	RestartApplication();
}
//-----------------------------------------------------------------------------
void ISCore::RestartApplication()
{
	ExitApplication();
	ISSystem::SleepSeconds(1);
	QProcess::startDetached(QApplication::applicationFilePath());
}
//-----------------------------------------------------------------------------
void ISCore::ExitApplication()
{
	ISSystem::SetObjectProperty(ISMemoryObjects::GetInstance().GetMainWindow(), "CloseEvent", false);

	qApp->closeAllWindows();
	qApp->quit();
}
//-----------------------------------------------------------------------------
QString ISCore::GetObjectName(PMetaClassTable *MetaTable, int ObjectID)
{
	QString ObjectName;

	if (MetaTable->GetTitleName().length())
	{
		QString TitleName = MetaTable->GetTitleName();
		QStringList StringList = TitleName.split(";");
		QString QueryText = "SELECT ";

		if (StringList.count() > 1) //���� ��� ������� �������� �� ���������� �����
		{
			QueryText += "concat(";
			for (int i = 0; i < StringList.count(); i++)
			{
				QueryText += MetaTable->GetAlias() + "_" + StringList.at(i) + ", ' ', ";
			}

			ISSystem::RemoveLastSymbolFromString(QueryText, 7);
			QueryText += ") \n";
		}
		else //���� ������� ������ ���� ����
		{
			QueryText += MetaTable->GetAlias() + "_" + TitleName + " \n";
		}

		QueryText += "FROM " + MetaTable->GetName() + " \n";
		QueryText += "WHERE " + MetaTable->GetAlias() + "_id = :ObjectID";

		ISQuery qSelectName(QueryText);
		qSelectName.BindValue(":ObjectID", ObjectID);
		if (qSelectName.ExecuteFirst())
		{
			ObjectName = qSelectName.ReadColumn(0).toString();
		}
	}
	else
	{
		ObjectName = QString::number(ObjectID);
	}

	return ObjectName;
}
//-----------------------------------------------------------------------------
