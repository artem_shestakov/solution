#include "StdAfx.h"
#include "ISExportWord.h"
//-----------------------------------------------------------------------------
ISExportWord::ISExportWord(QObject *parent) : ISExportWorker(parent)
{
	WordApplication = nullptr;
}
//-----------------------------------------------------------------------------
ISExportWord::~ISExportWord()
{
	delete WordApplication;
}
//-----------------------------------------------------------------------------
bool ISExportWord::Prepare()
{
	WordApplication = new QAxObject("Word.Application", this);
	if (!WordApplication)
	{
		return false;
	}

	QAxObject *documents = WordApplication->querySubObject("Documents"); //�������� ��������� ����������
	QAxObject *document = documents->querySubObject("Add()"); //��������� ���� �������� � ���������
	QAxObject* ActiveDocument = WordApplication->querySubObject("ActiveDocument()");
	ActiveDocument->setProperty("Name", tr("������")); //��������� ���� �������� � ���������

	QAxObject* selection = WordApplication->querySubObject("Selection");

	QAxObject *range = selection->querySubObject("Range()");
	QAxObject *tables = document->querySubObject("Tables()");
	QAxObject *table = tables->querySubObject("Add(Range,NumRows,NumColumns)", range->asVariant(), 3, 4);

	WordApplication->setProperty("Visible", true);

	return true;
}
//-----------------------------------------------------------------------------
bool ISExportWord::Export()
{
	return true;
}
//-----------------------------------------------------------------------------
