#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISTasks : public QObject
{
	Q_OBJECT

public:
	ISTasks(QObject *parent = 0);
	virtual ~ISTasks();

	static bool CheckAttached(int TaskID, const QString &TableName, int ObjectID); //��������� ����������� �� ������ � ��������
	static bool AttachRecord(int TaskID, const QString &TableName, int ObjectID); //���������� ������ � ������
	static bool DetachRecord(int TaskID, const QString &TableName, int ObjectID); //��������� ������ �� ������
	static void ShowTaskForm(int TaskID, QWidget *parent = 0); //������� ������ �� ��������
};
//-----------------------------------------------------------------------------
