#include "StdAfx.h"
#include "ISLicense.h"
#include "EXDefines.h"
#include "ISQuery.h"
#include "ISSystem.h"
#include "ISCrypterLicense.h"
//-----------------------------------------------------------------------------
static QString QS_LICENSE_COUNT = PREPARE_QUERY("SELECT COUNT(*) FROM _license");
//-----------------------------------------------------------------------------
static QString QS_LICENSE = PREPARE_QUERY("SELECT lcns_uid, lcns_licensekey, lcns_counter FROM _license LIMIT 1");
//-----------------------------------------------------------------------------
static QString QU_RECUDE_COUNTER = PREPARE_QUERY("UPDATE _license SET lcns_counter = :Counter");
//-----------------------------------------------------------------------------
static QString QU_ACTIVATE = PREPARE_QUERY("UPDATE _license SET lcns_licensekey = :LicenseKey");
//-----------------------------------------------------------------------------
static QString QU_LICENSE_RESET = PREPARE_QUERY("UPDATE _license SET lcns_licensekey = NULL");
//-----------------------------------------------------------------------------
static QString QU_LICENSE_COUNTER = PREPARE_QUERY("UPDATE _license SET lcns_counter = :Counter");
//-----------------------------------------------------------------------------
static QString QI_LICENSE_EXTEND = PREPARE_QUERY("INSERT INTO _licenseextend(lcex_inputcount, lcex_counter) VALUES(:InputCount, :Counter)");
//-----------------------------------------------------------------------------
static QString QS_LICENSE_EXTEND = PREPARE_QUERY("SELECT COUNT(*) FROM _licenseextend WHERE lcex_counter = :Counter");
//-----------------------------------------------------------------------------
ISLicense::ISLicense()
{
	ErrorString = "No error";
}
//-----------------------------------------------------------------------------
ISLicense::~ISLicense()
{

}
//-----------------------------------------------------------------------------
ISLicense& ISLicense::GetInstance()
{
	static ISLicense Licence;
	return Licence;
}
//-----------------------------------------------------------------------------
void ISLicense::Initialize()
{
	ISQuery qSelect(QS_LICENSE);
	if (qSelect.ExecuteFirst())
	{
		LicenseUID = qSelect.ReadColumn("lcns_uid").toString();
		LicenseKey = qSelect.ReadColumn("lcns_licensekey").toString();
		
		QString StringCounter = qSelect.ReadColumn("lcns_counter").toString();
		StringCounter = ISCrypterLicense::Decrypt(StringCounter);

		Counter = StringCounter.toInt();
	}
}
//-----------------------------------------------------------------------------
void ISLicense::ReduceCounter()
{
	Counter--;

	QString NewCounter = QString::number(Counter);
	NewCounter = ISCrypterLicense::Crypt(NewCounter);

	ISQuery qReduceCounter(QU_RECUDE_COUNTER);
	qReduceCounter.BindValue(":Counter", NewCounter);
	qReduceCounter.Execute();
}
//-----------------------------------------------------------------------------
void ISLicense::Activate(const QString &key)
{
	LicenseKey = key;

	ISQuery qActivate(QU_ACTIVATE);
	qActivate.BindValue(":LicenseKey", LicenseKey);
	qActivate.Execute();
}
//-----------------------------------------------------------------------------
void ISLicense::Extend(int counter, const QVariant &CounterValue)
{
	ISQuery qUpdateCounter(QU_LICENSE_COUNTER);
	qUpdateCounter.BindValue(":Counter", CounterValue);
	qUpdateCounter.Execute();

	ISQuery qInsertExpand(QI_LICENSE_EXTEND);
	qInsertExpand.BindValue(":InputCount", Counter);
	qInsertExpand.BindValue(":Counter", CounterValue);
	qInsertExpand.Execute();

	Counter = counter;
}
//-----------------------------------------------------------------------------
void ISLicense::ResetLicense()
{
	ISQuery qReset(QU_LICENSE_RESET);
	qReset.Execute();
}
//-----------------------------------------------------------------------------
QString ISLicense::GetErrorString() const
{
	return ErrorString;
}
//-----------------------------------------------------------------------------
bool ISLicense::GetActivated() const
{
	if (LicenseKey.length())
	{
		return true;
	}

	return false;
}
//-----------------------------------------------------------------------------
QString ISLicense::GetLicenseUID() const
{
	return LicenseUID;
}
//-----------------------------------------------------------------------------
QString ISLicense::GetLicenseKey() const
{
	return LicenseKey;
}
//-----------------------------------------------------------------------------
int ISLicense::GetCounter() const
{
	return Counter;
}
//-----------------------------------------------------------------------------
bool ISLicense::GetTableEmpty() const
{
	ISQuery qSelectCount(QS_LICENSE_COUNT);
	if (qSelectCount.ExecuteFirst())
	{
		int Count = qSelectCount.ReadColumn("count").toInt();
		if (!Count)
		{
			return true;
		}
	}

	return false;

}
//-----------------------------------------------------------------------------
bool ISLicense::CheckExtend(const QVariant &EnteredCounterCode) const
{
	ISQuery qSelectExtend(QS_LICENSE_EXTEND);
	qSelectExtend.BindValue(":Counter", EnteredCounterCode);
	if (qSelectExtend.ExecuteFirst())
	{
		int Count = qSelectExtend.ReadColumn("count").toInt();
		if (Count)
		{
			return false;
		}

		return true;
	}

	return false;
}
//-----------------------------------------------------------------------------
