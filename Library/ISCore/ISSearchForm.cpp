#include "StdAfx.h"
#include "ISSearchForm.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "EXDefines.h"
#include "ISControls.h"
#include "ISCore.h"
#include "ISAssert.h"
#include "ISListEdit.h"
//-----------------------------------------------------------------------------
ISSearchForm::ISSearchForm(PMetaClassTable *meta_table, QWidget *parent) : ISInterfaceDialogForm(parent)
{
	MetaTable = meta_table;
	SearchResult = ISNamespace::SFR_Unknown;
	EditValue = nullptr;

	setWindowTitle(LOCALIZATION("Search"));
	setWindowIcon(BUFFER_ICONS("Search"));

	GetMainLayout()->setContentsMargins(LAYOUT_MARGINS_10_PX);

	QFormLayout *FormLayout = new QFormLayout();
	GetMainLayout()->addLayout(FormLayout);

	QLabel *LabelWhereSearch = new QLabel(this);
	LabelWhereSearch->setText(LOCALIZATION("Search.WhereSearch") + ":");

	//����
	ComboFields = new ISComboEdit(this);
	ComboFields->SetVisibleClear(false);
	ComboFields->SetEditable(false);
	connect(ComboFields, &ISComboEdit::ValueChange, this, &ISSearchForm::FieldChanged);
	FormLayout->addRow(LabelWhereSearch, ComboFields);

	QLabel *LabelWhatSearch = new QLabel(this);
	LabelWhatSearch->setText(LOCALIZATION("Search.WhatSearch") + ":");

	LayoutValue = new QHBoxLayout();
	LayoutValue->setContentsMargins(LAYOUT_MARGINS_NULL);

	QWidget *Widget = new QWidget(this);
	Widget->setLayout(LayoutValue);
	FormLayout->addRow(LabelWhatSearch, Widget);

	QLabel *LabelHowSearch = new QLabel(this);
	LabelHowSearch->setText(LOCALIZATION("Search.HowSearch") + ":");

	WidgetHowSearch = new QWidget(this);
	WidgetHowSearch->setLayout(new QVBoxLayout());
	FormLayout->addRow(LabelHowSearch, WidgetHowSearch);

	//������ ������
	RadioBeginString = new QRadioButton(WidgetHowSearch);
	RadioBeginString->setText(LOCALIZATION("Search.HowSearch.BeginString"));
	connect(RadioBeginString, &QRadioButton::clicked, this, &ISSearchForm::EnabledSearch);
	WidgetHowSearch->layout()->addWidget(RadioBeginString);

	//��������� ������
	RadioEndString = new QRadioButton(WidgetHowSearch);
	RadioEndString->setText(LOCALIZATION("Search.HowSearch.EndString"));
	connect(RadioEndString, &QRadioButton::clicked, this, &ISSearchForm::EnabledSearch);
	WidgetHowSearch->layout()->addWidget(RadioEndString);

	//����� ������
	RadioPartString = new QRadioButton(WidgetHowSearch);
	RadioPartString->setText(LOCALIZATION("Search.HowSearch.PartString"));
	connect(RadioPartString, &QRadioButton::clicked, this, &ISSearchForm::EnabledSearch);
	WidgetHowSearch->layout()->addWidget(RadioPartString);

	//������ ����������
	RadioExactMatch = new QRadioButton(WidgetHowSearch);
	RadioExactMatch->setText(LOCALIZATION("Search.HowSearch.ExactMatch"));
	connect(RadioExactMatch, &QRadioButton::clicked, this, &ISSearchForm::EnabledSearch);
	WidgetHowSearch->layout()->addWidget(RadioExactMatch);

	GetMainLayout()->addStretch();
	GetMainLayout()->addWidget(ISControls::CreateHorizontalLine(this));

	QHBoxLayout *LayoutBottom = new QHBoxLayout();
	LayoutBottom->addStretch();
	GetMainLayout()->addLayout(LayoutBottom);

	ButtonSearch = new ISPushButton(this);
	ButtonSearch->setText(LOCALIZATION("Search"));
	ButtonSearch->setIcon(BUFFER_ICONS("Search"));
	ButtonSearch->setFont(FONT_APPLICATION_BOLD);
	connect(ButtonSearch, &ISPushButton::clicked, this, &ISSearchForm::Search);
	LayoutBottom->addWidget(ButtonSearch);

	ISPushButton *ButtonClear = new ISPushButton(this);
	ButtonClear->setText(LOCALIZATION("Search.Cancel"));
	connect(ButtonClear, &ISPushButton::clicked, this, &ISSearchForm::Clear);
	LayoutBottom->addWidget(ButtonClear);

	ISPushButton *ButtonClose = new ISPushButton(this);
	ButtonClose->setText(LOCALIZATION("Close"));
	connect(ButtonClose, &ISPushButton::clicked, this, &ISSearchForm::Close);
	LayoutBottom->addWidget(ButtonClose);

	LoadFields();
}
//-----------------------------------------------------------------------------
ISSearchForm::~ISSearchForm()
{

}
//-----------------------------------------------------------------------------
void ISSearchForm::showEvent(QShowEvent *e)
{
	ISInterfaceDialogForm::showEvent(e);
	EditValue->SetFocus();
}
//-----------------------------------------------------------------------------
ISNamespace::SearchFormResult ISSearchForm::GetSearchResult()
{
	return SearchResult;
}
//-----------------------------------------------------------------------------
QString ISSearchForm::GetField() const
{
	return ComboFields->GetValue().toString();
}
//-----------------------------------------------------------------------------
QVariant ISSearchForm::GetValue() const
{
	return EditValue->GetValue();
}
//-----------------------------------------------------------------------------
ISNamespace::SearchHow ISSearchForm::GetSearchHow()
{
	if (RadioBeginString->isChecked())
	{
		return ISNamespace::SH_BeginString;
	}
	else if (RadioEndString->isChecked())
	{
		return ISNamespace::SH_EndString;
	}
	else if (RadioPartString->isChecked())
	{
		return ISNamespace::SH_PartString;
	}
	else if (RadioExactMatch->isChecked())
	{
		return ISNamespace::SH_ExactMatch;
	}

	IS_ASSERT(false, "Invalid search.");
	return ISNamespace::SH_Unknown;
}
//-----------------------------------------------------------------------------
void ISSearchForm::LoadFields()
{
	PMetaClassField *MetaFieldID = MetaTable->GetFieldID();
	ComboFields->AddItem(MetaFieldID->GetLabelName(), MetaFieldID->GetName());

	for (int i = 0; i < MetaTable->GetFields().count(); i++)
	{
		PMetaClassField *MetaField = MetaTable->GetFields().at(i);
		if (MetaField->GetNotSearch() || MetaField->GetQueryText().length()) //���� �� ��������� ���� ����� ��������
		{
			continue;
		}

		ComboFields->AddItem(MetaField->GetLabelName(), MetaField->GetName());
	}
}
//-----------------------------------------------------------------------------
void ISSearchForm::FieldChanged(const QVariant &Value)
{
	PMetaClassField *MetaField = MetaTable->GetField(Value.toString());
	ISNamespace::FieldType FieldType = MetaField->GetType();

	if (EditValue)
	{
		delete EditValue;
		EditValue = nullptr;
	}

	EditValue = ISCore::CreateColumnForField(this, MetaField);
	ISListEdit *ListEdit = dynamic_cast<ISListEdit*>(EditValue);
	if (ListEdit) //���� ���� �������� ���������� �����������
	{
		//ListEdit->SetListComboBox(false);
		//ListEdit->SetVisibleList(false);
		
		RadioExactMatch->setChecked(true);
		WidgetHowSearch->setEnabled(false);
	}
	else if (FieldType == ISNamespace::FT_Bool || FieldType == ISNamespace::FT_DateTime || FieldType == ISNamespace::FT_Date || FieldType == ISNamespace::FT_Time)
	{
		RadioExactMatch->setChecked(true);
		WidgetHowSearch->setEnabled(false);
	}
	else if (FieldType == ISNamespace::FT_Int)
	{
		RadioExactMatch->setChecked(true);
		WidgetHowSearch->setEnabled(false);
	}
	else
	{
		RadioBeginString->setChecked(true);
		WidgetHowSearch->setEnabled(true);
	}

	connect(EditValue, &ISFieldEditBase::ValueChange, this, &ISSearchForm::EnabledSearch);
	LayoutValue->addWidget(EditValue);
}
//-----------------------------------------------------------------------------
void ISSearchForm::EnabledSearch()
{
	QVariant Value = EditValue->GetValue();
	if (Value.isValid() && !Value.isNull())
	{
		if (RadioBeginString->isChecked() || RadioPartString->isChecked() || RadioExactMatch->isChecked() || RadioEndString->isChecked())
		{
			ButtonSearch->setEnabled(true);
		}
		else
		{
			ButtonSearch->setEnabled(false);
		}
	}
	else
	{
		ButtonSearch->setEnabled(false);
	}
}
//-----------------------------------------------------------------------------
void ISSearchForm::EnterClicked()
{
	Search();
}
//-----------------------------------------------------------------------------
void ISSearchForm::EscapeClicked()
{
	Close();
}
//-----------------------------------------------------------------------------
void ISSearchForm::Search()
{
	SearchResult = ISNamespace::SFR_Search;
	SetResult(true);
	close();
}
//-----------------------------------------------------------------------------
void ISSearchForm::Clear()
{
	SearchResult = ISNamespace::SFR_ClearResult;
	SetResult(true);
	close();
}
//-----------------------------------------------------------------------------
void ISSearchForm::Close()
{
	SearchResult = ISNamespace::SFR_Unknown;
	SetResult(false);
	close();
}
//-----------------------------------------------------------------------------
