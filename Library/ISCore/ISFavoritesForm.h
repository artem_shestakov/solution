#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
#include "ISInterfaceForm.h"
#include "PMetaClassTable.h"
#include "ISListWidget.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISFavoritesForm : public ISInterfaceForm
{
	Q_OBJECT

signals:
	void AddFormFromTab(QWidget *ObjectForm);

public:
	ISFavoritesForm(QWidget *parent = 0, PMetaClassTable *MetaTable = nullptr);
	virtual ~ISFavoritesForm();

protected:
	void LoadFavorites(); //�������� ����������
	void ReloadFavorites(); //������������ ���������
	void OpenFavorite(); //������� ��������� ������
	void DeleteFavorite(); //������� ��������� ������
	void ClearFavorites(); //�������� ��������� �������
	void EscapeClicked() override;

protected slots:
	void ListWidgetDoubleClicked(QListWidgetItem *Item);
	void ItemClicked(QListWidgetItem *Item);

private:
	QToolBar *ToolBar;
	QAction *ActionOpen;
	QAction *ActionDelete;
	QAction *ActionClearFavorites;
	ISListWidget *ListWidget;
	PMetaClassTable *MetaTable;
};
//-----------------------------------------------------------------------------
