#include "StdAfx.h"
#include "ISListBaseForm.h"
#include "ISAssert.h"
#include "ISCore.h"
#include "ISSettings.h"
#include "EXDefines.h"
#include "ISLocalization.h"
#include "ISSortingBuffer.h"
#include "ISProtocol.h"
#include "ISRecordInfoForm.h"
#include "ISNotificationService.h"
#include "ISExportForm.h"
#include "ISProgressForm.h"
#include "ISProcessForm.h"
#include "ISFavoritesForm.h"
#include "ISBuffer.h"
#include "ISStyleSheet.h"
#include "ISControls.h"
#include "ISMessageBox.h"
#include "ISPrintingBase.h"
#include "ISPrintingHtml.h"
#include "ISPrintingWord.h"
#include "ISUserRoleEntity.h"
#include "ISSystem.h"
#include "ISQuery.h"
#include "ISPrintingEntity.h"
#include "ISPrintForm.h"
#include "ISDatabaseHelper.h"
#include "ISNoteObjectForm.h"
#include "ISDatabase.h"
#include "ISDelegateBoolean.h"
#include "ISDelegateImage.h"
#include "ISDelegateColor.h"
#include "ISMetaData.h"
#include "EXConstants.h"
#include "ISMetaUser.h"
#include "ISDebug.h"
#include "ISColumnSizer.h"
#include "ISStorageFileLoader.h"
#include "ISFileDialog.h"
#include "ISInputDialog.h"
#include "ISExportWorker.h"
#include "ISExportExcel.h"
#include "ISExportWord.h"
#include "ISTrace.h"
#include "ISTasks.h"
#include "ISTasksAttachForm.h"
#include "ISMetaQuery.h"
//-----------------------------------------------------------------------------
ISListBaseForm::ISListBaseForm(PMetaClassTable *meta_table, QWidget *parent) : ISInterfaceMetaForm(parent)
{
	MetaTable = meta_table;
	SqlModel = nullptr;
	CurrentShowDataType = ISNamespace::SDT_Actual;
	PageNavigation = nullptr;
	QueryModel = nullptr;
	SearchForm = nullptr;
	UpdateDataSort = false;
	SelectObjectAfterUpdate = 0;
	DelegatesCreated = false;
	ShowOnly = MetaTable->GetShowOnly();
	
	if (MetaTable->GetParent().length())
	{
		IsQueryForm = true;
	}
	else
	{
		IsQueryForm = false;
	}

	//���������� ������ �������
	RememberColumnSize = SETTING_BOOL(CONST_UID_SETTING_TABLES_REMEMBERCOLUMNSIZE);

	//������ ��������, ����������� ������ � ������ �������
	ActionObjectGroup = new QActionGroup(this);

	//�������� ��������
	CreateActions();

	//�������� ����������� ��������
	CreateSpecialActions();

	//�������� �������
	CreateToolBar();

	//�������� �������
	CreateTableView();

	//�������� ������������ ����
	CreateContextMenu();

	//�������� �������
	CreateModels();

	//�������� ������-����
	CreateStatusBar();

	ListIndicatorWidget = new ISListIndicatorWidget(this);
}
//-----------------------------------------------------------------------------
ISListBaseForm::~ISListBaseForm()
{
	ModelThreadQuery->quit();
	bool Wait = ModelThreadQuery->wait();
	IS_ASSERT(Wait, "Not wait() thread");

	if (SearchForm)
	{
		delete SearchForm;
		SearchForm = nullptr;
	}
}
//-----------------------------------------------------------------------------
int ISListBaseForm::GetCurrentRowIndex()
{
	int CurrentRow = TableView->selectionModel()->currentIndex().row();
	if (CurrentRow == -1)
	{
		return 0;
	}
	
	return CurrentRow;
}
//-----------------------------------------------------------------------------
int ISListBaseForm::GetObjectID()
{
	int ObjectID = GetCurrentRecordValue("ID").toInt();
	IS_ASSERT(ObjectID, "ObjectID invalid.");
	return ObjectID;
}
//-----------------------------------------------------------------------------
int ISListBaseForm::GetObjectID(int RowIndex)
{
	int ObjectID = SqlModel->index(RowIndex, 0).data().toInt();
	return ObjectID;
}
//-----------------------------------------------------------------------------
int ISListBaseForm::GetRowIndex(int object_id)
{
	for (int i = 0; i < SqlModel->rowCount(); i++)
	{
		int RowIndex = SqlModel->data(SqlModel->index(i, 0)).toInt();
		if (RowIndex == object_id)
		{
			return i;
		}
	}

	return -1;
}
//-----------------------------------------------------------------------------
QSqlRecord ISListBaseForm::GetCurrentRecord()
{
	QSqlRecord Record = SqlModel->GetRecord(GetCurrentRowIndex());
	return Record;
}
//-----------------------------------------------------------------------------
QVariant ISListBaseForm::GetCurrentRecordValue(const QString &FieldName)
{
	QVariant Value = GetCurrentRecord().value(FieldName);
	return Value;
}
//-----------------------------------------------------------------------------
QVariant ISListBaseForm::GetCurrentRecordValueDB(const QString &FieldName)
{
	ISQuery qSelect(QString("SELECT %1_%2 FROM %3 WHERE %1_id = :ObjectID").arg(MetaTable->GetAlias()).arg(FieldName).arg(MetaTable->GetName()));
	qSelect.BindValue(":ObjectID", GetObjectID());
	if (qSelect.ExecuteFirst())
	{
		return qSelect.ReadColumn(0);
	}

	return QVariant();
}
//-----------------------------------------------------------------------------
QVariant ISListBaseForm::GetRecordValue(const QString &FieldName, int RowIndex)
{
	QVariant Value = SqlModel->GetRecord(RowIndex).value(FieldName);
	return Value;
}
//-----------------------------------------------------------------------------
QVector<int> ISListBaseForm::GetSelectedIDs()
{
	QVector<int> Vector;

	QModelIndexList ModelIndexList = GetTableView()->selectionModel()->selectedRows();
	if (ModelIndexList.count())
	{
		for (int i = 0; i < ModelIndexList.count(); i++)
		{
			QModelIndex ModelIndex = ModelIndexList.at(i);
			int RowIndex = ModelIndex.row();
			int ObjectID = GetObjectID(RowIndex);
			Vector.append(ObjectID);
		}
	}

	return Vector;
}
//-----------------------------------------------------------------------------
QVector<int> ISListBaseForm::GetSelectedRowIndexes()
{
	QVector<int> Vector;

	QModelIndexList ModelIndexList = GetTableView()->selectionModel()->selectedRows();
	if (ModelIndexList.count())
	{
		for (int i = 0; i < ModelIndexList.count(); i++)
		{
			QModelIndex ModelIndex = ModelIndexList.at(i);
			Vector.append(ModelIndex.row());
		}
	}

	return Vector;
}
//-----------------------------------------------------------------------------
ISQueryModel* ISListBaseForm::GetQueryModel()
{
	return QueryModel;
}
//-----------------------------------------------------------------------------
void ISListBaseForm::SetSelectObjectAfterUpdate(int object_id)
{
	SelectObjectAfterUpdate = object_id;
}
//-----------------------------------------------------------------------------
PMetaClassTable* ISListBaseForm::GetMetaTable()
{
	return MetaTable;
}
//-----------------------------------------------------------------------------
void ISListBaseForm::DoubleClickedTable(const QModelIndex &ModelIndex)
{
	if (ShowOnly)
	{
		emit DoubleClicked(ModelIndex);
	}
	else
	{
		if (ISUserRoleEntity::GetInstance().CheckPermission(GetUID(), ISNamespace::PT_Edit))
		{
			Edit();
		}
	}
}
//-----------------------------------------------------------------------------
void ISListBaseForm::HeaderResized(int Column, int OldSize, int NewSize)
{
	if (RememberColumnSize)
	{
		QString FieldName;

		for (int i = 0; i < SqlModel->columnCount(); i++)
		{
			if (Column == i)
			{
				FieldName = SqlModel->headerData(i, Qt::Horizontal, Qt::UserRole).toString();
				break;
			}
		}

		if (MetaTable->GetField(FieldName)->GetIsSystem())
		{
			return;
		}

		ISColumnSizer::GetInstance().SetColumnSize(MetaTable->GetName(), FieldName, NewSize);
	}
}
//-----------------------------------------------------------------------------
void ISListBaseForm::SortingChanged(int LogicalIndex, Qt::SortOrder SortOrder)
{
	QString FieldName = SqlModel->headerData(LogicalIndex, Qt::Horizontal, Qt::UserRole).toString();
	QString OrderField = MetaTable->GetAlias() + "_" + FieldName.toLower();
	QueryModel->SetOrderField(OrderField);
	QueryModel->SetOrderSort(SortOrder);
	
	SqlModel->SetCurrentSorting(LogicalIndex, SortOrder);

	if (SETTING_BOOL(CONST_UID_SETTING_TABLES_REMEMBERSORTING))
	{
		ISSortingBuffer::GetInstance().AddSorting(MetaTable->GetName(), FieldName, SortOrder);
	}

	if (UpdateDataSort)
	{
		Update();
	}

	//������������ � ���������� ����� �����������, ����� � ���� ������ ���������� ��������
	disconnect(TableView->horizontalHeader(), &QHeaderView::sortIndicatorChanged, this, &ISListBaseForm::SortingChanged);
	TableView->horizontalHeader()->setSortIndicator(LogicalIndex, SortOrder);
	connect(TableView->horizontalHeader(), &QHeaderView::sortIndicatorChanged, this, &ISListBaseForm::SortingChanged);
}
//-----------------------------------------------------------------------------
void ISListBaseForm::SortingDefault()
{
	if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.SortingDefault")))
	{
		TableView->horizontalHeader()->sortIndicatorChanged(0, Qt::AscendingOrder);
	}
}
//-----------------------------------------------------------------------------
void ISListBaseForm::HideSystemFields()
{
	HideField("IsDeleted");
	HideField("IsSystem");
	HideField("CreationUser");
	HideField("UpdationUser");
}
//-----------------------------------------------------------------------------
void ISListBaseForm::SelectedRowEvent(const QItemSelection &ItemSelected, const QItemSelection &ItemDeSelected)
{
	int SelectedRows = TableView->selectionModel()->selectedRows().count();
	if (SelectedRows > 1 || SelectedRows == 0)
	{
		SetEnabledActionObject(false);
	}
	else
	{
		SetEnabledActionObject(true);
	}

	ActionSetEnabled(ISNamespace::AT_Delete, SelectedRows);
	ActionSetEnabled(ISNamespace::AT_DeleteCascade, SelectedRows);
}
//-----------------------------------------------------------------------------
void ISListBaseForm::LoadData()
{
	ISSortingMetaTable *MetaSorting = ISSortingBuffer::GetInstance().GetSorting(MetaTable->GetName());
	if (MetaSorting) //���� ���������� ��� ���� ������� ��� ����������, ������������ �
	{
		int FieldIndex = SqlModel->GetFieldIndex(MetaSorting->GetFieldName());
		Qt::SortOrder Sorting = static_cast<Qt::SortOrder>(MetaSorting->GetSortingType());
		SortingChanged(FieldIndex, Sorting);
	}
	else //���������� �� ����������, ������������ ����������� �� ���� (�� �������� � ��������)
	{
		SortingChanged(0, Qt::AscendingOrder);
	}

	QueryModel->SetParentObjectIDClassFilter(GetParentObjectID());
	Update();

	UpdateDataSort = true;
}
//-----------------------------------------------------------------------------
void ISListBaseForm::CornerButtonClicked()
{
	if (SqlModel->rowCount())
	{
		TableView->selectAll();
		TableView->setFocus();
	}
}
//-----------------------------------------------------------------------------
void ISListBaseForm::LoadDataAfterEvent()
{
	ResizeColumnsToContents();
	CalculateRowCount();
	SetEnabledActionObject(false);
	SelectRowObject(SelectObjectAfterUpdate);

	if (SqlModel->rowCount())
	{
		ListIndicatorWidget->setCursor(CURSOR_WAIT);
		ListIndicatorWidget->setToolTip(QString());

		EditSearch->setEnabled(true);
		ProgressSearchFast->setEnabled(true);
		ProgressSearchFast->setMaximum(SqlModel->rowCount());
	}
	else
	{
		ListIndicatorWidget->SetPixmap(BUFFER_ICONS("LabelNoDataTable").pixmap(SIZE_32_32));
		ListIndicatorWidget->SetText(LOCALIZATION("NoData"));
		ListIndicatorWidget->setCursor(CURSOR_WHATS_THIS);
		ListIndicatorWidget->setToolTip(LOCALIZATION("ClickCreateFromCreateObject"));
		ListIndicatorWidget->setVisible(true);

		EditSearch->setEnabled(false);
		ProgressSearchFast->setEnabled(false);
		ProgressSearchFast->setMaximum(100);
		ProgressSearchFast->setValue(0);
	}

	TableView->setFocus();

	if (ShowOnly)
	{
		ActionSetVisible(ISNamespace::AT_Create, false);
		ActionSetVisible(ISNamespace::AT_CreateCopy, false);
		ActionSetVisible(ISNamespace::AT_Edit, false);
		ActionSetVisible(ISNamespace::AT_Delete, false);
		ActionSetVisible(ISNamespace::AT_DeleteCascade, false);
	}
	else
	{
		if (CurrentShowDataType == ISNamespace::SDT_Actual)
		{
			if (ISUserRoleEntity::GetInstance().CheckPermission(GetUID(), ISNamespace::PT_Create))
			{
				ActionSetVisible(ISNamespace::AT_Create, true);
			}

			if (ISUserRoleEntity::GetInstance().CheckPermission(GetUID(), ISNamespace::PT_CreateCopy))
			{
				ActionSetVisible(ISNamespace::AT_CreateCopy, true);
			}

			if (ISUserRoleEntity::GetInstance().CheckPermission(GetUID(), ISNamespace::PT_Edit))
			{
				ActionSetVisible(ISNamespace::AT_Edit, true);
			}
		}
		else if (CurrentShowDataType == ISNamespace::SDT_Deleted)
		{
			if (ISUserRoleEntity::GetInstance().CheckPermission(GetUID(), ISNamespace::PT_Create))
			{
				ActionSetVisible(ISNamespace::AT_Create, false);
			}

			if (ISUserRoleEntity::GetInstance().CheckPermission(GetUID(), ISNamespace::PT_CreateCopy))
			{
				ActionSetVisible(ISNamespace::AT_CreateCopy, false);
			}

			if (ISUserRoleEntity::GetInstance().CheckPermission(GetUID(), ISNamespace::PT_Edit))
			{
				ActionSetVisible(ISNamespace::AT_Edit, false);
			}
		}
	}

	emit Updated();
}
//-----------------------------------------------------------------------------
void ISListBaseForm::AfterShowEvent()
{
	ISInterfaceMetaForm::AfterShowEvent();
	UseAccess();
}
//-----------------------------------------------------------------------------
void ISListBaseForm::AddAction(QAction *Action, bool AddingToActionGroup, bool AddingToContextMenu)
{
	ToolBar->addAction(Action);
	TableView->addAction(Action);

	if (AddingToActionGroup)
	{
		ActionObjectGroup->addAction(Action);
	}

	if (AddingToContextMenu)
	{
		ContextMenu->addAction(Action);
	}
}
//-----------------------------------------------------------------------------
void ISListBaseForm::InsertAction(QAction *ActionBefore, QAction *ActionAfter, bool AddingToActionGroup)
{
	ToolBar->insertAction(ActionAfter, ActionBefore);
	TableView->insertAction(ActionAfter, ActionBefore);

	if (AddingToActionGroup)
	{
		ActionObjectGroup->addAction(ActionBefore);
	}
}
//-----------------------------------------------------------------------------
void ISListBaseForm::paintEvent(QPaintEvent *e)
{
	ISInterfaceMetaForm::paintEvent(e);

	if (ListIndicatorWidget->isVisible())
	{
		CenteringIndicatorWidget();
	}
}
//-----------------------------------------------------------------------------
ISBaseTableView* ISListBaseForm::GetTableView()
{
	return TableView;
}
//-----------------------------------------------------------------------------
QToolBar* ISListBaseForm::GetToolBar()
{
	return ToolBar;
}
//-----------------------------------------------------------------------------
QHBoxLayout* ISListBaseForm::GetLayoutTableView()
{
	return LayoutTableView;
}
//-----------------------------------------------------------------------------
QAction* ISListBaseForm::GetAction(ISNamespace::ActionType action_type)
{
	return Actions.value(action_type);
}
//-----------------------------------------------------------------------------
QAction* ISListBaseForm::GetSpecialAction(ISNamespace::ActionSpecialType action_special)
{
	return ActionsSpecial.value(action_special);
}
//-----------------------------------------------------------------------------
ISSqlModelCore* ISListBaseForm::GetSqlModel()
{
	return SqlModel;
}
//-----------------------------------------------------------------------------
ISModelThreadQuery* ISListBaseForm::GetModelThread()
{
	return ModelThreadQuery;
}
//-----------------------------------------------------------------------------
void ISListBaseForm::CreateDelegates()
{
	if (DelegatesCreated) //���� �������� ��� ������� - �������� �� ������
	{
		return;
	}

	for (int i = 0; i < SqlModel->columnCount(); i++) //����� �����
	{
		QString HeaderData = SqlModel->headerData(i, Qt::Horizontal, Qt::UserRole).toString();
		PMetaClassField *MetaField = MetaTable->GetField(HeaderData);

		QAbstractItemDelegate *AbstractItemDelegate = TableView->itemDelegateForColumn(i);
		if (!AbstractItemDelegate)
		{
			switch (MetaField->GetType())
			{
			case ISNamespace::FT_Bool: AbstractItemDelegate = new ISDelegateBoolean(TableView); break;
			case ISNamespace::FT_Image: AbstractItemDelegate = new ISDelegateImage(TableView); break;
			case ISNamespace::FT_Color: AbstractItemDelegate = new ISDelegateColor(TableView); break;
			}

			if (AbstractItemDelegate)
			{
				TableView->setItemDelegateForColumn(i, AbstractItemDelegate);
			}
		}
	}

	DelegatesCreated = true;
}
//-----------------------------------------------------------------------------
bool ISListBaseForm::CheckIsSystemObject()
{
	bool IsSystem = GetCurrentRecordValue("IsSystem").toBool();
	return IsSystem;
}
//-----------------------------------------------------------------------------
void ISListBaseForm::HideField(const QString &FieldName)
{
	for (int i = 0; i < SqlModel->columnCount(); i++)
	{
		QString HeaderData = SqlModel->headerData(i, Qt::Horizontal, Qt::UserRole).toString();
		if (HeaderData == FieldName)
		{
			TableView->hideColumn(i);
			return;
		}
	}

	IS_ASSERT(false, QString("Not found field: %1").arg(FieldName));
}
//-----------------------------------------------------------------------------
void ISListBaseForm::ShowField(const QString &FieldName)
{
	for (int i = 0; i < SqlModel->columnCount(); i++)
	{
		QString HeaderData = SqlModel->headerData(i, Qt::Horizontal, Qt::UserRole).toString();
		if (HeaderData == FieldName)
		{
			TableView->showColumn(i);
			return;
		}
	}

	IS_ASSERT(false, QString("Not found field: %1").arg(FieldName));
}
//-----------------------------------------------------------------------------
void ISListBaseForm::SetShowOnly(bool show_only)
{
	ShowOnly = show_only;
}
//-----------------------------------------------------------------------------
void ISListBaseForm::UseAccess()
{
	QList<QAction*> ActionsList = Actions.values();
	for (QAction *Action : ActionsList)
	{
		if (Action->data().isValid())
		{
			ISNamespace::PermissionsType Type = qvariant_cast<ISNamespace::PermissionsType>(Action->data());
			if (Type != ISNamespace::PT_Unknown)
			{
				bool Aviable = ISUserRoleEntity::GetInstance().CheckPermission(GetUID(), Type);
				Action->setVisible(Aviable);
			}
		}
	}

	//�������� ��������� ������ ������
	if (ISPrintingEntity::GetInstance().GetCountReports(MetaTable->GetName()) && ISUserRoleEntity::GetInstance().CheckPermission(GetUID(), ISNamespace::PT_Print))
	{
		ActionSetVisible(ISNamespace::AT_Print, true); 
	}
	else
	{
		ActionSetVisible(ISNamespace::AT_Print, false);
	}
}
//-----------------------------------------------------------------------------
void ISListBaseForm::ResizeColumnsToContents()
{
	disconnect(TableView->horizontalHeader(), &QHeaderView::sectionResized, this, &ISListBaseForm::HeaderResized);

	for (int i = 0; i < SqlModel->columnCount(); i++)
	{
		QString FieldName = SqlModel->headerData(i, Qt::Horizontal, Qt::UserRole).toString();
		int ColumnSize = ISColumnSizer::GetInstance().GetColumnSize(MetaTable->GetName(), FieldName);
		if (ColumnSize) //���� ���� ������ ������� � ������, ������������ ���
		{
			TableView->setColumnWidth(i, ColumnSize);
		}
		else //�����, ���� � ����-������ ���������� ���� ������� ������ �������, ������ ������ ������
		{
			if (MetaTable->GetField(FieldName)->GetResizeColumn())
			{
				TableView->resizeColumnToContents(i);
			}
		}
	}

	connect(TableView->horizontalHeader(), &QHeaderView::sectionResized, this, &ISListBaseForm::HeaderResized);
}
//-----------------------------------------------------------------------------
void ISListBaseForm::CalculateRowCount()
{
	QString Text = LOCALIZATION("RecordsCount") + ": " + QString::number(SqlModel->rowCount());
	LabelRowCount->setText(Text); //��������� �������� � ������� "�������"
}
//-----------------------------------------------------------------------------
void ISListBaseForm::AddWidgetToBottom(QWidget *Widget)
{
	StatusBar->addWidget(Widget);
}
//-----------------------------------------------------------------------------
void ISListBaseForm::SetVisibleBottom(bool Visible)
{
	StatusBar->setVisible(Visible);
}
//-----------------------------------------------------------------------------
void ISListBaseForm::ClosingObjectForm()
{
	TableView->setFocus();
}
//-----------------------------------------------------------------------------
void ISListBaseForm::CenteringIndicatorWidget()
{
	QRect Rect = TableView->frameGeometry();
	QPoint CenterPoint = Rect.center();
	CenterPoint.setX(CenterPoint.x() - (ListIndicatorWidget->width() / 2));
	CenterPoint.setY(CenterPoint.y() - (ListIndicatorWidget->height() / 2));
	ListIndicatorWidget->move(CenterPoint);
}
//-----------------------------------------------------------------------------
void ISListBaseForm::SelectRowObject(int object_id)
{
	if (object_id)
	{
		int RowIndex = GetRowIndex(object_id);
		if (RowIndex != -1)
		{
			SelectRowIndex(RowIndex);
			SetSelectObjectAfterUpdate(-1);
		}
	}
}
//-----------------------------------------------------------------------------
void ISListBaseForm::SelectRowIndex(int row_index)
{
	TableView->selectRow(row_index);
	TableView->scrollTo(TableView->model()->index(row_index, 0));
}
//-----------------------------------------------------------------------------
void ISListBaseForm::SetEnabledActionObject(bool Enabled)
{
	for (int i = 0; i < ActionObjectGroup->actions().count(); i++)
	{
		ActionObjectGroup->actions().at(i)->setEnabled(Enabled);
	}
}
//-----------------------------------------------------------------------------
void ISListBaseForm::SearchFast(const QVariant &value)
{
	if (value.toString() == EditSearch->accessibleName()) //���� ���������� ��������� ��������� ����� ������ - ����� �� ���������
	{
		return;
	}

	ISSystem::SetWaitGlobalCursor(true);

	if (value.isValid()) //���� ������������ ���� ��������� ��������
	{
		ProgressSearchFast->setFormat(LOCALIZATION("Searching") + ": %p%");

		QString SearchValue = value.toString().toLower(); //��������� ��������
		int FoundedRecords = 0; //���������� ��������� �������
		int PreviousValueCount = EditSearch->accessibleName().length(); //���������� �������� ����������� ���������� ���������

		for (int Row = 0; Row < SqlModel->rowCount(); Row++) //����� �������
		{
			if (SearchValue.length() > PreviousValueCount) //���� ���������� ���� ������ (���������� �������� ������ ���������� �������� ������ �����������)
			{
				if (TableView->isRowHidden(Row)) //���� ������ ��� ������ (�������� � ��� �� ���� ������� ��� ���������� ������) - ���������� ����� �� ���
				{
					continue;
				}
			}

			for (int Column = 0; Column < SqlModel->columnCount(); Column++) //����� ����� ������
			{
				ISNamespace::FieldType FieldType = SqlModel->GetField(Column)->GetType();

				//������ ������ �� ����� ����:
				if (FieldType == ISNamespace::FT_Bool || 
					FieldType == ISNamespace::FT_ByteArray || 
					FieldType == ISNamespace::FT_Color ||
					FieldType == ISNamespace::FT_Image)
				{
					continue;
				}

				QString CurrentValue = SqlModel->data(SqlModel->index(Row, Column)).toString().toLower(); //SqlField.value().toString().toLower();
				if (CurrentValue.contains(SearchValue))
				{
					TableView->showRow(Row);
					FoundedRecords++;
					break;
				}
				else
				{
					TableView->hideRow(Row);
				}
			}

			ProgressSearchFast->setValue(Row);
		}

		ProgressSearchFast->setValue(SqlModel->rowCount());
		ProgressSearchFast->setFormat(LOCALIZATION("Founded") + ": " + QString::number(FoundedRecords));
		EditSearch->setAccessibleName(SearchValue); //��������� ���������� ��������� ��������
	}
	else //���� ������������ ������� ��������� ��������
	{
		//������ ��� ������ � ������� �� ��������
		for (int Row = 0; Row < SqlModel->rowCount(); Row++)
		{
			TableView->showRow(Row);
		}

		ProgressSearchFast->setFormat(QString());
		EditSearch->setAccessibleName(QString());
	}

	ProgressSearchFast->setValue(0);
	ISSystem::SetWaitGlobalCursor(false);
}
//-----------------------------------------------------------------------------
void ISListBaseForm::ActionSetVisible(ISNamespace::ActionType action_type, bool visible)
{
	QAction *Action = GetAction(action_type);
	if (Action)
	{
		Action->setVisible(visible);
	}
}
//-----------------------------------------------------------------------------
void ISListBaseForm::ActionSetEnabled(ISNamespace::ActionType action_type, bool enabled)
{
	QAction *Action = GetAction(action_type);
	if (Action)
	{
		Action->setEnabled(enabled);
	}
}
//-----------------------------------------------------------------------------
void ISListBaseForm::ActionSetText(ISNamespace::ActionType action_type, const QString &text)
{
	QAction *Action = GetAction(action_type);
	if (Action)
	{
		Action->setText(text);
	}
}
//-----------------------------------------------------------------------------
void ISListBaseForm::ModelThreadStarted()
{
	ISSystem::SetWaitGlobalCursor(true);

	ListIndicatorWidget->SetVisibleAnimation(true, BUFFER_ANIMATION("Load", nullptr));
	ListIndicatorWidget->SetText(LOCALIZATION("LoadData") + "...");
	ListIndicatorWidget->setVisible(true);

	LabelRowCount->setText(LOCALIZATION("RecordsCount") + ": " + LOCALIZATION("Calculated") + "...");
	ToolBar->setEnabled(false);
}
//-----------------------------------------------------------------------------
void ISListBaseForm::ModelThreadLoadingData()
{
	ListIndicatorWidget->SetText(LOCALIZATION("FillTableData") + "...");
}
//-----------------------------------------------------------------------------
void ISListBaseForm::ModelThreadFinished()
{
	ISSystem::SetWaitGlobalCursor(false);

	ListIndicatorWidget->hide();
	ToolBar->setEnabled(true);

	CreateDelegates();
	LoadDataAfterEvent();
}
//-----------------------------------------------------------------------------
void ISListBaseForm::ModelThreadErrorConnection(const QSqlError &SqlError)
{
	ISSystem::SetWaitGlobalCursor(false);

	ListIndicatorWidget->hide();
	ToolBar->setEnabled(true);

	if (SqlError.type() == QSqlError::ConnectionError)
	{
		throw ISQueryExceptionConnection(SqlError.text());
	}
	else
	{
		ISDebug::ShowWarningString(SqlError.text());
		ISMessageBox::ShowCritical(this, LOCALIZATION("Message.Error.ConnectionLoadingData") + "\n\n" + SqlError.text());
	}
}
//-----------------------------------------------------------------------------
void ISListBaseForm::ModelThreadErrorQuery(const QSqlError &SqlError, const QString &QueryText)
{
	ISSystem::SetWaitGlobalCursor(false);

	ListIndicatorWidget->hide();
	ToolBar->setEnabled(true);

	if (SqlError.type() == QSqlError::ConnectionError)
	{
		throw ISQueryExceptionConnection(SqlError.text());
	}
	else
	{
		ISDebug::ShowWarningString(SqlError.text());
		ISMessageBox::ShowCritical(this, LOCALIZATION("Message.Error.ConnectionLoadingData") + "\n\n" + SqlError.text(), QueryText);
	}
}
//-----------------------------------------------------------------------------
void ISListBaseForm::Picking()
{

}
//-----------------------------------------------------------------------------
void ISListBaseForm::Select()
{

}
//-----------------------------------------------------------------------------
void ISListBaseForm::Create()
{
	ISObjectFormBase *ObjectForm = ISCore::CreateObjectForm(ISNamespace::OFT_New, MetaTable, 0, parentWidget());
	ObjectForm->SetParentObjectID(GetParentObjectID());
	connect(ObjectForm, &ISObjectFormBase::SavedObject, this, &ISListBaseForm::SetSelectObjectAfterUpdate);
	connect(ObjectForm, &ISObjectFormBase::SavedObject, this, &ISListBaseForm::Updated);
	connect(ObjectForm, &ISObjectFormBase::UpdateList, this, &ISListBaseForm::Update);
	connect(ObjectForm, &ISObjectFormBase::Close, this, &ISListBaseForm::ClosingObjectForm);
	emit AddFormFromTab(ObjectForm);
}
//-----------------------------------------------------------------------------
void ISListBaseForm::CreateCopy()
{
	if (CheckIsSystemObject())
	{
		ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.SystemObject.NotEdit"));
		return;
	}

	ISObjectFormBase *ObjectForm = ISCore::CreateObjectForm(ISNamespace::ObjectFormType::OFT_Copy, MetaTable, GetObjectID(), parentWidget());
	connect(ObjectForm, &ISObjectFormBase::SavedObject, this, &ISListBaseForm::SetSelectObjectAfterUpdate);
	connect(ObjectForm, &ISObjectFormBase::SavedObject, this, &ISListBaseForm::Updated);
	connect(ObjectForm, &ISObjectFormBase::UpdateList, this, &ISListBaseForm::Update);
	connect(ObjectForm, &ISObjectFormBase::Close, this, &ISListBaseForm::ClosingObjectForm);
	ObjectForm->SetParentObjectID(GetParentObjectID());
	emit AddFormFromTab(ObjectForm);
}
//-----------------------------------------------------------------------------
void ISListBaseForm::Edit()
{
	if (CheckIsSystemObject())
	{
		ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.SystemObject.NotEdit"));
		return;
	}

	ISObjectFormBase *ObjectForm = ISCore::CreateObjectForm(ISNamespace::OFT_Edit, MetaTable, GetObjectID(), parentWidget());
	ObjectForm->SetParentObjectID(GetParentObjectID());
	connect(ObjectForm, &ISObjectFormBase::SavedObject, this, &ISListBaseForm::SetSelectObjectAfterUpdate);
	connect(ObjectForm, &ISObjectFormBase::SavedObject, this, &ISListBaseForm::Updated);
	connect(ObjectForm, &ISObjectFormBase::UpdateList, this, &ISListBaseForm::Update);
	connect(ObjectForm, &ISObjectFormBase::Close, this, &ISListBaseForm::ClosingObjectForm);
	emit AddFormFromTab(ObjectForm);
}
//-----------------------------------------------------------------------------
void ISListBaseForm::Update()
{
	SqlModel->Clear();
	ModelThreadQuery->Execute(QueryModel->GetQueryText(), QueryModel->GetConditions());

	if (SETTING_BOOL(CONST_UID_SETTING_TABLES_PAGE_NAVIGATION))
	{
		PageNavigation->SetRowCount(ISDatabaseHelper::GetCountRows(MetaTable->GetName(), MetaTable->GetAlias()));
	}
}
//-----------------------------------------------------------------------------
void ISListBaseForm::Delete()
{
	QVector<int> Vector = GetSelectedIDs();
	if (Vector.count() == 1) //���� ���������� �� �������� ���� ������
	{
		if (CheckIsSystemObject())
		{
			ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.SystemObject.NotDelete"));
			return;
		}

		bool Deleted = GetCurrentRecordValue("IsDeleted").toBool();
		int ObjectID = Vector.at(0);

		if (Deleted) //���� ������ ��������� - ������������
		{
			if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.RecoveryObjectSelected")))
			{
				if (ISCore::DeleteOrRecoveryObject(ISNamespace::DRO_Recovery, MetaTable->GetName(), MetaTable->GetAlias(), ObjectID, MetaTable->GetLocalListName())) //���� �������������� ������ �������, �������� �������
				{
					ISNotificationService::ShowNotification(ISNamespace::NotificationFormType::NFT_Recovery, MetaTable->GetLocalName());
					SqlModel->RemoveRecord(GetCurrentRowIndex());
				}
			}
		}
		else //���� ������ �� ��������� - �������
		{
			if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.DeleteSelectedRecord")))
			{
				if (ISCore::DeleteOrRecoveryObject(ISNamespace::DRO_Delete, MetaTable->GetName(), MetaTable->GetAlias(), ObjectID, MetaTable->GetLocalListName())) //���� �������� ������ �������, �������� �������
				{
					ISNotificationService::ShowNotification(ISNamespace::NotificationFormType::NFT_Delete, MetaTable->GetLocalName());
					SqlModel->RemoveRecord(GetCurrentRowIndex());
				}
			}
		}
	}
	else //���������� �� �������� ��������� �������
	{
		if (CurrentShowDataType == ISNamespace::ShowDataType::SDT_Actual) //�������� �������
		{
			if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Objects.Delete").arg(Vector.count())))
			{
				ISProgressForm ProgressForm(0, Vector.count(), this);
				ProgressForm.show();
				ProgressForm.SetText(LOCALIZATION("DeletingObjects").arg(0).arg(Vector.count()) + "...");

				for (int i = 0; i < Vector.count(); i++)
				{
					ProgressForm.SetText(LOCALIZATION("DeletingObjects").arg(i + 1).arg(Vector.count()) + "...");
					ProgressForm.AddOneValue();

					bool Deleted = ISCore::DeleteOrRecoveryObject(ISNamespace::DRO_Delete, MetaTable->GetName(), MetaTable->GetAlias(), Vector.at(i), MetaTable->GetLocalListName());
					if (ProgressForm.wasCanceled())
					{
						break;
					}
				}

				Update();
			}
		}
		else if (CurrentShowDataType == ISNamespace::ShowDataType::SDT_Deleted) //��������������
		{
			if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Objects.Recovery").arg(Vector.count())))
			{
				ISProgressForm ProgressForm(0, Vector.count(), this);
				ProgressForm.show();
				ProgressForm.SetText(LOCALIZATION("RecoveryObjects").arg(0).arg(Vector.count()) + "...");

				for (int i = 0; i < Vector.count(); i++)
				{
					ProgressForm.SetText(LOCALIZATION("RecoveryObjects").arg(i + 1).arg(Vector.count()) + "...");
					ProgressForm.AddOneValue();

					bool Recovered = ISCore::DeleteOrRecoveryObject(ISNamespace::DRO_Recovery, MetaTable->GetName(), MetaTable->GetAlias(), Vector.at(i), MetaTable->GetLocalListName());
					if (ProgressForm.wasCanceled())
					{
						break;
					}
				}

				Update();
			}
		}
	}
}
//-----------------------------------------------------------------------------
bool ISListBaseForm::DeleteCascade()
{
	QVector<int> Vector = GetSelectedIDs();
	if (Vector.count() == 1) //���� ��������� ���� ������
	{
		if (CheckIsSystemObject())
		{
			ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.SystemObject.NotDelete"));
			return false;
		}

		if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Object.Delete.Cascade"), LOCALIZATION("Message.Object.Delete.Cascade.Help")))
		{
			int ObjectID = Vector.at(0);

			bool Deleted = ISCore::DeleteCascadeObject(MetaTable->GetName(), MetaTable->GetAlias(), ObjectID);
			if (Deleted)
			{
				ISNotificationService::ShowNotification(LOCALIZATION("NotificationForm.Title.Deleted.Cascade").arg(ObjectID));
				ISProtocol::DeleteCascadeObject(MetaTable->GetName(), MetaTable->GetLocalListName(), GetObjectID());
				Update();

				return true;
			}
		}
	}
	else //��������� ��������� �������
	{
		if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Objects.Delete.Cascade").arg(Vector.count()), LOCALIZATION("Message.Object.Delete.Cascade.Help")))
		{
			ISProgressForm ProgressForm(0, Vector.count(), this);
			ProgressForm.show();
			ProgressForm.SetText(LOCALIZATION("DeletingCascadeObjects").arg(0).arg(Vector.count()) + "...");

			for (int i = 0; i < Vector.count(); i++)
			{
				ProgressForm.SetText(LOCALIZATION("DeletingCascadeObjects").arg(i + 1).arg(Vector.count()) + "...");
				ProgressForm.AddOneValue();

				int ObjectID = Vector.at(i);
				bool Deleted = ISCore::DeleteCascadeObject(MetaTable->GetName(), MetaTable->GetAlias(), ObjectID);
				if (Deleted)
				{
					ISProtocol::DeleteCascadeObject(MetaTable->GetName(), MetaTable->GetLocalListName(), GetObjectID());
				}
				else
				{
					ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.NotDeleteCascadeRecord").arg(ObjectID));
					return false;
				}

				if (ProgressForm.wasCanceled())
				{
					break;
				}
			}

			Update();
			return true;
		}
	}

	return false;
}
//-----------------------------------------------------------------------------
void ISListBaseForm::ShowActual()
{
	CurrentShowDataType = ISNamespace::SDT_Actual;

	if (!GetAction(ISNamespace::AT_ShowActual)->isChecked())
	{
		GetAction(ISNamespace::AT_ShowActual)->setChecked(true);
		return;
	}

	QueryModel->SetShowDataType(CurrentShowDataType);
	LabelSwichSelectedType->setText(LOCALIZATION("SwichSelectedType.Label") + ": " + LOCALIZATION("SwichSelectedType.Actual"));

	connect(TableView, &ISBaseTableView::doubleClicked, this, &ISListBaseForm::DoubleClickedTable);
	
	GetAction(ISNamespace::AT_ShowActual)->setChecked(true);
	GetAction(ISNamespace::AT_ShowDeleted)->setChecked(false);
	
	Update();
}
//-----------------------------------------------------------------------------
void ISListBaseForm::ShowDeleted()
{
	CurrentShowDataType = ISNamespace::SDT_Deleted;

	if (!GetAction(ISNamespace::AT_ShowDeleted)->isChecked())
	{
		GetAction(ISNamespace::AT_ShowDeleted)->setChecked(true);
		return;
	}

	QueryModel->SetShowDataType(CurrentShowDataType);
	LabelSwichSelectedType->setText(LOCALIZATION("SwichSelectedType.Label") + ": " + LOCALIZATION("SwichSelectedType.IsDeleted"));
	
	disconnect(TableView, &ISBaseTableView::doubleClicked, this, &ISListBaseForm::DoubleClickedTable);

	GetAction(ISNamespace::AT_ShowActual)->setChecked(false);
	GetAction(ISNamespace::AT_ShowDeleted)->setChecked(true);
	
	Update();
}
//-----------------------------------------------------------------------------
void ISListBaseForm::Search()
{
	ISSystem::SetWaitGlobalCursor(true);

	if (!SearchForm)
	{
		SearchForm = new ISSearchForm(MetaTable, this);
	}

	ISSystem::SetWaitGlobalCursor(false);

	if (SearchForm->Exec())
	{
		if (SearchForm->GetSearchResult() == ISNamespace::SFR_Search)
		{
			QString FieldName = MetaTable->GetAlias() + "_" + SearchForm->GetField().toLower(); //���� ������
			QVariant SearchValue = SearchForm->GetValue(); //�������� ������

			QString SearchFilter;

			switch (SearchForm->GetSearchHow())
			{
			case ISNamespace::SH_BeginString: SearchFilter = FieldName + " LIKE :Condition || '%'"; break; //�� ������ ������
			case ISNamespace::SH_EndString: SearchFilter = FieldName + " LIKE '%' || :Condition"; break; //�� ��������� ������
			case ISNamespace::SH_PartString: SearchFilter = FieldName + " LIKE '%' || :Condition || '%'"; break; //�� ����� ������
			case ISNamespace::SH_ExactMatch: SearchFilter = FieldName + " = :Condition"; break; //�� ������� ����������
			}

			GetQueryModel()->SetSearchFilter(SearchFilter);
			GetQueryModel()->ClearConditions();
			GetQueryModel()->AddCondition(":Condition", SearchValue);

			Update();
			ActionSetEnabled(ISNamespace::AT_SearchClear, true);
		}
		else if (SearchForm->GetSearchResult() == ISNamespace::SFR_ClearResult)
		{
			SearchClear();
		}
	}
}
//-----------------------------------------------------------------------------
void ISListBaseForm::SearchClear()
{
	GetQueryModel()->ClearConditions();
	GetQueryModel()->ClearSearchFilter();
	Update();
	ActionSetEnabled(ISNamespace::AT_SearchClear, false);

	if (SearchForm)
	{
		delete SearchForm;
		SearchForm = nullptr;
	}
}
//-----------------------------------------------------------------------------
void ISListBaseForm::Export()
{
	if (!SqlModel->rowCount())
	{
		ISMessageBox::ShowWarning(this, LOCALIZATION("Export.NoData"));
		return;
	}

	ISExportForm ExportForm(MetaTable);
	if (!ExportForm.Exec())
	{
		return;
	}

	ISExportWorker *ExportWorker = nullptr;
	switch (ExportForm.GetSelectedType())
	{
	case ISNamespace::ET_Excel:
		ExportWorker = new ISExportExcel(this);
		break;

	case ISNamespace::ET_Word:
		ExportWorker = new ISExportWord(this);
		break;
	}

	ExportWorker->SetTableName(MetaTable->GetLocalListName());
	ExportWorker->SetFields(ExportForm.GetSelectedFields());
	ExportWorker->SetModel(SqlModel);
	ExportWorker->SetHeader(ExportForm.GetHeader());
	ExportWorker->SetSelectedRows(GetSelectedRowIndexes());

	ISProgressForm ProgressForm(0, SqlModel->rowCount(), this);
	ProgressForm.show();
	ProgressForm.SetText(LOCALIZATION("Export.Process.Prepare") + "...");
	connect(&ProgressForm, &ISProgressForm::canceled, ExportWorker, &ISExportWorker::Cancel);
	connect(ExportWorker, &ISExportWorker::ExportedRow, &ProgressForm, &ISProgressForm::AddOneValue);
	connect(ExportWorker, &ISExportWorker::Message, &ProgressForm, &ISProgressForm::SetText);

	bool Prepared = ExportWorker->Prepare();
	if (Prepared)
	{
		bool Exported = ExportWorker->Export();
		if (Exported)
		{
			ProgressForm.close();
			ISProtocol::Insert(true, CONST_UID_PROTOCOL_EXPORT_TABLE, MetaTable->GetName(), MetaTable->GetLocalListName(), QVariant());
			ISMessageBox::ShowInformation(this, LOCALIZATION("Export.Completed").arg(ExportForm.GetSelectTypeName()));
		}
		else
		{
			ISMessageBox::ShowWarning(this, LOCALIZATION("Export.Error.Export"), ExportWorker->GetErrorString());
		}
	}
	else
	{
		ISMessageBox::ShowWarning(this, LOCALIZATION("Export.Error.Prepare"), ExportWorker->GetErrorString());
	}

	delete ExportWorker;
	ExportWorker = nullptr;
}
//-----------------------------------------------------------------------------
void ISListBaseForm::Print()
{
	ISSystem::SetWaitGlobalCursor(true);
	ISPrintForm PrintListForm(MetaTable->GetName(), this);
	ISSystem::SetWaitGlobalCursor(false);
	PrintListForm.Exec();

	ISPrintMetaReport *MetaReport = PrintListForm.GetMetaReport();
	if (!MetaReport)
	{
		return;
	}

	ISProtocol::Insert(true, CONST_UID_PROTOCOL_PRINT, MetaTable->GetName(), MetaTable->GetLocalListName(), GetObjectID(), MetaReport->GetLocalName());

	ISSystem::SetWaitGlobalCursor(true);

	ISProcessForm ProcessForm;
	ProcessForm.show();
	ProcessForm.SetText(LOCALIZATION("PrintProcess.Preapre"));
	
	ISPrintingBase *PrintingBase = nullptr;
	bool EditPrintForm = ISUserRoleEntity::GetInstance().CheckPermissionSpecial(CONST_UID_PERMISSION_SPECIAL_EDIT_PRINT_FORM);

	if (MetaReport->GetType() == ISNamespace::RT_Html)
	{
		PrintingBase = new ISPrintingHtml(MetaReport, GetObjectID(), this);
		PrintingBase->setProperty("PDF", PrintListForm.GetPDF());
		PrintingBase->setProperty("PathPDF", QDir::homePath() + "/" + MetaReport->GetLocalName());
		PrintingBase->setProperty("EditPreview", EditPrintForm);
	}
	else if (MetaReport->GetType() == ISNamespace::RT_Word)
	{
		PrintingBase = new ISPrintingWord(MetaReport, GetObjectID(), this);
	}

	PrintingBase->SetReportLocalName(MetaReport->GetLocalName());

	connect(PrintingBase, &ISPrintingBase::SetVisibleDialog, &ProcessForm, &ISProcessForm::setVisible);
	connect(PrintingBase, &ISPrintingBase::Message, &ProcessForm, &ISProcessForm::SetText);

	bool Result = false;

	Result = PrintingBase->Prepare();
	if (Result)
	{
		ProcessForm.SetText(LOCALIZATION("PrintProcess.ReadFileTemplate"));
		Result = PrintingBase->PrepareTempate();
		if (Result)
		{
			ProcessForm.SetText(LOCALIZATION("PrintProcess.FillTemplateData"));

			Result = PrintingBase->FillTemplate(); //���������� ������� �������
			if (Result)
			{
				if (PrintListForm.GetPreview()) //���� ������ ��������������� ��������
				{
					Result = PrintingBase->PreviewDocument();
				}

				if (Result)
				{
					ProcessForm.SetText(LOCALIZATION("PrintProcess.Printing"));
					Result = PrintingBase->Print();
				}
			}
			else
			{
				ISMessageBox::ShowCritical(this, LOCALIZATION("Message.Error.ErrorFillTemplateData"), PrintingBase->GetErrorString());
			}
		}
		else
		{
			ISMessageBox::ShowCritical(this, LOCALIZATION("Message.Error.ErrorOpenedFileTemplatePrinting"), PrintingBase->GetErrorString());
		}
	}
	else
	{

	}

	ISSystem::SetWaitGlobalCursor(false);

	if (PrintingBase)
	{
		delete PrintingBase;
		PrintingBase = nullptr;
	}
}
//-----------------------------------------------------------------------------
void ISListBaseForm::ShowSystemInfo()
{
	ISSystem::SetWaitGlobalCursor(true);
	ISRecordInfoForm Form(MetaTable, GetObjectID());
	ISSystem::SetWaitGlobalCursor(false);
	Form.Exec();
}
//-----------------------------------------------------------------------------
void ISListBaseForm::AttachTask()
{
	QVector<int> Objects = GetSelectedIDs();
	if (!Objects.count())
	{
		ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.NotSelectedRecordsFromAttachToTask"));
		return;
	}

	if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.AttachCardsFromTask").arg(Objects.count())))
	{
		ISSystem::SetWaitGlobalCursor(true);
		ISTasksAttachForm TasksAttachForm(this);
		ISSystem::SetWaitGlobalCursor(false);

		if (TasksAttachForm.Exec())
		{
			int TaskID = TasksAttachForm.GetSelectedTaskID();
			
			for (int i = 0; i < Objects.count(); i++)
			{
				bool Attached = ISTasks::CheckAttached(TaskID, MetaTable->GetName(), Objects.at(i));
				if (!Attached)
				{
					ISTasks::AttachRecord(TaskID, MetaTable->GetName(), Objects.at(i));
				}
			}

			ISMessageBox::ShowInformation(this, LOCALIZATION("Message.Information.AttachedCardsFromTask").arg(Objects.count()).arg(TasksAttachForm.GetSelectedTaskName()));
		}
	}
}
//-----------------------------------------------------------------------------
void ISListBaseForm::ShowFavorites()
{
	ISSystem::SetWaitGlobalCursor(true);

	ISFavoritesForm *FavoritesForm = new ISFavoritesForm(nullptr, MetaTable);
	connect(FavoritesForm, &ISFavoritesForm::AddFormFromTab, this, &ISListBaseForm::AddFormFromTab);
	FavoritesForm->show();

	ISSystem::SetWaitGlobalCursor(false);
}
//-----------------------------------------------------------------------------
void ISListBaseForm::ShowReference()
{

}
//-----------------------------------------------------------------------------
void ISListBaseForm::NavigationSelectBeginRecord()
{
	if (!SqlModel->rowCount())
	{
		return;
	}

	TableView->selectRow(0);
	TableView->verticalScrollBar()->setValue(TableView->verticalScrollBar()->minimum());
}
//-----------------------------------------------------------------------------
void ISListBaseForm::NavigationSelectPreviousRecord()
{
	if (!SqlModel->rowCount() || GetCurrentRowIndex() == NULL)
	{
		return;
	}

	TableView->selectRow(GetCurrentRowIndex() - 1);
}
//-----------------------------------------------------------------------------
void ISListBaseForm::NavigationSelectNextRecord()
{
	if (!SqlModel->rowCount() || GetCurrentRowIndex() == SqlModel->rowCount() - 1)
	{
		return;
	}

	TableView->selectRow(GetCurrentRowIndex() + 1);
}
//-----------------------------------------------------------------------------
void ISListBaseForm::NavigationSelectLastRecord()
{
	if (!SqlModel->rowCount())
	{
		return;
	}

	TableView->selectRow(SqlModel->rowCount() - 1);
	TableView->verticalScrollBar()->setValue(TableView->verticalScrollBar()->maximum());
}
//-----------------------------------------------------------------------------
void ISListBaseForm::NoteObject()
{
	ISSystem::SetWaitGlobalCursor(true);
	ISNoteObjectForm NoteObjectForm(MetaTable->GetName().toLower(), GetObjectID(), this);
	ISSystem::SetWaitGlobalCursor(false);
	NoteObjectForm.Exec();
}
//-----------------------------------------------------------------------------
void ISListBaseForm::AutoFitColumnWidth()
{
	ISSystem::SetWaitGlobalCursor(true);
	TableView->resizeColumnsToContents();
	ISSystem::SetWaitGlobalCursor(false);
}
//-----------------------------------------------------------------------------
void ISListBaseForm::ResetWidthColumn()
{
	if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.ResetWidthColumn")))
	{
		ISSystem::SetWaitGlobalCursor(true);

		for (int i = 0; i < SqlModel->columnCount(); i++)
		{
			TableView->setColumnWidth(i, 100);
		}

		ISSystem::SetWaitGlobalCursor(false);
	}
}
//-----------------------------------------------------------------------------
void ISListBaseForm::CopyRecord()
{
	QString Content;
	QSqlRecord SqlRecord = GetCurrentRecord();

	for (int i = 0; i < SqlModel->columnCount(); i++)
	{
		Content += SqlRecord.value(i).toString() + "\n";
	}

	ISSystem::RemoveLastSymbolFromString(Content);
	QApplication::clipboard()->setText(Content);
}
//-----------------------------------------------------------------------------
void ISListBaseForm::CreateActions()
{
	//������
	QAction *ActionPicking = ISControls::CreateActionPicking(this);
	ActionPicking->setFont(FONT_APPLICATION_BOLD);
	ActionPicking->setVisible(false);
	connect(ActionPicking, &QAction::triggered, this, &ISListBaseForm::Picking);
	Actions.insert(ISNamespace::AT_Picking, ActionPicking);

	//�����
	QAction *ActionSelect = ISControls::CreateActionSelect(this);
	ActionSelect->setFont(FONT_APPLICATION_BOLD);
	ActionSelect->setVisible(false);
	connect(ActionSelect, &QAction::triggered, this, &ISListBaseForm::Select);
	Actions.insert(ISNamespace::AT_Select, ActionSelect);

	//�������
	QAction *ActionCreate = ISControls::CreateActionCreate(this);
	ActionCreate->setData(qvariant_cast<ISNamespace::PermissionsType>(ISNamespace::PT_Create));
	connect(ActionCreate, &QAction::triggered, this, &ISListBaseForm::Create);
	Actions.insert(ISNamespace::AT_Create, ActionCreate);

	//������� �����
	QAction *ActionCreateCopy = ISControls::CreateActionCreateCopy(this);
	ActionCreateCopy->setData(qvariant_cast<ISNamespace::PermissionsType>(ISNamespace::PT_CreateCopy));
	connect(ActionCreateCopy, &QAction::triggered, this, &ISListBaseForm::CreateCopy);
	Actions.insert(ISNamespace::AT_CreateCopy, ActionCreateCopy);

	//��������
	QAction *ActionEdit = ISControls::CreateActionEdit(this);
	ActionEdit->setData(qvariant_cast<ISNamespace::PermissionsType>(ISNamespace::PT_Edit));
	connect(ActionEdit, &QAction::triggered, this, &ISListBaseForm::Edit);
	Actions.insert(ISNamespace::AT_Edit, ActionEdit);

	//�������
	QAction *ActionDelete = ISControls::CreateActionDelete(this);
	ActionDelete->setData(qvariant_cast<ISNamespace::PermissionsType>(ISNamespace::PT_Delete));
	connect(ActionDelete, &QAction::triggered, this, &ISListBaseForm::Delete);
	Actions.insert(ISNamespace::AT_Delete, ActionDelete);

	//������� ��������
	QAction *ActionDeleteCascade = ISControls::CreateActionDeleteCascade(this);
	ActionDeleteCascade->setData(qvariant_cast<ISNamespace::PermissionsType>(ISNamespace::PT_DeleteCascade));
	connect(ActionDeleteCascade, &QAction::triggered, this, &ISListBaseForm::DeleteCascade);
	Actions.insert(ISNamespace::AT_DeleteCascade, ActionDeleteCascade);

	//��������
	QAction *ActionUpdate = ISControls::CreateActionUpdate(this);
	ActionUpdate->setData(qvariant_cast<ISNamespace::PermissionsType>(ISNamespace::PT_UpdateList));
	connect(ActionUpdate, &QAction::triggered, this, &ISListBaseForm::Update);
	Actions.insert(ISNamespace::AT_Update, ActionUpdate);

	//���������� ���������� ������
	QAction *ActionShowActual = ISControls::CreateActionShowActual(this);
	connect(ActionShowActual, &QAction::triggered, this, &ISListBaseForm::ShowActual);
	Actions.insert(ISNamespace::AT_ShowActual, ActionShowActual);

	//���������� ��������� ������
	QAction *ActionShowDeleted = ISControls::CreateActionShowDeleted(this);
	ActionShowDeleted->setData(qvariant_cast<ISNamespace::PermissionsType>(ISNamespace::PT_ShowDeleted));
	connect(ActionShowDeleted, &QAction::triggered, this, &ISListBaseForm::ShowDeleted);
	Actions.insert(ISNamespace::AT_ShowDeleted, ActionShowDeleted);

	//�����
	QAction *ActionSearch = ISControls::CreateActionSearch(this);
	ActionSearch->setData(qvariant_cast<ISNamespace::PermissionsType>(ISNamespace::PT_Search));
	connect(ActionSearch, &QAction::triggered, this, &ISListBaseForm::Search);
	Actions.insert(ISNamespace::AT_Search, ActionSearch);

	//������� ����������� ������
	QAction *ActionSearchClearResult = ISControls::CreateActionSearchClearResults(this);
	ActionSearchClearResult->setEnabled(false);
	connect(ActionSearchClearResult, &QAction::triggered, this, &ISListBaseForm::SearchClear);
	Actions.insert(ISNamespace::AT_SearchClear, ActionSearchClearResult);

	//�������
	QAction *ActionExport = ISControls::CreateActionExport(this);
	ActionExport->setData(qvariant_cast<ISNamespace::PermissionsType>(ISNamespace::PT_Export));
	connect(ActionExport, &QAction::triggered, this, &ISListBaseForm::Export);
	Actions.insert(ISNamespace::AT_Export, ActionExport);

	//������
	QAction *ActionPrint = ISControls::CreateActionPrint(this);
	ActionPrint->setData(qvariant_cast<ISNamespace::PermissionsType>(ISNamespace::PT_Print));
	connect(ActionPrint, &QAction::triggered, this, &ISListBaseForm::Print);
	Actions.insert(ISNamespace::AT_Print, ActionPrint);

	//��������� ����������
	QAction *ActionSystemInformation = ISControls::CreateActionRecordInformartion(this);
	ActionSystemInformation->setData(qvariant_cast<ISNamespace::PermissionsType>(ISNamespace::PT_SystemInformation));
	connect(ActionSystemInformation, &QAction::triggered, this, &ISListBaseForm::ShowSystemInfo);
	Actions.insert(ISNamespace::AT_SystemInfo, ActionSystemInformation);

	//������������ � ������
	QAction *ActionAttachTask = ISControls::CreateActionAttachTask(this);
	ActionAttachTask->setData(qvariant_cast<ISNamespace::PermissionsType>(ISNamespace::PT_AttachTask));
	connect(ActionAttachTask, &QAction::triggered, this, &ISListBaseForm::AttachTask);
	Actions.insert(ISNamespace::AT_AttachTask, ActionAttachTask);

	//���������
	QAction *ActionFavorites = ISControls::CreateActionFavorites(this);
	connect(ActionFavorites, &QAction::triggered, this, &ISListBaseForm::ShowFavorites);
	Actions.insert(ISNamespace::AT_Favorites, ActionFavorites);

	//�������
	QAction *ActionReference = ISControls::CreateActionReference(this);
	connect(ActionReference, &QAction::triggered, this, &ISListBaseForm::ShowReference);
	Actions.insert(ISNamespace::AT_Reference, ActionReference);

	//���������
	{
		//������ ������
		QAction *ActionNavigationBegin = ISControls::CreateActionNavigationBegin(this);
		ActionNavigationBegin->setData(qvariant_cast<ISNamespace::PermissionsType>(ISNamespace::PT_TableNavigation));
		connect(ActionNavigationBegin, &QAction::triggered, this, &ISListBaseForm::NavigationSelectBeginRecord);
		Actions.insert(ISNamespace::AT_NavigationBegin, ActionNavigationBegin);

		//���������� ������
		QAction *ActionNavigationPrevious = ISControls::CreateActionNavigationPrevious(this);
		ActionNavigationPrevious->setData(qvariant_cast<ISNamespace::PermissionsType>(ISNamespace::PT_TableNavigation));
		connect(ActionNavigationPrevious, &QAction::triggered, this, &ISListBaseForm::NavigationSelectPreviousRecord);
		Actions.insert(ISNamespace::AT_NavigationPrevious, ActionNavigationPrevious);

		//��������� ������
		QAction *ActionNavigationNext = ISControls::CreateActionNavigationNext(this);
		ActionNavigationNext->setData(qvariant_cast<ISNamespace::PermissionsType>(ISNamespace::PT_TableNavigation));
		connect(ActionNavigationNext, &QAction::triggered, this, &ISListBaseForm::NavigationSelectNextRecord);
		Actions.insert(ISNamespace::AT_NavigationNext, ActionNavigationNext);

		//��������� ������
		QAction *ActionNavigationLast = ISControls::CreateActionNavigationLast(this);
		ActionNavigationLast->setData(qvariant_cast<ISNamespace::PermissionsType>(ISNamespace::PT_TableNavigation));
		connect(ActionNavigationLast, &QAction::triggered, this, &ISListBaseForm::NavigationSelectLastRecord);
		Actions.insert(ISNamespace::AT_NavigationLast, ActionNavigationLast);
	}
}
//-----------------------------------------------------------------------------
void ISListBaseForm::CreateSpecialActions()
{
	//���������� �� ���������
	QAction *ActionSortDefault = ISControls::CreateActionSortDefault(this);
	connect(ActionSortDefault, &QAction::triggered, this, &ISListBaseForm::SortingDefault);
	ActionsSpecial.insert(ISNamespace::AST_SortDefault, ActionSortDefault);

	//����������
	QAction *ActionNoteObject = ISControls::CreateActionNoteObject(this);
	connect(ActionNoteObject, &QAction::triggered, this, &ISListBaseForm::NoteObject);
	ActionsSpecial.insert(ISNamespace::AST_Note, ActionNoteObject);

	//���������� ������
	QAction *ActionResizeFromContent = new QAction(this);
	ActionResizeFromContent->setText(LOCALIZATION("AutoFitColumnWidth"));
	ActionResizeFromContent->setToolTip(LOCALIZATION("AutoFitColumnWidth"));
	ActionResizeFromContent->setIcon(BUFFER_ICONS("AutoFitColumnWidth"));
	connect(ActionResizeFromContent, &QAction::triggered, this, &ISListBaseForm::AutoFitColumnWidth);
	ActionsSpecial.insert(ISNamespace::AST_ResizeFromContent, ActionResizeFromContent);

	//����� ������ �������
	QAction *ActionResetWidthColumn = new QAction(this);
	ActionResetWidthColumn->setText(LOCALIZATION("ResetWidthColumn"));
	ActionResetWidthColumn->setToolTip(LOCALIZATION("ResetWidthColumn"));
	connect(ActionResetWidthColumn, &QAction::triggered, this, &ISListBaseForm::ResetWidthColumn);
	ActionsSpecial.insert(ISNamespace::AST_ResetWidthColumn, ActionResetWidthColumn);

	//����������� ������ � �����
	QAction *ActionCopyRecord = new QAction(this);
	ActionCopyRecord->setText(LOCALIZATION("CopyRecord"));
	ActionCopyRecord->setToolTip(LOCALIZATION("CopyRecord"));
	connect(ActionCopyRecord, &QAction::triggered, this, &ISListBaseForm::CopyRecord);
	ActionsSpecial.insert(ISNamespace::AST_CopyRecord, ActionCopyRecord);

	ActionObjectGroup->addAction(ActionNoteObject);
}
//-----------------------------------------------------------------------------
void ISListBaseForm::CreateToolBar()
{
	ToolBar = new QToolBar(this);
	ToolBar->setToolButtonStyle(Qt::ToolButtonIconOnly);
	ToolBar->setEnabled(false);
	GetMainLayout()->addWidget(ToolBar);

	ToolBar->addAction(GetAction(ISNamespace::AT_Picking));
	ToolBar->addAction(GetAction(ISNamespace::AT_Select));
	ToolBar->addAction(GetAction(ISNamespace::AT_Create));
	ToolBar->addAction(GetAction(ISNamespace::AT_CreateCopy));
	ToolBar->addAction(GetAction(ISNamespace::AT_Edit));
	ToolBar->addAction(GetAction(ISNamespace::AT_Delete));
	ToolBar->addAction(GetAction(ISNamespace::AT_DeleteCascade));
	ToolBar->addAction(GetAction(ISNamespace::AT_Update));
	ToolBar->addAction(GetAction(ISNamespace::AT_Search));
	ToolBar->addAction(GetAction(ISNamespace::AT_SearchClear));
	ToolBar->addAction(GetAction(ISNamespace::AT_Print));
	ToolBar->addAction(GetAction(ISNamespace::AT_Favorites));

	QAction *ActionAdditionally = new QAction(ToolBar);
	ActionAdditionally->setToolTip(LOCALIZATION("Additionally") + "...");
	ActionAdditionally->setIcon(BUFFER_ICONS("AdditionallyActions"));
	ActionAdditionally->setMenu(new QMenu(ToolBar));
	ToolBar->addAction(ActionAdditionally);

	QToolButton *ButtonAdditionally = dynamic_cast<QToolButton*>(ToolBar->widgetForAction(ActionAdditionally));
	ButtonAdditionally->setPopupMode(QToolButton::InstantPopup);
	ButtonAdditionally->setCursor(CURSOR_POINTING_HAND);
	ButtonAdditionally->setStyleSheet(STYLE_SHEET("QToolButtonMenu"));

	if (SETTING_BOOL(CONST_UID_SETTING_TABLES_SHOWNAVIGATION))
	{
		ToolBar->addAction(GetAction(ISNamespace::AT_NavigationBegin));
		ToolBar->addAction(GetAction(ISNamespace::AT_NavigationPrevious));
		ToolBar->addAction(GetAction(ISNamespace::AT_NavigationNext));
		ToolBar->addAction(GetAction(ISNamespace::AT_NavigationLast));
	}
	
	ActionAdditionally->menu()->addAction(GetAction(ISNamespace::AT_ShowActual));
	ActionAdditionally->menu()->addAction(GetAction(ISNamespace::AT_ShowDeleted));
	ActionAdditionally->menu()->addSeparator();
	ActionAdditionally->menu()->addAction(GetAction(ISNamespace::AT_Export));
	ActionAdditionally->menu()->addAction(GetSpecialAction(ISNamespace::AST_SortDefault));
	ActionAdditionally->menu()->addAction(GetSpecialAction(ISNamespace::AST_ResizeFromContent));
	ActionAdditionally->menu()->addAction(GetSpecialAction(ISNamespace::AST_ResetWidthColumn));
	ActionAdditionally->menu()->addAction(GetSpecialAction(ISNamespace::AST_CopyRecord));

	if (GetAction(ISNamespace::AT_Select)) ActionObjectGroup->addAction(GetAction(ISNamespace::AT_Select));
	if (GetAction(ISNamespace::AT_CreateCopy)) ActionObjectGroup->addAction(GetAction(ISNamespace::AT_CreateCopy));
	if (GetAction(ISNamespace::AT_Edit)) ActionObjectGroup->addAction(GetAction(ISNamespace::AT_Edit));
	if (GetAction(ISNamespace::AT_Delete)) ActionObjectGroup->addAction(GetAction(ISNamespace::AT_Delete));
	if (GetAction(ISNamespace::AT_DeleteCascade)) ActionObjectGroup->addAction(GetAction(ISNamespace::AT_DeleteCascade));
	if (GetAction(ISNamespace::AT_SystemInfo)) ActionObjectGroup->addAction(GetAction(ISNamespace::AT_SystemInfo));
	if (GetAction(ISNamespace::AT_Print)) ActionObjectGroup->addAction(GetAction(ISNamespace::AT_Print));
}
//-----------------------------------------------------------------------------
void ISListBaseForm::CreateTableView()
{
	LayoutTableView = new QHBoxLayout();
	GetMainLayout()->addLayout(LayoutTableView);

	TableView = new ISBaseTableView(this);
	TableView->SetCornerText(LOCALIZATION("Reduction.SerialNumber"));
	TableView->SetCornerToolTip(LOCALIZATION("OrdinalNumber"));
	TableView->horizontalHeader()->setHighlightSections(false);
	connect(TableView->horizontalHeader(), &QHeaderView::sectionResized, this, &ISListBaseForm::HeaderResized);
	connect(TableView, &ISBaseTableView::customContextMenuRequested, this, &ISListBaseForm::ShowContextMenu);
	connect(TableView, &ISBaseTableView::CornerClicked, this, &ISListBaseForm::CornerButtonClicked);
	LayoutTableView->addWidget(TableView);

	connect(TableView, &ISBaseTableView::doubleClicked, this, &ISListBaseForm::DoubleClickedTable);

	if (SETTING_BOOL(CONST_UID_SETTING_TABLES_MINIMIZEHEIGHTROWS))
	{
		TableView->verticalHeader()->setDefaultSectionSize(19);
	}

	QString SelectionBehavior = SETTING_STRING(CONST_UID_SETTING_TABLES_SELECTIONBEHAVIOR);
	if (SelectionBehavior == "SelectItems")
	{
		TableView->setSelectionBehavior(QAbstractItemView::SelectItems);
	}
	else if (SelectionBehavior == "SelectRows")
	{
		TableView->setSelectionBehavior(QAbstractItemView::SelectRows);
	}
	else if (SelectionBehavior == "SelectColumns")
	{
		TableView->setSelectionBehavior(QAbstractItemView::SelectColumns);
	}

	bool AlternativeRowColors = SETTING_BOOL(CONST_UID_SETTING_TABLES_HIGHLIGHTINGODDROWS);
	TableView->setAlternatingRowColors(AlternativeRowColors);

	bool ShowHorizontalHeader = SETTING_BOOL(CONST_UID_SETTING_TABLES_SHOWHORIZONTALHEADER);
	TableView->horizontalHeader()->setVisible(ShowHorizontalHeader);

	bool ShowVerticalHeader = SETTING_BOOL(CONST_UID_SETTING_TABLES_SHOWVERTICALHEADER);
	TableView->SetVisibleVerticalHeader(ShowVerticalHeader);

	QString FontText = SETTING_STRING(CONST_UID_SETTING_FONTS_TABLEVIEW);
	QFont Font = ISSystem::StringToFont(FontText);
	TableView->setFont(Font);
}
//-----------------------------------------------------------------------------
void ISListBaseForm::CreateContextMenu()
{
	ContextMenu = new QMenu(this);
	if (GetAction(ISNamespace::AT_Picking)) ContextMenu->addAction(GetAction(ISNamespace::AT_Picking));
	if (GetAction(ISNamespace::AT_Select)) ContextMenu->addAction(GetAction(ISNamespace::AT_Select));
	if (GetAction(ISNamespace::AT_Create)) ContextMenu->addAction(GetAction(ISNamespace::AT_Create));
	if (GetAction(ISNamespace::AT_CreateCopy)) ContextMenu->addAction(GetAction(ISNamespace::AT_CreateCopy));
	if (GetAction(ISNamespace::AT_Edit)) ContextMenu->addAction(GetAction(ISNamespace::AT_Edit));
	if (GetAction(ISNamespace::AT_Delete)) ContextMenu->addAction(GetAction(ISNamespace::AT_Delete));
	if (GetAction(ISNamespace::AT_DeleteCascade)) ContextMenu->addAction(GetAction(ISNamespace::AT_DeleteCascade));
	if (GetAction(ISNamespace::AT_Update)) ContextMenu->addAction(GetAction(ISNamespace::AT_Update));
	if (GetAction(ISNamespace::AT_SystemInfo)) ContextMenu->addAction(GetAction(ISNamespace::AT_SystemInfo));
	if (GetAction(ISNamespace::AT_AttachTask)) ContextMenu->addAction(GetAction(ISNamespace::AT_AttachTask));

	ContextMenu->addAction(GetSpecialAction(ISNamespace::AST_Note));
}
//-----------------------------------------------------------------------------
void ISListBaseForm::CreateModels()
{
	QString SqlModelName = MetaTable->GetSqlModel();
	if (SqlModelName.length()) //���� � ����-������ ������� ������� ���������������� ������, ��������� �
	{
		int ObjectType = QMetaType::type((SqlModelName + "*").toLocal8Bit().constData());
		const QMetaObject *MetaObject = QMetaType::metaObjectForType(ObjectType);
		SqlModel = dynamic_cast<ISSqlModelCore*>(MetaObject->newInstance(Q_ARG(PMetaClassTable *, MetaTable), Q_ARG(QObject *, TableView)));
	}
	else //������ � ����-������ ������� �� �������, ��������� ����������� (�������)
	{
		SqlModel = new ISSqlModelCore(MetaTable, TableView);
	}

	SqlModel->FillColumns();

	bool ShowToolTip = SETTING_BOOL(CONST_UID_SETTING_TABLES_SHOWTOOLTIP);
	SqlModel->SetShowToolTip(ShowToolTip);

	TableView->setModel(SqlModel);
	connect(TableView->selectionModel(), &QItemSelectionModel::selectionChanged, this, &ISListBaseForm::SelectedRowEvent);//��� ���������� ����������� ������ ���� ����� ������������ ������ � QTableView

	if (IsQueryForm) //���� ����-������� �������� ��������
	{

	}
	else
	{
		QueryModel = new ISQueryModel(MetaTable, ISNamespace::QMT_List, this);
		HideSystemFields();
	}

	ModelThreadQuery = new ISModelThreadQuery(this);
	connect(ModelThreadQuery, &ISModelThreadQuery::Started, this, &ISListBaseForm::ModelThreadStarted);
	connect(ModelThreadQuery, &ISModelThreadQuery::Finished, this, &ISListBaseForm::ModelThreadFinished);
	connect(ModelThreadQuery, &ISModelThreadQuery::ExecutedQuery, this, &ISListBaseForm::ModelThreadLoadingData);
	connect(ModelThreadQuery, &ISModelThreadQuery::Results, SqlModel, &ISSqlModelCore::SetRecords);
	connect(ModelThreadQuery, &ISModelThreadQuery::ErrorConnection, this, &ISListBaseForm::ModelThreadErrorConnection);
	connect(ModelThreadQuery, &ISModelThreadQuery::ErrorQuery, this, &ISListBaseForm::ModelThreadErrorQuery);
	ModelThreadQuery->start(QThread::TimeCriticalPriority); //������ ������
}
//-----------------------------------------------------------------------------
void ISListBaseForm::CreateStatusBar()
{
	StatusBar = new QStatusBar(this);
	StatusBar->setSizeGripEnabled(false);
	GetMainLayout()->addWidget(StatusBar);

	LabelRowCount = new QLabel(StatusBar);
	LabelRowCount->setVisible(SETTING_BOOL(CONST_UID_SETTING_TABLES_SHOWCOUNTRECORD));
	LabelRowCount->setText(LOCALIZATION("RecordsCount") + ": " + LOCALIZATION("Calculated") + "...");
	StatusBar->addWidget(LabelRowCount);

	LabelSwichSelectedType = new QLabel(StatusBar);
	LabelSwichSelectedType->setText(LOCALIZATION("SwichSelectedType.Label") + ": " + LOCALIZATION("SwichSelectedType.Actual"));
	LabelSwichSelectedType->setVisible(!IsQueryForm); //�� ���������� ��� ������� � ����� ������ ���������
	StatusBar->addWidget(LabelSwichSelectedType);

	if (!IsQueryForm)
	{
		if (SETTING_BOOL(CONST_UID_SETTING_TABLES_PAGE_NAVIGATION))
		{
			QueryModel->SetLimit(SETTING_INT(CONST_UID_SETTING_TABLES_PAGE_NAVIGATION_LIMIT));

			PageNavigation = new ISPageNavigation(StatusBar);
			PageNavigation->SetLimit(SETTING_INT(CONST_UID_SETTING_TABLES_PAGE_NAVIGATION_LIMIT));
			connect(PageNavigation, &ISPageNavigation::OffsetSignal, QueryModel, &ISQueryModel::SetOffset);
			connect(PageNavigation, &ISPageNavigation::Update, this, &ISListBaseForm::Update);
			StatusBar->addWidget(PageNavigation);
		}
	}

	QWidget *WidgetSearch = new QWidget(StatusBar);
	WidgetSearch->setLayout(new QHBoxLayout());
	WidgetSearch->layout()->setContentsMargins(LAYOUT_MARGINS_NULL);
	WidgetSearch->layout()->setSpacing(0);
	StatusBar->addPermanentWidget(WidgetSearch);

	EditSearch = new ISLineEdit(WidgetSearch);
	EditSearch->SetPlaceholderText(LOCALIZATION("Search") + "...");
	EditSearch->SetToolTip(LOCALIZATION("EnteringSearchQuery"));
	EditSearch->SetVisibleClearStandart(true);
	EditSearch->SetVisibleClear(false);
	EditSearch->setSizePolicy(QSizePolicy::Maximum, EditSearch->sizePolicy().verticalPolicy());
	EditSearch->SetIcon(BUFFER_ICONS("Search"));
	EditSearch->setSizePolicy(QSizePolicy::Maximum, EditSearch->sizePolicy().verticalPolicy());
	connect(EditSearch, &ISLineEdit::ValueChange, this, &ISListBaseForm::SearchFast);
	WidgetSearch->layout()->addWidget(EditSearch);

	ProgressSearchFast = new QProgressBar(WidgetSearch);
	ProgressSearchFast->setSizePolicy(QSizePolicy::Maximum, ProgressSearchFast->sizePolicy().verticalPolicy());
	ProgressSearchFast->setStyleSheet(STYLE_SHEET("QProgressBar.ListForm.Search"));
	ProgressSearchFast->setMinimumWidth(120);
	ProgressSearchFast->setFormat(QString());
	ProgressSearchFast->setValue(-1);
	WidgetSearch->layout()->addWidget(ProgressSearchFast);
}
//-----------------------------------------------------------------------------
void ISListBaseForm::ShowContextMenu(const QPoint &Point)
{
	ContextMenu->exec(TableView->viewport()->mapToGlobal(Point));
}
//-----------------------------------------------------------------------------
