#include "StdAfx.h"
#include "ISObjectSearchDuplicates.h"
#include "EXDefines.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISMessageBox.h"
#include "ISQueryModel.h"
#include "ISPushButton.h"
//-----------------------------------------------------------------------------
ISObjectSearchDuplicates::ISObjectSearchDuplicates(PMetaClassTable *meta_table, QWidget *parent) : QWidget(parent)
{
	MetaTable = meta_table;
	VisibleFlag = false;

	QVBoxLayout *Layout = new QVBoxLayout();
	Layout->setContentsMargins(LAYOUT_MARGINS_NULL);
	Layout->setSpacing(0);
	setLayout(Layout);

	QHBoxLayout *LayoutGroupBox = new QHBoxLayout();
	LayoutGroupBox->setContentsMargins(LAYOUT_MARGINS_NULL);

	GroupBox = new QGroupBox(this);
	GroupBox->setTitle(LOCALIZATION("SearchObjectForm"));
	GroupBox->setLayout(LayoutGroupBox);
	Layout->addWidget(GroupBox);

	LineEdit = new ISLineEdit(GroupBox);
	LineEdit->SetPlaceholderText(LOCALIZATION("Keyword"));
	LineEdit->SetVisibleClear(false);
	LayoutGroupBox->addWidget(LineEdit);

	QLabel *Label = new QLabel(GroupBox);
	Label->setText(LOCALIZATION("Search.ToField") + ":");
	LayoutGroupBox->addWidget(Label);

	ComboEdit = new ISComboEdit(GroupBox);
	ComboEdit->SetEditable(false);
	ComboEdit->SetVisibleClear(false);
	LayoutGroupBox->addWidget(ComboEdit);

	ISPushButton *ButtonSearch = new ISPushButton(GroupBox);
	ButtonSearch->setText(LOCALIZATION("Search"));
	connect(ButtonSearch, &ISPushButton::clicked, this, &ISObjectSearchDuplicates::CreateQuery);
	LayoutGroupBox->addWidget(ButtonSearch);

	LayoutGroupBox->addStretch();

	for (int i = 0; i < MetaTable->GetFields().count(); i++)
	{
		PMetaClassField *MetaField = MetaTable->GetFields().at(i);
		ISNamespace::FieldType FieldType = MetaField->GetType();

		if (MetaField->GetNotSearch())
		{
			continue;
		}

		if (FieldType == ISNamespace::FT_String || FieldType == ISNamespace::FT_Text)
		{
			ComboEdit->AddItem(MetaField->GetLabelName(), MetaTable->GetAlias() + "_" + MetaField->GetName());
		}
	}

	QHBoxLayout *LayoutButton = new QHBoxLayout();
	LayoutButton->setContentsMargins(LAYOUT_MARGINS_NULL);
	Layout->addLayout(LayoutButton);

	GroupBox->adjustSize();
	Heigth = GroupBox->height();
	GroupBox->setMaximumHeight(0);

	PropertyAnimation = new QPropertyAnimation(GroupBox, "maximumHeight", this);
	PropertyAnimation->setDuration(450);
}
//-----------------------------------------------------------------------------
ISObjectSearchDuplicates::~ISObjectSearchDuplicates()
{

}
//-----------------------------------------------------------------------------
void ISObjectSearchDuplicates::Hide()
{
	if (VisibleFlag)
	{
		Visible();
		
		connect(PropertyAnimation, &QPropertyAnimation::finished, [=]
		{
			hide();
		});
	}
	else
	{
		hide();
	}
}
//-----------------------------------------------------------------------------
void ISObjectSearchDuplicates::Visible()
{
	if (GroupBox->height())
	{
		PropertyAnimation->setStartValue(Heigth);
		PropertyAnimation->setEndValue(0);
		VisibleFlag = false;

		emit Hided();
	}
	else
	{
		PropertyAnimation->setStartValue(0);
		PropertyAnimation->setEndValue(Heigth);
		VisibleFlag = true;
		LineEdit->SetFocus();
	}

	PropertyAnimation->start();
}
//-----------------------------------------------------------------------------
void ISObjectSearchDuplicates::CreateQuery()
{
	if (LineEdit->GetValue().toString().length())
	{
		ISQueryModel Model(MetaTable, ISNamespace::QMT_Object, this);
		Model.SetClassFilter(ComboEdit->GetValue().toString() + " = '" + LineEdit->GetValue().toString() + "'");
		
		emit Search(Model.GetQueryText());
	}
	else
	{
		ISMessageBox::ShowWarning(this, LOCALIZATION("EnterKeywordFromSearch"));
	}
}
//-----------------------------------------------------------------------------
