#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
#include "ISMetaUserData.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISMetaUser : public QObject
{
	Q_OBJECT

public:
	ISMetaUser(const ISMetaUser &) = delete;
	ISMetaUser(ISMetaUser &&) = delete;
	ISMetaUser &operator=(const ISMetaUser &) = delete;
	ISMetaUser &operator=(ISMetaUser &&) = delete;
	~ISMetaUser();

	static ISMetaUser& GetInstance();

	void Initialize(const QString &login);

	ISMetaUserData* GetData();
	bool CheckPassword(const QString &EnteredPassword);
	
	void SetProtocolEnterID(int protocol_enter_id);
	int GetProtocolEnterID() const;

	QUuid GetSessionUID() const;

private:
	ISMetaUser();

	ISMetaUserData *UserData;
	int ProtocolEnterID;
	QUuid Session; //������
};
//-----------------------------------------------------------------------------
#define CURRENT_USER_ID ISMetaUser::GetInstance().GetData()->ID
#define CURRENT_USER_LOGIN ISMetaUser::GetInstance().GetData()->Login
#define CURRENT_USER_FULL_NAME ISMetaUser::GetInstance().GetData()->FullName
//-----------------------------------------------------------------------------
