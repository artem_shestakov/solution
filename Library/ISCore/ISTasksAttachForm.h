#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
#include "ISInterfaceDialogForm.h"
#include "ISListWidget.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISTasksAttachForm : public ISInterfaceDialogForm
{
	Q_OBJECT

public:
	ISTasksAttachForm(QWidget *parent = 0);
	virtual ~ISTasksAttachForm();

	int GetSelectedTaskID() const;
	QString GetSelectedTaskName() const;

protected:
	void LoadTasks();
	void Select();

private:
	ISListWidget *ListWidget;
};
//-----------------------------------------------------------------------------
