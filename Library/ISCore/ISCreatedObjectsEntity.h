#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
#include "ISObjectFormBase.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISCreatedObjectsEntity : public QObject
{
	Q_OBJECT

public:
	ISCreatedObjectsEntity(const ISCreatedObjectsEntity &) = delete;
	ISCreatedObjectsEntity(ISCreatedObjectsEntity &&) = delete;
	ISCreatedObjectsEntity &operator=(const ISCreatedObjectsEntity &) = delete;
	ISCreatedObjectsEntity &operator=(ISCreatedObjectsEntity &&) = delete;
	~ISCreatedObjectsEntity();

	static ISCreatedObjectsEntity& GetInstance();

	void RegisterForm(ISObjectFormBase *ObjectForm); //���������������� ����� �������
	void UnregisterForm(const QString &FormUID); //�������� ����������� ����� �������
	bool CheckExistForms(); //�������� ������������ ����

private:
	ISCreatedObjectsEntity();

	QMap<QString, ISObjectFormBase*> ObjectForms;
};
//-----------------------------------------------------------------------------
