#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
#include "ISNamespace.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISProtocol : public QObject
{
	Q_OBJECT

public:
	ISProtocol();
	virtual ~ISProtocol();

	static int EnterApplication(); //���� � ���������
	static void ExitApplication(); //����� �� ���������

	static void OpenSubSystem(const QString &TableName, const QString &LocalListName); //�������� ����������

	static void CreateObject(const QString &TableName, const QString &LocalListName, const QVariant &ObjectID, const QString &ObjectName); //�������� ������
	static void CreateCopyObject(const QString &TableName, const QString &LocalListName, const QVariant &ObjectID, const QString &ObjectName); //�������� ����� ������
	static void EditObject(const QString &TableName, const QString &LocalListName, const QVariant &ObjectID, const QString &ObjectName); //�������������� ������
	static void ShowObject(const QString &TableName, const QString &LocalListName, const QVariant &ObjectID, const QString &ObjectName); //�������� ������
	
	static void DeleteObject(const QString &TableName, const QString &LocalListName, const QVariant &ObjectID); //�������� ������
	static void RecoveryObject(const QString &TableName, const QString &LocalListName, const QVariant &ObjectID); //�������������� ������
	static void DeleteCascadeObject(const QString &TableName, const QString &LocalListName, const QVariant &ObjectID); //��������� �������� ������

	static void EnterProtocol(int ProtocolID); //������������� ���� � �������
	static void ExitProtocol(); //������������� ����� �� �������

	static int Insert(bool Thread, const QString &ProtocolTypeUID, const QString &TableName, const QString &LocalListName, const QVariant &ObjectID, const QString &Information = QString());
};
//-----------------------------------------------------------------------------
