#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISMetaColumnSizeTable : public QObject
{
	Q_OBJECT

public:
	ISMetaColumnSizeTable(QObject *parent = 0);
	virtual ~ISMetaColumnSizeTable();

	void SetTableName(const QString &table_name);
	QString GetTableName() const;

	void SetFieldSize(const QString &FieldName, int Size);
	int GetFieldSize(const QString &FieldName);

	QMap<QString, int> GetFields();

private:
	QString TableName;
	QMap<QString, int> Fields;
};
//-----------------------------------------------------------------------------
