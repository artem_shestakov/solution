#include "StdAfx.h"
#include "ISTasks.h"
#include "ISQuery.h"
#include "ISSystem.h"
#include "ISTaskShowForm.h"
//-----------------------------------------------------------------------------
static QString QS_TASK_ATTACHED_CARD = PREPARE_QUERY("SELECT COUNT(*) FROM _taskattachedcards WHERE tatc_task = :TaskID AND tatc_tablename = :TableName AND tatc_objectid = :ObjectID");
//-----------------------------------------------------------------------------
static QString QI_TASK_ATTACHED_CARD = PREPARE_QUERY("INSERT INTO _taskattachedcards(tatc_task, tatc_tablename, tatc_objectid) "
													 "VALUES(:TaskID, :TableName, :ObjectID)");
//-----------------------------------------------------------------------------
static QString QD_TASK_ATTACHED_CARD = PREPARE_QUERY("DELETE FROM _taskattachedcards WHERE tatc_task = :TaskID AND tatc_tablename = :TableName AND tatc_objectid = :ObjectID");
//-----------------------------------------------------------------------------
ISTasks::ISTasks(QObject *parent) : QObject(parent)
{

}
//-----------------------------------------------------------------------------
ISTasks::~ISTasks()
{

}
//-----------------------------------------------------------------------------
bool ISTasks::CheckAttached(int TaskID, const QString &TableName, int ObjectID)
{
	ISQuery qSelect(QS_TASK_ATTACHED_CARD);
	qSelect.BindValue(":TaskID", TaskID);
	qSelect.BindValue(":TableName", TableName);
	qSelect.BindValue(":ObjectID", ObjectID);
	if (qSelect.ExecuteFirst())
	{
		int Count = qSelect.ReadColumn("count").toInt();
		if (Count)
		{
			return true;
		}
	}

	return false;
}
//-----------------------------------------------------------------------------
bool ISTasks::AttachRecord(int TaskID, const QString &TableName, int ObjectID)
{
	ISQuery qInsert(QI_TASK_ATTACHED_CARD);
	qInsert.BindValue(":TaskID", TaskID);
	qInsert.BindValue(":TableName", TableName);
	qInsert.BindValue(":ObjectID", ObjectID);
	return qInsert.Execute();
}
//-----------------------------------------------------------------------------
bool ISTasks::DetachRecord(int TaskID, const QString &TableName, int ObjectID)
{
	ISQuery qDelete(QD_TASK_ATTACHED_CARD);
	qDelete.BindValue(":TaskID", TaskID);
	qDelete.BindValue(":TableName", TableName);
	qDelete.BindValue(":ObjectID", ObjectID);
	return qDelete.Execute();
}
//-----------------------------------------------------------------------------
void ISTasks::ShowTaskForm(int TaskID, QWidget *parent)
{
	ISSystem::SetWaitGlobalCursor(true);

	ISTaskShowForm *TaskShowForm = new ISTaskShowForm(TaskID, parent);
	TaskShowForm->show();

	ISSystem::SetWaitGlobalCursor(false);
}
//-----------------------------------------------------------------------------
