#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
#include "ISLineEdit.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISSystemInfoObject : public QWidget
{
	Q_OBJECT

public:
	ISSystemInfoObject(QWidget *parent = 0);
	virtual ~ISSystemInfoObject();

	void SetInfo(int ID, const QString &CreationUser = QString(), const QString &UpdationUser = QString());

private:
	ISLineEdit *EditID;
	ISLineEdit *EditCreationUser;
	ISLineEdit *EditUpdationUser;
};
//-----------------------------------------------------------------------------
