#include "StdAfx.h"
#include "ISTabBarMain.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "EXDefines.h"
#include "ISSystem.h"
#include "ISSettings.h"
#include "EXConstants.h"
//-----------------------------------------------------------------------------
ISTabBarMain::ISTabBarMain(QWidget *parent) : QTabBar(parent)
{
	setObjectName(metaObject()->className());

	MouseClick = false;
	MouseRightClickTabIndex = 0;

	setCursor(CURSOR_POINTING_HAND);
	
	CreateContextMenu();
}
//-----------------------------------------------------------------------------
ISTabBarMain::~ISTabBarMain()
{

}
//-----------------------------------------------------------------------------
void ISTabBarMain::AddFixedTab(const QString &TabUID)
{
	FixedTabs.append(TabUID);
}
//-----------------------------------------------------------------------------
void ISTabBarMain::RemoveFixedTab(const QString &TabUID)
{
	FixedTabs.removeOne(TabUID);
}
//-----------------------------------------------------------------------------
bool ISTabBarMain::CheckFixedTab(const QString &TabUID)
{
	return FixedTabs.contains(TabUID);
}
//-----------------------------------------------------------------------------
void ISTabBarMain::mousePressEvent(QMouseEvent *e)
{
	QTabBar::mousePressEvent(e);

	MouseRightClickTabIndex = tabAt(e->pos());

	if (e->button() == Qt::LeftButton)
	{
		PressPos = e->localPos();
		MouseClick = true;
	}
	else if (e->button() == Qt::MidButton)
	{
		QTabBar::mousePressEvent(new QMouseEvent(QEvent::Type::MouseButtonPress, e->localPos(), Qt::MouseButton::LeftButton, e->buttons(), e->modifiers()));
		emit MidButtonClicked(currentIndex());
	}
	else if (e->button() == Qt::RightButton) //����� ������������ ����
	{
		SetVisibleContextActions(true);

		if (MouseRightClickTabIndex) //����������� ���� ���������� ��� ������� ��������
		{
			if (FixedTabs.contains(tabData(MouseRightClickTabIndex).toString()))
			{
				ActionFixedTab->setText(LOCALIZATION("UnfixedTab"));
				ActionFixedTab->setIcon(BUFFER_ICONS("Tab.Unfixed"));
			}
			else
			{
				ActionFixedTab->setText(LOCALIZATION("FixedTab"));
				ActionFixedTab->setIcon(BUFFER_ICONS("Tab.Fixed"));
			}
		}
		else //����������� ���� ���������� ��� ������� �������
		{
			ActionCloseTab->setVisible(false);
			ActionCloseRightTabs->setVisible(false);
			ActionSeparateWindow->setVisible(false);
			ActionFixedTab->setVisible(false);
		}

		ContextMenu->exec(e->globalPos());
	}
}
//-----------------------------------------------------------------------------
void ISTabBarMain::mouseMoveEvent(QMouseEvent *e)
{
	if (MouseClick)
	{
		if (FixedTabs.contains(tabData(MouseRightClickTabIndex).toString())) //���� ������� �������� ������������
		{
			return;
		}

		if (currentIndex() >= 1)
		{
			if (currentIndex() == 1)
			{
				QPointF ClickPos = e->localPos();
				if (ClickPos.x() < PressPos.x())
				{
					return;
				}
				else
				{
					QTabBar::mouseMoveEvent(e);
				}
			}
			else
			{
				QTabBar::mouseMoveEvent(e);
			}
		}
	}
}
//-----------------------------------------------------------------------------
void ISTabBarMain::mouseReleaseEvent(QMouseEvent *e)
{
	QTabBar::mouseReleaseEvent(e);

	MouseClick = false;
	PressPos.setX(0);
	PressPos.setY(0);
}
//-----------------------------------------------------------------------------
void ISTabBarMain::wheelEvent(QWheelEvent *e)
{
	bool WheelScrollingTabs = SETTING_BOOL(CONST_UID_SETTING_TABS_WHEELSCROLLINGTABS);
	if (WheelScrollingTabs)
	{
		QTabBar::wheelEvent(e);
	}
}
//-----------------------------------------------------------------------------
void ISTabBarMain::CreateContextMenu()
{
	ContextMenu = new QMenu(this);
	
	ActionCloseTab = new QAction(ContextMenu);
	ActionCloseTab->setText(LOCALIZATION("CloseTab"));
	ActionCloseTab->setIcon(BUFFER_ICONS("Tab.Close"));
	ContextMenu->addAction(ActionCloseTab);
	connect(ActionCloseTab, &QAction::triggered, this, [=] 
	{ 
		emit MidButtonClicked(MouseRightClickTabIndex);
		MouseRightClickTabIndex = 0;
	});

	ActionCloseRightTabs = new QAction(ContextMenu);
	ActionCloseRightTabs->setText(LOCALIZATION("CloseRightTabs"));
	ActionCloseRightTabs->setIcon(BUFFER_ICONS("Tab.CloseRightTabs"));
	connect(ActionCloseRightTabs, &QAction::triggered, this, &ISTabBarMain::CloseRightTabs);
	ContextMenu->addAction(ActionCloseRightTabs);

	ActionCloseAllTabs = new QAction(ContextMenu);
	ActionCloseAllTabs->setText(LOCALIZATION("CloseAllTabs"));
	ActionCloseAllTabs->setIcon(BUFFER_ICONS("Tab.CloseAllTabs"));
	connect(ActionCloseAllTabs, &QAction::triggered, this, &ISTabBarMain::CloseAllTabs);
	ContextMenu->addAction(ActionCloseAllTabs);

	ContextMenu->addSeparator();

	ActionDuplicate = new QAction(ContextMenu);
	ActionDuplicate->setText(LOCALIZATION("DuplicateTab"));
	ActionDuplicate->setIcon(BUFFER_ICONS("Tab.Duplicate"));
	ContextMenu->addAction(ActionDuplicate);
	connect(ActionDuplicate, &QAction::triggered, [=]
	{
		emit DuplicateWindow(MouseRightClickTabIndex);
		MouseRightClickTabIndex = 0;
	});

	ActionSeparateWindow = new QAction(ContextMenu);
	ActionSeparateWindow->setText(LOCALIZATION("InSeparateWindow"));
	ActionSeparateWindow->setIcon(BUFFER_ICONS("Tab.Separated"));
	ContextMenu->addAction(ActionSeparateWindow);
	connect(ActionSeparateWindow, &QAction::triggered, [=]
	{
		emit SeparateWindow(MouseRightClickTabIndex);
		MouseRightClickTabIndex = 0;
	});

	ActionFixedTab = new QAction(ContextMenu);
	ActionFixedTab->setText(LOCALIZATION("FixedTab"));
	ActionFixedTab->setIcon(BUFFER_ICONS("Tab.Fixed"));
	ContextMenu->addAction(ActionFixedTab);
	connect(ActionFixedTab, &QAction::triggered, [=]
	{
		emit FixedTab(MouseRightClickTabIndex);
		MouseRightClickTabIndex = 0;
	});
}
//-----------------------------------------------------------------------------
void ISTabBarMain::SetVisibleContextActions(bool Visible)
{
	for (int i = 0; i < ContextMenu->actions().count(); i++)
	{
		ContextMenu->actions().at(i)->setVisible(Visible);
	}
}
//-----------------------------------------------------------------------------
void ISTabBarMain::CloseAllTabs()
{
	for (int i = count() - 1; i > 0; --i)
	{
		emit MidButtonClicked(i);
	}

	MouseRightClickTabIndex = 0;
}
//-----------------------------------------------------------------------------
void ISTabBarMain::CloseRightTabs()
{
	for (int i = count() - 1; i > MouseRightClickTabIndex; --i)
	{
		emit MidButtonClicked(i);
	}

	MouseRightClickTabIndex = 0;
}
//-----------------------------------------------------------------------------
