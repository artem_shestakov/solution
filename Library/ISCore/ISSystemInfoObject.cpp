#include "StdAfx.h"
#include "ISSystemInfoObject.h"
#include "EXDefines.h"
#include "ISLocalization.h"
//-----------------------------------------------------------------------------
ISSystemInfoObject::ISSystemInfoObject(QWidget *parent) : QWidget(parent)
{
	QHBoxLayout *Layout = new QHBoxLayout();
	Layout->setContentsMargins(LAYOUT_MARGINS_NULL);
	setLayout(Layout);

	EditID = new ISLineEdit(this);
	EditID->setToolTip(LOCALIZATION("AutoFillField"));
	EditID->SetCursor(CURSOR_WHATS_THIS);
	EditID->setEnabled(false);
	EditID->SetVisibleClear(false);
	Layout->addWidget(EditID);

	QLabel *LabelCreationUser = new QLabel(this);
	LabelCreationUser->setText(LOCALIZATION("SystemField.CreationUser") + ":");
	LabelCreationUser->setFont(FONT_APPLICATION_BOLD);
	Layout->addWidget(LabelCreationUser);

	EditCreationUser = new ISLineEdit(this);
	EditCreationUser->setToolTip(LOCALIZATION("AutoFillField"));
	EditCreationUser->SetCursor(CURSOR_WHATS_THIS);
	EditCreationUser->SetReadOnly(true);
	EditCreationUser->SetVisibleClear(false);
	Layout->addWidget(EditCreationUser);

	QLabel *LabelUpdationUser = new QLabel(this);
	LabelUpdationUser->setText(LOCALIZATION("SystemField.UpdationUser") + ":");
	LabelUpdationUser->setFont(FONT_APPLICATION_BOLD);
	Layout->addWidget(LabelUpdationUser);

	EditUpdationUser = new ISLineEdit(this);
	EditUpdationUser->setToolTip(LOCALIZATION("AutoFillField"));
	EditUpdationUser->SetCursor(CURSOR_WHATS_THIS);
	EditUpdationUser->SetReadOnly(true);
	EditUpdationUser->SetVisibleClear(false);
	Layout->addWidget(EditUpdationUser);

	Layout->addStretch();
}
//-----------------------------------------------------------------------------
ISSystemInfoObject::~ISSystemInfoObject()
{

}
//-----------------------------------------------------------------------------
void ISSystemInfoObject::SetInfo(int ID, const QString &CreationUser, const QString &UpdationUser)
{
	EditID->SetValue(ID);

	if (CreationUser.length())
	{
		EditCreationUser->SetValue(CreationUser);
	}

	if (UpdationUser.length())
	{
		EditUpdationUser->SetValue(UpdationUser);
	}
}
//-----------------------------------------------------------------------------
