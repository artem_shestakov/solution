#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
#include "ISSelectBaseListForm.h"
//-----------------------------------------------------------------------------
//!����� ������ ������ ����� ������
class ISCORE_EXPORT ISSelectObjectListForm : public ISSelectBaseListForm
{
	Q_OBJECT

signals:
	void Selected(int ObjectID);

public:
	ISSelectObjectListForm(PMetaClassTable *MetaTable, QWidget *parent = 0);
	virtual ~ISSelectObjectListForm();

protected:
	void Select() override;
};
//-----------------------------------------------------------------------------
