#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
#include "PMetaUserPermission.h"
#include "ISNamespace.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISUserRoleEntity : public QObject
{
	Q_OBJECT

public:
	ISUserRoleEntity(const ISUserRoleEntity &) = delete;
	ISUserRoleEntity(ISUserRoleEntity &&) = delete;
	ISUserRoleEntity &operator=(const ISUserRoleEntity &) = delete;
	ISUserRoleEntity &operator=(ISUserRoleEntity &&) = delete;
	~ISUserRoleEntity();

	static ISUserRoleEntity& GetInstance();
	
	void InitializeAccess();
	void InitializeAccessSpecial();

	bool CheckPermission(const QString &UID, ISNamespace::PermissionsType Type);
	bool CheckPermissionSpecial(const QString &PermissionSpecialUID);
	bool CheckAllSubSystemsForSystem(const QString &SystemUID);
	int GetCountPermissions();

private:
	ISUserRoleEntity();

	QVector<PMetaUserPermission*> Permissions;
	QVector<QString> PermissionsSpecial;
};
//-----------------------------------------------------------------------------
