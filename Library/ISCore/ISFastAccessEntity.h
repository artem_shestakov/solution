#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
#include "ISMetaExternalTool.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISFastAccessEntity : public QObject
{
	Q_OBJECT

public:
	ISFastAccessEntity(const ISFastAccessEntity &) = delete;
	ISFastAccessEntity(ISFastAccessEntity &&) = delete;
	ISFastAccessEntity &operator=(const ISFastAccessEntity &) = delete;
	ISFastAccessEntity &operator=(ISFastAccessEntity &&) = delete;
	~ISFastAccessEntity();

	static ISFastAccessEntity& GetInstance();

	void LoadExternalTools(); //��������� ������� �����������
	void ReloadExternalTools(); //������������� ������� �����������
	QVector<ISMetaExternalTool*> GetExternalTools();

	void LoadCreateRecords();
	void ReloadCreateRecords();
	QVector<QString> GetCreateRecords();

private:
	ISFastAccessEntity();

	QVector<ISMetaExternalTool*> ExternalTools;
	QVector<QString> CreateRecords;
};
//-----------------------------------------------------------------------------
