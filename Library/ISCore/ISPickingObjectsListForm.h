#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
#include "ISSelectBaseListForm.h"
//-----------------------------------------------------------------------------
//!����� ������ �������������� ������ �������
class ISCORE_EXPORT ISPickingObjectsListForm : public ISSelectBaseListForm
{
	Q_OBJECT

signals:
	void Matched(const QVector<int> &Vector);

public:
	ISPickingObjectsListForm(PMetaClassTable *MetaTable, QWidget *parent = 0);
	virtual ~ISPickingObjectsListForm();

protected:
	void Picking() override;
};
//-----------------------------------------------------------------------------
