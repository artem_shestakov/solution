#include "StdAfx.h"
#include "ISListWidgetItemFavorite.h"
#include "EXDefines.h"
//-----------------------------------------------------------------------------
ISListWidgetItemFavorite::ISListWidgetItemFavorite(QListWidget *ListWidget) : QListWidgetItem(ListWidget)
{
	ObjectID = 0;
	FavoriteID = 0;
}
//-----------------------------------------------------------------------------
ISListWidgetItemFavorite::~ISListWidgetItemFavorite()
{

}
//-----------------------------------------------------------------------------
void ISListWidgetItemFavorite::SetTableName(const QString &table_name)
{
	TableName = table_name;
}
//-----------------------------------------------------------------------------
QString ISListWidgetItemFavorite::GetTableName() const
{
	return TableName;
}
//-----------------------------------------------------------------------------
void ISListWidgetItemFavorite::SetObjectID(int object_id)
{
	ObjectID = object_id;
}
//-----------------------------------------------------------------------------
int ISListWidgetItemFavorite::GetObjectID() const
{
	return ObjectID;
}
//-----------------------------------------------------------------------------
void ISListWidgetItemFavorite::SetFavoriteID(int favorite_id)
{
	FavoriteID = favorite_id;
}
//-----------------------------------------------------------------------------
int ISListWidgetItemFavorite::GetFavoriteID() const
{
	return FavoriteID;
}
//-----------------------------------------------------------------------------
