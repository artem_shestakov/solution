#include "StdAfx.h"
#include "ISProgressForm.h"
#include "ISPushButton.h"
#include "ISBuffer.h"
#include "ISLocalization.h"
#include "ISControls.h"
#include "ISSystem.h"
#include "EXDefines.h"
//-----------------------------------------------------------------------------
ISProgressForm::ISProgressForm(int Minimum, int Maximum, QWidget *parent) : QProgressDialog(parent)
{
	setWindowTitle(LOCALIZATION("PleaseWait"));
	setWindowModality(Qt::WindowModal);
	setFixedSize(SIZE_PROGRESS_FORM);
	setMinimum(Minimum);
	setMaximum(Maximum);

	ISControls::SetBackgroundColorWidget(this, COLOR_WHITE);

	LabelStatus = new QLabel(this);
	LabelStatus->setAlignment(Qt::AlignLeft);
	setLabel(LabelStatus);

	QMargins Margins = LabelStatus->contentsMargins();
	Margins.setTop(10);
	LabelStatus->setContentsMargins(Margins);
	
	ISPushButton *ButtonClose = new ISPushButton(this);
	ButtonClose->setText(LOCALIZATION("Close"));
	ButtonClose->setIcon(BUFFER_ICONS("Close"));
	setCancelButton(dynamic_cast<QPushButton*>(ButtonClose));
}
//-----------------------------------------------------------------------------
ISProgressForm::~ISProgressForm()
{

}
//-----------------------------------------------------------------------------
void ISProgressForm::SetText(const QString &Text)
{
	setLabelText(Text);
	ISSystem::RepaintWidget(LabelStatus);
	qApp->processEvents();
}
//-----------------------------------------------------------------------------
void ISProgressForm::AddOneValue()
{
	int Value = value();
	Value++;
	setValue(Value);
}
//-----------------------------------------------------------------------------
void ISProgressForm::showEvent(QShowEvent *e)
{
	QProgressDialog::showEvent(e);
	ISSystem::RepaintWidget(this);
	qApp->processEvents();
}
//-----------------------------------------------------------------------------
