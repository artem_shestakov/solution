#include "StdAfx.h"
#include "ISMetaSystemsEntity.h"
#include "ISQuery.h"
#include "ISAssert.h"
#include "ISBuffer.h"
#include "EXDefines.h"
#include "ISSystem.h"
#include "ISCountingTime.h"
#include "ISDebug.h"
#include "ISPlugin.h"
//-----------------------------------------------------------------------------
static QString QS_SYSTEMS = PREPARE_QUERY("SELECT stms_issystem, stms_id, stms_uid, stms_localname, stms_orderid, stms_icon, stms_hint "
										  "FROM _systems "
										  "WHERE NOT stms_isdeleted "
										  "ORDER BY stms_orderid");
//-----------------------------------------------------------------------------
static QString QS_SUB_SYSTEMS = PREPARE_QUERY("SELECT sbsm_id, sbsm_uid, sbsm_localname, sbsm_orderid, sbsm_icon, sbsm_classname, sbsm_tablename FROM _subsystems WHERE sbsm_system = :SystemID AND NOT sbsm_isdeleted ORDER BY sbsm_orderid");
//-----------------------------------------------------------------------------
ISMetaSystemsEntity::ISMetaSystemsEntity()
{
	Initialize();
}
//-----------------------------------------------------------------------------
ISMetaSystemsEntity::~ISMetaSystemsEntity()
{

}
//-----------------------------------------------------------------------------
ISMetaSystemsEntity& ISMetaSystemsEntity::GetInstance()
{
	static ISMetaSystemsEntity MetaSystemsEntity;
	return MetaSystemsEntity;
}
//-----------------------------------------------------------------------------
void ISMetaSystemsEntity::AppendSystem(ISMetaSystem *MetaSystem)
{
	Systems.append(MetaSystem);
}
//-----------------------------------------------------------------------------
QVector<ISMetaSystem*> ISMetaSystemsEntity::GetSystems()
{
	return Systems;
}
//-----------------------------------------------------------------------------
QVector<ISMetaSubSystem*> ISMetaSystemsEntity::GetAllSubSystems()
{
	return SubSystems;
}
//-----------------------------------------------------------------------------
ISMetaSystem* ISMetaSystemsEntity::GetSystem(const QString &SystemUID)
{
	IS_ASSERT(SystemUID.length(), "Not SystemUID");

	int CountSystems = GetCountSystems();
	for (int i = 0; i < CountSystems; i++)
	{
		ISMetaSystem *MetaSystem = GetSystems().at(i);
		if (MetaSystem->GetUID() == SystemUID)
		{
			return MetaSystem;
		}
	}

	IS_ASSERT(false, "Not found system to SystemUID: " + SystemUID);
	return nullptr;
}
//-----------------------------------------------------------------------------
ISMetaSubSystem* ISMetaSystemsEntity::GetSubSystem(const QString &SubSystemUID)
{
	IS_ASSERT(SubSystemUID.length(), "Not SubSystemUID");

	for (int i = 0; i < SubSystems.count(); i++)
	{
		ISMetaSubSystem *MetaSubSystem = SubSystems.at(i);
		if (MetaSubSystem->GetUID() == SubSystemUID)
		{
			return MetaSubSystem;
		}
	}

	IS_ASSERT(false, "Not found system to SubSystemUID: " + SubSystemUID);
	return nullptr;
}
//-----------------------------------------------------------------------------
int ISMetaSystemsEntity::GetCountSystems()
{
	return Systems.count();
}
//-----------------------------------------------------------------------------
void ISMetaSystemsEntity::Initialize()
{
	ISCountingTime Time;

	ISQuery qSelectSystem(QS_SYSTEMS);
	qSelectSystem.Execute();
	while (qSelectSystem.Next())
	{
		//�������� �������
		bool IsSystem = qSelectSystem.ReadColumn("stms_issystem").toBool();
		int SystemID = qSelectSystem.ReadColumn("stms_id").toInt();
		ISUuid SystemUID = qSelectSystem.ReadColumn("stms_uid");
		QString SystemLocalName = qSelectSystem.ReadColumn("stms_localname").toString();
		int SystemOrderID = qSelectSystem.ReadColumn("stms_orderid").toInt();
		QString SystemIconName = qSelectSystem.ReadColumn("stms_icon").toString();
		QString SystemHint = qSelectSystem.ReadColumn("stms_hint").toString();

		//�������� ����������� �������
		ISMetaSystem *MetaSystem = new ISMetaSystem(this);
		MetaSystem->SetIsSystem(IsSystem);
		MetaSystem->SetID(SystemID);
		MetaSystem->SetUID(SystemUID);
		MetaSystem->SetLocalName(SystemLocalName);
		MetaSystem->SetOrderID(SystemOrderID);
		MetaSystem->SetIconName(SystemIconName);
		MetaSystem->SetHint(SystemHint);

		//������ � �����������
		ISQuery qSelectSubSystem(QS_SUB_SYSTEMS);
		qSelectSubSystem.BindValue(":SystemID", SystemUID);
		qSelectSubSystem.Execute();
		while (qSelectSubSystem.Next())
		{
			//�������� ����������
			int SubSystemID = qSelectSubSystem.ReadColumn("sbsm_id").toInt();
			ISUuid SubSystemUID = qSelectSubSystem.ReadColumn("sbsm_uid");
			QString SubSystemLocalName = qSelectSubSystem.ReadColumn("sbsm_localname").toString();
			int SubSystemOrderID = qSelectSubSystem.ReadColumn("sbsm_orderid").toInt();
			QString SubSystemIconName = qSelectSubSystem.ReadColumn("sbsm_icon").toString();
			QString SubSystemClassName = qSelectSubSystem.ReadColumn("sbsm_classname").toString();
			QString SubSystemTableName = qSelectSubSystem.ReadColumn("sbsm_tablename").toString();

			//�������� ����������� ����������
			ISMetaSubSystem *MetaSubSystem = new ISMetaSubSystem(MetaSystem);
			MetaSubSystem->SetID(SubSystemID);
			MetaSubSystem->SetUID(SubSystemUID);
			MetaSubSystem->SetLocalName(SubSystemLocalName);
			MetaSubSystem->SetOrderID(SubSystemOrderID);
			MetaSubSystem->SetIconName(SubSystemIconName);
			MetaSubSystem->SetClassName(SubSystemClassName);
			MetaSubSystem->SetTableName(SubSystemTableName);
			MetaSubSystem->SetSystemUID(SystemUID);
			MetaSubSystem->SetSystemID(SystemID);
			MetaSubSystem->SetSystemLocalName(SystemLocalName);

			MetaSystem->AppendSubSystem(MetaSubSystem);
			SubSystems.append(MetaSubSystem);
		}

		Systems.append(MetaSystem);
	}

	QString TimeInitialized = ISSystem::MillisecondsToString(Time.GetElapsed());
	ISDebug::ShowDebugString("Initialize MetaSystems time: " + TimeInitialized);
}
//-----------------------------------------------------------------------------
QIcon ISMetaSystemsEntity::GetSystemIcon(ISMetaSystem *MetaSystem) const
{
	QIcon IconSystem;

	if (MetaSystem->GetIsSystem()) //������� �������� ���������
	{
		IconSystem = BUFFER_ICONS(MetaSystem->GetIconName());
	}
	else //������� �������� ���������������� (�.�. �� ������������)
	{
		IconSystem = ISPlugin::GetInstance().GetPluginInterface()->GetIcon(MetaSystem->GetIconName());
	}

	return IconSystem;
}
//-----------------------------------------------------------------------------
QIcon ISMetaSystemsEntity::GetSubSystemIcon(ISMetaSubSystem *MetaSubSystem) const
{
	QIcon IconSubSystem;

	if (ISMetaSystemsEntity::GetInstance().GetSystem(MetaSubSystem->GetSystemUID())->GetIsSystem())
	{
		IconSubSystem = BUFFER_ICONS(MetaSubSystem->GetIconName());
	}
	else
	{
		IconSubSystem = ISPlugin::GetInstance().GetPluginInterface()->GetIcon(MetaSubSystem->GetIconName());
	}

	return IconSubSystem;
}
//-----------------------------------------------------------------------------
QString ISMetaSystemsEntity::GetSystemToolTip(ISMetaSystem *MetaSystem) const
{
	QString ToolTip;

	if (MetaSystem->GetHint().length())
	{
		ToolTip = MetaSystem->GetHint();
	}
	else
	{
		ToolTip = MetaSystem->GetLocalName();
	}

	return ToolTip;
}
//-----------------------------------------------------------------------------
QString ISMetaSystemsEntity::GetSubSystemToolTip(ISMetaSubSystem *MetaSubSystem) const
{
	QString ToolTip;

	if (MetaSubSystem->GetHint().length())
	{
		ToolTip = MetaSubSystem->GetHint();
	}
	else
	{
		ToolTip = MetaSubSystem->GetLocalName();
	}

	return ToolTip;
}
//-----------------------------------------------------------------------------
