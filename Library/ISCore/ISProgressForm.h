#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISProgressForm : public QProgressDialog
{
	Q_OBJECT

public:
	ISProgressForm(int Minimum, int Maximum, QWidget *parent);
	virtual ~ISProgressForm();

	void SetText(const QString &Text); //�������� ����� �� �����
	void AddOneValue(); //��������� �������� �� ���� �������

protected:
	void showEvent(QShowEvent *e);

private:
	QLabel *LabelStatus;
};
//-----------------------------------------------------------------------------
