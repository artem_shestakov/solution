#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
#include "ISInterfaceMetaForm.h"
#include "PMetaClassTable.h"
#include "ISQueryModel.h"
#include "ISSqlModelCore.h"
#include "ISModelThreadQuery.h"
#include "ISBaseTableView.h"
#include "ISListIndicatorWidget.h"
#include "ISSearchForm.h"
#include "ISLineEdit.h"
#include "ISPageNavigation.h"
//-----------------------------------------------------------------------------
//!������� ����� ����� ������
class ISCORE_EXPORT ISListBaseForm : public ISInterfaceMetaForm
{
	Q_OBJECT

signals:
	void Updated(); //������ ���������� ����� ���������� �������� ������ � �������
	void DoubleClicked(const QModelIndex &ModelIndex); //������ � ������� ������� �� ������ � �������

public:
	ISListBaseForm(PMetaClassTable *meta_table, QWidget *parent = 0);
	virtual ~ISListBaseForm();

	int GetCurrentRowIndex(); //�������� ������ ������� ������
	int GetObjectID(); //�������� ������������� �������� �������
	int GetObjectID(int RowIndex); //�������� ������������� ������� �� ������� ������
	int GetRowIndex(int object_id); //�������� ������ ������ �� �������������� �������
	QSqlRecord GetCurrentRecord(); //�������� ������� ������
	QVariant GetCurrentRecordValue(const QString &FieldName); //�������� �������� �� ������� �� ������� ������
	QVariant GetCurrentRecordValueDB(const QString &FieldName); //�������� �������� �� ������� �� ������� ������ (�������� ����� ����� �� ����)
	QVariant GetRecordValue(const QString &FieldName, int RowIndex); //�������� �������� �� ������� �� ������� ������
	QVector<int> GetSelectedIDs(); //�������� ������ ��������������� ���������� �����
	QVector<int> GetSelectedRowIndexes(); //�������� ������ �������� ���������� �����
	ISQueryModel* GetQueryModel(); //�������� ��������� �� ������ �������
	void SetSelectObjectAfterUpdate(int object_id); //�������� ��� �������, ������� ����� ������� ����� �������� (��������, �������� �����, ���������) ��� ��� 
	PMetaClassTable* GetMetaTable(); //�������� ��������� �� ����-�������

	virtual void Picking(); //������ ��������
	virtual void Select(); //����� �������
	virtual void Create(); //�������� �������
	virtual void CreateCopy(); //�������� ����� �������
	virtual void Edit(); //��������� �������
	virtual void Delete(); //�������� �������
	virtual bool DeleteCascade(); //�������� ������� ��������
	virtual void Update(); //���������� ������
	virtual void ShowActual(); //���������� ���������� ������
	virtual void ShowDeleted(); //���������� ��������� ������
	virtual void Search(); //�����
	virtual void SearchClear(); //�������� ���������� ������
	virtual void Export(); //��������� �������
	virtual void Print(); //������
	virtual void ShowSystemInfo(); //�������� ����� � ��������� ����������� �� �������
	virtual void AttachTask(); //���������� ������ � ������
	virtual void ShowFavorites(); //�������� ����� � ���������� ���������
	virtual void ShowReference(); //�������
	virtual void NavigationSelectBeginRecord(); //��������� ������ ������ � ������
	virtual void NavigationSelectPreviousRecord(); //��������� ���������� ������
	virtual void NavigationSelectNextRecord(); //��������� ��������� ������
	virtual void NavigationSelectLastRecord(); //��������� ��������� ������
	void NoteObject(); //���������� �������
	void AutoFitColumnWidth(); //���������� ������
	void ResetWidthColumn(); //����� ������ �������
	void CopyRecord(); //����������� ������ � ����� ������
	virtual void LoadData() override; //�������� ������ � �������
	virtual void CornerButtonClicked(); //������� ������� �� cornerButton
	
	void AddAction(QAction *Action, bool AddingToActionGroup = true, bool AddingToContextMenu = false); //�������� ������-��������
	void InsertAction(QAction *ActionBefore, QAction *ActionAfter, bool AddingToActionGroup = true); //�������� ������-��������

	void HideField(const QString &FieldName); //������� ���� �� ��� �����
	void ShowField(const QString &FieldName); //����������� ���� �� ��� �����
	void SetShowOnly(bool show_only);
	void UseAccess();

protected:
	void paintEvent(QPaintEvent *e);

	ISBaseTableView* GetTableView(); //�������� ��������� �� ������ ������
	QToolBar* GetToolBar(); //�������� ��������� �� ������ �������
	QHBoxLayout* GetLayoutTableView(); //�������� ��������� �� ����������� �������
	QAction* GetAction(ISNamespace::ActionType action_type);
	QAction* GetSpecialAction(ISNamespace::ActionSpecialType action_special);
	ISSqlModelCore* GetSqlModel(); //�������� ��������� �� ������
	ISModelThreadQuery* GetModelThread(); //�������� �������� ������

	virtual void SelectedRowEvent(const QItemSelection &ItemSelected, const QItemSelection &ItemDeSelected); //������� ������ ������ � �������
	virtual void LoadDataAfterEvent(); //������� ������������ ����� �������� ������
	virtual void AfterShowEvent() override;
	
	bool CheckIsSystemObject(); //�������� ������� �� ������ "���������"
	void ResizeColumnsToContents(); //������ ������ ����� � ������������ � ����������
	void CalculateRowCount(); //������� ���������� ����� � ������� ��� ������� "LabelRowCount"
	void AddWidgetToBottom(QWidget *Widget); //�������� ������ � ������ ������ ����� ������ (WidgetBottom)
	void SetVisibleBottom(bool Visible); //�������� ��������� ������� ������� (WidgetBottom)
	void ClosingObjectForm(); //����������� ��� �������� ����� ������� (����������� ���� ������ ������)
	void CenteringIndicatorWidget(); //������������� ������� ���������
	void SelectRowObject(int object_id); //��������� ������ �� �������������� �������
	void SelectRowIndex(int row_index); //��������� ������ �� �������
	void SetEnabledActionObject(bool Enabled); //�������� ����������� �������� ��� ��������
	void SearchFast(const QVariant &value); //������� �����
	
	void ActionSetVisible(ISNamespace::ActionType action_type, bool visible); //�������� ��������� ������-��������
	void ActionSetEnabled(ISNamespace::ActionType action_type, bool enabled); //�������� ����������� ������-��������
	void ActionSetText(ISNamespace::ActionType action_type, const QString &text); //�������� ����� ������-��������

	void ModelThreadStarted(); //������� ������� ������� �� �������� ������
	void ModelThreadLoadingData(); //������� �������� ������ � ������
	void ModelThreadFinished(); //������� ���������� ������� �� �������� ������
	void ModelThreadErrorConnection(const QSqlError &SqlError); //������� ��� ������ ����������� � ��
	void ModelThreadErrorQuery(const QSqlError &SqlError, const QString &QueryText); //������� ��� ������ � �������

	virtual void DoubleClickedTable(const QModelIndex &ModelIndex); //���������� ������� �������� ������� �� ������ �������

	void HeaderResized(int Column, int OldSize, int NewSize);
	void SortingChanged(int LogicalIndex, Qt::SortOrder SortOrder);
	void SortingDefault(); //��������� ���������� �� ���������
	void HideSystemFields(); //������� ��������� �����

private:
	void CreateActions(); //�������� ������-��������
	void CreateSpecialActions(); //�������� ����������� ������-��������
	void CreateToolBar(); //�������� �������
	void CreateTableView(); //�������� �������
	void CreateContextMenu(); //�������� ������������ ����
	virtual void CreateModels(); //�������� �������
	void CreateStatusBar(); //������� ������-����
	void CreateDelegates(); //�������� ���������
	
	void ShowContextMenu(const QPoint &Point); //����������� ������������ ����

private:
	PMetaClassTable *MetaTable;
	ISSqlModelCore *SqlModel;
	ISNamespace::ShowDataType CurrentShowDataType;
	QLabel *LabelRowCount;
	QLabel *LabelSwichSelectedType;
	ISPageNavigation *PageNavigation;
	ISLineEdit *EditSearch;
	QProgressBar *ProgressSearchFast;
	QStatusBar *StatusBar;

	ISModelThreadQuery *ModelThreadQuery;
	ISQueryModel *QueryModel;
	ISSearchForm *SearchForm;

	bool UpdateDataSort; //��������� ���������� ���������� ������� ��� ����������
	int SelectObjectAfterUpdate;
	bool DelegatesCreated; //��������� �������� ������� �������� ��� ���
	bool RememberColumnSize;
	bool ShowOnly;
	bool IsQueryForm; //�������� �� ��� ����� ������ ��������
	
	QToolBar *ToolBar;
	QMap<ISNamespace::ActionType, QAction*> Actions;
	QMap<ISNamespace::ActionSpecialType, QAction *> ActionsSpecial;
	QActionGroup *ActionObjectGroup; //������ �������� ��� ������ �������

	QMenu *ContextMenu;

	ISBaseTableView *TableView;
	ISListIndicatorWidget *ListIndicatorWidget;
	QHBoxLayout *LayoutTableView;
};
//-----------------------------------------------------------------------------
