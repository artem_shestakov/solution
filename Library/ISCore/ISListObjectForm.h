#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
#include "ISListBaseForm.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISListObjectForm : public ISListBaseForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISListObjectForm(PMetaClassTable *MetaTable, int ParentObjectID, QWidget *parent = 0);
	virtual ~ISListObjectForm();
};
//-----------------------------------------------------------------------------
