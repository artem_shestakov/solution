#include "StdAfx.h"
#include "ISObjectFormBase.h"
#include "EXDefines.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISMetaData.h"
#include "ISListEdit.h"
#include "ISPhoneEdit.h"
#include "ISSettings.h"
#include "ISCore.h"
#include "ISSystem.h"
#include "ISControls.h"
#include "ISMessageBox.h"
#include "ISDatabase.h"
#include "ISQuery.h"
#include "ISAssert.h"
#include "ISNotificationService.h"
#include "ISProtocol.h"
#include "ISListObjectForm.h"
#include "ISCountingTime.h"
#include "ISDebug.h"
#include "ISDatabaseHelper.h"
#include "ISTrace.h"
#include "ISStyleSheet.h"
#include "ISFavorites.h"
#include "ISHistory.h"
#include "EXConstants.h"
#include "ISCreatedObjectsEntity.h"
//-----------------------------------------------------------------------------
ISObjectFormBase::ISObjectFormBase(ISNamespace::ObjectFormType form_type, PMetaClassTable *meta_table, QWidget *parent, int object_id) : ISInterfaceForm(parent)
{
	FormType = form_type;
	MetaTable = meta_table;
	ObjectID = object_id;
	ParentObjectID = 0;

	WidgetObject = nullptr;
	WidgetEscort = nullptr;
	SearchDuplicatesPanel = nullptr;

	SystemInfoObject = nullptr;

	BeginFieldEdit = nullptr;

	ModificationFlag = false;

	ObjectModel = new ISObjectModel(this);

	GetMainLayout()->setContentsMargins(0, 3, 0, 0);
	GetMainLayout()->setSpacing(0);

	ActionGroup = new QActionGroup(this);

	CreateToolBarEscorts();
	CreateMainTabWidget();
	CreateSearchDuplicates();
	CreateToolBar();
	CreateFieldsWidget();
	FillDataFields();

	ISCreatedObjectsEntity::GetInstance().RegisterForm(this);
}
//-----------------------------------------------------------------------------
ISObjectFormBase::~ISObjectFormBase()
{
	ISCreatedObjectsEntity::GetInstance().UnregisterForm(GetFormUID());
}
//-----------------------------------------------------------------------------
int ISObjectFormBase::GetParentObjectID() const
{
	return ParentObjectID;
}
//-----------------------------------------------------------------------------
void ISObjectFormBase::SetParentObjectID(int parent_object_id)
{
	ParentObjectID = parent_object_id;
}
//-----------------------------------------------------------------------------
ISNamespace::ObjectFormType ISObjectFormBase::GetFormType()
{
	return FormType;
}
//-----------------------------------------------------------------------------
int ISObjectFormBase::GetObjectID() const
{
	return ObjectID;
}
//-----------------------------------------------------------------------------
PMetaClassTable* ISObjectFormBase::GetMetaTable()
{
	return MetaTable;
}
//-----------------------------------------------------------------------------
bool ISObjectFormBase::GetModificationFlag() const
{
	return ModificationFlag;
}
//-----------------------------------------------------------------------------
void ISObjectFormBase::SetVisibleNavigationBar(bool Visible)
{
	ToolBarNavigation->setVisible(Visible);
}
//-----------------------------------------------------------------------------
void ISObjectFormBase::SetVisibleFieldID(bool Visible)
{
	LabelSystemInfoObject->setVisible(Visible);
	SystemInfoObject->setVisible(Visible);
}
//-----------------------------------------------------------------------------
void ISObjectFormBase::SetVisibleField(const QString &FieldName, bool Visible)
{
	LabelsMap.value(FieldName)->setVisible(Visible);
	FieldsMap.value(FieldName)->setVisible(Visible);
}
//-----------------------------------------------------------------------------
void ISObjectFormBase::SetVisibleSearchPanel(bool Visible)
{
	if (SearchDuplicatesPanel)
	{
		SearchDuplicatesPanel->setVisible(Visible);
	}
}
//-----------------------------------------------------------------------------
void ISObjectFormBase::SetVisibleFavorites(bool Visible)
{
	ActionFavorites->setVisible(Visible);
}
//-----------------------------------------------------------------------------
void ISObjectFormBase::SetVisibleDelete(bool Visible)
{
	ActionDelete->setVisible(Visible);
}
//-----------------------------------------------------------------------------
void ISObjectFormBase::SetVisibleSearch(bool Visible)
{
	if (SearchDuplicatesPanel)
	{
		ActionSearchDuplicates->setVisible(Visible);
	}
}
//-----------------------------------------------------------------------------
void ISObjectFormBase::SetVisibleReRead(bool Visible)
{
	ActionReRead->setVisible(Visible);
}
//-----------------------------------------------------------------------------
void ISObjectFormBase::closeEvent(QCloseEvent *e)
{
	if (ModificationFlag)
	{
		emit CurrentObjectTab();

		ISMessageBox MessageBox(QMessageBox::Warning, LOCALIZATION("SavingProcess"), LOCALIZATION("Message.Question.SaveObjectChanged").arg(MetaTable->GetLocalName()), QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel, this);
		MessageBox.setWindowIcon(BUFFER_ICONS("Save"));
		MessageBox.setDefaultButton(QMessageBox::No);
		QMessageBox::StandardButtons ClickedButton = MessageBox.Exec();

		switch (ClickedButton)
		{
		case ISMessageBox::Save:
			if (Save())
			{
				ISInterfaceForm::closeEvent(e);
			}
			else
			{
				e->ignore();
			}
			break;

		case ISMessageBox::Discard:
			ISInterfaceForm::closeEvent(e);
			break;

		case ISMessageBox::Cancel:
			e->ignore();
			break;
		}
	}
	else
	{
		ISInterfaceForm::closeEvent(e);
	}

	emit Close();
}
//-----------------------------------------------------------------------------
void ISObjectFormBase::keyPressEvent(QKeyEvent *e)
{
	if (FormType != ISNamespace::OFT_Show)
	{
		if (e->modifiers() == Qt::CTRL && e->key() == Qt::Key_Return || e->key() == Qt::Key_Enter)
		{
			SaveClose();
		}
		else if (e->modifiers() == Qt::CTRL && e->key() == Qt::Key_S)
		{
			Save();
		}
	}
	else
	{
		ISInterfaceForm::keyPressEvent(e);
	}
}
//-----------------------------------------------------------------------------
void ISObjectFormBase::EscapeClicked()
{
	close();
}
//-----------------------------------------------------------------------------
void ISObjectFormBase::AfterShowEvent()
{
	ISInterfaceForm::AfterShowEvent();

	if (FormType == ISNamespace::OFT_New || FormType == ISNamespace::OFT_Copy)
	{
		ToolBarNavigation->UpdateEnabledActionsList(false);
	}
	else
	{
		ToolBarNavigation->UpdateEnabledActionsList(true);
	}

	if (FormType == ISNamespace::OFT_Edit)
	{
		ISProtocol::ShowObject(MetaTable->GetName(), MetaTable->GetLocalListName(), ObjectID, GetObjectName());
	}

	if (BeginFieldEdit)
	{
		BeginFieldEdit->SetFocus();
	}

	ISFieldEditBase *FieldEditWidget = FieldsMap.value(MetaTable->GetClassFilterField());
	if (FieldEditWidget)
	{
		FieldEditWidget->SetValue(ParentObjectID);
	}

	RenameReiconForm();
	SetModificationFlag(false);
	UpdateObjectActions();
}
//-----------------------------------------------------------------------------
void ISObjectFormBase::CreateToolBarEscorts()
{
	ToolBarNavigation = new ISToolBarObject(this);
	ToolBarNavigation->setIconSize(SIZE_20_20);
	GetMainLayout()->addWidget(ToolBarNavigation);

	//�������� �������
	QAction *ActionObject = ToolBarNavigation->CreateAction(BUFFER_ICONS("Document"), MetaTable->GetLocalName(), ISNamespace::OAT_Object);
	ActionObject->setChecked(true);
	ToolBarNavigation->addAction(ActionObject);
	ToolBarNavigation->actionTriggered(ActionObject);

	//������ ��������� ��������
	QAction *ActionOther = ToolBarNavigation->CreateAction(BUFFER_ICONS("AdditionallyActions"), LOCALIZATION("ObjectOther"), ISNamespace::OAT_Other);
	ToolBarNavigation->addAction(ActionOther);

	QToolButton *ToolButtonOther = dynamic_cast<QToolButton*>(ToolBarNavigation->widgetForAction(ActionOther));
	ToolButtonOther->setPopupMode(QToolButton::InstantPopup);
	ToolButtonOther->setMenu(new QMenu(ToolButtonOther));
	ToolButtonOther->setCursor(CURSOR_POINTING_HAND);
	ToolButtonOther->setStyleSheet(STYLE_SHEET("QToolButtonMenu"));
	ToolButtonOther->menu()->addAction(ToolBarNavigation->CreateAction(BUFFER_ICONS("Protocol"), LOCALIZATION("ProtocolCard"), ISNamespace::OAT_Service, QString(), "ISProtocolObjectListForm"));
	ToolButtonOther->menu()->addAction(ToolBarNavigation->CreateAction(BUFFER_ICONS("Discussion"), LOCALIZATION("Discussion"), ISNamespace::OAT_Service, QString(), "ISDiscussionListForm"));

	for (int i = 0; i < MetaTable->GetEscorts().count(); i++) //����� ��������� ����-������
	{
		PMetaClassEscort *MetaEscort = MetaTable->GetEscorts().at(i);
		
		QAction *ActionEscort = ToolBarNavigation->CreateAction(BUFFER_ICONS("Table"), MetaEscort->GetLocalName(), ISNamespace::OAT_Escort, MetaEscort->GetTableName(), MetaEscort->GetClassName());
		ActionEscort->setProperty("ClassFilter", MetaEscort->GetClassFilter());
		ToolBarNavigation->addAction(ActionEscort);
	}

	connect(ToolBarNavigation, &ISToolBarObject::ActionClicked, this, &ISObjectFormBase::ToolBarClicked);
}
//-----------------------------------------------------------------------------
void ISObjectFormBase::CreateMainTabWidget()
{
	TabWidgetMain = new ISTabWidgetObject(this);
	GetMainLayout()->addWidget(TabWidgetMain);

	StackedWidget = new QStackedWidget(TabWidgetMain);
	TabWidgetMain->addTab(StackedWidget, LOCALIZATION("Card"));

	WidgetObjectLayout = new QVBoxLayout();

	WidgetObject = new QWidget(TabWidgetMain);
	WidgetObject->setLayout(WidgetObjectLayout);
	StackedWidget->addWidget(WidgetObject);

	WidgetTabEscort = new QWidget(TabWidgetMain);
	WidgetTabEscort->setLayout(new QVBoxLayout());
	StackedWidget->addWidget(WidgetTabEscort);
}
//-----------------------------------------------------------------------------
void ISObjectFormBase::CreateSearchDuplicates()
{
	if (FormType == ISNamespace::OFT_New)
	{
		SearchDuplicatesPanel = new ISObjectSearchDuplicates(MetaTable, this);
		SearchDuplicatesPanel->setSizePolicy(SearchDuplicatesPanel->sizePolicy().horizontalPolicy(), QSizePolicy::Maximum);
		WidgetObjectLayout->addWidget(SearchDuplicatesPanel);
		
		connect(SearchDuplicatesPanel, &ISObjectSearchDuplicates::Search, this, &ISObjectFormBase::SearchDuplicates);
		connect(SearchDuplicatesPanel, &ISObjectSearchDuplicates::Hided, [=]
		{
			BeginFieldEdit->SetFocus();
		});
	}
}
//-----------------------------------------------------------------------------
void ISObjectFormBase::CreateToolBar()
{
	ToolBar = new QToolBar(this);
	ToolBar->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
	WidgetObjectLayout->addWidget(ToolBar);

	//��������� � ������� ��������
	ActionSaveClose = ISControls::CreateActionSaveAndClose(ToolBar);
	ActionSaveClose->setToolTip(LOCALIZATION("SaveChangeClose"));
	connect(ActionSaveClose, &QAction::triggered, this, &ISObjectFormBase::SaveClose);
	ToolBar->addAction(ActionSaveClose);

	//��������� ��������
	ActionSave = ISControls::CreateActionSave(ToolBar);
	ActionSave->setToolTip(LOCALIZATION("SaveChange"));
	connect(ActionSave, &QAction::triggered, this, &ISObjectFormBase::Save);
	ToolBar->addAction(ActionSave);

	//������� ��������
	ActionClose = ISControls::CreateActionClose(ToolBar);
	ActionClose->setToolTip(LOCALIZATION("CloseCard"));
	connect(ActionClose, &QAction::triggered, this, &ISObjectFormBase::close);
	ToolBar->addAction(ActionClose);

	if (SearchDuplicatesPanel)
	{
		//����� ����������
		ActionSearchDuplicates = new QAction(ToolBar);
		ActionSearchDuplicates->setText(LOCALIZATION("ClickFromVisibleSearchPanel"));
		ActionSearchDuplicates->setToolTip(LOCALIZATION("ClickFromVisibleSearchPanel"));
		ActionSearchDuplicates->setIcon(BUFFER_ICONS("Search"));
		ActionSearchDuplicates->setPriority(QAction::LowPriority);
		connect(ActionSearchDuplicates, &QAction::triggered, SearchDuplicatesPanel, &ISObjectSearchDuplicates::Visible);
		ToolBar->addAction(ActionSearchDuplicates);
	}

	//���������
	ActionFavorites = new QAction(ToolBar);
	ActionFavorites->setText(LOCALIZATION("AddToFavorites"));
	ActionFavorites->setToolTip(LOCALIZATION("AddToFavorites"));
	ActionFavorites->setIcon(BUFFER_ICONS("Favorites"));
	ActionFavorites->setPriority(QAction::LowPriority);
	ActionFavorites->setCheckable(true);
	connect(ActionFavorites, &QAction::triggered, this, &ISObjectFormBase::AddFavoite);
	AddAction(ActionFavorites);

	//�������/������������ ��������
	ActionDelete = ISControls::CreateActionDelete(ToolBar);
	ActionDelete->setPriority(QAction::LowPriority);
	connect(ActionDelete, &QAction::triggered, this, &ISObjectFormBase::Delete);
	AddAction(ActionDelete);

	//�������� ���������
	ActionCancelChange = new QAction(ToolBar);
	ActionCancelChange->setEnabled(false);
	ActionCancelChange->setText(LOCALIZATION("CancelChanged"));
	ActionCancelChange->setToolTip(LOCALIZATION("CancelChanged"));
	ActionCancelChange->setIcon(BUFFER_ICONS("CancelChangedObject"));
	ActionCancelChange->setPriority(QAction::LowPriority);
	connect(ActionCancelChange, &QAction::triggered, this, &ISObjectFormBase::CancelChanged);
	ToolBar->addAction(ActionCancelChange);

	//����������
	ActionReRead = new QAction(ToolBar);
	ActionReRead->setEnabled(false);
	ActionReRead->setText(LOCALIZATION("ReReadCard"));
	ActionReRead->setToolTip(LOCALIZATION("ReReadCard"));
	ActionReRead->setIcon(BUFFER_ICONS("Update"));
	ActionReRead->setPriority(QAction::LowPriority);
	connect(ActionReRead, &QAction::triggered, this, &ISObjectFormBase::ReRead);
	ToolBar->addAction(ActionReRead);
}
//-----------------------------------------------------------------------------
void ISObjectFormBase::CreateFieldsWidget()
{
	QFormLayout *FormLayout = new QFormLayout();

	ISScrollArea *ScrollAreaMain = new ISScrollArea(this); //������ �������� (�������) ������ �����
	ScrollAreaMain->widget()->setLayout(FormLayout);
	WidgetObjectLayout->addWidget(ScrollAreaMain);

	CreateSystemInfoWidget(FormLayout);

	for (int i = 0; i < MetaTable->GetFields().count(); i++) //����� �����
	{
		PMetaClassField *MetaField = MetaTable->GetFields().at(i);

		if (MetaField->GetQueryText().length())
		{
			continue;
		}

		if (MetaField->GetType() == ISNamespace::FT_ByteArray && !MetaField->GetControlWidget().length())
		{
			continue;
		}

		QString LayoutName = MetaField->GetLayoutName();
		if (LayoutName.length())
		{
			if (!Layouts.contains(LayoutName))
			{
				QHBoxLayout *LayoutHorizontal = new QHBoxLayout();
				LayoutHorizontal->setProperty("Inserted", false);
				Layouts.insert(LayoutName, LayoutHorizontal);
			}
		}

		ISFieldEditBase *FieldEditBase = CreateColumnForField(MetaField);
		AddColumnForField(MetaField->GetLabelName(), FieldEditBase, FormLayout);
	}
}
//-----------------------------------------------------------------------------
void ISObjectFormBase::FillDataFields()
{
	if (FormType == ISNamespace::OFT_Edit || FormType == ISNamespace::OFT_Copy)
	{
		//���������� �������
		ISQueryModel QueryModel(MetaTable, ISNamespace::QMT_Object);
		QueryModel.SetClassFilter(MetaTable->GetAlias() + "." + MetaTable->GetAlias() + "_id = " + QString::number(ObjectID));
		QString QueryText = QueryModel.GetQueryText();

		//���������� �������
		ISDebug::ShowDebugString(QString("Start select query from object %1 in table \"%2\"").arg(ObjectID).arg(MetaTable->GetName()));
		ISCountingTime Time;
		ISQuery qSelect(QueryText);
		IS_ASSERT(qSelect.ExecuteFirst(), "Not executed query " + QueryText);
		ISDebug::ShowDebugString(QString("Finished select query from object %1 in table \"%2\". %3 msec.").arg(ObjectID).arg(MetaTable->GetName()).arg(Time.GetElapsed()));
		QSqlRecord SqlRecord = qSelect.GetRecord();

		SystemInfoObject->SetInfo(ObjectID, SqlRecord.value("CreationUser").toString(), SqlRecord.value("UpdationUser").toString());

		for (int i = 0; i < SqlRecord.count(); i++) //����� ����� ������
		{
			QString FieldName = SqlRecord.fieldName(i);
			QVariant Value = SqlRecord.value(FieldName);
			if (Value.isNull()) //���� �������� ������, ������� �� ��������� ��� �����
			{
				continue;
			}

			ISFieldEditBase *FieldEditWidget = FieldsMap.value(FieldName);
			if (FieldEditWidget)
			{
				disconnect(FieldEditWidget, &ISFieldEditBase::DataChanged, this, &ISObjectFormBase::DataChanged);

				ISListEdit *ListEdit = dynamic_cast<ISListEdit*>(FieldEditWidget);
				if (ListEdit)
				{
					QVariant ListObjectID = ISDatabaseHelper::GetObjectIDToList(MetaTable, MetaTable->GetField(FieldName), ObjectID);
					ListEdit->SetValue(ListObjectID);
					
					ObjectModel->SetOldValue(FieldName, ListObjectID);
					ObjectModel->SetNewValue(FieldName, ListObjectID);
				}
				else
				{
					FieldEditWidget->SetValue(Value);
					
					ObjectModel->SetOldValue(FieldName, Value);
					ObjectModel->SetNewValue(FieldName, Value);
				}

				if (FormType == ISNamespace::OFT_Copy) //���� ����� ����������� ��� �������� �����
				{
					FieldEditWidget->SetModificationFlag(true);
				}
				else
				{
					FieldEditWidget->SetModificationFlag(false);
				}

				connect(FieldEditWidget, &ISFieldEditBase::DataChanged, this, &ISObjectFormBase::DataChanged);
			}
		}

		ISHistory::AddHistory(MetaTable->GetName(), MetaTable->GetLocalListName(), GetObjectName(), ObjectID);
	}
}
//-----------------------------------------------------------------------------
void ISObjectFormBase::CreateSystemInfoWidget(QFormLayout *FormLayout)
{
	LabelSystemInfoObject = new QLabel(this);
	LabelSystemInfoObject->setText(LOCALIZATION("SystemField.ID") + ":");
	LabelSystemInfoObject->setFont(FONT_APPLICATION_BOLD);

	SystemInfoObject = new ISSystemInfoObject(this);
	SystemInfoObject->setSizePolicy(QSizePolicy::Maximum, SystemInfoObject->sizePolicy().verticalPolicy());

	FormLayout->addRow(LabelSystemInfoObject, SystemInfoObject);

	SetVisibleFieldID(SETTING_BOOL(CONST_UID_SETTING_OBJECTS_VISIBLESYSTEMINFO));
}
//-----------------------------------------------------------------------------
ISFieldEditBase* ISObjectFormBase::CreateColumnForField(PMetaClassField *MetaField)
{
	ISFieldEditBase	*FieldEditBase = ISCore::CreateColumnForField(this, MetaField);
	FieldsMap.insert(MetaField->GetName(), FieldEditBase);
	connect(FieldEditBase, &ISFieldEditBase::DataChanged, this, &ISObjectFormBase::DataChanged);
	return FieldEditBase;
}
//-----------------------------------------------------------------------------
void ISObjectFormBase::AddColumnForField(const QString &LabelName, ISFieldEditBase *FieldEditBase, QFormLayout *FormLayout)
{
	QString FieldName = FieldEditBase->GetFieldName();
	QVariant DefaultValueWidget = FieldEditBase->GetDefaultValueWidget();
	bool NotNull = FieldEditBase->GetNotNull();
	bool ReadOnly = FieldEditBase->GetReadOnly();
	bool HideFromObject = FieldEditBase->GetHideFromObject();

	QLabel *LabelField = new QLabel(this);
	LabelField->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Expanding);
	LabelsMap.insert(FieldName, LabelField);

	ObjectModel->Append(FieldName);

	if (DefaultValueWidget.isValid()) //���� � ���� ������� �������� �� ��������� ��� �������-���������
	{
		FieldEditBase->SetValue(DefaultValueWidget);
		FieldEditBase->SetModificationFlag(true);
	}

	if (NotNull) //���� ���� ���������� ��� ����������
	{
		LabelField->setTextFormat(Qt::RichText);
		LabelField->setText(QString("<font>%1:</font><font color=#FF0000 size=4>*</font>").arg(LabelName));
		LabelField->setToolTip(LOCALIZATION("FieldNotNull"));
		LabelField->setCursor(CURSOR_WHATS_THIS);
	}
	else
	{
		LabelField->setText(LabelName + ":");
	}

	if (ReadOnly)
	{
		FieldEditBase->SetEnabledClear(!ReadOnly);
		FieldEditBase->SetReadOnly(ReadOnly);
	}

	//��������� ������
	QString FontText = SETTING_STRING(CONST_UID_SETTING_FONTS_FIELDEDITBASE);
	QFont Font = ISSystem::StringToFont(FontText);
	FieldEditBase->SetFont(Font);

	if (HideFromObject) //��� ������� ������ ���� ����� ��������� � ���� ������: ������� ������� � �������� �� �����
	{
		LabelField->setVisible(false);
		FieldEditBase->setVisible(false);
	}
	else
	{
		if (!BeginFieldEdit && !ReadOnly)
		{
			BeginFieldEdit = FieldEditBase;
		}
	}

	QHBoxLayout *LayoutHorizontal = Layouts.value(FieldEditBase->GetMetaField()->property("LayoutName").toString());
	if (LayoutHorizontal)
	{
		if (LayoutHorizontal->property("Inserted").toBool())
		{
			LayoutHorizontal->addWidget(LabelField);
			LayoutHorizontal->addWidget(FieldEditBase);
		}
		else
		{
			LayoutHorizontal->setProperty("Inserted", true);

			FormLayout->addRow(LabelField, LayoutHorizontal);
			LayoutHorizontal->addWidget(FieldEditBase);
		}
	}
	else
	{
		FormLayout->addRow(LabelField, FieldEditBase);
	}
}
//-----------------------------------------------------------------------------
void ISObjectFormBase::AddObjectEscort(QWidget *ObjectForm)
{
	//��������� ������������ ��������� ������� ����� �������
	connect(ObjectForm, &QWidget::windowTitleChanged, [=](const QString &WindowTitle)
	{
		TabWidgetMain->setTabText(TabWidgetMain->indexOf(ObjectForm), WindowTitle);
	});

	//��������� ������ ��������� ������� ����� �������
	connect(ObjectForm, &QWidget::windowIconChanged, [=](const QIcon &WindowIcon)
	{
		TabWidgetMain->setTabIcon(TabWidgetMain->indexOf(ObjectForm), WindowIcon);
	});

	ObjectForm->setParent(this);
	TabWidgetMain->addTab(ObjectForm, ObjectForm->windowIcon(), ObjectForm->windowTitle());
	TabWidgetMain->setCurrentWidget(ObjectForm);
}
//-----------------------------------------------------------------------------
void ISObjectFormBase::ToolBarClicked(QAction *ActionClicked)
{
	if (WidgetEscort)
	{
		delete WidgetEscort;
		WidgetEscort = nullptr;
	}

	TabWidgetMain->setCurrentWidget(StackedWidget);

	if (qvariant_cast<ISNamespace::ObjectActionType>(ActionClicked->property("Type")) == ISNamespace::OAT_Object) //������ ������
	{
		TabWidgetMain->setTabText(0, LOCALIZATION("Card"));
		StackedWidget->setCurrentWidget(WidgetObject);
	}
	else //������ ������
	{
		TabWidgetMain->setTabText(TabWidgetMain->indexOf(StackedWidget), ActionClicked->text());
		StackedWidget->setCurrentWidget(WidgetTabEscort);

		QString TableName = ActionClicked->property("TableName").toString();
		QString ClassName = ActionClicked->property("ClassName").toString();
		QString ClassFilter = ActionClicked->property("ClassFilter").toString();

		if (ClassName.length()) //�������� �������
		{
			int ObjectType = QMetaType::type((ClassName + "*").toLocal8Bit().constData());
			IS_ASSERT(ObjectType, "Class for SybSystem is NULL. ClassName: " + ClassName);

			const QMetaObject *MetaObject = QMetaType::metaObjectForType(ObjectType);
			IS_ASSERT(MetaObject, "Error opening subsystem widget.");

			WidgetEscort = dynamic_cast<ISInterfaceMetaForm*>(MetaObject->newInstance(Q_ARG(QWidget *, WidgetTabEscort)));
			IS_ASSERT(WidgetEscort, "Error instance escort class.");
		}
		else //�������� �������
		{
			PMetaClassTable *MetaTableEscort = ISMetaData::GetInstanse().GetMetaTable(TableName);
			
			ISListObjectForm *ListForm = new ISListObjectForm(MetaTableEscort, ObjectID, WidgetTabEscort);
			ListForm->SetUID(MetaTableEscort->GetUID());
			
			if (ClassFilter.length())
			{
				ListForm->GetQueryModel()->SetClassFilter(ClassFilter);
			}

			WidgetEscort = ListForm;
		}

		connect(WidgetEscort, &ISInterfaceMetaForm::AddFormFromTab, this, &ISObjectFormBase::AddObjectEscort);
		WidgetTabEscort->layout()->addWidget(WidgetEscort);

		//���������� ���� ������ ���� ����� ������� LoadData()
		WidgetEscort->SetParentTableName(MetaTable->GetName());
		WidgetEscort->SetParentObjectID(ObjectID);

		WidgetEscort->LoadData();
	}
}
//-----------------------------------------------------------------------------
void ISObjectFormBase::SaveClose()
{
	if (ModificationFlag)
	{
		if (Save())
		{
			close();
		}
	}
	else
	{
		close();
	}
}
//-----------------------------------------------------------------------------
bool ISObjectFormBase::Save()
{
	QVariantMap ValuesMap;
	QVector<QString> FieldsVector;
	QString QueryText;
	bool Executed = false;

	for (const auto Field : FieldsMap.toStdMap()) //����� ������������ ����� �� �����
	{
		QString FieldName = Field.first;
		PMetaClassField *MetaField = MetaTable->GetField(FieldName);
		ISFieldEditBase *FieldEditWidget = Field.second;

		QVariant Value = FieldEditWidget->GetValue();
		if (Value.isNull()) //���� �������� � ���� �����������, ��������� ����������� �� ���� ��� ����������
		{
			if (MetaField->GetNotNull() && !MetaField->GetHideFromObject()) //���� ���� ����������� ��� ����������
			{
				ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Error.Field.NullValue").arg(MetaField->GetLabelName()));
				FieldEditWidget->BlinkRed();
				return false;
			}
		}

		if (!FieldEditWidget->GetModificationFlag()) //���� �������� ���� ������������� �� ����������, ���������� � ����������
		{
			if (MetaTable->GetClassFilterField() != FieldName) //���� ������� ���� �� �������� ����������� (������� ������� �� �������� ���������)
			{
				continue;
			}
		}

		if (!FieldEditWidget->IsValid())
		{
			ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.ValueFieldEditInvalid").arg(MetaField->GetLabelName()));
			FieldEditWidget->BlinkRed();
			return false;
		}

		ValuesMap.insert(FieldName, Value);
		FieldsVector.append(FieldName);
	}

	//������������ ������� �� ����������/���������/�����������
	if (FormType == ISNamespace::OFT_New || FormType == ISNamespace::OFT_Copy)
	{
		QString InsertFields = "INSERT INTO " + MetaTable->GetName() + " (";
		QString InsertValues = "VALUES (";

		for (int i = 0; i < FieldsVector.count(); i++)
		{
			InsertFields += MetaTable->GetAlias() + "_" + FieldsVector.at(i) + ", ";
			InsertValues += ":" + FieldsVector.at(i) + ", ";
		}

		ISSystem::RemoveLastSymbolFromString(InsertFields, 2);
		ISSystem::RemoveLastSymbolFromString(InsertValues, 2);

		QueryText += InsertFields + ") \n";
		QueryText += InsertValues + ") \n";
		QueryText += "RETURNING " + MetaTable->GetAlias() + "_id, " + MetaTable->GetAlias() + "_creationuser, " + MetaTable->GetAlias() + "_updationuser";
	}
	else if (FormType == ISNamespace::OFT_Edit)
	{
		QueryText += "UPDATE " + MetaTable->GetName() + " SET \n";

		//���������� ����� ����������� � ������������� � ������� ����
		QueryText += MetaTable->GetAlias() + "_updationdate = now(), \n";
		QueryText += MetaTable->GetAlias() + "_updationuser = CURRENT_USER, \n";

		for (int i = 0; i < FieldsVector.count(); i++)
		{
			QueryText += MetaTable->GetAlias() + "_" + FieldsVector.at(i) + " = :" + FieldsVector.at(i) + ", \n";
		}

		ISSystem::RemoveLastSymbolFromString(QueryText, 3);

		QueryText += " \n";
		QueryText += "WHERE " + MetaTable->GetAlias() + "_id = " + QString::number(ObjectID) + " \n";
		QueryText += "RETURNING " + MetaTable->GetAlias() + "_updationuser";
	}

	ISDatabase::GetInstance().GetDefaultDB().transaction(); //�������� ����������
	SaveEvent();

	//���������� ������� ����������
	ISQuery SqlQuery(QueryText);
	for (const auto Value : ValuesMap.toStdMap())
	{
		bool BindValue = SqlQuery.BindValue(":" + Value.first, Value.second);
		IS_ASSERT(BindValue, "Not bind value");
	}

	try
	{
		Executed = SqlQuery.Execute(); //���������� �������
		IS_ASSERT(SqlQuery.First(), "Not first SqlQuery");

		if (FormType == ISNamespace::OFT_New || FormType == ISNamespace::OFT_Copy)
		{
			ObjectID = SqlQuery.ReadColumn(MetaTable->GetAlias() + "_id").toInt();
			SystemInfoObject->SetInfo(ObjectID, SqlQuery.ReadColumn(MetaTable->GetAlias() + "_creationuser").toString());
		}
	}
	catch (ISQueryException &e)
	{
		ISDatabase::GetInstance().GetDefaultDB().rollback(); //����� ����������
		ISMessageBox::ShowWarning(this, QString::fromLocal8Bit(e.what()));
	}

	if (Executed) //������ �������� �������
	{
		ISDatabase::GetInstance().GetDefaultDB().commit();

		if (FormType == ISNamespace::OFT_New)
		{
			FormType = ISNamespace::OFT_Edit;
			ISNotificationService::ShowNotification(ISNamespace::NFT_Create, MetaTable->GetLocalName(), GetObjectName());
			ISProtocol::CreateObject(MetaTable->GetName(), MetaTable->GetLocalListName(), ObjectID, GetObjectName());
		}
		else if (FormType == ISNamespace::OFT_Copy)
		{
			FormType = ISNamespace::OFT_Edit;
			ISNotificationService::ShowNotification(ISNamespace::NFT_CreateCopy, MetaTable->GetLocalName(), GetObjectName());
			ISProtocol::CreateCopyObject(MetaTable->GetName(), MetaTable->GetLocalListName(), ObjectID, GetObjectName());
		}
		else if (FormType == ISNamespace::OFT_Edit)
		{
			ISNotificationService::ShowNotification(ISNamespace::NFT_Edit, MetaTable->GetLocalName(), GetObjectName());
			ISProtocol::EditObject(MetaTable->GetName(), MetaTable->GetLocalListName(), ObjectID, GetObjectName());
			SystemInfoObject->SetInfo(ObjectID, QString(), SqlQuery.ReadColumn(MetaTable->GetAlias() + "_updationuser").toString());
		}

		RenameReiconForm();
		SetModificationFlag(false);
		UpdateObjectActions();
		ToolBarNavigation->UpdateEnabledActionsList(true);
		ActionFavorites->setEnabled(true);

		if (SearchDuplicatesPanel)
		{
			SearchDuplicatesPanel->Hide();
			ActionSearchDuplicates->setVisible(false);
		}

		emit SavedObject(ObjectID);
		emit UpdateList();

		return true;
	}

	return false;
}
//-----------------------------------------------------------------------------
void ISObjectFormBase::SaveEvent()
{

}
//-----------------------------------------------------------------------------
void ISObjectFormBase::RenameReiconForm()
{
	QString TitleName;
	QIcon TitleIcon;

	switch (FormType)
	{
	case ISNamespace::OFT_New:
		TitleName = LOCALIZATION("Creating") + " (" + MetaTable->GetLocalName() + ")";
		TitleIcon = BUFFER_ICONS("Add");
		break;

	case ISNamespace::OFT_Edit:
		TitleName = MetaTable->GetLocalName() + ": " + GetObjectName();
		TitleIcon = BUFFER_ICONS("Edit");
		break;

	case ISNamespace::OFT_Copy:
		TitleName = LOCALIZATION("Coping") + " (" + MetaTable->GetLocalName() + "): " + GetObjectName();
		TitleIcon = BUFFER_ICONS("AddCopy");
		break;

	case ISNamespace::OFT_Show:
		TitleName = LOCALIZATION("ShowObject") + ": " + MetaTable->GetLocalListName() + " - " + GetObjectName();
		TitleIcon = BUFFER_ICONS("ShowObject");
		break;
	}

	setWindowIcon(TitleIcon);
	setWindowTitle(TitleName);
}
//-----------------------------------------------------------------------------
void ISObjectFormBase::DataChanged()
{
	SetModificationFlag(true);

	ISFieldEditBase *FieldEdited = dynamic_cast<ISFieldEditBase*>(sender());
	if (FieldEdited)
	{
		QString FieldName = FieldsMap.key(FieldEdited);
		if (FieldName.length())
		{
			ObjectModel->SetNewValue(FieldName, FieldEdited->GetValue());
		}
	}
}
//-----------------------------------------------------------------------------
void ISObjectFormBase::SetModificationFlag(bool modification)
{
	ModificationFlag = modification;
	ActionSave->setEnabled(modification);
	ActionCancelChange->setEnabled(modification);

	QString WindowTitle = windowTitle();
	if (modification)
	{
		if (!WindowTitle.contains(SYMBOL_OBJECT_CHANGED))
		{
			WindowTitle += SYMBOL_OBJECT_CHANGED;
		}
	}
	else
	{
		if (WindowTitle.contains(SYMBOL_OBJECT_CHANGED))
		{
			WindowTitle.remove(SYMBOL_OBJECT_CHANGED);
		}
	}

	setWindowTitle(WindowTitle);
}
//-----------------------------------------------------------------------------
void ISObjectFormBase::UpdateObjectActions()
{
	if (FormType == ISNamespace::OFT_New)
	{
		SetEnabledActions(false);
	}
	else if (FormType == ISNamespace::OFT_Copy)
	{
		SetEnabledActions(false);
		ActionSave->setEnabled(true);
	}
	else if (FormType == ISNamespace::OFT_Edit)
	{
		SetEnabledActions(true);
		ActionSave->setEnabled(false);
		ActionReRead->setEnabled(true);
		ActionFavorites->setChecked(ISFavorites::GetInstance().CheckExistFavoriteObject(MetaTable->GetName(), ObjectID));
	}
}
//-----------------------------------------------------------------------------
void ISObjectFormBase::SearchDuplicates(const QString &SqlText)
{
	ISQuery qSelect(SqlText);
	if (qSelect.Execute())
	{
		if (qSelect.GetCountResultRows())
		{
			QList<QSqlRecord> Data;

			while (qSelect.Next())
			{
				Data.push_back(qSelect.GetRecord());
			}

			ISBaseTableView *TableResult = new ISBaseTableView();

			ISSqlModelCore *SqlModel = new ISSqlModelCore(MetaTable, TableResult);
			SqlModel->SetRecords(Data);

			//�������� ��������� ����� �� ������
			SqlModel->RemoveColumn(MetaTable->GetField("IsDeleted"));
			SqlModel->RemoveColumn(MetaTable->GetField("IsSystem"));
			
			TableResult->setModel(SqlModel);
			TableResult->show();
		}
		else
		{

		}
	}
}
//-----------------------------------------------------------------------------
void ISObjectFormBase::AddFavoite()
{
	if (ISFavorites::GetInstance().CheckExistFavoriteObject(MetaTable->GetName(), ObjectID))
	{
		ISFavorites::GetInstance().DeleteFavorite(MetaTable->GetName(), ObjectID);
		ActionFavorites->setChecked(false);
		ISNotificationService::ShowNotification(LOCALIZATION("RecordRemoveFavorites"));
	}
	else
	{
		ISFavorites::GetInstance().AddFavorite(MetaTable->GetName(), MetaTable->GetLocalListName(), GetObjectName(), ObjectID);
		ActionFavorites->setChecked(true);
		ISNotificationService::ShowNotification(LOCALIZATION("RecordAddFavorites"));
	}
}
//-----------------------------------------------------------------------------
void ISObjectFormBase::Delete()
{
	if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.DeleteThisRecord")))
	{
		bool Result = ISCore::DeleteOrRecoveryObject(ISNamespace::DRO_Delete, MetaTable->GetName(), MetaTable->GetAlias(), GetObjectID(), MetaTable->GetLocalListName());
		if (Result)
		{
			emit UpdateList();
			close();
		}
	}
}
//-----------------------------------------------------------------------------
void ISObjectFormBase::CancelChanged()
{
	if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.CancelChanged")))
	{
		for (const auto &FieldItem : FieldsMap.toStdMap())
		{
			QString FieldName = FieldItem.first;
			ISFieldEditBase *FieldWidget = FieldItem.second;

			QVariant OldValue = ObjectModel->GetOldValue(FieldName);
			QVariant CurrentValue = FieldWidget->GetValue();

			if (OldValue != CurrentValue)
			{
				FieldWidget->SetValue(OldValue);
			}
		}

		SetModificationFlag(false);
	}
}
//-----------------------------------------------------------------------------
void ISObjectFormBase::ReRead()
{
	if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.ReReadCard")))
	{
		for (const auto &FieldItem : FieldsMap.toStdMap())
		{
			QString FieldName = FieldItem.first;
			ISFieldEditBase *FieldWidget = FieldItem.second;
			QVariant ValueLocal = FieldWidget->GetValue();

			ISQuery qSelect("SELECT " + MetaTable->GetAlias() + "_" + FieldName + " FROM " + MetaTable->GetName() + " WHERE " + MetaTable->GetAlias() + "_id = :ObjectID");
			qSelect.BindValue(":ObjectID", ObjectID);
			if (qSelect.ExecuteFirst())
			{
				QVariant ValueDB = qSelect.ReadColumn(MetaTable->GetAlias() + "_" + FieldName);
				if (ValueDB != ValueLocal)
				{
					disconnect(FieldWidget, &ISFieldEditBase::DataChanged, this, &ISObjectFormBase::DataChanged);
					FieldWidget->SetValue(ValueDB);
					FieldWidget->SetModificationFlag(false);
					connect(FieldWidget, &ISFieldEditBase::DataChanged, this, &ISObjectFormBase::DataChanged);
				}
			}
		}

		RenameReiconForm();
		BeginFieldEdit->SetFocus();
	}
}
//-----------------------------------------------------------------------------
void ISObjectFormBase::AddAction(QAction *Action, bool AddingToActionGroup)
{
	ToolBar->addAction(Action);

	if (AddingToActionGroup)
	{
		ActionGroup->addAction(Action);
	}
}
//-----------------------------------------------------------------------------
void ISObjectFormBase::SetEnabledActions(bool Enabled)
{
	for (int i = 0; i < ActionGroup->actions().count(); i++)
	{
		ActionGroup->actions().at(i)->setEnabled(Enabled);
	}
}
//-----------------------------------------------------------------------------
QString ISObjectFormBase::GetObjectName() const
{
	QString ObjectName;

	if (MetaTable->GetTitleName().length())
	{
		QString TitleName = MetaTable->GetTitleName();

		QStringList TitleListNames = TitleName.split(";");
		if (TitleListNames.count() > 1) //���� ��� ������� �������� �� ���������� �����
		{
			for (int i = 0; i < TitleListNames.count(); i++)
			{
				QString FieldName = TitleListNames.at(i);
				QString FieldValue = FieldsMap.value(FieldName)->GetValue().toString() + " ";
				ObjectName += FieldValue;
			}

			ISSystem::RemoveLastSymbolFromString(ObjectName);
		}
		else //���� ������� ������ ���� ����
		{
			ObjectName += FieldsMap.value(TitleName)->GetValue().toString();
		}
	}
	else
	{
		ObjectName = QString::number(ObjectID);
	}

	return ObjectName;
}
//-----------------------------------------------------------------------------
ISFieldEditBase* ISObjectFormBase::GetFieldWidget(const QString &FieldName)
{
	return FieldsMap.value(FieldName);
}
//-----------------------------------------------------------------------------
QVariant ISObjectFormBase::GetFieldValue(const QString &FieldName) const
{
	return ObjectModel->GetNewValue(FieldName);
}
//-----------------------------------------------------------------------------
ISTabWidgetObject* ISObjectFormBase::GetTabWidget()
{
	return TabWidgetMain;
}
//-----------------------------------------------------------------------------
QVBoxLayout* ISObjectFormBase::GetLayoutWidgetObject()
{
	return WidgetObjectLayout;
}
//-----------------------------------------------------------------------------
QToolBar* ISObjectFormBase::GetToolBar()
{
	return ToolBar;
}
//-----------------------------------------------------------------------------
