#include "StdAfx.h"
#include "ISMetaUser.h"
#include "ISAssert.h"
#include "EXDefines.h"
#include "ISQuery.h"
#include "ISSystem.h"
//-----------------------------------------------------------------------------
static QString QS_USER = PREPARE_QUERY("SELECT usrs_issystem, usrs_id, usrs_group, usrs_surname, usrs_name, usrs_patronymic, usrs_birthday, usrs_accessallowed, usrs_accountlifetime, usrs_accountlifetimestart, usrs_accountlifetimeend, tpht_uid, tpht_classname, usrs_asterisk_server, usrs_asterisk_port, usrs_asterisk_login, usrs_asterisk_password, usrs_asterisk_pattern, usrs_beeline_pattern "
									   "FROM _users "
									   "LEFT JOIN _telephonytype ON tpht_id = usrs_telephony "
									   "WHERE NOT usrs_isdeleted "
									   "AND usrs_login = :Login");
//-----------------------------------------------------------------------------
static QString QS_USER_GROUP = PREPARE_QUERY("SELECT usgp_name, usgp_fullaccess FROM _usergroup WHERE NOT usgp_isdeleted AND usgp_id = :GroupID");
//-----------------------------------------------------------------------------
static QString QS_EXCLUSION_PROTOCOL_TYPE = PREPARE_QUERY("SELECT ugep_protocoltype FROM _usergroupexclusionprotocol WHERE NOT ugep_isdeleted AND ugep_group = :GroupID");
//-----------------------------------------------------------------------------
static QString QS_USER_PASSWORD = PREPARE_QUERY("SELECT passwd FROM pg_shadow WHERE usename = :Login");
//-----------------------------------------------------------------------------
ISMetaUser::ISMetaUser() : QObject()
{
	UserData = new ISMetaUserData(this);
	ProtocolEnterID = 0;
	Session = ISSystem::GenerateUuid();
}
//-----------------------------------------------------------------------------
ISMetaUser::~ISMetaUser()
{

}
//-----------------------------------------------------------------------------
ISMetaUser& ISMetaUser::GetInstance()
{
	static ISMetaUser UserData;
	return UserData;
}
//-----------------------------------------------------------------------------
void ISMetaUser::Initialize(const QString &login)
{
	UserData->Login = login;

	ISQuery qSelectUser(QS_USER);
	qSelectUser.BindValue(":Login", login);
	if (qSelectUser.ExecuteFirst())
	{
		UserData->System = qSelectUser.ReadColumn("usrs_issystem").toBool();
		UserData->ID = qSelectUser.ReadColumn("usrs_id").toInt();
		UserData->GroupID = qSelectUser.ReadColumn("usrs_group").toInt();
		UserData->Surname = qSelectUser.ReadColumn("usrs_surname").toString();
		UserData->Name = qSelectUser.ReadColumn("usrs_name").toString();
		UserData->Patronymic = qSelectUser.ReadColumn("usrs_patronymic").toString();
		UserData->Birthday = qSelectUser.ReadColumn("usrs_birthday").toDate();
		UserData->FullName = UserData->Surname + " " + UserData->Name + " " + UserData->Patronymic;
		UserData->AccessAllowed = qSelectUser.ReadColumn("usrs_accessallowed").toBool();
		
		UserData->AccountLifetime = qSelectUser.ReadColumn("usrs_accountlifetime").toBool();
		UserData->AccountLifetimeStart = qSelectUser.ReadColumn("usrs_accountlifetimestart").toDate();
		UserData->AccountLifetimeEnd = qSelectUser.ReadColumn("usrs_accountlifetimeend").toDate();
		
		UserData->TelephonyType = qSelectUser.ReadColumn("tpht_uid");
		UserData->TelephonyClass = qSelectUser.ReadColumn("tpht_classname").toString();

		UserData->AsteriskServer = qSelectUser.ReadColumn("usrs_asterisk_server").toString();
		UserData->AsteriskPort = qSelectUser.ReadColumn("usrs_asterisk_port").toInt();
		UserData->AsteriskLogin = qSelectUser.ReadColumn("usrs_asterisk_login").toString();
		UserData->AsteriskPassword = qSelectUser.ReadColumn("usrs_asterisk_password").toString();
		UserData->AsteriskPattern = qSelectUser.ReadColumn("usrs_asterisk_pattern").toInt();

		UserData->BeelinePattern = qSelectUser.ReadColumn("usrs_beeline_pattern").toInt();
	}

	ISQuery qSelectGroup(QS_USER_GROUP);
	qSelectGroup.BindValue(":GroupID", UserData->GroupID);
	if (qSelectGroup.ExecuteFirst())
	{
		UserData->GroupName = qSelectGroup.ReadColumn("usgp_name").toString();
		UserData->GroupFullAccess = qSelectGroup.ReadColumn("usgp_fullaccess").toBool();
	}

	ISQuery qSelectExclusionProtocol(QS_EXCLUSION_PROTOCOL_TYPE);
	qSelectExclusionProtocol.BindValue(":GroupID", UserData->GroupID);
	if (qSelectExclusionProtocol.Execute())
	{
		while (qSelectExclusionProtocol.Next())
		{
			UserData->ExclusionProtocolType.append("{" + qSelectExclusionProtocol.ReadColumn("ugep_protocoltype").toString().toUpper() + "}");
		}
	}
}
//-----------------------------------------------------------------------------
ISMetaUserData* ISMetaUser::GetData()
{
	return UserData;
}
//-----------------------------------------------------------------------------
bool ISMetaUser::CheckPassword(const QString &EnteredPassword)
{
	ISQuery qSelectPassword(QS_USER_PASSWORD);
	qSelectPassword.BindValue(":Login", UserData->Login);
	if (qSelectPassword.ExecuteFirst())
	{
		QString MD5 = qSelectPassword.ReadColumn("passwd").toString();
		MD5 = MD5.remove(0, 3);

		QCryptographicHash CryptographicHash(QCryptographicHash::Md5);
		CryptographicHash.addData(QString(EnteredPassword + UserData->Login).toUtf8());
		QString HashString(CryptographicHash.result().toHex());

		if (MD5 == HashString)
		{
			return true;
		}
	}

	return false;
}
//-----------------------------------------------------------------------------
void ISMetaUser::SetProtocolEnterID(int protocol_enter_id)
{
	ProtocolEnterID = protocol_enter_id;
}
//-----------------------------------------------------------------------------
int ISMetaUser::GetProtocolEnterID() const
{
	return ProtocolEnterID;
}
//-----------------------------------------------------------------------------
QUuid ISMetaUser::GetSessionUID() const
{
	return Session;
}
//-----------------------------------------------------------------------------
