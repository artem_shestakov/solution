#include "StdAfx.h"
#include "ISListEdit.h"
#include "EXDefines.h"
#include "ISListEditPopup.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISQuery.h"
#include "ISMetaData.h"
#include "ISMetaDataHelper.h"
#include "ISSelectObjectListForm.h"
#include "ISControls.h"
#include "ISCore.h"
#include "ISUserRoleEntity.h"
#include "ISSystem.h"
#include "ISStyleSheet.h"
//-----------------------------------------------------------------------------
ISListEdit::ISListEdit(QObject *MetaField, QWidget *parent) : ISFieldEditBase(MetaField, parent)
{
	MetaTable = nullptr;

	ListEditPopup = new ISListEditPopup(this);
	connect(ListEditPopup, &ISListEditPopup::Selected, this, &ISListEdit::SelectedValue);
	connect(ListEditPopup, &ISListEditPopup::Hided, this, &ISListEdit::HidedPopup);

	ButtonMain = new ISPushButton(this);
	ButtonMain->setText(LOCALIZATION("NotSelected"));
	ButtonMain->setToolTip(LOCALIZATION("ClickFromShowList"));
	ButtonMain->setIcon(BUFFER_ICONS("ArrowDown"));
	ButtonMain->setMinimumHeight(SIZE_MINIMUM_HEIGHT_EDIT_FIELD);
	ButtonMain->setCursor(CURSOR_POINTING_HAND);
	ButtonMain->setStyleSheet(ButtonMain->styleSheet() + STYLE_SHEET("QPushButtonListEdit"));
	ButtonMain->setCheckable(true);
	connect(ButtonMain, &ISPushButton::clicked, this, &ISListEdit::ShowPopup);
	AddWidgetEdit(ButtonMain, this);

	ButtonList = new QToolButton(this);
	ButtonList->setMinimumHeight(SIZE_MINIMUM_HEIGHT_EDIT_FIELD);
	ButtonList->setIcon(BUFFER_ICONS("Search"));
	ButtonList->setToolTip(LOCALIZATION("OpenList"));
	ButtonList->setCursor(CURSOR_POINTING_HAND);
	ButtonList->setAutoRaise(true);
	connect(ButtonList, &QToolButton::clicked, this, &ISListEdit::ShowListForm);
	AddWidgetToRight(ButtonList);

	if (MetaField)
	{
		QObject *MetaForeign = qvariant_cast<QObject*>(MetaField->property("Foreign"));
		ListEditPopup->SetMetaForeign(MetaForeign);
		MetaTable = ISMetaData::GetInstanse().GetMetaTable(MetaForeign->property("ForeignClass").toString());
	}

	if (MetaTable->GetShowOnly())
	{
		ButtonList->setPopupMode(QToolButton::DelayedPopup);

		ActionCreate = nullptr;
		ActionEdit = nullptr;
	}
	else
	{
		bool AviableCreate = ISUserRoleEntity::GetInstance().CheckPermission(MetaTable->GetUID(), ISNamespace::PermissionsType::PT_Create);
		ActionCreate = ISControls::CreateActionCreate(ButtonList->menu());
		ActionCreate->setVisible(AviableCreate);
		connect(ActionCreate, &QAction::triggered, this, &ISListEdit::CreateObject);

		bool AviableEdit = ISUserRoleEntity::GetInstance().CheckPermission(MetaTable->GetUID(), ISNamespace::PermissionsType::PT_Edit);
		ActionEdit = ISControls::CreateActionEdit(ButtonList->menu());
		ActionEdit->setEnabled(false);
		ActionEdit->setVisible(AviableEdit);
		connect(ActionEdit, &QAction::triggered, this, &ISListEdit::EditObject);

		if (AviableCreate || AviableEdit)
		{
			QMenu *Menu = new QMenu(ButtonList);
			ButtonList->setMenu(Menu);
			ButtonList->setPopupMode(QToolButton::MenuButtonPopup);

			Menu->addAction(ActionCreate);
			Menu->addAction(ActionEdit);
		}
	}
}
//-----------------------------------------------------------------------------
ISListEdit::ISListEdit(QWidget *parent) : ISListEdit(nullptr, parent)
{

}
//-----------------------------------------------------------------------------
ISListEdit::~ISListEdit()
{

}
//-----------------------------------------------------------------------------
void ISListEdit::SetValue(const QVariant &value)
{
	ID = value;

	if (!value.isValid())
	{
		Clear();
		return;
	}

	PMetaClassField *MetaField = dynamic_cast<PMetaClassField*>(GetMetaField());
	PMetaClassForeign *MetaForeign = MetaField->GetForeign();
	QString SqlText = ISMetaDataHelper::GenerateSqlQueryFromForeign(MetaForeign, QString(), ID);
	
	ISQuery qSelect(SqlText);
	qSelect.BindValue(":ObjectID", ID);
	if (qSelect.ExecuteFirst())
	{
		QString Name = qSelect.ReadColumn(1).toString();
		ButtonMain->setText(Name);
		ValueChanged();
	}

	if (ActionEdit)
	{
		ActionEdit->setEnabled(true);
	}
}
//-----------------------------------------------------------------------------
QVariant ISListEdit::GetValue() const
{
	return ID;
}
//-----------------------------------------------------------------------------
void ISListEdit::Clear()
{
	ID.clear();
	ButtonMain->setText(LOCALIZATION("NotSelected"));
	ValueChanged();
	
	if (ActionEdit)
	{
		ActionEdit->setEnabled(false);
	}
}
//-----------------------------------------------------------------------------
QString ISListEdit::GetCurrentText() const
{
	return ButtonMain->text();
}
//-----------------------------------------------------------------------------
void ISListEdit::SetEnabled(bool Enabled)
{
	ButtonMain->setEnabled(Enabled);
	ButtonList->setEnabled(Enabled);
	SetEnabledClear(Enabled);
}
//-----------------------------------------------------------------------------
void ISListEdit::SetSqlFilter(const QString &sql_filter)
{
	ListEditPopup->SetSqlFilter(sql_filter);
}
//-----------------------------------------------------------------------------
void ISListEdit::ClearSqlFilter()
{
	ListEditPopup->ClearSqlFilter();
}
//-----------------------------------------------------------------------------
void ISListEdit::SetReadOnly(bool read_only)
{
	ButtonMain->setEnabled(!read_only);
}
//-----------------------------------------------------------------------------
void ISListEdit::SelectedValue(const QVariant &id, const QString &text)
{
	ID = id;
	ButtonMain->setText(text);
	ValueChanged();

	if (ActionEdit)
	{
		ActionEdit->setEnabled(true);
	}
}
//-----------------------------------------------------------------------------
void ISListEdit::ShowPopup()
{
	ButtonMain->setIcon(BUFFER_ICONS("ArrowUp"));
	ButtonMain->setChecked(true);

	QPoint Point = ButtonMain->mapToGlobal(QPoint());
	Point.setY(Point.y() + ButtonMain->height() - 1);

	ListEditPopup->move(Point);
	ListEditPopup->SetCurrentValue(ID);
	ListEditPopup->adjustSize();
	ListEditPopup->resize(ButtonMain->width(), ListEditPopup->height());
	ListEditPopup->show();
}
//-----------------------------------------------------------------------------
void ISListEdit::HidedPopup()
{
	ButtonMain->setIcon(BUFFER_ICONS("ArrowDown"));
	ButtonMain->setChecked(false);
}
//-----------------------------------------------------------------------------
void ISListEdit::ShowListForm()
{
	ISSystem::SetWaitGlobalCursor(true);

	ISSelectObjectListForm *SelectListForm = new ISSelectObjectListForm(MetaTable);
	SelectListForm->SetSqlFilter(ListEditPopup->GetSqlFilter());
	connect(SelectListForm, &ISSelectObjectListForm::Selected, this, &ISListEdit::SetValue);
	SelectListForm->show();

	int CurrentObjectID = GetValue().toInt();
	if (CurrentObjectID)
	{
		SelectListForm->SetSelectObjectAfterUpdate(CurrentObjectID);
	}

	ISSystem::SetWaitGlobalCursor(false);
	SelectListForm->LoadData();
}
//-----------------------------------------------------------------------------
void ISListEdit::CreateObject()
{
	ISCore::CreateObjectForm(ISNamespace::OFT_New, MetaTable)->show();
}
//-----------------------------------------------------------------------------
void ISListEdit::EditObject()
{
	ISCore::CreateObjectForm(ISNamespace::OFT_Edit, MetaTable, GetValue().toInt())->show();
}
//-----------------------------------------------------------------------------
