#include "StdAfx.h"
#include "ISSelectObjectListForm.h"
#include "EXDefines.h"
#include "ISAssert.h"
//-----------------------------------------------------------------------------
ISSelectObjectListForm::ISSelectObjectListForm(PMetaClassTable *MetaTable, QWidget *parent) : ISSelectBaseListForm(MetaTable, parent)
{
	GetAction(ISNamespace::AT_Select)->setVisible(true);
}
//-----------------------------------------------------------------------------
ISSelectObjectListForm::~ISSelectObjectListForm()
{

}
//-----------------------------------------------------------------------------
void ISSelectObjectListForm::Select()
{
	int SelectedObjectID = GetObjectID();
	IS_ASSERT(SelectedObjectID, "Invalid ObjectID");

	emit Selected(SelectedObjectID);
	close();
}
//-----------------------------------------------------------------------------
