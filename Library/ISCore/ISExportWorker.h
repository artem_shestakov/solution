#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
#include "ISSqlModelCore.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISExportWorker : public QObject
{
	Q_OBJECT

signals:
	void ExportedRow();
	void Message(const QString &Message);

public:
	ISExportWorker(QObject *parent = 0);
	virtual ~ISExportWorker();

	virtual bool Prepare() = 0; //���������� ��������
	virtual bool Export() = 0; //�������

	void Cancel(); //���������

	void SetTableName(const QString &LocalName);
	void SetModel(ISSqlModelCore *model);
	void SetFields(const QList<QString> &fields);
	void SetHeader(bool header);
	void SetSelectedRows(const QVector<int> &selected_rows);

	QString GetErrorString() const;

protected:
	QVariant PrepareValue(const QVariant &Value) const;

protected:
	QString ErrorString;
	QString TableName;
	ISSqlModelCore *Model;
	QList<QString> Fields;
	bool Header;
	QVector<int> SelectedRows;
	bool Canceled;
};
//-----------------------------------------------------------------------------
