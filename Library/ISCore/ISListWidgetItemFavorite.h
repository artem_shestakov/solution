#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISListWidgetItemFavorite : public QObject, public QListWidgetItem
{
	Q_OBJECT

public:
	ISListWidgetItemFavorite(QListWidget *ListWidget);
	virtual ~ISListWidgetItemFavorite();

	void SetTableName(const QString &table_name);
	QString GetTableName() const;

	void SetObjectID(int object_id);
	int GetObjectID() const;

	void SetFavoriteID(int favorite_id);
	int GetFavoriteID() const;

private:
	QString TableName;
	int ObjectID;
	int FavoriteID;
};
//-----------------------------------------------------------------------------
