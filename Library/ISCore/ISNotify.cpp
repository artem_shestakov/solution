#include "StdAfx.h"
#include "ISNotify.h"
#include "EXDefines.h"
#include "ISAssert.h"
#include "ISQuery.h"
#include "ISDatabase.h"
#include "ISMetaUser.h"
#include "ISNotificationService.h"
#include "ISBuffer.h"
#include "ISQueryPool.h"
#include "ISSystem.h"
//-----------------------------------------------------------------------------
static QString QS_NOTIFICATIONS = PREPARE_QUERY("SELECT ntfn_uid, ntfn_name, ntfn_message, ntfn_soundfilename, ntfn_signalname FROM _notification WHERE NOT ntfn_isdeleted ORDER BY ntfn_id");
//-----------------------------------------------------------------------------
static QString QS_NOTIFICATION = PREPARE_QUERY("SELECT pg_notify(:NotificationUID, :Payload)");
//-----------------------------------------------------------------------------
static QString QI_NOTIFICATION_USER = PREPARE_QUERY("INSERT INTO _notificationuser(ntfu_notification, ntfu_userfrom, ntfu_userto, ntfu_payload, ntfu_saved) "
													"VALUES(:NotificationUID, :UserFrom, :UserTo, :Payload, :Saved)");
//-----------------------------------------------------------------------------
ISNotify::ISNotify() : QObject()
{
	Sound = nullptr;

	SqlDriver = ISDatabase::GetInstance().GetDefaultDB().driver();
	connect(SqlDriver, static_cast<void(QSqlDriver::*)(const QString &, QSqlDriver::NotificationSource, const QVariant &)>(&QSqlDriver::notification), this, &ISNotify::Notification);

	IS_ASSERT(SqlDriver->subscribeToNotification(CURRENT_USER_LOGIN), SqlDriver->lastError().text()); //����������� � �����������
}
//-----------------------------------------------------------------------------
ISNotify::~ISNotify()
{

}
//-----------------------------------------------------------------------------
ISNotify& ISNotify::GetInstance()
{
	static ISNotify Notify;
	return Notify;
}
//-----------------------------------------------------------------------------
void ISNotify::Initialize()
{
	ISQuery qSelect(QS_NOTIFICATIONS);
	if (qSelect.Execute())
	{
		while (qSelect.Next())
		{
			ISUuid UID = qSelect.ReadColumn("ntfn_uid");
			QString Name = qSelect.ReadColumn("ntfn_name").toString();
			QString Message = qSelect.ReadColumn("ntfn_message").toString();
			QString SoundFileName = qSelect.ReadColumn("ntfn_soundfilename").toString();
			QString SignalName = qSelect.ReadColumn("ntfn_signalname").toString();

			ISMetaNotify *MetaNotify = new ISMetaNotify(this);
			MetaNotify->SetUID(UID);
			MetaNotify->SetName(Name);
			MetaNotify->SetMessage(Message);
			MetaNotify->SetSoundFileName(SoundFileName);
			MetaNotify->SetSignalName(SignalName);
			Notifications.insert(UID, MetaNotify);
		}
	}
}
//-----------------------------------------------------------------------------
ISMetaNotify* ISNotify::GetMetaNotify(const QString &NotificationUID)
{
	ISMetaNotify *MetaNotify = Notifications.value(NotificationUID);
	IS_ASSERT(MetaNotify, QString("Not found notification from uid: %1").arg(NotificationUID));
	return MetaNotify;
}
//-----------------------------------------------------------------------------
void ISNotify::SendNotification(const QString &NotificationUID, const QVariant &Payload, bool Saved, int UserTo)
{
	IS_ASSERT(UserTo >= 0, "Invalid UserTo");

	if (UserTo != CURRENT_USER_ID) //���� ����������� ������������ �� ������ ����
	{
		QVariantMap VariantMap;
		VariantMap.insert(":NotificationUID", NotificationUID);
		VariantMap.insert(":UserFrom", CURRENT_USER_ID); //��� �������� �����������
		VariantMap.insert(":UserTo", UserTo); //���� ���������� �����������
		VariantMap.insert(":Payload", Payload);
		VariantMap.insert(":Saved", Saved);
		ISQueryPool::GetInstance().AddQuery(QI_NOTIFICATION_USER, VariantMap);
	}
}
//-----------------------------------------------------------------------------
void ISNotify::Notification(const QString &NotificationName, QSqlDriver::NotificationSource Source, const QVariant &Payload)
{
	if (Sound)
	{
		delete Sound;
		Sound = nullptr;
	}

	QVariantMap VariantMap = ISSystem::JsonStringToVariantMap(Payload.toString());
	QString NotificationUID = VariantMap.value("NotificationUID").toString();
	QString PayloadString = VariantMap.value("Payload").toString();
	int ID = VariantMap.value("ID").toInt();
	QDateTime DateTime = VariantMap.value("DateTime").toDateTime();

	ISMetaNotify *MetaNotify = GetMetaNotify(NotificationUID);

	if (MetaNotify->GetSoundFileName().length()) //���� � ����������� ������ �������� ������
	{
		Sound = new QSound(BUFFER_AUDIO(MetaNotify->GetSoundFileName()), this);
		Sound->play();
	}

	ISNotificationService::ShowNotification(MetaNotify->GetMessage());

	QString SignalName = MetaNotify->GetSignalName();
	if (SignalName.length()) //���� ������ ������������ ������ ��� �����������
	{
		bool Invoked = QMetaObject::invokeMethod(this, MetaNotify->GetSignalName().toUtf8().data(), Q_ARG(const QVariantMap &, VariantMap));
		IS_ASSERT(Invoked, QString("Not invoke method: %1").arg(MetaNotify->GetSignalName()));
	}
	else //������������ ������ ��� ����������� �� ������ - ��������� ����� �������
	{
		emit Notify(VariantMap);
		emit Notify();
	}
}
//-----------------------------------------------------------------------------
