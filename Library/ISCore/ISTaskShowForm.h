#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
#include "ISInterfaceForm.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISTaskShowForm : public ISInterfaceForm
{
	Q_OBJECT

public:
	ISTaskShowForm(int task_id, QWidget *parent = 0);
	virtual ~ISTaskShowForm();

protected:
	void EscapeClicked();

private:
	int TaskID;
};
//-----------------------------------------------------------------------------
