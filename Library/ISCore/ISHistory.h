#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISHistory : public QObject
{
	Q_OBJECT

public:
	ISHistory();
	virtual ~ISHistory();

	static void AddHistory(const QString &TableName, const QString &LocalListName, const QString &ObjectName, int ObjectID); //�������� ������ � �������
	static void ClearHistory(); //�������� �������
};
//-----------------------------------------------------------------------------
