#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
#include "ISInterfaceForm.h"
//-----------------------------------------------------------------------------
class ISCORE_EXPORT ISProcessForm : public ISInterfaceForm
{
	Q_OBJECT

public:
	ISProcessForm(QString &Text = QString(), QWidget *parent = 0);
	virtual ~ISProcessForm();

	void showEvent(QShowEvent *e);
	void SetText(const QString &Text);

private:
	QLabel *LabelText;
};
//-----------------------------------------------------------------------------
