#include "StdAfx.h"
#include "ISSettings.h"
#include "ISQuery.h"
#include "ISMetaData.h"
#include "ISSettingsHelper.h"
#include "ISMetaUser.h"
#include "ISAssert.h"
#include "EXDefines.h"
#include "ISCountingTime.h"
#include "ISDebug.h"
#include "ISSystem.h"
//-----------------------------------------------------------------------------
static QString QS_SETTINGS_GROUPS = PREPARE_QUERY("SELECT stgp_uid, stgp_name, stgp_localname, stgp_iconname, stgp_hint FROM _settingsgroup WHERE NOT stgp_isdeleted ORDER BY stgp_order");
//-----------------------------------------------------------------------------
static QString QS_SETTINGS = PREPARE_QUERY("SELECT stgs_uid, stgs_name, stgs_type, stgs_widgeteditname, stgs_localname, stgs_hint, stgs_defaultvalue FROM _settings WHERE NOT stgs_isdeleted AND stgs_group = :GroupUID ORDER BY stgs_order");
//-----------------------------------------------------------------------------
ISSettings::ISSettings()
{
	CountAllSettings = 0;
	Initialize();
}
//-----------------------------------------------------------------------------
ISSettings::~ISSettings()
{

}
//-----------------------------------------------------------------------------
ISSettings& ISSettings::GetInstance()
{
	static ISSettings Settings;
	return Settings;
}
//-----------------------------------------------------------------------------
bool ISSettings::GetValueBool(const QString &SettingUID)
{
	bool BoolValue = GetValue(SettingUID).toBool();
	return BoolValue;
}
//-----------------------------------------------------------------------------
QString ISSettings::GetValueString(const QString &SettingUID)
{
	QString StringValue = GetValue(SettingUID).toString();
	return StringValue;
}
//-----------------------------------------------------------------------------
int ISSettings::GetValueInt(const QString &SettingUID)
{
	int IntValue = GetValue(SettingUID).toInt();
	return IntValue;
}
//-----------------------------------------------------------------------------
QVariant ISSettings::GetValue(const QString &SettingUID)
{
	return GetMetaSetting(SettingUID)->GetValue();
}
//-----------------------------------------------------------------------------
void ISSettings::SetValue(const QString &SettingUID, const QVariant &Value)
{
	ISMetaSetting *MetaSetting = GetMetaSetting(SettingUID);
	MetaSetting->SetValue(Value);
}
//-----------------------------------------------------------------------------
QVector<ISMetaSettingsGroup*> ISSettings::GetSettingGroups()
{
	return SettingGroups;
}
//-----------------------------------------------------------------------------
ISMetaSetting* ISSettings::GetMetaSetting(const QString &SettingUID)
{
	for (int i = 0; i < SettingGroups.count(); i++)
	{
		ISMetaSettingsGroup *MetaGroup = SettingGroups.at(i);

		for (int j = 0; j < MetaGroup->GetSettings().count(); j++)
		{
			ISMetaSetting *MetaSetting = MetaGroup->GetSettings()[j];
			if (MetaSetting->GetUID() == SettingUID)
			{
				return MetaSetting;
			}
		}
	}

	IS_ASSERT(false, QString("Not found setting \"%1\"").arg(SettingUID));
	return nullptr;
}
//-----------------------------------------------------------------------------
void ISSettings::Initialize()
{
	ISCountingTime Time;

	ISQuery qSelectGroups(QS_SETTINGS_GROUPS);
	if (qSelectGroups.Execute())
	{
		while (qSelectGroups.Next()) //����� ����� ��������
		{
			ISUuid GroupUID = qSelectGroups.ReadColumn("stgp_uid");
			QString GroupName = qSelectGroups.ReadColumn("stgp_name").toString();
			QString GroupLocalName = qSelectGroups.ReadColumn("stgp_localname").toString();
			QString GroupIconName = qSelectGroups.ReadColumn("stgp_iconname").toString();
			QString GroupHint = qSelectGroups.ReadColumn("stgp_hint").toString();

			ISMetaSettingsGroup *MetaGroup = new ISMetaSettingsGroup(this);
			MetaGroup->SetUID(GroupUID);
			MetaGroup->SetName(GroupName);
			MetaGroup->SetLocalName(GroupLocalName);
			MetaGroup->SetIconName(GroupIconName);
			MetaGroup->SetHint(GroupHint);

			ISQuery qSelectSettings(QS_SETTINGS);
			qSelectSettings.BindValue(":GroupUID", GroupUID);
			if (qSelectSettings.Execute())
			{
				while (qSelectSettings.Next()) //����� �������� ������� ������
				{
					ISUuid SettingUID = qSelectSettings.ReadColumn("stgs_uid");
					QString SettingName = qSelectSettings.ReadColumn("stgs_name").toString();
					ISNamespace::FieldType SettingType = ISMetaData::GetInstanse().GetAssociationTypes().GetTypeField(qSelectSettings.ReadColumn("stgs_type").toString());
					QString SettingWidgetEditName = qSelectSettings.ReadColumn("stgs_widgeteditname").toString();
					QString SettingLocalName = qSelectSettings.ReadColumn("stgs_localname").toString();
					QString SettingHint = qSelectSettings.ReadColumn("stgs_hint").toString();
					QVariant SettingDefaultValue = qSelectSettings.ReadColumn("stgs_defaultvalue");

					ISMetaSetting *Setting = new ISMetaSetting(MetaGroup);
					Setting->SetUID(SettingUID);
					Setting->SetName(SettingName);
					Setting->SetType(SettingType);
					Setting->SetWidgetEditName(SettingWidgetEditName);
					Setting->SetLocalName(SettingLocalName);
					Setting->SetHint(SettingHint);
					Setting->SetDefaultValue(SettingDefaultValue);
					MetaGroup->AppendSetting(Setting);

					if (!ISSettingsHelper::CheckExistSettng(SettingUID)) //���� ����� ��������� ��� � ������������ - ��������
					{
						ISSettingsHelper::InsertSetting(SettingUID, SettingDefaultValue);
						Setting->SetValue(SettingDefaultValue);
					}
					else
					{
						QVariant UserValue = ISSettingsHelper::GetUserValue(SettingUID);
						Setting->SetValue(UserValue);
					}

					CountAllSettings++;
				}
			}

			SettingGroups.append(MetaGroup);
		}
	}

	QString TimeInitialized = ISSystem::MillisecondsToString(Time.GetElapsed());
	ISDebug::ShowDebugString("Initialize Settings time: " + TimeInitialized);
}
//-----------------------------------------------------------------------------
