#include "StdAfx.h"
#include "ISPageNavigation.h"
#include "EXDefines.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISInputDialog.h"
//-----------------------------------------------------------------------------
ISPageNavigation::ISPageNavigation(QWidget *parent) : QWidget(parent)
{
	RowCount = -1;
	PageCount = 0;
	CurrentPage = 0;
	Limit = 0;
	Offset = 0;

	QHBoxLayout *Layout = new QHBoxLayout();
	Layout->setContentsMargins(LAYOUT_MARGINS_NULL);
	setLayout(Layout);

	ButtonBegin = new ISServiceButton(this);
	ButtonBegin->setEnabled(false);
	ButtonBegin->setToolTip(LOCALIZATION("Page.Begin"));
	ButtonBegin->setIcon(BUFFER_ICONS("PageNavigation.Begin"));
	ButtonBegin->setFlat(true);
	ButtonBegin->setFixedSize(SIZE_22_22);
	ButtonBegin->setCursor(CURSOR_POINTING_HAND);
	connect(ButtonBegin, &QToolButton::clicked, this, &ISPageNavigation::BeginClicked);
	Layout->addWidget(ButtonBegin);

	ButtonPrevious = new ISServiceButton(this);
	ButtonPrevious->setEnabled(false);
	ButtonPrevious->setToolTip(LOCALIZATION("Page.Previous"));
	ButtonPrevious->setIcon(BUFFER_ICONS("PageNavigation.Previous"));
	ButtonPrevious->setFlat(true);
	ButtonPrevious->setFixedSize(SIZE_22_22);
	ButtonPrevious->setCursor(CURSOR_POINTING_HAND);
	connect(ButtonPrevious, &QToolButton::clicked, this, &ISPageNavigation::PreviousClicked);
	Layout->addWidget(ButtonPrevious);

	ButtonSelect = new ISPushButton(this);
	ButtonSelect->setText(LOCALIZATION("Page.Select").arg(0).arg(0));
	ButtonSelect->setToolTip(LOCALIZATION("ClickFromSelectNumberPage"));
	ButtonSelect->setCursor(CURSOR_POINTING_HAND);
	connect(ButtonSelect, &QToolButton::clicked, this, &ISPageNavigation::Select);
	Layout->addWidget(ButtonSelect);

	ButtonNext = new ISServiceButton(this);
	ButtonNext->setToolTip(LOCALIZATION("Page.Next"));
	ButtonNext->setIcon(BUFFER_ICONS("PageNavigation.Next"));
	ButtonNext->setFlat(true);
	ButtonNext->setFixedSize(SIZE_22_22);
	ButtonNext->setCursor(CURSOR_POINTING_HAND);
	connect(ButtonNext, &QToolButton::clicked, this, &ISPageNavigation::NextClicked);
	Layout->addWidget(ButtonNext);

	ButtonEnd = new ISServiceButton(this);
	ButtonEnd->setToolTip(LOCALIZATION("Page.End"));
	ButtonEnd->setIcon(BUFFER_ICONS("PageNavigation.End"));
	ButtonEnd->setFlat(true);
	ButtonEnd->setFixedSize(SIZE_22_22);
	ButtonEnd->setCursor(CURSOR_POINTING_HAND);
	connect(ButtonEnd, &QToolButton::clicked, this, &ISPageNavigation::EndClicked);
	Layout->addWidget(ButtonEnd);
}
//-----------------------------------------------------------------------------
ISPageNavigation::~ISPageNavigation()
{

}
//-----------------------------------------------------------------------------
void ISPageNavigation::SetRowCount(int row_count)
{
	if (row_count)
	{
		RowCount = row_count;

		PageCount = RowCount / Limit;
		if (row_count % Limit)
		{
			PageCount++;
		}

		ButtonSelect->setText(LOCALIZATION("Page.Select").arg(CurrentPage + 1).arg(PageCount));

		if (PageCount == 1) //���� ���������� ������� ����
		{
			setEnabled(false);
		}
		else
		{
			setEnabled(true);
		}
	}
}
//-----------------------------------------------------------------------------
void ISPageNavigation::SetLimit(int limit)
{
	Limit = limit;
}
//-----------------------------------------------------------------------------
void ISPageNavigation::BeginClicked()
{
	CurrentPage = 0;
	Offset = 0;
	emit OffsetSignal(Offset);
	
	PageChanged();
}
//-----------------------------------------------------------------------------
void ISPageNavigation::PreviousClicked()
{
	CurrentPage--;

	int NewOffset = Offset - Limit;
	Offset = NewOffset;
	emit OffsetSignal(NewOffset);

	PageChanged();
}
//-----------------------------------------------------------------------------
void ISPageNavigation::Select()
{
	int Page = ISInputDialog::GetInteger(this, LOCALIZATION("Page"), LOCALIZATION("InputNumberPage"), 1, PageCount, CurrentPage + 1).toInt();
	if (Page)
	{
		CurrentPage = Page - 1;
		Offset = CurrentPage * Limit;
		emit OffsetSignal(Offset);

		PageChanged();
	}
}
//-----------------------------------------------------------------------------
void ISPageNavigation::NextClicked()
{
	CurrentPage++;

	int NewOffset = Offset + Limit;
	Offset = NewOffset;
	emit OffsetSignal(NewOffset);

	PageChanged();
}
//-----------------------------------------------------------------------------
void ISPageNavigation::EndClicked()
{
	CurrentPage = PageCount;
	CurrentPage--;
	Offset = CurrentPage * Limit;
	emit OffsetSignal(Offset);

	PageChanged();
}
//-----------------------------------------------------------------------------
void ISPageNavigation::PageChanged()
{
	if (CurrentPage + 1 > 1 && CurrentPage + 1 < PageCount) //���� ������� �������� �� ������ � �� ���������
	{
		ButtonBegin->setEnabled(true);
		ButtonPrevious->setEnabled(true);
		ButtonNext->setEnabled(true);
		ButtonEnd->setEnabled(true);
	}
	else if (CurrentPage + 1 == 1) //���� ������� �������� ������
	{
		ButtonBegin->setEnabled(false);
		ButtonPrevious->setEnabled(false);
		ButtonNext->setEnabled(true);
		ButtonEnd->setEnabled(true);
	}
	else if (CurrentPage + 1 == PageCount) //���� ������� �������� ���������
	{
		ButtonBegin->setEnabled(true);
		ButtonPrevious->setEnabled(true);
		ButtonNext->setEnabled(false);
		ButtonEnd->setEnabled(false);
	}

	emit Update();
}
//-----------------------------------------------------------------------------
