#pragma once
//-----------------------------------------------------------------------------
#include "iscore_global.h"
#include "ISInterfaceForm.h"
#include "ISNamespace.h"
#include "PMetaClassTable.h"
#include "ISToolBarObject.h"
#include "ISFieldEditBase.h"
#include "ISLineEdit.h"
#include "ISInterfaceMetaForm.h"
#include "ISScrollArea.h"
#include "ISObjectModel.h"
#include "ISObjectSearchDuplicates.h"
#include "ISTabWidgetObject.h"
#include "ISSystemInfoObject.h"
//-----------------------------------------------------------------------------
//!������� ����� ����� �������
class ISCORE_EXPORT ISObjectFormBase : public ISInterfaceForm
{
	Q_OBJECT

signals:
	void SavedObject(int ObjectID);
	void UpdateList();
	void CurrentObjectTab(); //������ ��� ������� (������������ ������� �������)
	void Close();

public:
	ISObjectFormBase(ISNamespace::ObjectFormType form_type, PMetaClassTable *meta_table, QWidget *parent, int object_id = 0);
	virtual ~ISObjectFormBase();

	int GetParentObjectID() const;
	void SetParentObjectID(int parent_object_id);

	ISNamespace::ObjectFormType GetFormType(); //�������� ��� �����
	int GetObjectID() const; //�������� ������������� �������
	PMetaClassTable* GetMetaTable(); //�������� ����-�������
	bool GetModificationFlag() const; //�������� ���� ����������� ������ 

	void SetVisibleNavigationBar(bool Visible); //�������� ��������� � ������� ��������
	void SetVisibleFieldID(bool Visible); //������� ��������� � ���� "���"
	void SetVisibleField(const QString &FieldName, bool Visible); //�������� ��������� � ����
	void SetVisibleSearchPanel(bool Visible); //�������� ��������� � ��������� ������
	void SetVisibleFavorites(bool Visible); //�������� ��������� ������ "���������"
	void SetVisibleDelete(bool Visible); //�������� ��������� ������ "�������"
	void SetVisibleSearch(bool Visible); //�������� ��������� ������ "�����"
	void SetVisibleReRead(bool Visible); //�������� ��������� ������ "���������� ��������"

protected:
	virtual void closeEvent(QCloseEvent *e);
	virtual void keyPressEvent(QKeyEvent *e);
	
	void EscapeClicked() override;
	void AfterShowEvent() override;

protected:
	void CreateToolBarEscorts(); //�������� �������� �������
	void CreateMainTabWidget(); //�������� �������� ����
	void CreateSearchDuplicates(); //�������� ������ ������ ����������
	void CreateToolBar(); //�������� ������ ���������� ��������
	void CreateFieldsWidget(); //�������� ����� �������������� ����������
	void FillDataFields(); //���������� ����� �������

	void CreateSystemInfoWidget(QFormLayout *FormLayout); //�������� ������� � ��������� ����������
	ISFieldEditBase* CreateColumnForField(PMetaClassField *MetaField); //������� ���� �������������� ����������
	void AddColumnForField(const QString &LabelName, ISFieldEditBase *FieldEditBase, QFormLayout *FormLayout); //�������� ���� �������������� ���������� �� �����
	void AddObjectEscort(QWidget *ObjectForm);

	void ToolBarClicked(QAction *ActionClicked); //������� ������� �� �������� � �������

protected:
	void SaveClose(); //��������� � �������
	virtual bool Save(); //���������
	virtual void SaveEvent(); //������� ��� ���������� �������
	void RenameReiconForm(); //���������������� �����
	void DataChanged(); //������ ��� ��������� �������� ������ �� �����
	void SetModificationFlag(bool modification); //�������� ���� ����������� ������
	void UpdateObjectActions(); //���������� ��������� �������� �������
	void SearchDuplicates(const QString &SqlText); //����� ���������
	void AddFavoite(); //�������� � ���������
	void Delete();
	void CancelChanged(); //������ ���������
	void ReRead(); //����������

	void AddAction(QAction *Action, bool AddingToActionGroup = true); //�������� ������-�������� �� ������
	void SetEnabledActions(bool Enabled); //��������� ����������� ������ ��������

	QString GetObjectName() const; //�������� ��� �������
	ISFieldEditBase* GetFieldWidget(const QString &FieldName); //������� ������ �������������� �� ��� �����
	QVariant GetFieldValue(const QString &FieldName) const; //�������� �������� ����
	ISTabWidgetObject* GetTabWidget(); //�������� ��������� �� ���-������
	QVBoxLayout* GetLayoutWidgetObject(); //�������� ��������� �� ����������� �������
	QToolBar* GetToolBar(); //�������� ��������� �� ������ ������-��������

private:
	ISNamespace::ObjectFormType FormType; //��� �����
	PMetaClassTable *MetaTable; //����-�������
	int ObjectID; //������������� �������� �������
	int ParentObjectID; //������������� ��������

	ISToolBarObject *ToolBarNavigation; //������ ��������
	ISTabWidgetObject *TabWidgetMain; //������� ���
	QStackedWidget *StackedWidget;
	QWidget *WidgetObject; //������ �������
	QVBoxLayout *WidgetObjectLayout;
	QWidget *WidgetTabEscort;
	ISInterfaceMetaForm *WidgetEscort;
	ISObjectSearchDuplicates *SearchDuplicatesPanel;
	QToolBar *ToolBar;
	QActionGroup *ActionGroup;

	QAction *ActionSaveClose;
	QAction *ActionSave;
	QAction *ActionClose;
	QAction *ActionSearchDuplicates;
	QAction *ActionFavorites;
	QAction *ActionDelete;
	QAction *ActionCancelChange;
	QAction *ActionReRead;

	QLabel *LabelSystemInfoObject;
	ISSystemInfoObject *SystemInfoObject;

	ISFieldEditBase *BeginFieldEdit; //��������� �� ������ ���� �������������� ����������
	QMap<QString, ISFieldEditBase *> FieldsMap;
	QMap<QString, QLabel*> LabelsMap;
	QMap<QString, QHBoxLayout*> Layouts;

	ISObjectModel *ObjectModel;
	bool ModificationFlag;
};
//-----------------------------------------------------------------------------
