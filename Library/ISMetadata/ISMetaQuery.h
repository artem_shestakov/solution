#pragma once
//-----------------------------------------------------------------------------
#include "ismetadata_global.h"
//-----------------------------------------------------------------------------
class ISMETADATA_EXPORT ISMetaQuery : public QObject
{
	Q_OBJECT

public:
	ISMetaQuery();
	virtual ~ISMetaQuery();

	static QString GetQueryText(const QString &QueryName, const QString &ClassFilter = QString()); //�������� ����� ����-�������
};
//-----------------------------------------------------------------------------
