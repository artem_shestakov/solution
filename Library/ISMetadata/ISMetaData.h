#pragma once
//-----------------------------------------------------------------------------
#include "ismetadata_global.h"
#include "PMetaClassTable.h"
#include "PMetaClassQuery.h"
#include "PMetaClassResource.h"
#include "PMetaClassFieldBox.h"
#include "PMetaClassQueryField.h"
#include "ISAssociationTypes.h"
//-----------------------------------------------------------------------------
class ISMETADATA_EXPORT ISMetaData : public QObject
{
	Q_OBJECT

public:
	ISMetaData(const ISMetaData &) = delete;
	ISMetaData(ISMetaData &&) = delete;
	ISMetaData &operator=(const ISMetaData &) = delete;
	ISMetaData &operator=(ISMetaData &&) = delete;
	~ISMetaData();

	static ISMetaData& GetInstanse();

	bool GetInitialized() const;
	void Initialize(bool InitXSR); //�������������

	PMetaClassTable* GetMetaTable(const QString &TableName); //�������� ����-������� �� �����
	PMetaClassTable* GetMetaQuery(const QString &QueryName); //�������� ����-������ �� �����
	PMetaClassField* GetMetaField(PMetaClassTable *MetaTable, const QString &FieldName); //�������� ����-���� �� ��������� �������
	PMetaClassField* GetMetaField(const QString &TableName, const QString &FieldName); //�������� ����-���� �� ��������� �������

	QList<PMetaClassTable*> GetTables(); //�������� ������ ���� ������
	QVector<QString> GetMetaQueries(); //�������� ������ ���� ����-��������
	QVector<PMetaClassIndex*> GetSystemIndexes(); //�������� ������ ��������� ��������
	QVector<PMetaClassIndex*> GetIndexes(); //�������� ������ �������� ��� ���������������� �����
	QVector<PMetaClassIndex*> GetCompoundIndexes(); //�������� ������ ��������� ��������
	QVector<PMetaClassForeign*> GetForeigns(); //�������� ������ ������� ������
	QVector<PMetaClassResource*> GetResources(); //�������� �������

	bool CheckExitField(PMetaClassTable *MetaTable, const QString &FieldName) const; //��������� ������� ���������� ���� � ��������� �������
	ISAssociationTypes& GetAssociationTypes(); //�������� ����������

protected:
	void CheckUniqueAllIdentifiers(); //�������� ������������ ���� ���������������
	void CheckUniqueAllAliases(); //�������� ������������ ���� ����������� ������
	void GenerateSqlFromForeigns(); //��������� SQL-�������� ��� ������� ������

	void InitializeXSN(); //������������� XSN
	void InitializeXSN(const QString &Content); //������������� �������� XSN
	
	void InitializeXSNTable(QDomNode &DomNode); //������������� �������
	void InitializeXSNTableSystemFields(PMetaClassTable *MetaTable); //������������� ��������� ����� ��� �������
	void InitializeXSNTableSystemFieldsVisible(PMetaClassTable *MetaTable, QDomNode &DomNode); //������������� ��������� ��������� �����
	void InitializeXSNTableFields(PMetaClassTable *MetaTable, QDomNode &DomNode); //������������� ����� �������
	void InitializeXSNTableIndexes(PMetaClassTable *MetaTable, QDomNode &DomNode); //������������� �������� ����� �������
	void InitializeXSNTableForeigns(PMetaClassTable *MetaTable, QDomNode &DomNode); //������������� ������� ������ ����� �������
	void InitializeXSNTableEscorts(PMetaClassTable *MetaTable, QDomNode &DomNode); //������������� �������� �������
	void InitializeXSNTableFieldBoxes(PMetaClassTable *MetaTable, QDomNode &DomNode); //������������� ����� ������
	void InitializeXSNTableJoins(PMetaClassTable *MetaTable, QDomNode &DomNode); //������������� JOIN'��

	void InitializeXSR(); //������������� XSR
	void InitializeXSRFile(const QString &Content); //������������� ����� XSR

private:
	ISMetaData();

	void SetPropertyObject(PMetaClass *MetaClass, const QString &PropertyName, const QVariant &PropertyValue);
	QDomNode GetChildDomNode(QDomNode &TableNode, const QString &TagName);

	QMap<QString, PMetaClassTable *> TablesMap; //�������
	QMap<QString, PMetaClassTable *> QueriesMap; //�������

	QVector<PMetaClassResource*> Resources; //������
	QVector<PMetaClassIndex*> IndexesCompound; //��������� �������

	QString CurrentXSN; //������� �������������� XSN
	QString CurrentXSR; //������� �������������� XSR
	bool Initialized;

	ISAssociationTypes AssociationTypes;
};
//-----------------------------------------------------------------------------
