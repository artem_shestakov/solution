#include "StdAfx.h"
#include "ISMetaQuery.h"
#include "ISMetaData.h"
#include "PMetaClassTable.h"
#include "PMetaClassField.h"
#include "ISSystem.h"
//-----------------------------------------------------------------------------
ISMetaQuery::ISMetaQuery()
{

}
//-----------------------------------------------------------------------------
ISMetaQuery::~ISMetaQuery()
{

}
//-----------------------------------------------------------------------------
QString ISMetaQuery::GetQueryText(const QString &QueryName, const QString &ClassFilter)
{
	QString Result = "SELECT \n";

	PMetaClassTable *MetaQuery = ISMetaData::GetInstanse().GetMetaQuery(QueryName);

	for (int i = 0; i < MetaQuery->GetFields().count(); i++)
	{
		PMetaClassField *MetaQueryField = MetaQuery->GetFields().at(i);
		Result += MetaQueryField->GetQueryText() + " AS \"" + MetaQueryField->GetLocalListName() + "\", \n";
	}

	ISSystem::RemoveLastSymbolFromString(Result, 3);
	Result += " \n";

	Result += "FROM " + MetaQuery->GetParent() + " \n";

	for (int i = 0; i < MetaQuery->GetJoins().count(); i++)
	{
		Result += MetaQuery->GetJoins().at(i) + " \n";
	}

	if (MetaQuery->GetWhere().length())
	{
		Result += "WHERE " + MetaQuery->GetWhere() + " \n";
	}

	if (ClassFilter.length())
	{
		Result += "AND " + ClassFilter + " \n";
	}

	Result += "ORDER BY " + MetaQuery->GetOrderField() + " ASC";

	return Result;
}
//-----------------------------------------------------------------------------
