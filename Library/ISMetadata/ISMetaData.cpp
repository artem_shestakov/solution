#include "StdAfx.h"
#include "ISMetaData.h"
#include "EXDefines.h"
#include "ISDebug.h"
#include "ISAssert.h"
#include "ISSystem.h"
#include "ISPlugin.h"
#include "ISCountingTime.h"
#include "ISMetaDataHelper.h"
#include "ISUuid.h"
//-----------------------------------------------------------------------------
ISMetaData::ISMetaData() : QObject()
{
	AssociationTypes.Insert("Int", ISNamespace::FT_Int, "INTEGER", "ISIntegerEdit");
	AssociationTypes.Insert("String", ISNamespace::FT_String, "VARCHAR", "ISLineEdit");
	AssociationTypes.Insert("Text", ISNamespace::FT_Text, "TEXT", "ISTextEdit");
	AssociationTypes.Insert("UID", ISNamespace::FT_UID, "UUID", "ISLineEdit");
	AssociationTypes.Insert("Bool", ISNamespace::FT_Bool, "BOOLEAN", "ISCheckEdit");
	AssociationTypes.Insert("Double", ISNamespace::FT_Double, "NUMERIC", "ISDoubleEdit");
	AssociationTypes.Insert("ByteArray", ISNamespace::FT_ByteArray, "BYTEA", QString());
	AssociationTypes.Insert("Date", ISNamespace::FT_Date, "DATE", "ISDateEdit");
	AssociationTypes.Insert("Time", ISNamespace::FT_Time, "TIME", "ISTimeEdit");
	AssociationTypes.Insert("DateTime", ISNamespace::FT_DateTime, "TIMESTAMP", "ISDateTimeEdit");
	AssociationTypes.Insert("Inn", ISNamespace::FT_Inn, "VARCHAR", "ISINNEdit");
	AssociationTypes.Insert("Kpp", ISNamespace::FT_Kpp, "VARCHAR", "ISKPPEdit");
	AssociationTypes.Insert("Ogrn", ISNamespace::FT_Ogrn, "VARCHAR", "ISOGRNEdit");
	AssociationTypes.Insert("Okpo", ISNamespace::FT_Okpo, "VARCHAR", "ISOKPOEdit");
	AssociationTypes.Insert("Bik", ISNamespace::FT_Bik, "VARCHAR", "ISBIKEdit");
	AssociationTypes.Insert("Icq", ISNamespace::FT_Icq, "VARCHAR", "ISICQEdit");
	AssociationTypes.Insert("Year", ISNamespace::FT_Year, "INTEGER", "ISYearEdit");
	AssociationTypes.Insert("Phone", ISNamespace::FT_Phone, "VARCHAR", "ISPhoneEdit");
	AssociationTypes.Insert("IPAddress", ISNamespace::FT_IPAddress, "VARCHAR", "ISIPAddressEdit");
	AssociationTypes.Insert("Password", ISNamespace::FT_Password, "VARCHAR", "ISPasswordEdit");
	AssociationTypes.Insert("Image", ISNamespace::FT_Image, "BYTEA", "ISImageEdit");
	AssociationTypes.Insert("Color", ISNamespace::FT_Color, "VARCHAR", "ISColorEdit");
	AssociationTypes.Insert("EMail", ISNamespace::FT_EMail, "VARCHAR", "ISEMailEdit");
	AssociationTypes.Insert("Passport", ISNamespace::FT_Passport, "VARCHAR", "ISPassportEdit");
}
//-----------------------------------------------------------------------------
ISMetaData::~ISMetaData()
{

}
//-----------------------------------------------------------------------------
ISMetaData& ISMetaData::GetInstanse()
{
	static ISMetaData MetaData;
	return MetaData;
}
//-----------------------------------------------------------------------------
bool ISMetaData::GetInitialized() const
{
	return Initialized;
}
//-----------------------------------------------------------------------------
void ISMetaData::Initialize(bool InitXSR)
{
	IS_ASSERT(!Initialized, "Meta data already initialized.");

	ISDebug::ShowDebugString("Initialize MetaData...");
	ISCountingTime Time;

	InitializeXSN();

	if (InitXSR)
	{
		InitializeXSR();
	}

	CheckUniqueAllIdentifiers();
	CheckUniqueAllAliases();
	GenerateSqlFromForeigns();

	QString TimeInitialized = ISSystem::MillisecondsToString(Time.GetElapsed());
	ISDebug::ShowDebugString("Initialize MetaData time: " + TimeInitialized);

	Initialized = true;
}
//-----------------------------------------------------------------------------
PMetaClassTable* ISMetaData::GetMetaTable(const QString &TableName)
{
	PMetaClassTable *MetaTable = TablesMap.value(TableName);
	IS_ASSERT(MetaTable, QString("Not found meta table: %1").arg(TableName));
	return MetaTable;
}
//-----------------------------------------------------------------------------
PMetaClassTable* ISMetaData::GetMetaQuery(const QString &QueryName)
{
	PMetaClassTable *MetaQuery = QueriesMap.value(QueryName);
	IS_ASSERT(MetaQuery, QString("Not found query: %1").arg(QueryName));
	return MetaQuery;
}
//-----------------------------------------------------------------------------
PMetaClassField* ISMetaData::GetMetaField(PMetaClassTable *MetaTable, const QString &FieldName)
{
	PMetaClassField *MetaField = nullptr;

	for (int i = 0; i < MetaTable->GetAllFields().count(); i++)
	{
		MetaField = MetaTable->GetAllFields().at(i);
		if (MetaField->GetName().toLower() == FieldName.toLower())
		{
			break;
		}
	}

	IS_ASSERT(MetaField, QString("Field \"%1\" not found in meta table \"%2\"").arg(FieldName).arg(MetaTable->GetName()));
	return MetaField;
}
//-----------------------------------------------------------------------------
PMetaClassField* ISMetaData::GetMetaField(const QString &TableName, const QString &FieldName)
{
	PMetaClassField *MetaField = GetMetaField(TablesMap.value(TableName), FieldName);
	IS_ASSERT(MetaField, QString("Field \"%1\" not found in meta table \"%2\"").arg(FieldName).arg(TableName));
	return MetaField;
}
//-----------------------------------------------------------------------------
QList<PMetaClassTable*> ISMetaData::GetTables()
{
	return TablesMap.values();
}
//-----------------------------------------------------------------------------
QVector<QString> ISMetaData::GetMetaQueries()
{
	QVector<QString> Vector;

	for (const auto a : QueriesMap.toStdMap())
	{
		PMetaClassTable *MetaClassQuery = a.second;
		Vector.append(MetaClassQuery->GetName());
	}

	return Vector;
}
//-----------------------------------------------------------------------------
QVector<PMetaClassIndex*> ISMetaData::GetSystemIndexes()
{
	QVector<PMetaClassIndex*> SystemIndexes;

	for (int i = 0; i < GetTables().count(); i++) //����� ������
	{
		PMetaClassTable *MetaTable = GetTables().at(i);

		for (int j = 0; j < MetaTable->GetSystemFields().count(); j++) //����� �����
		{
			PMetaClassIndex *Index = MetaTable->GetSystemFields().at(j)->GetIndex();
			if (Index)
			{
				SystemIndexes.append(Index);
			}
		}
	}

	return SystemIndexes;
}
//-----------------------------------------------------------------------------
QVector<PMetaClassIndex*> ISMetaData::GetIndexes()
{
	QVector<PMetaClassIndex*> Indexes;

	for (int i = 0; i < GetTables().count(); i++) //����� ������
	{
		PMetaClassTable *MetaTable = GetTables().at(i);

		for (int j = 0; j < MetaTable->GetFields().count(); j++) //����� �����
		{
			PMetaClassIndex *Index = MetaTable->GetFields().at(j)->GetIndex();
			if (Index)
			{
				Indexes.append(Index);
			}
		}
	}

	return Indexes;
}
//-----------------------------------------------------------------------------
QVector<PMetaClassIndex*> ISMetaData::GetCompoundIndexes()
{
	return IndexesCompound;
}
//-----------------------------------------------------------------------------
QVector<PMetaClassForeign*> ISMetaData::GetForeigns()
{
	QVector<PMetaClassForeign*> Foreigns;

	for (int i = 0; i < GetTables().count(); i++)
	{
		PMetaClassTable *MetaTable = GetTables().at(i);

		for (int j = 0; j < MetaTable->GetFields().count(); j++) //����� �����
		{
			PMetaClassForeign *Foreign = MetaTable->GetFields().at(j)->GetForeign();
			if (Foreign)
			{
				Foreigns.append(Foreign);
			}
		}
	}

	return Foreigns;
}
//-----------------------------------------------------------------------------
QVector<PMetaClassResource*> ISMetaData::GetResources()
{
	return Resources;
}
//-----------------------------------------------------------------------------
bool ISMetaData::CheckExitField(PMetaClassTable *MetaTable, const QString &FieldName) const
{
	bool Result = false;

	for (int i = 0; i < MetaTable->GetAllFields().count(); i++)
	{
		if (MetaTable->GetAllFields().at(i)->GetName().toLower() == FieldName.toLower())
		{
			Result = true;
			break;
		}
	}

	return Result;
}
//-----------------------------------------------------------------------------
ISAssociationTypes& ISMetaData::GetAssociationTypes()
{
	return AssociationTypes;
}
//-----------------------------------------------------------------------------
void ISMetaData::CheckUniqueAllIdentifiers()
{
	QStringList StringList;

	for (const auto a : TablesMap.toStdMap()) //����� ������
	{
		PMetaClassTable *MetaTable = a.second;
		IS_ASSERT(MetaTable, "Error");

		QString TableUID = MetaTable->GetUID();
		IS_ASSERT(!StringList.contains(TableUID), QString("DUPLICATE GUID for table \"%1\" already exist.").arg(TableUID));
		StringList.append(TableUID);

		for (int i = 0; i < MetaTable->GetFields().count(); i++) //����� �����
		{
			PMetaClassField *MetaField = MetaTable->GetFields().at(i);
			QString FieldUID = MetaField->GetUID();
			IS_ASSERT(!StringList.contains(FieldUID), QString("DUPLICATE GUID for table field \"%1\" already exist.").arg(FieldUID));
			StringList.append(FieldUID);
		}
	}

	for (const auto a : QueriesMap.toStdMap()) //����� ��������
	{
		PMetaClassTable *MetaQuery = a.second;
		IS_ASSERT(MetaQuery, "Error");

		QString QueryUID = MetaQuery->GetUID();
		IS_ASSERT(!StringList.contains(QueryUID), QString("DUPLICATE GUID for query \"%1\" already exist.").arg(QueryUID));
		StringList.append(QueryUID);

		for (int i = 0; i < MetaQuery->GetFields().count(); i++) //����� �����
		{
			PMetaClassField *MetaQueryField = MetaQuery->GetFields().at(i);
			QString FieldUID = MetaQueryField->GetUID();
			IS_ASSERT(!StringList.contains(FieldUID), QString("DUPLICATE GUID for query field \"%1\" already exist.").arg(FieldUID));
			StringList.append(FieldUID);
		}
	}

	for (int i = 0; i < Resources.count(); i++) //����� ��������
	{
		PMetaClassResource *MetaResource = Resources.at(i);
		IS_ASSERT(MetaResource, "Error");

		QString ResourceUID = MetaResource->GetUID();
		IS_ASSERT(!StringList.contains(ResourceUID), QString("DUPLICATE GUID for resource \"%1\" already exist.").arg(ResourceUID));
		StringList.append(ResourceUID);
	}

	for (int i = 0; i < StringList.count(); i++) //����� ��������������� (�������� ���������� ������� ��������������)
	{
		ISUuid UID = StringList.at(i);
		IS_ASSERT(!QUuid(UID).isNull(), QString("Invalid UID \"%1\"").arg(UID));
	}

	StringList.clear();
}
//-----------------------------------------------------------------------------
void ISMetaData::CheckUniqueAllAliases()
{
	QStringList StringList;

	for (const auto a : TablesMap.toStdMap()) //����� ������
	{
		PMetaClassTable *MetaTable = a.second;
		IS_ASSERT(MetaTable, "Error");

		QString Alias = MetaTable->GetAlias().toLower();
		IS_ASSERT(!StringList.contains(Alias), QString("Duplicate alias: %1. Table: %2.").arg(Alias).arg(MetaTable->GetName()));
		StringList.append(Alias);
	}

	StringList.clear();
}
//-----------------------------------------------------------------------------
void ISMetaData::GenerateSqlFromForeigns()
{
	QVector<PMetaClassForeign*> Foreigns = GetForeigns();
	for (int i = 0; i < Foreigns.count(); i++)
	{
		PMetaClassForeign *MetaForeign = Foreigns.at(i);
		MetaForeign->SetSqlQuery(ISMetaDataHelper::GenerateSqlQueryFromForeign(MetaForeign)); //��������� SQL-������� ��� �������� �����
	}
}
//-----------------------------------------------------------------------------
void ISMetaData::InitializeXSN()
{
	QStringList Filter("*.xsn");

	QFileInfoList FileInfoList = QDir(":Scheme").entryInfoList(Filter, QDir::NoFilter, QDir::SortFlag::Name);
	for (int i = 0; i < FileInfoList.count(); i++) //����� ���� XSN ������
	{
		QString FilePath = FileInfoList.at(i).filePath();

		QFile FileXSN(FilePath);
		IS_ASSERT(FileXSN.open(QIODevice::ReadOnly), QString("Not opened xsn file: %1").arg(FilePath));

		QString Content = FileXSN.readAll();
		InitializeXSN(Content);

		FileXSN.close();
	}

	QStringList ModuleXSNs = ISPlugin::GetInstance().GetPluginInterface()->GetScheme(Filter);
	for (int i = 0; i < ModuleXSNs.count(); i++) //����� ���� XSN ������ ������
	{
		QString Content = ModuleXSNs.at(i);
		InitializeXSN(Content);
	}
}
//-----------------------------------------------------------------------------
void ISMetaData::InitializeXSN(const QString &Content)
{
	ISCountingTime Time;
	QDomElement DomElement = ISSystem::GetDomElement(Content);

	QString TagName = DomElement.tagName();
	IS_ASSERT(TagName.length(), "Empty tag name");
	IS_ASSERT(TagName == "XSN", QString("Invalid tag name \"%1\" in XSN file.").arg(TagName));

	QString SchemeName = DomElement.attributes().namedItem("Name").nodeValue();
	IS_ASSERT(SchemeName.length(), "Empty schema name.");

	CurrentXSN = SchemeName;

	QDomNode DomNode = DomElement.firstChild();
	while (!DomNode.isNull())
	{
		QDomNode CurrentNode = DomNode;

		if (CurrentNode.nodeName() == "Table")
		{
			InitializeXSNTable(CurrentNode);
		}
		else
		{
			IS_ASSERT(false, QString("Invalid NodeName \"%1\" in XSN file %1").arg(CurrentNode.nodeName()).arg(SchemeName));
		}

		DomNode = DomNode.nextSibling();
	}

	CurrentXSN.clear();
	ISDebug::ShowDebugString(QString("Init XSN %1: %2 msec").arg(SchemeName).arg(Time.GetElapsed()));
}
//-----------------------------------------------------------------------------
void ISMetaData::InitializeXSNTable(QDomNode &DomNode)
{
	IS_ASSERT(DomNode.attributes().length(), QString("Empty attributes table. File: %1. Line: %2").arg(CurrentXSN).arg(DomNode.lineNumber()));

	PMetaClassTable *MetaTable = new PMetaClassTable(this);
	QString TableName = DomNode.attributes().namedItem("Name").nodeValue();
	QString Parent = DomNode.attributes().namedItem("Parent").nodeValue();
	IS_ASSERT(TableName.length(), QString("Empty table name. File: %1. Line: %2").arg(CurrentXSN).arg(DomNode.lineNumber()));
	MetaTable->setObjectName(TableName);
	MetaTable->SetTypeObject("Table");

	for (int i = 0; i < DomNode.attributes().length(); i++) //����� ������� �������
	{
		QDomNode Parameter = DomNode.attributes().item(i);
		QString ParameterName = Parameter.nodeName();
		QString ParameterValue = Parameter.nodeValue();

		SetPropertyObject(MetaTable, ParameterName, ParameterValue);
	}

	if (!Parent.length()) //���� ����-������� �� �������� ��������, ������������������� ��������� ����
	{
		InitializeXSNTableSystemFields(MetaTable); //������������� ��������� �����
	}

	InitializeXSNTableSystemFieldsVisible(MetaTable, GetChildDomNode(DomNode, "IncludeSystemFields").firstChild()); //������������� ��������� ��������� �����
	InitializeXSNTableFields(MetaTable, GetChildDomNode(DomNode, "Fields").firstChild()); //������������� ���������������� �����
	InitializeXSNTableIndexes(MetaTable, GetChildDomNode(DomNode, "Indexes").firstChild()); //������������� ��������
	InitializeXSNTableForeigns(MetaTable, GetChildDomNode(DomNode, "Foreigns").firstChild()); //������������� ������� ������
	InitializeXSNTableEscorts(MetaTable, GetChildDomNode(DomNode, "Escorts").firstChild()); //������������� ��������
	InitializeXSNTableFieldBoxes(MetaTable, GetChildDomNode(DomNode, "FieldBoxes").firstChild()); //������������� ������ �����
	InitializeXSNTableJoins(MetaTable, GetChildDomNode(DomNode, "Joins").firstChild()); //������������� JOIN'��

	if (Parent.length()) //���� � ����-������� ���� ������������ ������� - ����-������� �������� ��������
	{
		MetaTable->SetSqlModel("ISSqlModelView");
		MetaTable->SetShowOnly(true);

		IS_ASSERT(!QueriesMap.contains(TableName), QString("Query \"%1\" already exist in meta data").arg(TableName));
		QueriesMap.insert(TableName, MetaTable);
	}
	else //� ����-������� ��� ������������ �������
	{
		IS_ASSERT(!TablesMap.contains(TableName), QString("Table \"%1\" already exist in meta data").arg(TableName));
		TablesMap.insert(TableName, MetaTable);
	}
}
//-----------------------------------------------------------------------------
void ISMetaData::InitializeXSNTableSystemFields(PMetaClassTable *MetaTable)
{
	QDomElement DomElement = ISSystem::GetDomElement(QFile(SCHEMA_TEMPLATE_FIELDS_PATH));
	QDomNode DomNode = DomElement.firstChild();
	InitializeXSNTableFields(MetaTable, DomNode);

	PMetaClassField *FieldID = MetaTable->GetField("ID");
	IS_ASSERT(FieldID, "Null field object");
	FieldID->SetIndex(new PMetaClassIndex(true, MetaTable->GetAlias(), MetaTable->GetName(), FieldID->GetName(), FieldID));

	PMetaClassField *FieldUID = MetaTable->GetField("UID");
	IS_ASSERT(FieldUID, "Null field object");
	FieldUID->SetIndex(new PMetaClassIndex(true, MetaTable->GetAlias(), MetaTable->GetName(), FieldUID->GetName(), FieldUID));
	
	PMetaClassField *FieldIsDeleted = MetaTable->GetField("IsDeleted");
	IS_ASSERT(FieldIsDeleted, "Null field object");
	FieldIsDeleted->SetIndex(new PMetaClassIndex(false, MetaTable->GetAlias(), MetaTable->GetName(), FieldIsDeleted->GetName(), FieldIsDeleted));

	PMetaClassField *FieldIsSystem = MetaTable->GetField("IsSystem");
	IS_ASSERT(FieldIsSystem, "Null field object");
	FieldIsSystem->SetIndex(new PMetaClassIndex(false, MetaTable->GetAlias(), MetaTable->GetName(), FieldIsSystem->GetName(), FieldIsSystem));

	PMetaClassField *FieldCreationDate = MetaTable->GetField("CreationDate");
	IS_ASSERT(FieldCreationDate, "Null field object");
	FieldCreationDate->SetIndex(new PMetaClassIndex(false, MetaTable->GetAlias(), MetaTable->GetName(), FieldCreationDate->GetName(), FieldCreationDate));

	PMetaClassField *FieldUpdationDate = MetaTable->GetField("UpdationDate");
	IS_ASSERT(FieldUpdationDate, "Null field object");
	FieldUpdationDate->SetIndex(new PMetaClassIndex(false, MetaTable->GetAlias(), MetaTable->GetName(), FieldUpdationDate->GetName(), FieldUpdationDate));

	PMetaClassField *FieldDeletionDate = MetaTable->GetField("DeletionDate");
	IS_ASSERT(FieldDeletionDate, "Null field object");
	FieldDeletionDate->SetIndex(new PMetaClassIndex(false, MetaTable->GetAlias(), MetaTable->GetName(), FieldDeletionDate->GetName(), FieldDeletionDate));

	PMetaClassField *FieldCreationUser = MetaTable->GetField("CreationUser");
	IS_ASSERT(FieldCreationUser, "Null field object");
	FieldCreationUser->SetIndex(new PMetaClassIndex(false, MetaTable->GetAlias(), MetaTable->GetName(), FieldCreationUser->GetName(), FieldCreationUser));

	PMetaClassField *FieldUpdationUser = MetaTable->GetField("UpdationUser");
	IS_ASSERT(FieldUpdationUser, "Null field object");
	FieldUpdationUser->SetIndex(new PMetaClassIndex(false, MetaTable->GetAlias(), MetaTable->GetName(), FieldUpdationUser->GetName(), FieldUpdationUser));

	PMetaClassField *FieldDeletionUser = MetaTable->GetField("DeletionUser");
	IS_ASSERT(FieldDeletionUser, "Null field object");
	FieldDeletionUser->SetIndex(new PMetaClassIndex(false, MetaTable->GetAlias(), MetaTable->GetName(), FieldDeletionUser->GetName(), FieldDeletionUser));
}
//-----------------------------------------------------------------------------
void ISMetaData::InitializeXSNTableSystemFieldsVisible(PMetaClassTable *MetaTable, QDomNode &DomNode)
{
	while (!DomNode.isNull()) //����� �����
	{
		IS_ASSERT(DomNode.attributes().length(), QString("Empty attributes system field. File: %1. Line: %2").arg(CurrentXSN).arg(DomNode.lineNumber()));
		
		QString FieldName = DomNode.attributes().namedItem("Name").nodeValue();
		IS_ASSERT(FieldName.length(), QString("Empty system field name. File: %1. Line: %2").arg(CurrentXSN).arg(DomNode.lineNumber()));

		PMetaClassField *MetaField = MetaTable->GetField(FieldName);
		IS_ASSERT(MetaField, QString("Not found visible system field %1. File: %2. Line: %3").arg(FieldName).arg(CurrentXSN).arg(DomNode.lineNumber()));

		QString LocalName = DomNode.attributes().namedItem("LocalName").nodeValue();
		if (LocalName.length())
		{
			MetaField->SetLabelName(LocalName);
			MetaField->SetLocalListName(LocalName);
		}

		MetaTable->AddVisibleSystemField(MetaField);
		DomNode = DomNode.nextSibling();
	}
}
//-----------------------------------------------------------------------------
void ISMetaData::InitializeXSNTableFields(PMetaClassTable *MetaTable, QDomNode &DomNode)
{
	while (!DomNode.isNull()) //����� �����
	{
		if (DomNode.isComment()) //���� ��������� ����������� - ���������� �� ��������� ���
		{
			DomNode = DomNode.nextSibling();
			continue;
		}
		else
		{
			IS_ASSERT(DomNode.attributes().length(), QString("Empty attributes field. File: %1. Line: %2").arg(CurrentXSN).arg(DomNode.lineNumber()));
		}
		
		PMetaClassField *MetaField = new PMetaClassField(MetaTable);
		QString FieldName = DomNode.attributes().namedItem("Name").nodeValue();
		IS_ASSERT(FieldName.length(), QString("Empty field name. File: %1. Line: %2").arg(CurrentXSN).arg(DomNode.lineNumber()));
		MetaField->setObjectName(FieldName);
		MetaField->SetTypeObject("Field");

		for (int i = 0; i < DomNode.attributes().length(); i++) //����� ������� ����
		{
			QDomNode Parameter = DomNode.attributes().item(i);

			QString ParameterName = Parameter.nodeName();
			QString ParameterValue = Parameter.nodeValue();

			SetPropertyObject(MetaField, ParameterName, ParameterValue);
		}

		if (MetaField->GetIsSystem()) //���� ���� ���������
		{
			MetaTable->AddSystemField(MetaField);
		}
		else //���� �����������
		{
			MetaTable->AddField(MetaField);
		}

		IS_ASSERT(!MetaTable->GetField(FieldName), QString("Field \"%1\" already exist. TableName: %2").arg(FieldName).arg(MetaTable->GetName())); //�������� �� ������������ ���� � ����-�������
		MetaTable->AddAllField(MetaField);
		DomNode = DomNode.nextSibling();
	}
}
//-----------------------------------------------------------------------------
void ISMetaData::InitializeXSNTableIndexes(PMetaClassTable *MetaTable, QDomNode &DomNode)
{
	while (!DomNode.isNull()) //����� ��������
	{
		if (DomNode.isComment()) //���� ��������� ����������� - ���������� �� ��������� ���
		{
			DomNode = DomNode.nextSibling();
			continue;
		}
		else
		{
			IS_ASSERT(DomNode.attributes().length(), QString("Empty attributes index. File: %1. Line: %2").arg(CurrentXSN).arg(DomNode.lineNumber()));
		}

		QString FieldName = DomNode.attributes().namedItem("Field").nodeValue();
		IS_ASSERT(FieldName.length(), QString("Empty index name. File: %1. Line: %2").arg(CurrentXSN).arg(DomNode.lineNumber()));

		bool Unique = QVariant(DomNode.attributes().namedItem("Unique").nodeValue()).toBool();
		QStringList IndexList = FieldName.split(";");

		if (IndexList.count() > 1) //���� ������ ���������
		{
			PMetaClassIndex *Index = new PMetaClassIndex(Unique, MetaTable->GetAlias(), MetaTable->GetName(), QString(), this);

			for (int i = 0; i < IndexList.count(); i++)
			{
				Index->AddField(IndexList.at(i));
			}

			IndexesCompound.append(Index);
		}
		else //������ �����������
		{
			PMetaClassField *MetaField = MetaTable->GetField(FieldName);
			IS_ASSERT(MetaField, QString("Not found field from name: %1").arg(FieldName));
			IS_ASSERT(!MetaField->GetIndex(), QString("Index already exist. TableName: %1. FieldName: %2.").arg(MetaTable->GetName()).arg(FieldName)); //�������� ������� �� ������������
			MetaField->SetIndex(new PMetaClassIndex(Unique, MetaTable->GetAlias(), MetaTable->GetName(), FieldName, MetaField));
		}

		DomNode = DomNode.nextSibling();
	}
}
//-----------------------------------------------------------------------------
void ISMetaData::InitializeXSNTableForeigns(PMetaClassTable *MetaTable, QDomNode &DomNode)
{
	while (!DomNode.isNull())
	{
		IS_ASSERT(DomNode.attributes().length(), QString("Empty attributes foreign. File: %1. Line: %2").arg(CurrentXSN).arg(DomNode.lineNumber()));

		PMetaClassForeign *MetaForeign = new PMetaClassForeign(this);
		QString FieldName = DomNode.attributes().namedItem("Field").nodeValue();
		IS_ASSERT(MetaTable->GetField(FieldName), QString("Not found field \"%1\" in table \"%2\"").arg(FieldName).arg(MetaTable->GetName()));
		IS_ASSERT(FieldName.length(), QString("Empty foreign ffield. File: %1. Line: %2").arg(CurrentXSN).arg(DomNode.lineNumber()));
		MetaForeign->setObjectName(FieldName);
		MetaForeign->SetTypeObject("Foreign");

		for (int i = 0; i < DomNode.attributes().length(); i++)
		{
			QDomNode Parameter = DomNode.attributes().item(i);

			QString ParameterName = Parameter.nodeName();
			QString ParameterValue = Parameter.nodeValue();

			SetPropertyObject(MetaForeign, ParameterName, ParameterValue);
		}

		MetaForeign->SetTableName(MetaTable->GetName());

		PMetaClassField *MetaField = MetaTable->GetField(FieldName);
		IS_ASSERT(MetaField, QString("Not found field \"%1\" in table \"%2\"").arg(FieldName).arg(MetaTable->GetName())); //�������� ���� �� �������������
		IS_ASSERT(!MetaField->GetForeign(), QString("Foreign already exist. TableName: %1. FieldName: %2.").arg(MetaTable->GetName()).arg(FieldName)); //�������� �������� ����� �� ������������
		MetaField->SetForeign(MetaForeign);

		DomNode = DomNode.nextSibling();
	}
}
//-----------------------------------------------------------------------------
void ISMetaData::InitializeXSNTableEscorts(PMetaClassTable *MetaTable, QDomNode &DomNode)
{
	while (!DomNode.isNull())
	{
		IS_ASSERT(DomNode.attributes().length(), QString("Empty attributes escort. File: %1. Line: %2").arg(CurrentXSN).arg(DomNode.lineNumber()));

		PMetaClassEscort *MetaEscort = new PMetaClassEscort(MetaTable);
		MetaEscort->SetTypeObject("Escort");

		for (int i = 0; i < DomNode.attributes().length(); i++)
		{
			QDomNode Parameter = DomNode.attributes().item(i);

			QString ParameterName = Parameter.nodeName();
			QString ParameterValue = Parameter.nodeValue();

			SetPropertyObject(MetaEscort, ParameterName, ParameterValue);
		}

		MetaTable->AddEscort(MetaEscort);
		DomNode = DomNode.nextSibling();
	}
}
//-----------------------------------------------------------------------------
void ISMetaData::InitializeXSNTableFieldBoxes(PMetaClassTable *MetaTable, QDomNode &DomNode)
{
	while (!DomNode.isNull())
	{
		IS_ASSERT(DomNode.attributes().length(), QString("Empty attributes field boxes. File: %1. Line: %2").arg(CurrentXSN).arg(DomNode.lineNumber()));

		PMetaClassFieldBox *MetaFieldBox = new PMetaClassFieldBox(MetaTable);
		MetaFieldBox->SetTypeObject("FieldBox");

		for (int i = 0; i < DomNode.attributes().length(); i++)
		{
			QDomNode Parameter = DomNode.attributes().item(i);

			QString ParameterName = Parameter.nodeName();
			QString ParameterValue = Parameter.nodeValue();

			SetPropertyObject(MetaFieldBox, ParameterName, ParameterValue);
		}

		MetaTable->AddFieldBox(MetaFieldBox);
		DomNode = DomNode.nextSibling();
	}
}
//-----------------------------------------------------------------------------
void ISMetaData::InitializeXSNTableJoins(PMetaClassTable *MetaTable, QDomNode &DomNode)
{
	while (!DomNode.isNull())
	{
		IS_ASSERT(DomNode.attributes().length(), QString("Empty attributes field. File: %1. Line: %2").arg(CurrentXSN).arg(DomNode.lineNumber()));

		QString JoinText = DomNode.attributes().namedItem("Text").nodeValue();
		IS_ASSERT(JoinText.length(), QString("Empty join text. File: %1. Line: %2").arg(CurrentXSN).arg(DomNode.lineNumber()));

		MetaTable->AddJoin(JoinText);
		DomNode = DomNode.nextSibling();
	}
}
//-----------------------------------------------------------------------------
void ISMetaData::InitializeXSR()
{
	QStringList Filter("*.xsr");

	QFileInfoList FileInfoList = QDir(":Scheme").entryInfoList(Filter, QDir::NoFilter, QDir::SortFlag::Name);
	for (int i = 0; i < FileInfoList.count(); i++) //����� ���� XSR ������
	{
		QString FilePath = FileInfoList.at(i).filePath();

		QFile FileXSR(FilePath);
		IS_ASSERT(FileXSR.open(QIODevice::ReadOnly), QString("Not opened xsr file: %1").arg(FilePath));

		QString Content = FileXSR.readAll();
		InitializeXSRFile(Content);

		FileXSR.close();
	}

	QStringList ModuleXSNs = ISPlugin::GetInstance().GetPluginInterface()->GetScheme(Filter);
	for (int i = 0; i < ModuleXSNs.count(); i++) //����� ���� XSR ������ ������
	{
		QString Content = ModuleXSNs.at(i);
		InitializeXSRFile(Content);
	}
}
//-----------------------------------------------------------------------------
void ISMetaData::InitializeXSRFile(const QString &Content)
{
	ISCountingTime Time;
	QDomElement DomElement = ISSystem::GetDomElement(Content);

	QString TagName = DomElement.tagName();
	IS_ASSERT(TagName.length(), "Empty tag name");
	IS_ASSERT(TagName == "XSR", QString("Invalid tag name \"%1\" in XSR file").arg(TagName));
	
	QString SchemeName = DomElement.attributes().namedItem("Name").nodeValue();
	IS_ASSERT(SchemeName.length(), "Empty schema name");

	CurrentXSR = SchemeName;

	QDomNode DomNode = DomElement.firstChild();
	while (!DomNode.isNull()) //����� ��������
	{
		if (DomNode.isComment())
		{
			DomNode = DomNode.nextSibling();
			continue;
		}

		QDomNamedNodeMap DomNodeMap = DomNode.attributes();
		IS_ASSERT(DomNodeMap.length(), QString("Empty resource arguments. File: %1. Line: %2").arg(CurrentXSR).arg(DomNode.lineNumber()));

		QString TableName = DomNode.nodeName();
		IS_ASSERT(TableName.length(), QString("Empty resource name. File: %1. Line: %2").arg(CurrentXSR).arg(DomNode.lineNumber()));

		PMetaClassResource *MetaResource = new PMetaClassResource(this);
		MetaResource->SetTableName(TableName);

		for (int i = 0; i < DomNodeMap.length(); i++) //����� ����� �������
		{
			QDomNode DomItem = DomNodeMap.item(i); // ����-��������

			QString FieldName = DomItem.nodeName(); //��� ����
			QString Value = DomItem.nodeValue(); //�������� ����

			PMetaClassTable *MetaTable = TablesMap.value(TableName);
			IS_ASSERT(MetaTable, QString("Invalid table name \"%1\" in resource. File: %2. Line: %3").arg(TableName).arg(CurrentXSR).arg(DomNode.lineNumber()));

			PMetaClassField *MetaField = MetaTable->GetField(FieldName);
			IS_ASSERT(MetaField, QString("Invalid field name \"%1\" in resource. File: %2. Line: %3").arg(FieldName).arg(CurrentXSR).arg(DomNode.lineNumber()));

			MetaResource->AddField(FieldName, Value);
		}

		MetaResource->SetUID(ISUuid(MetaResource->GetUID()));
		IS_ASSERT(MetaResource->GetUID().length(), QString("Empty UID in resource. File: %1. Line: %2").arg(CurrentXSR).arg(DomNode.lineNumber()));

		Resources.append(MetaResource);
		DomNode = DomNode.nextSibling();
	}

	CurrentXSR.clear();
	ISDebug::ShowDebugString(QString("Init XSR %1: %2 msec").arg(SchemeName).arg(Time.GetElapsed()));
}
//-----------------------------------------------------------------------------
void ISMetaData::SetPropertyObject(PMetaClass *MetaClass, const QString &PropertyName, const QVariant &PropertyValue)
{
	bool Found = false;

	const QMetaObject *MetaObject = MetaClass->metaObject();
	for (int i = 0; i < MetaObject->propertyCount(); i++)
	{
		QMetaProperty MetaProperty = MetaObject->property(i);
		const char *Name = MetaProperty.name();
		if (Name == PropertyName)
		{
			bool Changed = MetaClass->setProperty(PropertyName.toUtf8().data(), PropertyValue);
			IS_ASSERT(Changed, QString("Not changed property: %1. TypeObjec: %2. ObjectName: %3. File: %4").arg(PropertyName).arg(MetaClass->GetTypeObject()).arg(MetaClass->objectName()).arg(CurrentXSN));
			Found = true;
			break;
		}
	}

	IS_ASSERT(Found, QString("Not found property: %1. TypeObject: %2. ObjectName: %3. File: %4").arg(PropertyName).arg(MetaClass->GetTypeObject()).arg(MetaClass->objectName()).arg(CurrentXSN));
}
//-----------------------------------------------------------------------------
QDomNode ISMetaData::GetChildDomNode(QDomNode &TableNode, const QString &TagName)
{
	QDomNode DomNode = TableNode.firstChild();
	while (!DomNode.isNull())
	{
		if (DomNode.nodeName() == TagName)
		{
			return DomNode;
		}

		DomNode = DomNode.nextSibling();
	}

	return QDomNode();
}
//-----------------------------------------------------------------------------
