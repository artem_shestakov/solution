#pragma once
//-----------------------------------------------------------------------------
#include "ismetadata_global.h"
#include "PMetaClassField.h"
//-----------------------------------------------------------------------------
class ISMETADATA_EXPORT PMetaClassQueryField : public PMetaClassField
{
	Q_OBJECT

public:
	PMetaClassQueryField(QObject *parent = 0);
	virtual ~PMetaClassQueryField();
};
//-----------------------------------------------------------------------------
