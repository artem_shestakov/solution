#pragma once
//-----------------------------------------------------------------------------
#include "ismetadata_global.h"
#include "PMetaClass.h"
//-----------------------------------------------------------------------------
class ISMETADATA_EXPORT PMetaClassForeign : public PMetaClass
{
	Q_OBJECT

	Q_PROPERTY(QString Field READ GetFieldName WRITE SetFieldName)
	Q_PROPERTY(QString ForeignClass READ GetForeignClass WRITE SetForeignClass)
	Q_PROPERTY(QString ForeignField READ GetForeginField WRITE SetForeignField)
	Q_PROPERTY(QString ForeignViewNameField READ GetForeignViewNameField WRITE SetForeignViewNameField)
	Q_PROPERTY(QString TableName READ GetTableName WRITE SetTableName)
	Q_PROPERTY(QString SqlQuery READ GetSqlQuery WRITE SetSqlQuery)

public:
	PMetaClassForeign(QObject *parent = 0);
	virtual ~PMetaClassForeign();

	void SetFieldName(const QString &field);
	QString GetFieldName() const;

	void SetForeignClass(const QString &foreign_class);
	QString GetForeignClass() const;

	void SetForeignField(const QString &foreign_field);
	QString GetForeginField() const;

	void SetForeignViewNameField(const QString &foreign_view_name);
	QString GetForeignViewNameField() const;

	void SetTableName(const QString &table_name);
	QString GetTableName() const;

	void SetSqlQuery(const QString &sql_query);
	QString GetSqlQuery() const;

private:
	QString Field; //����, �� ������� ��������������� ������� ����
	QString ForeignClass; //�� ����� ������� ���������� ������� ����
	QString ForeignField; //�� ����� ���� ���������� ������� ����
	QString ForeignViewNameField; //����� ���� (����) ���������� � ������� �� �������

	QString TableName; //�������, ���������� ����, �� ������� ��������������� ������� ����
	QString SqlQuery; //������ �� ������ �� �������� �����
};
//-----------------------------------------------------------------------------
