#include "StdAfx.h"
#include "ISMetaDataHelper.h"
#include "EXDefines.h"
#include "ISAssert.h"
#include "ISMetaData.h"
#include "ISSystem.h"
//-----------------------------------------------------------------------------
ISMetaDataHelper::ISMetaDataHelper()
{

}
//-----------------------------------------------------------------------------
ISMetaDataHelper::~ISMetaDataHelper()
{

}
//-----------------------------------------------------------------------------
QString ISMetaDataHelper::GenerateSqlQueryFromForeign(PMetaClassForeign *MetaForeign, const QString &SqlFilter, const QVariant &ObjectID)
{
	PMetaClassTable *MetaTableForeign = ISMetaData::GetInstanse().GetMetaTable(MetaForeign->GetForeignClass()); //������� �� ������� ��������� ������� ����
	QString ForeignAlias = MetaTableForeign->GetAlias();
	QStringList FieldList = MetaForeign->GetForeignViewNameField().split(";");

	QString SqlQuery = "SELECT " + ForeignAlias + "_" + MetaForeign->GetForeginField().toLower() + " AS ID, concat(";

	for (int i = 0; i < FieldList.count(); i++) //����� ����� (������� ������ ���� ����������)
	{
		SqlQuery += ForeignAlias + "_" + FieldList.at(i).toLower() + ", ' ', ";
	}

	ISSystem::RemoveLastSymbolFromString(SqlQuery, 7);

	SqlQuery += ") ";
	SqlQuery += "AS Value \n";
	SqlQuery += "FROM " + MetaTableForeign->GetName() + " \n";
	SqlQuery += "WHERE NOT " + ForeignAlias + "_isdeleted \n";

	if (SqlFilter.length())
	{
		SqlQuery += "AND " + SqlFilter + " \n";
	}

	if (ObjectID.isValid())
	{
		SqlQuery += "AND " + ForeignAlias + "_" + MetaForeign->GetForeginField() + " = :ObjectID \n";
	}

	SqlQuery += "ORDER BY 2 ASC";

	return SqlQuery;
}
//-----------------------------------------------------------------------------
