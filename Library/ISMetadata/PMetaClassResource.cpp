#include "StdAfx.h"
#include "PMetaClassResource.h"
#include "EXDefines.h"
#include "ISAssert.h"
//-----------------------------------------------------------------------------
PMetaClassResource::PMetaClassResource(QObject *parent) : QObject(parent)
{
	
}
//-----------------------------------------------------------------------------
PMetaClassResource::~PMetaClassResource()
{

}
//-----------------------------------------------------------------------------
void PMetaClassResource::AddField(const QString &FieldName, const QString &Value)
{
	if (FieldName.toLower() == "uid")
	{
		IS_ASSERT(!UID.length(), "Already exist uid " + UID);
		SetUID(Value);
	}
	else
	{
		Parameters.insert(FieldName, Value);
	}
}
//-----------------------------------------------------------------------------
QString PMetaClassResource::GetParameterValue(const QString &ParameterName) const
{
	QString Value = Parameters.value(ParameterName);
	return Value;
}
//-----------------------------------------------------------------------------
QMap<QString, QString> PMetaClassResource::GetParameters()
{
	return Parameters;
}
//-----------------------------------------------------------------------------
void PMetaClassResource::SetTableName(const QString &table_name)
{
	this->TableName = table_name;
}
//-----------------------------------------------------------------------------
QString PMetaClassResource::GetTableName() const
{
	return this->TableName;
}
//-----------------------------------------------------------------------------
void PMetaClassResource::SetUID(const ISUuid &uid)
{
	UID = uid;
}
//-----------------------------------------------------------------------------
ISUuid PMetaClassResource::GetUID() const
{
	return UID;
}
//-----------------------------------------------------------------------------
