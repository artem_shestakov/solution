#pragma once
//-----------------------------------------------------------------------------
#include "ismetadata_global.h"
#include "ISNamespace.h"
//-----------------------------------------------------------------------------
struct ISMETADATA_EXPORT ISType 
{
	QString TypeName;
	ISNamespace::FieldType TypeField;
	QString TypeDB;
	QString ControlWidget;
};
//-----------------------------------------------------------------------------
class ISMETADATA_EXPORT ISAssociationTypes
{
public:
	ISAssociationTypes();
	~ISAssociationTypes();

	void Insert(const QString &type_name, ISNamespace::FieldType meta_field_type, const QString &database_type, const QString &control_widget); //�������� ���������� ����
	ISNamespace::FieldType GetTypeField(const QString &type_name); //�������� ��� ������ �� ����-�����
	QString GetTypeDB(ISNamespace::FieldType field_type) const; //�������� ������������ ���� � ���� ������
	QString GetTypeDB(const QString &type_name) const; //�������� ������������ ���� � ���� ������
	QString GetControlWidgetFromType(ISNamespace::FieldType field_type) const; //�������� ������������ ����� ��� ���� ������

private:
	QVector<ISType> Types;
};
//-----------------------------------------------------------------------------
