#include "StdAfx.h"
#include "ISAssociationTypes.h"
#include "EXDefines.h"
#include "ISAssert.h"
//-----------------------------------------------------------------------------
ISAssociationTypes::ISAssociationTypes()
{

}
//-----------------------------------------------------------------------------
ISAssociationTypes::~ISAssociationTypes()
{

}
//-----------------------------------------------------------------------------
void ISAssociationTypes::Insert(const QString &type_name, ISNamespace::FieldType meta_field_type, const QString &database_type, const QString &control_widget)
{
	ISType Type;
	Type.TypeName = type_name;
	Type.TypeField = meta_field_type;
	Type.TypeDB = database_type;
	Type.ControlWidget = control_widget;

	Types.append(Type);
}
//-----------------------------------------------------------------------------
ISNamespace::FieldType ISAssociationTypes::GetTypeField(const QString &type_name)
{
	if (Types.count())
	{
		for (const ISType &Type : Types)
		{
			if (Type.TypeName == type_name)
			{
				return Type.TypeField;
			}
		}
	}

	IS_ASSERT(false, QString("Not found field type from string: %1").arg(type_name));
	return ISNamespace::FT_Unknown;
}
//-----------------------------------------------------------------------------
QString ISAssociationTypes::GetTypeDB(ISNamespace::FieldType field_type) const
{
	if (Types.count())
	{
		for (const ISType &Type : Types)
		{
			if (Type.TypeField == field_type)
			{
				return Type.TypeDB;
			}
		}
	}

	IS_ASSERT(false, "Not found type db.");
	return QString();
}
//-----------------------------------------------------------------------------
QString ISAssociationTypes::GetTypeDB(const QString &type_name) const
{
	if (Types.count())
	{
		for (const ISType &Type : Types)
		{
			if (Type.TypeName == type_name)
			{
				return Type.TypeDB;
			}
		}
	}

	IS_ASSERT(false, "Not found type db.");
	return QString();
}
//-----------------------------------------------------------------------------
QString ISAssociationTypes::GetControlWidgetFromType(ISNamespace::FieldType field_type) const
{
	if (Types.count())
	{
		for (const ISType &Type : Types)
		{
			if (Type.TypeField == field_type)
			{
				return Type.ControlWidget;
			}
		}
	}

	IS_ASSERT(false, "Not found control widget from field type.");
	return QString();
}
//-----------------------------------------------------------------------------
