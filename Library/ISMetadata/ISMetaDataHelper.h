#pragma once
//-----------------------------------------------------------------------------
#include "ismetadata_global.h"
#include "ISNamespace.h"
#include "PMetaClassForeign.h"
//-----------------------------------------------------------------------------
class ISMETADATA_EXPORT ISMetaDataHelper : public QObject
{
	Q_OBJECT

public:
	ISMetaDataHelper();
	virtual ~ISMetaDataHelper();

	static QString GenerateSqlQueryFromForeign(PMetaClassForeign *MetaForeign, const QString &SqlFilter = QString(), const QVariant &ObjectID = QVariant()); //��������� SQL-������� ��� �������� �����
};
//-----------------------------------------------------------------------------
