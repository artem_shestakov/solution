#pragma once
//-----------------------------------------------------------------------------
#include "isprocess_global.h"
#include "ISCountingTime.h"
//-----------------------------------------------------------------------------
class ISPROCESS_EXPORT ISProcess : public QObject
{
	Q_OBJECT

signals:
	void Message(const QString &Message);
	void Finished();
	void Delete(QObject *Object);

public:
	ISProcess(QObject *parent);
	~ISProcess();

	void SetProcessEnvironment(const QProcessEnvironment &ProcessEnvironment);

	void Start(const QString &ProgrammPath, const QString &Arguments);

protected slots:
	void OnFinished(int ExitCode);
	void OnReadyReadStandartError();
	void OnReadyReadStandartOutput();
	void OnStarted();
	void OnStateChanged(QProcess::ProcessState State);
	void OnErrorOccured(QProcess::ProcessError Error);
	void OnDelete();

private:
	QProcess *Process;
	ISCountingTime TimeWorking;
};
//-----------------------------------------------------------------------------
