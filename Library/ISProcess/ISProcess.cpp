#include "StdAfx.h"
#include "ISProcess.h"
#include "ISAssert.h"
#include "ISLocalization.h"
#include "EXDefines.h"
#include "ISSystem.h"
//-----------------------------------------------------------------------------
ISProcess::ISProcess(QObject *parent) : QObject(parent)
{
	Process = new QProcess(this);
	connect(Process, SIGNAL(finished(int)), this, SLOT(OnFinished(int)));
	connect(Process, &QProcess::readyReadStandardError, this, &ISProcess::OnReadyReadStandartError);
	connect(Process, &QProcess::readyReadStandardOutput, this, &ISProcess::OnReadyReadStandartOutput);
	connect(Process, &QProcess::started, this, &ISProcess::OnStarted);
	connect(Process, &QProcess::stateChanged, this, &ISProcess::OnStateChanged);
	connect(Process, &QProcess::errorOccurred, this, &ISProcess::OnErrorOccured);

	TimeWorking.Restart();
}
//-----------------------------------------------------------------------------
ISProcess::~ISProcess()
{

}
//-----------------------------------------------------------------------------
void ISProcess::SetProcessEnvironment(const QProcessEnvironment &ProcessEnvironment)
{
	Process->setProcessEnvironment(ProcessEnvironment);
}
//-----------------------------------------------------------------------------
void ISProcess::Start(const QString &ProgrammPath, const QString &Arguments)
{
	IS_ASSERT(ProgrammPath.length(), "Programm path is null");

	if (ProgrammPath.contains("\""))
	{
		QString Path = ProgrammPath;
		Path = Path.replace("\"", QString());
		IS_ASSERT(QFile::exists(Path), "Programm " + Path + " not exist.");
	}

	QString Command = ProgrammPath + " " + Arguments;

	Process->start(Command);

	emit Message(LOCALIZATION("StartCommand") + ": " + Command);
}
//-----------------------------------------------------------------------------
void ISProcess::OnFinished(int ExitCode)
{
	QString FinishString = QString();

	if (ExitCode)
	{
		FinishString = LOCALIZATION("Process.Finish.Failure");
	}
	else
	{
		FinishString = LOCALIZATION("Process.Finish.Normal");
		
		emit Finished();
		QTimer::singleShot(3000, Qt::PreciseTimer, this, &ISProcess::OnDelete);
	}

	emit Message(FinishString);
	
	QString WorkingTime = ISSystem::MillisecondsToString(TimeWorking.GetElapsed());
	emit Message(LOCALIZATION("TimeWork") + ": " + WorkingTime);
}
//-----------------------------------------------------------------------------
void ISProcess::OnReadyReadStandartError()
{
	QByteArray ByteArray = Process->readAllStandardError();
	QString StandartError = QString::fromLocal8Bit(ByteArray);

	emit Message(StandartError);
}
//-----------------------------------------------------------------------------
void ISProcess::OnReadyReadStandartOutput()
{
	QByteArray ByteArray = Process->readAllStandardOutput();
	QString StandartOutput = QString::fromLocal8Bit(ByteArray);

	emit Message(StandartOutput);
}
//-----------------------------------------------------------------------------
void ISProcess::OnStarted()
{
	emit Message(LOCALIZATION("Process.Signal.Started"));
}
//-----------------------------------------------------------------------------
void ISProcess::OnStateChanged(QProcess::ProcessState State)
{
	QString StateText = QString();

	switch (State)
	{
	case QProcess::ProcessState::Starting: StateText = LOCALIZATION("Process.ProcessState.Starting"); break;
	case QProcess::ProcessState::Running: StateText = LOCALIZATION("Process.ProcessState.Running"); break;
	case QProcess::ProcessState::NotRunning: StateText = LOCALIZATION("Process.ProcessState.NotRunning"); break;
	}

	if (State == QProcess::Starting)
	{
		TimeWorking.Restart();
	}

	emit Message(StateText);
}
//-----------------------------------------------------------------------------
void ISProcess::OnErrorOccured(QProcess::ProcessError Error)
{
	QString ErrorText;

	switch (Error)
	{
	case QProcess::ProcessError::Crashed: ErrorText = LOCALIZATION("Process.ProcessError.Crashed"); break;
	case QProcess::ProcessError::FailedToStart: ErrorText = LOCALIZATION("Process.ProcessError.FailedToStart"); break;
	case QProcess::ProcessError::ReadError: ErrorText = LOCALIZATION("Process.ProcessError.ReadError"); break;
	case QProcess::ProcessError::WriteError: ErrorText = LOCALIZATION("Process.ProcessError.WriteError"); break;
	case QProcess::ProcessError::Timedout: ErrorText = LOCALIZATION("Process.ProcessError.Timedout"); break;
	case QProcess::ProcessError::UnknownError: ErrorText = LOCALIZATION("Process.ProcessError.UnknownError"); break;
	}

	emit Message(ErrorText + " " + Process->errorString());
}
//-----------------------------------------------------------------------------
void ISProcess::OnDelete()
{
	emit Delete(this);
}
//-----------------------------------------------------------------------------
