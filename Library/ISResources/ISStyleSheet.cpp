#include "StdAfx.h"
#include "ISStyleSheet.h"
#include "EXDefines.h"
#include "ISDebug.h"
#include "ISAssert.h"
#include "ISCountingTime.h"
//-----------------------------------------------------------------------------
ISStyleSheet::ISStyleSheet()
{
	Initialize();
}
//-----------------------------------------------------------------------------
ISStyleSheet::~ISStyleSheet()
{

}
//-----------------------------------------------------------------------------
ISStyleSheet& ISStyleSheet::GetInstance()
{
	static ISStyleSheet StyleSheet;
	return StyleSheet;
}
//-----------------------------------------------------------------------------
QString ISStyleSheet::GetStyle(const QString &StyleName, const QString &SourceFile, int FileLine) const
{
	bool Contains = StyleSheets.contains(StyleName);
	IS_ASSERT(Contains, "StyleSheet '" + StyleName + "' not found. File: " + SourceFile + "; Line: " + QString::number(FileLine));

	QString FilePath = StyleSheets.value(StyleName);
	QFile File(FilePath);
	File.open(QIODevice::ReadOnly | QIODevice::Text);
	QString StyleSheet = File.readAll();
	File.close();

	return StyleSheet;
}
//-----------------------------------------------------------------------------
void ISStyleSheet::Initialize()
{
	ISCountingTime Time;

	QFileInfoList FileInfoList = QDir(":CSS").entryInfoList(QDir::NoFilter);
	for (int i = 0; i < FileInfoList.count(); i++)
	{
		QFileInfo FileInfo = FileInfoList.at(i);

		QString FileName = FileInfo.completeBaseName();
		QString FilePath = FileInfo.filePath();

		AddStyle(FileName, FilePath);
	}

	int Msec = Time.GetElapsed();
	ISDebug::ShowDebugString(QString("StyleSheet Initialized. msec: %1. Items: %2").arg(Msec).arg(StyleSheets.count()));
}
//-----------------------------------------------------------------------------
void ISStyleSheet::AddStyle(const QString &FileName, const QString &FilePath)
{
	bool Contains = StyleSheets.contains(FileName);
	IS_ASSERT(!Contains, "StyleSheet '" + FileName + "' already exist in buffer StyleSheets");

	QFile File(FilePath);
	bool Exist = File.exists();
	IS_ASSERT(Exist, "File " + FileName + " StyleSheet not exist");

	bool Opened = File.open(QIODevice::ReadOnly | QIODevice::Text);
	IS_ASSERT(Opened, "File " + FileName + " StyleSheet not open. Error: " + File.errorString());

	File.close();

	StyleSheets.insert(FileName, FilePath);
}
//-----------------------------------------------------------------------------
