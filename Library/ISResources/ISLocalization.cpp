#include "StdAfx.h"
#include "ISLocalization.h"
#include "EXDefines.h"
#include "ISSystem.h"
#include "ISDebug.h"
#include "ISAssert.h"
#include "ISCountingTime.h"
//-----------------------------------------------------------------------------
ISLocalization::ISLocalization()
{
	QTranslator *Translator = new QTranslator(qApp);
	if (Translator->load(APPLICATION_TRANSLATIOS_DIR + "/qt_ru.qm"))
	{
		if (qApp->installTranslator(Translator))
		{
			ISDebug::ShowInfoString("Translator installing: OK");
		}
		else
		{
			ISDebug::ShowWarningString("Translator installing: error");
		}
	}
	else
	{
		ISDebug::ShowWarningString("Not load translator file");
	}
}
//-----------------------------------------------------------------------------
ISLocalization::~ISLocalization()
{

}
//-----------------------------------------------------------------------------
ISLocalization& ISLocalization::GetInstance()
{
	static ISLocalization Localization;
	return Localization;
}
//-----------------------------------------------------------------------------
QString ISLocalization::GetLocalString(const QString &ParameterName, const QString &SourceFile, int FileLine) const
{
	IS_ASSERT(ParameterName.length(), QString("Localization parameter is null. Parameter: %1. File: %2. Line: %3").arg(ParameterName).arg(SourceFile).arg(FileLine));
	IS_ASSERT(Localization.count(), "Localization list is empty");
	
	QString LocalizationValue = Localization.value(ParameterName);
	IS_ASSERT(LocalizationValue.length(), QString("Localization \"%1\" not found in buffer localizations. File: %2. Line: %3").arg(ParameterName).arg(SourceFile).arg(FileLine));
	return LocalizationValue;
}
//-----------------------------------------------------------------------------
void ISLocalization::LoadResourceFile(const QString &FileName)
{
	QFile File(":Localization/" + FileName + "." + EXTENSION_LANG);
	IS_ASSERT(File.open(QIODevice::ReadOnly), QString("Not opened localization file \"%1\". Error: %2").arg(File.fileName()).arg(File.errorString()));
	InitializeContent(File.readAll());
	File.close();
}
//-----------------------------------------------------------------------------
void ISLocalization::InitializeContent(const QString &Content)
{
	ISCountingTime Time;
	int CountItems = 0;
	
	QDomElement DomElement = ISSystem::GetDomElement(Content);
	QDomNode NodeLocalization = DomElement.firstChild();
	
	QString LocalizationName = DomElement.attributes().namedItem("Name").nodeValue();
	IS_ASSERT(LocalizationName.length(), "Invalid name file localization.");

	IS_ASSERT(!LoadedFiles.contains(LocalizationName), QString("Localization file \"%1\" already initialized.").arg(LocalizationName));

	while (!NodeLocalization.isNull())
	{
		QString ParameterName = NodeLocalization.attributes().namedItem("Name").nodeValue();
		QString Value = NodeLocalization.attributes().namedItem("Russian").nodeValue();

		if (ParameterName.length())
		{
			IS_ASSERT(!Localization.contains(ParameterName), QString("Key \"%1\" already exist in localization map. File: %2. Line: %3").arg(ParameterName).arg(LocalizationName).arg(NodeLocalization.lineNumber()));
			Localization.insert(ParameterName, Value);
			CountItems++;
		}

		NodeLocalization = NodeLocalization.nextSibling();
	}
	
	int Msec = Time.GetElapsed();
	ISDebug::ShowInfoString(QString("Localization \"%1\" Initialized. msec: %2. Items: %3").arg(LocalizationName).arg(Msec).arg(CountItems));

	LoadedFiles.append(LocalizationName);
}
//-----------------------------------------------------------------------------
bool ISLocalization::CheckExistLocal(const QString &ParameterName)
{
	QString Value = Localization.value(ParameterName);
	if (Value.length())
	{
		return true;
	}

	return false;
}
//-----------------------------------------------------------------------------
