#pragma once
//-----------------------------------------------------------------------------
#include "isresources_global.h"
//-----------------------------------------------------------------------------
class ISRESOURCES_EXPORT ISLocalization : public QObject
{
	Q_OBJECT

public:
	ISLocalization(const ISLocalization &) = delete;
	ISLocalization(ISLocalization &&) = delete;
	ISLocalization &operator=(const ISLocalization &) = delete;
	ISLocalization &operator=(ISLocalization &&) = delete;
	~ISLocalization();

	static ISLocalization& GetInstance();

	QString GetLocalString(const QString &ParameterName, const QString &SourceFile, int FileLine) const;
	void LoadResourceFile(const QString &FileName); //������������� ����� �� ��������
	void InitializeContent(const QString &Content); //�������� ��������� ����������� � �����

protected:
	bool CheckExistLocal(const QString &ParameterName);

private:
	ISLocalization();

	QMap<QString, QString> Localization;
	QVector<QString>  LoadedFiles;
};
//-----------------------------------------------------------------------------
#define LOCALIZATION(PARAMETER) ISLocalization::GetInstance().GetLocalString(PARAMETER, __FILE__, __LINE__)
//-----------------------------------------------------------------------------
