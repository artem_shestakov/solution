#include "StdAfx.h"
#include "ISBuffer.h"
#include "EXDefines.h"
#include "ISDebug.h"
#include "ISAssert.h"
#include "ISCountingTime.h"
//-----------------------------------------------------------------------------
ISBuffer::ISBuffer()
{
	InitializeAnimations();
	InitializeIcons();
	InitializePixmaps();
	InitializeAudios();
}
//-----------------------------------------------------------------------------
ISBuffer::~ISBuffer()
{

}
//-----------------------------------------------------------------------------
ISBuffer& ISBuffer::GetInstance()
{
	static ISBuffer Buffer;
	return Buffer;
}
//-----------------------------------------------------------------------------
QMovie* ISBuffer::GetAnimation(const QString &AnimationName, QObject *parent, const QString &SourceFile, int FileLine)
{
	bool Contains = Animations.contains(AnimationName);
	IS_ASSERT(Contains, QString("Animation \"%1\" not found in buffer animations").arg(AnimationName));

	QString AnimationPath = Animations.value(AnimationName);
	QMovie *Movie = new QMovie(AnimationPath, nullptr, parent);
	return Movie;
}
//-----------------------------------------------------------------------------
QIcon ISBuffer::GetIcon(const QString &IconName, const QString &SourceFile, int FileLine)
{
	bool Contains = Icons.contains(IconName);
	IS_ASSERT(Contains, QString("Icon \"%1\" not found in buffer icons").arg(IconName));

	QString IconPath = Icons.value(IconName);
	QIcon Icon(IconPath);
	return Icon;
}
//-----------------------------------------------------------------------------
QPixmap ISBuffer::GetPixmap(const QString &PixmapName, const QString &SourceFile, int FileLine)
{
	bool Contains = Pixmaps.contains(PixmapName);
	IS_ASSERT(Contains, QString("Pixmap \"%1\" not found in buffer pixmaps").arg(PixmapName));

	QString PixmapPath = Pixmaps.value(PixmapName);
	QPixmap Pixmap(PixmapPath);
	return Pixmap;
}
//-----------------------------------------------------------------------------
QString ISBuffer::GetAudio(const QString &AudioName)
{
	return Audios.value(AudioName);
}
//-----------------------------------------------------------------------------
void ISBuffer::InitializeAnimations()
{
	ISCountingTime Time;

	QFileInfoList FileInfoList = QDir(":Animation").entryInfoList(QDir::NoFilter);
	for (const QFileInfo &FileInfo : FileInfoList)
	{
		QString FileName = FileInfo.completeBaseName();
		QString FilePath = FileInfo.filePath();

		AddAnimations(FileName, FilePath);
	}

	int MsecAnimations = Time.GetElapsed();
	ISDebug::ShowDebugString(QString("Buffer animation initialized. msec: %1. Items: %2").arg(MsecAnimations).arg(Animations.count()));
}
//-----------------------------------------------------------------------------
void ISBuffer::InitializeIcons()
{
	ISCountingTime Time;
	
	QFileInfoList FileInfoList = QDir(":ImageIcons").entryInfoList(QDir::NoFilter);
	for (const QFileInfo &FileInfo : FileInfoList)
	{
		QString FileName = FileInfo.completeBaseName();
		QString FilePath = FileInfo.filePath();

		AddImageIcon(FileName, FilePath);
	}

	int MsecIcons = Time.GetElapsed();
	ISDebug::ShowDebugString(QString("Buffer icons initialized. msec: %1. Items: %2").arg(MsecIcons).arg(Icons.count()));
}
//-----------------------------------------------------------------------------
void ISBuffer::InitializePixmaps()
{
	ISCountingTime Time;
	
	QFileInfoList FileInfoList = QDir(":Images").entryInfoList(QDir::NoFilter);
	for (const QFileInfo &FileInfo : FileInfoList)
	{
		QString FileName = FileInfo.completeBaseName();
		QString FilePath = FileInfo.filePath();

		AddImage(FileName, FilePath);
	}

	int MsecPixmaps = Time.GetElapsed();
	ISDebug::ShowDebugString(QString("Buffer images initialized. msec: %1. Items: %2").arg(MsecPixmaps).arg(Pixmaps.count()));
}
//-----------------------------------------------------------------------------
void ISBuffer::InitializeAudios()
{
	ISCountingTime Time;

	QFileInfoList FileInfoList = QDir(":Audio").entryInfoList(QDir::NoFilter);
	for (const QFileInfo &FileInfo : FileInfoList)
	{
		QString FileName = FileInfo.completeBaseName();
		QString FilePath = FileInfo.filePath();

		AddAudio(FileName, FilePath);
	}

	int MsecAudios = Time.GetElapsed();
	ISDebug::ShowDebugString(QString("Buffer audio initialized. msec: %1. Items: %2").arg(MsecAudios).arg(Audios.count()));
}
//-----------------------------------------------------------------------------
void ISBuffer::AddAnimations(const QString &AnimationName, const QString &AnimationPath)
{
	bool Contains = Animations.count(AnimationName);
	IS_ASSERT(!Contains, QString("Animation \"%1\" already exist in buffer animations").arg(AnimationName));

	QFile File(AnimationPath);

	QMovie Movie(&File);
	Movie.setObjectName(AnimationName);
	bool Valid = Movie.isValid();
	IS_ASSERT(Valid, "Animation invalid: " + AnimationName);

	Animations.insert(AnimationName, AnimationPath);
}
//-----------------------------------------------------------------------------
void ISBuffer::AddImageIcon(const QString &IconName, const QString &IconPath)
{
	bool Contains = Icons.contains(IconName);
	IS_ASSERT(!Contains, QString("Icon \"%1\" already exist in buffer icons").arg(IconName));

	QIcon Icon(IconPath);
	IS_ASSERT(!Icon.isNull(), "Null icon: " + IconName);
	Icons.insert(IconName, IconPath);
}
//-----------------------------------------------------------------------------
void ISBuffer::AddImage(const QString &PixmapName, const QString &PixmapPath)
{
	bool Contains = Pixmaps.contains(PixmapName);
	IS_ASSERT(!Contains, QString("Pixmap \"%1\" already exist in buffer pixmaps").arg(PixmapName));

	QPixmap Pixmap(PixmapPath);
	IS_ASSERT(!Pixmap.isNull(), "Null pixmap: " + PixmapName);
	Pixmaps.insert(PixmapName, PixmapPath);
}
//-----------------------------------------------------------------------------
void ISBuffer::AddAudio(const QString &AudioName, const QString &AudioPath)
{
	bool Contains = Audios.contains(AudioName);
	IS_ASSERT(!Contains, QString("Audio \"%1\" already exist in buffer audios.").arg(AudioName));

	Audios.insert(AudioName, AudioPath);
}
//-----------------------------------------------------------------------------
