#pragma once
//-----------------------------------------------------------------------------
#include "isresources_global.h"
//-----------------------------------------------------------------------------
class ISRESOURCES_EXPORT ISStyleSheet : public QObject
{
	Q_OBJECT

public:
	ISStyleSheet(const ISStyleSheet &) = delete;
	ISStyleSheet(ISStyleSheet &&) = delete;
	ISStyleSheet &operator=(const ISStyleSheet &) = delete;
	ISStyleSheet &operator=(ISStyleSheet &&) = delete;
	~ISStyleSheet();

	static ISStyleSheet& GetInstance();
	QString GetStyle(const QString &StyleName, const QString &SourceFile, int FileLine) const;

protected:
	void Initialize();
	void AddStyle(const QString &FileName, const QString &FilePath);

private:
	ISStyleSheet();

	QMap<QString, QString> StyleSheets;
};
//-----------------------------------------------------------------------------
#define STYLE_SHEET(STYLE_NAME) ISStyleSheet::GetInstance().GetStyle(STYLE_NAME, __FILE__, __LINE__)
//-----------------------------------------------------------------------------
