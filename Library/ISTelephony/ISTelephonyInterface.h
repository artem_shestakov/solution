#pragma once
//-----------------------------------------------------------------------------
#include "istelephony_global.h"
#include "ISTelephonyBase.h"
//-----------------------------------------------------------------------------
class ISTELEPHONY_EXPORT ISTelephonyInterface : public QObject
{
	Q_OBJECT

public:
	ISTelephonyInterface(const ISTelephonyInterface &) = delete;
	ISTelephonyInterface(ISTelephonyInterface &&) = delete;
	ISTelephonyInterface &operator=(const ISTelephonyInterface &) = delete;
	ISTelephonyInterface &operator=(ISTelephonyInterface &&) = delete;
	virtual ~ISTelephonyInterface();

	static ISTelephonyInterface& GetInstance();
	
	ISTelephonyBase* GetPointer(); //�������� �������� �� ��������� ���������
	void SetPointer(ISTelephonyBase *telephony_base); //�������� ��������� �� ��������� ���������

private:
	ISTelephonyInterface();

	ISTelephonyBase *TelephonyBase;
};
//-----------------------------------------------------------------------------
