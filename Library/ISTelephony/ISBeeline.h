#pragma once
//-----------------------------------------------------------------------------
#include "istelephony_global.h"
#include "ISTelephonyBase.h"
//-----------------------------------------------------------------------------
class ISTELEPHONY_EXPORT ISBeeline : public ISTelephonyBase
{
	Q_OBJECT

public:
	Q_INVOKABLE ISBeeline();
	virtual ~ISBeeline();

	bool Initialize() override;
	void CallPhone(const QString &PhoneNumber) override;

protected:
	void NetworkReplyError(QNetworkReply::NetworkError ReplyError);
	void NetworkReplyFinished();

	void NetworkAccessManagerFinished(QNetworkReply *NetworkReply);

private:
	QNetworkAccessManager *NetworkAccessManager;
	QNetworkReply *NetworkReply;
};
//-----------------------------------------------------------------------------
