#include "StdAfx.h"
#include "ISTelephonyInterface.h"
//-----------------------------------------------------------------------------
ISTelephonyInterface::ISTelephonyInterface() : QObject()
{
	TelephonyBase = nullptr;
}
//-----------------------------------------------------------------------------
ISTelephonyInterface::~ISTelephonyInterface()
{

}
//-----------------------------------------------------------------------------
ISTelephonyInterface& ISTelephonyInterface::GetInstance()
{
	static ISTelephonyInterface TelephonyInterface;
	return TelephonyInterface;
}
//-----------------------------------------------------------------------------
ISTelephonyBase* ISTelephonyInterface::GetPointer()
{
	return TelephonyBase;
}
//-----------------------------------------------------------------------------
void ISTelephonyInterface::SetPointer(ISTelephonyBase *telephony_base)
{
	TelephonyBase = telephony_base;
}
//-----------------------------------------------------------------------------
