#pragma once
//-----------------------------------------------------------------------------
#include "istelephony_global.h"
#include "ISTelephonyBase.h"
//-----------------------------------------------------------------------------
class ISTELEPHONY_EXPORT ISAsterisk : public ISTelephonyBase
{
	Q_OBJECT

public:
	Q_INVOKABLE ISAsterisk();
	virtual ~ISAsterisk();

	bool Initialize() override;
	void CallPhone(const QString &PhoneNumber) override;

protected:
	void Connected();
	void Error(QAbstractSocket::SocketError Error);
	void ReadyRead();
	QMap<QString, QVariantMap> ParseIncomingCall(const QString &String);

private:
	QTcpSocket *TcpSocket;
	QString ID; //�������������
};
//-----------------------------------------------------------------------------
