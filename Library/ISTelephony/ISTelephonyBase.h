#pragma once
//-----------------------------------------------------------------------------
#include "istelephony_global.h"
//-----------------------------------------------------------------------------
class ISTELEPHONY_EXPORT ISTelephonyBase : public QObject
{
	Q_OBJECT

signals:
	void IncomingCall(const QString &Number);

public:
	ISTelephonyBase();
	virtual ~ISTelephonyBase();

	virtual bool Initialize() = 0;
	virtual void CallPhone(const QString &PhoneNumber) = 0;

	QString GetErrorString() const;

	void SetUserName(const QString &user_name);
	QString GetUserName() const;

	QString GetServer() const;
	void SetServer(const QString &server);

	int GetPort() const;
	void SetPort(int port);

	QString GetLogin() const;
	void SetLogin(const QString &login);

	QString GetPassword() const;
	void SetPassword(const QString &password);

	int GetPattern() const;
	void SetPattern(int pattern);

protected:
	void SetErrorString(const QString &error_string);

private:
	QString ErrorString;

	QString UserName;
	QString Server;
	int Port;
	QString Login;
	QString Password;
	int Pattern;
};
//-----------------------------------------------------------------------------
