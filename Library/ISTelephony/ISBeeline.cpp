#include "StdAfx.h"
#include "ISBeeline.h"
#include "ISDebug.h"
//-----------------------------------------------------------------------------
ISBeeline::ISBeeline() : ISTelephonyBase()
{
	NetworkAccessManager = new QNetworkAccessManager(this);
	connect(NetworkAccessManager, &QNetworkAccessManager::finished, this, &ISBeeline::NetworkAccessManagerFinished);

	NetworkReply = nullptr;
}
//-----------------------------------------------------------------------------
ISBeeline::~ISBeeline()
{

}
//-----------------------------------------------------------------------------
bool ISBeeline::Initialize()
{
	return true;
}
//-----------------------------------------------------------------------------
void ISBeeline::CallPhone(const QString &PhoneNumber)
{
	QString UrlText = QString("https://cloudpbx.beeline.ru/apis/portal/abonents/%1/call?phoneNumber=%2").arg(GetPattern()).arg(PhoneNumber);

	QNetworkRequest Request;
	Request.setUrl(QUrl(UrlText));
	Request.setRawHeader(QByteArray("X-MPBX-API-AUTH-TOKEN"), QByteArray("0bde3053-f09b-4b1b-9259-f2c5aa791985"));
	Request.setRawHeader(QByteArray("Content-Type"), QByteArray("application/json"));

	NetworkReply = NetworkAccessManager->post(Request, QByteArray());
	connect(NetworkReply, static_cast<void(QNetworkReply::*)(QNetworkReply::NetworkError)>(&QNetworkReply::error), this, &ISBeeline::NetworkReplyError);
	connect(NetworkReply, &QNetworkReply::finished, this, &ISBeeline::NetworkReplyFinished);
}
//-----------------------------------------------------------------------------
void ISBeeline::NetworkReplyError(QNetworkReply::NetworkError ReplyError)
{
	if (ReplyError != QNetworkReply::NoError)
	{
		ISDebug::ShowCriticalString(NetworkReply->errorString());
	}
}
//-----------------------------------------------------------------------------
void ISBeeline::NetworkReplyFinished()
{
	if (NetworkReply->error() != QNetworkReply::NoError)
	{
		ISDebug::ShowCriticalString(NetworkReply->errorString());
	}

	delete NetworkReply;
	NetworkReply = nullptr;
}
//-----------------------------------------------------------------------------
void ISBeeline::NetworkAccessManagerFinished(QNetworkReply *NetworkReply)
{
	QString ReplyString = NetworkReply->readAll();
	QStringList StringList = ReplyString.split(":");
	if (StringList.count() != 2)
	{
		ISDebug::ShowCriticalString(NetworkReply->errorString());
	}
}
//-----------------------------------------------------------------------------
