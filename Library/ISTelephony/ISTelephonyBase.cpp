#include "StdAfx.h"
#include "ISTelephonyBase.h"
#include "EXDefines.h"
#include "ISAssert.h"
//-----------------------------------------------------------------------------
ISTelephonyBase::ISTelephonyBase() : QObject()
{
	
}
//-----------------------------------------------------------------------------
ISTelephonyBase::~ISTelephonyBase()
{

}
//-----------------------------------------------------------------------------
QString ISTelephonyBase::GetErrorString() const
{
	return ErrorString;
}
//-----------------------------------------------------------------------------
void ISTelephonyBase::SetUserName(const QString &user_name)
{
	UserName = user_name;
}
//-----------------------------------------------------------------------------
QString ISTelephonyBase::GetUserName() const
{
	return UserName;
}
//-----------------------------------------------------------------------------
QString ISTelephonyBase::GetServer() const
{
	return Server;
}
//-----------------------------------------------------------------------------
void ISTelephonyBase::SetServer(const QString &server)
{
	Server = server;
}
//-----------------------------------------------------------------------------
int ISTelephonyBase::GetPort() const
{
	return Port;
}
//-----------------------------------------------------------------------------
void ISTelephonyBase::SetPort(int port)
{
	Port = port;
}
//-----------------------------------------------------------------------------
QString ISTelephonyBase::GetLogin() const
{
	return Login;
}
//-----------------------------------------------------------------------------
void ISTelephonyBase::SetLogin(const QString &login)
{
	Login = login;
}
//-----------------------------------------------------------------------------
QString ISTelephonyBase::GetPassword() const
{
	return Password;
}
//-----------------------------------------------------------------------------
void ISTelephonyBase::SetPassword(const QString &password)
{
	Password = password;
}
//-----------------------------------------------------------------------------
int ISTelephonyBase::GetPattern() const
{
	return Pattern;
}
//-----------------------------------------------------------------------------
void ISTelephonyBase::SetPattern(int pattern)
{
	Pattern = pattern;
}
//-----------------------------------------------------------------------------
void ISTelephonyBase::SetErrorString(const QString &error_string)
{
	ErrorString = error_string;
}
//-----------------------------------------------------------------------------
