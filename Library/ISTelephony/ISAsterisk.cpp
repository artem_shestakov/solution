#include "StdAfx.h"
#include "ISAsterisk.h"
#include "ISSystem.h"
//-----------------------------------------------------------------------------
ISAsterisk::ISAsterisk() : ISTelephonyBase()
{
	ID = ISSystem::GenerateUuid();

	TcpSocket = new QTcpSocket(this);
	connect(TcpSocket, &QTcpSocket::connected, this, &ISAsterisk::Connected);
	connect(TcpSocket, &QTcpSocket::readyRead, this, &ISAsterisk::ReadyRead);
	connect(TcpSocket, static_cast<void(QTcpSocket::*)(QAbstractSocket::SocketError)>(&QTcpSocket::error), this, &ISAsterisk::Error);
}
//-----------------------------------------------------------------------------
ISAsterisk::~ISAsterisk()
{

}
//-----------------------------------------------------------------------------
bool ISAsterisk::Initialize()
{
	QEventLoop EventLoop;
	connect(TcpSocket, &QTcpSocket::connected, &EventLoop, &QEventLoop::quit);

	TcpSocket->connectToHost(GetServer(), GetPort());
	EventLoop.exec();

	if (TcpSocket->state() != QAbstractSocket::ConnectedState)
	{
		SetErrorString(TcpSocket->errorString());
		return false;
	}

	return true;
}
//-----------------------------------------------------------------------------
void ISAsterisk::CallPhone(const QString &PhoneNumber)
{
	QString NumberString = PhoneNumber;
	NumberString = NumberString.replace("+", QString());
	NumberString = NumberString.replace(" ", QString());
	NumberString = NumberString.replace("(", QString());
	NumberString = NumberString.replace(")", QString());
	NumberString = NumberString.replace("-", QString());

	TcpSocket->write(QString("Action:Originate\r\n").toUtf8().data());
	TcpSocket->write(QString("Channel:SIP/%1\r\n").arg(GetPattern()).toUtf8().data());
	TcpSocket->write(QString("Context:out\r\n").toUtf8().data());
	TcpSocket->write(QString("Exten:%1\r\n").arg(NumberString).toUtf8().data());
	TcpSocket->write(QString("Priority:1\r\n").toUtf8().data());
	TcpSocket->write(QString("Callerid:%1\r\n").arg(GetPattern()).toUtf8().data());
	TcpSocket->write(QString("ActionID:%1\r\n").arg(ID).toUtf8().data());
	TcpSocket->write("\r\n");
}
//-----------------------------------------------------------------------------
void ISAsterisk::Connected()
{
	if (TcpSocket->state() == QAbstractSocket::ConnectedState)
	{
		TcpSocket->write(QString("Action: Login\r\n").toUtf8().data());
		TcpSocket->write(QString("ActionID: %1\r\n").arg(ID).toUtf8().data());
		TcpSocket->write(QString("Username: %2\r\n").arg(GetLogin()).toUtf8().data());
		TcpSocket->write(QString("Secret: %2\r\n").arg(GetPassword()).toUtf8().data());
		TcpSocket->write("\r\n");
	}
}
//-----------------------------------------------------------------------------
void ISAsterisk::Error(QAbstractSocket::SocketError Error)
{
	SetErrorString(TcpSocket->errorString());
}
//-----------------------------------------------------------------------------
void ISAsterisk::ReadyRead()
{
	QMap<QString, QVariantMap> VariantMap = ParseIncomingCall(TcpSocket->readAll());
	if (VariantMap.count())
	{
		QVariantMap EventMap = VariantMap.value("UserEvent");
		if (EventMap.count())
		{
			if (EventMap.contains("Number"))
			{
				emit IncomingCall(EventMap.value("Number").toString());
			}
		}
	}
}
//-----------------------------------------------------------------------------
QMap<QString, QVariantMap> ISAsterisk::ParseIncomingCall(const QString &String)
{
	QMap<QString, QVariantMap> Map;
	bool UserEvent = false;

	QStringList MessageList = String.split("\r\n\r\n");
	for (int i = 0; i < MessageList.count(); i++) //����� ��� ���������
	{
		QString Message = MessageList.at(i);
		if (!Message.length())
		{
			continue;
		}

		QVariantMap VariantMap;
		QStringList StringList = Message.split("\n"); //������ ����� ���������

		while (StringList.count()) //����� ���������
		{
			QString LineString = StringList.takeFirst();
			ISSystem::RemoveLastSymbolFromString(LineString);
			QStringList LineList = LineString.split(": ");
			if (LineList.count() == 2)
			{
				VariantMap.insert(LineList.at(0), LineList.at(1));
			}
		}

		if (VariantMap.contains("Event")) //���� ��������� �������� ��������
		{
			Map.insert(VariantMap.value("Event").toString(), VariantMap);
		}
	}

	return Map;
}
//-----------------------------------------------------------------------------
