#pragma once
//-----------------------------------------------------------------------------
#include "isprinting_global.h"
#include "ISPrintingBase.h"
//-----------------------------------------------------------------------------
class ISPRINTING_EXPORT ISPrintingHtml : public ISPrintingBase
{
	Q_OBJECT

	Q_PROPERTY(bool PDF WRITE SetPDF)
	Q_PROPERTY(QString PathPDF WRITE SetPathPDF)
	Q_PROPERTY(bool EditPreview WRITE SetEditPreview)

public:
	ISPrintingHtml(ISPrintMetaReport *meta_report, int object_id, QObject *parent = 0);
	virtual ~ISPrintingHtml();

	bool Prepare() override;
	bool PrepareTempate() override;
	bool FillTemplate() override;
	bool PreviewDocument() override;
	bool Print() override;

	void SetPDF(bool pdf);
	void SetPathPDF(const QString &path_pdf);
	void SetEditPreview(bool edit_preview);

private:
	bool PDF;
	QString PathPDF;
	bool EditPreview;

	QString Html;
};
//-----------------------------------------------------------------------------
