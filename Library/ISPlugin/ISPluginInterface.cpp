#include "StdAfx.h"
#include "ISPluginInterface.h"
#include "ISPlugin.h"
//-----------------------------------------------------------------------------
ISPluginInterface::ISPluginInterface()
{

}
//-----------------------------------------------------------------------------
ISPluginInterface::~ISPluginInterface()
{

}
//-----------------------------------------------------------------------------
bool ISPluginInterface::IsPlugin() const
{
	return true;
}
//-----------------------------------------------------------------------------
QIcon ISPluginInterface::GetIcon(const QString &IconName) const
{
	QIcon Icon(":Resources/" + IconName + ".png");
	return Icon;
}
//-----------------------------------------------------------------------------
QString ISPluginInterface::GetLocalization() const
{
	QString Content;

	QFile File(":Resources/" + ISPlugin::GetInstance().GetName() + ".lang");
	if (File.open(QIODevice::ReadOnly))
	{
		Content = File.readAll();
		File.close();
	}

	return Content;
}
//-----------------------------------------------------------------------------
QStringList ISPluginInterface::GetScheme(const QStringList &Filter) const
{
	QStringList Scheme;

	QFileInfoList FileInfoList = QDir(":Resources").entryInfoList(Filter, QDir::NoFilter, QDir::SortFlag::Name);
	for (int i = 0; i < FileInfoList.count(); i++)
	{
		QString FilePath = FileInfoList.at(i).filePath();
		QFile File(FilePath);
		if (File.open(QIODevice::ReadOnly))
		{
			Scheme.append(File.readAll());
			File.close();
		}
		else
		{
			qDebug() << "Not open file " + FilePath + ". Error: " + File.errorString();
		}
	}

	return Scheme;
}
//-----------------------------------------------------------------------------
