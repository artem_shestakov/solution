#include "StdAfx.h"
#include "ISPlugin.h"
#include "ISConfig.h"
#include "ISAssert.h"
#include "EXConstants.h"
#include "EXDefines.h"
#include "ISLocalization.h"
#include "ISCountingTime.h"
#include "ISDebug.h"
#include "ISSystem.h"
//-----------------------------------------------------------------------------
ISPlugin::ISPlugin() : QObject()
{
	PluginInterface = nullptr;

	PluginPath = ISConfig::GetInstance().GetValue(CONST_CONFIG_OTHER_MODULE).toString();

	PluginLoader = new QPluginLoader(this);
	PluginLoader->setFileName(PluginPath);
}
//-----------------------------------------------------------------------------
ISPlugin::~ISPlugin()
{

}
//-----------------------------------------------------------------------------
ISPlugin& ISPlugin::GetInstance()
{
	static ISPlugin MetaPlugin;
	return MetaPlugin;
}
//-----------------------------------------------------------------------------
bool ISPlugin::Load()
{
	if (!QFile::exists(GetPath()))
	{
		ErrorString = LOCALIZATION("Message.Warning.NotFoundModuleFile").arg(GetPath());
		return false;
	}

	ISCountingTime CountingTime;
	ISDebug::ShowInfoString(QString("Loading module %1...").arg(ISSystem::GetFileName(PluginPath)));

	bool Result = PluginLoader->load();

	if (Result)
	{
		PluginInterface = qobject_cast<ISPluginInterface*>(PluginLoader->instance());
		IS_ASSERT(PluginInterface, QString("Not instance plugin: %1").arg(PluginLoader->errorString()));

		if (PluginInterface->IsPlugin())
		{
			VariantMap = PluginLoader->metaData().value("MetaData").toObject().toVariantMap();
			ISDebug::ShowInfoString(QString("Loading module %1 done. %2 msec").arg(ISSystem::GetFileName(PluginPath)).arg(CountingTime.GetElapsed()));
		}
		else
		{
			Result = false;
			ErrorString = LOCALIZATION("ModuleIsNotPlugin");
		}
	}
	else
	{
		ErrorString = PluginLoader->errorString();
		ISDebug::ShowInfoString(QString("Loading module %1 error: %2").arg(ISSystem::GetFileName(PluginPath)).arg(ErrorString));
	}
	
	return Result;
}
//-----------------------------------------------------------------------------
ISPluginInterface* ISPlugin::GetPluginInterface()
{
	return PluginInterface;
}
//-----------------------------------------------------------------------------
ISUuid ISPlugin::GetUID() const
{
	return VariantMap.value("UID").toString();
}
//-----------------------------------------------------------------------------
QString ISPlugin::GetLocalName() const
{
	return VariantMap.value("LocalName").toString();
}
//-----------------------------------------------------------------------------
QString ISPlugin::GetDesktopForm() const
{
	return VariantMap.value("DesktopForm").toString();
}
//-----------------------------------------------------------------------------
QString ISPlugin::GetName() const
{
	return PluginLoader->metaData().toVariantMap().value("IID").toString();
}
//-----------------------------------------------------------------------------
QString ISPlugin::GetFileName() const
{
	QFileInfo FileInfo(PluginLoader->fileName());
	return FileInfo.fileName();
}
//-----------------------------------------------------------------------------
QString ISPlugin::GetPath() const
{
	return PluginPath;
}
//-----------------------------------------------------------------------------
QString ISPlugin::GetErrorString() const
{
	return ErrorString;
}
//-----------------------------------------------------------------------------
