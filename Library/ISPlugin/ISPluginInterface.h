#pragma once
//-----------------------------------------------------------------------------
#include "isplugin_global.h"
//-----------------------------------------------------------------------------
class ISPLUGIN_EXPORT ISPluginInterface
{
public:
	ISPluginInterface();
	virtual ~ISPluginInterface();

	virtual void RegisterMetaTypes() const = 0;

	bool IsPlugin() const;
	QIcon GetIcon(const QString &IconName) const;
	QString GetLocalization() const;
	QStringList GetScheme(const QStringList &Filter) const;
};
//-----------------------------------------------------------------------------
Q_DECLARE_INTERFACE(ISPluginInterface, "ru.itnotesblog.IntegralSystem.PluginInterface/1.0")
//-----------------------------------------------------------------------------
