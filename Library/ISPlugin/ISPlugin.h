#pragma once
//-----------------------------------------------------------------------------
#include "isplugin_global.h"
#include "ISPluginInterface.h"
#include "ISUuid.h"
//-----------------------------------------------------------------------------
class ISPLUGIN_EXPORT ISPlugin : public QObject
{
	Q_OBJECT

public:
	ISPlugin(const ISPlugin &) = delete;
	ISPlugin(ISPlugin &&) = delete;
	ISPlugin &operator=(const ISPlugin &) = delete;
	ISPlugin &operator=(ISPlugin &&) = delete;
	~ISPlugin();

	static ISPlugin& GetInstance();

	bool Load(); //�������� �������

	ISPluginInterface* GetPluginInterface(); //�������� ��������� �� ���� �������
	ISUuid GetUID() const; //�������� ���������� ������������� �������
	QString GetLocalName() const; //�������� ��������� ������������ �������
	QString GetDesktopForm() const; //�������� ������������ ����� �������� �����
	QString GetFileName() const; //������� ������������ ����� �������
	QString GetName() const; //������� ������������ �������
	QString GetPath() const; //�������� ���� � ������

	QString GetErrorString() const; //�������� ����� � �������

private:
	ISPlugin();

	QPluginLoader *PluginLoader;
	ISPluginInterface *PluginInterface;
	QVariantMap VariantMap;

	QString PluginPath;
	QString ErrorString;
};
//-----------------------------------------------------------------------------
