#pragma once
//-----------------------------------------------------------------------------
#include "isconfigurator_global.h"
#include "EXDefines.h"
#include "ISDebug.h"
#include "ISSystem.h"
#include "ISMetaData.h"
#include "ISConfig.h"
#include "ISCountingTime.h"
#include "ISAssert.h"
//-----------------------------------------------------------------------------
class ISCONFIGURATOR_EXPORT ISConfiguratorBase : public QObject
{
	Q_OBJECT

signals:
	void ProgressSignal(int Step, int StepCount);

public:
	ISConfiguratorBase();
	virtual ~ISConfiguratorBase();

protected:
	void Progress(int Step, int Steps);
};
//-----------------------------------------------------------------------------
