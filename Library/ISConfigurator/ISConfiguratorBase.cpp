#include "StdAfx.h"
#include "ISConfiguratorBase.h"
//-----------------------------------------------------------------------------
ISConfiguratorBase::ISConfiguratorBase() : QObject()
{
	
}
//-----------------------------------------------------------------------------
ISConfiguratorBase::~ISConfiguratorBase()
{

}
//-----------------------------------------------------------------------------
void ISConfiguratorBase::Progress(int Step, int Steps)
{
	int NewStep = Step;
	NewStep++;

	//QString Message = "TITLE " + StepName + ": " + QString::number(NewStep) + " or " + QString::number(Steps);
	emit ProgressSignal(NewStep, Steps);
	//system(Message.toLatin1().data());
}
//-----------------------------------------------------------------------------
