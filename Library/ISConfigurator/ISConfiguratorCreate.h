#pragma once
//-----------------------------------------------------------------------------
#include "isconfigurator_global.h"
#include "ISConfiguratorBase.h"
//-----------------------------------------------------------------------------
class ISCONFIGURATOR_EXPORT ISConfiguratorCreate : public ISConfiguratorBase
{
	Q_OBJECT

public:
	Q_INVOKABLE ISConfiguratorCreate();
	virtual ~ISConfiguratorCreate();

public slots:
	void License();
};
//-----------------------------------------------------------------------------
