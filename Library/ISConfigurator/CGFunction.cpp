#include "StdAfx.h"
#include "CGFunction.h"
#include "ISQuery.h"
//-----------------------------------------------------------------------------
static QString QS_FUNCTIONS = PREPARE_QUERY("SELECT COUNT(*) FROM pg_proc WHERE proname = :FunctionName");
//-----------------------------------------------------------------------------
CGFunction::CGFunction()
{

}
//-----------------------------------------------------------------------------
CGFunction::~CGFunction()
{

}
//-----------------------------------------------------------------------------
bool CGFunction::CreateFunction(const QString &FunctionName, const QString &QueryFunction, QString &ErrorString)
{
	QString SqlQuery = QueryFunction.arg(FunctionName);

	ISQuery qCreateFunction;
	bool Created = qCreateFunction.Execute(SqlQuery);
	if (!Created)
	{
		ErrorString = qCreateFunction.GetSqlQuery().lastError().text();
	}

	return Created;
}
//-----------------------------------------------------------------------------
bool CGFunction::CheckExistFunction(const QString &FunctionName)
{
	ISQuery qSelectFunctions(QS_FUNCTIONS);
	qSelectFunctions.BindValue(":FunctionName", FunctionName);
	if (qSelectFunctions.ExecuteFirst())
	{
		int Count = qSelectFunctions.ReadColumn("count").toInt();
		if (Count == 1)
		{
			return true;
		}
	}

	return false;
}
//-----------------------------------------------------------------------------
