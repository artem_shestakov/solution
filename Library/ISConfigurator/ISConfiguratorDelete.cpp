#include "StdAfx.h"
#include "ISConfiguratorDelete.h"
#include "ISQuery.h"
#include "ISQueryText.h"
//-----------------------------------------------------------------------------
static QString QS_INDEXES = PREPARE_QUERY("SELECT indexname FROM pg_indexes WHERE schemaname = current_schema()");
//-----------------------------------------------------------------------------
static QString QD_INDEX = "DROP INDEX public.%1";
//-----------------------------------------------------------------------------
static QString QS_FOREIGNS = PREPARE_QUERY("SELECT constraint_name FROM information_schema.constraint_table_usage WHERE table_catalog = current_database() AND table_schema = current_schema()");
//-----------------------------------------------------------------------------
static QString QD_FOREIGN = "ALTER TABLE public.%1 DROP CONSTRAINT %2 RESTRICT";
//-----------------------------------------------------------------------------
static QString QD_SYSTEMS = PREPARE_QUERY("DELETE FROM _systems");
//-----------------------------------------------------------------------------
static QString QD_SUB_SYSTEMS = PREPARE_QUERY("DELETE FROM _subsystems");
//-----------------------------------------------------------------------------
static QString QD_LICENSE = PREPARE_QUERY("DELETE FROM _license");
//-----------------------------------------------------------------------------
ISConfiguratorDelete::ISConfiguratorDelete() : ISConfiguratorBase()
{

}
//-----------------------------------------------------------------------------
ISConfiguratorDelete::~ISConfiguratorDelete()
{

}
//-----------------------------------------------------------------------------
void ISConfiguratorDelete::Indexes()
{
	ISDebug::ShowDebugString("Deleting indexes...");
	ISSystem::SleepMilliseconds(ONE_SECOND_TO_MILLISECOND);

	ISQuery qSelectIndexes(QS_INDEXES);
	if (qSelectIndexes.Execute())
	{
		int Deleted = 0;
		int CountIndexes = qSelectIndexes.GetCountResultRows();

		while (qSelectIndexes.Next())
		{
			QString IndexName = qSelectIndexes.ReadColumn("indexname").toString();

			ISQuery qDeleteIndex;
			if (qDeleteIndex.Execute(QD_INDEX.arg(IndexName)))
			{
				Deleted++;
			}
		}

		ISDebug::ShowInfoString("Deleted " + QString::number(Deleted) + " or " + QString::number(CountIndexes) + " indexes");
	}
}
//-----------------------------------------------------------------------------
void ISConfiguratorDelete::Foreigns()
{
	ISDebug::ShowDebugString("Deleting foreigns...");
	ISSystem::SleepMilliseconds(ONE_SECOND_TO_MILLISECOND);

	ISQuery qSelectForeigns(QS_FOREIGNS);
	if (qSelectForeigns.Execute())
	{
		int Deleted = 0;
		int CountForeigns = qSelectForeigns.GetCountResultRows();

		while (qSelectForeigns.Next())
		{
			QString ForeignName = qSelectForeigns.ReadColumn("constraint_name").toString();
			QString TableName;

			QStringList StringList = ForeignName.split("_");
			if (StringList[0].length())
			{
				TableName = StringList[0];
			}
			else
			{
				TableName = "_" + StringList[1];
			}


			ISQuery qDeleteForeign;
			if (qDeleteForeign.Execute(QD_FOREIGN.arg(TableName).arg(ForeignName)))
			{
				Deleted++;
			}
		}

		ISDebug::ShowInfoString("Deleted " + QString::number(Deleted) + " or " + QString::number(CountForeigns) + " foreigns");
	}
}
//-----------------------------------------------------------------------------
void ISConfiguratorDelete::Systems()
{
	ISDebug::ShowDebugString("Deleting systems...");
	ISSystem::SleepMilliseconds(ONE_SECOND_TO_MILLISECOND);

	ISQuery qDelete(QD_SYSTEMS);
	if (qDelete.Execute())
	{
		ISDebug::ShowInfoString("Deleting all systems done");
	}
}
//-----------------------------------------------------------------------------
void ISConfiguratorDelete::SubSystems()
{
	ISDebug::ShowDebugString("Deleting subsystems...");
	ISSystem::SleepMilliseconds(ONE_SECOND_TO_MILLISECOND);

	ISQuery qDelete(QD_SUB_SYSTEMS);
	if (qDelete.Execute())
	{
		ISDebug::ShowInfoString("Deleting all subsystems done");
	}
}
//-----------------------------------------------------------------------------
void ISConfiguratorDelete::License()
{
	ISDebug::ShowDebugString("Deleting license...");
	ISSystem::SleepMilliseconds(ONE_SECOND_TO_MILLISECOND);

	ISQuery qDelete(QD_LICENSE);
	if (qDelete.Execute())
	{
		ISDebug::ShowInfoString("Deleting license done");
	}
}
//-----------------------------------------------------------------------------
