#include "StdAfx.h"
#include "ISConfiguratorOther.h"
#include "ISDatabaseServiceSettingsForm.h"
//-----------------------------------------------------------------------------
ISConfiguratorOther::ISConfiguratorOther() : ISConfiguratorBase()
{

}
//-----------------------------------------------------------------------------
ISConfiguratorOther::~ISConfiguratorOther()
{

}
//-----------------------------------------------------------------------------
void ISConfiguratorOther::DatabaseService()
{
	ISDatabaseServiceSettingsForm DatabaseServiceSettingsForm;
	DatabaseServiceSettingsForm.Exec();
}
//-----------------------------------------------------------------------------
