#pragma once
//-----------------------------------------------------------------------------
#include "isconfigurator_global.h"
#include "ISConfiguratorBase.h"
//-----------------------------------------------------------------------------
class ISCONFIGURATOR_EXPORT ISConfiguratorDelete : public ISConfiguratorBase
{
	Q_OBJECT

public:
	Q_INVOKABLE ISConfiguratorDelete();
	virtual ~ISConfiguratorDelete();

public slots:
	void Indexes();
	void Foreigns();
	void Systems();
	void SubSystems();
	void License();
};
//-----------------------------------------------------------------------------
