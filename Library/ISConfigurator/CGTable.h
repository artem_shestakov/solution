#pragma once
//-----------------------------------------------------------------------------
#include "isconfigurator_global.h"
#include "PMetaClassTable.h"
//-----------------------------------------------------------------------------
class ISCONFIGURATOR_EXPORT CGTable : public QObject
{
	Q_OBJECT

public:
	CGTable();
	virtual ~CGTable();

	static bool CreateTable(PMetaClassTable *MetaTable, QString &ErrorString);
	static bool UpdateTable(PMetaClassTable *MetaTable);
	static bool CheckExistTable(PMetaClassTable *MetaTable);
};
//-----------------------------------------------------------------------------
