#pragma once
//-----------------------------------------------------------------------------
#include "ISInterfaceDialogForm.h"
#include "ISIPAddressEdit.h"
#include "ISIntegerEdit.h"
#include "ISLineEdit.h"
#include "ISPasswordEdit.h"
#include "ISPathEditDir.h"
//-----------------------------------------------------------------------------
class ISDatabaseServiceSettingsForm : public ISInterfaceDialogForm
{
	Q_OBJECT

public:
	ISDatabaseServiceSettingsForm(QWidget *parent = 0);
	virtual ~ISDatabaseServiceSettingsForm();

protected:
	void Save();

private:
	ISIPAddressEdit *EditServer;
	ISIntegerEdit *EditPort;
	ISLineEdit *EditDatabase;
	ISLineEdit *EditLogin;
	ISPasswordEdit *EditPassword;
	ISPathEditDir *EditBackupDir;
	ISPathEditDir *EditPostgresDir;
	ISIntegerEdit *EditCompression;
};
//-----------------------------------------------------------------------------
