#include "StdAfx.h"
#include "CGTemplateField.h"
#include "EXDefines.h"
#include "ISSystem.h"
#include "CGSequence.h"
#include "ISMetaData.h"
//-----------------------------------------------------------------------------
CGTemplateField::CGTemplateField() : QObject()
{

}
//-----------------------------------------------------------------------------
CGTemplateField::~CGTemplateField()
{

}
//-----------------------------------------------------------------------------
QString CGTemplateField::GetSqlTextForTemplateSystemFields(const QString &ClassName, const QString &ClassAlias)
{
	QString SqlText = QString();

	QDomElement Root = ISSystem::GetDomElement(QFile(SCHEMA_TEMPLATE_FIELDS_PATH));
	QDomNode NodeTemplateFields = Root.firstChild();

	while (!NodeTemplateFields.isNull())
	{
		QString FieldName = NodeTemplateFields.attributes().namedItem("Name").nodeValue();
		QString FieldType = ISMetaData::GetInstanse().GetAssociationTypes().GetTypeDB(NodeTemplateFields.attributes().namedItem("Type").nodeValue());
		QString FieldLocalListName = NodeTemplateFields.attributes().namedItem("LocalListName").nodeValue();
		QString FieldSequence = NodeTemplateFields.attributes().namedItem("Sequence").nodeValue();
		QString FieldDefaultValue = NodeTemplateFields.attributes().namedItem("DefaultValue").nodeValue();
		QString FieldNotNull = NodeTemplateFields.attributes().namedItem("NotNull").nodeValue();

		SqlText += ClassAlias + "_" + FieldName.toLower() + " " + FieldType;

		if (FieldSequence == "true")
		{
			SqlText += " DEFAULT nextval('" + CGSequence::GetSequenceNameForClass(ClassName) + "'::regclass)";
		}

		if (!FieldDefaultValue.isEmpty())
		{
			SqlText += " DEFAULT " + FieldDefaultValue;
		}

		if (FieldNotNull == "true")
		{
			SqlText += " NOT NULL";
		}

		SqlText += ",\n";

		NodeTemplateFields = NodeTemplateFields.nextSibling();
	}

	return SqlText;
}
//-----------------------------------------------------------------------------
