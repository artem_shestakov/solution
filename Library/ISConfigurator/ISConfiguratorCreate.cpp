#include "StdAfx.h"
#include "ISConfiguratorCreate.h"
#include "ISDatabase.h"
#include "EXConstants.h"
#include "ISQuery.h"
#include "ISQueryText.h"
#include "ISCrypterLicense.h"
#include "ISLocalization.h"
//-----------------------------------------------------------------------------
static QString QS_LICENSE = PREPARE_QUERY("SELECT COUNT(*) FROM _license");
//-----------------------------------------------------------------------------
static QString QI_LICENSE = PREPARE_QUERY("INSERT INTO _license(lcns_counter) VALUES(:Counter)");
//-----------------------------------------------------------------------------
ISConfiguratorCreate::ISConfiguratorCreate() : ISConfiguratorBase()
{

}
//-----------------------------------------------------------------------------
ISConfiguratorCreate::~ISConfiguratorCreate()
{

}
//-----------------------------------------------------------------------------
void ISConfiguratorCreate::License()
{
	ISDebug::ShowDebugString("Creating license...");
	ISSystem::SleepMilliseconds(ONE_SECOND_TO_MILLISECOND);

	ISQuery qSelect(QS_LICENSE);
	if (qSelect.ExecuteFirst())
	{
		int Count = qSelect.ReadColumn("count").toInt();
		if (Count)
		{
			ISDebug::ShowInfoString("License already exist");
		}
		else
		{
			QString Counter = QString::number(LICENSE_COUNTER);
			Counter = ISCrypterLicense::Crypt(Counter);

			ISQuery qInsertLicense(QI_LICENSE);
			qInsertLicense.BindValue(":Counter", Counter);
			bool Execute = qInsertLicense.Execute();
			if (Execute)
			{
				ISDebug::ShowInfoString("License created");
			}
			else
			{
				IS_ASSERT(Execute, "Not executed query: " + QI_LICENSE + ". Error: " + qInsertLicense.GetSqlQuery().lastError().text());
			}
		}
	}
}
//-----------------------------------------------------------------------------
