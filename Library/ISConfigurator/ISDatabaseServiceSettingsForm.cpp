#include "StdAfx.h"
#include "ISDatabaseServiceSettingsForm.h"
#include "EXDefines.h"
#include "EXConstants.h"
#include "ISLocalization.h"
#include "ISButtonDialog.h"
#include "ISConfig.h"
//-----------------------------------------------------------------------------
ISDatabaseServiceSettingsForm::ISDatabaseServiceSettingsForm(QWidget *parent) : ISInterfaceDialogForm(parent)
{
	setWindowTitle(LOCALIZATION("SettingDatabaseService"));

	GetMainLayout()->setContentsMargins(LAYOUT_MARGINS_10_PX);

	QFormLayout *FormLayout = new QFormLayout();
	GetMainLayout()->addLayout(FormLayout);

	EditServer = new ISIPAddressEdit(this);
	EditServer->SetValue(ISConfig::GetInstance().GetValue(CONST_CONFIG_DATABASESERVICE_SERVER));
	FormLayout->addRow(LOCALIZATION("Server") + ":", EditServer);

	EditPort = new ISIntegerEdit(this);
	EditPort->SetValue(ISConfig::GetInstance().GetValue(CONST_CONFIG_DATABASESERVICE_PORT));
	FormLayout->addRow(LOCALIZATION("Port") + ":", EditPort);

	EditDatabase = new ISLineEdit(this);
	EditDatabase->SetValue(ISConfig::GetInstance().GetValue(CONST_CONFIG_DATABASESERVICE_DATABASE));
	FormLayout->addRow(LOCALIZATION("Database") + ":", EditDatabase);

	EditLogin = new ISLineEdit(this);
	EditLogin->SetValue(ISConfig::GetInstance().GetValue(CONST_CONFIG_DATABASESERVICE_LOGIN));
	FormLayout->addRow(LOCALIZATION("Login") + ":", EditLogin);

	EditPassword = new ISPasswordEdit(this);
	EditPassword->SetValue(ISConfig::GetInstance().GetValue(CONST_CONFIG_DATABASESERVICE_PASSWORD));
	FormLayout->addRow(LOCALIZATION("Password") + ":", EditPassword);

	EditBackupDir = new ISPathEditDir(this);
	EditBackupDir->SetValue(ISConfig::GetInstance().GetValue(CONST_CONFIG_DATABASESERVICE_FOLDERBACKUP));
	FormLayout->addRow(LOCALIZATION("BackupDir") + ":", EditBackupDir);

	EditPostgresDir = new ISPathEditDir(this);
	EditPostgresDir->SetValue(ISConfig::GetInstance().GetValue(CONST_CONFIG_DATABASESERVICE_FOLDERPOSTGRESBIN));
	FormLayout->addRow(LOCALIZATION("PostgresDir") + ":", EditPostgresDir);

	EditCompression = new ISIntegerEdit(this);
	EditCompression->SetValue(ISConfig::GetInstance().GetValue(CONST_CONFIG_DATABASESERVICE_COMPRESSION));
	EditCompression->SetMinimum(0);
	EditCompression->SetMaximum(9);
	FormLayout->addRow(LOCALIZATION("CompressionLevel") + ":", EditCompression);

	ISButtonDialog *ButtonDialog = new ISButtonDialog(this, LOCALIZATION("Save"));
	connect(ButtonDialog, &ISButtonDialog::Apply, this, &ISDatabaseServiceSettingsForm::Save);
	connect(ButtonDialog, &ISButtonDialog::Close, this, &ISDatabaseServiceSettingsForm::close);
	GetMainLayout()->addWidget(ButtonDialog);
}
//-----------------------------------------------------------------------------
ISDatabaseServiceSettingsForm::~ISDatabaseServiceSettingsForm()
{

}
//-----------------------------------------------------------------------------
void ISDatabaseServiceSettingsForm::Save()
{
	ISConfig::GetInstance().SetValue(CONST_CONFIG_DATABASESERVICE_SERVER, EditServer->GetValue());
	ISConfig::GetInstance().SetValue(CONST_CONFIG_DATABASESERVICE_PORT, EditPort->GetValue());
	ISConfig::GetInstance().SetValue(CONST_CONFIG_DATABASESERVICE_DATABASE, EditDatabase->GetValue());
	ISConfig::GetInstance().SetValue(CONST_CONFIG_DATABASESERVICE_LOGIN, EditLogin->GetValue());
	ISConfig::GetInstance().SetValue(CONST_CONFIG_DATABASESERVICE_PASSWORD, EditPassword->GetValue());
	ISConfig::GetInstance().SetValue(CONST_CONFIG_DATABASESERVICE_FOLDERBACKUP, EditBackupDir->GetValue());
	ISConfig::GetInstance().SetValue(CONST_CONFIG_DATABASESERVICE_FOLDERPOSTGRESBIN, EditPostgresDir->GetValue());
	ISConfig::GetInstance().SetValue(CONST_CONFIG_DATABASESERVICE_COMPRESSION, EditCompression->GetValue());

	close();
}
//-----------------------------------------------------------------------------
