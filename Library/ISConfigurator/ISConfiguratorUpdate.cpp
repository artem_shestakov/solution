#include "StdAfx.h"
#include "ISConfiguratorUpdate.h"
#include "CGFunction.h"
#include "CGTable.h"
#include "CGIndex.h"
#include "CGForeign.h"
#include "CGResource.h"
#include "CGClass.h"
//-----------------------------------------------------------------------------
ISConfiguratorUpdate::ISConfiguratorUpdate() : ISConfiguratorBase()
{

}
//-----------------------------------------------------------------------------
ISConfiguratorUpdate::~ISConfiguratorUpdate()
{

}
//-----------------------------------------------------------------------------
void ISConfiguratorUpdate::Database()
{
	Functions();
	Tables();
	SystemIndexes();
	Indexes();
	CompoundIndexes();
	Foreigns();
	Resources();
	Class();
}
//-----------------------------------------------------------------------------
void ISConfiguratorUpdate::Functions()
{
	ISDebug::ShowDebugString("Updating functions...");

	QDomElement Root = ISSystem::GetDomElement(QFile(SCHEMA_FUNCTIONS_PATH));
	QDomNode Node = Root.firstChild();

	while (!Node.isNull()) //����� ���� �������	
	{
		QString FunctionName = Node.attributes().namedItem("Name").nodeValue().toLower();
		QString FunctionSqlText = Node.attributes().namedItem("SqlText").nodeValue();

		if (!CGFunction::CheckExistFunction(FunctionName)) //���� ������� ����������, ���������� �����
		{
			QString ErrorString;
			bool Created = CGFunction::CreateFunction(FunctionName, FunctionSqlText, ErrorString);
		}

		Node = Node.nextSibling();
	}

	ISDebug::ShowEmptyString(true);
}
//-----------------------------------------------------------------------------
void ISConfiguratorUpdate::Tables()
{
	ISDebug::ShowDebugString("Updating tables...");

	int CountTables = ISMetaData::GetInstanse().GetTables().count();
	for (int i = 0; i < CountTables; i++) //����� ������
	{
		Progress(i, CountTables);

		PMetaClassTable *MetaTable = ISMetaData::GetInstanse().GetTables().at(i);
		QString ErrorString;
		bool Result = true;

		if (CGTable::CheckExistTable(MetaTable)) //���� ������� ����������, �������� �, ����� - �������
		{
			Result = CGTable::UpdateTable(MetaTable);
		}
		else
		{
			Result = CGTable::CreateTable(MetaTable, ErrorString);
		}
	}

	ISDebug::ShowEmptyString(true);
}
//-----------------------------------------------------------------------------
void ISConfiguratorUpdate::SystemIndexes()
{
	ISDebug::ShowDebugString("Updating system indexes...");

	int CountIndexes = ISMetaData::GetInstanse().GetSystemIndexes().count();
	for (int i = 0; i < CountIndexes; i++) //����� ��������
	{
		Progress(i, CountIndexes);

		PMetaClassIndex *MetaIndex = ISMetaData::GetInstanse().GetSystemIndexes().at(i);
		QString ErrorString;
		bool Result = true;

		if (CGIndex::CheckExistIndex(MetaIndex))
		{
			if (MetaIndex->GetFieldName().toLower() == "id") //���� ���� primary_key - ������ reindex
			{
				Result = CGIndex::ReindexIndex(MetaIndex, ErrorString);
			}
			else if (CGIndex::CheckIndexForeign(MetaIndex)) //���� �� ����, ��� ���������� ������� ������ ��������� ������� ���� - ������ reindex
			{
				Result = CGIndex::ReindexIndex(MetaIndex, ErrorString);
			}
			else
			{
				Result = CGIndex::UpdateIndex(MetaIndex, ErrorString);
			}
		}
		else
		{
			Result = CGIndex::CreateIndex(MetaIndex, ErrorString);
		}
	}

	ISDebug::ShowEmptyString(true);
}
//-----------------------------------------------------------------------------
void ISConfiguratorUpdate::Indexes()
{
	ISDebug::ShowDebugString("Updating indexes...");

	int CountIndexes = ISMetaData::GetInstanse().GetIndexes().count();
	for (int i = 0; i < CountIndexes; i++) //����� ��������
	{
		Progress(i, CountIndexes);

		PMetaClassIndex *MetaIndex = ISMetaData::GetInstanse().GetIndexes().at(i);
		QString ErrorString;
		bool Result = true;

		if (CGIndex::CheckExistIndex(MetaIndex))
		{
			Result = CGIndex::UpdateIndex(MetaIndex, ErrorString);
		}
		else
		{
			Result = CGIndex::CreateIndex(MetaIndex, ErrorString);
		}
	}

	ISDebug::ShowEmptyString(true);
}
//-----------------------------------------------------------------------------
void ISConfiguratorUpdate::CompoundIndexes()
{
	ISDebug::ShowDebugString("Updating compound indexes...");

	int CountIndexes = ISMetaData::GetInstanse().GetCompoundIndexes().count();
	for (int i = 0; i < CountIndexes; i++)
	{
		Progress(i, CountIndexes);

		PMetaClassIndex *MetaIndex = ISMetaData::GetInstanse().GetCompoundIndexes().at(i);
		QString ErrorString;
		bool Result = true;

		if (CGIndex::CheckExistIndex(MetaIndex))
		{
			Result = CGIndex::UpdateIndex(MetaIndex, ErrorString);
		}
		else
		{
			Result = CGIndex::CreateIndex(MetaIndex, ErrorString);
		}
	}

	ISDebug::ShowEmptyString(true);
}
//-----------------------------------------------------------------------------
void ISConfiguratorUpdate::Foreigns()
{
	ISDebug::ShowDebugString("Updating foreigns...");

	int CountForeigns = ISMetaData::GetInstanse().GetForeigns().count();
	for (int i = 0; i < CountForeigns; i++)
	{
		Progress(i, CountForeigns);

		PMetaClassForeign *MetaForeign = ISMetaData::GetInstanse().GetForeigns().at(i);
		QString ErrorString;
		bool Result = true;

		if (CGForeign::CheckExistForeign(MetaForeign))
		{
			Result = CGForeign::UpdateForeign(MetaForeign, ErrorString);
		}
		else
		{
			Result = CGForeign::CreateForeign(MetaForeign, ErrorString);
		}
	}

	ISDebug::ShowEmptyString(true);
}
//-----------------------------------------------------------------------------
void ISConfiguratorUpdate::Resources()
{
	ISDebug::ShowDebugString("Updating resources...");

	int CountResources = ISMetaData::GetInstanse().GetResources().count();
	for (int i = 0; i < CountResources; i++)
	{
		Progress(i, CountResources);

		PMetaClassResource *MetaResource = ISMetaData::GetInstanse().GetResources().at(i);
		QString ErrorString;

		if (CGResource::CheckExistResource(MetaResource))
		{
			CGResource::UpdateResource(MetaResource);
		}
		else
		{
			bool Inserted = CGResource::InsertResource(MetaResource, ErrorString);
		}
	}

	ISDebug::ShowEmptyString(true);
}
//-----------------------------------------------------------------------------
void ISConfiguratorUpdate::Class()
{
	ISDebug::ShowDebugString("Updating class...");

	int CountTables = ISMetaData::GetInstanse().GetTables().count();
	for (int i = 0; i < ISMetaData::GetInstanse().GetTables().count(); i++)
	{
		Progress(i, CountTables);

		PMetaClassTable *MetaTable = ISMetaData::GetInstanse().GetTables().at(i);
		if (CGClass::CheckExistClass(MetaTable))
		{
			CGClass::UpdateClass(MetaTable);
		}
		else
		{
			CGClass::InsertClass(MetaTable);
		}
	}
}
//-----------------------------------------------------------------------------
