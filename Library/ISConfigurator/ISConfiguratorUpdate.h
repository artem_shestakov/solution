#pragma once
//-----------------------------------------------------------------------------
#include "isconfigurator_global.h"
#include "ISConfiguratorBase.h"
//-----------------------------------------------------------------------------
class ISCONFIGURATOR_EXPORT ISConfiguratorUpdate : public ISConfiguratorBase
{
	Q_OBJECT

public:
	Q_INVOKABLE ISConfiguratorUpdate();
	virtual ~ISConfiguratorUpdate();

public slots:
	void Database(); //���������� ���� ������
	void Functions(); //���������� �������
	void Tables(); //���������� ������
	void SystemIndexes(); //���������� ��������� ��������
	void Indexes(); //���������� ��������
	void CompoundIndexes(); //���������� ��������� ��������
	void Foreigns(); //���������� ������� ������
	void Resources(); //���������� ��������
	void Class(); //���������� �������� ������
};
//-----------------------------------------------------------------------------
