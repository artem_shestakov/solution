#include "StdAfx.h"
#include "ISConfiguratorService.h"
#include "ISQuery.h"
#include "ISMetaData.h"
//-----------------------------------------------------------------------------
static QString QS_TABLES = PREPARE_QUERY("SELECT tablename FROM pg_tables WHERE schemaname = current_schema() ORDER BY tablename");
//-----------------------------------------------------------------------------
static QString QS_COLUMNS = PREPARE_QUERY("SELECT column_name FROM information_schema.columns WHERE table_catalog = current_database() AND table_schema = current_schema() AND table_name = :TableName ORDER BY column_name");
//-----------------------------------------------------------------------------
static QString Q_VACUUM = "VACUUM";
//-----------------------------------------------------------------------------
static QString Q_VACUUM_ANALYZE = "VACUUM ANALYZE";
//-----------------------------------------------------------------------------
static QString Q_VACUUM_FULL = "VACUUM FULL";
//-----------------------------------------------------------------------------
static QString QS_INDEXES = PREPARE_QUERY("SELECT indexname FROM pg_indexes WHERE schemaname = current_schema()");
//-----------------------------------------------------------------------------
static QString QS_SEQUENCES = PREPARE_QUERY("SELECT sequence_name "
										    "FROM information_schema.sequences "
										    "WHERE sequence_catalog = current_database() "
										    "AND sequence_name NOT IN(:Where) "
										    "ORDER BY sequence_name")
//-----------------------------------------------------------------------------
ISConfiguratorService::ISConfiguratorService() : ISConfiguratorBase()
{

}
//-----------------------------------------------------------------------------
ISConfiguratorService::~ISConfiguratorService()
{

}
//-----------------------------------------------------------------------------
void ISConfiguratorService::ShowNotNeededTables()
{
	ISDebug::ShowString("Searching not needed tables...");

	int FoundedTables = 0;

	ISQuery qSelectTables(QS_TABLES);
	if (qSelectTables.Execute())
	{
		ISDebug::ShowString("Tables:");
		while (qSelectTables.Next()) //����� ������ ����
		{
			QString TableName = qSelectTables.ReadColumn("tablename").toString().toLower();

			PMetaClassTable *MetaTable = FoundTable(TableName);
			if (!MetaTable)
			{
				ISDebug::ShowString(TableName);
				FoundedTables++;
			}
		}
	}

	ISDebug::ShowString(QString("Founded tables: %1").arg(FoundedTables));
	ISDebug::ShowEmptyString(true);
}
//-----------------------------------------------------------------------------
void ISConfiguratorService::ShowNotNeededFields()
{
	ISDebug::ShowString("Searching not needed fields...");

	ISQuery qSelectTables(QS_TABLES);
	if (qSelectTables.Execute())
	{
		while (qSelectTables.Next()) //����� ������ ����
		{
			QString TableName = qSelectTables.ReadColumn("tablename").toString().toLower();

			PMetaClassTable *MetaTable = FoundTable(TableName);
			if (MetaTable)
			{
				ISQuery qSelectColumns(QS_COLUMNS);
				qSelectColumns.BindValue(":TableName", TableName);
				if (qSelectColumns.Execute())
				{
					while (qSelectColumns.Next()) //����� ����� �������
					{
						QString ColumnName = qSelectColumns.ReadColumn("column_name").toString().toLower();

						PMetaClassField *MetaField = FoundField(MetaTable, ColumnName);
						if (!MetaField)
						{
							ISDebug::ShowString(TableName + ": " + ColumnName);
						}
					}
				}
			}
		}
	}

	ISDebug::ShowEmptyString(true);
}
//-----------------------------------------------------------------------------
void ISConfiguratorService::ShowNotNeededResources()
{
	ISDebug::ShowString("Searching not needed resources...");

	QMap<QString, QVector<QString> *> Map;
	QMap <QString, QVector<QString> *> MapOutput;

	for (int i = 0; i < ISMetaData::GetInstanse().GetResources().count(); i++)
	{
		PMetaClassResource *MetaResource = ISMetaData::GetInstanse().GetResources().at(i);

		QString TableName = MetaResource->GetTableName();
		QString ResourceUID = MetaResource->GetUID().toLower();

		if (!Map.contains(TableName))
		{
			Map.insert(TableName, new QVector<QString>());
		}

		Map.value(TableName)->append(ResourceUID);
	}

	for (const auto &MapItem : Map.toStdMap())
	{
		QString TableName = MapItem.first;
		QVector<QString> *Vector = MapItem.second;

		QString SqlText = "SELECT %1_uid FROM %2 WHERE %1_uid NOT IN(%3)";
		SqlText = SqlText.arg(ISMetaData::GetInstanse().GetMetaTable(TableName)->GetAlias());
		SqlText = SqlText.arg(TableName);

		QString NotIN;
		for (int i = 0; i < Vector->count(); i++)
		{
			NotIN += "'" + Vector->at(i) + "', ";
		}

		ISSystem::RemoveLastSymbolFromString(NotIN, 2);
		SqlText = SqlText.arg(NotIN);

		ISQuery qSelect(SqlText);
		if (qSelect.Execute())
		{
			while (qSelect.Next())
			{
				ISUuid UID = qSelect.ReadColumn(0).toString();
				
				if (!MapOutput.contains(TableName))
				{
					MapOutput.insert(TableName, new QVector<QString>());
				}

				MapOutput.value(TableName)->append(UID);
			}
		}
	}

	ISDebug::ShowEmptyString(true);

	for (const auto &OutputItem : MapOutput.toStdMap())
	{
		QString TableName = OutputItem.first;
		QVector<QString> *Vector = OutputItem.second;

		ISDebug::ShowString(TableName + ":");

		for (int i = 0; i < Vector->count(); i++)
		{
			ISDebug::ShowString(Vector->at(i));
		}

		ISDebug::ShowEmptyString(true);
	}

	ISDebug::ShowEmptyString(true);
}
//-----------------------------------------------------------------------------
void ISConfiguratorService::ShowNotNeededSequence()
{
	ISDebug::ShowString("Searching not needed sequences...");

	QString Where;
	for (int i = 0; i < ISMetaData::GetInstanse().GetTables().count(); i++)
	{
		QString TableName = ISMetaData::GetInstanse().GetTables().at(i)->GetName().toLower();
		QString SequnceName = TableName + "_sequence";
		Where += "'" + SequnceName + "', ";
	}

	ISSystem::RemoveLastSymbolFromString(Where, 2);

	ISQuery qSelectSequences(QS_SEQUENCES.replace(":Where", Where));
	if (qSelectSequences.Execute())
	{
		while (qSelectSequences.Next())
		{
			QString SequenceName = qSelectSequences.ReadColumn("sequence_name").toString();
			ISDebug::ShowString(SequenceName);
		}
	}
}
//-----------------------------------------------------------------------------
void ISConfiguratorService::Reindex()
{
	ISDebug::ShowString("Reindex...");

	int CountTables = ISMetaData::GetInstanse().GetTables().count();
	for (int i = 0; i < CountTables; i++)
	{
		Progress(i, CountTables);

		QString TableName = ISMetaData::GetInstanse().GetTables().at(i)->GetName();

		ISDebug::ShowString("Reindex table: " + TableName);

		ISQuery qReindexTable;
		bool Executed = qReindexTable.Execute(QString("REINDEX TABLE %1").arg(TableName));
		if (Executed)
		{
			ISDebug::ShowString(QString("Reindex table %1 done").arg(TableName));
		}
		else
		{
			ISDebug::ShowString(QString("Reindex table %1 error").arg(TableName));
		}
	}

	ISDebug::ShowString("Reindex done");
	ISDebug::ShowEmptyString(true);
}
//-----------------------------------------------------------------------------
void ISConfiguratorService::Vacuum()
{
	ISDebug::ShowDebugString("Vacuum...");
	ISSystem::SleepMilliseconds(ONE_SECOND_TO_MILLISECOND);

	ISQuery qVacuum;
	if (qVacuum.Execute(Q_VACUUM))
	{
		ISDebug::ShowInfoString("Vacuum done");
	}
	else
	{
		ISDebug::ShowWarningString("Vacuum error: " + qVacuum.GetSqlQuery().lastError().text());
	}
}
//-----------------------------------------------------------------------------
void ISConfiguratorService::VacuumAnalyze()
{
	ISDebug::ShowDebugString("Vacuum analyze...");
	ISSystem::SleepMilliseconds(ONE_SECOND_TO_MILLISECOND);

	ISQuery qVacuumAnalyze;
	if (qVacuumAnalyze.Execute(Q_VACUUM_ANALYZE))
	{
		ISDebug::ShowInfoString("Vacuum analyze done");
	}
	else
	{
		ISDebug::ShowWarningString("Vacuum analyze error: " + qVacuumAnalyze.GetSqlQuery().lastError().text());
	}
}
//-----------------------------------------------------------------------------
void ISConfiguratorService::VacuumFull()
{
	ISDebug::ShowDebugString("Vacuum full...");
	ISSystem::SleepMilliseconds(ONE_SECOND_TO_MILLISECOND);

	ISQuery qVacuumFull;
	if (qVacuumFull.Execute(Q_VACUUM_FULL))
	{
		ISDebug::ShowInfoString("Vacuum full done");
	}
	else
	{
		ISDebug::ShowWarningString("Vacuum full error: " + qVacuumFull.GetSqlQuery().lastError().text());
	}
}
//-----------------------------------------------------------------------------
PMetaClassTable* ISConfiguratorService::FoundTable(const QString &TableName)
{
	for (int i = 0; i < ISMetaData::GetInstanse().GetTables().count(); i++)
	{
		PMetaClassTable *MetaTable = ISMetaData::GetInstanse().GetTables().at(i);
		if (MetaTable->GetName().toLower() == TableName)
		{
			return MetaTable;
		}
	}

	return nullptr;
}
//-----------------------------------------------------------------------------
PMetaClassField* ISConfiguratorService::FoundField(PMetaClassTable *MetaTable, const QString &ColumnName)
{
	for (int i = 0; i < MetaTable->GetAllFields().count(); i++)
	{
		PMetaClassField *MetaField = MetaTable->GetAllFields().at(i);
		if (QString(MetaTable->GetAlias().toLower() + "_" + MetaField->GetName().toLower()) == ColumnName)
		{
			return MetaField;
		}
	}

	return nullptr;
}
//-----------------------------------------------------------------------------
