#include "StdAfx.h"
#include "CGSequence.h"
#include "EXDefines.h"
#include "ISQuery.h"
#include "ISConfig.h"
#include "EXConstants.h"
//-----------------------------------------------------------------------------
static QString QS_SEQUENCES = PREPARE_QUERY("SELECT COUNT(*) "
											"FROM information_schema.sequences t "
											"WHERE t.sequence_catalog = :DatabaseName "
											"AND t.sequence_name = :SequenceName");
//-----------------------------------------------------------------------------
static QString QC_SEQUENCE = "CREATE SEQUENCE public.%1 "
							 "INCREMENT 1 MINVALUE 1 "
							 "MAXVALUE 2147483647 START 1 "
							 "CACHE 1";
//-----------------------------------------------------------------------------
CGSequence::CGSequence()
{

}
//-----------------------------------------------------------------------------
CGSequence::~CGSequence()
{

}
//-----------------------------------------------------------------------------
bool CGSequence::CreateSequence(const QString &ClassName)
{
	QString Query = QC_SEQUENCE.arg(GetSequenceNameForClass(ClassName)); //������������ �������
	ISQuery qCreateSequence;
	bool Created = qCreateSequence.Execute(Query);
	return Created;
}
//-----------------------------------------------------------------------------
bool CGSequence::CheckExistSequence(const QString &ClassName)
{
	ISQuery qSelectSequences(QS_SEQUENCES);
	qSelectSequences.BindValue(":DatabaseName", ISConfig::GetInstance().GetValue(CONST_CONFIG_CONNECTION_DATABASE));
	qSelectSequences.BindValue(":SequenceName", GetSequenceNameForClass(ClassName));
	qSelectSequences.ExecuteFirst();
	if (qSelectSequences.ReadColumn("count").toInt() == 1)
	{
		return true;
	}

	return false;
}
//-----------------------------------------------------------------------------
QString CGSequence::GetSequenceNameForClass(const QString &ClassName)
{
	return ClassName + "_sequence";
}
//-----------------------------------------------------------------------------