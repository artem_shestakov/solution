#pragma once
//-----------------------------------------------------------------------------
#include "isconfigurator_global.h"
//-----------------------------------------------------------------------------
class ISCONFIGURATOR_EXPORT CGFunction : public QObject
{
	Q_OBJECT

public:
	CGFunction();
	virtual ~CGFunction();

	static bool CreateFunction(const QString &FunctionName, const QString &QueryFunction, QString &ErrorString); //�������� �������
	static bool CheckExistFunction(const QString &FunctionName); //�������� ������������� �������
};
//-----------------------------------------------------------------------------
