#include "StdAfx.h"
#include "CGIndex.h"
#include "EXDefines.h"
#include "ISQuery.h"
#include "ISSystem.h"
#include "PMetaClassForeign.h"
#include "ISMetaData.h"
#include "CGHelper.h"
//-----------------------------------------------------------------------------
static QString QS_INDEXES = PREPARE_QUERY("SELECT COUNT(*) FROM pg_indexes WHERE schemaname = current_schema() AND tablename = :TableName AND indexname = :IndexName;");
//-----------------------------------------------------------------------------
static QString QD_INDEX = "DROP INDEX public.%1;";
//-----------------------------------------------------------------------------
static QString QC_INDEX = "CREATE %1 INDEX %2 ON public.%3 USING btree(%4);";
//-----------------------------------------------------------------------------
static QString Q_REINDEX = "REINDEX INDEX %1";
//-----------------------------------------------------------------------------
CGIndex::CGIndex()
{

}
//-----------------------------------------------------------------------------
CGIndex::~CGIndex()
{

}
//-----------------------------------------------------------------------------
bool CGIndex::CreateIndex(PMetaClassIndex *Index, QString &ErrorString)
{
	QString IndexUnique = QString();
	if (Index->GetUnique())
	{
		IndexUnique = "UNIQUE";
	}

	QString SqlText = QC_INDEX;
	SqlText = SqlText.arg(IndexUnique);
	SqlText = SqlText.arg(OnGetIndexName(Index));
	SqlText = SqlText.arg(Index->GetTableName());

	QString Fields = QString();

	if (Index->GetFields().count())
	{
		for (int i = 0; i < Index->GetFields().count(); i++)
		{
			Fields += Index->GetAlias() + "_" + Index->GetFields().at(i) + ", ";
		}

		ISSystem::RemoveLastSymbolFromString(Fields, 2);
		SqlText = SqlText.arg(Fields);
	}
	else
	{
		SqlText = SqlText.arg(Index->GetAlias().toLower() + "_" + Index->GetFieldName().toLower());
	}

	ISQuery qCreateIndex;
	bool Created = qCreateIndex.Execute(SqlText);
	if (!Created)
	{
		ErrorString = qCreateIndex.GetSqlQuery().lastError().text();
	}

	return Created;
}
//-----------------------------------------------------------------------------
bool CGIndex::UpdateIndex(PMetaClassIndex *Index, QString &ErrorString)
{
	QString IndexName = QString();

	if (Index->GetFields().count())
	{
		for (int i = 0; i < Index->GetFields().count(); i++)
		{
			IndexName += Index->GetFields().at(i).toLower() + "_";
		}

		ISSystem::RemoveLastSymbolFromString(IndexName);
	}
	else
	{
		IndexName = OnGetIndexName(Index);
	}

	QString QueryDeleteIndex = QD_INDEX;
	QueryDeleteIndex = QueryDeleteIndex.arg(IndexName);

	ISQuery qDelete;
	bool Deleted = qDelete.Execute(QueryDeleteIndex);
	if (Deleted)
	{
		bool Result = CreateIndex(Index, ErrorString);
		return Result;
	}

	return false;
}
//-----------------------------------------------------------------------------
bool CGIndex::CheckExistIndex(PMetaClassIndex *Index)
{
	ISQuery qSelectIndex(QS_INDEXES);
	qSelectIndex.BindValue(":TableName", Index->GetTableName().toLower());
	qSelectIndex.BindValue(":IndexName", OnGetIndexName(Index));
	if (qSelectIndex.ExecuteFirst())
	{
		int Count = qSelectIndex.ReadColumn("count").toInt();
		if (Count)
		{
			return true;
		}
	}

	return false;
}
//-----------------------------------------------------------------------------
bool CGIndex::CheckIndexForeign(PMetaClassIndex *Index)
{
	QVector<PMetaClassForeign*> Foreigns = ISMetaData::GetInstanse().GetForeigns();

	for (int i = 0; i < Foreigns.count(); i++)
	{
		PMetaClassForeign *MetaForeign = Foreigns.at(i);
		if (Index->GetTableName().toLower() == MetaForeign->GetForeignClass().toLower())
		{
			if (Index->GetFieldName().toLower() == MetaForeign->GetForeginField().toLower())
			{
				return true;
			}
		}
	}

	return false;
}
//-----------------------------------------------------------------------------
bool CGIndex::ReindexIndex(PMetaClassIndex *Index, QString &ErrorString)
{
	QString QueryText = Q_REINDEX.arg(OnGetIndexName(Index));
	ISQuery qReindex;
	bool Executed = qReindex.Execute(QueryText);

	if (!Executed)
	{
		ErrorString = qReindex.GetSqlQuery().lastError().text();
	}

	return Executed;
}
//-----------------------------------------------------------------------------
QString CGIndex::OnGetIndexName(PMetaClassIndex *Index)
{
	QString IndexName = QString();

	if (Index->GetFields().count())
	{
		for (int i = 0; i < Index->GetFields().count(); i++)
		{
			IndexName += Index->GetFields().at(i).toLower() + "_";
		}

		ISSystem::RemoveLastSymbolFromString(IndexName);
	}
	else
	{
		IndexName = Index->GetTableName().toLower() + "_" + Index->GetAlias().toLower() + "_" + Index->GetFieldName().toLower() + "_index";
	}

	
	return IndexName;
}
//-----------------------------------------------------------------------------
