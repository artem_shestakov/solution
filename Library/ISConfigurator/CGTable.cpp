#include "StdAfx.h"
#include "CGTable.h"
#include "EXDefines.h"
#include "ISQuery.h"
#include "ISMetaData.h"
#include "ISAssert.h"
#include "CGTemplateField.h"
#include "CGHelper.h"
#include "CGSequence.h"
#include "ISSystem.h"
//-----------------------------------------------------------------------------
static QString QS_TABLE = PREPARE_QUERY("SELECT COUNT(*) FROM pg_tables WHERE schemaname = current_schema() AND tablename = :TableName");
//-----------------------------------------------------------------------------
static QString QS_COLUMNS_COUNT = PREPARE_QUERY("SELECT COUNT(*) "
								  "FROM information_schema.columns "
								  "WHERE table_catalog = current_database() "
								  "AND table_schema = current_schema() "
								  "AND table_name = :TableName");
//-----------------------------------------------------------------------------
static QString QS_COLUMNS = PREPARE_QUERY("SELECT column_name, column_default, is_nullable::BOOLEAN, data_type "
										  "FROM information_schema.columns "
										  "WHERE table_catalog = current_database() "
										  "AND table_schema = current_schema() "
										  "AND table_name = :TableName "
										  "ORDER BY ordinal_position");
//-----------------------------------------------------------------------------
CGTable::CGTable()
{

}
//-----------------------------------------------------------------------------
CGTable::~CGTable()
{

}
//-----------------------------------------------------------------------------
bool CGTable::CreateTable(PMetaClassTable *MetaTable, QString &ErrorString)
{
	QString TableName = MetaTable->GetName().toLower();
	QString TableAlias = MetaTable->GetAlias();

	//�������� ������������� ������������������, ���� ������������������ �� ���������� - ���������� � ��������
	if (!CGSequence::CheckExistSequence(TableName))
	{
		CGSequence::CreateSequence(TableName);
	}

	QString SqlText = "CREATE TABLE public." + MetaTable->GetName().toLower() + "(\n";
	SqlText += CGTemplateField::GetSqlTextForTemplateSystemFields(TableName, TableAlias);

	//������������ ������� �� �������� �������
	int CountFields = MetaTable->GetFields().count();
	for (int i = 0; i < CountFields; i++) //����� ����� �������
	{
		PMetaClassField *MetaField = MetaTable->GetFields().at(i);
		ISNamespace::FieldType FieldType = MetaField->GetType(); //��� ����
		
		if (MetaField->GetQueryText().length())
		{
			continue;
		}

		QString FieldName = MetaField->GetName(); //��� ����
		int FieldSize = MetaField->GetSize(); //������ ����
		QString FieldDefalutValue = MetaField->GetDefaultValue().toString(); //�������� �� ��������� ��� ����
		bool FieldNotNull = MetaField->GetNotNull(); //������ ������������� ���������� ����

		SqlText += TableAlias + "_" + FieldName.toLower();
		SqlText += " " + ISMetaData::GetInstanse().GetAssociationTypes().GetTypeDB(FieldType);

		if (FieldSize) //���� ������ ������ ����
		{
			SqlText += "(" + QString::number(FieldSize) + ")";
		}

		if (FieldDefalutValue.length()) //���� ������� �������� �� ���������
		{
			if (FieldType == ISNamespace::FT_UID)
			{
				SqlText += " DEFAULT '" + FieldDefalutValue + "'";
			}
			else
			{
				SqlText += " DEFAULT " + FieldDefalutValue;
			}
		}
		else //����� �������� ��� ��������� NULL
		{
			SqlText += " DEFAULT NULL";
		}

		if (FieldNotNull) //���� ���� ����������� ��� ����������
		{
			SqlText += " NOT NULL";
		}

		SqlText += ",\n";
	}

	//���������� ������������ ������� �� �������� �������
	ISSystem::RemoveLastSymbolFromString(SqlText, 2);
	SqlText += "\n);";

	//���������� �������
	ISQuery qCreateTable;
	bool Created = qCreateTable.Execute(SqlText);

	if (Created)
	{
		for (int i = 0; i < MetaTable->GetAllFields().count(); i++)
		{
			PMetaClassField *MetaField = MetaTable->GetAllFields().at(i);
			if (MetaField->GetQueryText().length())
			{
				continue;
			}

			CGHelper::CommentField(MetaTable->GetName().toLower(), MetaTable->GetAlias() + "_" + MetaField->GetName().toLower(), MetaField->GetLocalListName());
		}
	}
	else
	{
		ErrorString = qCreateTable.GetSqlQuery().lastError().text();
	}

	return Created; //������ ������� ��������� �������
}
//-----------------------------------------------------------------------------
bool CGTable::UpdateTable(PMetaClassTable *MetaTable)
{
	ISQuery qSelectColumnsCount(QS_COLUMNS_COUNT);
	qSelectColumnsCount.BindValue(":TableName", MetaTable->GetName().toLower());
	IS_ASSERT(qSelectColumnsCount.ExecuteFirst(), QS_COLUMNS_COUNT);
	
	CGHelper::CommentTable(MetaTable->GetName().toLower(), MetaTable->GetLocalListName());

	int QueryColumnCount = qSelectColumnsCount.ReadColumn("count").toInt();
	int MetaColumnCount = MetaTable->GetAllFields().count();
	
	if (QueryColumnCount == MetaColumnCount) //���� ���������� ����� � ������� � ����-������ �����, ��������� ���� � �.�.
	{
		ISQuery qSelectColumns(QS_COLUMNS);
		qSelectColumns.BindValue(":TableName", MetaTable->GetName().toLower());
		if (qSelectColumns.Execute())
		{
			while (qSelectColumns.Next())
			{
				QString ColumnNameFull = qSelectColumns.ReadColumn("column_name").toString();
				QString ColumnName = qSelectColumns.ReadColumn("column_name").toString().split("_")[1];
				QString ColumnDefaultValue = qSelectColumns.ReadColumn("column_default").toString();
				bool ColumnNotNull = !qSelectColumns.ReadColumn("is_nullable").toBool();
				QString ColumnType = qSelectColumns.ReadColumn("data_type").toString().toUpper();

				if (!ISMetaData::GetInstanse().CheckExitField(MetaTable, ColumnName))
				{
					continue;
				}

				PMetaClassField *MetaField = ISMetaData::GetInstanse().GetMetaField(MetaTable, ColumnName);
				QString MetaType = ISMetaData::GetInstanse().GetAssociationTypes().GetTypeDB(MetaField->GetType());
				QString MetaDefaultValue = MetaField->GetDefaultValue().toString();
				bool MetaNotNull = MetaField->GetNotNull();

				CGHelper::CommentField(MetaTable->GetName().toLower(), MetaTable->GetAlias() + "_" + MetaField->GetName().toLower(), MetaField->GetLocalListName());

				if (ColumnName == "id")
				{
					continue;
				}

				//�������� ������������ �������� �� ���������
				if (ColumnDefaultValue != MetaDefaultValue)
				{
					QString QueryText;

					if (MetaDefaultValue.length()) //���� �������� �� ��������� �������
					{
						if (MetaDefaultValue.toLower() == "null") //���� �������� �� ��������� ������� �������
						{
							QueryText = "ALTER TABLE public." + MetaTable->GetName().toLower() + " ALTER COLUMN " + ColumnNameFull + " SET DEFAULT NULL";
						}
						else
						{
							QueryText = "ALTER TABLE public." + MetaTable->GetName().toLower() + " ALTER COLUMN " + ColumnNameFull + " SET DEFAULT '" + MetaDefaultValue + "'";
						}
					}
					else //������� �������� �� ���������
					{
						QueryText = "ALTER TABLE public." + MetaTable->GetName().toLower() + " ALTER COLUMN " + ColumnNameFull + " DROP DEFAULT";
					}

					ISQuery qAlterColumn;
					bool Executed = qAlterColumn.Execute(QueryText);
				}

				if (ColumnNotNull != MetaNotNull)
				{
					//�������� ������������ �� ������� ��������
					if (ColumnNotNull && MetaNotNull)
					{
						QString QueryText = "ALTER TABLE public." + MetaTable->GetName().toLower() + " ALTER COLUMN " + MetaTable->GetAlias() + "_" + MetaField->GetName().toLower() + " SET NOT NULL";
						ISQuery qAlterNotNull;
						qAlterNotNull.Execute(QueryText);
					}
					else if (ColumnNotNull && !MetaNotNull)
					{
						QString QueryText = "ALTER TABLE public." + MetaTable->GetName().toLower() + " ALTER COLUMN " + MetaTable->GetAlias() + "_" + MetaField->GetName().toLower() + " DROP NOT NULL";
						ISQuery qAlterNotNull;
						bool Executed = qAlterNotNull.Execute(QueryText);
					}
				}
			}
		}

		return true;
	}
	else
	{
		if (MetaColumnCount > QueryColumnCount) //���������� ����� � ����-������� ������ ��� � ������� ���� - ������� ����� ����
		{
			for (int i = 0; i < MetaTable->GetFields().count(); i++) //����� �����
			{
				PMetaClassField *MetaField = MetaTable->GetFields().at(i);
				if (MetaField->GetQueryText().length())
				{
					continue;
				}

				QString FieldName = MetaTable->GetAlias() + "_" + MetaField->GetName().toLower();

				if (!CGHelper::CheckExistColumn(MetaTable, FieldName)) //���� ���� �� ����������
				{
					QString AddColumn = "ALTER TABLE public." + MetaTable->GetName() + " \n" +
										"ADD COLUMN \"" + FieldName + "\" " + ISMetaData::GetInstanse().GetAssociationTypes().GetTypeDB(MetaField->GetType());

					if (MetaField->GetSize()) //���� ������ ������ ����
					{
						AddColumn += "(" + QString::number(MetaField->GetSize()) + ")";
					}

					if (!MetaField->GetDefaultValue().toString().isEmpty()) //���� ������� �������� �� ���������
					{
						if (MetaField->GetType() == ISNamespace::FT_Int) //���� ��� ���� �������� INTEGER
						{
							AddColumn += " DEFAULT " + MetaField->GetDefaultValue().toString(); //�������� NULL ��� �������
						}
						else
						{
							AddColumn += " DEFAULT '" + MetaField->GetDefaultValue().toString() + "'"; //�������� NULL � �������
						}
					}

					if (MetaField->GetNotNull())
					{
						AddColumn += " NOT NULL";
					}

					ISQuery qAddColumn;
					bool Alter = qAddColumn.Execute(AddColumn);
					if (Alter)
					{
						CGHelper::CommentField(MetaTable->GetName().toLower(), MetaTable->GetAlias() + "_" + MetaField->GetName().toLower(), MetaField->GetLocalListName());
					}
				}
			}

			return true;
		}
	}

	return false;
}
//-----------------------------------------------------------------------------
bool CGTable::CheckExistTable(PMetaClassTable *MetaTable)
{
	ISQuery qSelectTable(QS_TABLE);
	qSelectTable.BindValue(":TableName", MetaTable->GetName().toLower());
	qSelectTable.ExecuteFirst();
	if (qSelectTable.ReadColumn("count").toInt() == 1) //���� ������� ����������
	{
		return true;
	}

	return false; //�� ����������
}
//-----------------------------------------------------------------------------
