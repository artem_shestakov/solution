#pragma once
//-----------------------------------------------------------------------------
#include "isconfigurator_global.h"
#include "ISConfiguratorBase.h"
//-----------------------------------------------------------------------------
class ISCONFIGURATOR_EXPORT ISConfiguratorOther : public ISConfiguratorBase
{
	Q_OBJECT

public:
	Q_INVOKABLE ISConfiguratorOther();
	virtual ~ISConfiguratorOther();

public slots:
	void DatabaseService();
};
//-----------------------------------------------------------------------------
