#pragma once
//-----------------------------------------------------------------------------
#include "isconfigurator_global.h"
#include "ISConfiguratorBase.h"
#include "PMetaClassTable.h"
#include "PMetaClassField.h"
//-----------------------------------------------------------------------------
class ISCONFIGURATOR_EXPORT ISConfiguratorService : public ISConfiguratorBase
{
	Q_OBJECT

public:
	Q_INVOKABLE ISConfiguratorService();
	virtual ~ISConfiguratorService();

public slots:
	void ShowNotNeededTables();
	void ShowNotNeededFields();
	void ShowNotNeededResources();
	void ShowNotNeededSequence();
	void Reindex();
	void Vacuum();
	void VacuumAnalyze();
	void VacuumFull();

private:
	PMetaClassTable* FoundTable(const QString &TableName);
	PMetaClassField* FoundField(PMetaClassTable *MetaTable, const QString &ColumnName);
};
//-----------------------------------------------------------------------------
