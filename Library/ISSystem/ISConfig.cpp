#include "StdAfx.h"
#include "ISConfig.h"
#include "EXDefines.h"
#include "ISAssert.h"
//-----------------------------------------------------------------------------
ISConfig::ISConfig() : QObject()
{
	Settings = nullptr;
}
//-----------------------------------------------------------------------------
ISConfig::~ISConfig()
{

}
//-----------------------------------------------------------------------------
ISConfig& ISConfig::GetInstance()
{
	static ISConfig Config;
	return Config;
}
//-----------------------------------------------------------------------------
void ISConfig::Initialize(const QString &ConfigFilePath)
{
	bool Exist = QFile::exists(ConfigFilePath);
	IS_ASSERT(Exist, "Not found file config " + ConfigFilePath);
	Settings = new QSettings(ConfigFilePath, QSettings::IniFormat, this);
	IS_ASSERT(Settings->allKeys().count(), QString("Config file \"%1\" is empty.").arg(ConfigFilePath));
}
//-----------------------------------------------------------------------------
QVariant ISConfig::GetValue(const QString &ParameterName)
{
	IS_ASSERT(Settings, "Config.Settings is null");
	bool Contains = Settings->contains(ParameterName);
	IS_ASSERT(Contains, QString("Not found config key \"%1\" in file \"%2\"").arg(ParameterName).arg(Settings->fileName()));

	QVariant Value = Settings->value(ParameterName);
	return Value;
}
//-----------------------------------------------------------------------------
void ISConfig::SetValue(const QString &ParameterName, const QVariant &Value)
{
	bool Contains = Settings->contains(ParameterName);
	IS_ASSERT(Contains, QString("Not found config key \"%1\" in file \"%2\"").arg(ParameterName).arg(Settings->fileName()));

	Settings->setValue(ParameterName, Value);
}
//-----------------------------------------------------------------------------
void ISConfig::ClearValue(const QString &ParameterName)
{
	bool Contains = Settings->contains(ParameterName);
	IS_ASSERT(Contains, QString("Not found config key \"%1\" in file \"%2\"").arg(ParameterName).arg(Settings->fileName()));

	Settings->setValue(ParameterName, QString());
}
//-----------------------------------------------------------------------------
