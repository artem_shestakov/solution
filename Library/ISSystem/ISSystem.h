#pragma once
//-----------------------------------------------------------------------------
#include "issystem_global.h"
#include "ISNamespace.h"
//-----------------------------------------------------------------------------
class ISSYSTEM_EXPORT ISSystem
{
public:

	//��������
	static void SetObjectProperty(QObject *Object, const char *PropertyName, const QVariant &PropertyValue); //�������� ������� �������� �������
	static void TerminateCurrentProcess(int Code); //���������� �������� ��������
	static QDomElement GetDomElement(QFile &File); //�������� ������� ����� ����� �� �����
	static QDomElement GetDomElement(const QString &Content); //�������� ������� ����� ����� �� ��������
	static void SleepMilliseconds(unsigned long Milliseconds); //�������� ������ (� �������������)
	static void SleepSeconds(unsigned long Seconds); //�������� ������ (� ��������)
	static qint64 GetFileSize(const QString &FilePath); //�������� ������ �����
	static QString FileSizeFromString(qint64 FileSize); //�������� ������ �����
	static QString GetSizeDir(const QString &DirPath); //�������� ������ ������ � �����
	static QString GetCompleteBaseFileName(const QString &FilePath); //�������� ��� �����
	static QString GetFileName(const QString &FilePath); //�������� ������ ��� ����� � �����������
	static QString GetVersionInFileName(const QString &FileName); //�������� ������ �� ������������ ����� ��������� IntegralSystem
	static QString FormatQFuncInfo(const QString &QFuncInfo, ISNamespace::FunctionNameFormat Format); //������������� ��������� ������� Q_FUNC_INFO
	static QString GetGeneratedCode(int CountSymbols = 5); //�������� ��� � ���������� (��������) ��������
	static QString GetCurrentDayOfWeekName(); //�������� �������� �������� ��� ������
	static QString GetDayOfWeekName(Qt::DayOfWeek Day); //�������� �������� ���
	static QString GetConfiguration(); //�������� ������� ������������
	static void ClearDirRecursive(const QString &DirPath); //�������� ����� ��������� (� ����������)
	static bool LoadModule(const QString &ModulePath, QString &ErrorString = QString()); //��������� ������ � ������
	static void RemoveLastSymbolFromString(QString &String, int CountSymbols = 1); //������� ��������� ������� �� ������
	static bool CallVirtualKeyboard(); //����� �������� ����������
	static bool OpenFile(const QString &FilePath); //������� ���� ����������� �� ���������
	static bool OpenFolder(const QString &FolderPath); //������� ����������
	static void RepaintWidget(QWidget *Widget, bool adjust_size = true); //������������ ��������� � ��������� ������
	static void ProcessEvents();
	static void MoveWidgetToCenter(QWidget *Widget); //������� ������� � ����� ������
	static void SetWaitGlobalCursor(bool Wait); //�������� ���������� ������ ��� ��������
	static ISNamespace::ApplicationType GetApplicationType(); //�������� ��� ����������� ����������
	static QString GenerateUuid(); //������������� ����� UID
	static QString PrepareLongToolTip(QString ToolTipText, int MaxWidth = 250); //������ ������ ������������ ��������� (��� �������� ���� �� ����� ������)
	static void BeginSymbolToUpper(QString &String); //�������������� ������� ������� ������ � ������� �������
	static QIcon GetIconFile(const QString &FilePath); //�������� ������ �����
	static QByteArray GetFileMD5(const QString &FilePath); //�������� ��� �����
	static int GetRandom(int Maximum); //�������� ���������� ����� �����
	static QString GetLogFileName(); //�������� ���� � ����� ���� �� ������� ����

//���� � �����
	static int TimeFromMinutes(const QTime &Time); //��������� ����� � ������
	static QString MillisecondsToString(int Milliseconds); //��������� ����������� � ������
	static QDateTime GetCreatedDateFile(const QString &FilePath); //�������� ���� � ����� �������� �����
	static QDateTime GetLastModifiedFile(const QString &FilePath); //�������� ���� � ����� ���������� ��������� �����
	static QString GetDateLongName(const QDate &Date); //�������� ���� ������� � ������� �������: 01 ������ 2000

//�����������
	static QString StringToBase64(const QString &String); //����������� ������ � Base64
	static QString Base64ToString(const QString &Base64); //����������� Base64 � ������
	static QFont StringToFont(const QString &FontText); //����������� ������ � �����, ���� ���������� ������ ������, ������������ ����� ���������� ����������
	static QByteArray PixmapToByteArray(const QPixmap &Pixmap); //����������� ����������� Pixmap � ������ ������
	static QPixmap ByteArrayToPixmap(const QByteArray &ByteArray); //����������� ������� ������ � ����������� Pixmap
	static QString SizeToString(const QSize &Size); //����������� ������� � ������
	static QColor StringToColor(const QString &String); //����������� ������ � ����
	static double MillisecondToSecond(int Milliseconds); //����������� ���������� � �������
	static QVariantMap JsonStringToVariantMap(const QString &JsonString); //����������� JSON-������ � ������ ����-��������
	static QString VariantMapToJsonString(const QVariantMap &VariantMap); //����������� ������ ����-�������� � JSON-������

//EXCEL
	static void Excel_SetBoldCell(QAxObject *Cell, bool bold); //�������� ����� � ������
	static void Excel_SetBackgrounCell(QAxObject *Cell, const QColor &Color); //�������� ���� ���� ������
	static void Excel_CreateBorderCell(QAxObject *Cell); //������� ����� ������ ������
};
//-----------------------------------------------------------------------------
