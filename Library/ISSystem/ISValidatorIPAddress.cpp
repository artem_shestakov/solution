#include "StdAfx.h"
#include "ISValidatorIPAddress.h"
#include "ISSystem.h"
//-----------------------------------------------------------------------------
ISValidatorIPAddress::ISValidatorIPAddress(QLineEdit *parent) : ISValidator(parent)
{

}
//-----------------------------------------------------------------------------
ISValidatorIPAddress::~ISValidatorIPAddress()
{

}
//-----------------------------------------------------------------------------
void ISValidatorIPAddress::Validate(QString &string)
{
	QStringList StringList = string.split(".");
	QStringList TempList;

	if (string.length() > OldAddress.length()) //���������� �������
	{
		for (int i = 0; i < StringList.count(); i++) //����� ���� ������ ������
		{
			QString Section = StringList.at(i);
			bool IsDigit = true;
			int Digit = Section.toInt(&IsDigit);
			if (IsDigit) //���� ������ �������� ������
			{
				if (Digit > 255) //���� ����� � ������ ������ ����������� - ����� �� �����
				{
					ISSystem::RemoveLastSymbolFromString(Section);
				}
				TempList.append(Section);
			}
			else //������ ����������� ������
			{
				QString LastSymbol = string.at(string.length() - 1);
				if (LastSymbol == "." || LastSymbol == ",")
				{
					TempList.append(".");
				}
			}
		}
	}
	else //�������� �������
	{
		//ISSystem::RemoveLastSymbolFromString(string);
	}

	string.clear();
	for (QString &Section : TempList) //���������� ������ ������
	{
		if (Section.length() == 3 || Section == ".")
		{
			Section.append(".");
		}

		string.append(Section);
	}

	OldAddress = string;
}
//-----------------------------------------------------------------------------
