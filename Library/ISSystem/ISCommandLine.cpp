#include "StdAfx.h"
#include "ISCommandLine.h"
#include "ISDebug.h"
//-----------------------------------------------------------------------------
ISCommandLine::ISCommandLine()
{

}
//-----------------------------------------------------------------------------
ISCommandLine::~ISCommandLine()
{

}
//-----------------------------------------------------------------------------
bool ISCommandLine::Question(const QString &QuestionText)
{
	bool Result = true;

	ISDebug::ShowEmptyString(true);
	ISDebug::ShowString(QuestionText);

	std::string String;
	std::getline(std::cin, String);
	QString InputAnswer(String.c_str());
	InputAnswer = InputAnswer.toLower();

	if (InputAnswer == "y")
	{
		Result = true;
	}
	else if (InputAnswer == "n")
	{
		Result = false;
	}
	else
	{
		ISDebug::ShowString("Invalid answer!");
		Question(QuestionText);
	}

	return Result;
}
//-----------------------------------------------------------------------------
