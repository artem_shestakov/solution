#pragma once
//-----------------------------------------------------------------------------
#include "issystem_global.h"
//-----------------------------------------------------------------------------
class ISSYSTEM_EXPORT ISNamespace
{
public:
	enum ApplicationType //��� ����������� ����������
	{
		AT_UNKNOWN,
		AT_GUI,
		AT_CONSOLE,
	};

	enum ShowDataType //���� ����������� ������
	{
		SDT_Actual, //�� ���������
		SDT_Deleted, //���������
	};

	enum FieldType //���� ������ ����
	{
		FT_Unknown, //�����������
		FT_Int, //����� �����
		FT_String, //������
		FT_Text, //������� ������
		FT_UID, //���������� �������������
		FT_Bool, //����������
		FT_Double, //����� � ��������� �������
		FT_ByteArray, //������ ������
		FT_Date, //����
		FT_Time, //�����
		FT_DateTime, //���� � �����
		FT_Inn, //������������� ����� �����������������
		FT_Kpp, //��� ������� ���������� �� ����
		FT_Ogrn, //�������� ��������������� ��������������� �����
		FT_Okpo, //�������������� ������������� ����������� � �����������
		FT_Bik, //���������� ���������������� �����
		FT_Icq,
		FT_Year, //���
		FT_Phone, //����� ��������
		FT_IPAddress, //IP-�����
		FT_Password, //������
		FT_Image, //�����������
		FT_Color, //����
		FT_EMail, //����� ����������� �����
		FT_Passport, //�������
	};

	enum ObjectFormType //���� ����� �������
	{
		OFT_New, //����� ������
		OFT_Copy, //����� �������
		OFT_Edit, //��������� �������
		OFT_Show, //�������� �������
	};

	enum NotificationFormType //���� ����� �����������
	{
		NFT_Create, //�������� �������
		NFT_Edit, //�������������� �������
		NFT_CreateCopy, //�������� ����� �������
		NFT_Delete, //�������� �������
		NFT_Recovery, //�������������� �������
	};

	enum SearchOperatorType //���� ���������� ������
	{
		SearchUnknown, //����������
		SearchAnyway, //�����
		SearchNotAnyway, //�� �����
		SearchBegins, //����������
		SearchEnds, //�������������
		SearchContains, //��������
		SearchNull, //�����
		SearchNotNull, //�� �����
		SearchMore, //������
		SearchLess, //������
		SearchMoreOrEqual, //������ ��� �����
		SearchLessOrEqual, //������ ��� �����
		SearchRange, //��������
	};

	enum ExportType //���� �������� �������� ������
	{
		ET_Unknown,//�����������
		ET_Excel,
		ET_Word,
	};

	enum SMSStateChangedType //���� �������� ��� ������
	{
		SMS_Sent, //��������� ����������
		SMS_SendingCompleted, //�������� ���������
		SMS_Error, //������
	};

	enum MessageBoxButton
	{
		MBB_Unknown, //���������� (�� ������������ ������ ����)
	};

	enum DeleteRecoveryObject
	{
		DRO_Delete,
		DRO_Recovery,
	};

	enum PermissionsType
	{
		PT_Unknown,
		PT_Show,
		PT_Create,
		PT_CreateCopy,
		PT_Edit,
		PT_Delete,
		PT_DeleteCascade,
		PT_UpdateList,
		PT_ShowDeleted,
		PT_ShowAll,
		PT_Search,
		PT_Export,
		PT_Print,
		PT_SystemInformation,
		PT_AttachTask,
		PT_TableNavigation,
	};

	enum ExitFormAction
	{
		EF_Action_Unknown,
		EF_Action_Exit,
		EF_Action_ChangeUser,
		EF_Action_Lock,
	};

	enum SearchHow
	{
		SH_Unknown,
		SH_BeginString,
		SH_EndString,
		SH_PartString,
		SH_ExactMatch,
	};

	enum UpdateResult
	{
		UR_ExitApplication,
		UR_ContinueWork,
		UR_ErrorStartUpdate,
	};

	enum ActionType
	{
		AT_Picking,
		AT_Select,
		AT_Create,
		AT_CreateCopy,
		AT_Edit,
		AT_Delete,
		AT_DeleteCascade,
		AT_Update,
		AT_ShowActual,
		AT_ShowDeleted,
		AT_Search,
		AT_SearchClear,
		AT_Export,
		AT_Print,
		AT_SystemInfo,
		AT_AttachTask,
		AT_Favorites,
		AT_Reference,
		AT_NavigationBegin,
		AT_NavigationPrevious,
		AT_NavigationNext,
		AT_NavigationLast,
	};

	enum ActionSpecialType
	{
		AST_SortDefault,
		AST_Note,
		AST_ResizeFromContent,
		AST_ResetWidthColumn,
		AST_CopyRecord,
	};

	enum ObjectActionType
	{
		OAT_Object, //�������� �������
		OAT_Escort, //��������� ����� (������/������)
		OAT_Service, //��������� ������ (�������� � �.�.)
		OAT_Other, //������ (��������, ����)
	};

	enum BorderItemType
	{
		BIT_Unknown,
		BIT_Top,
		BIT_TopLeft,
		BIT_TopRight,
		BIT_Left,
		BIT_Right,
		BIT_Bottom,
		BIT_BottomLeft,
		BIT_BottomRight,
	};

	enum LicenseFormAction
	{
		LFA_Unknown,
		LFA_Activated,
		LFA_TestingApplication,
		LFA_Exit,
	};

	enum AttachChatType
	{
		ACT_NoAttach,
		ACT_Image,
		ACT_File,
	};

	enum InputType
	{
		IT_Unknown,
		IT_Client,
		IT_Service,
	};

	enum ReportType
	{
		RT_Unknown,
		RT_Html,
		RT_Word,
	};

	enum QueryModelType
	{
		QMT_Object,
		QMT_List,
	};

	enum SearchFormResult
	{
		SFR_Unknown,
		SFR_Search,
		SFR_ClearResult,
	};

	enum FunctionNameFormat //��� �������������� ���������� � �������
	{
		FNF_All, //������
		FNF_TypeAndFunction, //��� � �������
		FNF_Type, //���
		FNF_Function, //�������
	};
};
//-----------------------------------------------------------------------------
Q_DECLARE_METATYPE(ISNamespace::ApplicationType);
Q_DECLARE_METATYPE(ISNamespace::ShowDataType);
Q_DECLARE_METATYPE(ISNamespace::FieldType);
Q_DECLARE_METATYPE(ISNamespace::ObjectFormType);
Q_DECLARE_METATYPE(ISNamespace::NotificationFormType);
Q_DECLARE_METATYPE(ISNamespace::SearchOperatorType);
Q_DECLARE_METATYPE(ISNamespace::ExportType);
Q_DECLARE_METATYPE(ISNamespace::SMSStateChangedType);
Q_DECLARE_METATYPE(ISNamespace::MessageBoxButton);
Q_DECLARE_METATYPE(ISNamespace::DeleteRecoveryObject);
Q_DECLARE_METATYPE(ISNamespace::PermissionsType);
Q_DECLARE_METATYPE(ISNamespace::ExitFormAction);
Q_DECLARE_METATYPE(ISNamespace::SearchHow);
Q_DECLARE_METATYPE(ISNamespace::UpdateResult);
Q_DECLARE_METATYPE(ISNamespace::ActionType);
Q_DECLARE_METATYPE(ISNamespace::ActionSpecialType);
Q_DECLARE_METATYPE(ISNamespace::ObjectActionType);
Q_DECLARE_METATYPE(ISNamespace::AttachChatType);
Q_DECLARE_METATYPE(ISNamespace::InputType);
Q_DECLARE_METATYPE(ISNamespace::ReportType);
Q_DECLARE_METATYPE(ISNamespace::QueryModelType);
Q_DECLARE_METATYPE(ISNamespace::SearchFormResult);
//-----------------------------------------------------------------------------
