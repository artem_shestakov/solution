#include "StdAfx.h"
#include "ISValidatorPhone.h"
#include "ISSystem.h"
//-----------------------------------------------------------------------------
ISValidatorPhone::ISValidatorPhone(QLineEdit *parent) : ISValidator(parent)
{

}
//-----------------------------------------------------------------------------
ISValidatorPhone::~ISValidatorPhone()
{

}
//-----------------------------------------------------------------------------
void ISValidatorPhone::Validate(QString &string)
{
	QString Temp;
	Fixup(string);

	if (string.length() > OldString.length())
	{
		Temp = string;
		Parse(string);
	}
	else
	{
		Temp = string;
		Parse(string);

		if (string.length())
		{
			if (string.at(string.length() - 1) == "-")
			{
				ISSystem::RemoveLastSymbolFromString(string);
			}
		}
	}

	OldString = Temp;
}
//-----------------------------------------------------------------------------
void ISValidatorPhone::Parse(QString &String)
{
	QString Temp;

	for (int i = 0; i < String.count(); i++) //����� ���������� ������ ��������
	{
		QChar InputSymbol = String.at(i); //������� ������

		Temp.append(InputSymbol);
		if (i == 2 || i == 5 || i == 7)
		{
			Temp.append("-");
		}
	}

	String = Temp;
}
//-----------------------------------------------------------------------------
void ISValidatorPhone::Fixup(QString &String)
{
	QString Temp;
	for (int i = String.count() - 1; i >= 0; i--)
	{
		if (Temp.length() == 10)
		{
			break;
		}
		else
		{
			QChar Symbol = String.at(i);
			if (Symbol.isDigit())
			{
				Temp.insert(0, Symbol);
			}
		}
	}
	String = Temp;
}
//-----------------------------------------------------------------------------
