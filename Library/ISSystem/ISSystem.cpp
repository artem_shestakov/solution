#include "StdAfx.h"
#include "ISSystem.h"
#include "EXDefines.h"
//-----------------------------------------------------------------------------
void ISSystem::SetObjectProperty(QObject *Object, const char *PropertyName, const QVariant &PropertyValue)
{
	if (Object)
	{
		Object->setProperty(PropertyName, PropertyValue);
	}
}
//-----------------------------------------------------------------------------
void ISSystem::TerminateCurrentProcess(int Code)
{
	TerminateProcess(GetCurrentProcess(), Code);
}
//-----------------------------------------------------------------------------
QDomElement ISSystem::GetDomElement(QFile &File)
{
	QDomDocument XmlDocument;
	XmlDocument.setContent(&File);
	QDomElement DomElement = XmlDocument.documentElement();
	return DomElement;
}
//-----------------------------------------------------------------------------
QDomElement ISSystem::GetDomElement(const QString &Content)
{
	QDomDocument XmlDocument;
	XmlDocument.setContent(Content);
	QDomElement DomElement = XmlDocument.documentElement();
	return DomElement;
}
//-----------------------------------------------------------------------------
void ISSystem::SleepMilliseconds(unsigned long Milliseconds)
{
	QThread::msleep(Milliseconds);
}
//-----------------------------------------------------------------------------
void ISSystem::SleepSeconds(unsigned long Seconds)
{
	QThread::sleep(Seconds);
}
//-----------------------------------------------------------------------------
qint64 ISSystem::GetFileSize(const QString &FilePath)
{
	QFileInfo FileInfo(FilePath);
	qint64 Size = FileInfo.size();
	return Size;
}
//-----------------------------------------------------------------------------
QString ISSystem::FileSizeFromString(qint64 FileSize)
{
	qint64 Size = FileSize;
	qint64 i = NULL;
	for (; Size > 1023; Size /= 1024, ++i){}
	QString String = QString().setNum(Size) + "BKMGT"[i];

	if (String.contains("B"))
	{
		String.replace("B", " B");
	}
	else if (String.contains("K"))
	{
		String.replace("K", " Kb");
	}
	else if (String.contains("M"))
	{
		String.replace("M", " Mb");
	}
	else if (String.contains("G"))
	{
		String.replace("G", " Gb");
	}
	else if (String.contains("T"))
	{
		String.replace("T", " Tb");
	}

	return String;
}
//-----------------------------------------------------------------------------
QString ISSystem::GetSizeDir(const QString &DirPath)
{
	QDir Dir(DirPath);
	QFileInfoList FileInfoList = Dir.entryInfoList(QDir::Filter::Files);

	qint64 Size = 0;

	for (int i = 0; i < FileInfoList.count(); i++)
	{
		QFileInfo FileInfo = FileInfoList.at(i);
		QString FilePath = FileInfo.absoluteFilePath();
		qint64 FileSize = GetFileSize(FilePath);
		Size += FileSize;
	}

	QString StringSize = FileSizeFromString(Size);
	return StringSize;
}
//-----------------------------------------------------------------------------
QString ISSystem::GetCompleteBaseFileName(const QString &FilePath)
{
	return QFileInfo(FilePath).completeBaseName();
}
//-----------------------------------------------------------------------------
QString ISSystem::GetFileName(const QString &FilePath)
{
	return QFileInfo(FilePath).fileName();
}
//-----------------------------------------------------------------------------
QString ISSystem::GetVersionInFileName(const QString &FileName)
{
	QStringList StringListFile = FileName.split("_");
	QString Version = StringListFile[StringListFile.length() - 1];
	QStringList StringListVersion = Version.split(".");

	QString VersionComplete;
	for (int i = 0; i < StringListVersion.count(); i++)
	{
		QString String = StringListVersion.at(i);
		VersionComplete += String + ".";
	}

	ISSystem::RemoveLastSymbolFromString(VersionComplete);
	return VersionComplete;
}
//-----------------------------------------------------------------------------
QString ISSystem::FormatQFuncInfo(const QString &QFuncInfo, ISNamespace::FunctionNameFormat Format)
{
	QString Result;
	QString Temp = QFuncInfo;

	switch (Format)
	{
	case ISNamespace::FNF_All:
		Result = QFuncInfo;
		break;

	case ISNamespace::FNF_TypeAndFunction:
		Result = Temp.replace("__thiscall ", QString());;
		break;

	case ISNamespace::FNF_Type:
		Result = Temp.replace("__thiscall ", QString()).split("::").at(0);
		break;

	case ISNamespace::FNF_Function:
		Result = Temp.split("::").at(1);
		break;
	}

	return Result;
}
//-----------------------------------------------------------------------------
QString ISSystem::GetGeneratedCode(int CountSymbols)
{
	QString GeneratedCode = QString();

	if (CountSymbols)
	{
		QString Code = GenerateUuid();
		Code.replace("{", QString());
		Code.replace("}", QString());
		Code.replace("-", QString());

		if (CountSymbols > Code.length())
		{
			return GeneratedCode;
		}

		for (int i = 0; i < CountSymbols; i++)
		{
			GeneratedCode += Code.at(i);
		}
	}

	return GeneratedCode;
}
//-----------------------------------------------------------------------------
QString ISSystem::GetCurrentDayOfWeekName()
{
	QString DayOfWeekName = QDate::longDayName(QDate::currentDate().dayOfWeek());
	BeginSymbolToUpper(DayOfWeekName);
	return DayOfWeekName;
}
//-----------------------------------------------------------------------------
QString ISSystem::GetDayOfWeekName(Qt::DayOfWeek Day)
{
	QString DayOfWeekName = QDate::longDayName(Day);
	DayOfWeekName.replace(0, 1, DayOfWeekName.at(0).toUpper()); //��������� ������ ����� � ������� �������
	return DayOfWeekName;
}
//-----------------------------------------------------------------------------
QString ISSystem::GetConfiguration()
{
	QString Configuration;

#ifdef QT_DEBUG
	Configuration = "Debug";
#else
	Configuration = "Release";
#endif

	return Configuration;
}
//-----------------------------------------------------------------------------
void ISSystem::ClearDirRecursive(const QString &DirPath)
{
	QDir Dir(DirPath);
	QStringList Files = Dir.entryList(QDir::Files);

	QStringList::Iterator FileIterator = Files.begin();
	while (FileIterator != Files.end())
	{
		QFile File(DirPath + "/" + *FileIterator);
		File.remove();
		++FileIterator;
	}

	QStringList Dirs = Dir.entryList(QDir::Dirs);
	QStringList::Iterator DirIterator = Dirs.begin();
	while (DirIterator != Dirs.end())
	{
		if (*DirIterator != "." && *DirIterator != "..")
		{
			ClearDirRecursive(DirPath + "/" + *DirIterator);
		}

		++DirIterator;
	}

	Dir.rmdir(DirPath);
}
//-----------------------------------------------------------------------------
bool ISSystem::LoadModule(const QString &ModulePath, QString &ErrorString)
{
	bool Result = false;

	QLibrary Library(ModulePath);
	Result = Library.load();

	if (!Result)
	{
		ErrorString = Library.errorString();
	}

	return Result;
}
//-----------------------------------------------------------------------------
void ISSystem::RemoveLastSymbolFromString(QString &String, int CountSymbols)
{
	String.remove(String.length() - CountSymbols, CountSymbols);
}
//-----------------------------------------------------------------------------
bool ISSystem::CallVirtualKeyboard()
{
	QUrl URL = QUrl::fromLocalFile(VIVRTUAL_KEYBOARD_PATH);
	bool Result = QDesktopServices::openUrl(URL);
	return Result;
}
//-----------------------------------------------------------------------------
bool ISSystem::OpenFile(const QString &FilePath)
{
	bool Result = false;

	SetWaitGlobalCursor(true);
	Result = QDesktopServices::openUrl(QUrl(QUrl::fromLocalFile(FilePath)));
	SetWaitGlobalCursor(false);

	return Result;
}
//-----------------------------------------------------------------------------
bool ISSystem::OpenFolder(const QString &FolderPath)
{
	bool Opened = QDesktopServices::openUrl(QUrl(FolderPath));
	return Opened;
}
//-----------------------------------------------------------------------------

void ISSystem::RepaintWidget(QWidget *Widget, bool adjust_size)
{
	if (adjust_size)
	{
		Widget->adjustSize();
	}

	Widget->repaint();
}
//-----------------------------------------------------------------------------
void ISSystem::ProcessEvents()
{
	qApp->processEvents();
}
//-----------------------------------------------------------------------------
void ISSystem::MoveWidgetToCenter(QWidget *Widget)
{
	QRect Rect = Widget->frameGeometry();
	Rect.moveCenter(QDesktopWidget().availableGeometry().center());
	Widget->move(Rect.topLeft());
}
//-----------------------------------------------------------------------------
void ISSystem::SetWaitGlobalCursor(bool Wait)
{
	if (Wait)
	{
		QApplication::setOverrideCursor(CURSOR_WAIT);
	}
	else
	{
		QApplication::restoreOverrideCursor();

		if (QApplication::overrideCursor())
		{
			if (QApplication::overrideCursor()->shape() == CURSOR_WAIT)
			{
				QApplication::restoreOverrideCursor();
			}
		}
	}
}
//-----------------------------------------------------------------------------
ISNamespace::ApplicationType ISSystem::GetApplicationType()
{
	QObject *ApplicationObject = qobject_cast<QApplication*>(QCoreApplication::instance());
	if (ApplicationObject)
	{
		return ISNamespace::AT_GUI;
	}
	else
	{
		return ISNamespace::AT_CONSOLE;
	}

	return ISNamespace::AT_UNKNOWN;
}
//-----------------------------------------------------------------------------
QString ISSystem::GenerateUuid()
{
	return QUuid::createUuid().toString().toUpper();
}
//-----------------------------------------------------------------------------
QString ISSystem::PrepareLongToolTip(QString ToolTipText, int MaxWidth)
{
	QFontMetrics FontMetrics(QToolTip::font());
	QString Result;

	for (;;)
	{
		int i = 0;
		while (i < ToolTipText.length())
		{
			if (FontMetrics.width(ToolTipText.left(++i + 1)) > MaxWidth)
			{
				int j = ToolTipText.lastIndexOf(' ', i);
				if (j > 0)
				{
					i = j;
				}

				Result += ToolTipText.left(i);
				Result += '\n';
				ToolTipText = ToolTipText.mid(i + 1);
				break;
			}
		}

		if (i >= ToolTipText.length())
		{
			break;
		}
	}

	return Result + ToolTipText;
}
//-----------------------------------------------------------------------------
void ISSystem::BeginSymbolToUpper(QString &String)
{
	if (String.length())
	{
		String = String.replace(0, 1, String.at(0).toUpper());
	}
}
//-----------------------------------------------------------------------------
QIcon ISSystem::GetIconFile(const QString &FilePath)
{
	QFileInfo FileInfo(FilePath);
	QFileIconProvider FileIconProvider;
	return FileIconProvider.icon(FileInfo);
}
//-----------------------------------------------------------------------------
QByteArray ISSystem::GetFileMD5(const QString &FilePath)
{
	QFile File(FilePath);
	if (File.open(QIODevice::ReadOnly))
	{
		QCryptographicHash MD5(QCryptographicHash::Md5);
		MD5.addData(File.readAll());
		File.close();
		return MD5.result();
	}

	return QByteArray();
}
//-----------------------------------------------------------------------------
int ISSystem::GetRandom(int Maximum)
{
	srand(time(NULL));
	int Result = 0;
	while (true)
	{
		Result = qrand();
		if (Result > Maximum)
		{
			continue;
		}
		else
		{
			return Result;
		}
	}
}
//-----------------------------------------------------------------------------
QString ISSystem::GetLogFileName()
{
	return APPLICATION_LOGS_PATH + "/" + APPLICATION_NAME + " " + QDateTime::currentDateTime().toString(DATE_FORMAT_STRING_V2) + "." + EXTENSION_LOG;
}
//-----------------------------------------------------------------------------
int ISSystem::TimeFromMinutes(const QTime &Time)
{
	int Minute = 0;

	if (Time.hour())
	{
		Minute += Time.hour() * 60;
	}

	if (Time.minute())
	{
		Minute += Time.minute();
	}

	return Minute;
}
//-----------------------------------------------------------------------------
QString ISSystem::MillisecondsToString(int Milliseconds)
{
	QString FormattedTime;

	int hours = Milliseconds / (1000 * 60 * 60);
	int minutes = (Milliseconds - (hours * 1000 * 60 * 60)) / (1000 * 60);
	int seconds = (Milliseconds - (minutes * 1000 * 60) - (hours * 1000 * 60 * 60)) / 1000;
	int milliseconds = Milliseconds - (seconds * 1000) - (minutes * 1000 * 60) - (hours * 1000 * 60 * 60);
	FormattedTime.append(QString("%1").arg(hours, 2, 10, QLatin1Char('0')) + ":" + QString("%1").arg(minutes, 2, 10, QLatin1Char('0')) + ":" + QString("%1").arg(seconds, 2, 10, QLatin1Char('0')) + ":" + QString("%1").arg(milliseconds, 3, 10, QLatin1Char('0')));

	return FormattedTime;
}
//-----------------------------------------------------------------------------
QDateTime ISSystem::GetCreatedDateFile(const QString &FilePath)
{
	QFileInfo FileInfo(FilePath);
	QDateTime DateTime = FileInfo.created();
	return DateTime;
}
//-----------------------------------------------------------------------------
QDateTime ISSystem::GetLastModifiedFile(const QString &FilePath)
{
	QFileInfo FileInfo(FilePath);
	QDateTime DateTime = FileInfo.lastModified();
	return DateTime;
}
//-----------------------------------------------------------------------------
QString ISSystem::GetDateLongName(const QDate &Date)
{
	QString Result = QString("%1 %2 %3").arg(Date.day()).arg(Date.longMonthName(Date.month())).arg(Date.year());
	return Result;
}
//-----------------------------------------------------------------------------
QString ISSystem::StringToBase64(const QString &String)
{
	QByteArray ByteArray = String.toUtf8();
	QByteArray StringBase64 = ByteArray.toBase64();
	return QString(StringBase64);
}
//-----------------------------------------------------------------------------
QString ISSystem::Base64ToString(const QString &Base64)
{
	QString String = QByteArray::fromBase64(Base64.toUtf8());
	return String;
}
//-----------------------------------------------------------------------------
QFont ISSystem::StringToFont(const QString &FontText)
{
	QFont FontReturned;

	if (FontText.length())
	{
		FontReturned = qvariant_cast<QFont>(FontText);
	}
	else
	{
		FontReturned = FONT_APPLICATION_STRING;
	}

	return FontReturned;
}
//-----------------------------------------------------------------------------
QByteArray ISSystem::PixmapToByteArray(const QPixmap &Pixmap)
{
	QPixmap ConvertingPixmap = Pixmap;

	QByteArray ByteArray;
	QBuffer Buffer(&ByteArray);
	bool Opened = Buffer.open(QIODevice::WriteOnly);
	bool Saved = ConvertingPixmap.save(&Buffer, "PNG");

	return ByteArray;
}
//-----------------------------------------------------------------------------
QPixmap ISSystem::ByteArrayToPixmap(const QByteArray &ByteArray)
{
	QPixmap Pixmap;
	bool Loaded = Pixmap.loadFromData(ByteArray, "PNG");
	return Pixmap;
}
//-----------------------------------------------------------------------------
QString ISSystem::SizeToString(const QSize &Size)
{
	int Height = Size.height();
	int Width = Size.width();

	QString HeightString = QString::number(Height);
	QString WidthString = QString::number(Width);

	QString SizeString = HeightString + " x " + WidthString;
	return SizeString;
}
//-----------------------------------------------------------------------------
QColor ISSystem::StringToColor(const QString &String)
{
	QColor Color(String);
	return Color;
}
//-----------------------------------------------------------------------------
double ISSystem::MillisecondToSecond(int Milliseconds)
{
	double Seconds = Milliseconds / 1000;
	return Seconds;
}
//-----------------------------------------------------------------------------
QVariantMap ISSystem::JsonStringToVariantMap(const QString &JsonString)
{
	QJsonDocument JsonDocument = QJsonDocument::fromJson(JsonString.toUtf8());
	QJsonObject JsonObject = JsonDocument.object();
	QVariantMap VariantMap = JsonObject.toVariantMap();
	return VariantMap;
}
//-----------------------------------------------------------------------------
QString ISSystem::VariantMapToJsonString(const QVariantMap &VariantMap)
{
	QJsonObject JsonObject = QJsonObject::fromVariantMap(VariantMap);
	QJsonDocument JsonDocument(JsonObject);
	QString String = JsonDocument.toJson();
	return String;
}
//-----------------------------------------------------------------------------
void ISSystem::Excel_SetBoldCell(QAxObject *Cell, bool bold)
{
	QAxObject *Font = Cell->querySubObject("Font");
	Font->setProperty("Bold", bold);
	delete Font;
}
//-----------------------------------------------------------------------------
void ISSystem::Excel_SetBackgrounCell(QAxObject *Cell, const QColor &Color)
{
	QAxObject* Interior = Cell->querySubObject("Interior");
	Interior->setProperty("Color", Color);
	delete Interior;
}
//-----------------------------------------------------------------------------
void ISSystem::Excel_CreateBorderCell(QAxObject *Cell)
{
	QAxObject *BorderTop = Cell->querySubObject("Borders(xlEdgeTop)");
	BorderTop->setProperty("LineStyle", 1);
	BorderTop->setProperty("Weight", 2);
	delete BorderTop;

	QAxObject *BorderLeft = Cell->querySubObject("Borders(xlEdgeLeft)");
	BorderLeft->setProperty("LineStyle", 1);
	BorderLeft->setProperty("Weight", 2);
	delete BorderLeft;

	QAxObject *BorderRight = Cell->querySubObject("Borders(xlEdgeRight)");
	BorderRight->setProperty("LineStyle", 1);
	BorderRight->setProperty("Weight", 2);
	delete BorderRight;

	QAxObject *BorderBottom = Cell->querySubObject("Borders(xlEdgeBottom)");
	BorderBottom->setProperty("LineStyle", 1);
	BorderBottom->setProperty("Weight", 2);
	delete BorderBottom;
}
//-----------------------------------------------------------------------------
