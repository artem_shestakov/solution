#include "StdAfx.h"
#include "ISLogger.h"
#include "EXDefines.h"
#include "ISAssert.h"
#include "ISSystem.h"
//-----------------------------------------------------------------------------
ISLogger::ISLogger() : QObject()
{
	LogPath = ISSystem::GetLogFileName();

	QFile LogFile(LogPath);
	bool Opened = LogFile.open(QIODevice::Append);
	IS_ASSERT(Opened, QString("Not opened log file \"%1\". Error: %2").arg(LogPath).arg(LogFile.errorString()));
	LogFile.close();

	FutureWatcher = new QFutureWatcher<void>(this);

	QTimer *Timer = new QTimer(this);
	Timer->setInterval(1000);
	connect(Timer, &QTimer::timeout, this, &ISLogger::Timeout);
	Timer->start();
}
//-----------------------------------------------------------------------------
ISLogger::~ISLogger()
{

}
//-----------------------------------------------------------------------------
ISLogger& ISLogger::GetInstance()
{
	static ISLogger Logger;
	return Logger;
}
//-----------------------------------------------------------------------------
void ISLogger::AddMessage(const QString &Message)
{
	Queue.enqueue(Message);
}
//-----------------------------------------------------------------------------
void ISLogger::AddMessageFunc(const QString &Function, const QString &Message)
{
	QString String = Function + ": " + Message;
	AddMessage(String);
}
//-----------------------------------------------------------------------------
void ISLogger::Timeout()
{
	if (Queue.count())
	{
		if (FutureWatcher->isRunning())
		{
			return;
		}

		QFuture<void> Future = QtConcurrent::run(this, &ISLogger::AddData);
		FutureWatcher->setFuture(Future);
	}
}
//-----------------------------------------------------------------------------
void ISLogger::AddData()
{
	while (Queue.count())
	{
		QString Message = Queue.dequeue();

		QFile File(LogPath);
		if (File.open(QIODevice::Append))
		{
			QTextStream TextStream(&File);
			TextStream << Message << endl;
			File.close();
		}
	}
}
//-----------------------------------------------------------------------------
