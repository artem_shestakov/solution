#pragma once
//-----------------------------------------------------------------------------
#include "issystem_global.h"
#include "ISValidator.h"
//-----------------------------------------------------------------------------
class ISSYSTEM_EXPORT ISValidatorPhone : ISValidator
{
	Q_OBJECT

public:
	ISValidatorPhone(QLineEdit *parent);
	virtual ~ISValidatorPhone();

	void Validate(QString &string) override;

protected:
	void Parse(QString &String); //������������ ����
	void Fixup(QString &String); //�������� ������ ��������

private:
	QString OldString;
};
//-----------------------------------------------------------------------------
