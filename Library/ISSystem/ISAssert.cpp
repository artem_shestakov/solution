#include "StdAfx.h"
#include "ISAssert.h"
#include "EXDefines.h"
#include "ISDebug.h"
#include "ISStackWalker.h"
//-----------------------------------------------------------------------------
ISASSERT::ISASSERT()
{

}
//-----------------------------------------------------------------------------
ISASSERT::~ISASSERT()
{

}
//-----------------------------------------------------------------------------
void ISASSERT::Assert(QObject *Object, const QString &FalseMessage, const QString &QFuncInfo, int SourceLine, const QString &File)
{
	if (!Object)
	{
		ShowAssert(FalseMessage, QFuncInfo, SourceLine, File);
	}
}
//-----------------------------------------------------------------------------
void ISASSERT::Assert(bool Boolean, const QString &FalseMessage, const QString &QFuncInfo, int SourceLine, const QString &File)
{
	if (!Boolean)
	{
		ShowAssert(FalseMessage, QFuncInfo, SourceLine, File);
	}
}
//-----------------------------------------------------------------------------
void ISASSERT::Assert(int Integer, const QString &FalseMessage, const QString &QFuncInfo, int SourceLine, const QString &File)
{
	if (!Integer)
	{
		ShowAssert(FalseMessage, QFuncInfo, SourceLine, File);
	}
}
//-----------------------------------------------------------------------------
void ISASSERT::Assert(QString String, const QString &FalseMessage, const QString &QFuncInfo, int SourceLine, const QString &File)
{
	if (!String.length())
	{
		ShowAssert(FalseMessage, QFuncInfo, SourceLine, File);
	}
}
//-----------------------------------------------------------------------------
void ISASSERT::ShowAssert(const QString &FalseMessage, const QString &QFuncInfo, int SourceLine, const QString &File)
{
	QString AssertMessage = QString();
	AssertMessage += "FUNCTION: " + QFuncInfo + ".\n";
	AssertMessage += "FILE: " + File + ".\n";
	AssertMessage += "SOURCE LINE: " + QString::number(SourceLine) + ". \n\n";
	AssertMessage += FalseMessage;

	ISDebug::ShowCriticalString(AssertMessage);
	MessageBox(nullptr, QString(AssertMessage).toStdWString().c_str(), L"ASSERT ERROR", MB_OK | MB_ICONERROR | MB_TASKMODAL);
	
	ISDebug::GetInstance().SetAssertMessage(AssertMessage);
	throw std::runtime_error("Exception");
}
//-----------------------------------------------------------------------------
