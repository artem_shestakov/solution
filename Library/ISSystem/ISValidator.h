#pragma once
//-----------------------------------------------------------------------------
#include "issystem_global.h"
//-----------------------------------------------------------------------------
class ISSYSTEM_EXPORT ISValidator : public QObject
{
	Q_OBJECT

public:
	ISValidator(QLineEdit *parent);
	virtual ~ISValidator();

	void StringChanged(const QString &string);

protected:
	virtual void Validate(QString &string) = 0;

private:
	QLineEdit *LineEdit;
};
//-----------------------------------------------------------------------------
