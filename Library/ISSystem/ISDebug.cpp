#include "StdAfx.h"
#include "ISDebug.h"
#include "EXDefines.h"
#include "ISProperty.h"
#include "ISLogger.h"
#include "ISSystem.h"
//-----------------------------------------------------------------------------
ISDebug::ISDebug() : QObject()
{
	
}
//-----------------------------------------------------------------------------
ISDebug::~ISDebug()
{

}
//-----------------------------------------------------------------------------
ISDebug& ISDebug::GetInstance()
{
	static ISDebug Debug;
	return Debug;
}
//-----------------------------------------------------------------------------
void ISDebug::ShowCaratString(QObject *Parent, const QString &String)
{
	GetInstance().CaratString(Parent->objectName(), String);
}
//-----------------------------------------------------------------------------
void ISDebug::ShowString(const QString &String)
{
	GetInstance().OutputString(String);
}
//-----------------------------------------------------------------------------
void ISDebug::ShowEmptyString(bool AddInLog)
{
	GetInstance().EmptyString(AddInLog);
}
//-----------------------------------------------------------------------------
void ISDebug::ShowDebugString(const QString &String)
{
	GetInstance().DebugString(String);
}
//-----------------------------------------------------------------------------
void ISDebug::ShowInfoString(const QString &String)
{
	GetInstance().InfoString(String);
}
//-----------------------------------------------------------------------------
void ISDebug::ShowWarningString(const QString &String)
{
	GetInstance().WarningString(String);
}
//-----------------------------------------------------------------------------
void ISDebug::ShowCriticalString(const QString &String)
{
	GetInstance().CriticalString(String);
}
//-----------------------------------------------------------------------------
void ISDebug::OutputString(const QString &String, bool AddInLog)
{
	QString NewString = String.simplified();

	if (ISSystem::GetApplicationType() == ISNamespace::AT_CONSOLE)
	{
		QTextStream TextStream(stdout);
		TextStream.setCodec(TEXT_CODEC_IBM866);
		TextStream << NewString << endl;
	}
	else
	{
		qDebug().noquote() << NewString;
	}

	if (AddInLog)
	{
		IS_LOGGER(NewString);
	}

	Mutex.lock();
	Messages.append(NewString);
	Mutex.unlock();

	emit Output(NewString);
}
//-----------------------------------------------------------------------------
void ISDebug::EmptyString(bool AddInLog)
{
	OutputString(QString(), AddInLog);
}
//-----------------------------------------------------------------------------
void ISDebug::CaratString(const QString &CoreName, const QString &String)
{
	QString CompleteString = QString("%1 [%2]: %3").arg(QDateTime::currentDateTime().toString(DATE_TIME_FORMAT_V4)).arg(CoreName).arg(String);
	OutputString(CompleteString);
}
//-----------------------------------------------------------------------------
void ISDebug::DebugString(const QString &String)
{
	QString CompleteString = GetCompleteString(QtMsgType::QtDebugMsg, String);
	OutputString(CompleteString);
}
//-----------------------------------------------------------------------------
void ISDebug::InfoString(const QString &String)
{
	QString CompleteString = GetCompleteString(QtMsgType::QtInfoMsg, String);
	OutputString(CompleteString);
}
//-----------------------------------------------------------------------------
void ISDebug::WarningString(const QString &String)
{
	QString CompleteString = GetCompleteString(QtMsgType::QtWarningMsg, String);
	OutputString(CompleteString);
}
//-----------------------------------------------------------------------------
void ISDebug::CriticalString(const QString &String)
{
	QString CompleteString = GetCompleteString(QtMsgType::QtCriticalMsg, String);
	OutputString(CompleteString);
}
//-----------------------------------------------------------------------------
void ISDebug::SetAssertMessage(const QString &assert_message)
{
	AssertMessage = assert_message;
}
//-----------------------------------------------------------------------------
QString ISDebug::GetAssertMessage() const
{
	return AssertMessage;
}
//-----------------------------------------------------------------------------
QStringList ISDebug::GetDebugMessages() const
{
	return Messages;
}
//-----------------------------------------------------------------------------
QString ISDebug::GetCompleteString(QtMsgType MessageType, const QString &String)
{
	QString MessageTypeString;

	switch (MessageType)
	{
	case QtMsgType::QtDebugMsg: MessageTypeString = "[Debug]"; break;
	case QtMsgType::QtInfoMsg: MessageTypeString = "[Info]"; break;
	case QtMsgType::QtWarningMsg: MessageTypeString = "[Warning]"; break;
	case QtMsgType::QtCriticalMsg: MessageTypeString = "[Critical]"; break;
	case QtMsgType::QtFatalMsg: MessageTypeString = "[Fatal]"; break;
	}

	QString CompleteString = QString("%1 %2 %3: %4").arg(QDateTime::currentDateTime().toString(DATE_TIME_FORMAT_V4)).arg(quint64(QThread::currentThreadId())).arg(MessageTypeString).arg(String);
	return CompleteString;
}
//-----------------------------------------------------------------------------
