#pragma once
//-----------------------------------------------------------------------------
#include "issystem_global.h"
//-----------------------------------------------------------------------------
#define IS_LOGGER(Message) ISLogger::GetInstance().AddMessage(Message) //�������� ��������� � ���-����
#define IS_LOGGER_FUNC(Message) ISLogger::GetInstance().AddMessageFunc(Q_FUNC_INFO, Message) //�������� ��������� � ���-���� �� �������
//-----------------------------------------------------------------------------
class ISSYSTEM_EXPORT ISLogger : public QObject
{
	Q_OBJECT

public:
	ISLogger(const ISLogger &) = delete;
	ISLogger(ISLogger &&) = delete;
	ISLogger &operator=(const ISLogger &) = delete;
	ISLogger &operator=(ISLogger &&) = delete;
	~ISLogger();

	static ISLogger& GetInstance(); //�������� ������ �������
	
	void AddMessage(const QString &Message); //�������� ��������� � ���-����
	void AddMessageFunc(const QString &Function, const QString &Message); //�������� ��������� � ���-���� �� �������

protected:
	void Timeout();
	void AddData(); //���������� ������ � ����

private:
	ISLogger();

	QFutureWatcher<void> *FutureWatcher;
	QQueue<QString> Queue;
	QString LogPath;
};
//-----------------------------------------------------------------------------
