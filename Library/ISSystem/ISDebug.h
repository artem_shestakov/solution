#pragma once
//-----------------------------------------------------------------------------
#include "issystem_global.h"
//-----------------------------------------------------------------------------
class ISSYSTEM_EXPORT ISDebug : public QObject
{
	Q_OBJECT

signals:
	void Output(const QString &String);

public:
	ISDebug(const ISDebug &) = delete;
	ISDebug(ISDebug &&) = delete;
	ISDebug &operator=(const ISDebug &) = delete;
	ISDebug &operator=(ISDebug &&) = delete;
	~ISDebug();

	static ISDebug& GetInstance();

	static void ShowCaratString(QObject *Parent, const QString &String);
	static void ShowString(const QString &String);
	static void ShowEmptyString(bool AddInLog = true);
	static void ShowDebugString(const QString &String);
	static void ShowInfoString(const QString &String);
	static void ShowWarningString(const QString &String);
	static void ShowCriticalString(const QString &String);

	void OutputString(const QString &String, bool AddInLog = true); //����� ���������
	void EmptyString(bool AddInLog = true); //�������� ������ ������

	void CaratString(const QString &CoreName, const QString &String); //����� ��������� � �����
	void DebugString(const QString &String); //����� ����������� ���������
	void InfoString(const QString &String); //����� ��������������� ���������
	void WarningString(const QString &String); //����� ��������������
	void CriticalString(const QString &String); //����� ������������ ���������

	void SetAssertMessage(const QString &assert_message);
	QString GetAssertMessage() const;
	QStringList GetDebugMessages() const;

protected:
	QString GetCompleteString(QtMsgType MessageType, const QString &String); //������������ ������
	
private:
	ISDebug();

	QStringList Messages;
	QString AssertMessage;
	QMutex Mutex;
};
//-----------------------------------------------------------------------------
