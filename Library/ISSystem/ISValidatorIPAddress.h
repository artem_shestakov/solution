#pragma once
//-----------------------------------------------------------------------------
#include "issystem_global.h"
#include "ISValidator.h"
//-----------------------------------------------------------------------------
class ISSYSTEM_EXPORT ISValidatorIPAddress : public ISValidator
{
	Q_OBJECT

public:
	ISValidatorIPAddress(QLineEdit *parent);
	virtual ~ISValidatorIPAddress();

protected:
	void Validate(QString &string) override;

private:
	QString OldAddress;
};
//-----------------------------------------------------------------------------
