#include "StdAfx.h"
#include "ISValidator.h"
#include "EXDefines.h"
#include "ISAssert.h"
//-----------------------------------------------------------------------------
ISValidator::ISValidator(QLineEdit *parent) : QObject(parent), LineEdit(parent)
{
	IS_ASSERT(parent, "Invalid parent validator is null");
	connect(LineEdit, &QLineEdit::textChanged, this, &ISValidator::StringChanged);
}
//-----------------------------------------------------------------------------
ISValidator::~ISValidator()
{

}
//-----------------------------------------------------------------------------
void ISValidator::StringChanged(const QString &string)
{
	disconnect(LineEdit, &QLineEdit::textChanged, this, &ISValidator::StringChanged);

	QString NewString = string;
	Validate(NewString);
	LineEdit->setText(NewString);

	connect(LineEdit, &QLineEdit::textChanged, this, &ISValidator::StringChanged);
}
//-----------------------------------------------------------------------------
