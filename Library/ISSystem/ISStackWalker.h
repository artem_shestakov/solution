#pragma once
//-----------------------------------------------------------------------------
#include "issystem_global.h"
#include "StackWalker.h"
//-----------------------------------------------------------------------------
class ISSYSTEM_EXPORT ISStackWalker : public StackWalker
{
public:
	ISStackWalker();

	virtual void OnOutput(LPCSTR szText);
	QString GetCallStack() const;

private:
	QString StackString;
};
//-----------------------------------------------------------------------------
