#include "StdAfx.h"
#include "PMetaClassFieldBox.h"
//-----------------------------------------------------------------------------
PMetaClassFieldBox::PMetaClassFieldBox(QObject *parent) : PMetaClass(parent)
{

}
//-----------------------------------------------------------------------------
PMetaClassFieldBox::~PMetaClassFieldBox()
{

}
//-----------------------------------------------------------------------------
void PMetaClassFieldBox::SetLocalName(const QString &local_name)
{
	LocalName = local_name;
}
//-----------------------------------------------------------------------------
QString PMetaClassFieldBox::GetLocalName() const
{
	return LocalName;
}
//-----------------------------------------------------------------------------
