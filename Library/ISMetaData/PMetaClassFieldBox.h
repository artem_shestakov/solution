#pragma once
//-----------------------------------------------------------------------------
#include "ismetadata_global.h"
#include "PMetaClass.h"
//-----------------------------------------------------------------------------
class ISMETADATA_EXPORT PMetaClassFieldBox : public PMetaClass
{
	Q_OBJECT

	Q_PROPERTY(QString LocalName READ GetLocalName WRITE SetLocalName)

public:
	PMetaClassFieldBox(QObject *parent = 0);
	virtual ~PMetaClassFieldBox();

	void SetLocalName(const QString &local_name);
	QString GetLocalName() const;

private:
	QString LocalName;
};
//-----------------------------------------------------------------------------
