#include "StdAfx.h"
#include "ISICQEdit.h"
#include "EXDefines.h"
#include "ISLocalization.h"
//-----------------------------------------------------------------------------
ISICQEdit::ISICQEdit(QObject *MetaField, QWidget *parent) : ISLineEdit(MetaField, parent)
{
	SetValidator(new QRegExpValidator(QRegExp(REG_EXP_NUMBER_COUNT_9), this));
	SetPlaceholderText(LOCALIZATION("Field.Icq.PlaceholderText"));
}
//-----------------------------------------------------------------------------
ISICQEdit::ISICQEdit(QWidget *parent) : ISICQEdit(nullptr, parent)
{

}
//-----------------------------------------------------------------------------
ISICQEdit::~ISICQEdit()
{

}
//-----------------------------------------------------------------------------
