#include "StdAfx.h"
#include "ISINNEdit.h"
#include "EXDefines.h"
#include "ISServiceButton.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
//-----------------------------------------------------------------------------
ISINNEdit::ISINNEdit(QObject *MetaField, QWidget *parent) : ISLineEdit(MetaField, parent)
{
	SetPlaceholderText(LOCALIZATION("Field.Inn.PlaceholderText"));
	SetMaxLength(12);

	ISServiceButton *ButtonSearch = new ISServiceButton(this);
	ButtonSearch->setToolTip(LOCALIZATION("SearchFromINN") + "...");
	ButtonSearch->setIcon(BUFFER_ICONS("Taxation"));
	ButtonSearch->setFlat(true);
	ButtonSearch->setStyleSheet(QString());
	connect(ButtonSearch, &ISServiceButton::clicked, this, &ISINNEdit::SearchFromINN);
	AddWidgetToRight(ButtonSearch);
}
//-----------------------------------------------------------------------------
ISINNEdit::ISINNEdit(QWidget *parent) : ISINNEdit(nullptr, parent)
{

}
//-----------------------------------------------------------------------------
ISINNEdit::~ISINNEdit()
{

}
//-----------------------------------------------------------------------------
bool ISINNEdit::IsValid() const
{
	int CountSymbols = GetValue().toString().length();
	if (CountSymbols == 10 || CountSymbols == 12)
	{
		return true;
	}

	return false;
}
//-----------------------------------------------------------------------------
