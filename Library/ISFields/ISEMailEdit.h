#pragma once
//-----------------------------------------------------------------------------
#include "isfields_global.h"
#include "ISLineEdit.h"
//-----------------------------------------------------------------------------
class ISFIELDS_EXPORT ISEMailEdit : public ISLineEdit
{
	Q_OBJECT

public:
	Q_INVOKABLE ISEMailEdit(QObject *MetaField, QWidget *parent);
	Q_INVOKABLE ISEMailEdit(QWidget *parent);
	virtual ~ISEMailEdit();

	bool IsValid() const override;

protected:
	void EMailChanged(const QVariant &value);

protected:
	QRegExpValidator *RegExpValidator;
};
//-----------------------------------------------------------------------------
