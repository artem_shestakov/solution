#include "StdAfx.h"
#include "ISFieldEditBase.h"
#include "EXDefines.h"
#include "ISAssert.h"
#include "ISSystem.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISMessageBox.h"
//-----------------------------------------------------------------------------
ISFieldEditBase::ISFieldEditBase(QObject *MetaField, QWidget *parent) : QWidget(parent)
{
	ObjectField = MetaField;
	ButtonHint = nullptr;
	LabelReadOnly = nullptr;
	EditWidget = nullptr;
	FieldEditPointer = nullptr;
	ModificationFlag = false;
	BorderRed = false;

	//������� �����������
	MainLayout = new QHBoxLayout();
	MainLayout->setContentsMargins(LAYOUT_MARGINS_NULL);
	setLayout(MainLayout);

	//����� �����������
	LayoutLeft = new QHBoxLayout();
	MainLayout->addLayout(LayoutLeft);

	//����������� ���������
	LayoutEdit = new QHBoxLayout();
	LayoutEdit->setContentsMargins(LAYOUT_MARGINS_2_PX);
	MainLayout->addLayout(LayoutEdit);

	//������ �����������
	LayoutRight = new QHBoxLayout();
	MainLayout->addLayout(LayoutRight);

	//������ ������� �������� ����
	ButtonClear = new ISButtonClear(this);
	connect(ButtonClear, &ISButtonClear::clicked, this, &ISFieldEditBase::Clear);
	connect(ButtonClear, &ISButtonClear::clicked, this, &ISFieldEditBase::SetFocus);
	MainLayout->addWidget(ButtonClear);

	if (MetaField)
	{
		FieldName = MetaField->property("Name").toString();
		Size = MetaField->property("Size").toInt();
		Upper = MetaField->property("Upper").toBool();
		Lower = MetaField->property("Lower").toBool();
		DefaultValueWidget = MetaField->property("DefaultValueWidget");
		NotNull = MetaField->property("NotNull").toBool();
		ReadOnly = MetaField->property("ReadOnly").toBool();
		PlaceholderText = MetaField->property("PlaceholderText").toString();
		RegExp = MetaField->property("RegExp").toString();
		HideFromObject = MetaField->property("HideFromObject").toBool();

		CreateHint(MetaField->property("Hint").toString());
	}
	else
	{
		Size = 0;
		ReadOnly = false;
		NotNull = false;
		Upper = false;
		Lower = false;
		HideFromObject = false;
	}
}
//-----------------------------------------------------------------------------
ISFieldEditBase::ISFieldEditBase(QWidget *parent) : ISFieldEditBase(nullptr, parent)
{

}
//-----------------------------------------------------------------------------
ISFieldEditBase::~ISFieldEditBase()
{

}
//-----------------------------------------------------------------------------
void ISFieldEditBase::paintEvent(QPaintEvent *PaintEvent)
{
	QWidget::paintEvent(PaintEvent);

	if (EditWidget)
	{
		if (BorderRed)
		{
			//������������ �����
			QRect Rect(EditWidget->pos().x() - 1, EditWidget->pos().y() - 1, EditWidget->width() + 2, EditWidget->height() + 2);

			QPainter Painter(this);
			Painter.setBrush(QBrush(Qt::transparent, Qt::SolidPattern));
			Painter.setPen(QPen(EDIT_WIDGET_COLOR_RED, 2.5, Qt::SolidLine));
			Painter.drawRect(Rect);
		}
	}
}
//-----------------------------------------------------------------------------
void ISFieldEditBase::SetValue(const QVariant &value)
{
	std::runtime_error(Q_FUNC_INFO);
}
//-----------------------------------------------------------------------------
QVariant ISFieldEditBase::GetValue() const
{
	std::runtime_error(Q_FUNC_INFO);
	return QVariant();
}
//-----------------------------------------------------------------------------
void ISFieldEditBase::Clear()
{
	std::runtime_error(Q_FUNC_INFO);
}
//-----------------------------------------------------------------------------
bool ISFieldEditBase::IsValid() const
{
	return true;
}
//-----------------------------------------------------------------------------
void ISFieldEditBase::SetFont(const QFont &Font)
{
	EditWidget->setFont(Font);
}
//-----------------------------------------------------------------------------
void ISFieldEditBase::SetVisibleClear(bool visible)
{
	ButtonClear->setVisible(visible);
}
//-----------------------------------------------------------------------------
void ISFieldEditBase::SetEnabledClear(bool enabled)
{
	ButtonClear->setEnabled(enabled);
}
//-----------------------------------------------------------------------------
void ISFieldEditBase::SetCursor(const QCursor &Cursor)
{
	EditWidget->setCursor(Cursor);
}
//-----------------------------------------------------------------------------
void ISFieldEditBase::SetToolTip(const QString &ToolTip)
{
	EditWidget->setToolTip(ToolTip);
}
//-----------------------------------------------------------------------------
void ISFieldEditBase::BlinkRed()
{
	EditWidget->clearFocus();

	BorderRed = true;
	repaint();
	ISSystem::SleepMilliseconds(100);

	BorderRed = false;
	repaint();
	ISSystem::SleepMilliseconds(100);

	BorderRed = true;
	repaint();
	ISSystem::SleepMilliseconds(100);

	BorderRed = false;
	repaint();
	ISSystem::SleepMilliseconds(100);

	BorderRed = true;
	repaint();
	ISSystem::SleepMilliseconds(100);

	BorderRed = false;
	repaint();
	ISSystem::SleepMilliseconds(100);

	SetFocus();
}
//-----------------------------------------------------------------------------
QObject* ISFieldEditBase::GetMetaField()
{
	return ObjectField;
}
//-----------------------------------------------------------------------------
QString ISFieldEditBase::GetFieldName() const
{
	return FieldName;
}
//-----------------------------------------------------------------------------
int ISFieldEditBase::GetSize() const
{
	return Size;
}
//-----------------------------------------------------------------------------
bool ISFieldEditBase::GetUpper() const
{
	return Upper;
}
//-----------------------------------------------------------------------------
bool ISFieldEditBase::GetLower() const
{
	return Lower;
}
//-----------------------------------------------------------------------------
QVariant ISFieldEditBase::GetDefaultValueWidget() const
{
	return DefaultValueWidget;
}
//-----------------------------------------------------------------------------
bool ISFieldEditBase::GetNotNull() const
{
	return NotNull;
}
//-----------------------------------------------------------------------------
bool ISFieldEditBase::GetHideFromObject() const
{
	return HideFromObject;
}
//-----------------------------------------------------------------------------
QString ISFieldEditBase::GetPlaceholderText() const
{
	return PlaceholderText;
}
//-----------------------------------------------------------------------------
QString ISFieldEditBase::GetRegExp() const
{
	return RegExp;
}
//-----------------------------------------------------------------------------
void ISFieldEditBase::SetReadOnly(bool read_only)
{
	ReadOnly = read_only;
	bool Invoked = QMetaObject::invokeMethod(FieldEditPointer, "SetReadOnly", Q_ARG(bool, read_only));
	CheckInvokeMethod(Invoked);
	CreateLabelReadOnly(read_only);
}
//-----------------------------------------------------------------------------
bool ISFieldEditBase::GetReadOnly() const
{
	return ReadOnly;
}
//-----------------------------------------------------------------------------
void ISFieldEditBase::SetModificationFlag(bool modification_flag)
{
	ModificationFlag = modification_flag;
}
//-----------------------------------------------------------------------------
bool ISFieldEditBase::GetModificationFlag() const
{
	return ModificationFlag;
}
//-----------------------------------------------------------------------------
void ISFieldEditBase::SetBorderRed(bool border_red)
{
	BorderRed = border_red;
}
//-----------------------------------------------------------------------------
bool ISFieldEditBase::GetBorderRed() const
{
	return BorderRed;
}
//-----------------------------------------------------------------------------
void ISFieldEditBase::CreateHint(const QString &Hint)
{
	if (Hint.length())
	{
		if (ButtonHint)
		{
			ButtonHint->setAccessibleDescription(Hint);
		}
		else
		{
			ButtonHint = new QToolButton(this);
			ButtonHint->setFocusPolicy(Qt::NoFocus);
			ButtonHint->setIcon(BUFFER_ICONS("Hint"));
			ButtonHint->setAutoRaise(true);
			ButtonHint->setFixedSize(SIZE_22_22);
			ButtonHint->setCursor(CURSOR_POINTING_HAND);
			ButtonHint->setToolTip(LOCALIZATION("ClickToViewHelp"));
			ButtonHint->setAccessibleDescription(Hint);
			connect(ButtonHint, &QToolButton::clicked, this, &ISFieldEditBase::ShowHint);
			LayoutLeft->addWidget(ButtonHint);
		}
	}
}
//-----------------------------------------------------------------------------
void ISFieldEditBase::CreateLabelReadOnly(bool read_only)
{
	if (read_only)
	{
		IS_ASSERT(!LabelReadOnly, "Label read only already created.");

		LabelReadOnly = new QLabel(this);
		LabelReadOnly->setToolTip(LOCALIZATION("FieldReadOnly"));
		LabelReadOnly->setCursor(CURSOR_WHATS_THIS);
		LabelReadOnly->setPixmap(BUFFER_ICONS("Exclamation").pixmap(SIZE_16_16));
		LabelReadOnly->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Expanding);
		AddWidgetToLeft(LabelReadOnly);
	}
}
//-----------------------------------------------------------------------------
void ISFieldEditBase::SetFocus()
{
	IS_ASSERT(EditWidget, "Null EditWidget");
	EditWidget->setFocus();
}
//-----------------------------------------------------------------------------
void ISFieldEditBase::CheckInvokeMethod(bool Invoked)
{
	IS_ASSERT(Invoked, QString("Invoke method not done: %1").arg(FieldName));
}
//-----------------------------------------------------------------------------
void ISFieldEditBase::ShowHint()
{
	QToolButton *SenderButton = dynamic_cast<QToolButton*>(sender());
	if (SenderButton)
	{
		ISMessageBox::ShowInformation(this, SenderButton->accessibleDescription());
	}
}
//-----------------------------------------------------------------------------
void ISFieldEditBase::ValueChanged()
{
	SetModificationFlag(true);
	emit DataChanged();
	emit ValueChange(GetValue());
}
//-----------------------------------------------------------------------------
void ISFieldEditBase::AddWidgetEdit(QWidget *edit_widget, QWidget *field_edit_pointer)
{
	IS_ASSERT(!EditWidget, "EditWidget already exist");

	EditWidget = edit_widget;
	FieldEditPointer = field_edit_pointer;

	LayoutEdit->addWidget(edit_widget);
}
//-----------------------------------------------------------------------------
void ISFieldEditBase::AddWidgetEditStretch()
{
	LayoutEdit->addStretch();
}
//-----------------------------------------------------------------------------
void ISFieldEditBase::AddMainLayoutStretch()
{
	MainLayout->addStretch();
}
//-----------------------------------------------------------------------------
void ISFieldEditBase::AddWidgetToLeft(QWidget *widget)
{
	LayoutLeft->addWidget(widget);
}
//-----------------------------------------------------------------------------
void ISFieldEditBase::AddWidgetToRight(QWidget *widget)
{
	LayoutRight->addWidget(widget);
}
//-----------------------------------------------------------------------------
