#include "StdAfx.h"
#include "ISDoubleEdit.h"
#include "EXDefines.h"
#include "ISStyleSheet.h"
//-----------------------------------------------------------------------------
ISDoubleEdit::ISDoubleEdit(QObject *MetaField, QWidget *parent) : ISFieldEditBase(MetaField, parent)
{
	DoubleSpinBox = new ISQDoubleSpinBox(this);
	DoubleSpinBox->setStyleSheet(STYLE_SHEET("ISDoubleEdit"));
	DoubleSpinBox->setMinimumHeight(SIZE_MINIMUM_HEIGHT_EDIT_FIELD);
	DoubleSpinBox->setSpecialValueText(SYMBOL_SPACE);
	DoubleSpinBox->setAccelerated(true);
	DoubleSpinBox->setReadOnly(GetReadOnly());
	connect(DoubleSpinBox, static_cast<void(ISQDoubleSpinBox::*)(double)>(&ISQDoubleSpinBox::valueChanged), this, &ISDoubleEdit::ValueChanged);
	connect(DoubleSpinBox, static_cast<void(ISQDoubleSpinBox::*)(const QString &)>(&ISQDoubleSpinBox::valueChanged), this, &ISDoubleEdit::OnValueChanged);
	connect(DoubleSpinBox, static_cast<void(ISQDoubleSpinBox::*)(const QString &)>(&ISQDoubleSpinBox::TextChanged), this, &ISDoubleEdit::ValueChanged);
	connect(DoubleSpinBox, static_cast<void(ISQDoubleSpinBox::*)(const QString &)>(&ISQDoubleSpinBox::TextChanged), this, &ISDoubleEdit::OnValueChanged);
	AddWidgetEdit(DoubleSpinBox, this);
}
//-----------------------------------------------------------------------------
ISDoubleEdit::ISDoubleEdit(QWidget *parent) : ISDoubleEdit(nullptr, parent)
{

}
//-----------------------------------------------------------------------------
ISDoubleEdit::~ISDoubleEdit()
{

}
//-----------------------------------------------------------------------------
void ISDoubleEdit::SetValue(const QVariant &value)
{
	DoubleSpinBox->setValue(value.toDouble());
	OnValueChanged(value.toString());
}
//-----------------------------------------------------------------------------
QVariant ISDoubleEdit::GetValue() const
{
	if (DoubleValue.length())
	{
		return DoubleSpinBox->value();
	}

	return QVariant();
}
//-----------------------------------------------------------------------------
void ISDoubleEdit::Clear()
{
	DoubleSpinBox->clear();
	DoubleSpinBox->setSpecialValueText(SYMBOL_SPACE);
	DoubleSpinBox->clearFocus();
	DoubleSpinBox->setFocus();
}
//-----------------------------------------------------------------------------
void ISDoubleEdit::SetReadOnly(bool read_only)
{
	DoubleSpinBox->setReadOnly(read_only);
}
//-----------------------------------------------------------------------------
void ISDoubleEdit::OnValueChanged(const QString &Text)
{
	if (Text.length()) //���� �������� ������� (�����)
	{
		if (Text == "0")
		{
			DoubleSpinBox->setSpecialValueText(QString());
		}
	}
	else
	{
		Clear();
	}

	DoubleValue = Text;
}
//-----------------------------------------------------------------------------
