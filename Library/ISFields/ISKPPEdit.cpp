#include "StdAfx.h"
#include "ISKPPEdit.h"
#include "EXDefines.h"
#include "ISLocalization.h"
//-----------------------------------------------------------------------------
ISKPPEdit::ISKPPEdit(QObject *MetaField, QWidget *parent) : ISLineEdit(MetaField, parent)
{
	SetValidator(new QRegExpValidator(QRegExp(REG_EXP_NUMBER_COUNT_9), this));
	SetPlaceholderText(LOCALIZATION("Field.Kpp.PlaceholderText"));
}
//-----------------------------------------------------------------------------
ISKPPEdit::ISKPPEdit(QWidget *parent) : ISKPPEdit(nullptr, parent)
{

}
//-----------------------------------------------------------------------------
ISKPPEdit::~ISKPPEdit()
{

}
//-----------------------------------------------------------------------------
