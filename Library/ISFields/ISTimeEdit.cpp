#include "StdAfx.h"
#include "ISTimeEdit.h"
#include "EXDefines.h"
//-----------------------------------------------------------------------------
ISTimeEdit::ISTimeEdit(QObject *MetaField, QWidget *parent) : ISDateTimeEdit(MetaField, parent)
{
	SetDisplayFormat(TIME_FORMAT_STRING_V1);
	SetVisibleCalendar(false);
	SetSelectSection(ISQDateTimeEdit::HourSection);
}
//-----------------------------------------------------------------------------
ISTimeEdit::ISTimeEdit(QWidget *parent) : ISTimeEdit(nullptr, parent)
{

}
//-----------------------------------------------------------------------------
ISTimeEdit::~ISTimeEdit()
{

}
//-----------------------------------------------------------------------------
void ISTimeEdit::SetValue(const QVariant &value)
{
	if (value.isValid())
	{
		ISDateTimeEdit::SetTime(value.toTime());
	}
	else
	{
		GetCheckEnable()->setChecked(false);
	}
}
//-----------------------------------------------------------------------------
QVariant ISTimeEdit::GetValue() const
{
	QVariant TimeValue = ISDateTimeEdit::GetValue();
	if (TimeValue.isValid())
	{
		QTime Time = TimeValue.toTime();
		Time = QTime::fromString(Time.toString(TIME_FORMAT_STRING_V1));
		return Time;
	}

	return QVariant();
}
//-----------------------------------------------------------------------------
