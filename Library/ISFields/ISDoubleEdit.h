#pragma once
//-----------------------------------------------------------------------------
#include "isfields_global.h"
#include "ISFieldEditBase.h"
#include "ISQDoubleSpinBox.h"
//-----------------------------------------------------------------------------
class ISFIELDS_EXPORT ISDoubleEdit : public ISFieldEditBase
{
	Q_OBJECT

public:
	Q_INVOKABLE ISDoubleEdit(QObject *MetaField, QWidget *parent);
	Q_INVOKABLE ISDoubleEdit(QWidget *parent);
	virtual ~ISDoubleEdit();

	void SetValue(const QVariant &value) override;
	QVariant GetValue() const override;
	void Clear() override;

public slots:
	void SetReadOnly(bool read_only);

protected:
	void OnValueChanged(const QString &Text);

private:
	ISQDoubleSpinBox *DoubleSpinBox;
	QString DoubleValue;
};
//-----------------------------------------------------------------------------
