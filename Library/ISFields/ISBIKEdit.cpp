#include "StdAfx.h"
#include "ISBIKEdit.h"
#include "EXDefines.h"
#include "ISLocalization.h"
//-----------------------------------------------------------------------------
ISBIKEdit::ISBIKEdit(QObject *MetaField, QWidget *parent) : ISLineEdit(MetaField, parent)
{
	SetValidator(new QRegExpValidator(QRegExp(REG_EXP_NUMBER_COUNT_9), this));
	SetPlaceholderText(LOCALIZATION("Field.Bik.PlaceholderText"));
}
//-----------------------------------------------------------------------------
ISBIKEdit::ISBIKEdit(QWidget *parent) : ISBIKEdit(nullptr, parent)
{

}
//-----------------------------------------------------------------------------
ISBIKEdit::~ISBIKEdit()
{

}
//-----------------------------------------------------------------------------
