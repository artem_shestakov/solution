#include "StdAfx.h"
#include "ISTextEdit.h"
#include "ISAssert.h"
#include "EXDefines.h"
//-----------------------------------------------------------------------------
ISTextEdit::ISTextEdit(QObject *MetaField, QWidget *parent) : ISFieldEditBase(MetaField, parent)
{
	TextEdit = new ISQTextEdit(this);
	TextEdit->setReadOnly(GetReadOnly());
	TextEdit->setPlaceholderText(GetPlaceholderText());
	TextEdit->setTabChangesFocus(true);
	TextEdit->setAcceptRichText(false);
	connect(TextEdit, &ISQTextEdit::textChanged, this, &ISTextEdit::ValueChanged);
	connect(TextEdit, &ISQTextEdit::KeyPressEnter, this, &ISTextEdit::KeyPressEnter);
	AddWidgetEdit(TextEdit, this);
}
//-----------------------------------------------------------------------------
ISTextEdit::ISTextEdit(QWidget *parent) : ISTextEdit(nullptr, parent)
{

}
//-----------------------------------------------------------------------------
ISTextEdit::~ISTextEdit()
{

}
//-----------------------------------------------------------------------------
void ISTextEdit::SetValue(const QVariant &value)
{
	TextEdit->setText(value.toString());
}
//-----------------------------------------------------------------------------
QVariant ISTextEdit::GetValue() const
{
	if (TextEdit->toPlainText().length())
	{
		return TextEdit->toPlainText();
	}

	return QVariant();
}
//-----------------------------------------------------------------------------
void ISTextEdit::Clear()
{
	TextEdit->clear();
}
//-----------------------------------------------------------------------------
void ISTextEdit::AddText(const QString &Text)
{
	TextEdit->append(Text);
}
//-----------------------------------------------------------------------------
void ISTextEdit::SetPlaceholderText(const QString &PlaceholderText)
{
	TextEdit->setPlaceholderText(PlaceholderText);
}
//-----------------------------------------------------------------------------
void ISTextEdit::SetExecuteEnter(bool Execute)
{
	TextEdit->SetExecuteEnter(Execute);
}
//-----------------------------------------------------------------------------
void ISTextEdit::SetUppercase(bool uppercase)
{
	if (uppercase)
	{
		connect(TextEdit, &ISQTextEdit::textChanged, this, &ISTextEdit::OnUpperText);
	}
	else
	{
		disconnect(TextEdit, &ISQTextEdit::textChanged, this, &ISTextEdit::OnUpperText);
	}
}
//-----------------------------------------------------------------------------
void ISTextEdit::SetLowercase(bool lowercase)
{
	if (lowercase)
	{
		connect(TextEdit, &ISQTextEdit::textChanged, this, &ISTextEdit::OnLowerText);
	}
	else
	{
		disconnect(TextEdit, &ISQTextEdit::textChanged, this, &ISTextEdit::OnLowerText);
	}
}
//-----------------------------------------------------------------------------
ISQTextEdit* ISTextEdit::GetTextEdit()
{
	return TextEdit;
}
//-----------------------------------------------------------------------------
void ISTextEdit::SetReadOnly(bool read_only)
{
	TextEdit->setReadOnly(read_only);
	SetVisibleClear(!read_only);
}
//-----------------------------------------------------------------------------
void ISTextEdit::OnUpperText()
{
	disconnect(TextEdit, &ISQTextEdit::textChanged, this, &ISTextEdit::OnUpperText);

	QTextCursor TextCursor = TextEdit->textCursor();
	int Position = TextCursor.position();
	TextEdit->setText(TextEdit->toPlainText().toUpper());
	TextCursor.setPosition(Position);
	TextEdit->setTextCursor(TextCursor);

	connect(TextEdit, &ISQTextEdit::textChanged, this, &ISTextEdit::OnUpperText);
}
//-----------------------------------------------------------------------------
void ISTextEdit::OnLowerText()
{
	disconnect(TextEdit, &ISQTextEdit::textChanged, this, &ISTextEdit::OnLowerText);

	QTextCursor TextCursor = TextEdit->textCursor();
	int Position = TextCursor.position();
	TextEdit->setText(TextEdit->toPlainText().toLower());
	TextCursor.setPosition(Position);
	TextEdit->setTextCursor(TextCursor);

	connect(TextEdit, &ISQTextEdit::textChanged, this, &ISTextEdit::OnLowerText);
}
//-----------------------------------------------------------------------------
void ISTextEdit::ResetFontcase()
{
	disconnect(TextEdit, &ISQTextEdit::textChanged, this, &ISTextEdit::OnUpperText);
	disconnect(TextEdit, &ISQTextEdit::textChanged, this, &ISTextEdit::OnLowerText);
}
//-----------------------------------------------------------------------------
