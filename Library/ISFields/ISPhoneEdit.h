#pragma once
//-----------------------------------------------------------------------------
#include "ISLineEdit.h"
#include "ISServiceButton.h"
//-----------------------------------------------------------------------------
class ISFIELDS_EXPORT ISPhoneEdit : public ISLineEdit
{
	Q_OBJECT

signals:
	void Called();

public:
	Q_INVOKABLE ISPhoneEdit(QObject *MetaField, QWidget *parent);
	Q_INVOKABLE ISPhoneEdit(QWidget *parent);
	virtual ~ISPhoneEdit();

	void SetValue(const QVariant &value);
	QVariant GetValue() const;

	bool IsValid() const override; //�������� ������������ ������

protected:
	void Call();
	void PhoneChanged();

private:
	ISServiceButton *ButtonCall;
	QString original; //��������� �������
	QString LastPhone;
};
//-----------------------------------------------------------------------------
