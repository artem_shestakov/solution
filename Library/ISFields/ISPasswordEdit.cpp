#include "StdAfx.h"
#include "ISPasswordEdit.h"
#include "EXDefines.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISPassword.h"
//-----------------------------------------------------------------------------
ISPasswordEdit::ISPasswordEdit(QObject *MetaField, QWidget *parent) : ISLineEdit(MetaField, parent)
{
	PasswordCheckeder = true;

	SetEchoMode(QLineEdit::Password);
	SetPlaceholderText(LOCALIZATION("Field.Password.PlaceholderText"));
	connect(GetLineEdit(), &QLineEdit::textChanged, this, &ISPasswordEdit::PasswordChanged);

	CheckPasswordVisible = new QCheckBox(this);
	CheckPasswordVisible->setFocusPolicy(Qt::NoFocus);
	CheckPasswordVisible->setCursor(CURSOR_POINTING_HAND);
	CheckPasswordVisible->setToolTip(LOCALIZATION("ShowHidePassword"));
	connect(CheckPasswordVisible, &QCheckBox::stateChanged, this, &ISPasswordEdit::PasswordVisibleChanged);
	AddWidgetToLeft(CheckPasswordVisible);

	LabelReliability = new QLabel(this);
	LabelReliability->setPixmap(BUFFER_ICONS("PasswordChanger.Null").pixmap(SIZE_20_20));
	LabelReliability->setVisible(PasswordCheckeder);
	LabelReliability->setToolTip(LOCALIZATION("ReliabilityPassword"));
	LabelReliability->setFocusPolicy(Qt::NoFocus);
	LabelReliability->setCursor(CURSOR_WHATS_THIS);
	LabelReliability->setFixedSize(SIZE_22_22);
	AddWidgetToRight(LabelReliability);

	ButtonGeneratePassword = new ISServiceButton(this);
	ButtonGeneratePassword->setVisible(true);
	ButtonGeneratePassword->setToolTip(LOCALIZATION("GeneratePassword") + "...");
	ButtonGeneratePassword->setIcon(BUFFER_ICONS("PasswordGenerate"));
	connect(ButtonGeneratePassword, &ISPushButton::clicked, this, &ISPasswordEdit::GeneratePassword);
	AddWidgetToRight(ButtonGeneratePassword);

	SetVisibleGenerate(false);
	SetPasswordCheckeder(false);
}
//-----------------------------------------------------------------------------
ISPasswordEdit::ISPasswordEdit(QWidget *parent) : ISPasswordEdit(nullptr, parent)
{

}
//-----------------------------------------------------------------------------
ISPasswordEdit::~ISPasswordEdit()
{

}
//-----------------------------------------------------------------------------
void ISPasswordEdit::SetVisibleGenerate(bool Visible)
{
	ButtonGeneratePassword->setVisible(Visible);
}
//-----------------------------------------------------------------------------
void ISPasswordEdit::SetPasswordCheckeder(bool checkeder)
{
	PasswordCheckeder = checkeder;
	LabelReliability->setVisible(checkeder);
}
//-----------------------------------------------------------------------------
void ISPasswordEdit::SetVisibleCheckBox(bool Visible)
{
	CheckPasswordVisible->setVisible(Visible);
}
//-----------------------------------------------------------------------------
void ISPasswordEdit::PasswordVisibleChanged(int State)
{
	switch (State)
	{
	case Qt::Checked: SetEchoMode(QLineEdit::Normal); break;
	case Qt::Unchecked: SetEchoMode(QLineEdit::Password); break;
	}

	SetFocus();
}
//-----------------------------------------------------------------------------
void ISPasswordEdit::GeneratePassword()
{
	SetValue(ISPassword::GeneratePassword());
}
//-----------------------------------------------------------------------------
void ISPasswordEdit::PasswordChanged(const QString &Password)
{
	if (PasswordCheckeder)
	{
		int Weight = ISPassword::GetWidthPassword(Password);

		if (Weight < 0) //[-бесконечность; 0]
		{
			LabelReliability->setPixmap(BUFFER_ICONS("PasswordChanger.Null").pixmap(SIZE_20_20));
		}
		else if (Weight >= 0 && Weight <= 25) //[0; 25]
		{
			LabelReliability->setPixmap(BUFFER_ICONS("PasswordChanger.Red").pixmap(SIZE_20_20));
		}
		else if (Weight > 25 && Weight <= 50) //[26; 50]
		{
			LabelReliability->setPixmap(BUFFER_ICONS("PasswordChanger.Yellow").pixmap(SIZE_20_20));
		}
		else if (Weight > 51 && Weight <= 75) //[51; 75]
		{
			LabelReliability->setPixmap(BUFFER_ICONS("PasswordChanger.Blue").pixmap(SIZE_20_20));
		}
		else if (Weight > 75 && Weight <= 100) //[76; 100]
		{
			LabelReliability->setPixmap(BUFFER_ICONS("PasswordChanger.Green").pixmap(SIZE_20_20));
		}
		else if (Weight > 100) //[100; бесконечность]
		{
			LabelReliability->setPixmap(BUFFER_ICONS("PasswordChanger.SuperPassword").pixmap(SIZE_20_20));
		}
	}
}
//-----------------------------------------------------------------------------
