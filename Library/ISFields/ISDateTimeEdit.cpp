#include "StdAfx.h"
#include "ISDateTimeEdit.h"
#include "EXDefines.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISStyleSheet.h"
//-----------------------------------------------------------------------------
ISDateTimeEdit::ISDateTimeEdit(QObject *MetaField, QWidget *parent) : ISFieldEditBase(MetaField, parent)
{
	VisibleCalendar = true;

	CheckEnable = new QCheckBox(this);
	CheckEnable->setCursor(CURSOR_POINTING_HAND);
	CheckEnable->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Expanding);
	CheckEnable->setToolTip(LOCALIZATION("ActivateDeactivateField"));
	connect(CheckEnable, &QCheckBox::stateChanged, this, &ISDateTimeEdit::DateEnableChanged);
	AddWidgetToLeft(CheckEnable);

	DateTimeEdit = new ISQDateTimeEdit(this);
	DateTimeEdit->setMinimumHeight(SIZE_MINIMUM_HEIGHT_EDIT_FIELD);
	DateTimeEdit->setWrapping(true);
	DateTimeEdit->setAccelerated(true);
	DateTimeEdit->setButtonSymbols(QAbstractSpinBox::NoButtons);
	DateTimeEdit->setDisplayFormat(DATE_TIME_FORMAT_V1);
	DateTimeEdit->setSpecialValueText(LOCALIZATION("NotIndicated"));
	DateTimeEdit->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding);
	AddWidgetEdit(DateTimeEdit, this);

	SetVisibleClear(false);

	ButtonCalendar = new QToolButton(this);
	ButtonCalendar->setToolTip(LOCALIZATION("ShowCalendar") + "...");
	ButtonCalendar->setIcon(BUFFER_ICONS("Calendar"));
	ButtonCalendar->setCursor(CURSOR_POINTING_HAND);
	connect(ButtonCalendar, &QToolButton::clicked, this, &ISDateTimeEdit::ShowCalendar);
	AddWidgetToRight(ButtonCalendar);

	CalendarWidget = new ISCalendarPopup(this);
	CalendarWidget->hide();
	connect(CalendarWidget, &ISCalendarWidget::clicked, this, &ISDateTimeEdit::SetValue);
	connect(CalendarWidget, &ISCalendarWidget::Hide, this, &ISDateTimeEdit::HideCalendar);

	AnimationCalendar = new QPropertyAnimation(CalendarWidget, "windowOpacity", this);
	AnimationCalendar->setStartValue(0.0);
	AnimationCalendar->setEndValue(1.0);
	AnimationCalendar->setDuration(500);

	CheckEnable->stateChanged(Qt::Unchecked); //�� ��������� ���� �� �������
	connect(DateTimeEdit, &QDateTimeEdit::dateTimeChanged, this, &ISDateTimeEdit::ValueChanged); //������ ������� ������ ���� ����������� ����� ���������� �������, ����� ���� ����� ����� ������ "��������" ����� ����� ��������
}
//-----------------------------------------------------------------------------
ISDateTimeEdit::ISDateTimeEdit(QWidget *parent) : ISDateTimeEdit(nullptr, parent)
{

}
//-----------------------------------------------------------------------------
ISDateTimeEdit::~ISDateTimeEdit()
{

}
//-----------------------------------------------------------------------------
void ISDateTimeEdit::SetValue(const QVariant &value)
{
	if (value.toDateTime().isValid())
	{
		CheckEnable->setChecked(true);
		DateTimeEdit->setDateTime(value.toDateTime());
		HideCalendar();
	}
	else
	{
		CheckEnable->setChecked(false);
	}
}
//-----------------------------------------------------------------------------
QVariant ISDateTimeEdit::GetValue() const
{
	if (CheckEnable->isChecked())
	{
		return DateTimeEdit->dateTime();
	}

	return QVariant();
}
//-----------------------------------------------------------------------------
void ISDateTimeEdit::Clear()
{
	ISFieldEditBase::Clear();
}
//-----------------------------------------------------------------------------
void ISDateTimeEdit::SetReadOnly(bool ReadOnly)
{
	DateTimeEdit->setReadOnly(ReadOnly);
	CheckEnable->setVisible(!ReadOnly);

	if (VisibleCalendar)
	{
		ButtonCalendar->setVisible(!ReadOnly);
	}
}
//-----------------------------------------------------------------------------
QCheckBox* ISDateTimeEdit::GetCheckEnable()
{
	return CheckEnable;
}
//-----------------------------------------------------------------------------
QToolButton* ISDateTimeEdit::GetButtonCalendar()
{
	return ButtonCalendar;
}
//-----------------------------------------------------------------------------
void ISDateTimeEdit::SetDate(const QDate &Date)
{
	CheckEnable->setChecked(true);
	DateTimeEdit->setDate(Date);
	HideCalendar();
}
//-----------------------------------------------------------------------------
void ISDateTimeEdit::SetTime(const QTime &Time)
{
	CheckEnable->setChecked(true);
	DateTimeEdit->setTime(Time);
}
//-----------------------------------------------------------------------------
void ISDateTimeEdit::DateEnableChanged(int State)
{
	if (State == Qt::Checked)
	{
		DateTimeEdit->setSpecialValueText(QString());
		DateTimeEdit->setDateTime(QDateTime::currentDateTime());
		DateTimeEdit->setEnabled(true);
		DateTimeEdit->setSelectedSection(SelectSection);
		ButtonCalendar->setEnabled(true);
		SetFocus();
	}
	else
	{
		DateTimeEdit->setSpecialValueText(LOCALIZATION("NotIndicated"));
		DateTimeEdit->setDateTime(DateTimeEdit->minimumDateTime());
		DateTimeEdit->setEnabled(false);
		ButtonCalendar->setEnabled(false);
	}
}
//-----------------------------------------------------------------------------
void ISDateTimeEdit::ShowCalendar()
{
	QPoint Point = DateTimeEdit->mapToGlobal(QPoint());
	Point.setY(Point.y() + DateTimeEdit->height());

	CalendarWidget->move(Point);
	CalendarWidget->setSelectedDate(GetValue().toDate());
	CalendarWidget->setWindowOpacity(0.0);
	CalendarWidget->show();
	CalendarWidget->setFocus();
	AnimationCalendar->start();
}
//-----------------------------------------------------------------------------
void ISDateTimeEdit::HideCalendar()
{
	CalendarWidget->hide();
	SetFocus();
}
//-----------------------------------------------------------------------------
void ISDateTimeEdit::SetDisplayFormat(const QString &DisplayFormat)
{
	DateTimeEdit->setDisplayFormat(DisplayFormat);
}
//-----------------------------------------------------------------------------
void ISDateTimeEdit::SetVisibleCalendar(bool visible)
{
	VisibleCalendar = visible;

	ButtonCalendar->setVisible(VisibleCalendar);
	CalendarWidget->setVisible(VisibleCalendar);
}
//-----------------------------------------------------------------------------
void ISDateTimeEdit::SetSelectSection(ISQDateTimeEdit::Section select_section)
{
	SelectSection = select_section;
}
//-----------------------------------------------------------------------------
