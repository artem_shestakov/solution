#pragma once
//-----------------------------------------------------------------------------
#include "isfields_global.h"
#include "ISLineEdit.h"
#include "ISServiceButton.h"
//-----------------------------------------------------------------------------
class ISFIELDS_EXPORT ISFontEdit : public ISLineEdit
{
	Q_OBJECT

public:
	Q_INVOKABLE ISFontEdit(QObject *MetaField, QWidget *parent);
	Q_INVOKABLE ISFontEdit(QWidget *parent);
	virtual ~ISFontEdit();

protected:
	void InstallDefaultFont();
	void SelectFont();

private:
	ISServiceButton *ButtonDefaultFont;
	ISServiceButton *ButtonSelectFont;
};
//-----------------------------------------------------------------------------
