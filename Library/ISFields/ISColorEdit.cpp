#include "StdAfx.h"
#include "ISColorEdit.h"
#include "ISServiceButton.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "EXDefines.h"
//-----------------------------------------------------------------------------
ISColorEdit::ISColorEdit(QObject *MetaField, QWidget *parent) : ISFieldEditBase(MetaField, parent)
{
	QGroupBox *GroupBox = new QGroupBox(this);
	GroupBox->setLayout(new QHBoxLayout());
	GroupBox->layout()->setContentsMargins(LAYOUT_MARGINS_NULL);
	GroupBox->setMinimumHeight(SIZE_MINIMUM_HEIGHT_EDIT_FIELD);
	AddWidgetEdit(GroupBox, this);

	WidgetColor = new QWidget(this);
	WidgetColor->setAutoFillBackground(true);
	GroupBox->layout()->addWidget(WidgetColor);

	SetVisibleClear(false);

	ISServiceButton *ButtonSelectColor = new ISServiceButton(this);
	ButtonSelectColor->setToolTip(LOCALIZATION("SelectColor") + "...");
	ButtonSelectColor->setIcon(BUFFER_ICONS("Color"));
	connect(ButtonSelectColor, &ISServiceButton::clicked, this, &ISColorEdit::SelectColor);
	AddWidgetToRight(ButtonSelectColor);
}
//-----------------------------------------------------------------------------
ISColorEdit::ISColorEdit(QWidget *parent) : ISColorEdit(nullptr, parent)
{

}
//-----------------------------------------------------------------------------
ISColorEdit::~ISColorEdit()
{

}
//-----------------------------------------------------------------------------
void ISColorEdit::SetValue(const QVariant &value)
{
	if (value.toString().length())
	{
		QPalette Palette = WidgetColor->palette();
		Palette.setColor(QPalette::Background, QColor(value.toString()));
		WidgetColor->setPalette(Palette);
	}
}
//-----------------------------------------------------------------------------
QVariant ISColorEdit::GetValue() const
{
	QColor Color = WidgetColor->palette().color(QPalette::Background);
	return Color.name();
}
//-----------------------------------------------------------------------------
void ISColorEdit::Clear()
{

}
//-----------------------------------------------------------------------------
void ISColorEdit::SetReadOnly(bool read_only)
{
	WidgetColor->setEnabled(!read_only);
}
//-----------------------------------------------------------------------------
void ISColorEdit::SelectColor()
{
	QColor Color = QColorDialog::getColor(WidgetColor->palette().color(QPalette::Background), this, LOCALIZATION("SelectedColor"), QColorDialog::DontUseNativeDialog);
	QString ColorName = Color.name();
	if (ColorName != COLOR_STANDART)
	{
		SetValue(ColorName);
		emit ValueChanged();
	}
}
//-----------------------------------------------------------------------------
