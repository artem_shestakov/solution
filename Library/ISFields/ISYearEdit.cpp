#include "StdAfx.h"
#include "ISYearEdit.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISServiceButton.h"
//-----------------------------------------------------------------------------
ISYearEdit::ISYearEdit(QObject *MetaField, QWidget *parent) : ISIntegerEdit(MetaField, parent)
{
	SetMinimum(1900);
	SetMaximum(2099);

	ISServiceButton *ButtonCurrentYear = new ISServiceButton(this);
	ButtonCurrentYear->setToolTip(LOCALIZATION("CurrentYear"));
	ButtonCurrentYear->setIcon(BUFFER_ICONS("Calendar"));
	connect(ButtonCurrentYear, &ISServiceButton::clicked, this, &ISYearEdit::SelectCurrentYear);
	AddWidgetToRight(ButtonCurrentYear);
}
//-----------------------------------------------------------------------------
ISYearEdit::ISYearEdit(QWidget *parent) : ISYearEdit(nullptr, parent)
{

}
//-----------------------------------------------------------------------------
ISYearEdit::~ISYearEdit()
{

}
//-----------------------------------------------------------------------------
void ISYearEdit::SelectCurrentYear()
{
	SetValue(QDate::currentDate().year());
}
//-----------------------------------------------------------------------------
