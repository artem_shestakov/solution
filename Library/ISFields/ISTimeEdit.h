#pragma once
//-----------------------------------------------------------------------------
#include "isfields_global.h"
#include "ISDateTimeEdit.h"
//-----------------------------------------------------------------------------
class ISFIELDS_EXPORT ISTimeEdit : public ISDateTimeEdit
{
	Q_OBJECT

public:
	Q_INVOKABLE ISTimeEdit(QObject *MetaField, QWidget *parent);
	Q_INVOKABLE ISTimeEdit(QWidget *parent);
	virtual ~ISTimeEdit();

	void SetValue(const QVariant &value) override;
	QVariant GetValue() const override;
};
//-----------------------------------------------------------------------------
