#pragma once
//-----------------------------------------------------------------------------
#include "isfields_global.h"
#include "ISFieldEditBase.h"
#include "ISQComboBox.h"
//-----------------------------------------------------------------------------
class ISFIELDS_EXPORT ISComboEdit : public ISFieldEditBase
{
	Q_OBJECT

public:
	Q_INVOKABLE ISComboEdit(QObject *MetaField, QWidget *parent);
	Q_INVOKABLE ISComboEdit(QWidget *parent);
	virtual ~ISComboEdit();

	void SetValue(const QVariant &value) override;
	QVariant GetValue() const override;
	void Clear() override;

	QString GetCurrentText() const;
	void SetEditable(bool Editable);
	void SetCurrentItem(int Index);
	void InsertItem(int Index, const QString &Text, const QVariant &UserData);
	void AddItem(const QString &Text, const QVariant &UserData);
	void SetIcon(int Index, const QIcon &Icon);

public slots:
	void SetReadOnly(bool read_only);

protected:
	void CurrentIndexChanged(int Index);

private:
	ISQComboBox *ComboBox;
};
//-----------------------------------------------------------------------------
