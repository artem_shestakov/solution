#pragma once
//-----------------------------------------------------------------------------
#include "isfields_global.h"
#include "ISDateTimeEdit.h"
//-----------------------------------------------------------------------------
class ISFIELDS_EXPORT ISDateEdit : public ISDateTimeEdit
{
	Q_OBJECT

public:
	Q_INVOKABLE ISDateEdit(QObject *MetaField, QWidget *parent);
	Q_INVOKABLE ISDateEdit(QWidget *parent);
	virtual ~ISDateEdit();

	void SetValue(const QVariant &value) override;
	QVariant GetValue() const override;

protected:
	void BeforeYesterday(); //���������
	void Yesterday(); //�����
	void Today(); //�������
	void Tomorrow(); //������
	void AfterTomorrow(); //�����������
};
//-----------------------------------------------------------------------------
