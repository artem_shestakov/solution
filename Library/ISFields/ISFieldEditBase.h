#pragma once
//-----------------------------------------------------------------------------
#include "isfields_global.h"
#include "ISButtonClear.h"
//-----------------------------------------------------------------------------
//������� ����� ��� ���� ����� �������������� ����������
class ISFIELDS_EXPORT ISFieldEditBase : public QWidget
{
	Q_OBJECT

	Q_PROPERTY(QString FieldName READ GetFieldName)
	Q_PROPERTY(int Size READ GetSize)
	Q_PROPERTY(bool Upper READ GetUpper)
	Q_PROPERTY(bool Lower READ GetLower)
	Q_PROPERTY(QVariant DefaultValueWidget READ GetDefaultValueWidget)
	Q_PROPERTY(bool NotNull READ GetNotNull)
	Q_PROPERTY(bool ReadOnly READ GetReadOnly)
	Q_PROPERTY(bool HideFromObject READ GetHideFromObject)
	Q_PROPERTY(QString PlaceholderText READ GetPlaceholderText)
	Q_PROPERTY(QString RegExp READ GetRegExp)
	
	Q_PROPERTY(bool ModificationFlag READ GetModificationFlag)
	Q_PROPERTY(bool BorderRed READ GetBorderRed WRITE SetBorderRed)

signals:
	void DataChanged(); //������ ��� ��������� ������
	void ValueChange(const QVariant &Value); //������ ��� ��������� ������ ���������� ���������� ��������

public:
	ISFieldEditBase(QObject *MetaField, QWidget *parent);
	ISFieldEditBase(QWidget *parent);
	virtual ~ISFieldEditBase();

	virtual void paintEvent(QPaintEvent *PaintEvent);

	virtual void SetValue(const QVariant &value) = 0; //�������� ��������
	virtual QVariant GetValue() const = 0; //�������� ��������
	virtual void Clear() = 0; //������� ��������

	virtual bool IsValid() const; //�������� ������������ ����
	virtual void SetFont(const QFont &Font);
	void SetVisibleClear(bool visible); //�������� ��������� ������ ������� ��������
	void SetEnabledClear(bool enabled); //�������� ����������� ������ ������� ��������
	void SetCursor(const QCursor &Cursor); //�������� ������
	void SetToolTip(const QString &ToolTip); //�������� ����������� ���������-���������
	void BlinkRed(); //���������� ���� ������� ������

	QObject* GetMetaField();

	QString GetFieldName() const;
	int GetSize() const;
	bool GetUpper() const;
	bool GetLower() const;
	QVariant GetDefaultValueWidget() const;
	bool GetNotNull() const;
	bool GetHideFromObject() const;
	QString GetPlaceholderText() const;
	QString GetRegExp() const;

	void SetReadOnly(bool read_only);
	bool GetReadOnly() const;

	void SetModificationFlag(bool modification_flag); //�������� ���� ��������� ��������
	bool GetModificationFlag() const; //�������� ���� ��������� ��������

	void SetBorderRed(bool border_red);
	bool GetBorderRed() const;

	void CreateHint(const QString &Hint); //������� �������� (���� �������, ��������� ����� ���������)
	void CreateLabelReadOnly(bool read_only); //������� ������ ���� ���� ���������� ��� ��������������
	void SetFocus(); //���������� ����� �� ������ ��������������

protected:
	void CheckInvokeMethod(bool Invoked);
	void ShowHint(); //�������� ���������
	virtual void ValueChanged();
	
	void AddWidgetEdit(QWidget *edit_widget, QWidget *field_edit_pointer); //�������� ������ ���������
	void AddWidgetEditStretch(); //�������� ���������� � ����������� ������� ��������������
	void AddMainLayoutStretch(); //�������� ���������� � ������� ����������� ����
	void AddWidgetToLeft(QWidget *widget); //�������� ������ � ����� �����������
	void AddWidgetToRight(QWidget *widget); //�������� ������ � ������ �����������

private:
	QHBoxLayout *MainLayout;
	QToolButton *ButtonHint;
	QHBoxLayout *LayoutLeft;
	QHBoxLayout *LayoutEdit;
	QHBoxLayout *LayoutRight;
	ISButtonClear *ButtonClear;
	QLabel *LabelReadOnly;
	
	QWidget *EditWidget;
	QWidget *FieldEditPointer;

	QString FieldName;
	int Size;
	bool Upper;
	bool Lower;
	QVariant DefaultValueWidget;
	bool NotNull;
	bool ReadOnly;
	bool HideFromObject;
	QString PlaceholderText;
	QString RegExp;

	QObject *ObjectField;
	bool ModificationFlag;
	bool BorderRed;
};
//-----------------------------------------------------------------------------
