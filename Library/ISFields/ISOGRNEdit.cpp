#include "StdAfx.h"
#include "ISOGRNEdit.h"
#include "ISLocalization.h"
#include "EXDefines.h"
//-----------------------------------------------------------------------------
ISOGRNEdit::ISOGRNEdit(QObject *MetaField, QWidget *parent) : ISLineEdit(MetaField, parent)
{
	SetValidator(new QRegExpValidator(QRegExp(REG_EXP_NUMBER_COUNT_13), this));
	SetPlaceholderText(LOCALIZATION("Field.Ogrn.PlaceholderText"));
}
//-----------------------------------------------------------------------------
ISOGRNEdit::ISOGRNEdit(QWidget *parent) : ISOGRNEdit(nullptr, parent)
{

}
//-----------------------------------------------------------------------------
ISOGRNEdit::~ISOGRNEdit()
{

}
//-----------------------------------------------------------------------------
