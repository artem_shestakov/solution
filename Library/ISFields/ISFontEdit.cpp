#include "StdAfx.h"
#include "ISFontEdit.h"
#include "EXDefines.h"
#include "ISSystem.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
//-----------------------------------------------------------------------------
ISFontEdit::ISFontEdit(QObject *MetaField, QWidget *parent) : ISLineEdit(MetaField, parent)
{
	SetReadOnly(true);
	SetVisibleClear(false);

	ButtonDefaultFont = new ISServiceButton(this);
	ButtonDefaultFont->setToolTip(LOCALIZATION("FontDefault"));
	ButtonDefaultFont->setIcon(BUFFER_ICONS("Font"));
	connect(ButtonDefaultFont, &ISServiceButton::clicked, this, &ISFontEdit::InstallDefaultFont);
	AddWidgetToRight(ButtonDefaultFont);

	ButtonSelectFont = new ISServiceButton(this);
	ButtonSelectFont->setText("...");
	ButtonSelectFont->setToolTip(LOCALIZATION("ClickToSelectFont"));
	connect(ButtonSelectFont, &ISPushButton::clicked, this, &ISFontEdit::SelectFont);
	AddWidgetToRight(ButtonSelectFont);
}
//-----------------------------------------------------------------------------
ISFontEdit::ISFontEdit(QWidget *parent) : ISFontEdit(nullptr, parent)
{

}
//-----------------------------------------------------------------------------
ISFontEdit::~ISFontEdit()
{

}
//-----------------------------------------------------------------------------
void ISFontEdit::InstallDefaultFont()
{
	SetValue(FONT_APPLICATION_STRING);
}
//-----------------------------------------------------------------------------
void ISFontEdit::SelectFont()
{
	QFont Font;

	if (GetValue().toString().length())
	{
		Font = ISSystem::StringToFont(GetValue().toString());
	}
	else
	{
		Font = FONT_APPLICATION_STRING;
	}

	bool Selected = false;
	Font = QFontDialog::getFont(&Selected, Font, this, LOCALIZATION("SelectFont"));
	if (Selected)
	{
		SetValue(Font.toString());
	}
}
//-----------------------------------------------------------------------------
