#include "StdAfx.h"
#include "ISEMailEdit.h"
#include "EXDefines.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
//-----------------------------------------------------------------------------
ISEMailEdit::ISEMailEdit(QObject *MetaField, QWidget *parent) : ISLineEdit(MetaField, parent)
{
	QRegExp RegExp(REG_EXP_EMAIL);
	RegExp.setCaseSensitivity(Qt::CaseInsensitive);
	RegExp.setPatternSyntax(QRegExp::RegExp);

	RegExpValidator = new QRegExpValidator(RegExp, this);
	SetValidator(RegExpValidator);
	SetIcon(BUFFER_ICONS("EMail.FieldEdit.Acceptable"));
	SetPlaceholderText(LOCALIZATION("Field.EMail.PlaceholderText"));
	connect(this, &ISEMailEdit::ValueChange, this, &ISEMailEdit::EMailChanged);
}
//-----------------------------------------------------------------------------
ISEMailEdit::ISEMailEdit(QWidget *parent) : ISEMailEdit(nullptr, parent)
{

}
//-----------------------------------------------------------------------------
ISEMailEdit::~ISEMailEdit()
{

}
//-----------------------------------------------------------------------------
bool ISEMailEdit::IsValid() const
{
	if (GetValue().isValid())
	{
		int Position = 0;
		if (RegExpValidator->validate(GetValue().toString(), Position) != QValidator::Acceptable)
		{
			return false;
		}
	}

	return true;
}
//-----------------------------------------------------------------------------
void ISEMailEdit::EMailChanged(const QVariant &value)
{
	if (value.isValid())
	{
		int Position = 0;
		QValidator::State ValidatorState = RegExpValidator->validate(value.toString(), Position);
		if (ValidatorState == QValidator::Acceptable)
		{
			SetIcon(BUFFER_ICONS("EMail.FieldEdit.Acceptable"));
		}
		else if (ValidatorState == QValidator::Intermediate)
		{
			SetIcon(BUFFER_ICONS("EMail.FieldEdit.Intermediate"));
		}
		else if (ValidatorState == QValidator::Invalid)
		{
			SetIcon(BUFFER_ICONS("EMail.FieldEdit.Invalid"));
		}
	}
	else
	{
		SetIcon(BUFFER_ICONS("EMail.FieldEdit.Acceptable"));
	}
}
//-----------------------------------------------------------------------------
