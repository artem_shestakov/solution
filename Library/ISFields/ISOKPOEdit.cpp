#include "StdAfx.h"
#include "ISOKPOEdit.h"
#include "ISLocalization.h"
#include "EXDefines.h"
//-----------------------------------------------------------------------------
ISOKPOEdit::ISOKPOEdit(QObject *MetaField, QWidget *parent) : ISLineEdit(MetaField, parent)
{
	SetValidator(new QRegExpValidator(QRegExp(REG_EXP_NUMBER_COUNT_8), this));
	SetPlaceholderText(LOCALIZATION("Field.Okpo.PlaceholderText"));
}
//-----------------------------------------------------------------------------
ISOKPOEdit::ISOKPOEdit(QWidget *parent) : ISOKPOEdit(nullptr, parent)
{

}
//-----------------------------------------------------------------------------
ISOKPOEdit::~ISOKPOEdit()
{

}
//-----------------------------------------------------------------------------
