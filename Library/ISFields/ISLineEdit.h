#pragma once
//-----------------------------------------------------------------------------
#include "isfields_global.h"
#include "ISFieldEditBase.h"
#include "ISQLineEdit.h"
//-----------------------------------------------------------------------------
class ISFIELDS_EXPORT ISLineEdit : public ISFieldEditBase
{
	Q_OBJECT

public:
	Q_INVOKABLE ISLineEdit(QObject *MetaField, QWidget *parent);
	Q_INVOKABLE ISLineEdit(QWidget *parent);
	virtual ~ISLineEdit();

	virtual void SetValue(const QVariant &value) override;
	virtual QVariant GetValue() const override;
	virtual void Clear() override;

	void SetValidator(QValidator *Validator);
	void SetEchoMode(QLineEdit::EchoMode EchoMode);
	void SetCompleter(QCompleter *Completer);
	void SetTextAlignment(Qt::Alignment Alignment);
	void SetPlaceholderText(const QString &Text);
	void SetInputMask(const QString &InputMask);
	void SetFocusPolicy(Qt::FocusPolicy focus_policy);
	void SetMaxLength(int Length);
	void SetVisibleClearStandart(bool Enabled);
	void SetIcon(const QIcon &Icon);
	void SelectAll();

	void SetUppercase(bool uppercase);
	void SetLowercase(bool lowercase);
	void ResetFontcase(); //����� �������� ��������

public slots:
	void SetReadOnly(bool read_only);

protected:
	void OnUpperText(const QString &Text);
	void OnLowerText(const QString &Text);
	ISQLineEdit* GetLineEdit();

private:
	ISQLineEdit *LineEdit;
};
//-----------------------------------------------------------------------------
