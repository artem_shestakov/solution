#include "StdAfx.h"
#include "ISLineEdit.h"
#include "EXDefines.h"
#include "ISAssert.h"
#include "ISLocalization.h"
//-----------------------------------------------------------------------------
ISLineEdit::ISLineEdit(QObject *MetaField, QWidget *parent) : ISFieldEditBase(MetaField, parent)
{
	LineEdit = new ISQLineEdit(this);
	LineEdit->setMinimumHeight(SIZE_MINIMUM_HEIGHT_EDIT_FIELD);
	LineEdit->setReadOnly(GetReadOnly());
	LineEdit->setPlaceholderText(GetPlaceholderText());
	connect(LineEdit, &ISQLineEdit::textChanged, this, &ISLineEdit::ValueChanged);
	AddWidgetEdit(LineEdit, this);

	if (GetSize())
	{
		SetMaxLength(GetSize());
	}

	if (GetUpper())
	{
		SetUppercase(true);
	}

	if (GetLower())
	{
		SetLowercase(true);
	}

	if (GetRegExp().length())
	{
		QRegExp RegExp(GetRegExp());
		SetValidator(new QRegExpValidator(RegExp, this));
	}
}
//-----------------------------------------------------------------------------
ISLineEdit::ISLineEdit(QWidget *parent) : ISLineEdit(nullptr, parent)
{

}
//-----------------------------------------------------------------------------
ISLineEdit::~ISLineEdit()
{

}
//-----------------------------------------------------------------------------
void ISLineEdit::SetValue(const QVariant &value)
{
	LineEdit->setText(value.toString());
}
//-----------------------------------------------------------------------------
QVariant ISLineEdit::GetValue() const
{
	if (LineEdit->text().length())
	{
		return LineEdit->text();
	}

	return QVariant();
}
//-----------------------------------------------------------------------------
void ISLineEdit::Clear()
{
	LineEdit->clear();
}
//-----------------------------------------------------------------------------
void ISLineEdit::SetValidator(QValidator *Validator)
{
	LineEdit->setValidator(Validator);
}
//-----------------------------------------------------------------------------
void ISLineEdit::SetEchoMode(QLineEdit::EchoMode EchoMode)
{
	LineEdit->setEchoMode(EchoMode);
}
//-----------------------------------------------------------------------------
void ISLineEdit::SetCompleter(QCompleter *Completer)
{
	LineEdit->setCompleter(Completer);
}
//-----------------------------------------------------------------------------
void ISLineEdit::SetTextAlignment(Qt::Alignment Alignment)
{
	LineEdit->setAlignment(Alignment);
}
//-----------------------------------------------------------------------------
void ISLineEdit::SetUppercase(bool uppercase)
{
	if (uppercase)
	{
		connect(LineEdit, &ISQLineEdit::textChanged, this, &ISLineEdit::OnUpperText);
	}
	else
	{
		disconnect(LineEdit, &ISQLineEdit::textChanged, this, &ISLineEdit::OnUpperText);
	}
}
//-----------------------------------------------------------------------------
void ISLineEdit::SetLowercase(bool lowercase)
{
	if (lowercase)
	{
		connect(LineEdit, &ISQLineEdit::textChanged, this, &ISLineEdit::OnLowerText);
	}
	else
	{
		disconnect(LineEdit, &ISQLineEdit::textChanged, this, &ISLineEdit::OnLowerText);
	}
}
//-----------------------------------------------------------------------------
void ISLineEdit::ResetFontcase()
{
	disconnect(LineEdit, &ISQLineEdit::textChanged, this, &ISLineEdit::OnUpperText);
	disconnect(LineEdit, &ISQLineEdit::textChanged, this, &ISLineEdit::OnLowerText);
}
//-----------------------------------------------------------------------------
void ISLineEdit::SetReadOnly(bool read_only)
{
	LineEdit->setReadOnly(read_only);
	SetVisibleClear(!read_only);
}
//-----------------------------------------------------------------------------
void ISLineEdit::OnUpperText(const QString &Text)
{
	int Position = LineEdit->cursorPosition();
	LineEdit->setText(Text.toUpper());
	LineEdit->setCursorPosition(Position);
}
//-----------------------------------------------------------------------------
void ISLineEdit::OnLowerText(const QString &Text)
{
	int Position = LineEdit->cursorPosition();
	LineEdit->setText(Text.toLower());
	LineEdit->setCursorPosition(Position);
}
//-----------------------------------------------------------------------------
void ISLineEdit::SetPlaceholderText(const QString &Text)
{
	LineEdit->setPlaceholderText(Text);
}
//-----------------------------------------------------------------------------
void ISLineEdit::SetInputMask(const QString &InputMask)
{
	LineEdit->setInputMask(InputMask);
}
//-----------------------------------------------------------------------------
void ISLineEdit::SetFocusPolicy(Qt::FocusPolicy focus_policy)
{
	LineEdit->setFocusPolicy(focus_policy);
}
//-----------------------------------------------------------------------------
void ISLineEdit::SetMaxLength(int Length)
{
	LineEdit->setMaxLength(Length);
}
//-----------------------------------------------------------------------------
void ISLineEdit::SetVisibleClearStandart(bool Enabled)
{
	LineEdit->setClearButtonEnabled(Enabled);
	
	if (Enabled)
	{
		QAction *ActionClear = findChild<QAction*>("_q_qlineeditclearaction");
		ActionClear->setToolTip(LOCALIZATION("Clear.Field"));
	}
}
//-----------------------------------------------------------------------------
void ISLineEdit::SetIcon(const QIcon &Icon)
{
	LineEdit->SetIcon(Icon);
}
//-----------------------------------------------------------------------------
void ISLineEdit::SelectAll()
{
	QTimer::singleShot(10, GetLineEdit(), &ISQLineEdit::selectAll);
}
//-----------------------------------------------------------------------------
ISQLineEdit* ISLineEdit::GetLineEdit()
{
	return LineEdit;
}
//-----------------------------------------------------------------------------
