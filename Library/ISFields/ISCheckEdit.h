#pragma once
//-----------------------------------------------------------------------------
#include "isfields_global.h"
#include "ISFieldEditBase.h"
//-----------------------------------------------------------------------------
class ISFIELDS_EXPORT ISCheckEdit : public ISFieldEditBase
{
	Q_OBJECT

public:
	Q_INVOKABLE ISCheckEdit(QObject *MetaField, QWidget *parent);
	Q_INVOKABLE ISCheckEdit(QWidget *parent);
	virtual ~ISCheckEdit();

	void SetValue(const QVariant &value) override;
	QVariant GetValue() const override;
	void Clear() override;

	void SetText(const QString &Text);
	
public slots:
	void SetReadOnly(bool ReadOnly);

private:
	QCheckBox *CheckBox;
};
//-----------------------------------------------------------------------------
