#include "StdAfx.h"
#include "ISCheckEdit.h"
#include "EXDefines.h"
//-----------------------------------------------------------------------------
ISCheckEdit::ISCheckEdit(QObject *MetaField, QWidget *parent) : ISFieldEditBase(MetaField, parent)
{
	CheckBox = new QCheckBox(this);
	CheckBox->setEnabled(!GetReadOnly());
	CheckBox->setChecked(false);
	CheckBox->setTristate(false);
	CheckBox->setCursor(CURSOR_POINTING_HAND);
	connect(CheckBox, &QCheckBox::stateChanged, this, &ISCheckEdit::ValueChanged);
	AddWidgetEdit(CheckBox, this);
	AddWidgetEditStretch();

	SetVisibleClear(false);
}
//-----------------------------------------------------------------------------
ISCheckEdit::ISCheckEdit(QWidget *parent) : ISCheckEdit(nullptr, parent)
{

}
//-----------------------------------------------------------------------------
ISCheckEdit::~ISCheckEdit()
{

}
//-----------------------------------------------------------------------------
void ISCheckEdit::SetValue(const QVariant &value)
{
	CheckBox->setChecked(value.toBool());
}
//-----------------------------------------------------------------------------
QVariant ISCheckEdit::GetValue() const
{
	return CheckBox->isChecked();
}
//-----------------------------------------------------------------------------
void ISCheckEdit::Clear()
{
	ISFieldEditBase::Clear();
}
//-----------------------------------------------------------------------------
void ISCheckEdit::SetText(const QString &Text)
{
	CheckBox->setText(Text);
}
//-----------------------------------------------------------------------------
void ISCheckEdit::SetReadOnly(bool ReadOnly)
{
	CheckBox->setEnabled(!ReadOnly);
}
//-----------------------------------------------------------------------------
