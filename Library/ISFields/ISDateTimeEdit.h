#pragma once
//-----------------------------------------------------------------------------
#include "isfields_global.h"
#include "ISFieldEditBase.h"
#include "ISCalendarPopup.h"
#include "ISQDateTimeEdit.h"
//-----------------------------------------------------------------------------
class ISFIELDS_EXPORT ISDateTimeEdit : public ISFieldEditBase
{
	Q_OBJECT

public:
	Q_INVOKABLE ISDateTimeEdit(QObject *MetaField, QWidget *parent);
	Q_INVOKABLE ISDateTimeEdit(QWidget *parent);
	virtual ~ISDateTimeEdit();

	virtual void SetValue(const QVariant &value) override;
	virtual QVariant GetValue() const override;
	virtual void Clear() override;

public slots:
	void SetReadOnly(bool ReadOnly);

protected:
	QCheckBox* GetCheckEnable();
	QToolButton* GetButtonCalendar();

	void SetDate(const QDate &Date);
	void SetTime(const QTime &Time);

	void DateEnableChanged(int State);
	void ShowCalendar();
	void HideCalendar();

	void SetDisplayFormat(const QString &DisplayFormat);
	void SetVisibleCalendar(bool visible);
	void SetSelectSection(ISQDateTimeEdit::Section select_section);

private:
	QCheckBox *CheckEnable;
	ISQDateTimeEdit *DateTimeEdit;
	
	QToolButton *ButtonCalendar;
	ISCalendarPopup *CalendarWidget;
	QPropertyAnimation *AnimationCalendar;

	ISQDateTimeEdit::Section SelectSection;
	bool VisibleCalendar;
};
//-----------------------------------------------------------------------------
