#include "StdAfx.h"
#include "ISPhoneEdit.h"
#include "EXDefines.h"
#include "ISBuffer.h"
#include "ISLocalization.h"
#include "ISTelephonyInterface.h"
#include "ISSystem.h"
#include "ISMessageBox.h"
#include "ISValidatorPhone.h"
//-----------------------------------------------------------------------------
ISPhoneEdit::ISPhoneEdit(QObject *MetaField, QWidget *parent) : ISLineEdit(MetaField, parent)
{
	new ISValidatorPhone(GetLineEdit());
	SetPlaceholderText(LOCALIZATION("PhoneMask"));
	SetIcon(BUFFER_ICONS("Phone.Correcting.Standart").pixmap(SIZE_16_16));
	connect(this, &ISPhoneEdit::DataChanged, this, &ISPhoneEdit::PhoneChanged);
	connect(GetLineEdit(), &ISQLineEdit::FocusInSignal, this, &ISPhoneEdit::SelectAll);
	
	ButtonCall = new ISServiceButton(this);
	ButtonCall->setToolTip(LOCALIZATION("Call"));
	ButtonCall->setIcon(BUFFER_ICONS("CallPhone"));
	ButtonCall->setEnabled(false);
	connect(ButtonCall, &ISServiceButton::clicked, this, &ISPhoneEdit::Call);
	AddWidgetToRight(ButtonCall);

	ISServiceButton *b = new ISServiceButton(this);
	b->setText("?");
	connect(b, &ISServiceButton::clicked, [=]
	{
		QWidget *w = new QWidget(nullptr);
		w->setLayout(new QVBoxLayout());
		w->layout()->addWidget(new QLineEdit(original, w));
		w->show();
	});
	AddWidgetToRight(b);
}
//-----------------------------------------------------------------------------
ISPhoneEdit::ISPhoneEdit(QWidget *parent) : ISPhoneEdit(nullptr, parent)
{

}
//-----------------------------------------------------------------------------
ISPhoneEdit::~ISPhoneEdit()
{

}
//-----------------------------------------------------------------------------
void ISPhoneEdit::SetValue(const QVariant &value)
{
	original = value.toString();
	ISLineEdit::SetValue(value);
}
//-----------------------------------------------------------------------------
QVariant ISPhoneEdit::GetValue() const
{
	QString PhoneNumber = ISLineEdit::GetValue().toString();
	if (PhoneNumber.length() == 18)
	{
		return PhoneNumber;
	}

	return QVariant();
}
//-----------------------------------------------------------------------------
bool ISPhoneEdit::IsValid() const
{
	bool Result = true;

	QString Phone = ISLineEdit::GetValue().toString();
	if (Phone.length())
	{
		if (Phone.length() != 13)
		{
			Result = false;
		}
	}

	return Result;
}
//-----------------------------------------------------------------------------
void ISPhoneEdit::Call()
{
	if (ISTelephonyInterface::GetInstance().GetPointer())
	{
		ISSystem::SetWaitGlobalCursor(true);
		ISTelephonyInterface::GetInstance().GetPointer()->CallPhone(GetValue().toString());
		ISSystem::SetWaitGlobalCursor(false);
		
		emit Called();
	}
	else
	{
		ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.NotSettingTelephony"));
	}
}
//-----------------------------------------------------------------------------
void ISPhoneEdit::PhoneChanged()
{
	disconnect(this, &ISPhoneEdit::ValueChange, this, &ISPhoneEdit::PhoneChanged);

	QString Phone = GetLineEdit()->text(); //����� ������ � ���������
	QIcon Icon;

	if (Phone.length() == 13) //���� ����� �������� ����������
	{
		QToolTip::hideText();
		Icon = BUFFER_ICONS("Phone.Correcting.True").pixmap(SIZE_16_16);
		ButtonCall->setEnabled(true);
	}
	else //����� �������� ������������
	{
		ButtonCall->setEnabled(false);

		if (Phone.length() > 0 && Phone.length() < 13) //����� �������� ������ ��������
		{
			QPoint Point = GetLineEdit()->mapToGlobal(QPoint());
			Point.setX(Point.x() - 2);
			Point.setY(Point.y() + 8);
			QToolTip::showText(Point, LOCALIZATION("PhoneNumberInputIncorrect"), GetLineEdit(), QRect(), 2000);

			Icon = BUFFER_ICONS("Phone.Correcting.False").pixmap(SIZE_16_16);
		}
		else if (Phone.length() == 0) //����� �������� �� ������ ������
		{
			QToolTip::hideText();
			Icon = BUFFER_ICONS("Phone.Correcting.Standart").pixmap(SIZE_16_16);
		}
	}

	SetIcon(Icon);
}
//-----------------------------------------------------------------------------
