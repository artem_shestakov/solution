#include "StdAfx.h"
#include "ISIntegerEdit.h"
#include "EXDefines.h"
#include "ISStyleSheet.h"
//-----------------------------------------------------------------------------
ISIntegerEdit::ISIntegerEdit(QObject *MetaField, QWidget *parent) : ISFieldEditBase(MetaField, parent)
{
	SpinBox = new ISQSpinBox(this);
	SpinBox->setStyleSheet(STYLE_SHEET("ISIntegerEdit"));
	SpinBox->setMinimumHeight(SIZE_MINIMUM_HEIGHT_EDIT_FIELD);
	SpinBox->setSpecialValueText(SYMBOL_SPACE);
	SpinBox->setAccelerated(true);
	SpinBox->setReadOnly(GetReadOnly());
	connect(SpinBox, static_cast<void(ISQSpinBox::*)(int)>(&ISQSpinBox::valueChanged), this, &ISIntegerEdit::ValueChanged);
	connect(SpinBox, static_cast<void(ISQSpinBox::*)(const QString &)>(&ISQSpinBox::valueChanged), this, &ISIntegerEdit::OnValueChanged);
	connect(SpinBox, static_cast<void(ISQSpinBox::*)(const QString &)>(&ISQSpinBox::TextChanged), this, &ISIntegerEdit::ValueChanged);
	connect(SpinBox, static_cast<void(ISQSpinBox::*)(const QString &)>(&ISQSpinBox::TextChanged), this, &ISIntegerEdit::OnValueChanged);
	AddWidgetEdit(SpinBox, this);
}
//-----------------------------------------------------------------------------
ISIntegerEdit::ISIntegerEdit(QWidget *parent) : ISIntegerEdit(nullptr, parent)
{

}
//-----------------------------------------------------------------------------
ISIntegerEdit::~ISIntegerEdit()
{

}
//-----------------------------------------------------------------------------
void ISIntegerEdit::SetValue(const QVariant &value)
{
	SpinBox->setValue(value.toInt());
	OnValueChanged(value.toString());
}
//-----------------------------------------------------------------------------
QVariant ISIntegerEdit::GetValue() const
{
	if (IntValue.length())
	{
		return SpinBox->value();
	}

	return QVariant();
}
//-----------------------------------------------------------------------------
void ISIntegerEdit::Clear()
{
	SpinBox->clear();
	SpinBox->setSpecialValueText(SYMBOL_SPACE);
	SpinBox->clearFocus();
	SpinBox->setFocus();
}
//-----------------------------------------------------------------------------
void ISIntegerEdit::SetMinimum(int Minimum)
{
	SpinBox->setMinimum(Minimum);
}
//-----------------------------------------------------------------------------
void ISIntegerEdit::SetMaximum(int Maximum)
{
	SpinBox->setMaximum(Maximum);
}
//-----------------------------------------------------------------------------
void ISIntegerEdit::SetReadOnly(bool ReadOnly)
{
	SpinBox->setReadOnly(ReadOnly);
}
//-----------------------------------------------------------------------------
void ISIntegerEdit::OnValueChanged(const QString &Text)
{
	if (Text.length()) //���� �������� ������� (�����)
	{
		if (Text == "0") //���� ����� ����, ������ ����. �������� ����� ���� �����������
		{
			SpinBox->setSpecialValueText(QString());
		}
	}
	else //�������� ����������� (������� ��� �������)
	{
		Clear();
	}

	IntValue = Text;
}
//-----------------------------------------------------------------------------
