#include "StdAfx.h"
#include "ISPathEditFile.h"
#include "ISPushButton.h"
#include "ISLocalization.h"
#include "ISFileDialog.h"
#include "ISSystem.h"
//-----------------------------------------------------------------------------
ISPathEditFile::ISPathEditFile(QObject *MetaField, QWidget *parent) : ISLineEdit(MetaField, parent)
{
	SetReadOnly(true);
	SetFocusPolicy(Qt::NoFocus);

	ISPushButton *ButtonSelect = new ISPushButton(this);
	ButtonSelect->setText(LOCALIZATION("Overview") + "...");
	ButtonSelect->setToolTip(LOCALIZATION("SelectFile") + "...");
	connect(ButtonSelect, &ISPushButton::clicked, this, &ISPathEditFile::SelectFile);
	AddWidgetToRight(ButtonSelect);
}
//-----------------------------------------------------------------------------
ISPathEditFile::ISPathEditFile(QWidget *parent) : ISPathEditFile(nullptr, parent)
{

}
//-----------------------------------------------------------------------------
ISPathEditFile::~ISPathEditFile()
{

}
//-----------------------------------------------------------------------------
void ISPathEditFile::SetValue(const QVariant &value)
{
	ISLineEdit::SetValue(value);
	SetToolTip(value.toString());
}
//-----------------------------------------------------------------------------
void ISPathEditFile::SetFilterFile(const QString &filter_file)
{
	FilterFile = filter_file;
}
//-----------------------------------------------------------------------------
void ISPathEditFile::SelectFile()
{
	ISSystem::SetWaitGlobalCursor(true);
	
	QString Path = ISFileDialog::GetOpenFileName(this, GetValue().toString(), FilterFile);
	if (Path.length())
	{
		SetValue(Path);
	}

	ISSystem::SetWaitGlobalCursor(false);
}
//-----------------------------------------------------------------------------
