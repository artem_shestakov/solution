#include "StdAfx.h"
#include "ISDateEdit.h"
#include "EXDefines.h"
#include "ISLocalization.h"
#include "ISSystem.h"
//-----------------------------------------------------------------------------
ISDateEdit::ISDateEdit(QObject *MetaField, QWidget *parent) : ISDateTimeEdit(MetaField, parent)
{
	SetDisplayFormat(DATE_FORMAT_STRING_V1);
	SetSelectSection(ISQDateTimeEdit::DaySection);
	
	GetButtonCalendar()->setPopupMode(QToolButton::MenuButtonPopup);

	QMenu *Menu = new QMenu(GetButtonCalendar());
	GetButtonCalendar()->setMenu(Menu);

	QAction *ActionBeforeYesterday = new QAction(Menu);
	ActionBeforeYesterday->setText(LOCALIZATION("BeforeYesterday"));
	ActionBeforeYesterday->setToolTip(LOCALIZATION("BeforeYesterday"));
	connect(ActionBeforeYesterday, &QAction::triggered, this, &ISDateEdit::BeforeYesterday);
	Menu->addAction(ActionBeforeYesterday);

	QAction *ActionYesterday = new QAction(Menu);
	ActionYesterday->setText(LOCALIZATION("Yesterday"));
	ActionYesterday->setToolTip(LOCALIZATION("Yesterday"));
	connect(ActionYesterday, &QAction::triggered, this, &ISDateEdit::Yesterday);
	Menu->addAction(ActionYesterday);

	QAction *ActionToday = new QAction(Menu);
	ActionToday->setText(LOCALIZATION("Today"));
	ActionToday->setToolTip(LOCALIZATION("Today"));
	ActionToday->setFont(FONT_APPLICATION_BOLD);
	connect(ActionToday, &QAction::triggered, this, &ISDateEdit::Today);
	Menu->addAction(ActionToday);
	
	QAction *ActionTomorrow = new QAction(Menu);
	ActionTomorrow->setText(LOCALIZATION("Tomorrow"));
	ActionTomorrow->setToolTip(LOCALIZATION("Tomorrow"));
	connect(ActionTomorrow, &QAction::triggered, this, &ISDateEdit::Tomorrow);
	Menu->addAction(ActionTomorrow);

	QAction *ActionAfterTomorrow = new QAction(Menu);
	ActionAfterTomorrow->setText(LOCALIZATION("AfterTomorrow"));
	ActionAfterTomorrow->setToolTip(LOCALIZATION("AfterTomorrow"));
	connect(ActionAfterTomorrow, &QAction::triggered, this, &ISDateEdit::AfterTomorrow);
	Menu->addAction(ActionAfterTomorrow);
}
//-----------------------------------------------------------------------------
ISDateEdit::ISDateEdit(QWidget *parent) : ISDateEdit(nullptr, parent)
{

}
//-----------------------------------------------------------------------------
ISDateEdit::~ISDateEdit()
{

}
//-----------------------------------------------------------------------------
void ISDateEdit::SetValue(const QVariant &value)
{
	if (value.isValid())
	{
		ISDateTimeEdit::SetDate(value.toDate());
	}
	else
	{
		GetCheckEnable()->setChecked(false);
	}
}
//-----------------------------------------------------------------------------
QVariant ISDateEdit::GetValue() const
{
	QVariant DateValue = ISDateTimeEdit::GetValue();
	if (DateValue.isValid())
	{
		return DateValue.toDate();
	}

	return QVariant();
}
//-----------------------------------------------------------------------------
void ISDateEdit::BeforeYesterday()
{
	SetValue(QDate::currentDate().addDays(-2));
}
//-----------------------------------------------------------------------------
void ISDateEdit::Yesterday()
{
	SetValue(QDate::currentDate().addDays(-1));
}
//-----------------------------------------------------------------------------
void ISDateEdit::Today()
{
	SetValue(QDate::currentDate());
}
//-----------------------------------------------------------------------------
void ISDateEdit::Tomorrow()
{
	SetValue(QDate::currentDate().addDays(1));
}
//-----------------------------------------------------------------------------
void ISDateEdit::AfterTomorrow()
{
	SetValue(QDate::currentDate().addDays(2));
}
//-----------------------------------------------------------------------------
