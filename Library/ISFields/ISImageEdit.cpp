#include "StdAfx.h"
#include "ISImageEdit.h"
#include "EXDefines.h"
#include "ISSystem.h"
#include "ISBuffer.h"
#include "ISLocalization.h"
//-----------------------------------------------------------------------------
ISImageEdit::ISImageEdit(QObject *MetaField, QWidget *parent) : ISFieldEditBase(MetaField, parent)
{
	ImageWidget = new ISImageWidget(this);
	ImageWidget->setFixedSize(SIZE_200_200);
	ImageWidget->setCursor(CURSOR_POINTING_HAND);
	ImageWidget->setToolTip(LOCALIZATION("ClickRightButtonMouseForCallContextMenu"));
	connect(ImageWidget, &ISImageWidget::ImageChanged, this, &ISImageEdit::ValueChanged);
	AddWidgetEdit(ImageWidget, this);

	AddMainLayoutStretch();
}
//-----------------------------------------------------------------------------
ISImageEdit::ISImageEdit(QWidget *parent) : ISImageEdit(nullptr, parent)
{

}
//-----------------------------------------------------------------------------
ISImageEdit::~ISImageEdit()
{

}
//-----------------------------------------------------------------------------
void ISImageEdit::SetValue(const QVariant &value)
{
	if (value.isValid())
	{
		QPixmap Pixmap = ISSystem::ByteArrayToPixmap(value.toByteArray());
		ImageWidget->SetPixmap(Pixmap);
	}
	else
	{
		ImageWidget->Clear();
	}
}
//-----------------------------------------------------------------------------
QVariant ISImageEdit::GetValue() const
{
	QByteArray ByteArray = ISSystem::PixmapToByteArray(ImageWidget->GetPixmap());
	if (!ByteArray.isNull())
	{
		return ByteArray;
	}
	
	return QVariant();
}
//-----------------------------------------------------------------------------
void ISImageEdit::Clear()
{
	ImageWidget->Clear();
}
//-----------------------------------------------------------------------------
void ISImageEdit::SetReadOnly(bool read_only)
{
	ImageWidget->setEnabled(!read_only);
}
//-----------------------------------------------------------------------------
