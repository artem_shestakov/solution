#pragma once
//-----------------------------------------------------------------------------
#include "isfields_global.h"
#include "ISFieldEditBase.h"
#include "ISQTextEdit.h"
//-----------------------------------------------------------------------------
class ISFIELDS_EXPORT ISTextEdit : public ISFieldEditBase
{
	Q_OBJECT

signals:
	void KeyPressEnter();

public:
	Q_INVOKABLE ISTextEdit(QObject *MetaField, QWidget *parent = 0);
	Q_INVOKABLE ISTextEdit(QWidget *parent = 0);
	virtual ~ISTextEdit();

	void SetValue(const QVariant &value) override;
	QVariant GetValue() const override;
	void Clear() override;

	void AddText(const QString &Text);
	void SetPlaceholderText(const QString &PlaceholderText);
	void SetExecuteEnter(bool Execute);
	
	void SetUppercase(bool uppercase);
	void SetLowercase(bool lowercase);
	void ResetFontcase(); //����� �������� ��������

	ISQTextEdit* GetTextEdit();

public slots:
	void SetReadOnly(bool read_only);

protected:
	void OnUpperText();
	void OnLowerText();

private:
	ISQTextEdit *TextEdit;
};
//-----------------------------------------------------------------------------
