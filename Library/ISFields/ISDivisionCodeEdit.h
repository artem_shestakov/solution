#pragma once
//-----------------------------------------------------------------------------
#include "isfields_global.h"
#include "ISLineEdit.h"
//-----------------------------------------------------------------------------
class ISFIELDS_EXPORT ISDivisionCodeEdit : public ISLineEdit
{
	Q_OBJECT

public:
	Q_INVOKABLE ISDivisionCodeEdit(QObject *MetaField, QWidget *parent);
	Q_INVOKABLE ISDivisionCodeEdit(QWidget *parent);
	virtual ~ISDivisionCodeEdit();

	bool IsValid() const;
};
//-----------------------------------------------------------------------------
