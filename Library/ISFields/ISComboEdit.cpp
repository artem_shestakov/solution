#include "StdAfx.h"
#include "ISComboEdit.h"
#include "EXDefines.h"
#include "ISQLineEdit.h"
#include "ISPopupDelegate.h"
//-----------------------------------------------------------------------------
ISComboEdit::ISComboEdit(QObject *MetaField, QWidget *parent) : ISFieldEditBase(MetaField, parent)
{
	ComboBox = new ISQComboBox(this);
	ComboBox->setLineEdit(new ISQLineEdit(ComboBox));
	ComboBox->setItemDelegate(new ISPopupDelegate(ComboBox));
	ComboBox->setFixedHeight(SIZE_MINIMUM_HEIGHT_EDIT_FIELD);
	ComboBox->setIconSize(SIZE_22_22);
	connect(ComboBox, static_cast<void(ISQComboBox::*)(int Index)>(&ISQComboBox::currentIndexChanged), this, &ISComboEdit::ValueChanged);
	AddWidgetEdit(ComboBox, this);
}
//-----------------------------------------------------------------------------
ISComboEdit::ISComboEdit(QWidget *parent) : ISComboEdit(nullptr, parent)
{

}
//-----------------------------------------------------------------------------
ISComboEdit::~ISComboEdit()
{

}
//-----------------------------------------------------------------------------
void ISComboEdit::SetValue(const QVariant &value)
{
	for (int i = 0; i < ComboBox->count(); i++)
	{
		if (value == ComboBox->itemData(i))
		{
			ComboBox->setCurrentIndex(i);
			break;
		}
	}
}
//-----------------------------------------------------------------------------
QVariant ISComboEdit::GetValue() const
{
	QVariant UserData = ComboBox->itemData(ComboBox->currentIndex());
	if (UserData.isNull())
	{
		return QVariant();
	}

	return UserData;
}
//-----------------------------------------------------------------------------
void ISComboEdit::Clear()
{

}
//-----------------------------------------------------------------------------
QString ISComboEdit::GetCurrentText() const
{
	return ComboBox->currentText();
}
//-----------------------------------------------------------------------------
void ISComboEdit::SetEditable(bool Editable)
{
	ComboBox->setEditable(Editable);
}
//-----------------------------------------------------------------------------
void ISComboEdit::SetCurrentItem(int Index)
{
	ComboBox->setCurrentIndex(Index);
}
//-----------------------------------------------------------------------------
void ISComboEdit::InsertItem(int Index, const QString &Text, const QVariant &UserData)
{
	ComboBox->insertItem(Index, Text, UserData);
}
//-----------------------------------------------------------------------------
void ISComboEdit::AddItem(const QString &Text, const QVariant &UserData)
{
	ComboBox->addItem(Text, UserData);
}
//-----------------------------------------------------------------------------
void ISComboEdit::SetIcon(int Index, const QIcon &Icon)
{
	ComboBox->setItemIcon(Index, Icon);
}
//-----------------------------------------------------------------------------
void ISComboEdit::SetReadOnly(bool read_only)
{
	ComboBox->setEnabled(!read_only);
}
//-----------------------------------------------------------------------------
void ISComboEdit::CurrentIndexChanged(int Index)
{
	emit ValueChanged();
}
//-----------------------------------------------------------------------------
