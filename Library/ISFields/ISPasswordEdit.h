#pragma once
//-----------------------------------------------------------------------------
#include "isfields_global.h"
#include "ISLineEdit.h"
#include "ISServiceButton.h"
//-----------------------------------------------------------------------------
class ISFIELDS_EXPORT ISPasswordEdit : public ISLineEdit
{
	Q_OBJECT

public:
	Q_INVOKABLE ISPasswordEdit(QObject *MetaField, QWidget *parent);
	Q_INVOKABLE ISPasswordEdit(QWidget *parent);
	virtual ~ISPasswordEdit();

	void SetVisibleGenerate(bool Visible); //����������/������������ ������ ��������� ������
	void SetPasswordCheckeder(bool checkeder); //����������/���������� �������� ��������� ������
	void SetVisibleCheckBox(bool Visible);

protected:
	void PasswordVisibleChanged(int State);
	void GeneratePassword(); //��������� ������
	void PasswordChanged(const QString &Password);

private:
	QCheckBox *CheckPasswordVisible;
	QLabel *LabelReliability;
	ISServiceButton *ButtonGeneratePassword;

	bool PasswordCheckeder;
};
//-----------------------------------------------------------------------------
