#include "StdAfx.h"
#include "ISCaratCoreBase.h"
#include "EXDefines.h"
#include "ISDatabase.h"
#include "ISDebug.h"
//-----------------------------------------------------------------------------
ISCaratCoreBase::ISCaratCoreBase(const QString &core_name, QObject *parent) : QObject(parent)
{
	setObjectName(core_name);

	DB = QSqlDatabase::cloneDatabase(ISDatabase::GetInstance().GetDefaultDB(), core_name + "DB");

	if (!DB.open(SYSTEM_USER_LOGIN, SYSTEM_USER_PASSWORD))
	{
		ISDebug::ShowCriticalString(DB.lastError().text());
	}

	Timer = new QTimer(this);
}
//-----------------------------------------------------------------------------
ISCaratCoreBase::~ISCaratCoreBase()
{
	
}
//-----------------------------------------------------------------------------
void ISCaratCoreBase::Shutdown()
{
	DB.close();
}
//-----------------------------------------------------------------------------
QSqlDatabase& ISCaratCoreBase::GetDB()
{
	return DB;
}
//-----------------------------------------------------------------------------
QTimer* ISCaratCoreBase::GetTimer()
{
	return Timer;
}
//-----------------------------------------------------------------------------
void ISCaratCoreBase::StartTimer(int Interval)
{
	Timer->setInterval(Interval);
	Timer->start();
}
//-----------------------------------------------------------------------------
