#pragma once
//-----------------------------------------------------------------------------
#include "iscarat_global.h"
//-----------------------------------------------------------------------------
class ISCARAT_EXPORT ISCaratCoreBase : public QObject
{
	Q_OBJECT

public:
	ISCaratCoreBase(const QString &core_name, QObject *parent);
	virtual ~ISCaratCoreBase();

	virtual void Shutdown(); //���������� ������ ����

protected:
	QSqlDatabase& GetDB(); //�������� ������ �� ������ ���� ������ ����
	QTimer* GetTimer(); //�������� ��������� �� ������

	void StartTimer(int Interval);

private:
	QSqlDatabase DB;
	QTimer *Timer;
};
//-----------------------------------------------------------------------------
