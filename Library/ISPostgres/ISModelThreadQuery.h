#pragma once
//-----------------------------------------------------------------------------
#include "ispostgres_global.h"
#include "ISQueryModel.h"
#include "ISModelThreadWorker.h"
//-----------------------------------------------------------------------------
class ISPOSTGRES_EXPORT ISModelThreadQuery : public QThread
{
	Q_OBJECT

signals:
	void Started();
	void Finished();
	void ExecuteQuery(const QString &SqlQuery, const QVariantMap &Conditions);
	void Results(const QList<QSqlRecord> &Records); //����� ����������� �� ������
	void ExecutedQuery(); //������ � ���������� �������
	void ErrorConnection(const QSqlError &SqlError); //������ �� ������ ���������� � ��
	void ErrorQuery(const QSqlError &SqlError, const QString &QueryText); //������ �� ������ � �������

public:
	ISModelThreadQuery(QObject *parent = 0);
	virtual ~ISModelThreadQuery();

	void Execute(const QString &SqlQuery, const QVariantMap &Conditions);

protected:
	void run();

private:
	ISModelThreadWorker *ModelWorker;
};
//-----------------------------------------------------------------------------
