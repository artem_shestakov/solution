#include "StdAfx.h"
#include "ISSqlModelCore.h"
#include "ISBuffer.h"
#include "EXDefines.h"
#include "ISTrace.h"
#include "ISQueryModel.h"
#include "ISQuery.h"
#include "ISAssert.h"
#include "ISSystem.h"
//-----------------------------------------------------------------------------
ISSqlModelCore::ISSqlModelCore(PMetaClassTable *meta_table, QObject *parent) : QAbstractItemModel(parent)
{
	MetaTable = meta_table;
	CurrentSortingColumn = -1;
	CurrentColumnSortOrder = Qt::AscendingOrder;
	IsSystemIndex = -1;
	IsDeletedIndex = -1;
	ShowToolTip = false;
	
	IconSortingUp = BUFFER_ICONS("Arrow.Up");
	IconSortingDown = BUFFER_ICONS("Arrow.Down");
}
//-----------------------------------------------------------------------------
ISSqlModelCore::~ISSqlModelCore()
{
	Clear();
}
//-----------------------------------------------------------------------------
void ISSqlModelCore::FillColumns()
{
	ISQueryModel QueryModel(MetaTable, ISNamespace::QMT_List);
	QueryModel.SetClassFilter(MetaTable->GetAlias() + "." + MetaTable->GetAlias() + "_id = 0");
	QString QueryText = QueryModel.GetQueryText();

	ISQuery qSelectColumns(QueryText);
	if (qSelectColumns.Execute())
	{
		QSqlRecord RecordColumn = qSelectColumns.GetRecord();

		for (int i = 0; i < RecordColumn.count(); i++)
		{
			AppendField(MetaTable->GetField(RecordColumn.fieldName(i)));
		}
	}
}
//-----------------------------------------------------------------------------
void ISSqlModelCore::SetRecords(const QList<QSqlRecord> &records)
{
	beginResetModel();
	Records = records;
	endResetModel();
}
//-----------------------------------------------------------------------------
void ISSqlModelCore::Clear()
{
	IS_TRACE();

	beginResetModel();

	int Step = 0;
	while (!Records.isEmpty())
	{
		Records.removeFirst();
		
		if (Step == 1000) //����� ��� �������� ������� �������
		{
			QThread::currentThread()->msleep(1);
			Step = 0;
		}
		else
		{
			Step++;
		}
	}

	endResetModel();
}
//-----------------------------------------------------------------------------
QSqlRecord ISSqlModelCore::GetRecord(int RowIndex) const
{
	return Records.at(RowIndex);
}
//-----------------------------------------------------------------------------
void ISSqlModelCore::RemoveRecord(int Index)
{
	beginResetModel();
	Records.removeAt(Index);
	endResetModel();
}
//-----------------------------------------------------------------------------
void ISSqlModelCore::RemoveColumn(PMetaClassField *MetaField)
{
	beginResetModel();
	
	int Index = Fields.indexOf(MetaField);
	Fields.remove(Index);

	for (int i = 0; i < Records.count(); i++)
	{
		QSqlRecord SqlRecord = Records.at(i);
		SqlRecord.remove(Index);
		Records.replace(i, SqlRecord);
	}

	endResetModel();
}
//-----------------------------------------------------------------------------
int ISSqlModelCore::GetFieldIndex(const QString &FieldName) const
{
	for (int i = 0; i < Fields.count(); i++)
	{
		if (Fields.at(i)->GetName() == FieldName)
		{
			return i;
		}
	}

	IS_ASSERT(false, QString("Not found field index from field name: %1.").arg(FieldName));
	return -1;
}
//-----------------------------------------------------------------------------
QString ISSqlModelCore::GetFieldLocalName(const QString &FieldName) const
{
	QString LocalName;

	for (int i = 0; i < columnCount(); i++)
	{
		QString CurrentField = headerData(i, Qt::Horizontal, Qt::UserRole).toString();
		if (CurrentField == FieldName)
		{
			LocalName = headerData(i, Qt::Horizontal).toString();
			break;
		}
	}

	IS_ASSERT(LocalName.length(), "Not found local name from field: " + FieldName);
	return LocalName;
}
//-----------------------------------------------------------------------------
PMetaClassField* ISSqlModelCore::GetField(int Index)
{
	return Fields.at(Index);
}
//-----------------------------------------------------------------------------
QVariant ISSqlModelCore::data(const QModelIndex &ModelIndex, int Role) const
{
	if (!ModelIndex.isValid())
	{
		return QVariant();
	}

	PMetaClassField *MetaField = MetaTable->GetField(headerData(ModelIndex.column(), Qt::Horizontal, Qt::UserRole).toString());
	ISNamespace::FieldType FieldType = MetaField->GetType();

	if (Role == Qt::TextColorRole) //���� ����� ������ � �������
	{
		if (GetIsSystem(ModelIndex.row())) //���� ������ ���������
		{
			return qVariantFromValue(QColor(Qt::blue)); //�������� � ����� ������
		}
		else if (GetIsDeleted(ModelIndex.row())) //���� ������ ���������
		{
			return qVariantFromValue(QColor(Qt::red)); //�������� � ������� ������
		}
	}
	else if (Role == Qt::ToolTipRole) //���� ����������� ��������� ��� ������ (ToolTip)
	{
		QVariant TempValue = ModelIndex.data();
		if (!TempValue.isNull())
		{
			return ValueForToolTip(TempValue, FieldType);
		}
	}
	else if (Role == Qt::TextAlignmentRole) //���� ��������� ������ � ������
	{
		return ValueFromTextAlignment(FieldType, MetaField->GetForeign());
	}
	else if (Role == Qt::DisplayRole)
	{
		QVariant Value = Records.at(ModelIndex.row()).value(ModelIndex.column());
		if (Value.isNull())
		{
			return QVariant();
		}

		return ValueForType(Value, FieldType);
	}

	return QVariant();
}
//-----------------------------------------------------------------------------
QVariant ISSqlModelCore::headerData(int Section, Qt::Orientation Orientation, int Role) const
{
	QVariant Value;

	if (Orientation == Qt::Horizontal) //�������������� ���������
	{
		if (Role == Qt::DisplayRole) //����������� ���������� ������������ �������
		{
			Value = Fields.at(Section)->GetLocalListName();
		}
		else if (Role == Qt::DecorationRole) //����������� ������ ������������ �������
		{
			Value = GetSortingIcon(Section);
		}
		else if (Role == Qt::UserRole) //����������� ������������ �������
		{
			Value = Fields.at(Section)->GetName();
		}
		else if (Role == Qt::ToolTipRole) //����������� ����� ��������� ����
		{
			Value = Fields.at(Section)->GetLocalListName();
		}
	}
	else
	{
		if (Role == Qt::DisplayRole)
		{
			return Section + 1;
		}
	}

	return Value;
}
//-----------------------------------------------------------------------------
int ISSqlModelCore::rowCount(const QModelIndex &parent) const
{
	return Records.count();
}
//-----------------------------------------------------------------------------
int ISSqlModelCore::columnCount(const QModelIndex &parent) const
{
	return Fields.count();
}
//-----------------------------------------------------------------------------
QModelIndex ISSqlModelCore::index(int row, int column, const QModelIndex &parent) const
{
	return createIndex(row, column);
}
//-----------------------------------------------------------------------------
QModelIndex ISSqlModelCore::parent(const QModelIndex &index) const
{
	return QModelIndex();
}
//-----------------------------------------------------------------------------
void ISSqlModelCore::SetIsDeletedIndex(int IndexColumn)
{
	IsDeletedIndex = IndexColumn;
}
//-----------------------------------------------------------------------------
void ISSqlModelCore::SetIsSystemIndex(int IndexColumn)
{
	IsSystemIndex = IndexColumn;
}
//-----------------------------------------------------------------------------
void ISSqlModelCore::SetCurrentSorting(int IndexColumn, Qt::SortOrder Order)
{
	CurrentSortingColumn = IndexColumn;
	CurrentColumnSortOrder = Order;
}
//-----------------------------------------------------------------------------
void ISSqlModelCore::SetShowToolTip(bool show_tooltip)
{
	ShowToolTip = show_tooltip;
}
//-----------------------------------------------------------------------------
PMetaClassTable* ISSqlModelCore::GetMetaTable()
{
	return MetaTable;
}
//-----------------------------------------------------------------------------
void ISSqlModelCore::AppendField(PMetaClassField *MetaField)
{
	Fields.append(MetaField);
}
//-----------------------------------------------------------------------------
bool ISSqlModelCore::GetIsSystem(int RowIndex) const
{
	bool IsSystem = Records.at(RowIndex).value("IsSystem").toBool();
	return IsSystem;
}
//-----------------------------------------------------------------------------
bool ISSqlModelCore::GetIsDeleted(int RowIndex) const
{
	bool IsDeleted = Records.at(RowIndex).value("IsDeleted").toBool();
	return IsDeleted;
}
//-----------------------------------------------------------------------------
QIcon ISSqlModelCore::GetSortingIcon(int Section) const
{
	QIcon SortingIcon;

	if (Section == CurrentSortingColumn)
	{
		if (CurrentColumnSortOrder == Qt::AscendingOrder)
		{
			SortingIcon = IconSortingUp;
		}
		else if (CurrentColumnSortOrder == Qt::DescendingOrder)
		{
			SortingIcon = IconSortingDown;
		}
	}

	return SortingIcon;
}
//-----------------------------------------------------------------------------
QVariant ISSqlModelCore::ValueForType(const QVariant &Value, ISNamespace::FieldType FieldType) const
{
	QVariant Result = Value;

	if (FieldType == ISNamespace::FT_Date)
	{
		Result = Value.toDate().toString(DATE_FORMAT_STRING_V1);
	}
	else if (FieldType == ISNamespace::FT_Time)
	{
		Result = Value.toTime().toString(TIME_FORMAT_STRING_V3);
	}
	else if (FieldType == ISNamespace::FT_DateTime)
	{
		Result = Value.toDateTime().toString(DATE_TIME_FORMAT_V3);
	}
	else if (FieldType == ISNamespace::FT_Password)
	{
		QString ValueString = Result.toString();
		for (int i = 0; i < ValueString.count(); i++)
		{
			ValueString.replace(i, 1, SYMBOL_CIRCLE);
		}
		Result = ValueString;
	}

	return Result;
}
//-----------------------------------------------------------------------------
QString ISSqlModelCore::ValueForToolTip(const QVariant &Value, ISNamespace::FieldType FieldType) const
{
	if (FieldType == ISNamespace::FT_Bool || FieldType == ISNamespace::FT_ByteArray)
	{
		return QString();
	}

	QString Result = Value.toString();

	if (!ShowToolTip)
	{
		Result.clear();
	}

	return Result;
}
//-----------------------------------------------------------------------------
QVariant ISSqlModelCore::ValueFromTextAlignment(ISNamespace::FieldType FieldType, PMetaClassForeign *MetaForeign) const
{
	QVariant Result;

	if (FieldType == ISNamespace::FT_Date || FieldType == ISNamespace::FT_Time || FieldType == ISNamespace::FT_DateTime)
	{
		Result = Qt::AlignCenter;
	}
	else if (FieldType == ISNamespace::FT_Int || FieldType == ISNamespace::FT_Double)
	{
		if (!MetaForeign)
		{
			Result = Qt::AlignRight + Qt::AlignVCenter;
		}
	}

	return Result;
}
//-----------------------------------------------------------------------------
