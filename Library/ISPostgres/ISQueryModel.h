#pragma once
//-----------------------------------------------------------------------------
#include "ispostgres_global.h"
#include "ISNamespace.h"
#include "ISQueryModelHelper.h"
#include "PMetaClassTable.h"
#include "PMetaClassField.h"
//-----------------------------------------------------------------------------
//!����� ��� ������������ �������� �������
class ISPOSTGRES_EXPORT ISQueryModel : public QObject
{
	Q_OBJECT

public:
	ISQueryModel(PMetaClassTable *meta_table, ISNamespace::QueryModelType model_type, QObject *parent = 0);
	virtual ~ISQueryModel();

	void AddCondition(const QString &Condition, const QVariant &Value);
	void ClearConditions();
	QVariantMap GetConditions();

	QString GetQueryText(); //�������� ����� ����� �������

	void SetShowDataType(ISNamespace::ShowDataType ShowType); //�������� ������ ����������� ��������� �������
	void SetParentObjectIDClassFilter(int ParentObjectID); //�������� ������� (�������������) ������� ��� ��������� �������
	
	void SetClassFilter(const QString &class_filter); //�������� ������
	QString GetClassFilter() const; //�������� ������
	void ClearClassFilter(); //�������� ������

	void SetSearchFilter(const QString &search_filter);
	QString GetSearchFilter() const;
	void ClearSearchFilter();

	void SetOrderField(const QString &FieldName); //�������� ��� �������, �� �������� ���� ����������
	void SetOrderSort(Qt::SortOrder OrderSort); //�������� ��� ����������
	void SetLimit(int limit); //�������� ���������� ��������� ������� �� ��������
	void SetOffset(int offset); //�������� ��������

protected:
	void CreateQuerySelectSystemFields(); //�������� ������ ��������� ����� ��� ������� �������
	void CreateQuerySelectFields(); //�������� ������ ����� ��� ������� �������
	void CreateQuerySelectIsDeleted(); //�������� ������� ����������� ����� ���������/�� ���������
	void CheckQuery(const QString &QueryText); //�������� �������

private:
	PMetaClassTable *MetaTable;
	ISNamespace::QueryModelType ModelType;
	
	QString QuerySelectText; //����� ������� �� �������
	QString QuerySelectSystemFields; //������ ��������� ����� ��� ������� �� �������
	QString QuerySelectFields;
	QString QuerySelectFrom; //��������� � �������
	QString QuerySelectLeftJoin;
	QString QuerySelectIsDeleted;
	QString QueryWhereText; //����� ������� �������
	
	QString QueryOrderText; //����������
	QString OrderField; //��� ���� �� �������� ���� ����������
	QString OrderFieldDefault; //���� �� ��������� �� ��� ����������
	Qt::SortOrder OrderSort; //��� ����������
	int Limit; //����� ������� �� ��������
	int Offset; //��������

	ISNamespace::ShowDataType CurrentShowType;

	QString ClassAlias;
	QString ClassFilter;
	QString SearchFilter;

	QVariantMap Conditions;
};
//-----------------------------------------------------------------------------
