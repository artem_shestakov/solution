#pragma once
//-----------------------------------------------------------------------------
#include "ispostgres_global.h"
//-----------------------------------------------------------------------------
class ISPOSTGRES_EXPORT ISDatabase : public QObject
{
	Q_OBJECT

public:
	ISDatabase(const ISDatabase &) = delete;
	ISDatabase(ISDatabase &&) = delete;
	ISDatabase &operator=(const ISDatabase &) = delete;
	ISDatabase &operator=(ISDatabase &&) = delete;
	~ISDatabase();

	static ISDatabase& GetInstance();

	QSqlDatabase CreateDatabase(const QString &Login, const QString &Password, const QString &ConnectionName = QString()); //������� ����� ��������� ����������� � ���� ������

	QSqlDatabase& GetDefaultDB(); //�������� ����������� ���� ������
	QSqlDatabase& GetSystemDB(); //�������� ��������� ���� ������

	bool CheckExistDatabase(const QString &Database); //��������� ������������� ���� ������
	QString GetVersionPostgres(); //�������� ������ PostgreSQL
	QString GetCurrentDatabaseSize(); //�������� ������ ���� ������
	QString GetStartTimeServer(); //�������� ����� ������� ������� ��� ������
	QString GetInetClientAddress(); //�������� ����� ��������� ������� ����������
	QString GetInetServerAddress(); //�������� ����� ��������� ������� ����������

	bool ConnectToTestDB(const QString &Server, int Port, const QString &Login, const QString &Password, const QString &Database, QString &ErrorConection); //������� �������� �����������
	void DisconnectFromTestDB(); //���������� �� ��������� ����������

	bool ConnectToDefaultDB(const QString &Login, const QString &Password, QString &ErrorConnection = QString()); //����������� � ����������� ���� ������
	bool ConnectToSystemDB(QString &ErrorConnection = QString()); //����������� � ��������� ���� ������

	void DisconnectFromDefaultDB(); //����������� �� ����������� ���� ������
	void DisconnectFromSystemDB(); //����������� �� ��������� ���� ������

	void DisconnectFromDatabase(QSqlDatabase &SqlDatabase); //����������� �� ���� ������

protected:
	bool ConnectToDatabase(QSqlDatabase &SqlDatabase, const QString &Login, const QString &Password, const QString &Database, QString &ErrorConnection = QString()); //����������� � ���� ������

private:
	ISDatabase();

	QSqlDatabase DefaultDB;
	QSqlDatabase SystemDB;
	QSqlDatabase TestDB;
};
//-----------------------------------------------------------------------------
