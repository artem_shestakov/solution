#include "StdAfx.h"
#include "ISModelThreadWorker.h"
#include "EXDefines.h"
#include "ISDatabase.h"
#include "ISCountingTime.h"
#include "ISDebug.h"
#include "ISAssert.h"
//-----------------------------------------------------------------------------
ISModelThreadWorker::ISModelThreadWorker(QObject *parent) : QObject(parent)
{
	DB = QSqlDatabase::database("ThreadDatabase");
	if (!DB.isValid())
	{
		DB = QSqlDatabase::cloneDatabase(ISDatabase::GetInstance().GetDefaultDB(), "ThreadDatabase");
	}
}
//-----------------------------------------------------------------------------
ISModelThreadWorker::~ISModelThreadWorker()
{

}
//-----------------------------------------------------------------------------
void ISModelThreadWorker::Execute(const QString &SqlQueryText, const QVariantMap &Conditions)
{
	QList<QSqlRecord> Records;
	int Step = 0;
	ISCountingTime Time;

	ISDebug::ShowDebugString("Connecting to DB...");
	if (DB.open())
	{
		ISDebug::ShowDebugString(QString("Connecting to DB %1 msec.").arg(Time.GetElapsed()));
		Time.Restart();

		QSqlQuery *SqlQuery = new QSqlQuery(DB);
		ISDebug::ShowDebugString("Preparing query...");
		bool Prepred = SqlQuery->prepare(SqlQueryText);
		IS_ASSERT(!SqlQuery->isSelect(), QString("Query not select:\n%1").arg(SqlQueryText));

		if (Prepred)
		{
			ISDebug::ShowDebugString(QString("Prepared query %1 msec.").arg(Time.GetElapsed()));
			Time.Restart();

			for (const auto Condition : Conditions.toStdMap()) //����� ���������� �������
			{
				if (SqlQuery->boundValues().contains(Condition.first)) //���� �������� ������
				{
					SqlQuery->bindValue(Condition.first, Condition.second);
				}
				else //�������� �� ������
				{
					IS_ASSERT(false, "Not BindValue.");
				}
			}

			ISDebug::ShowDebugString("Executing query...");
			if (SqlQuery->exec())
			{
				ISDebug::ShowDebugString(QString("Executed query %1 msec.").arg(Time.GetElapsed()));

				if (Time.GetElapsed() > MAX_QUERY_TIME)
				{
					ISDebug::ShowDebugString(QString("Long query %1 msec: %2").arg(Time.GetElapsed()).arg(SqlQuery->lastQuery().simplified()));
				}

				emit ExecutedQuery();

				Time.Restart();
				ISDebug::ShowDebugString("Loading records...");
				while (SqlQuery->next()) //������� �������
				{
					Records.push_back(SqlQuery->record());

					if (Step == 500) //����� ��� ���������� ������� �������
					{
						QThread::currentThread()->msleep(1);
						Step = 0;
					}
					else
					{
						Step++;
					}
				}
				ISDebug::ShowDebugString(QString("Loaded records %1 msec.").arg(Time.GetElapsed()));

				emit Results(Records);
				emit Finished();
			}
			else
			{
				emit ErrorQuery(SqlQuery->lastError(), SqlQueryText);
			}
		}
		else
		{
			emit ErrorQuery(SqlQuery->lastError(), SqlQueryText);
		}

		delete SqlQuery;
		SqlQuery = nullptr;

		DB.close();
	}
	else
	{
		emit ErrorConnection(DB.lastError());
	}
}
//-----------------------------------------------------------------------------
