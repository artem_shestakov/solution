#include "StdAfx.h"
#include "ISQueryModel.h"
#include "EXDefines.h"
#include "ISQuery.h"
#include "ISMetaData.h"
#include "ISDebug.h"
#include "ISSystem.h"
//-----------------------------------------------------------------------------
ISQueryModel::ISQueryModel(PMetaClassTable *meta_table, ISNamespace::QueryModelType model_type, QObject *parent) : QObject(parent)
{
	MetaTable = meta_table;
	ModelType = model_type;
	ClassAlias = MetaTable->GetAlias();
	ClassFilter = MetaTable->GetClassFilter();
	Limit = 0;
	Offset = 0;

	CreateQuerySelectSystemFields();
	CreateQuerySelectFields();
	CreateQuerySelectIsDeleted();

	//������������� ����������
	QuerySelectText = "SELECT \n" + ClassAlias + "." + ClassAlias + "_" + MetaTable->GetSystemFields().at(0)->GetName().toLower() + " AS \"" + MetaTable->GetSystemFields().at(0)->GetName() + "\", \n";
	QuerySelectFrom = "FROM " + MetaTable->GetName().toLower() + " " + ClassAlias + " \n";
	QueryWhereText = "WHERE \n";
	QueryWhereText += QuerySelectIsDeleted;
	OrderFieldDefault = ClassAlias + "." + ClassAlias + "_id";
	OrderSort = Qt::SortOrder::AscendingOrder;

	CurrentShowType = ISNamespace::ShowDataType::SDT_Actual;
}
//-----------------------------------------------------------------------------
ISQueryModel::~ISQueryModel()
{

}
//-----------------------------------------------------------------------------
void ISQueryModel::AddCondition(const QString &Condition, const QVariant &Value)
{
	Conditions.insert(Condition, Value);
}
//-----------------------------------------------------------------------------
void ISQueryModel::ClearConditions()
{
	Conditions.clear();
}
//-----------------------------------------------------------------------------
QVariantMap ISQueryModel::GetConditions()
{
	return Conditions;
}
//-----------------------------------------------------------------------------
QString ISQueryModel::GetQueryText()
{
	QString SqlText;

	SqlText += QuerySelectText;
	SqlText += QuerySelectSystemFields;
	SqlText += QuerySelectFields;
	SqlText += QuerySelectFrom;
	SqlText += QuerySelectLeftJoin;
	
	switch (CurrentShowType) //��� ����������� ������ (�������� ��� ����������)
	{
	case ISNamespace::ShowDataType::SDT_Actual: SqlText += QueryWhereText; SqlText += "false \n"; break;
	case ISNamespace::ShowDataType::SDT_Deleted: SqlText += QueryWhereText; SqlText += "true \n"; break;
	}

	//���� ��� ������� ���������� ������
	if (ClassFilter.length())
	{
		SqlText += "AND " + ClassFilter + " \n";
	}

	//������ ������
	if (SearchFilter.length())
	{
		SqlText += "AND " + SearchFilter + " \n";
	}

	//���������� ����������
	QString Sort;
	if (OrderSort == Qt::AscendingOrder)
	{
		Sort = " ASC";
	}
	else
	{
		Sort = " DESC";
	}

	if (OrderField.isEmpty())
	{
		QueryOrderText = "ORDER BY " + OrderFieldDefault + Sort;
	}
	else
	{
		QueryOrderText = "ORDER BY " + OrderField + Sort;
	}

	SqlText += QueryOrderText;

	if (Limit)
	{
		SqlText += QString(" \nLIMIT %1 OFFSET %2").arg(Limit).arg(Offset);
	}

	return SqlText;
}
//-----------------------------------------------------------------------------
void ISQueryModel::SetShowDataType(ISNamespace::ShowDataType ShowType)
{
	CurrentShowType = ShowType;
}
//-----------------------------------------------------------------------------
void ISQueryModel::SetParentObjectIDClassFilter(int ParentObjectID)
{
	bool Contains = ClassFilter.contains(":ParentObjectID");
	if (Contains)
	{
		ClassFilter.replace(":ParentObjectID", QString::number(ParentObjectID));
	}
}
//-----------------------------------------------------------------------------
void ISQueryModel::SetClassFilter(const QString &class_filter)
{
	if (class_filter.length())
	{
		ClassFilter = class_filter;
	}
}
//-----------------------------------------------------------------------------
QString ISQueryModel::GetClassFilter() const
{
	return ClassFilter;
}
//-----------------------------------------------------------------------------
void ISQueryModel::ClearClassFilter()
{
	ClassFilter.clear();
	Conditions.clear();
}
//-----------------------------------------------------------------------------
void ISQueryModel::SetSearchFilter(const QString &search_filter)
{
	SearchFilter = search_filter;
}
//-----------------------------------------------------------------------------
QString ISQueryModel::GetSearchFilter() const
{
	return SearchFilter;
}
//-----------------------------------------------------------------------------
void ISQueryModel::ClearSearchFilter()
{
	SearchFilter.clear();
}
//-----------------------------------------------------------------------------
void ISQueryModel::SetOrderField(const QString &FieldName)
{
	this->OrderField = FieldName;
}
//-----------------------------------------------------------------------------
void ISQueryModel::SetOrderSort(Qt::SortOrder OrderSort)
{
	this->OrderSort = OrderSort;
}
//-----------------------------------------------------------------------------
void ISQueryModel::SetLimit(int limit)
{
	Limit = limit;
}
//-----------------------------------------------------------------------------
void ISQueryModel::SetOffset(int offset)
{
	Offset = offset;
}
//-----------------------------------------------------------------------------
void ISQueryModel::CreateQuerySelectSystemFields()
{
	for (int i = 1; i < MetaTable->GetSystemFields().count(); i++) //����� ��������� ����� � ��������� �� � ������
	{
		PMetaClassField *SystemField = MetaTable->GetSystemFields().at(i);

		if (!MetaTable->GetVisibleSystemFields().contains(SystemField))
		{
			if (SystemField->GetHideFromList())
			{
				continue;
			}
		}

		QuerySelectSystemFields += ClassAlias + "." + ClassAlias + "_" + SystemField->GetName().toLower() + " AS \"" + SystemField->GetName() + "\", \n";
	}
}
//-----------------------------------------------------------------------------
void ISQueryModel::CreateQuerySelectFields()
{
	for (int i = 0; i < MetaTable->GetFields().count(); i++)
	{
		PMetaClassField *Field = MetaTable->GetFields().at(i);

		if (ModelType == ISNamespace::QMT_Object)
		{
			if (Field->GetHideFromObject())
			{
				continue;
			}
		}
		else if (ModelType == ISNamespace::QMT_List)
		{
			if (Field->GetHideFromList())
			{
				continue;
			}
		}

		if (Field->GetForeign()) //���� �� ���� ���������� ������� ����
		{
			PMetaClassForeign *MetaForeign = Field->GetForeign();
			PMetaClassTable *MetaTableForeign = ISMetaData::GetInstanse().GetMetaTable(MetaForeign->GetForeignClass());
			QuerySelectLeftJoin += "LEFT JOIN " + MetaTableForeign->GetName().toLower() + " " + ISQueryModelHelper::GetAliasForLeftJoinTable(MetaTableForeign->GetAlias(), i) + " ON " + ClassAlias + "." + ClassAlias + "_" + Field->GetName().toLower() + " = " + ISQueryModelHelper::GetAliasForLeftJoinTable(MetaTableForeign->GetAlias(), i) + "." + MetaTableForeign->GetAlias() + "_" + MetaForeign->GetForeginField().toLower() + " \n";
			QuerySelectFields += ISQueryModelHelper::GetForeignViewNameField(MetaTableForeign->GetAlias(), MetaForeign, i).toLower() + " AS \"" + Field->GetName() + "\", \n";
		}
		else
		{
			if (Field->GetQueryText().length())
			{
				QuerySelectFields += "(" + Field->GetQueryText() + ") AS \"" + Field->GetName() + "\", \n";
			}
			else
			{
				QuerySelectFields += ClassAlias + "." + ClassAlias + "_" + Field->GetName().toLower() + " AS \"" + Field->GetName() + "\", \n";
			}
		}
	}

	ISSystem::RemoveLastSymbolFromString(QuerySelectFields, 3);
	QuerySelectFields += " \n";
}
//-----------------------------------------------------------------------------
void ISQueryModel::CreateQuerySelectIsDeleted()
{
	QuerySelectIsDeleted = ClassAlias + "." + ClassAlias + "_isdeleted = ";
}
//-----------------------------------------------------------------------------
void ISQueryModel::CheckQuery(const QString &QueryText)
{
	ISQuery qQuery;
	if (qQuery.Prepare(QueryText))
	{
		ISDebug::ShowDebugString("Prepare Query - OK");
	}
	else
	{
		ISDebug::ShowCriticalString("Prepare Query - ERROR");
	}
}
//-----------------------------------------------------------------------------
