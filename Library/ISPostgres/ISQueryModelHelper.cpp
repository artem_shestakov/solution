#include "StdAfx.h"
#include "ISQueryModelHelper.h"
#include "EXDefines.h"
#include "ISSystem.h"
//-----------------------------------------------------------------------------
ISQueryModelHelper::ISQueryModelHelper()
{

}
//-----------------------------------------------------------------------------
ISQueryModelHelper::~ISQueryModelHelper()
{

}
//-----------------------------------------------------------------------------
QString ISQueryModelHelper::GetForeignViewNameField(const QString &MetaTableForeignAlias, PMetaClassForeign *MetaForeign, int Iterator)
{
	QString ForeignViewNameField = MetaForeign->GetForeignViewNameField();
	QStringList List = ForeignViewNameField.split(";");
	
	if (List.count() > 1)
	{
		QString SqlText = QString();
		
		for (int i = 0; i < List.count(); i++)
		{
			SqlText += ISQueryModelHelper::GetAliasForLeftJoinTable(MetaTableForeignAlias, Iterator) + "." + MetaTableForeignAlias + "_" + List.at(i).toLower();
			if (i == List.count() - 1)
			{
				continue;
			}

			SqlText += QString(" || '" + QString(FOREIGN_VIEW_NAME_FIELD_SPLIT) + "' || ");
		}

		return SqlText;
	}

	return ISQueryModelHelper::GetAliasForLeftJoinTable(MetaTableForeignAlias, Iterator) + "." + MetaTableForeignAlias + "_" + ForeignViewNameField.toLower();
}
//-----------------------------------------------------------------------------
QString ISQueryModelHelper::GetAliasForLeftJoinTable(const QString &TableAlias, int Iterator)
{
	QString Alias = TableAlias + QString::number(Iterator);
	return Alias;
}
//-----------------------------------------------------------------------------
