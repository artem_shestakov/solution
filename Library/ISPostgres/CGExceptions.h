#pragma once
//-----------------------------------------------------------------------------
#include "StdAfx.h"
//-----------------------------------------------------------------------------
struct ExceptionBase : public std::exception
{
	QString Description;
	int Line;
	QString ScrFile;

	ExceptionBase(const QString& error_code, const QString& description) : std::exception(error_code.toLocal8Bit()), Description(description)
	{

	}
	ExceptionBase(const QString& text) : std::exception(text.toLocal8Bit())
	{

	}
};
//-----------------------------------------------------------------------------