#pragma once
//-----------------------------------------------------------------------------
#include "ispostgres_global.h"
#include "ISSqlModelCore.h"
//-----------------------------------------------------------------------------
class ISPOSTGRES_EXPORT ISSqlModelView : public ISSqlModelCore
{
	Q_OBJECT

public:
	Q_INVOKABLE ISSqlModelView(PMetaClassTable *meta_table, QObject *parent = 0);
	virtual ~ISSqlModelView();

	void FillColumns() override;
};
//-----------------------------------------------------------------------------
