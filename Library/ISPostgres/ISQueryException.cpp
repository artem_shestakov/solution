#include "StdAfx.h"
#include "ISQueryException.h"
#include "ISDebug.h"
//-----------------------------------------------------------------------------
ISQueryException::ISQueryException(const QString& text, QSqlError::ErrorType type) : ExceptionBase(text), Type(type)
{
	ISDebug::ShowCriticalString(text);
}
//-----------------------------------------------------------------------------
ISQueryException::~ISQueryException()
{

}
//-----------------------------------------------------------------------------
QSqlError::ErrorType ISQueryException::GetType() const
{
	return Type;
}
//-----------------------------------------------------------------------------
ISQueryExceptionSyntax::ISQueryExceptionSyntax(const QString& text) : ISQueryException(text, QSqlError::StatementError)
{
	
}
//-----------------------------------------------------------------------------
ISQueryExceptionConnection::ISQueryExceptionConnection(const QString& text) : ISQueryException(text, QSqlError::ConnectionError)
{
	
}
//-----------------------------------------------------------------------------
ISQueryExceptionTransaction::ISQueryExceptionTransaction(const QString& text) : ISQueryException(text, QSqlError::TransactionError)
{
	
}
//-----------------------------------------------------------------------------
