#include "StdAfx.h"
#include "ISSqlModelView.h"
//-----------------------------------------------------------------------------
ISSqlModelView::ISSqlModelView(PMetaClassTable *meta_table, QObject *parent) : ISSqlModelCore(meta_table, parent)
{

}
//-----------------------------------------------------------------------------
ISSqlModelView::~ISSqlModelView()
{

}
//-----------------------------------------------------------------------------
void ISSqlModelView::FillColumns()
{
	for (int i = 0; i < GetMetaTable()->GetFields().count(); i++)
	{
		AppendField(GetMetaTable()->GetFields().at(i));
	}
}
//-----------------------------------------------------------------------------
