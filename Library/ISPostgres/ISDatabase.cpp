#include "StdAfx.h"
#include "ISDatabase.h"
#include "EXDefines.h"
#include "ISConfig.h"
#include "ISDebug.h"
#include "ISQuery.h"
#include "ISAssert.h"
#include "EXConstants.h"
#include "ISCountingTime.h"
//-----------------------------------------------------------------------------
static QString QS_DATABASE = PREPARE_QUERY("SELECT COUNT(*) FROM pg_database WHERE datname = :DatabaseName");
//-----------------------------------------------------------------------------
static QString QS_VERSION_POSTGRESQL = PREPARE_QUERY("SELECT version()");
//-----------------------------------------------------------------------------
static QString QS_DATABASE_SIZE = PREPARE_QUERY("SELECT pg_size_pretty(pg_database_size(current_database())) AS size_pretty");
//-----------------------------------------------------------------------------
static QString QS_START_TIME_SERVER = PREPARE_QUERY("SELECT pg_postmaster_start_time()");
//-----------------------------------------------------------------------------
static QString QS_INET_CLIENT_ADDRESS = PREPARE_QUERY("SELECT inet_client_addr()");
//-----------------------------------------------------------------------------
static QString QS_INET_SERVER_ADDRESS = PREPARE_QUERY("SELECT inet_server_addr()");
//-----------------------------------------------------------------------------
ISDatabase::ISDatabase() : QObject()
{
	DefaultDB = QSqlDatabase::addDatabase(SQL_DRIVER_QPSQL, "DefaultDB");
	SystemDB = QSqlDatabase::addDatabase(SQL_DRIVER_QPSQL, "SystemDB");
	TestDB = QSqlDatabase::addDatabase(SQL_DRIVER_QPSQL, "TestDB");
}
//-----------------------------------------------------------------------------
ISDatabase::~ISDatabase()
{

}
//-----------------------------------------------------------------------------
ISDatabase& ISDatabase::GetInstance()
{
	static ISDatabase Database;
	return Database;
}
//-----------------------------------------------------------------------------
QSqlDatabase ISDatabase::CreateDatabase(const QString &Login, const QString &Password, const QString &ConnectionName)
{
	QSqlDatabase DB = QSqlDatabase::addDatabase(SQL_DRIVER_QPSQL, ConnectionName);
	DB.setHostName(ISConfig::GetInstance().GetValue(CONST_CONFIG_CONNECTION_SERVER).toString());
	DB.setPort(ISConfig::GetInstance().GetValue(CONST_CONFIG_CONNECTION_PORT).toInt());
	DB.setDatabaseName(ISConfig::GetInstance().GetValue(CONST_CONFIG_CONNECTION_DATABASE).toString());
	DB.setUserName(Login);
	DB.setPassword(Password);
	return DB;
}
//-----------------------------------------------------------------------------
QSqlDatabase& ISDatabase::GetDefaultDB()
{
	return DefaultDB;
}
//-----------------------------------------------------------------------------
QSqlDatabase& ISDatabase::GetSystemDB()
{
	return SystemDB;
}
//-----------------------------------------------------------------------------
bool ISDatabase::CheckExistDatabase(const QString &Database)
{
	bool Result = false;

	ISQuery qSelectDatabase(SystemDB, QS_DATABASE);
	qSelectDatabase.BindValue(":DatabaseName", Database);
	if (qSelectDatabase.ExecuteFirst())
	{
		int Count = qSelectDatabase.ReadColumn("count").toInt();
		if (Count)
		{
			Result = true;
		}
	}

	return Result;
}
//-----------------------------------------------------------------------------
QString ISDatabase::GetVersionPostgres()
{
	QString Version;

	ISQuery qSelectVersion(QS_VERSION_POSTGRESQL);
	if (qSelectVersion.ExecuteFirst())
	{
		Version = qSelectVersion.ReadColumn("version").toString();
	}

	return Version;
}
//-----------------------------------------------------------------------------
QString ISDatabase::GetCurrentDatabaseSize()
{
	QString DatabaseSize;

	ISQuery qSelectDatabaseSize(QS_DATABASE_SIZE);
	if (qSelectDatabaseSize.ExecuteFirst())
	{
		DatabaseSize = qSelectDatabaseSize.ReadColumn("size_pretty").toString();
	}

	return DatabaseSize;
}
//-----------------------------------------------------------------------------
QString ISDatabase::GetStartTimeServer()
{
	QString StartTime;

	ISQuery qSelect(QS_START_TIME_SERVER);
	if (qSelect.ExecuteFirst())
	{
		StartTime = qSelect.ReadColumn("pg_postmaster_start_time").toDateTime().toString(DATE_TIME_FORMAT_V3);
	}

	return StartTime;
}
//-----------------------------------------------------------------------------
QString ISDatabase::GetInetClientAddress()
{
	QString Address;

	ISQuery qSelect(QS_INET_CLIENT_ADDRESS);
	if (qSelect.ExecuteFirst())
	{
		Address = qSelect.ReadColumn("inet_client_addr").toString();
	}

	return Address;
}
//-----------------------------------------------------------------------------
QString ISDatabase::GetInetServerAddress()
{
	QString Address;

	ISQuery qSelect(QS_INET_SERVER_ADDRESS);
	if (qSelect.ExecuteFirst())
	{
		Address = qSelect.ReadColumn("inet_server_addr").toString();
	}

	return Address;
}
//-----------------------------------------------------------------------------
bool ISDatabase::ConnectToTestDB(const QString &Server, int Port, const QString &Login, const QString &Password, const QString &Database, QString &ErrorConection)
{
	TestDB.setHostName(Server);
	TestDB.setPort(Port);
	TestDB.setUserName(Login);
	TestDB.setPassword(Password);
	TestDB.setDatabaseName(Database);
	bool Connected = TestDB.open();

	if (!Connected)
	{
		ErrorConection = TestDB.lastError().text();
	}

	return Connected;
}
//-----------------------------------------------------------------------------
void ISDatabase::DisconnectFromTestDB()
{
	TestDB.close();
	
	TestDB.setHostName(QString());
	TestDB.setPort(-1);
	TestDB.setUserName(QString());
	TestDB.setPassword(QString());
	TestDB.setDatabaseName(QString());
}
//-----------------------------------------------------------------------------
bool ISDatabase::ConnectToDefaultDB(const QString &Login, const QString &Password, QString &ErrorConnection)
{
	QString Database = ISConfig::GetInstance().GetValue(CONST_CONFIG_CONNECTION_DATABASE).toString();

	bool Connected = ConnectToDatabase(DefaultDB, Login, Password, Database, ErrorConnection);
	return Connected;
}
//-----------------------------------------------------------------------------
bool ISDatabase::ConnectToSystemDB(QString &ErrorConnection)
{
	bool Connected = ConnectToDatabase(SystemDB, SYSTEM_USER_LOGIN, SYSTEM_USER_PASSWORD, SYSTEM_DATABASE_NAME, ErrorConnection);
	return Connected;
}
//-----------------------------------------------------------------------------
void ISDatabase::DisconnectFromDefaultDB()
{
	DisconnectFromDatabase(DefaultDB);
}
//-----------------------------------------------------------------------------
void ISDatabase::DisconnectFromSystemDB()
{
	DisconnectFromDatabase(SystemDB);
}
//-----------------------------------------------------------------------------
void ISDatabase::DisconnectFromDatabase(QSqlDatabase &SqlDatabase)
{
	QString DatabaseName = SqlDatabase.databaseName();
	ISDebug::ShowDebugString(QString("Disconnect from database %1...").arg(DatabaseName));

	ISCountingTime CountingTime;
	SqlDatabase.close();

	SqlDatabase.setHostName(QString());
	SqlDatabase.setPort(-1);
	SqlDatabase.setDatabaseName(QString());
	SqlDatabase.setUserName(QString());
	SqlDatabase.setPassword(QString());

	ISDebug::ShowInfoString(QString("Disconnect from database \"%1\" done. %2 msec").arg(DatabaseName).arg(CountingTime.GetElapsed()));
}
//-----------------------------------------------------------------------------
bool ISDatabase::ConnectToDatabase(QSqlDatabase &SqlDatabase, const QString &Login, const QString &Password, const QString &Database, QString &ErrorConnection)
{
	QString Server = ISConfig::GetInstance().GetValue(CONST_CONFIG_CONNECTION_SERVER).toString();
	int Port = ISConfig::GetInstance().GetValue(CONST_CONFIG_CONNECTION_PORT).toInt();

	bool AlreadyOpen = SqlDatabase.isOpen();
	IS_ASSERT(!AlreadyOpen, QString("Database \"%1\" already open.").arg(SqlDatabase.databaseName()));

	SqlDatabase.setHostName(Server);
	SqlDatabase.setPort(Port);
	SqlDatabase.setDatabaseName(Database);
	SqlDatabase.setUserName(Login);
	SqlDatabase.setPassword(Password);

	ISCountingTime CountingTime;
	ISDebug::ShowInfoString(QString("Connecting to database %1...").arg(Database));

	bool Connected = SqlDatabase.open();
	if (Connected)
	{
		ISDebug::ShowInfoString(QString("Connection to database \"%1\" done. %2 msec").arg(Database).arg(CountingTime.GetElapsed()));
	}
	else
	{
		ErrorConnection = SqlDatabase.lastError().text();
		ISDebug::ShowWarningString(QString("Connection to database \"%1\" failed. Error: %2").arg(Database).arg(ErrorConnection));
	}

	return Connected;
}
//-----------------------------------------------------------------------------
