#pragma once
//-----------------------------------------------------------------------------
#include "ispostgres_global.h"
#include "CGExceptions.h"
//-----------------------------------------------------------------------------
struct ISPOSTGRES_EXPORT ISQueryException : ExceptionBase
{
	friend struct ISQueryExceptionSyntax;
	friend struct ISQueryExceptionConnection;
	friend struct ISQueryExceptionTransaction;

public:
	ISQueryException(const QString& text, QSqlError::ErrorType type);
	~ISQueryException();

	QSqlError::ErrorType GetType() const;

private:
	QSqlError::ErrorType Type;
};
//-----------------------------------------------------------------------------
struct ISPOSTGRES_EXPORT ISQueryExceptionSyntax : ISQueryException
{
	ISQueryExceptionSyntax(const QString& text);
};
//-----------------------------------------------------------------------------
struct ISPOSTGRES_EXPORT ISQueryExceptionConnection : ISQueryException
{
	ISQueryExceptionConnection(const QString& text);
};
//-----------------------------------------------------------------------------
struct ISPOSTGRES_EXPORT ISQueryExceptionTransaction : ISQueryException
{
	ISQueryExceptionTransaction(const QString& text);
};
//-----------------------------------------------------------------------------
