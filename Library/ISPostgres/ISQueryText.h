#pragma once
//-----------------------------------------------------------------------------
#include "ispostgres_global.h"
//-----------------------------------------------------------------------------
class ISPOSTGRES_EXPORT ISQueryText : public QObject
{
	Q_OBJECT

public:
	ISQueryText(const ISQueryText &) = delete;
	ISQueryText(ISQueryText &&) = delete;
	ISQueryText &operator=(const ISQueryText &) = delete;
	ISQueryText &operator=(ISQueryText &&) = delete;
	~ISQueryText();

	static ISQueryText& GetInstance();
	
	QString InsertQuery(const char *QueryText, const char *FileName, int Line); //�������� ������ � ������
	void CheckAllQueries(const QSqlDatabase &Database); //�������� ���� ��������
	void CheckAllMetaQuery(); //�������� ���� ����-��������

private:
	ISQueryText();

	QMap<QString, QString> Queries;
};
//-----------------------------------------------------------------------------
#define PREPARE_QUERY(SqlText) ISQueryText::GetInstance().InsertQuery(SqlText, __FILE__, __LINE__);
//-----------------------------------------------------------------------------
