#include "StdAfx.h"
#include "ISQueryText.h"
#include "ISAssert.h"
#include "EXDefines.h"
#include "ISDebug.h"
#include "ISSystem.h"
#include "ISCountingTime.h"
#include "ISMetaData.h"
#include "ISMetaQuery.h"
//-----------------------------------------------------------------------------
ISQueryText::ISQueryText() : QObject()
{

}
//-----------------------------------------------------------------------------
ISQueryText::~ISQueryText()
{

}
//-----------------------------------------------------------------------------
ISQueryText& ISQueryText::GetInstance()
{
	static ISQueryText QueryText;
	return QueryText;
}
//-----------------------------------------------------------------------------
QString ISQueryText::InsertQuery(const char *QueryText, const char *FileName, int Line)
{
	Queries.insert(QString("File: %1. Line: %2.").arg(FileName).arg(Line), QString(QueryText));
	return QString(QueryText);
}
//-----------------------------------------------------------------------------
void ISQueryText::CheckAllQueries(const QSqlDatabase &Database)
{
	if (!Queries.count())
	{
		return;
	}

	IS_ASSERT(Database.isOpen(), "Database not connected!");

	ISCountingTime Time;

	for (const auto Query : Queries.toStdMap())
	{
		QString About = Query.first;
		QString QueryText = Query.second;
		
		QSqlQuery Query(Database);
		Query.prepare(QueryText);
		IS_ASSERT(Query.lastError().type() == QSqlError::NoError, QString("%1\n%3\nError preparing Query:\n%2").arg(About, QueryText, Query.lastError().databaseText()));
	}

	for (int i = 0; i < ISMetaData::GetInstanse().GetMetaQueries().count(); i++)
	{
		QString QueryName = ISMetaData::GetInstanse().GetMetaQueries().at(i);
		QString QueryText = ISMetaQuery::GetQueryText(QueryName);

		QSqlQuery Query(Database);
		Query.prepare(QueryText);
		IS_ASSERT(Query.lastError().type() == QSqlError::NoError, QString("Error preparing query: %1 \n\n%2\n\nError: %3").arg(QueryName, QueryText, Query.lastError().databaseText()));
	}

	QString TimeInitialized = ISSystem::MillisecondsToString(Time.GetElapsed());
	ISDebug::ShowDebugString(QString("Check %1 queries time: %2").arg(Queries.count()).arg(TimeInitialized));

	if (Queries.count())
	{
		Queries.clear();
	}
}
//-----------------------------------------------------------------------------
void ISQueryText::CheckAllMetaQuery()
{

}
//-----------------------------------------------------------------------------
