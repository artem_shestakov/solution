#pragma once
//-----------------------------------------------------------------------------
#include "ISInterfaceForm.h"
#include "ISLabelNoDataTable.h"
#include "ISBaseTableWidget.h"
//-----------------------------------------------------------------------------
class ISCostSummaryBase : public ISInterfaceForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISCostSummaryBase(const QString &TableName, QWidget *parent = 0);
	virtual ~ISCostSummaryBase();

protected:
	void OnCreateInterface();
	void OnCreateTablePeriod(const QString &QueryText, const QString &PeriodYear);
	QStringList OnGetHorizonalHeaderLabels(QSqlQueryModel *Model);

private:
	QScrollArea *ScrollArea;
	QVBoxLayout *LayoutScrollArea;
	QFont FontLabel;

	QString TableName;
	int CountPeriods;
};
//-----------------------------------------------------------------------------
