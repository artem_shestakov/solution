#pragma once
//-----------------------------------------------------------------------------
#include "ISListBaseForm.h"
#include "ISCalculationsRecipe.h"
//-----------------------------------------------------------------------------
class ISExchangeRatesListForm : public ISListBaseForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISExchangeRatesListForm(PMetaClassTable *MetaTable, QWidget *parent = 0);
	virtual ~ISExchangeRatesListForm();

protected:
	void OnSelectActiveCourse(); //�������� ������� ���� ��� ��������
	void OnHighlightActiveCourse(); //�������� �������� ����
	QString OnGetDateSelectedCourse(); //�������� ���� ����������� �����
	void OnDelete();
};
//-----------------------------------------------------------------------------
