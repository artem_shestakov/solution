#include "StdAfx.h"
#include "ISBerta.h"
#include "ISDesktopBerta.h"
#include "ISRecipesListForm.h"
#include "ISExchangeRatesListForm.h"
#include "ISExchangeRatesObjectForm.h"
#include "ISRawsObjectForm.h"
#include "ISRawsListForm.h"
#include "ISLinksRecipesListForm.h"
#include "ISCostObjectListForm.h"
#include "ISCostSummaryBase.h"
#include "ISCostSummaryProduction.h"
#include "ISCostSummaryCommunications.h"
#include "ISCostSummaryTaxes.h"
#include "ISComparisonRecipesForm.h"
//-----------------------------------------------------------------------------
void OnRegisterMetaTypes()
{
	qRegisterMetaType<ISDesktopBerta*>("ISDesktopBerta");
	qRegisterMetaType<ISRecipesListForm*>("ISRecipesListForm");
	qRegisterMetaType<ISExchangeRatesListForm*>("ISExchangeRatesListForm");
	qRegisterMetaType<ISExchangeRatesObjectForm*>("ISExchangeRatesObjectForm");
	qRegisterMetaType<ISRawsObjectForm*>("ISRawsObjectForm");
	qRegisterMetaType<ISRawsListForm*>("ISRawsListForm");
	qRegisterMetaType<ISLinksRecipesListForm*>("ISLinksRecipesListForm");
	qRegisterMetaType<ISCostObjectListForm*>("ISCostObjectListForm");
	qRegisterMetaType<ISCostSummaryBase*>("ISCostSummaryBase");
	qRegisterMetaType<ISCostSummaryProduction*>("ISCostSummaryProduction");
	qRegisterMetaType<ISCostSummaryCommunications*>("ISCostSummaryCommunications");
	qRegisterMetaType<ISCostSummaryTaxes*>("ISCostSummaryTaxes");
	qRegisterMetaType<ISComparisonRecipesForm*>("ISComparisonRecipesForm");
}
//-----------------------------------------------------------------------------
QFile* OnGetFileRecipe()
{
	QFile *File = new QFile(":Resources/Recipe.xlsx");
	return File;
}
//-----------------------------------------------------------------------------
QDomElement OnGetSchema()
{
	QFile *File = new QFile(":Resources/Schema.xml");
	if (File->open(QIODevice::ReadOnly | QIODevice::Text))
	{
		QDomDocument XmlDocument;
		XmlDocument.setContent(File);
		File->close();
		QDomElement Root = XmlDocument.documentElement();
		return Root;
	}
	return QDomElement();
}
//-----------------------------------------------------------------------------
QDomElement OnGetLocalizationSchema()
{
	QFile *File = new QFile(":Resources/Localization.xml");
	if (File->open(QIODevice::ReadOnly | QIODevice::Text))
	{
		QDomDocument XmlDocument;
		XmlDocument.setContent(File);
		File->close();
		QDomElement Root = XmlDocument.documentElement();
		return Root;
	}
	return QDomElement();
}
//-----------------------------------------------------------------------------
QIcon OnGetIcon(const QString &IconName)
{
	QIcon Icon(":Resources/" + IconName + ".png");
	return Icon;
}
//-----------------------------------------------------------------------------
