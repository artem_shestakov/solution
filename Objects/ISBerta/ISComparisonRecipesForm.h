#pragma once
//-----------------------------------------------------------------------------
#include "ISInterfaceForm.h"
#include "ISSelectedListForm.h"
#include "ISCalculationsRecipe.h"
//-----------------------------------------------------------------------------
class ISComparisonRecipesForm : public ISInterfaceForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISComparisonRecipesForm(QWidget *parent = 0);
	virtual ~ISComparisonRecipesForm();

protected:
	void OnAddRecipeClicked();
	void OnAddRecipe(const int &RecipeID);
	void OnRemoveRecipe();

private:
	QHBoxLayout *LayoutRecipes;
	QVector<int> Recipes;
};
//-----------------------------------------------------------------------------
