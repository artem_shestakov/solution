#pragma once
//-----------------------------------------------------------------------------
#include "ISRateCurrent.h"
#include "ISMath.h"
//-----------------------------------------------------------------------------
//!����� ��� �������� ��������
class ISCalculationsRecipe : public QObject
{
	Q_OBJECT

public:
	ISCalculationsRecipe();
	virtual ~ISCalculationsRecipe();
	
	static double OnGetCountRaws(const int &RecipeID); //�������� ���������� ����� � ���������
	static double OnGetCountContainters(const int &RecipeID); //�������� ���������� ���� � ���������
	static double OnGetCountRawsAndContainers(const int &RecipeID); //�������� ���������� ����� � ���� � ���������
	static double OnGetCostPriceRecipe(const int &RecipeID, double Divider = 0); //�������� ������������� ���������
	static double OnGetCostPriceRecipeRaws(const int &RecipeID); //�������� ������������� ����� � ���������
	static double OnGetCostPriceContainters(const int &RecipeID); //�������� ��������� ���� � ���������
	
	static double OnGetExpenses(const int &RecipeID); //�������� �������
	static double OnGetPacking(const int &RecipeID); //�������� ����
	
	static double OnGetTotal(const int &RecipeID, const double &ExpensesValue, const double &PackingValue); //�������� �����

	static double OnGetPercentResult(const int &RecipeID, const double &Percent, const double &ExpensesValue, const double &PackingValue); //������ ���������

	static void OnReCalculateRaw(const int &RawID); //����������� �����
	static void OnReCalculateRaws(const int &RecipeID); //������������� ��� ����� �� ���������
	static void OnReCalculateResipe(const int &RecipeID); //������������ ���������
	static void OnReCalculateAllResipes(); //������������� ��� ���������

	static void OnAddCalculateToHistory(const int &RecipeID, const double &PercentDistributor, const double &PercentDealer, const double &PercentBigWholesale, const double &PercentWholesale, const double &PercentSmallWholesale, const double &PercentRetail); //�������� � ������� ������
	static void OnAddReCalculateToHistory(const int &RecipeID, const double &PercentDistributor, const double &PercentDealer, const double &PercentBigWholesale, const double &PercentWholesale, const double &PercentSmallWholesale, const double &PercentRetail); //�������� � ������� ����������
};
//-----------------------------------------------------------------------------
