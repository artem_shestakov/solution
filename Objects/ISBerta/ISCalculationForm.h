#pragma once
//-----------------------------------------------------------------------------
#include "ISCalculationsRecipe.h"
#include "ISInterfaceDialogForm.h"
#include "ISCalculationsCost.h"
#include "ISDatabaseHelper.h"
#include "ISDoubleEdit.h"
//-----------------------------------------------------------------------------
//!����� ����� �������
class ISCalculationForm : public ISInterfaceDialogForm
{
	Q_OBJECT

public:
	ISCalculationForm(const int &RecipeID, const QString &RecipeName, QWidget *RecipeListForm, QSqlRecord &Record, QWidget *parent = 0);
	virtual ~ISCalculationForm();
	
protected:
	void OnSaveAndClose(); //��������� � �������
	bool OnSave(); //���������
	void OnClearPercentFields(); //�������� ���� �������
	void OnCalculate(); //������
	void OnFillFields();

private:
	int RecipeID;
	QWidget *RecipeListForm;
	QSqlRecord Record;

	QLabel *LabelRecipeName;
	QFormLayout *FormLayout;

	ISDoubleEdit *EditDivider;

	ISDoubleEdit *EditCountRawsContainers; //���������� ����� � ����
	ISDoubleEdit *EditCountRaws; //���������� �����
	ISDoubleEdit *EditCountContainers; //���������� ����
	ISDoubleEdit *EditCostPrice; //������������� ���������
	ISDoubleEdit *EditCostPriceRaws; //������������� �����
	ISDoubleEdit *EditConstPriceContainers; //������������� ����
	ISDoubleEdit *EditExpenses; //�������
	ISDoubleEdit *EditContainer; //����
	ISDoubleEdit *EditTotal; //�����

	ISDoubleEdit *EditDistributor; //������������
	ISDoubleEdit *EditDealer; //������
	ISDoubleEdit *EditBigWholesale; //������� ���
	ISDoubleEdit *EditWholesale; //���
	ISDoubleEdit *EditSmallWholesale; //������ ���
	ISDoubleEdit *EditRetail; //�������

	ISDoubleEdit *EditPercentDistributor; //������������ (�������)
	ISDoubleEdit *EditPercentDealer; //������ (�������)
	ISDoubleEdit *EditPercentBigWholesale; //������� ��� (�������)
	ISDoubleEdit *EditPercentWholesale; //��� (�������)
	ISDoubleEdit *EditPercentSmallWholesale; //������ ��� (�������)
	ISDoubleEdit *EditPercentRetail; //������� (�������)
};
//-----------------------------------------------------------------------------
