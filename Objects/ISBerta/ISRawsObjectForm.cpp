﻿#include "StdAfx.h"
#include "ISRawsObjectForm.h"
#include "ISListBaseForm.h"
#include "EXDefines.h"
#include "ISLocalization.h"
#include "ISDoubleEdit.h"
#include "ISMessageBox.h"
#include "ISLineEditList.h"
#include "ISQuery.h"
//-----------------------------------------------------------------------------
static QString QI_PRICE_CHANGE = "INSERT INTO histrychangedrawsprice(hcrp_raw, hcrp_oldprice, hcrp_newprice, hcrp_index, hcrp_difference) "
								 "VALUES(:Raw, :OldPrice, :NewPrice, :Index, :Difference)";
//-----------------------------------------------------------------------------
ISRawsObjectForm::ISRawsObjectForm(ObjectFormType FormType, PMetaClassTable *MetaTable, QWidget *parent) : ISObjectBaseForm(FormType, MetaTable, parent)
{
	Rate = 0;

	ControlFieldName = OnGetFieldWidget("Name");

	ControlFieldPrice = OnGetFieldWidget("Price");
	connect(ControlFieldPrice, SIGNAL(OnValueChanged(const QString &, const QVariant &)), this, SLOT(OnPriceChanged(const QString &, const QVariant &)));
	
	ControlFieldCurrency = OnGetFieldWidget("Currency");
	connect(ControlFieldCurrency, SIGNAL(OnValueChanged(const QString &, const QVariant &)), this, SLOT(OnCurrencyChanged(const QString &, const QVariant &)));
	
	ControlFieldPriceRuble = OnGetFieldWidget("PriceRuble");
	ControlFieldPriceRuble->setEnabled(false);
	
	ControlFieldUnit = OnGetFieldWidget("Unit");
	
	ControlFieldLink = OnGetFieldWidget("Link");
	
	ControlFieldNote = OnGetFieldWidget("Note");
}
//-----------------------------------------------------------------------------
ISRawsObjectForm::~ISRawsObjectForm()
{

}
//-----------------------------------------------------------------------------
void ISRawsObjectForm::showEvent(QShowEvent *e)
{
	ISObjectBaseForm::showEvent(e);
	Price = ControlFieldPrice->OnGetValue().toDouble();
}
//-----------------------------------------------------------------------------
bool ISRawsObjectForm::OnSave()
{
	if (ControlFieldPrice->OnGetValue().toInt() == 0) //Если в поле "Цена" введен ноль
	{
		QMetaObject::invokeMethod(ControlFieldPrice, "OnBorderRed");
		ISMessageBox::OnWarning(this, LOCALIZATION("Message.PriceNotNull"));
		return false;
	}

	ObjectFormType FormType = OnGetCurrentFormType();
	bool Saved = ISObjectForm::OnSave();

	if (Saved && FormType == ObjectFormType::FormType_Edit)
	{
		double NewPrice = ControlFieldPrice->OnGetValue().toDouble();
		
		if (Price != NewPrice)
		{
			QString Difference = STRING_VALUE_EMPTY;
			double PriceChange = NULL;
			int Index = NULL;

			if (NewPrice > Price)
			{
				PriceChange = NewPrice - Price;
				Index = 1;
			}
			else
			{
				PriceChange = Price - NewPrice;
				Index = 2;
			}
			Difference = QString::number(PriceChange);

			ISQuery qInsertPriceChange(QI_PRICE_CHANGE);
			qInsertPriceChange.OnBindValue(":Raw", ObjectID);
			qInsertPriceChange.OnBindValue(":OldPrice", Price);
			qInsertPriceChange.OnBindValue(":NewPrice", ControlFieldPrice->OnGetValue());
			qInsertPriceChange.OnBindValue(":Index", Index);
			qInsertPriceChange.OnBindValue(":Difference", Difference);
			if (qInsertPriceChange.OnExecute())
			{
				Price = NewPrice;
			}
		}
	}

	return Saved;
}
//-----------------------------------------------------------------------------
void ISRawsObjectForm::OnCurrencyChanged(const QString &FieldName, const QVariant &Value)
{
	int ObjectID = dynamic_cast<ISLineEditList*>(ControlFieldCurrency)->OnGetObjectID().toInt();
	Rate = ObjectID;

	if (ObjectID == 1) //Если выбрана валюта рубли
	{
		dynamic_cast<ISDoubleEdit*>(ControlFieldPriceRuble)->OnSetValue(ControlFieldPrice->OnGetValue().toDouble());
	}
	else if (ObjectID == 2) //Если выбрана валюта доллары
	{
		double PriceRuble = ControlFieldPrice->OnGetValue().toDouble() * ISRateCurrent::OnGetDollar();
		dynamic_cast<ISDoubleEdit*>(ControlFieldPriceRuble)->OnSetValue(PriceRuble);
	}
	else if (ObjectID == 3) //Если выбрана валюта евро
	{
		double PriceRuble = ControlFieldPrice->OnGetValue().toDouble() * ISRateCurrent::OnGetEuro();
		dynamic_cast<ISDoubleEdit*>(ControlFieldPriceRuble)->OnSetValue(PriceRuble);
	}
	else if (ObjectID == 4) //Если выбрана валюта фунты
	{
		double PriceRuble = ControlFieldPrice->OnGetValue().toDouble() * ISRateCurrent::OnGetPounds();
		dynamic_cast<ISDoubleEdit*>(ControlFieldPriceRuble)->OnSetValue(PriceRuble);
	}
}
//-----------------------------------------------------------------------------
void ISRawsObjectForm::OnPriceChanged(const QString &FieldName, const QVariant &Value)
{
	ISDoubleEdit *EditPrice = dynamic_cast<ISDoubleEdit*>(ControlFieldPrice);
	ISDoubleEdit *EditPriceRuble = dynamic_cast<ISDoubleEdit*>(ControlFieldPriceRuble);

	switch (Rate)
	{
	case 1: EditPriceRuble->OnSetValue(Value.toDouble()); break;
	case 2: EditPriceRuble->OnSetValue(ControlFieldPrice->OnGetValue().toDouble() * ISRateCurrent::OnGetDollar()); break;
	case 3: EditPriceRuble->OnSetValue(ControlFieldPrice->OnGetValue().toDouble() * ISRateCurrent::OnGetEuro()); break;
	case 4: EditPriceRuble->OnSetValue(ControlFieldPrice->OnGetValue().toDouble() * ISRateCurrent::OnGetPounds()); break;
	}

	if (EditPrice->OnGetValue().toString().length() == 0)
	{
		//EditPriceRuble->OnClearText();
	}
}
//-----------------------------------------------------------------------------
