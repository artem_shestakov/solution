#pragma once
//-----------------------------------------------------------------------------
#include <QtCore/QObject>
#include <QtCore/QDate>
//-----------------------------------------------------------------------------
struct RateCurrentStruct
{
	double Dollar;
	double Euro;
	double Pounds;
	QDate Date;
};
//-----------------------------------------------------------------------------
class ISRateCurrent : public QObject
{
	Q_OBJECT

public:
	ISRateCurrent();
	virtual ~ISRateCurrent();

	static void OnSetDollar(const double &Dollar);
	static double OnGetDollar();

	static void OnSetEuro(const double &Euro);
	static double OnGetEuro();

	static void OnSetPounds(const double &Pounds);
	static double OnGetPounds();

	static void OnSetDate(const QDate &Date);
	static QDate OnGetDate();

private:
	static RateCurrentStruct CurrentStruct;
};
//-----------------------------------------------------------------------------
