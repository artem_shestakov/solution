#pragma once
//-----------------------------------------------------------------------------
#include "ISListObjectForm.h"
//-----------------------------------------------------------------------------
class ISLinksRecipesListForm : public ISListObjectForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISLinksRecipesListForm(PMetaClassTable *MetaTable, int ParentObjectID, QWidget *parent = 0);
	virtual ~ISLinksRecipesListForm();

	void OnCreate();

protected:
	void OnOpenLink();

protected slots:
	void OnOpenLinkSlot(const QModelIndex &ModelIndex);
};
//-----------------------------------------------------------------------------
