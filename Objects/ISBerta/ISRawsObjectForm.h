#pragma once
//-----------------------------------------------------------------------------
#include "ISObjectBaseForm.h"
#include "ISRateCurrent.h"
//-----------------------------------------------------------------------------
class ISRawsObjectForm : public ISObjectBaseForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISRawsObjectForm(ObjectFormType FormType, PMetaClassTable *MetaTable, QWidget *parent = 0);
	virtual ~ISRawsObjectForm();
	virtual void showEvent(QShowEvent *e);

protected:
	bool OnSave();

protected slots:
	void OnCurrencyChanged(const QString &FieldName, const QVariant &Value);
	void OnPriceChanged(const QString &FieldName, const QVariant &Value);

private:
	ISFieldEditBase *ControlFieldName; //���� ������������
	ISFieldEditBase *ControlFieldPrice; //���� ����
	ISFieldEditBase *ControlFieldCurrency; //���� ������
	ISFieldEditBase *ControlFieldPriceRuble; //���� ���� � ������
	ISFieldEditBase *ControlFieldUnit; //���� ������� ���������
	ISFieldEditBase *ControlFieldLink; //���� ������
	ISFieldEditBase *ControlFieldNote; //���� ����������

	int Rate;
	double Price = NULL;
};
//-----------------------------------------------------------------------------
