#include "StdAfx.h"
#include "ISRecipesListForm.h"
#include "ISInterfaceGUI.h"
#include "EXDefines.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISMessageBox.h"
#include "ISSystem.h"
#include "ISMetaData.h"
#include "ISConfig.h"
#include "ISModule.h"
#include "ISQuery.h"
//-----------------------------------------------------------------------------
static QString QS_RAWS_PRICE = "SELECT srln_raw FROM recipesrawslink WHERE NOT srln_isdeleted AND srln_recipes = :RecipeID";
//-----------------------------------------------------------------------------
static QString QS_RECIPE_CACLULATE_FIELDS = "SELECT rcpe_distributor, rcpe_dealer, rcpe_wholesale, rcpe_smallwholesale, rcpe_bigwholesale, rcpe_retail, rcpe_percentdistributor, rcpe_percentdealer, rcpe_percentwholesale, rcpe_percentsmallwholesale, rcpe_percentbigwholesale, rcpe_percentretail "
											"FROM recipes "
											"WHERE rcpe_id = :RecipeID";
//-----------------------------------------------------------------------------
static QString QS_RAWS_ALT = "SELECT "
							 "r.raws_id AS \"%ID%\", "
							 "r.raws_name AS \"%Name%\", "
							 "r.raws_price AS \"%Price%\", "
							 "c.crcy_name AS \"%Currency%\", "
							 "r.raws_priceruble AS \"%PriceRuble%\", "
							 "u.unts_name AS \"%Unit%\", "
							 "rrl.srln_count AS \"%Count%\", "
							 "r.raws_priceruble * rrl.srln_count AS \"%Sum%\" "
							 "FROM raws r "
							 "LEFT JOIN currency c ON c.crcy_id = r.raws_currency "
							 "LEFT JOIN units u ON u.unts_id = r.raws_unit "
							 "LEFT JOIN recipesrawslink rrl ON rrl.srln_raw = r.raws_id AND rrl.srln_recipes = :RecipeID "
							 "WHERE r.raws_id IN(SELECT srln_raw FROM recipesrawslink WHERE NOT srln_isdeleted AND srln_recipes = :RecipeID) "
							 "AND NOT r.raws_isdeleted "
							 "ORDER BY r.raws_name";
//-----------------------------------------------------------------------------
ISRecipesListForm::ISRecipesListForm(PMetaClassTable *MetaTable, QWidget *parent) : ISListBaseForm(MetaTable, parent)
{
	this->ShowingRightPanel = true;
	this->ShowingCalculateWidget = true;
	
	connect(this, SIGNAL(SelectedRow()), this, SLOT(OnSelectedRecord()));

	QAction *ActionCalculate = new QAction(OnGetTableViewBase());
	ActionCalculate->setText(LOCALIZATION("Calculation"));
	ActionCalculate->setToolTip(LOCALIZATION("Calculation"));
	ActionCalculate->setIcon(ISModule::OnGetModule().OnGetIcon("Calculation"));
	connect(ActionCalculate, &QAction::triggered, this, &ISRecipesListForm::OnShowCalculationForm);
	OnAddAction(ActionCalculate);

	QAction *ActionReCalculate = new QAction(OnGetTableViewBase());
	ActionReCalculate->setText(LOCALIZATION("ReCalculate"));
	ActionReCalculate->setToolTip(LOCALIZATION("ReCalculate"));
	ActionReCalculate->setIcon(ISModule::OnGetModule().OnGetIcon("ReCalculation"));
	connect(ActionReCalculate, &QAction::triggered, this, &ISRecipesListForm::OnReCalculateCurrent);
	OnAddAction(ActionReCalculate);

	QAction *ActionReCalculateAll = new QAction(OnGetTableViewBase());
	ActionReCalculateAll->setText(LOCALIZATION("ReCalculateAllRecipes"));
	ActionReCalculateAll->setToolTip(LOCALIZATION("ReCalculateAllRecipes"));
	ActionReCalculateAll->setIcon(ISModule::OnGetModule().OnGetIcon("ReCalculationAllRecipes"));
	connect(ActionReCalculateAll, &QAction::triggered, this, &ISRecipesListForm::OnReCalculateAll);
	OnAddAction(ActionReCalculateAll, false);

	QAction *ActionPrintRecipe = new QAction(OnGetTableViewBase());
	ActionPrintRecipe->setText(LOCALIZATION("PrintRecipe"));
	ActionPrintRecipe->setToolTip(LOCALIZATION("PrintRecipe"));
	ActionPrintRecipe->setIcon(BUFFER_ICONS("Print"));
	connect(ActionPrintRecipe, &QAction::triggered, this, &ISRecipesListForm::OnPrintRecipe);
	OnAddAction(ActionPrintRecipe);

	QAction *ActionCalculateHistory = new QAction(OnGetTableViewBase());
	ActionCalculateHistory->setText(LOCALIZATION("CalculateHistory"));
	ActionCalculateHistory->setToolTip(LOCALIZATION("CalculateHistory"));
	ActionCalculateHistory->setIcon(ISModule::OnGetModule().OnGetIcon("RecipesHistory"));
	connect(ActionCalculateHistory, &QAction::triggered, this, &ISRecipesListForm::OnShowCalculateHistory);
	OnAddAction(ActionCalculateHistory);

	Layout = new QHBoxLayout(); //������� ����������� ���� ����������
	OnGetMainLayout()->addLayout(Layout);

	WidgetBottomPanel = new QWidget(this);
	WidgetBottomPanel->setLayout(new QVBoxLayout());
	OnGetMainLayout()->addWidget(WidgetBottomPanel);

	QFont Font("Arial", 12, QFont::Bold);
	Font.setUnderline(true);

	QLabel *LabelRaws = new QLabel(WidgetBottomPanel);
	LabelRaws->setText(LOCALIZATION("Raws") + ":");
	LabelRaws->setFont(Font);
	dynamic_cast<QVBoxLayout*>(WidgetBottomPanel->layout())->addWidget(LabelRaws, 0, Qt::AlignLeft);

	TableViewRaws = new ISBaseTableView(this);
	WidgetBottomPanel->layout()->addWidget(TableViewRaws);

	OnCreateBottomPanel();

	QS_RAWS_ALT.replace("%ID%", LOCALIZATION("ID"));
	QS_RAWS_ALT.replace("%Name%", LOCALIZATION("Named"));
	QS_RAWS_ALT.replace("%Price%", LOCALIZATION("Price"));
	QS_RAWS_ALT.replace("%Currency%", LOCALIZATION("Currency")); 
	QS_RAWS_ALT.replace("%PriceRuble%", LOCALIZATION("PriceRuble"));
	QS_RAWS_ALT.replace("%Unit%", LOCALIZATION("Unit"));
	QS_RAWS_ALT.replace("%Count%", LOCALIZATION("Count"));
	QS_RAWS_ALT.replace("%Sum%", LOCALIZATION("Sum"));
	QuerySelectRaws = QS_RAWS_ALT;
}
//-----------------------------------------------------------------------------
ISRecipesListForm::~ISRecipesListForm()
{
	
}
//-----------------------------------------------------------------------------
void ISRecipesListForm::OnCreateBottomPanel()
{
	QWidget *WidgetBottom = new QWidget(this);
	WidgetBottom->setSizePolicy(QSizePolicy::Policy::Expanding, QSizePolicy::Policy::Maximum);
	WidgetBottom->setLayout(new QHBoxLayout());
	layout()->addWidget(WidgetBottom);

	//���������� ����� � ����
	WidgetBottom->layout()->addWidget(new QLabel(LOCALIZATION("CountRawsAndContainers") + ":", this));
	EditCountRawsContainers = new ISDoubleEdit(nullptr, this);
	EditCountRawsContainers->OnSetReadOnly(true);
	WidgetBottom->layout()->addWidget(EditCountRawsContainers);
	WidgetBottom->layout()->addWidget(ISInterfaceGUI::OnGetCreateVerticalLine(WidgetBottom));

	//���������� �����
	WidgetBottom->layout()->addWidget(new QLabel(LOCALIZATION("CountRaws") + ":", this));
	EditCountRaws = new ISDoubleEdit(nullptr, this);
	EditCountRaws->OnSetReadOnly(true);
	WidgetBottom->layout()->addWidget(EditCountRaws);
	WidgetBottom->layout()->addWidget(ISInterfaceGUI::OnGetCreateVerticalLine(WidgetBottom));

	//���������� ����
	WidgetBottom->layout()->addWidget(new QLabel(LOCALIZATION("CountContainters") + ":", this));
	EditCountContainers = new ISDoubleEdit(nullptr, this);
	EditCountContainers->OnSetReadOnly(true);
	WidgetBottom->layout()->addWidget(EditCountContainers);
	WidgetBottom->layout()->addWidget(ISInterfaceGUI::OnGetCreateVerticalLine(WidgetBottom));

	//������������� ���������
	WidgetBottom->layout()->addWidget(new QLabel(LOCALIZATION("CostPriceRecipe") + ":", this));
	EditCostPrice = new ISDoubleEdit(nullptr, this);
	EditCostPrice->OnSetReadOnly(true);
	WidgetBottom->layout()->addWidget(EditCostPrice);
	WidgetBottom->layout()->addWidget(ISInterfaceGUI::OnGetCreateVerticalLine(WidgetBottom));

	//������������� �����
	WidgetBottom->layout()->addWidget(new QLabel(LOCALIZATION("CostPriceRaws") + ":", this));
	EditCostPriceRaws = new ISDoubleEdit(nullptr, this);
	EditCostPriceRaws->OnSetReadOnly(true);
	WidgetBottom->layout()->addWidget(EditCostPriceRaws);
	WidgetBottom->layout()->addWidget(ISInterfaceGUI::OnGetCreateVerticalLine(WidgetBottom));

	//������������� ����
	WidgetBottom->layout()->addWidget(new QLabel(LOCALIZATION("CostPriceContainters") + ":", this));
	EditConstPriceContainers = new ISDoubleEdit(nullptr, this);
	EditConstPriceContainers->OnSetReadOnly(true);
	WidgetBottom->layout()->addWidget(EditConstPriceContainers);
	WidgetBottom->layout()->addWidget(ISInterfaceGUI::OnGetCreateVerticalLine(WidgetBottom));

	//�����
	WidgetBottom->layout()->addWidget(new QLabel(LOCALIZATION("Total") + ":", this));
	EditTotal = new ISDoubleEdit(nullptr, this);
	EditTotal->OnSetReadOnly(true);
	WidgetBottom->layout()->addWidget(EditTotal);
	WidgetBottom->layout()->addWidget(ISInterfaceGUI::OnGetCreateVerticalLine(WidgetBottom));

	ButtonShowHideTableRaws = new QToolButton(WidgetBottom);
	ButtonShowHideTableRaws->setToolTip(LOCALIZATION("HideListRaws"));
	ButtonShowHideTableRaws->setIcon(ISModule::OnGetModule().OnGetIcon("SubSystemRaws"));
	ButtonShowHideTableRaws->setCheckable(true);
	ButtonShowHideTableRaws->setChecked(true);
	ButtonShowHideTableRaws->setAutoRaise(true);
	connect(ButtonShowHideTableRaws, &QToolButton::clicked, this, &ISRecipesListForm::OnShowHideRigthPanel);
	WidgetBottom->layout()->addWidget(ButtonShowHideTableRaws);
}
//-----------------------------------------------------------------------------
void ISRecipesListForm::OnShowCalculationForm()
{
	if (TableViewRaws->model()->rowCount()) //��������� ����� ������� ������ ��� ������� ����� � ��������� ���������
	{
		ISCalculationForm *CalculationForm = new ISCalculationForm(OnGetObjectID(), OnGetValueOfCurrentRecord("Name").toString(), this, OnGetCurrentRecord(), this);
		CalculationForm->setWindowTitle(LOCALIZATION("CalculationForRecipe").arg(OnGetValueOfCurrentRecord("Name").toString()));
		CalculationForm->OnExec();
	}
	else
	{
		ISMessageBox::OnCritical(this, LOCALIZATION("Message.RawsInRecipeIsNull"));
	}
}
//-----------------------------------------------------------------------------
void ISRecipesListForm::OnReCalculateCurrent()
{
	if (ISMessageBox::OnQuestion(this, LOCALIZATION("Message.ReCalculateRecipe")))
	{
		ISCalculationsRecipe::OnReCalculateResipe(OnGetObjectID());
		OnUpdate();
	}
}
//-----------------------------------------------------------------------------
void ISRecipesListForm::OnReCalculateAll()
{
	if (ISMessageBox::OnQuestion(this, LOCALIZATION("Message.ReCalculateAllRecipes")))
	{
		ISCalculationsRecipe::OnReCalculateAllResipes();
		OnUpdate();
	}
}
//-----------------------------------------------------------------------------
void ISRecipesListForm::OnShowHideRigthPanel()
{
	if (ShowingRightPanel)
	{
		ButtonShowHideTableRaws->setToolTip(LOCALIZATION("ShowListRaws"));
		ButtonShowHideTableRaws->setChecked(false);
		WidgetBottomPanel->hide();
		ShowingRightPanel = false;
	}
	else
	{
		ButtonShowHideTableRaws->setToolTip(LOCALIZATION("HideListRaws"));
		ButtonShowHideTableRaws->setChecked(true);
		WidgetBottomPanel->show();
		ShowingRightPanel = true;
	}
}
//-----------------------------------------------------------------------------
void ISRecipesListForm::OnSelectedRecord()
{
	QString RawsID = STRING_VALUE_EMPTY;

	ISQuery qSelectRawsID(QS_RAWS_PRICE);
	qSelectRawsID.OnBindValue(":RecipeID", OnGetObjectID());
	if (qSelectRawsID.OnExecute())
	{
		while (qSelectRawsID.OnNext())
		{
			RawsID += qSelectRawsID.OnReadColumn("srln_raw").toString() + ", ";
		}

		//RawsID.remove(RawsID.length() - 2, 2);
		ISSystem::OnRemoveLastSymbolFromString(RawsID, 2);

		QString QueryText = QuerySelectRaws;
		TableViewRaws->OnRequery(QueryText.replace(":RecipeID", QString::number(OnGetObjectID())));
		OnFillBottomFields();
	}
}
//-----------------------------------------------------------------------------
void ISRecipesListForm::OnUpdate()
{
	ISListBaseForm::OnUpdate();
	TableViewRaws->OnRequery(STRING_VALUE_EMPTY);
	
	OnClearBottomFields();
}
//-----------------------------------------------------------------------------
void ISRecipesListForm::OnFillBottomFields()
{
	int RecipeID = OnGetObjectID();
	double CountRaws = ISCalculationsRecipe::OnGetCountRaws(RecipeID);

	if (CountRaws)
	{
		double CountRawsAndContainers = ISCalculationsRecipe::OnGetCountRawsAndContainers(RecipeID);
		double CountContainers = ISCalculationsRecipe::OnGetCountContainters(RecipeID);
		double CostPriceRecipe = ISCalculationsRecipe::OnGetCostPriceRecipe(RecipeID);
		double CostPriceRaws = ISCalculationsRecipe::OnGetCostPriceRecipeRaws(RecipeID);
		double CostPriceContaioners = ISCalculationsRecipe::OnGetCostPriceContainters(RecipeID);
		double Total = CostPriceRecipe;

		EditCountRawsContainers->OnSetValue(CountRawsAndContainers);
		EditCountRaws->OnSetValue(CountRaws);
		EditCountContainers->OnSetValue(CountContainers);
		EditCostPrice->OnSetValue(CostPriceRecipe);
		EditCostPriceRaws->OnSetValue(CostPriceRaws);
		EditConstPriceContainers->OnSetValue(CostPriceContaioners);
		EditTotal->OnSetValue(Total);
	}
	else
	{
		OnClearBottomFields();
	}
}
//-----------------------------------------------------------------------------
void ISRecipesListForm::OnClearBottomFields()
{
	//EditCountRawsContainers->OnClearText();
	//EditCountRaws->OnClearText();
	//EditCountContainers->OnClearText();
	//EditCostPrice->OnClearText();
	//EditCostPriceRaws->OnClearText();
	//EditConstPriceContainers->OnClearText();
	//EditTotal->OnClearText();
}
//-----------------------------------------------------------------------------
void ISRecipesListForm::OnPrintRecipe()
{
	/*ISProcessForm *ProcessForm = new ISProcessForm();
	ProcessForm->show();
	ISSystem::OnRepaintWidget(ProcessForm);

	QLibrary LibraryModule(MODULE_CURRENT_PATH);
	typedef void(*Printing) (const int &);
	Printing Function = (Printing)(LibraryModule.resolve("OnPrintRecipe"));
	if (Function) 
	{
		Function(OnGetObjectID());
	}

	delete ProcessForm;
	ProcessForm = nullptr;*/
}
//-----------------------------------------------------------------------------
void ISRecipesListForm::OnShowCalculateHistory()
{
	ISListBaseForm *ListBaseForm = new ISListBaseForm(ISMetaData::OnGetData().OnGetTable("HistoryCalculatesRecipes"), nullptr);
	ListBaseForm->setWindowTitle(LOCALIZATION("CalculateHistory"));
	ListBaseForm->setWindowIcon(ISModule::OnGetModule().OnGetIcon("RecipesHistory"));
	ListBaseForm->layout()->setContentsMargins(LAYOUT_MARGINS_10_PX);
	ListBaseForm->OnGetQueryModel()->OnSetClassFilter("hcrs_recipe = " + QString::number(OnGetObjectID()));
	ListBaseForm->OnLoadData();
	ListBaseForm->show();
}
//-----------------------------------------------------------------------------
