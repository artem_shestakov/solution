#pragma once
//-----------------------------------------------------------------------------
#include "ISCostSummaryBase.h"
//-----------------------------------------------------------------------------
class ISCostSummaryProduction : public ISCostSummaryBase
{
	Q_OBJECT

public:
	Q_INVOKABLE ISCostSummaryProduction(QWidget *parent = 0);
	virtual ~ISCostSummaryProduction();
};
//-----------------------------------------------------------------------------
