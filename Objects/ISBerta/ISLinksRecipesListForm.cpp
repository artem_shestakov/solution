#include "StdAfx.h"
#include "ISLinksRecipesListForm.h"
#include "ISSystem.h"
#include "ISLocalization.h"
#include "ISQuery.h"
#include "ISModule.h"
#include "ISFileDialog.h"
//-----------------------------------------------------------------------------
static QString QI_LINK = "INSERT INTO linksrecipes(lkrp_recipe, lkrp_link) "
						 "VALUES(:RecipeID, :Link)";
//-----------------------------------------------------------------------------
ISLinksRecipesListForm::ISLinksRecipesListForm(PMetaClassTable *MetaTable, int ParentObjectID, QWidget *parent) : ISListObjectForm(MetaTable, ParentObjectID, parent)
{
	QAction *ActionOpenLink = new QAction(OnGetTableViewBase());
	ActionOpenLink->setText(LOCALIZATION("OpenLink"));
	ActionOpenLink->setToolTip(LOCALIZATION("OpenLink"));
	ActionOpenLink->setIcon(ISModule::OnGetModule().OnGetIcon("OpenLink"));
	ActionOpenLink->setPriority(QAction::LowPriority);
	connect(ActionOpenLink, &QAction::triggered, this, &ISLinksRecipesListForm::OnOpenLink);
	OnInsertAction(ActionOpenLink, OnGetTableViewBase()->OnGetAction(TableViewAction::Action_Create), true);

	OnGetTableViewBase()->OnGetAction(TableViewAction::Action_CreateCopy)->setVisible(false);

	disconnect(OnGetTableViewBase()->OnGetTableView(), SIGNAL(doubleClicked(const QModelIndex &)), this, SLOT(OnDoubleClickedTable(const QModelIndex &)));
	connect(OnGetTableViewBase()->OnGetTableView(), SIGNAL(doubleClicked(const QModelIndex &)), this, SLOT(OnOpenLinkSlot(const QModelIndex &)));
}
//-----------------------------------------------------------------------------
ISLinksRecipesListForm::~ISLinksRecipesListForm()
{

}
//-----------------------------------------------------------------------------
void ISLinksRecipesListForm::OnCreate()
{
	QStringList FilesPath = ISFileDialog::OnStaticOpenFilesPath(this, LOCALIZATION("File.Filter.All"));

	for (int i = 0; i < FilesPath.count(); i++)
	{
		ISQuery qInsert(QI_LINK);
		qInsert.OnBindValue(":RecipeID", OnGetParentObjectID());
		qInsert.OnBindValue(":Link", FilesPath[i]);
		qInsert.OnExecute();
	}

	OnUpdate();
}
//-----------------------------------------------------------------------------
void ISLinksRecipesListForm::OnOpenLink()
{
	ISSystem::OnOpenFile(OnGetTableViewBase(), OnGetValueOfCurrentRecord("Link").toString());
}
//-----------------------------------------------------------------------------
void ISLinksRecipesListForm::OnOpenLinkSlot(const QModelIndex &ModelIndex)
{
	OnOpenLink();
}
//-----------------------------------------------------------------------------
