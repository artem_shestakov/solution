#pragma once
//-----------------------------------------------------------------------------
#include "ISListObjectForm.h"
#include "ISDoubleEdit.h"
//-----------------------------------------------------------------------------
class ISCostObjectListForm : public ISListObjectForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISCostObjectListForm(PMetaClassTable *MetaTable, int ParentObjectID, QWidget *parent = 0);
	virtual ~ISCostObjectListForm();

protected:
	void OnCreateBottomFields(); //������� ������ � ������� �� �������
	void OnCreateQuerySum();
	virtual void OnLoadDataAfterEvent();

private:
	ISBaseTableView *TableViewSum;
	QWidget *PanelFields;
	QHBoxLayout *LayutPanelBottom;

	ISDoubleEdit *EditJanuary;
	ISDoubleEdit *EditFebruary;
	ISDoubleEdit *EditMarch;
	ISDoubleEdit *EditApril;
	ISDoubleEdit *EditMay;
	ISDoubleEdit *EditJune;
	ISDoubleEdit *EditJuly;
	ISDoubleEdit *EditAugust;
	ISDoubleEdit *EditSeptember;
	ISDoubleEdit *EditOctober;
	ISDoubleEdit *EditNovember;
	ISDoubleEdit *EditDecember;
	
	QString QuerySumSelect;
};
//-----------------------------------------------------------------------------
