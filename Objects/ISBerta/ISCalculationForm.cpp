#include "StdAfx.h"
#include "ISCalculationForm.h"
#include "ISRecipesListForm.h"
#include "ISInterfaceGUI.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISMetaData.h"
#include "EXDefines.h"
#include "ISModule.h"
#include "ISQuery.h"
//-----------------------------------------------------------------------------
static QString QU_RECIPE = "UPDATE recipes SET "
						   "rcpe_costprice = :CostPrice, "
						   "rcpe_expenses = :Expenses, "
						   "rcpe_packing = :Containers, "
						   "rcpe_total = :Total, "
						   "rcpe_distributor = :Distributor, "
						   "rcpe_dealer = :Dealer, "
						   "rcpe_bigwholesale = :BigWholesale, "
						   "rcpe_wholesale = :Wholesale, "
						   "rcpe_smallwholesale = :SmallWholesale, "
						   "rcpe_retail = :Retail, "
						   "rcpe_percentdistributor = :PercentDistributor, "
						   "rcpe_percentdealer = :PercentDealer, "
						   "rcpe_percentbigwholesale = :PercentBigWholesale, "
						   "rcpe_percentwholesale = :PercentWholesale, "
						   "rcpe_percentsmallwholesale = :PercentSmallWholesale, "
						   "rcpe_percentretail = :PercentRetail "
						   "WHERE rcpe_id = :RecipeID";
//-----------------------------------------------------------------------------
ISCalculationForm::ISCalculationForm(const int &RecipeID, const QString &RecipeName, QWidget *RecipeListForm, QSqlRecord &Record, QWidget *parent) : ISInterfaceDialogForm(parent)
{
	this->RecipeID = RecipeID;
	this->RecipeListForm = RecipeListForm;
	this->Record = Record;

	OnGetMainLayout()->setContentsMargins(LAYOUT_MARGINS_10_PX);

	LabelRecipeName = new QLabel(this);
	LabelRecipeName->setFont(FONT_ARIAL_12_BOLD);
	LabelRecipeName->setText(LOCALIZATION("Recipe") + ": " + RecipeName);
	OnGetMainLayout()->addWidget(LabelRecipeName);

	QToolBar *ToolBar = new QToolBar(this);
	ToolBar->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
	OnGetMainLayout()->addWidget(ToolBar);

	OnGetMainLayout()->addWidget(ISInterfaceGUI::OnGetCreateHorizontalLine(this));

	QAction *ActionSaveAndClose = ISInterfaceGUI::OnGetActionSaveAndClose(ToolBar);
	connect(ActionSaveAndClose, &QAction::triggered, this, &ISCalculationForm::OnSaveAndClose);
	ToolBar->addAction(ActionSaveAndClose);

	QAction *ActionSave = ISInterfaceGUI::OnGetActionSave(ToolBar);
	connect(ActionSave, &QAction::triggered, this, &ISCalculationForm::OnSave);
	ToolBar->addAction(ActionSave);

	QAction *ActionClearPercentFields = new QAction(ToolBar);
	ActionClearPercentFields->setText(LOCALIZATION("ClearPercentFields"));
	ActionClearPercentFields->setToolTip(LOCALIZATION("ClearPercentFields"));
	ActionClearPercentFields->setIcon(BUFFER_ICONS("Clear"));
	connect(ActionClearPercentFields, &QAction::triggered, this, &ISCalculationForm::OnClearPercentFields);
	ToolBar->addAction(ActionClearPercentFields);

	QAction *ActionCaclulate = new QAction(ToolBar);
	ActionCaclulate->setText(LOCALIZATION("Calculating"));
	ActionCaclulate->setToolTip(LOCALIZATION("Calculating"));
	ActionCaclulate->setIcon(ISModule::OnGetModule().OnGetIcon("Calculation"));
	connect(ActionCaclulate, &QAction::triggered, this, &ISCalculationForm::OnCalculate);
	ToolBar->addAction(ActionCaclulate);

	FormLayout = new QFormLayout();
	OnGetMainLayout()->addLayout(FormLayout);
	OnGetMainLayout()->addStretch();

	EditDivider = new ISDoubleEdit(nullptr, this);
	FormLayout->addRow(LOCALIZATION("Divider") + ":", EditDivider);

	//���������� ����� � ����
	{
		QLabel *Label = new QLabel(LOCALIZATION("CountRawsAndContainers") + ":", this);
		Label->setToolTip(LOCALIZATION("ThisFieldAutoFill"));
		Label->setCursor(CURSOR_WHATS_THIS);
		Label->setFont(QFont("Arial", Label->font().pointSize(), QFont::Bold));

		EditCountRawsContainers = new ISDoubleEdit(nullptr, this);
		EditCountRawsContainers->OnSetReadOnly(true);
		FormLayout->addRow(Label, EditCountRawsContainers);
	}

	//���������� �����
	{
		QLabel *Label = new QLabel(LOCALIZATION("CountRaws") + ":", this);
		Label->setToolTip(LOCALIZATION("ThisFieldAutoFill"));
		Label->setCursor(CURSOR_WHATS_THIS);
		Label->setFont(QFont("Arial", Label->font().pointSize(), QFont::Bold));

		EditCountRaws = new ISDoubleEdit(nullptr, this);
		EditCountRaws->OnSetReadOnly(true);
		FormLayout->addRow(Label, EditCountRaws);
	}

	//���������� ����
	{
		QLabel *Label = new QLabel(LOCALIZATION("CountContainters") + ":", this);
		Label->setToolTip(LOCALIZATION("ThisFieldAutoFill"));
		Label->setCursor(CURSOR_WHATS_THIS);
		Label->setFont(QFont("Arial", Label->font().pointSize(), QFont::Bold));

		EditCountContainers = new ISDoubleEdit(nullptr, this);
		EditCountContainers->OnSetReadOnly(true);
		FormLayout->addRow(Label, EditCountContainers);
	}

	//������������� ���������
	{
		QLabel *Label = new QLabel(LOCALIZATION("CostPriceRecipe") + ":", this);
		Label->setToolTip(LOCALIZATION("ThisFieldAutoFill"));
		Label->setCursor(CURSOR_WHATS_THIS);
		Label->setFont(QFont("Arial", Label->font().pointSize(), QFont::Bold));

		EditCostPrice = new ISDoubleEdit(nullptr, this);
		EditCostPrice->OnSetReadOnly(true);
		FormLayout->addRow(Label, EditCostPrice);
	}

	//������������� �����
	{
		QLabel *Label = new QLabel(LOCALIZATION("CostPriceRaws") + ":", this);
		Label->setToolTip(LOCALIZATION("ThisFieldAutoFill"));
		Label->setCursor(CURSOR_WHATS_THIS);
		Label->setFont(QFont("Arial", Label->font().pointSize(), QFont::Bold));

		EditCostPriceRaws = new ISDoubleEdit(nullptr, this);
		EditCostPriceRaws->OnSetReadOnly(true);
		FormLayout->addRow(Label, EditCostPriceRaws);
	}

	//������������� ����
	{
		QLabel *Label = new QLabel(LOCALIZATION("CostPriceContainters") + ":", this);
		Label->setToolTip(LOCALIZATION("ThisFieldAutoFill"));
		Label->setCursor(CURSOR_WHATS_THIS);
		Label->setFont(QFont("Arial", Label->font().pointSize(), QFont::Bold));

		EditConstPriceContainers = new ISDoubleEdit(nullptr, this);
		EditConstPriceContainers->OnSetReadOnly(true);
		FormLayout->addRow(Label, EditConstPriceContainers);
	}

	EditExpenses = new ISDoubleEdit(nullptr, this);
	FormLayout->addRow(LOCALIZATION("Expenses") + ":", EditExpenses);

	EditContainer = new ISDoubleEdit(nullptr, this);
	FormLayout->addRow(LOCALIZATION("Container") + ":", EditContainer);

	//�����
	{
		QLabel *Label = new QLabel(LOCALIZATION("Total") + ":", this);
		Label->setToolTip(LOCALIZATION("ThisFieldAutoFill"));
		Label->setCursor(CURSOR_WHATS_THIS);
		Label->setFont(QFont("Arial", Label->font().pointSize(), QFont::Bold));

		EditTotal = new ISDoubleEdit(nullptr, this);
		EditTotal->OnSetReadOnly(true);
		FormLayout->addRow(Label, EditTotal);
	}

	//������������
	{
		QLabel *Label = new QLabel(LOCALIZATION("Distributor") + ":", this);
		Label->setToolTip(LOCALIZATION("ThisFieldAutoFill"));
		Label->setCursor(CURSOR_WHATS_THIS);
		Label->setFont(QFont("Arial", Label->font().pointSize(), QFont::Bold));

		EditDistributor = new ISDoubleEdit(nullptr, this);
		EditDistributor->OnSetReadOnly(true);
		FormLayout->addRow(Label, EditDistributor);
	}

	//������
	{
		QLabel *Label = new QLabel(LOCALIZATION("Dealer") + ":", this);
		Label->setToolTip(LOCALIZATION("ThisFieldAutoFill"));
		Label->setCursor(CURSOR_WHATS_THIS);
		Label->setFont(QFont("Arial", Label->font().pointSize(), QFont::Bold));

		EditDealer = new ISDoubleEdit(nullptr, this);
		EditDealer->OnSetReadOnly(true);
		FormLayout->addRow(Label, EditDealer);
	}

	//������� ���
	{
		QLabel *Label = new QLabel(LOCALIZATION("BigWholesale") + ":", this);
		Label->setToolTip(LOCALIZATION("ThisFieldAutoFill"));
		Label->setCursor(CURSOR_WHATS_THIS);
		Label->setFont(QFont("Arial", Label->font().pointSize(), QFont::Bold));

		EditBigWholesale = new ISDoubleEdit(nullptr, this);
		EditBigWholesale->OnSetReadOnly(true);
		FormLayout->addRow(Label, EditBigWholesale);
	}

	//���
	{
		QLabel *Label = new QLabel(LOCALIZATION("Wholesale") + ":", this);
		Label->setToolTip(LOCALIZATION("ThisFieldAutoFill"));
		Label->setCursor(CURSOR_WHATS_THIS);
		Label->setFont(QFont("Arial", Label->font().pointSize(), QFont::Bold));

		EditWholesale = new ISDoubleEdit(nullptr, this);
		EditWholesale->OnSetReadOnly(true);
		FormLayout->addRow(Label, EditWholesale);
	}

	//������ ���
	{
		QLabel *Label = new QLabel(LOCALIZATION("SmallWholesale") + ":", this);
		Label->setToolTip(LOCALIZATION("ThisFieldAutoFill"));
		Label->setCursor(CURSOR_WHATS_THIS);
		Label->setFont(QFont("Arial", Label->font().pointSize(), QFont::Bold));

		EditSmallWholesale = new ISDoubleEdit(nullptr, this);
		EditSmallWholesale->OnSetReadOnly(true);
		FormLayout->addRow(Label, EditSmallWholesale);
	}

	//�������
	{
		QLabel *Label = new QLabel(LOCALIZATION("Retail") + ":", this);
		Label->setToolTip(LOCALIZATION("ThisFieldAutoFill"));
		Label->setCursor(CURSOR_WHATS_THIS);
		Label->setFont(QFont("Arial", Label->font().pointSize(), QFont::Bold));

		EditRetail = new ISDoubleEdit(nullptr, this);
		EditRetail->OnSetReadOnly(true);
		FormLayout->addRow(Label, EditRetail);
	}

	EditPercentDistributor = new ISDoubleEdit(nullptr, this);
	FormLayout->addRow(LOCALIZATION("PercentDistributor") + ":", EditPercentDistributor);

	EditPercentDealer = new ISDoubleEdit(nullptr, this);
	FormLayout->addRow(LOCALIZATION("PercentDealer") + ":", EditPercentDealer);

	EditPercentBigWholesale = new ISDoubleEdit(nullptr, this);
	FormLayout->addRow(LOCALIZATION("PercentBigWholesale") + ":", EditPercentBigWholesale);

	EditPercentWholesale = new ISDoubleEdit(nullptr, this);
	FormLayout->addRow(LOCALIZATION("PercentWholesale") + ":", EditPercentWholesale);

	EditPercentSmallWholesale = new ISDoubleEdit(nullptr, this);
	FormLayout->addRow(LOCALIZATION("PercentSmallWholesale") + ":", EditPercentSmallWholesale);

	EditPercentRetail = new ISDoubleEdit(nullptr, this);
	FormLayout->addRow(LOCALIZATION("PercentRetail") + ":", EditPercentRetail);

	OnFillFields();
}
//-----------------------------------------------------------------------------
ISCalculationForm::~ISCalculationForm()
{

}
//-----------------------------------------------------------------------------
void ISCalculationForm::OnSaveAndClose()
{
	if (OnSave())
	{
		close();
	}
}
//-----------------------------------------------------------------------------
bool ISCalculationForm::OnSave()
{
	ISQuery qUpdate(QU_RECIPE);
	qUpdate.OnBindValue(":CostPrice", EditCostPrice->OnGetValue());
	qUpdate.OnBindValue(":Expenses", EditExpenses->OnGetValue());
	qUpdate.OnBindValue(":Containers", EditContainer->OnGetValue());
	qUpdate.OnBindValue(":Total", EditTotal->OnGetValue());
	qUpdate.OnBindValue(":Distributor", EditDistributor->OnGetValue());
	qUpdate.OnBindValue(":Dealer", EditDealer->OnGetValue());
	qUpdate.OnBindValue(":BigWholesale", EditBigWholesale->OnGetValue());
	qUpdate.OnBindValue(":Wholesale", EditWholesale->OnGetValue());
	qUpdate.OnBindValue(":SmallWholesale", EditSmallWholesale->OnGetValue());
	qUpdate.OnBindValue(":Retail", EditRetail->OnGetValue());
	qUpdate.OnBindValue(":PercentDistributor", EditPercentDistributor->OnGetValue());
	qUpdate.OnBindValue(":PercentDealer", EditPercentDealer->OnGetValue());
	qUpdate.OnBindValue(":PercentBigWholesale", EditPercentBigWholesale->OnGetValue());
	qUpdate.OnBindValue(":PercentWholesale", EditPercentWholesale->OnGetValue());
	qUpdate.OnBindValue(":PercentSmallWholesale", EditPercentSmallWholesale->OnGetValue());
	qUpdate.OnBindValue(":PercentRetail", EditPercentRetail->OnGetValue());
	qUpdate.OnBindValue(":RecipeID", RecipeID);
	bool Executed = qUpdate.OnExecute();

	if (Executed)
	{
		dynamic_cast<ISRecipesListForm*>(RecipeListForm)->OnUpdate();
	}

	return Executed;
}
//-----------------------------------------------------------------------------
void ISCalculationForm::OnClearPercentFields()
{
	EditPercentDistributor->OnClearValue();
	EditPercentDealer->OnClearValue();
	EditPercentBigWholesale->OnClearValue();
	EditPercentWholesale->OnClearValue();
	EditPercentSmallWholesale->OnClearValue();
	EditPercentRetail->OnClearValue();
}
//-----------------------------------------------------------------------------
void ISCalculationForm::OnCalculate()
{
	double CountRawsAndContainers = ISCalculationsRecipe::OnGetCountRawsAndContainers(RecipeID);
	double CountRaws = ISCalculationsRecipe::OnGetCountRaws(RecipeID);
	double CountContainers = ISCalculationsRecipe::OnGetCountContainters(RecipeID);
	double CostPriceRecipe = ISCalculationsRecipe::OnGetCostPriceRecipe(RecipeID, EditDivider->OnGetValue().toDouble());
	double CostPriceRaws = ISCalculationsRecipe::OnGetCostPriceRecipeRaws(RecipeID);
	double CostPriceContaioners = ISCalculationsRecipe::OnGetCostPriceContainters(RecipeID);
	double Expenses = EditExpenses->OnGetValue().toDouble();
	double Containers = EditContainer->OnGetValue().toDouble();
	double Total = CostPriceRecipe + Expenses + Containers;

	double Distributor = (Total * EditPercentDistributor->OnGetValue().toDouble() / 100) + Total;
	double Dealer = (Total * EditPercentDealer->OnGetValue().toDouble() / 100) + Total;
	double BigWholesale = (Total * EditPercentBigWholesale->OnGetValue().toDouble() / 100) + Total;
	double Wholesale = (Total * EditPercentWholesale->OnGetValue().toDouble() / 100) + Total;
	double SmallWholesale = (Total * EditPercentSmallWholesale->OnGetValue().toDouble() / 100) + Total;
	double Retail = (Total * EditPercentRetail->OnGetValue().toDouble() / 100) + Total;

	EditCountRawsContainers->OnSetValue(CountRawsAndContainers);
	EditCountRaws->OnSetValue(CountRaws);
	EditCountContainers->OnSetValue(CountContainers);
	EditCostPrice->OnSetValue(CostPriceRecipe);
	EditCostPriceRaws->OnSetValue(CostPriceRaws);
	EditConstPriceContainers->OnSetValue(CostPriceContaioners);

	EditDistributor->OnSetValue(Distributor);
	EditDealer->OnSetValue(Dealer);
	EditBigWholesale->OnSetValue(BigWholesale);
	EditWholesale->OnSetValue(Wholesale);
	EditSmallWholesale->OnSetValue(SmallWholesale);
	EditRetail->OnSetValue(Retail);

	EditTotal->OnSetValue(Total);
}
//-----------------------------------------------------------------------------
void ISCalculationForm::OnFillFields()
{
	PMetaClassTable *MetaTable = ISMetaData::OnGetData().OnGetTable("Recipes");

	double Distributor = ISDatabaseHelper::OnGetValueInRecord(Record, MetaTable->OnGetField("Distributor")).toDouble();
	double Dealer = ISDatabaseHelper::OnGetValueInRecord(Record, MetaTable->OnGetField("Dealer")).toDouble();
	double BigWholesale = ISDatabaseHelper::OnGetValueInRecord(Record, MetaTable->OnGetField("BigWholesale")).toDouble();
	double Wholesale = ISDatabaseHelper::OnGetValueInRecord(Record, MetaTable->OnGetField("Wholesale")).toDouble();
	double SmallWholesale = ISDatabaseHelper::OnGetValueInRecord(Record, MetaTable->OnGetField("SmallWholesale")).toDouble();
	double Retail = ISDatabaseHelper::OnGetValueInRecord(Record, MetaTable->OnGetField("Retail")).toDouble();

	double Expenses = ISDatabaseHelper::OnGetValueInRecord(Record, MetaTable->OnGetField("Expenses")).toDouble();
	double Packing = ISDatabaseHelper::OnGetValueInRecord(Record, MetaTable->OnGetField("Packing")).toDouble();

	double PercentDistributor = ISDatabaseHelper::OnGetValueInRecord(Record, MetaTable->OnGetField("PercentDistributor")).toDouble();
	double PercentDealer = ISDatabaseHelper::OnGetValueInRecord(Record, MetaTable->OnGetField("PercentDealer")).toDouble();
	double PercentBigWholesale = ISDatabaseHelper::OnGetValueInRecord(Record, MetaTable->OnGetField("PercentBigWholesale")).toDouble();
	double PercentWholesale = ISDatabaseHelper::OnGetValueInRecord(Record, MetaTable->OnGetField("PercentWholesale")).toDouble();
	double PercentSmallWholesale = ISDatabaseHelper::OnGetValueInRecord(Record, MetaTable->OnGetField("PercentSmallWholesale")).toDouble();
	double PercentRetail = ISDatabaseHelper::OnGetValueInRecord(Record, MetaTable->OnGetField("PercentRetail")).toDouble();

	EditDistributor->OnSetValue(Distributor);
	EditDealer->OnSetValue(Dealer);
	EditBigWholesale->OnSetValue(BigWholesale);
	EditWholesale->OnSetValue(Wholesale);
	EditSmallWholesale->OnSetValue(SmallWholesale);
	EditRetail->OnSetValue(Retail);

	EditExpenses->OnSetValue(Expenses);
	EditContainer->OnSetValue(Packing);

	EditPercentDistributor->OnSetValue(PercentDistributor);
	EditPercentDealer->OnSetValue(PercentDealer);
	EditPercentBigWholesale->OnSetValue(PercentBigWholesale);
	EditPercentWholesale->OnSetValue(PercentWholesale);
	EditPercentSmallWholesale->OnSetValue(PercentSmallWholesale);
	EditPercentRetail->OnSetValue(PercentRetail);

	OnCalculate();
}
//-----------------------------------------------------------------------------
