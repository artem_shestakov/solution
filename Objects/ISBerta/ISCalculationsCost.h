#pragma once
//-----------------------------------------------------------------------------
#include <QObject>
//-----------------------------------------------------------------------------
//!����� ��� �������� ������
class ISCalculationsCost : public QObject
{
	Q_OBJECT

public:
	ISCalculationsCost();
	virtual ~ISCalculationsCost();

	static double OnGetCosts(); //�������� ������� �� ���� ��������

	static double OnGetCostCommunicationsAndTaxes(); //�������� ������� �� ������������ � ������

	static double OnGetAllCostProductions(); //�������� ������� �� ��������� �� ���� ��������

	static double OnGetCostProductions(const int &PeriodID); //�������� ������� �� ��������� �� �������
	static double OnGetCostCommunications(const int &PeriodID); //�������� ������� �� ������������ �� �������
	static double OnGetCostTaxes(const int &PeriodID); //�������� ������� �� ������ �� �������

protected:
	static double OnGetCost(const QString &TableName, const QString &TableAlias, const int &PeriodID); //�������� �������
};
//-----------------------------------------------------------------------------
