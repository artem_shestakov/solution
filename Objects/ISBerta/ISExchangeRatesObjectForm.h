#pragma once
//-----------------------------------------------------------------------------
#include "ISObjectBaseForm.h"
#include "ISCalculationsRecipe.h"
//-----------------------------------------------------------------------------
class ISExchangeRatesObjectForm : public ISObjectBaseForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISExchangeRatesObjectForm(ObjectFormType FormType, PMetaClassTable *MetaTable, QWidget *parent = 0);
	virtual ~ISExchangeRatesObjectForm();

protected:
	bool OnSave();
	virtual void OnShowAfterEvent();
	void OnFill(); //��������� ���������� ���������� �� �����
	bool OnCheckActiveCurrentCourse(); //�������� ���������� �������� �����

private:
	ISFieldEditBase *FieldEditDollar;
	ISFieldEditBase *FieldEditEuro;
	ISFieldEditBase *FieldEditPounds;

	QVariant Dollar;
	QVariant Euro;
	QVariant Pounds;
};
//-----------------------------------------------------------------------------
