#include "StdAfx.h"
#include "ISCalculationsCost.h"
#include "ISQueryText.h"
#include "ISQuery.h"
//-----------------------------------------------------------------------------
static QString QS_PERIODS = PREPARE_QUERY("SELECT prds_id FROM periods WHERE NOT prds_isdeleted ORDER BY prds_id");
//-----------------------------------------------------------------------------
static QString QS_COST = PREPARE_QUERY("SELECT ALIAS_january, ALIAS_february, ALIAS_march, ALIAS_april, ALIAS_may, ALIAS_june, ALIAS_july, ALIAS_august, ALIAS_september, ALIAS_october, ALIAS_november, ALIAS_december FROM TABLE_NAME WHERE NOT ALIAS_isdeleted AND ALIAS_period = :PeriodID ORDER BY ALIAS_id");
//-----------------------------------------------------------------------------
ISCalculationsCost::ISCalculationsCost()
{

}
//-----------------------------------------------------------------------------
ISCalculationsCost::~ISCalculationsCost()
{

}
//-----------------------------------------------------------------------------
double ISCalculationsCost::OnGetCosts()
{
	double other = OnGetCostCommunicationsAndTaxes();
	double AllCostPriductions = OnGetAllCostProductions();
	double Result = other / AllCostPriductions;
	return Result;
}
//-----------------------------------------------------------------------------
double ISCalculationsCost::OnGetCostCommunicationsAndTaxes()
{
	ISQuery qSelectPeriods(QS_PERIODS);
	if (qSelectPeriods.OnExecute())
	{
		if (qSelectPeriods.OnGetCountResultRows())
		{
			double Costs = 0;
			while (qSelectPeriods.OnNext())
			{
				int PeriodID = qSelectPeriods.OnReadColumn("prds_id").toInt();

				double CostCommunications = OnGetCostCommunications(PeriodID);
				double CostTaxes = OnGetCostTaxes(PeriodID);

				double SumCostWithPeriod = CostCommunications + CostTaxes;
				Costs += SumCostWithPeriod;
			}

			return Costs;
		}
	}

	return -1;
}
//-----------------------------------------------------------------------------
double ISCalculationsCost::OnGetAllCostProductions()
{
	ISQuery qSelectPeriods(QS_PERIODS);
	if (qSelectPeriods.OnExecute())
	{
		if (qSelectPeriods.OnGetCountResultRows())
		{
			double Costs = 0;
			while (qSelectPeriods.OnNext())
			{
				int PeriodID = qSelectPeriods.OnReadColumn("prds_id").toInt();

				double Cost = OnGetCostProductions(PeriodID);
				Costs += Cost;
			}

			return Costs;
		}
	}

	return -1;
}
//-----------------------------------------------------------------------------
double ISCalculationsCost::OnGetCostProductions(const int &PeriodID)
{
	double CostProductions = OnGetCost("CostProduction", "pnct", PeriodID);
	return CostProductions;
}
//-----------------------------------------------------------------------------
double ISCalculationsCost::OnGetCostCommunications(const int &PeriodID)
{
	double CostCommunuications = OnGetCost("CostCommunications", "csct", PeriodID);
	return CostCommunuications;
}
//-----------------------------------------------------------------------------
double ISCalculationsCost::OnGetCostTaxes(const int &PeriodID)
{
	double CostTaxes = OnGetCost("CostTaxes", "txct", PeriodID);
	return CostTaxes;
}
//-----------------------------------------------------------------------------
double ISCalculationsCost::OnGetCost(const QString &TableName, const QString &TableAlias, const int &PeriodID)
{
	QString QueryText = QS_COST;
	QueryText.replace("ALIAS", TableAlias);
	QueryText.replace("TABLE_NAME", TableName);

	ISQuery qSelectProductions(QueryText);
	qSelectProductions.OnBindValue(":PeriodID", PeriodID);
	if (qSelectProductions.OnExecute())
	{
		if (qSelectProductions.OnGetCountResultRows())
		{
			double SumProcustions = 0;

			while (qSelectProductions.OnNext())
			{
				double January = qSelectProductions.OnReadColumn(TableAlias + "_january").toDouble();
				double February = qSelectProductions.OnReadColumn(TableAlias + "_february").toDouble();
				double March = qSelectProductions.OnReadColumn(TableAlias + "_march").toDouble();
				double April = qSelectProductions.OnReadColumn(TableAlias + "_april").toDouble();
				double May = qSelectProductions.OnReadColumn(TableAlias + "_may").toDouble();
				double June = qSelectProductions.OnReadColumn(TableAlias + "_june").toDouble();
				double July = qSelectProductions.OnReadColumn(TableAlias + "_july").toDouble();
				double August = qSelectProductions.OnReadColumn(TableAlias + "_august").toDouble();
				double September = qSelectProductions.OnReadColumn(TableAlias + "_september").toDouble();
				double October = qSelectProductions.OnReadColumn(TableAlias + "_october").toDouble();
				double November = qSelectProductions.OnReadColumn(TableAlias + "_november").toDouble();
				double December = qSelectProductions.OnReadColumn(TableAlias + "_december").toDouble();

				double CostRow = January + February + March + April + May + June + July + August + September + October + November + December;
				SumProcustions += CostRow;
			}

			int CountValues = qSelectProductions.OnGetCountResultRows() * 12;

			double CostProductions = SumProcustions / CountValues;
			return CostProductions;
		}
	}

	return 0;
}
//-----------------------------------------------------------------------------
