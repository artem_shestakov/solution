#include "StdAfx.h"
#include "ISCostObjectListForm.h"
#include "ISInterfaceGUI.h"
#include "ISLocalization.h"
#include "ISQuery.h"
//-----------------------------------------------------------------------------
static QString QS_PROCUTIONS_SUM = "SELECT "
								   "SUM(%1_january) AS \"january\", "
								   "SUM(%1_february) AS \"february\", "
								   "SUM(%1_march) AS \"march\", "
								   "SUM(%1_april) AS \"april\", "
								   "SUM(%1_may) AS \"may\", "
								   "SUM(%1_june) AS \"june\", "
								   "SUM(%1_july) AS \"july\", "
								   "SUM(%1_august) AS \"august\", "
								   "SUM(%1_september) AS \"september\", "
								   "SUM(%1_october) AS \"october\", "
								   "SUM(%1_november) AS \"november\", "
								   "SUM(%1_december) AS \"december\" "
								   "FROM %2 "
								   "WHERE NOT %1_isdeleted "
								   "AND %1_period = :PeriodID";
//-----------------------------------------------------------------------------
ISCostObjectListForm::ISCostObjectListForm(PMetaClassTable *MetaTable, int ParentObjectID, QWidget *parent) : ISListObjectForm(MetaTable, ParentObjectID, parent)
{
	OnGetTableViewBase()->setSizePolicy(QSizePolicy::Policy::Expanding, QSizePolicy::Policy::Minimum);

	OnGetMainLayout()->addWidget(ISInterfaceGUI::OnGetCreateHorizontalLine(this));

	QFont Font("Arial", 10, QFont::Bold);
	Font.setUnderline(true);

	QLabel *Label = new QLabel(this);
	Label->setText(LOCALIZATION("InTotalSum") + ":");
	Label->setFont(Font);
	OnGetMainLayout()->addWidget(Label, 0, Qt::AlignLeft);

	LayutPanelBottom = new QHBoxLayout();

	PanelFields = new QWidget(this);
	PanelFields->setVisible(false);
	PanelFields->setLayout(LayutPanelBottom);
	OnGetMainLayout()->addWidget(PanelFields);

	OnCreateBottomFields();
	OnCreateQuerySum();

	TableViewSum = new ISBaseTableView(this);
	TableViewSum->setSizePolicy(QSizePolicy::Policy::Expanding, QSizePolicy::Policy::Maximum);
	TableViewSum->verticalHeader()->setVisible(false);
	OnGetMainLayout()->addWidget(TableViewSum);
}
//-----------------------------------------------------------------------------
ISCostObjectListForm::~ISCostObjectListForm()
{

}
//-----------------------------------------------------------------------------
void ISCostObjectListForm::OnCreateBottomFields()
{
	QLabel *LabelJanuary = new QLabel(LOCALIZATION("Month.January") + ":", PanelFields);
	LayutPanelBottom->addWidget(LabelJanuary);
	EditJanuary = new ISDoubleEdit(nullptr, PanelFields);
	EditJanuary->OnSetReadOnly(true);
	LayutPanelBottom->addWidget(EditJanuary);

	QLabel *LabelFebruary = new QLabel(LOCALIZATION("Month.February") + ":", PanelFields);
	LayutPanelBottom->addWidget(LabelFebruary);
	EditFebruary = new ISDoubleEdit(nullptr, PanelFields);
	EditFebruary->OnSetReadOnly(true);
	LayutPanelBottom->addWidget(EditFebruary);

	QLabel *LabelMarch = new QLabel(LOCALIZATION("Month.March") + ":", PanelFields);
	LayutPanelBottom->addWidget(LabelMarch);
	EditMarch = new ISDoubleEdit(nullptr, PanelFields);
	EditMarch->OnSetReadOnly(true);
	LayutPanelBottom->addWidget(EditMarch);

	QLabel *LabelApril = new QLabel(LOCALIZATION("Month.April") + ":", PanelFields);
	LayutPanelBottom->addWidget(LabelApril);
	EditApril = new ISDoubleEdit(nullptr, PanelFields);
	EditApril->OnSetReadOnly(true);
	LayutPanelBottom->addWidget(EditApril);

	QLabel *LabelMay = new QLabel(LOCALIZATION("Month.May") + ":", PanelFields);
	LayutPanelBottom->addWidget(LabelMay);
	EditMay = new ISDoubleEdit(nullptr, PanelFields);
	EditMay->OnSetReadOnly(true);
	LayutPanelBottom->addWidget(EditMay);

	QLabel *LabelJune = new QLabel(LOCALIZATION("Month.June") + ":", PanelFields);
	LayutPanelBottom->addWidget(LabelJune);
	EditJune = new ISDoubleEdit(nullptr, PanelFields);
	EditJune->OnSetReadOnly(true);
	LayutPanelBottom->addWidget(EditJune);

	QLabel *LabelJuly = new QLabel(LOCALIZATION("Month.July") + ":", PanelFields);
	LayutPanelBottom->addWidget(LabelJuly);
	EditJuly = new ISDoubleEdit(nullptr, PanelFields);
	EditJuly->OnSetReadOnly(true);
	LayutPanelBottom->addWidget(EditJuly);

	QLabel *LabelAugust = new QLabel(LOCALIZATION("Month.August") + ":", PanelFields);
	LayutPanelBottom->addWidget(LabelAugust);
	EditAugust = new ISDoubleEdit(nullptr, PanelFields);
	EditAugust->OnSetReadOnly(true);
	LayutPanelBottom->addWidget(EditAugust);

	QLabel *LabelSeptember = new QLabel(LOCALIZATION("Month.September") + ":", PanelFields);
	LayutPanelBottom->addWidget(LabelSeptember);
	EditSeptember = new ISDoubleEdit(nullptr, PanelFields);
	EditSeptember->OnSetReadOnly(true);
	LayutPanelBottom->addWidget(EditSeptember);

	QLabel *LabelOctober = new QLabel(LOCALIZATION("Month.October") + ":", PanelFields);
	LayutPanelBottom->addWidget(LabelOctober);
	EditOctober = new ISDoubleEdit(nullptr, PanelFields);
	EditOctober->OnSetReadOnly(true);
	LayutPanelBottom->addWidget(EditOctober);

	QLabel *LabelNovember = new QLabel(LOCALIZATION("Month.November") + ":", PanelFields);
	LayutPanelBottom->addWidget(LabelNovember);
	EditNovember = new ISDoubleEdit(nullptr, PanelFields);
	EditNovember->OnSetReadOnly(true);
	LayutPanelBottom->addWidget(EditNovember);

	QLabel *LabelDecember = new QLabel(LOCALIZATION("Month.December") + ":", PanelFields);
	LayutPanelBottom->addWidget(LabelDecember);
	EditDecember = new ISDoubleEdit(nullptr, PanelFields);
	EditDecember->OnSetReadOnly(true);
	LayutPanelBottom->addWidget(EditDecember);
}
//-----------------------------------------------------------------------------
void ISCostObjectListForm::OnCreateQuerySum()
{
	QS_PROCUTIONS_SUM.replace("%1", OnGetMetaTable()->GetAlias());
	QS_PROCUTIONS_SUM.replace("%2", OnGetMetaTable()->GetName());

	QuerySumSelect += "SELECT "
		 "SUM(%1_january) AS \"" + LOCALIZATION("Month.January") + "\", \n"
		 "SUM(%1_february) AS \"" + LOCALIZATION("Month.February") + "\", \n"
		 "SUM(%1_march) AS \"" + LOCALIZATION("Month.March") + "\", \n"
		 "SUM(%1_april) AS \"" + LOCALIZATION("Month.April") + "\", \n"
		 "SUM(%1_May) AS \"" + LOCALIZATION("Month.May") + "\", \n"
		 "SUM(%1_june) AS \"" + LOCALIZATION("Month.June") + "\", \n"
		 "SUM(%1_july) AS \"" + LOCALIZATION("Month.July") + "\", \n"
		 "SUM(%1_august) AS \"" + LOCALIZATION("Month.August") + "\", \n"
		 "SUM(%1_september) AS \"" + LOCALIZATION("Month.September") + "\", \n"
		 "SUM(%1_october) AS \"" + LOCALIZATION("Month.October") + "\", \n"
		 "SUM(%1_november) AS \"" + LOCALIZATION("Month.November") + "\", \n"
		 "SUM(%1_december) AS \"" + LOCALIZATION("Month.December") + "\" \n"
		 "FROM " + OnGetMetaTable()->GetName() + " \n"
		 "WHERE NOT %1_isdeleted \n"
		 "AND %1_period = " + QString::number(OnGetParentObjectID());
	
	QuerySumSelect.replace("%1", OnGetMetaTable()->GetAlias());

}
//-----------------------------------------------------------------------------
void ISCostObjectListForm::OnLoadDataAfterEvent()
{
	ISQuery qSelect(QS_PROCUTIONS_SUM);
	qSelect.OnBindValue(":PeriodID", OnGetParentObjectID());
	if (qSelect.OnExecuteFirst())
	{
		EditJanuary->OnSetValue(qSelect.OnReadColumn("january").toDouble());
		EditFebruary->OnSetValue(qSelect.OnReadColumn("february").toDouble());
		EditMarch->OnSetValue(qSelect.OnReadColumn("march").toDouble());
		EditApril->OnSetValue(qSelect.OnReadColumn("april").toDouble());
		EditMay->OnSetValue(qSelect.OnReadColumn("may").toDouble());
		EditJune->OnSetValue(qSelect.OnReadColumn("june").toDouble());
		EditJuly->OnSetValue(qSelect.OnReadColumn("july").toDouble());
		EditAugust->OnSetValue(qSelect.OnReadColumn("august").toDouble());
		EditJanuary->OnSetValue(qSelect.OnReadColumn("september").toDouble());
		EditSeptember->OnSetValue(qSelect.OnReadColumn("october").toDouble());
		EditOctober->OnSetValue(qSelect.OnReadColumn("november").toDouble());
		EditNovember->OnSetValue(qSelect.OnReadColumn("november").toDouble());
		EditDecember->OnSetValue(qSelect.OnReadColumn("december").toDouble());
	}

	TableViewSum->OnRequery(QuerySumSelect);
}
//-----------------------------------------------------------------------------
