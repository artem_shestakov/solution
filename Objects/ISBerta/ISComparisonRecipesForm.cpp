#include "StdAfx.h"
#include "ISComparisonRecipesForm.h"
#include "ISInterfaceGUI.h"
#include "ISLocalization.h"
#include "ISQuery.h"
#include "ISBuffer.h"
#include "ISMessageBox.h"
#include "ISMetaData.h"
#include "EXDefines.h"
//-----------------------------------------------------------------------------
static QString QS_RECIPE = "SELECT gprp_name, rcpe_name, rcpe_active, rcpe_actiondate, rcpe_percentdistributor, rcpe_percentdealer, rcpe_percentbigwholesale, rcpe_percentwholesale, rcpe_percentsmallwholesale, rcpe_percentretail "
						   "FROM recipes "
						   "LEFT JOIN grouprecipes ON gprp_id = rcpe_grouprecipe "
						   "WHERE rcpe_id = :RecipeID";
//-----------------------------------------------------------------------------
static QString QS_RAWS_ALT = "SELECT "
							 "r.raws_id AS \"%ID%\", "
							 "r.raws_name AS \"%Name%\", "
							 "r.raws_price AS \"%Price%\", "
							 "c.crcy_name AS \"%Currency%\", "
							 "r.raws_priceruble AS \"%PriceRuble%\", "
							 "u.unts_name AS \"%Unit%\", "
							 "rrl.srln_count AS \"%Count%\", "
							 "r.raws_priceruble * rrl.srln_count AS \"%Sum%\" "
							 "FROM raws r "
							 "LEFT JOIN currency c ON c.crcy_id = r.raws_currency "
							 "LEFT JOIN units u ON u.unts_id = r.raws_unit "
							 "LEFT JOIN recipesrawslink rrl ON rrl.srln_raw = r.raws_id AND rrl.srln_recipes = :RecipeID "
							 "WHERE r.raws_id IN(SELECT srln_raw FROM recipesrawslink WHERE NOT srln_isdeleted AND srln_recipes = :RecipeID) "
							 "AND NOT r.raws_isdeleted "
							 "ORDER BY r.raws_name";
//-----------------------------------------------------------------------------
ISComparisonRecipesForm::ISComparisonRecipesForm(QWidget *parent) : ISInterfaceForm(parent)
{
	ISPushButton *ButtonAddRecipe = new ISPushButton(this);
	ButtonAddRecipe->setText(LOCALIZATION("AddRecipe") + "...");
	ButtonAddRecipe->setIcon(BUFFER_ICONS("Add"));
	connect(ButtonAddRecipe, &ISPushButton::clicked, this, &ISComparisonRecipesForm::OnAddRecipeClicked);
	OnGetMainLayout()->addWidget(ButtonAddRecipe, 0, Qt::AlignLeft | Qt::AlignTop);

	LayoutRecipes = new QHBoxLayout();
	OnGetMainLayout()->addLayout(LayoutRecipes);

	QS_RAWS_ALT.replace("%ID%", LOCALIZATION("ID"));
	QS_RAWS_ALT.replace("%Name%", LOCALIZATION("Named"));
	QS_RAWS_ALT.replace("%Price%", LOCALIZATION("Price"));
	QS_RAWS_ALT.replace("%Currency%", LOCALIZATION("Currency"));
	QS_RAWS_ALT.replace("%PriceRuble%", LOCALIZATION("PriceRuble"));
	QS_RAWS_ALT.replace("%Unit%", LOCALIZATION("Unit"));
	QS_RAWS_ALT.replace("%Count%", LOCALIZATION("Count"));
	QS_RAWS_ALT.replace("%Sum%", LOCALIZATION("Sum"));
}
//-----------------------------------------------------------------------------
ISComparisonRecipesForm::~ISComparisonRecipesForm()
{

}
//-----------------------------------------------------------------------------
void ISComparisonRecipesForm::OnAddRecipeClicked()
{
	ISSelectedListForm *SelectedListForm = new ISSelectedListForm(ISMetaData::OnGetData().OnGetTable("Recipes"), nullptr);
	//connect(SelectedListForm, &ISSelectedListForm::OnSelectedObject, this, &ISComparisonRecipesForm::OnAddRecipe);
	SelectedListForm->OnLoadData();
	SelectedListForm->show();
}
//-----------------------------------------------------------------------------
void ISComparisonRecipesForm::OnAddRecipe(const int &RecipeID)
{
	if (Recipes.count() == 5)
	{
		ISMessageBox::OnWarning(this, LOCALIZATION("Message.ComparisonRecipesLimit"));
		return;
	}

	if (Recipes.contains(RecipeID))
	{
		ISMessageBox::OnWarning(this, LOCALIZATION("Message.AddintRecipeToComparison"));
		return;
	}

	ISQuery qSelectRecipe(QS_RECIPE);
	qSelectRecipe.OnBindValue(":RecipeID", RecipeID);
	if (qSelectRecipe.OnExecuteFirst())
	{
		Recipes.append(RecipeID);

		QString GroupName = qSelectRecipe.OnReadColumn("gprp_name").toString();
		QString RecipeName = qSelectRecipe.OnReadColumn("rcpe_name").toString();
		bool ActiveRecipe = qSelectRecipe.OnReadColumn("rcpe_active").toBool();
		QString ActionDate = qSelectRecipe.OnReadColumn("rcpe_actiondate").toDate().toString(DATE_FORMAT_STRING);
		QString PercentDistributor = qSelectRecipe.OnReadColumn("rcpe_percentdistributor").toString();
		QString PercentDealer = qSelectRecipe.OnReadColumn("rcpe_percentdealer").toString();
		QString PercentBigWholesale = qSelectRecipe.OnReadColumn("rcpe_percentbigwholesale").toString();
		QString PercentWholesale = qSelectRecipe.OnReadColumn("rcpe_percentwholesale").toString();
		QString PercentSmallWholesale = qSelectRecipe.OnReadColumn("rcpe_percentsmallwholesale").toString();
		QString PercentRetail = qSelectRecipe.OnReadColumn("rcpe_percentretail").toString();
		

		QVBoxLayout *Layout = new QVBoxLayout();
		Layout->setContentsMargins(LAYOUT_MARGINS_10_PX);

		QGroupBox *GroupBox = new QGroupBox(this);
		GroupBox->setTitle(RecipeName);
		GroupBox->setFont(FONT_ARIAL_10);
		GroupBox->setLayout(Layout);
		LayoutRecipes->addWidget(GroupBox);

		ISButtonClear *ButtonClear = new ISButtonClear(GroupBox);
		ButtonClear->setToolTip(LOCALIZATION("DeleteRecipeinListComparison"));
		ButtonClear->setObjectName(QString::number(RecipeID));
		connect(ButtonClear, &ISPushButton::clicked, this, &ISComparisonRecipesForm::OnRemoveRecipe);
		Layout->addWidget(ButtonClear, 0, Qt::AlignRight);

		Layout->addWidget(ISInterfaceGUI::OnGetCreateHorizontalLine(GroupBox));

		QFormLayout *FormLayout = new QFormLayout();
		Layout->addLayout(FormLayout);

		//�������� ������ ���������
		QLabel *LabelRecipeGroupName = new QLabel(GroupBox);
		LabelRecipeGroupName->setText(GroupName);
		LabelRecipeGroupName->setToolTip(GroupName);
		LabelRecipeGroupName->setCursor(CURSOR_WHATS_THIS);
		FormLayout->addRow(LOCALIZATION("GroupRecipe") + ":", LabelRecipeGroupName);

		//�������� ���������
		QLabel *LabelRecipeName = new QLabel(GroupBox);
		LabelRecipeName->setText(RecipeName);
		LabelRecipeName->setToolTip(RecipeName);
		LabelRecipeName->setCursor(CURSOR_WHATS_THIS);
		FormLayout->addRow(LOCALIZATION("Named") + ":", LabelRecipeName);

		//���������� ���������
		QLabel *LabelActivateRecipe = new QLabel(GroupBox);
		if (ActiveRecipe)
		{
			LabelActivateRecipe->setText(LOCALIZATION("Yes"));
		}
		else
		{
			LabelActivateRecipe->setText(LOCALIZATION("No"));
		}
		FormLayout->addRow(LOCALIZATION("RecipeActive") + ":", LabelActivateRecipe);

		//��������� �
		QLabel *LabelRecipeActionDate = new QLabel(GroupBox);
		LabelRecipeActionDate->setText(ActionDate);
		FormLayout->addRow(LOCALIZATION("ActionDate") + ":", LabelRecipeActionDate);

		//���������� ����� �� ���������
		QLabel *LabelCountRaws = new QLabel(GroupBox);
		LabelCountRaws->setText(QString::number(ISCalculationsRecipe::OnGetCountRaws(RecipeID)));
		FormLayout->addRow(LOCALIZATION("CountRaws") + ":", LabelCountRaws);

		//���������� ���� �� ���������
		QLabel *LabelCountContainers = new QLabel(GroupBox);
		LabelCountContainers->setText(QString::number(ISCalculationsRecipe::OnGetCountContainters(RecipeID)));
		FormLayout->addRow(LOCALIZATION("CountContainters") + ":", LabelCountContainers);

		//��������� ���� � ������
		QLabel *LabelCostContainters = new QLabel(GroupBox);
		LabelCostContainters->setText(QString::number(ISCalculationsRecipe::OnGetCostPriceContainters(RecipeID)));
		FormLayout->addRow(LOCALIZATION("CostContaintersRuble") + ":", LabelCostContainters);

		//������������� �����
		QLabel *LabelCostPriceRaws = new QLabel(GroupBox);
		LabelCostPriceRaws->setText(QString::number(ISCalculationsRecipe::OnGetCostPriceRecipe(RecipeID)));
		FormLayout->addRow(LOCALIZATION("CostPriceRaws") + ":", LabelCostPriceRaws);

		//������������� ����
		QLabel *LabelCostPriceContaiters = new QLabel(GroupBox);
		LabelCostPriceContaiters->setText(QString::number(ISCalculationsRecipe::OnGetPacking(RecipeID)));
		FormLayout->addRow(LOCALIZATION("CostPriceContainters") + ":", LabelCostPriceContaiters);

		//������� �������������
		QLabel *LabelPercentDistributor = new QLabel(GroupBox);
		LabelPercentDistributor->setText(PercentDistributor);
		FormLayout->addRow(LOCALIZATION("PercentDistributor") + ":", LabelPercentDistributor);

		//������� �������
		QLabel *LabelPercentDealer = new QLabel(GroupBox);
		LabelPercentDealer->setText(PercentDealer);
		FormLayout->addRow(LOCALIZATION("PercentDealer") + ":", LabelPercentDealer);

		//������� �������� ����
		QLabel *LabelPercentBigWholesale = new QLabel(GroupBox);
		LabelPercentBigWholesale->setText(PercentBigWholesale);
		FormLayout->addRow(LOCALIZATION("PercentBigWholesale") + ":", LabelPercentBigWholesale);

		//������� ����
		QLabel *LabelPercentWholesale = new QLabel(GroupBox);
		LabelPercentWholesale->setText(PercentWholesale);
		FormLayout->addRow(LOCALIZATION("PercentWholesale") + ":", LabelPercentWholesale);

		//������� ������� ����
		QLabel *LabelPercentSmallWholesale = new QLabel(GroupBox);
		LabelPercentSmallWholesale->setText(PercentSmallWholesale);
		FormLayout->addRow(LOCALIZATION("PercentSmallWholesale") + ":", LabelPercentSmallWholesale);

		//������� �������
		QLabel *LabelPercentRetail = new QLabel(GroupBox);
		LabelPercentRetail->setText(PercentRetail);
		FormLayout->addRow(LOCALIZATION("PercentRetail") + ":", LabelPercentRetail);

		Layout->addWidget(ISInterfaceGUI::OnGetCreateHorizontalLine(GroupBox));

		QLabel *LabelRaws = new QLabel(GroupBox);
		LabelRaws->setText(LOCALIZATION("Raws") + ":");
		Layout->addWidget(LabelRaws, 0, Qt::AlignLeft);

		QString QueryText = QS_RAWS_ALT;
		QueryText.replace(":RecipeID", QString::number(RecipeID));

		ISBaseTableView *TableView = new ISBaseTableView(GroupBox);
		TableView->OnRequery(QueryText);
		Layout->addWidget(TableView);
	}
}
//-----------------------------------------------------------------------------
void ISComparisonRecipesForm::OnRemoveRecipe()
{
	ISButtonClear *ButtonClear = dynamic_cast<ISButtonClear*>(sender());
	if (ButtonClear)
	{
		int RecipeID = ButtonClear->objectName().toInt();
		
		for (int i = 0; i < Recipes.count(); i++)
		{
			if (Recipes[i] == RecipeID)
			{
				Recipes.remove(i);
			}
		}

		QGroupBox *GroupBox = dynamic_cast<QGroupBox*>(ButtonClear->parentWidget());
		if (GroupBox)
		{
			delete GroupBox;
			GroupBox = nullptr;
		}
	}
}
//-----------------------------------------------------------------------------
