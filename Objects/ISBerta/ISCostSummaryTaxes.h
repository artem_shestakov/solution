#pragma once
//-----------------------------------------------------------------------------
#include "ISCostSummaryBase.h"
//-----------------------------------------------------------------------------
class ISCostSummaryTaxes : public ISCostSummaryBase
{
	Q_OBJECT

public:
	Q_INVOKABLE ISCostSummaryTaxes(QWidget *parent = 0);
	virtual ~ISCostSummaryTaxes();
};
//-----------------------------------------------------------------------------
