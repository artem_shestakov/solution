#pragma once
//-----------------------------------------------------------------------------
#include "ISInterfaceDialogForm.h"
#include "ISDoubleEdit.h"
#include "ISDateEdit.h"
//-----------------------------------------------------------------------------
class ISDesktopRateInsert : public ISInterfaceDialogForm
{
	Q_OBJECT

signals:
	void OnAddingRate();

public:
	ISDesktopRateInsert(QWidget *parent = 0);
	virtual ~ISDesktopRateInsert();
	void closeEvent(QCloseEvent *e);

protected:
	void OnAdd();
	bool OnCheckFillFields();

private:
	QVBoxLayout *Layout;

	ISDoubleEdit *EditDollar;
	ISDoubleEdit *EditEuro;
	ISDoubleEdit *EditPounds;
	ISDateEdit *EditDate;
};
//-----------------------------------------------------------------------------
