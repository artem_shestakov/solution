#include "StdAfx.h"
#include "ISDesktopBerta.h"
#include "EXDefines.h"
#include "ISQuery.h"
#include "ISLocalization.h"
#include "ISMessageBox.h"
#include "ISRateCurrent.h"
#include "ISDesktopRateInsert.h"
//-----------------------------------------------------------------------------
static QString QS_EXCHANGE_RATE_COUNT = "SELECT COUNT(*) FROM exchangerate WHERE NOT exrt_isdeleted AND exrt_activecourse";
//-----------------------------------------------------------------------------
static QString QS_EXCHANGE_RATE = "SELECT exrt_pricedollar, exrt_priceeuro, exrt_pricepounds, exrt_date FROM exchangerate WHERE NOT exrt_isdeleted AND exrt_activecourse";
//-----------------------------------------------------------------------------
ISDesktopBerta::ISDesktopBerta(QWidget *parent) : ISDesktopIntegralSystem(parent)
{
	LayoutDesktop = new QVBoxLayout();
	LayoutDesktop->setContentsMargins(LAYOUT_MARGINS_10_PX);
	OnGetMainLayout()->addLayout(LayoutDesktop);

	QGroupBox *GroupBoxRate = new QGroupBox(this);
	GroupBoxRate->setTitle(LOCALIZATION("ActiveCourse"));
	GroupBoxRate->setLayout(new QHBoxLayout(GroupBoxRate));
	LayoutDesktop->addWidget(GroupBoxRate, 0, Qt::AlignLeft);

	GroupBoxRate->layout()->addWidget(new QLabel(LOCALIZATION("Dollar") + ":", this));
	LineEditDollar = new ISLineEdit(nullptr, this);
	LineEditDollar->OnSetReadOnly(true);
	LineEditDollar->OnSetVisibleButtonClear(false);
	GroupBoxRate->layout()->addWidget(LineEditDollar);

	GroupBoxRate->layout()->addWidget(new QLabel(LOCALIZATION("Euro") + ":", this));
	LineEditEuro = new ISLineEdit(nullptr, this);
	LineEditEuro->OnSetReadOnly(true);
	LineEditEuro->OnSetVisibleButtonClear(false);
	GroupBoxRate->layout()->addWidget(LineEditEuro);

	GroupBoxRate->layout()->addWidget(new QLabel(LOCALIZATION("Pounds") + ":", this));
	LineEditPounds = new ISLineEdit(nullptr, this);
	LineEditPounds->OnSetReadOnly(true);
	LineEditPounds->OnSetVisibleButtonClear(false);
	GroupBoxRate->layout()->addWidget(LineEditPounds);

	GroupBoxRate->layout()->addWidget(new QLabel(LOCALIZATION("Date") + ":", this));
	LineEditDateCourse = new ISLineEdit(nullptr, this);
	LineEditDateCourse->OnSetReadOnly(true);
	LineEditDateCourse->OnSetVisibleButtonClear(false);
	GroupBoxRate->layout()->addWidget(LineEditDateCourse);
	
	ISQuery qSelectCountRate(QS_EXCHANGE_RATE_COUNT);
	if (qSelectCountRate.OnExecuteFirst())
	{
		int CountRate = qSelectCountRate.OnReadColumn("count").toInt();
		if (CountRate == 0)
		{
			ISMessageBox::OnInformation(this, LOCALIZATION("Message.Question.SelecActiveCourse"));
			
			ISDesktopRateInsert *DesktopRateInsert = new ISDesktopRateInsert(nullptr);
			connect(DesktopRateInsert, SIGNAL(OnAddingRate()), this, SLOT(OnFillActiveCourse()));
			DesktopRateInsert->show();
		}
		else if (CountRate == 1)
		{
			OnFillActiveCourse();
		}
	}

	LayoutDesktop->addStretch();
}
//-----------------------------------------------------------------------------
ISDesktopBerta::~ISDesktopBerta()
{

}
//-----------------------------------------------------------------------------
void ISDesktopBerta::OnFillActiveCourse()
{
	ISQuery qSelect(QS_EXCHANGE_RATE);
	if (qSelect.OnExecuteFirst())
	{
		QVariant Dollar = qSelect.OnReadColumn("exrt_pricedollar");
		QVariant Euro = qSelect.OnReadColumn("exrt_priceeuro");
		QVariant Pounds = qSelect.OnReadColumn("exrt_pricepounds");
		QVariant Date = qSelect.OnReadColumn("exrt_date");

		LineEditDollar->OnSetValue(Dollar.toString());
		LineEditEuro->OnSetValue(Euro.toString());
		LineEditPounds->OnSetValue(Pounds.toString());
		LineEditDateCourse->OnSetValue(Date.toDate().toString(DATE_FORMAT_STRING));

		ISRateCurrent::OnSetDollar(Dollar.toDouble());
		ISRateCurrent::OnSetEuro(Euro.toDouble());
		ISRateCurrent::OnSetPounds(Pounds.toDouble());
		ISRateCurrent::OnSetDate(Date.toDate());
	}
}
//-----------------------------------------------------------------------------
