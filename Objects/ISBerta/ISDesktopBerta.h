#pragma once
//-----------------------------------------------------------------------------
#include "ISDesktopIntegralSystem.h"
#include "ISLineEdit.h"
//-----------------------------------------------------------------------------
class ISDesktopBerta : public ISDesktopIntegralSystem
{
	Q_OBJECT

public:
	Q_INVOKABLE ISDesktopBerta(QWidget *parent = 0);
	virtual ~ISDesktopBerta();

public slots:
	void OnFillActiveCourse();

private:
	QVBoxLayout *LayoutDesktop;

	ISLineEdit *LineEditDollar;
	ISLineEdit *LineEditEuro;
	ISLineEdit *LineEditPounds;
	ISLineEdit *LineEditDateCourse;
};
//-----------------------------------------------------------------------------
