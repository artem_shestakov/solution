#include "StdAfx.h"
#include "ISCalculationsRecipe.h"
#include "ISQuery.h"
//-----------------------------------------------------------------------------
static QString QS_COUNT_RAWS = "SELECT SUM(rl.srln_count) "
							   "FROM recipesrawslink rl "
							   "LEFT JOIN raws r ON r.raws_id = rl.srln_raw "
							   "WHERE rl.srln_recipes = :RecipeID "
							   "AND NOT rl.srln_isdeleted "
							   "AND r.raws_containter = :Container";
//-----------------------------------------------------------------------------
static QString QS_COST_CONTAINERS = "SELECT SUM(r.raws_priceruble * rl.srln_count)  "
									"FROM recipesrawslink rl "
									"LEFT JOIN raws r ON r.raws_id = rl.srln_raw AND r.raws_containter "
									"WHERE rl.srln_recipes = :RecipeID "
									"AND NOT rl.srln_isdeleted";
//-----------------------------------------------------------------------------
static QString QS_COST_PRICE = "SELECT rs.raws_priceruble, rl.srln_count "
							  "FROM recipesrawslink rl "
							  "LEFT JOIN raws rs ON rs.raws_id = rl.srln_raw "
							  "WHERE rl.srln_recipes = :RecipeID "
							  "AND NOT rl.srln_isdeleted";
//-----------------------------------------------------------------------------
static QString QS_COST_PRICE_RAWS = "SELECT rs.raws_priceruble, rl.srln_count "
									"FROM recipesrawslink rl "
									"LEFT JOIN raws rs ON rs.raws_id = rl.srln_raw "
									"WHERE rl.srln_recipes = :RecipeID "
									"AND rs.raws_containter = :Containter "
									"AND NOT rl.srln_isdeleted";
//-----------------------------------------------------------------------------
static QString QS_EXPENSES = "SELECT rcpe_expenses FROM recipes WHERE rcpe_id = :RecipeID";
//-----------------------------------------------------------------------------
static QString QS_PACKING = "SELECT rcpe_packing FROM recipes WHERE rcpe_id = :RecipeID";
//-----------------------------------------------------------------------------
static QString QS_RAWS_PRICE = "SELECT raws_price, crcy_uid FROM raws LEFT JOIN currency ON crcy_id = raws_currency WHERE NOT raws_isdeleted AND raws_id = :RawID";
//-----------------------------------------------------------------------------
static QString QU_RAWS_PRICE = "UPDATE raws SET raws_priceruble = :NewPriceRuble WHERE raws_id = :RawID";
//-----------------------------------------------------------------------------
static QString QS_RAWS_BY_RECIPE = "SELECT srln_raw FROM recipesrawslink WHERE srln_recipes = :RecipeID AND NOT srln_isdeleted";
//-----------------------------------------------------------------------------
static QString QS_RECIPE_PERCENTS = "SELECT rcpe_percentdistributor, rcpe_percentdealer, rcpe_percentbigwholesale, rcpe_percentwholesale, rcpe_percentsmallwholesale, rcpe_percentretail, rcpe_expenses, rcpe_packing FROM recipes WHERE rcpe_id = :RecipeID";
//-----------------------------------------------------------------------------
static QString QU_RECIPE = "UPDATE recipes SET "
						   "rcpe_costprice = :CostPrice, "
						   "rcpe_exit = :Exit, "
						   "rcpe_total = :Total, "
						   "rcpe_distributor = :Distributor, "
						   "rcpe_dealer = :Dealer, "
						   "rcpe_bigwholesale = :BigWholesale, "
						   "rcpe_wholesale = :Wholesale, "
						   "rcpe_smallwholesale = :SmallWholesale, "
						   "rcpe_retail = :Retail "
						   "WHERE rcpe_id = :RecipeID";
//-----------------------------------------------------------------------------
static QString QS_RECIPES = "SELECT rcpe_id FROM recipes WHERE NOT rcpe_isdeleted ORDER BY rcpe_id";
//-----------------------------------------------------------------------------
static QString QI_CALCULATE_HISTORY = "INSERT INTO historycalculatesrecipes (hcrs_actiontype, hcrs_recipe, hcrs_percentdistributor, hcrs_percentdealer, hcrs_percentbigwholesale, hcrs_percentwholesale, hcrs_percentsmallwholesale, hcrs_percentretail) "
									  "VALUES(:ActionTypeUID, :RecipeID, :PercentDistributor, :PercentDealer, :PercentBigWholesale, :PercentWholesale, :PercentSmallWholesale, :PercentRetail)";
//-----------------------------------------------------------------------------
ISCalculationsRecipe::ISCalculationsRecipe()
{

}
//-----------------------------------------------------------------------------
ISCalculationsRecipe::~ISCalculationsRecipe()
{

}
//-----------------------------------------------------------------------------
double ISCalculationsRecipe::OnGetCountRaws(const int &RecipeID)
{
	ISQuery qSelectCount(QS_COUNT_RAWS);
	qSelectCount.OnBindValue(":RecipeID", RecipeID);
	qSelectCount.OnBindValue(":Container", false);
	if (qSelectCount.OnExecuteFirst())
	{
		double Sum = qSelectCount.OnReadColumn("sum").toDouble();
		return Sum;
	}

	return -1;
}
//-----------------------------------------------------------------------------
double ISCalculationsRecipe::OnGetCountContainters(const int &RecipeID)
{
	ISQuery qSelectCount(QS_COUNT_RAWS);
	qSelectCount.OnBindValue(":RecipeID", RecipeID);
	qSelectCount.OnBindValue(":Container", true);
	if (qSelectCount.OnExecuteFirst())
	{
		double Sum = qSelectCount.OnReadColumn("sum").toDouble();
		return Sum;
	}

	return -1;
}
//-----------------------------------------------------------------------------
double ISCalculationsRecipe::OnGetCountRawsAndContainers(const int &RecipeID)
{
	double CountRawsAndContainers = OnGetCountRaws(RecipeID) + OnGetCountContainters(RecipeID);
	return CountRawsAndContainers;
}
//-----------------------------------------------------------------------------
double ISCalculationsRecipe::OnGetCostPriceRecipe(const int &RecipeID, double Divider)
{
	ISQuery qSelectCostPrice(QS_COST_PRICE);
	qSelectCostPrice.OnBindValue(":RecipeID", RecipeID);
	if (qSelectCostPrice.OnExecute())
	{
		double Sum = 0;
		while (qSelectCostPrice.OnNext())
		{
			double PriceRuble = qSelectCostPrice.OnReadColumn("raws_priceruble").toDouble();
			double RawsCount = qSelectCostPrice.OnReadColumn("srln_count").toDouble();
			double MultiplicationResult = PriceRuble * RawsCount;
			Sum = Sum + MultiplicationResult;
		}

		double Result = 0;

		if (Divider)
		{
			Result = Sum / Divider;
		}
		else
		{
			Result = Sum / OnGetCountRaws(RecipeID);
		}

		return Result;
	}

	return -1;
}
//-----------------------------------------------------------------------------
double ISCalculationsRecipe::OnGetCostPriceRecipeRaws(const int &RecipeID)
{
	ISQuery qSelect(QS_COST_PRICE_RAWS);
	qSelect.OnBindValue(":RecipeID", RecipeID);
	qSelect.OnBindValue(":Containter", false);
	if (qSelect.OnExecute())
	{
		double Sum = 0;
		while (qSelect.OnNext())
		{
			double PriceRuble = qSelect.OnReadColumn("raws_priceruble").toDouble();
			double RawsCount = qSelect.OnReadColumn("srln_count").toDouble();
			double MultiplicationResult = PriceRuble * RawsCount;
			Sum = Sum + MultiplicationResult;
		}

		double Result = Sum / OnGetCountRaws(RecipeID);
		return Result;
	}
	return -1;
}
//-----------------------------------------------------------------------------
double ISCalculationsRecipe::OnGetCostPriceContainters(const int &RecipeID)
{
	ISQuery qSelect(QS_COST_PRICE_RAWS);
	qSelect.OnBindValue(":RecipeID", RecipeID);
	qSelect.OnBindValue(":Containter", true);
	if (qSelect.OnExecute())
	{
		double Sum = 0;
		while (qSelect.OnNext())
		{
			double PriceRuble = qSelect.OnReadColumn("raws_priceruble").toDouble();
			double RawsCount = qSelect.OnReadColumn("srln_count").toDouble();
			double MultiplicationResult = PriceRuble * RawsCount;
			Sum = Sum + MultiplicationResult;
		}

		double Result = Sum / OnGetCountRaws(RecipeID);
		return Result;
	}
	return -1;
}
//-----------------------------------------------------------------------------
double ISCalculationsRecipe::OnGetExpenses(const int &RecipeID)
{
	ISQuery qSelectExpenses(QS_EXPENSES);
	qSelectExpenses.OnBindValue(":RecipeID", RecipeID);
	if (qSelectExpenses.OnExecuteFirst())
	{
		double Expenses = qSelectExpenses.OnReadColumn("rcpe_expenses").toDouble();
		return Expenses;
	}

	return -1;
}
//-----------------------------------------------------------------------------
double ISCalculationsRecipe::OnGetPacking(const int &RecipeID)
{
	ISQuery qSelectCostPrice(QS_COST_PRICE_RAWS);
	qSelectCostPrice.OnBindValue(":RecipeID", RecipeID);
	qSelectCostPrice.OnBindValue(":Containter", true);
	if (qSelectCostPrice.OnExecute())
	{
		double Sum = 0;
		while (qSelectCostPrice.OnNext())
		{
			double PriceRuble = qSelectCostPrice.OnReadColumn("raws_priceruble").toDouble();
			double RawsCount = qSelectCostPrice.OnReadColumn("srln_count").toDouble();
			double MultiplicationResult = PriceRuble * RawsCount;
			Sum = Sum + MultiplicationResult;
		}

		double Result = Sum / OnGetCountRaws(RecipeID);
		return Result;
	}

	return -1;
}
//-----------------------------------------------------------------------------
double ISCalculationsRecipe::OnGetTotal(const int &RecipeID, const double &ExpensesValue, const double &PackingValue)
{
	double CostPrice = OnGetCostPriceRecipe(RecipeID);

	double Expenses = 0;
	if (ExpensesValue)
	{
		Expenses = ExpensesValue;
	}
	else
	{
		Expenses = OnGetExpenses(RecipeID);
	}

	double Packing = 0;
	if (PackingValue)
	{
		Packing = PackingValue;
	}
	else
	{
		Packing = OnGetPacking(RecipeID);
	}

	double Total = CostPrice + Expenses + Packing;
	return Total;
}
//-----------------------------------------------------------------------------
double ISCalculationsRecipe::OnGetPercentResult(const int &RecipeID, const double &Percent, const double &ExpensesValue, const double &PackingValue)
{
	double Total = OnGetTotal(RecipeID, ExpensesValue, PackingValue);
	double Result = Total * Percent / 100 + Total;
	return Result;
}
//-----------------------------------------------------------------------------
void ISCalculationsRecipe::OnReCalculateRaw(const int &RawID)
{
	ISQuery qSelectRawPrice(QS_RAWS_PRICE);
	qSelectRawPrice.OnBindValue(":RawID", RawID);
	if (qSelectRawPrice.OnExecuteFirst())
	{
		double Price = qSelectRawPrice.OnReadColumn("raws_price").toDouble();
		QString CurrencyUID = qSelectRawPrice.OnReadColumn("crcy_uid").toString();
		
		double NewPriceRuble = 0;

		if (CurrencyUID == "cf24496b-e15f-415e-ba71-151ddc4b05b9")
		{
			NewPriceRuble = Price;
		}
		if (CurrencyUID == "314d5eb2-3722-4f48-a320-3c2c5f3e2e4c") //���� ������ ����� ������
		{
			NewPriceRuble = ISRateCurrent::OnGetDollar() * Price;
		}
		else if (CurrencyUID == "28cd4d7c-594e-427c-a808-6e6eb2a6a150") //���� ������ ����� ����
		{
			NewPriceRuble = ISRateCurrent::OnGetEuro() * Price;
		}
		else if (CurrencyUID == "645841dd-bc90-42d5-8341-f6c7b2002ab3") //���� ������ ����� ���� ���������
		{
			NewPriceRuble = ISRateCurrent::OnGetPounds() * Price;
		}

		NewPriceRuble = ISMath::OnLimitDoubleValue(NewPriceRuble, 4);

		ISQuery qUpdateRawPrice(QU_RAWS_PRICE);
		qUpdateRawPrice.OnBindValue(":NewPriceRuble", NewPriceRuble);
		qUpdateRawPrice.OnBindValue(":RawID", RawID);
		qUpdateRawPrice.OnExecute();
	}
}
//-----------------------------------------------------------------------------
void ISCalculationsRecipe::OnReCalculateRaws(const int &RecipeID)
{
	ISQuery qSelectRaws(QS_RAWS_BY_RECIPE);
	qSelectRaws.OnBindValue(":RecipeID", RecipeID);
	if (qSelectRaws.OnExecute())
	{
		while (qSelectRaws.OnNext())
		{
			int RawID = qSelectRaws.OnReadColumn("srln_raw").toInt();
			OnReCalculateRaw(RawID);
		}
	}
}
//-----------------------------------------------------------------------------
void ISCalculationsRecipe::OnReCalculateResipe(const int &RecipeID)
{
	OnReCalculateRaws(RecipeID);

	double CountRaws = OnGetCountRaws(RecipeID);
	double CostPrice = OnGetCostPriceRecipe(RecipeID);
	double Expenses = OnGetExpenses(RecipeID);
	double Packing = OnGetPacking(RecipeID);
	double Total = OnGetTotal(RecipeID, Expenses, Packing);

	ISQuery qSelectPercents(QS_RECIPE_PERCENTS);
	qSelectPercents.OnBindValue(":RecipeID", RecipeID);
	if (qSelectPercents.OnExecuteFirst())
	{
		double PercentDistributor = qSelectPercents.OnReadColumn("rcpe_percentdistributor").toDouble();
		double PercentDealer = qSelectPercents.OnReadColumn("rcpe_percentdealer").toDouble();
		double PercentBigWholesale = qSelectPercents.OnReadColumn("rcpe_percentbigwholesale").toDouble();
		double PercentWholesale = qSelectPercents.OnReadColumn("rcpe_percentwholesale").toDouble();
		double PercentSmallWholesale = qSelectPercents.OnReadColumn("rcpe_percentsmallwholesale").toDouble();
		double PercentRetail = qSelectPercents.OnReadColumn("rcpe_percentretail").toDouble();
		double Expenses = qSelectPercents.OnReadColumn("rcpe_expenses").toDouble();
		double Packing = qSelectPercents.OnReadColumn("rcpe_packing").toDouble();

		double NewPercentDistributor = OnGetPercentResult(RecipeID, PercentDistributor, Expenses, Packing);
		double NewPercentDealer = OnGetPercentResult(RecipeID, PercentDealer, Expenses, Packing);
		double NewPercentBigWholesale = OnGetPercentResult(RecipeID, PercentBigWholesale, Expenses, Packing);
		double NewPercentWholesale = OnGetPercentResult(RecipeID, PercentWholesale, Expenses, Packing);
		double NewPercentSmallWholesale = OnGetPercentResult(RecipeID, PercentSmallWholesale, Expenses, Packing);
		double NewPercentRetail = OnGetPercentResult(RecipeID, PercentRetail, Expenses, Packing);

		CostPrice = ISMath::OnLimitDoubleValue(CostPrice, 4);
		CountRaws = ISMath::OnLimitDoubleValue(CountRaws, 4);
		Total = ISMath::OnLimitDoubleValue(Total, 4);
		NewPercentDistributor = ISMath::OnLimitDoubleValue(NewPercentDistributor, 4);
		NewPercentDealer = ISMath::OnLimitDoubleValue(NewPercentDealer, 4);
		NewPercentBigWholesale = ISMath::OnLimitDoubleValue(NewPercentBigWholesale, 4);
		NewPercentWholesale = ISMath::OnLimitDoubleValue(NewPercentWholesale, 4);
		NewPercentSmallWholesale = ISMath::OnLimitDoubleValue(NewPercentSmallWholesale, 4);
		NewPercentRetail = ISMath::OnLimitDoubleValue(NewPercentRetail, 4);

		ISQuery qUpdateRecipePercents(QU_RECIPE);
		qUpdateRecipePercents.OnBindValue(":CostPrice", CostPrice);
		qUpdateRecipePercents.OnBindValue(":Exit", CountRaws);
		qUpdateRecipePercents.OnBindValue(":Total", Total);
		qUpdateRecipePercents.OnBindValue(":Distributor", NewPercentDistributor);
		qUpdateRecipePercents.OnBindValue(":Dealer", NewPercentDealer);
		qUpdateRecipePercents.OnBindValue(":BigWholesale", NewPercentBigWholesale);
		qUpdateRecipePercents.OnBindValue(":Wholesale", NewPercentWholesale);
		qUpdateRecipePercents.OnBindValue(":SmallWholesale", NewPercentSmallWholesale);
		qUpdateRecipePercents.OnBindValue(":Retail", NewPercentRetail);
		qUpdateRecipePercents.OnBindValue(":RecipeID", RecipeID);
		qUpdateRecipePercents.OnExecute();
		
		OnAddReCalculateToHistory(RecipeID, PercentDistributor, PercentDealer, PercentBigWholesale, PercentWholesale, PercentSmallWholesale, PercentRetail);
	}
}
//-----------------------------------------------------------------------------
void ISCalculationsRecipe::OnReCalculateAllResipes()
{
	ISQuery qSelectRecipe(QS_RECIPES);
	if (qSelectRecipe.OnExecute())
	{
		while (qSelectRecipe.OnNext())
		{
			int RecipeID = qSelectRecipe.OnReadColumn("rcpe_id").toInt();
			OnReCalculateResipe(RecipeID);
		}
	}
}
//-----------------------------------------------------------------------------
void ISCalculationsRecipe::OnAddCalculateToHistory(const int &RecipeID, const double &PercentDistributor, const double &PercentDealer, const double &PercentBigWholesale, const double &PercentWholesale, const double &PercentSmallWholesale, const double &PercentRetail)
{
	ISQuery qInsert(QI_CALCULATE_HISTORY);
	qInsert.OnBindValue(":ActionTypeUID", QUuid("{CC4FD639-DC2A-49F9-9586-6824168B8BBA}"));
	qInsert.OnBindValue(":RecipeID", RecipeID);
	qInsert.OnBindValue(":PercentDistributor", PercentDistributor);
	qInsert.OnBindValue(":PercentDealer", PercentDealer);
	qInsert.OnBindValue(":PercentBigWholesale", PercentBigWholesale);
	qInsert.OnBindValue(":PercentWholesale", PercentWholesale);
	qInsert.OnBindValue(":PercentSmallWholesale", PercentSmallWholesale);
	qInsert.OnBindValue(":PercentRetail", PercentRetail);
	qInsert.OnExecute();
}
//-----------------------------------------------------------------------------
void ISCalculationsRecipe::OnAddReCalculateToHistory(const int &RecipeID, const double &PercentDistributor, const double &PercentDealer, const double &PercentBigWholesale, const double &PercentWholesale, const double &PercentSmallWholesale, const double &PercentRetail)
{
	ISQuery qInsert(QI_CALCULATE_HISTORY);
	qInsert.OnBindValue(":ActionTypeUID", QUuid("{9AFB4D4C-5A2D-482D-AB32-70932122174D}"));
	qInsert.OnBindValue(":RecipeID", RecipeID);
	qInsert.OnBindValue(":PercentDistributor", PercentDistributor);
	qInsert.OnBindValue(":PercentDealer", PercentDealer);
	qInsert.OnBindValue(":PercentBigWholesale", PercentBigWholesale);
	qInsert.OnBindValue(":PercentWholesale", PercentWholesale);
	qInsert.OnBindValue(":PercentSmallWholesale", PercentSmallWholesale);
	qInsert.OnBindValue(":PercentRetail", PercentRetail);
	qInsert.OnExecute();
}
//-----------------------------------------------------------------------------
