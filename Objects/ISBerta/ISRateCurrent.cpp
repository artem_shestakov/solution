#include "StdAfx.h"
#include "ISRateCurrent.h"
//-----------------------------------------------------------------------------
RateCurrentStruct ISRateCurrent::CurrentStruct;
//-----------------------------------------------------------------------------
ISRateCurrent::ISRateCurrent()
{

}
//-----------------------------------------------------------------------------
ISRateCurrent::~ISRateCurrent()
{

}
//-----------------------------------------------------------------------------
void ISRateCurrent::OnSetDollar(const double &Dollar)
{
	CurrentStruct.Dollar = Dollar;
}
//-----------------------------------------------------------------------------
double ISRateCurrent::OnGetDollar()
{
	return CurrentStruct.Dollar;
}
//-----------------------------------------------------------------------------
void ISRateCurrent::OnSetEuro(const double &Euro)
{
	CurrentStruct.Euro = Euro;
}
//-----------------------------------------------------------------------------
double ISRateCurrent::OnGetEuro()
{
	return CurrentStruct.Euro;
}
//-----------------------------------------------------------------------------
void ISRateCurrent::OnSetPounds(const double &Pounds)
{
	CurrentStruct.Pounds = Pounds;
}
//-----------------------------------------------------------------------------
double ISRateCurrent::OnGetPounds()
{
	return CurrentStruct.Pounds;
}
//-----------------------------------------------------------------------------
void ISRateCurrent::OnSetDate(const QDate &Date)
{
	CurrentStruct.Date = Date;
}
//-----------------------------------------------------------------------------
QDate ISRateCurrent::OnGetDate()
{
	return CurrentStruct.Date;
}
//-----------------------------------------------------------------------------
