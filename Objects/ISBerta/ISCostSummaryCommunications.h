#pragma once
//-----------------------------------------------------------------------------
#include "ISCostSummaryBase.h"
//-----------------------------------------------------------------------------
class ISCostSummaryCommunications : public ISCostSummaryBase
{
	Q_OBJECT

public:
	Q_INVOKABLE ISCostSummaryCommunications(QWidget *parent = 0);
	virtual ~ISCostSummaryCommunications();
};
//-----------------------------------------------------------------------------
