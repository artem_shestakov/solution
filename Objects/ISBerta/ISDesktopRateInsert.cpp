#include "StdAfx.h"
#include "ISDesktopRateInsert.h"
#include "ISAssert.h"
#include "ISInterfaceGUI.h"
#include "ISQuery.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISMessageBox.h"
#include "EXDefines.h"
//-----------------------------------------------------------------------------
static QString QI_RATE = "INSERT INTO exchangerate(exrt_pricedollar, exrt_priceeuro, exrt_pricepounds, exrt_date, exrt_activecourse) "
						 "VALUES(:Dollar, :Euro, :Pounds, :Date, :Active)";
//-----------------------------------------------------------------------------
ISDesktopRateInsert::ISDesktopRateInsert(QWidget *parent) : ISInterfaceDialogForm(parent)
{
	setWindowTitle(LOCALIZATION("AddingActiveRate"));
	setWindowIcon(BUFFER_ICONS("Add"));
	OnForbidResize();
	
	Layout = new QVBoxLayout();
	Layout->setContentsMargins(LAYOUT_MARGINS_10_PX);
	OnGetMainLayout()->addLayout(Layout);

	QFormLayout *FormLayout = new QFormLayout();
	Layout->addLayout(FormLayout);

	EditDollar = new ISDoubleEdit(nullptr, this);
	FormLayout->addRow(LOCALIZATION("Dollar") + ":", EditDollar);

	EditEuro = new ISDoubleEdit(nullptr, this);
	FormLayout->addRow(LOCALIZATION("Euro") + ":", EditEuro);

	EditPounds = new ISDoubleEdit(nullptr, this);
	FormLayout->addRow(LOCALIZATION("Pounds") + ":", EditPounds);

	EditDate = new ISDateEdit(nullptr, this);
	FormLayout->addRow(LOCALIZATION("Date") + ":", EditDate);

	Layout->addWidget(ISInterfaceGUI::OnGetCreateHorizontalLine(this));

	ISPushButton *ButtonAdd = new ISPushButton(this);
	ButtonAdd->setText(LOCALIZATION("Addition"));
	ButtonAdd->setFixedWidth(100);
	connect(ButtonAdd, &ISPushButton::clicked, this, &ISDesktopRateInsert::OnAdd);
	Layout->addWidget(ButtonAdd, 0, Qt::AlignHCenter);
}
//-----------------------------------------------------------------------------
ISDesktopRateInsert::~ISDesktopRateInsert()
{
	
}
//-----------------------------------------------------------------------------
void ISDesktopRateInsert::closeEvent(QCloseEvent *e)
{
	if (EditDollar->OnGetValue() == VARIANT_VALUE_INVALID || EditEuro->OnGetValue() == VARIANT_VALUE_INVALID || EditPounds->OnGetValue() == VARIANT_VALUE_INVALID || EditDate->OnGetValue() == VARIANT_VALUE_INVALID)
	{
		ISMessageBox::OnCritical(this, LOCALIZATION("Message.AllFieldMustFill"));
		e->ignore();
	}
	else
	{
		ISInterfaceDialogForm::closeEvent(e);
	}
}
//-----------------------------------------------------------------------------
void ISDesktopRateInsert::OnAdd()
{
	if (OnCheckFillFields())
	{
		ISQuery qInsertRate(QI_RATE);
		qInsertRate.OnBindValue(":Dollar", EditDollar->OnGetValue());
		qInsertRate.OnBindValue(":Euro", EditEuro->OnGetValue());
		qInsertRate.OnBindValue(":Pounds", EditPounds->OnGetValue());
		qInsertRate.OnBindValue(":Date", EditDate->OnGetValue());
		qInsertRate.OnBindValue(":Active", true);
		if (qInsertRate.OnExecute())
		{
			emit OnAddingRate();
			close();
		}
		else
		{
			IS_ASSERT(false, QI_RATE);
		}
	}
	else
	{
		ISMessageBox::OnWarning(this, LOCALIZATION("Message.Warning.AllFieldNotNull"));
	}
}
//-----------------------------------------------------------------------------
bool ISDesktopRateInsert::OnCheckFillFields()
{
	if (EditDollar->OnGetValue() == VARIANT_VALUE_INVALID || EditEuro->OnGetValue() == VARIANT_VALUE_INVALID || EditPounds->OnGetValue() == VARIANT_VALUE_INVALID || EditDate->OnGetValue() == VARIANT_VALUE_INVALID)
	{
		return false;
	}

	return true;
}
//-----------------------------------------------------------------------------
