#include "StdAfx.h"
#include "ISExchangeRatesListForm.h"
#include "ISSystem.h"
#include "EXDefines.h"
#include "ISLocalization.h"
#include "ISQuery.h"
#include "ISMessageBox.h"
#include "ISModule.h"
//-----------------------------------------------------------------------------
static QString QU_COURSE_NOT_ACTIVE = "UPDATE exchangerate SET exrt_activecourse = false";
//-----------------------------------------------------------------------------
static QString QU_ACTIVE_COURSE = "UPDATE exchangerate SET exrt_activecourse = true WHERE exrt_id = :ObjectID";
//-----------------------------------------------------------------------------
ISExchangeRatesListForm::ISExchangeRatesListForm(PMetaClassTable *MetaTable, QWidget *parent) : ISListBaseForm(MetaTable, parent)
{
	QAction *ActionSelectActiveCourse = new QAction(OnGetTableViewBase());
	ActionSelectActiveCourse->setText(LOCALIZATION("ActiveCourse"));
	ActionSelectActiveCourse->setToolTip(LOCALIZATION("ActiveCourse"));
	ActionSelectActiveCourse->setIcon(ISModule::OnGetModule().OnGetIcon("ActiveCourse"));
	connect(ActionSelectActiveCourse, &QAction::triggered, this, &ISExchangeRatesListForm::OnSelectActiveCourse);
	OnAddAction(ActionSelectActiveCourse);

	QAction *ActionHighlightActiveCourse = new QAction(OnGetTableViewBase());
	ActionHighlightActiveCourse->setText(LOCALIZATION("HighlightActiveCourse"));
	ActionHighlightActiveCourse->setToolTip(LOCALIZATION("HighlightActiveCourse"));
	ActionHighlightActiveCourse->setIcon(ISModule::OnGetModule().OnGetIcon("HighlightActiveCourse"));
	ActionHighlightActiveCourse->setPriority(QAction::LowPriority);
	connect(ActionHighlightActiveCourse, &QAction::triggered, this, &ISExchangeRatesListForm::OnHighlightActiveCourse);
	OnAddAction(ActionHighlightActiveCourse, false);
}
//-----------------------------------------------------------------------------
ISExchangeRatesListForm::~ISExchangeRatesListForm()
{

}
//-----------------------------------------------------------------------------
void ISExchangeRatesListForm::OnSelectActiveCourse()
{
	if (ISMessageBox::OnQuestion(this, LOCALIZATION("Message.Question.ActiveCourse").arg(OnGetDateSelectedCourse())))
	{
		ISQuery qUpdateClearActiveCourse(QU_COURSE_NOT_ACTIVE);
		qUpdateClearActiveCourse.OnExecute();

		ISQuery qUpdateActiveCourse(QU_ACTIVE_COURSE);
		qUpdateActiveCourse.OnBindValue(":ObjectID", OnGetObjectID());
		qUpdateActiveCourse.OnExecute();

		OnUpdate();
		QMetaObject::invokeMethod(ISSystem::OnGetDesktop(), "OnFillActiveCourse");

		if (ISMessageBox::OnQuestion(this, LOCALIZATION("Message.Question.RecalculateRecipes")))
		{
			ISCalculationsRecipe::OnReCalculateAllResipes();
		}
	}
}
//-----------------------------------------------------------------------------
void ISExchangeRatesListForm::OnHighlightActiveCourse()
{
	int RowCount = OnGetSqlModel()->rowCount();
	for (int i = 0; i < RowCount; i++)
	{
		bool ActiveCourse = OnGetValueOfCurrentRecord("ActiveCourse").toBool();
		if (ActiveCourse)
		{
			OnGetTableViewBase()->OnGetTableView()->selectRow(i);
			return;
		}
	}
}
//-----------------------------------------------------------------------------
QString ISExchangeRatesListForm::OnGetDateSelectedCourse()
{
	QString DateString = OnGetValueOfCurrentRecord("Date").toDate().toString(DATE_FORMAT_STRING);
	return DateString;
}
//-----------------------------------------------------------------------------
void ISExchangeRatesListForm::OnDelete()
{
	bool ActiveCourse = OnGetValueOfCurrentRecord("ActiveCourse").toBool();
	if (ActiveCourse)
	{
		ISMessageBox::OnWarning(this, LOCALIZATION("Message.Warning.ActiveCourseNotDelete"));
	}
	else
	{
		ISListBaseForm::OnDelete();
	}
}
//-----------------------------------------------------------------------------
