#pragma once
//-----------------------------------------------------------------------------
#include "StdAfx.h"
#include "isberta_global.h"
//-----------------------------------------------------------------------------
extern "C" 
{
	void ISBERTA_EXPORT OnRegisterMetaTypes();
	ISBERTA_EXPORT QFile* OnGetFileRecipe();
	QDomElement ISBERTA_EXPORT OnGetSchema(); //�������� ����� ��
	QDomElement ISBERTA_EXPORT OnGetLocalizationSchema();
	QIcon ISBERTA_EXPORT OnGetIcon(const QString &IconName);
}
//-----------------------------------------------------------------------------