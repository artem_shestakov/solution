#pragma once
//-----------------------------------------------------------------------------
#include "ISListBaseForm.h"
#include "ISCalculationForm.h"
#include "ISProcessForm.h"
#include "ISDoubleEdit.h"
//-----------------------------------------------------------------------------
//!����� ����� ������ ��� �������� ��������
class ISRecipesListForm : public ISListBaseForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISRecipesListForm(PMetaClassTable *MetaTable, QWidget *parent = 0);
	virtual ~ISRecipesListForm();

	virtual void OnUpdate();

protected slots:
	void OnSelectedRecord();

protected:
	void OnCreateBottomPanel();
	void OnShowCalculationForm();
	void OnReCalculateCurrent();
	void OnReCalculateAll();
	void OnShowHideRigthPanel();
	void OnFillBottomFields(); //���������� ������ �����
	void OnClearBottomFields();
	void OnPrintRecipe();
	void OnShowCalculateHistory();

private:
	QHBoxLayout *Layout;
	QToolButton *ButtonShowHideTableRaws;
	QWidget *WidgetBottomPanel;
	ISBaseTableView *TableViewRaws;

	ISDoubleEdit *EditCountRawsContainers; //���������� ����� � ����
	ISDoubleEdit *EditCountRaws; //���������� �����
	ISDoubleEdit *EditCountContainers; //���������� ����
	ISDoubleEdit *EditCostPrice; //������������� ���������
	ISDoubleEdit *EditCostPriceRaws; //������������� �����
	ISDoubleEdit *EditConstPriceContainers; //������������� ����
	ISDoubleEdit *EditTotal; //�����

	bool ShowingRightPanel;
	bool ShowingCalculateWidget;

	QString QuerySelectRaws;
};
//-----------------------------------------------------------------------------
