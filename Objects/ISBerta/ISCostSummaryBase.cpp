#include "StdAfx.h"
#include "ISCostSummaryBase.h"
#include "EXDefines.h"
#include "ISQueryModel.h"
#include "ISInterfaceGUI.h"
#include "ISQuery.h"
#include "ISLocalization.h"
#include "ISMetaData.h"
//-----------------------------------------------------------------------------
static QString QS_PERIODS_PRODUCTION = "SELECT prds_id, prds_year FROM periods WHERE NOT prds_isdeleted ORDER BY prds_id";
//-----------------------------------------------------------------------------
ISCostSummaryBase::ISCostSummaryBase(const QString &TableName, QWidget *parent) : ISInterfaceForm(parent)
{
	this->TableName = TableName;
	this->CountPeriods = 0;

	LayoutScrollArea = new QVBoxLayout();

	ScrollArea = new QScrollArea(this);
	ScrollArea->setContentsMargins(LAYOUT_MARGINS_NULL);
	ScrollArea->setFrameShape(QFrame::NoFrame);
	ScrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	ScrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	ScrollArea->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
	ScrollArea->setWidgetResizable(true);
	ScrollArea->setWidget(new QWidget(ScrollArea));
	ScrollArea->widget()->setLayout(LayoutScrollArea);
	OnGetMainLayout()->addWidget(ScrollArea);

	FontLabel = FONT_ARIAL_12_BOLD;
	FontLabel.setUnderline(true);

	OnCreateInterface();

	if (CountPeriods)
	{
		LayoutScrollArea->addStretch();
	}
	else
	{
		ISLabelNoDataTable *LabelNoData = new ISLabelNoDataTable(ScrollArea);
		LayoutScrollArea->addWidget(LabelNoData,0, Qt::AlignCenter);
	}
}
//-----------------------------------------------------------------------------
ISCostSummaryBase::~ISCostSummaryBase()
{

}
//-----------------------------------------------------------------------------
void ISCostSummaryBase::OnCreateInterface()
{
	ISQuery qSelect(QS_PERIODS_PRODUCTION);
	if (qSelect.OnExecute())
	{
		while (qSelect.OnNext())
		{
			int PeriodID = qSelect.OnReadColumn("prds_id").toInt();
			QString PeriodYear = qSelect.OnReadColumn("prds_year").toString();

			//������������ �������
			ISQueryModel *QueryModel = new ISQueryModel(ISMetaData::OnGetData().OnGetTable(TableName));
			QueryModel->OnSetQuerySelectSystemFields(STRING_VALUE_EMPTY);
			QueryModel->OnSetParentObjectIDClassFilter(PeriodID);
			QString QueryText = QueryModel->OnGetQueryText();

			delete QueryModel;
			QueryModel = nullptr;

			OnCreateTablePeriod(QueryText, PeriodYear);
		}
	}
}
//-----------------------------------------------------------------------------
void ISCostSummaryBase::OnCreateTablePeriod(const QString &QueryText, const QString &PeriodYear)
{
	ISQuery qSelect(QueryText);
	if (qSelect.OnExecute())
	{
		if (qSelect.OnGetCountResultRows()) //���� � ���������� ������� ���� ������
		{
			this->CountPeriods++;

			QLabel *Label = new QLabel(ScrollArea);
			Label->setText(LOCALIZATION("Period") + ": " + PeriodYear);
			Label->setFont(FontLabel);
			LayoutScrollArea->addWidget(Label);

			const QSqlQuery Query = qSelect.OnGetSqlQuery();
			QSqlQueryModel *Model = new QSqlQueryModel(this);
			Model->setQuery(Query);

			ISBaseTableWidget *TableWidget = new ISBaseTableWidget(Model->rowCount(), Model->columnCount() + 1, ScrollArea);
			TableWidget->setSizePolicy(QSizePolicy::Policy::Expanding, QSizePolicy::Policy::Minimum);
			TableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
			TableWidget->setHorizontalHeaderLabels(OnGetHorizonalHeaderLabels(Model));
			LayoutScrollArea->addWidget(TableWidget);

			QLabel *LabelAverageValueByPeriod = new QLabel(this);
			LabelAverageValueByPeriod->setFont(FONT_ARIAL_10);
			LabelAverageValueByPeriod->setText(LOCALIZATION("AverageValueByPeriod") + ": ");
			LayoutScrollArea->addWidget(LabelAverageValueByPeriod);

			LayoutScrollArea->addWidget(ISInterfaceGUI::OnGetCreateHorizontalLinePlain(this));

			double AllSum = 0;
			int CountCells = 0;

			for (int i = 0; i < Model->rowCount(); i++)
			{
				double Sum = 0;

				for (int j = 0; j < Model->columnCount(); j++)
				{
					QVariant Value = Model->index(i, j).data();
					QTableWidgetItem *TableWidgetItem = new QTableWidgetItem();
					TableWidgetItem->setText(Value.toString());
					TableWidget->setItem(i, j, TableWidgetItem);

					bool *ok = false;

					if (Value.toDouble(ok) && Model->headerData(j, Qt::Horizontal).toString() != LOCALIZATION("ID"))
					{
						Sum += Value.toDouble();
						AllSum += Value.toDouble();
						CountCells++;
					}
				}

				double AverageValue = Sum / 12;

				//������� ��������
				QTableWidgetItem *AverageItem = new QTableWidgetItem(QString::number(AverageValue, 'f', 4));
				AverageItem->setFont(FONT_ARIAL_10_BOLD);
				TableWidget->setItem(i, Model->columnCount(), AverageItem);
			}

			double AllAverageValue = AllSum / CountCells;
			if (AllSum)
			{
				LabelAverageValueByPeriod->setText(LabelAverageValueByPeriod->text() + QString::number(AllAverageValue, 'f', 4));
			}
			else
			{
				LabelAverageValueByPeriod->setText(LabelAverageValueByPeriod->text() + "0");
			}

			TableWidget->setHorizontalHeaderItem(Model->columnCount(), new QTableWidgetItem(LOCALIZATION("AverageValue")));
			TableWidget->resizeColumnsToContents();

			delete Model;
			Model = nullptr;
		}
	}
}
//-----------------------------------------------------------------------------
QStringList ISCostSummaryBase::OnGetHorizonalHeaderLabels(QSqlQueryModel *Model)
{
	QStringList StringList;

	for (int i = 0; i < Model->columnCount(); i++)
	{
		StringList.append(Model->headerData(i, Qt::Horizontal).toString());
	}

	return StringList;
}
//-----------------------------------------------------------------------------
