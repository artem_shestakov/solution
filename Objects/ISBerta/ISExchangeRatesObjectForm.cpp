#include "StdAfx.h"
#include "ISExchangeRatesObjectForm.h"
#include "ISSystem.h"
#include "ISMessageBox.h"
#include "ISLocalization.h"
#include "ISQuery.h"
//-----------------------------------------------------------------------------
static QString QS_ACTIVE_COURSE = "SELECT exrt_activecourse FROM exchangerate WHERE exrt_id = :RateID";
//-----------------------------------------------------------------------------
ISExchangeRatesObjectForm::ISExchangeRatesObjectForm(ObjectFormType FormType, PMetaClassTable *MetaTable, QWidget *parent) : ISObjectBaseForm(FormType, MetaTable, parent)
{
	FieldEditDollar = OnGetFieldWidget("PriceDollar");
	FieldEditEuro = OnGetFieldWidget("PriceEuro");
	FieldEditPounds = OnGetFieldWidget("PricePounds");
}
//-----------------------------------------------------------------------------
ISExchangeRatesObjectForm::~ISExchangeRatesObjectForm()
{

}
//-----------------------------------------------------------------------------
bool ISExchangeRatesObjectForm::OnSave()
{
	bool Save = ISObjectBaseForm::OnSave();
	
	if (OnGetCurrentFormType() == ObjectFormType::FormType_New)
	{
		return Save;
	}
	
	if (Save && OnCheckActiveCurrentCourse())
	{
		QMetaObject::invokeMethod(ISSystem::OnGetDesktop(), "OnFillActiveCourse");

		bool Change = false;

		QString ListChangeCurrency;
		if (FieldEditDollar->OnGetValue() != Dollar)
		{
			ListChangeCurrency += QString(char('\7')) + " " + LOCALIZATION("Dollar") + "\n";
			Change = true;
		}
		if (FieldEditEuro->OnGetValue() != Euro)
		{
			ListChangeCurrency += QString(char('\7')) + " " + LOCALIZATION("Euro") + "\n";
			Change = true;
		}
		if (FieldEditPounds->OnGetValue() != Pounds)
		{
			ListChangeCurrency += QString(char('\7')) + " " + LOCALIZATION("Pounds") + "\n";
			Change = true;
		}

		if (Change)
		{
			OnFill();

			QString ChangeCurrnency = "\n\n" + LOCALIZATION("ChangeCurrency") + ":\n" + ListChangeCurrency;

			if (ISMessageBox::OnQuestion(this, LOCALIZATION("Message.Question.RecalculateRecipes") + " " + LOCALIZATION("ThisActionCanNotUndone") + ChangeCurrnency))
			{
				ISCalculationsRecipe::OnReCalculateAllResipes();
			}
		}
	}

	return Save;
}
//-----------------------------------------------------------------------------
void ISExchangeRatesObjectForm::OnShowAfterEvent()
{
	ISObjectBaseForm::OnShowAfterEvent();

	OnFill();
}
//-----------------------------------------------------------------------------
void ISExchangeRatesObjectForm::OnFill()
{
	this->Dollar = FieldEditDollar->OnGetValue();
	this->Euro = FieldEditEuro->OnGetValue();
	this->Pounds = FieldEditPounds->OnGetValue();
}
//-----------------------------------------------------------------------------
bool ISExchangeRatesObjectForm::OnCheckActiveCurrentCourse()
{
	ISQuery qSelect(QS_ACTIVE_COURSE);
	qSelect.OnBindValue(":RateID", ObjectID);
	if (qSelect.OnExecuteFirst())
	{
		bool ActiveRate = qSelect.OnReadColumn("exrt_activecourse").toBool();
		return ActiveRate;
	}
	return false;
}
//-----------------------------------------------------------------------------
