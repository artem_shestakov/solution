#pragma once
//-----------------------------------------------------------------------------
#include "ISListBaseForm.h"
//-----------------------------------------------------------------------------
class ISRawsListForm : public ISListBaseForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISRawsListForm(PMetaClassTable *MetaTable, QWidget *parent = 0);
	virtual ~ISRawsListForm();

protected:
	void OnShowParentRecipes();
};
//-----------------------------------------------------------------------------
