#include "StdAfx.h"
#include "ISRawsListForm.h"
#include "EXDefines.h"
#include "ISLocalization.h"
#include "ISQuery.h"
#include "ISMetaSystemsEntity.h"
#include "ISMessageBox.h"
#include "ISMetaData.h"
#include "ISModule.h"
#include "ISSystem.h"
//-----------------------------------------------------------------------------
static QString QS_RECIPES = "SELECT r.srln_recipes FROM recipesrawslink r WHERE r.srln_raw = :RawID AND NOT r.srln_isdeleted";
//-----------------------------------------------------------------------------
ISRawsListForm::ISRawsListForm(PMetaClassTable *MetaTable, QWidget *parent) : ISListBaseForm(MetaTable, parent)
{
	QAction *ActionRecipes = new QAction(OnGetTableViewBase());
	ActionRecipes->setText(LOCALIZATION("ParentRacipes"));
	ActionRecipes->setToolTip(LOCALIZATION("ParentRacipes"));
	ActionRecipes->setIcon(ISModule::OnGetModule().OnGetIcon("Recipes"));
	connect(ActionRecipes, &QAction::triggered, this, &ISRawsListForm::OnShowParentRecipes);
	OnAddAction(ActionRecipes);
}
//-----------------------------------------------------------------------------
ISRawsListForm::~ISRawsListForm()
{

}
//-----------------------------------------------------------------------------
void ISRawsListForm::OnShowParentRecipes()
{
	ISQuery qSelectRecipes(QS_RECIPES);
	qSelectRecipes.OnBindValue(":RawID", OnGetObjectID());
	if (qSelectRecipes.OnExecute())
	{
		if (!qSelectRecipes.OnGetCountResultRows())
		{
			ISMessageBox::OnWarning(this, LOCALIZATION("Message.NotParentRecipes"));
			return;
		}

		QString Recipes;

		while (qSelectRecipes.OnNext())
		{
			QString RecipeID = qSelectRecipes.OnReadColumn("srln_recipes").toString();
			Recipes += RecipeID + ", ";
		}

		//Recipes.remove(Recipes.length() - 2, 2);
		ISSystem::OnRemoveLastSymbolFromString(Recipes, 2);

		ISMetaSubSystem *SubSystem = ISMetaSystemsEntity::OnGetEntity().OnGetSubSystem("a7a261ae-047f-48cc-be0c-eb0563a611fc");
		SubSystem->OnSetStatusNotWhere(false);

		ISListBaseForm *ListForm = new ISListBaseForm(ISMetaData::OnGetData().OnGetTable("Recipes"));
		ListForm->layout()->setContentsMargins(LAYOUT_MARGINS_10_PX);
		ListForm->setWindowTitle(LOCALIZATION("ParentRacipesForRaw").arg(OnGetValueOfCurrentRecord("Name").toString()));
		ListForm->OnSetMetaSubSystem(SubSystem);
		ListForm->OnGetQueryModel()->OnSetClassFilter("rcpe_id IN(" + Recipes + ")");
		ListForm->show();
		ListForm->OnLoadData();
	}
}
//-----------------------------------------------------------------------------
