#pragma once
//-----------------------------------------------------------------------------
#include "isdemo_global.h"
#include "ISPluginInterface.h"
//-----------------------------------------------------------------------------
class ISDemo : public QObject, public ISPluginInterface
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "Demo" FILE "Demo.json")
	Q_INTERFACES(ISPluginInterface)

public:
	~ISDemo();

	void RegisterMetaTypes() const;
};
//-----------------------------------------------------------------------------
