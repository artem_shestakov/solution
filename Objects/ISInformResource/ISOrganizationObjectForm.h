#pragma once
//-----------------------------------------------------------------------------
#include "isinformresource_global.h"
#include "ISObjectFormBase.h"
#include "ISINNEdit.h"
#include "ISDaDataService.h"
//-----------------------------------------------------------------------------
class ISINFORMRESOURCE_EXPORT ISOrganizationObjectForm : public ISObjectFormBase
{
	Q_OBJECT

public:
	Q_INVOKABLE ISOrganizationObjectForm(ISNamespace::ObjectFormType form_type, PMetaClassTable *meta_table, QWidget *parent, int object_id = 0);
	virtual ~ISOrganizationObjectForm();

protected:
	void INNChanged(const QVariant &value);
	void Notify();
	void Called();

	void SearchFromINN();
	void SearchFinished();

private:
	ISINNEdit *INNEdit;
	ISDaDataService *DaDataService;
};
//-----------------------------------------------------------------------------
