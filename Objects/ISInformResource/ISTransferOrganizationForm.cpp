#include "StdAfx.h"
#include "ISTransferOrganizationForm.h"
#include "EXDefines.h"
#include "ISLocalization.h"
#include "ISUserEdit.h"
#include "ISButtonDialog.h"
//-----------------------------------------------------------------------------
ISTransferOrganizationForm::ISTransferOrganizationForm(int OrganizationCount, QWidget *parent) : ISInterfaceDialogForm(parent)
{
	GetMainLayout()->setContentsMargins(LAYOUT_MARGINS_10_PX);
	ForbidResize();

	QLabel *Label = new QLabel(LOCALIZATION("SelectUserFromOrganizationWork").arg(OrganizationCount), this);
	Label->setWordWrap(true);
	GetMainLayout()->addWidget(Label);

	UserEdit = new ISUserEdit(this);
	GetMainLayout()->addWidget(UserEdit);

	ISButtonDialog *ButtonDialog = new ISButtonDialog(this);
	connect(ButtonDialog, &ISButtonDialog::Apply, this, &ISTransferOrganizationForm::Select);
	connect(ButtonDialog, &ISButtonDialog::Close, this, &ISTransferOrganizationForm::close);
	GetMainLayout()->addWidget(ButtonDialog);
}
//-----------------------------------------------------------------------------
ISTransferOrganizationForm::~ISTransferOrganizationForm()
{

}
//-----------------------------------------------------------------------------
int ISTransferOrganizationForm::GetSelectedUserID() const
{
	return UserEdit->GetValue().toInt();
}
//-----------------------------------------------------------------------------
QString ISTransferOrganizationForm::GetSelectedUserName() const
{
	return UserEdit->GetCurrentText();
}
//-----------------------------------------------------------------------------
void ISTransferOrganizationForm::Select()
{
	SetResult(true);
	close();
}
//-----------------------------------------------------------------------------
