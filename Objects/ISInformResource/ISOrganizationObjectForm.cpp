#include "StdAfx.h"
#include "ISOrganizationObjectForm.h"
#include "ISQuery.h"
#include "ISMessageBox.h"
#include "ISMemoryObjects.h"
#include "ISCore.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISInputDialog.h"
#include "ISCalendar.h"
#include "EXDefines.h"
#include "ISPhoneEdit.h"
#include "ISCallHistoryObjectForm.h"
#include "ISMetaData.h"
//-----------------------------------------------------------------------------
static QString QS_ORGANIZATION = PREPARE_QUERY("SELECT orgz_id FROM organizations WHERE orgz_inn = :INN");
//-----------------------------------------------------------------------------
static QString QS_ORGANIZATION_USER = PREPARE_QUERY("SELECT usrs_surname, usrs_name, usrs_patronymic FROM _users WHERE usrs_id = :UserID");
//-----------------------------------------------------------------------------
static QString QI_CALL_HISTORY = PREPARE_QUERY("INSERT INTO callhistory(clhs_organization, clhs_phone, clhs_result) "
											   "VALUES(:Organization, :Phone, (SELECT clrs_id FROM callresult WHERE clrs_uid = '{5A92BB6B-9D51-4189-8BCE-787792964C99}')) "
											   "RETURNING clhs_id");
//-----------------------------------------------------------------------------
static QString QS_TASKS = PREPARE_QUERY("SELECT task_name, task_id FROM _tasks WHERE NOT task_isdeleted ORDER BY task_name");
//-----------------------------------------------------------------------------
static QString QS_OKVED = PREPARE_QUERY("SELECT okvd_id FROM _okved WHERE okvd_code = :Code");
//-----------------------------------------------------------------------------
ISOrganizationObjectForm::ISOrganizationObjectForm(ISNamespace::ObjectFormType form_type, PMetaClassTable *meta_table, QWidget *parent, int object_id) : ISObjectFormBase(form_type, meta_table, parent, object_id)
{
	DaDataService = new ISDaDataService(this);
	connect(DaDataService, &ISDaDataService::Finished, this, &ISOrganizationObjectForm::SearchFinished);

	for (int i = 0; i < meta_table->GetFields().count(); i++)
	{
		PMetaClassField *MetaField = meta_table->GetFields().at(i);
		if (MetaField->GetType() == ISNamespace::FT_Phone)
		{
			ISPhoneEdit *PhoneEdit = dynamic_cast<ISPhoneEdit*>(GetFieldWidget(MetaField->GetName()));
			connect(PhoneEdit, &ISPhoneEdit::Called, this, &ISOrganizationObjectForm::Called);
		}
	}

	QAction *ActionNotify = new QAction(this);
	ActionNotify->setText(LOCALIZATION("OrganizationNotify"));
	ActionNotify->setToolTip(LOCALIZATION("OrganizationNotify"));
	ActionNotify->setIcon(BUFFER_ICONS("Bell"));
	connect(ActionNotify, &QAction::triggered, this, &ISOrganizationObjectForm::Notify);
	AddAction(ActionNotify);

	QVariant UserID = GetFieldValue("User");
	if (UserID.isValid())
	{
		ISQuery qSelect(QS_ORGANIZATION_USER);
		qSelect.BindValue(":UserID", UserID);
		if (qSelect.ExecuteFirst())
		{
			QString UserName = qSelect.ReadColumn("usrs_surname").toString() + " " + qSelect.ReadColumn("usrs_name").toString() + " " + qSelect.ReadColumn("usrs_patronymic").toString();

			QLabel *LabelWorkIn = new QLabel(GetToolBar());
			LabelWorkIn->setFont(FONT_APPLICATION_BOLD);
			LabelWorkIn->setText(LOCALIZATION("OrganizationInWorkFromUser").arg(UserName));
			LabelWorkIn->setContentsMargins(10, 0, 0, 0);
			GetToolBar()->addWidget(LabelWorkIn);
		}
	}

	INNEdit = dynamic_cast<ISINNEdit*>(GetFieldWidget("INN"));
	connect(INNEdit, &ISINNEdit::ValueChange, this, &ISOrganizationObjectForm::INNChanged);
	connect(INNEdit, &ISINNEdit::SearchFromINN, this, &ISOrganizationObjectForm::SearchFromINN);
}
//-----------------------------------------------------------------------------
ISOrganizationObjectForm::~ISOrganizationObjectForm()
{

}
//-----------------------------------------------------------------------------
void ISOrganizationObjectForm::INNChanged(const QVariant &value)
{
	ISQuery qSelect(QS_ORGANIZATION);
	qSelect.BindValue(":INN", value);
	if (qSelect.ExecuteFirst())
	{
		if (qSelect.GetCountResultRows())
		{
			int OrganizationID = qSelect.ReadColumn("orgz_id").toInt();
			if (GetObjectID() == OrganizationID)
			{
				return;
			}

			if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.FoundOrganizationCard").arg(value.toString())))
			{
				ISObjectFormBase *ObjectForm = ISCore::CreateObjectForm(ISNamespace::OFT_Edit, GetMetaTable(), OrganizationID);
				QMetaObject::invokeMethod(ISMemoryObjects::GetInstance().GetWorkspaceForm(), "AddObjectForm", Q_ARG(QWidget *, ObjectForm));
			}
		}
	}
}
//-----------------------------------------------------------------------------
void ISOrganizationObjectForm::Notify()
{
	QDateTime DateTime = ISInputDialog::GetDateTime(this, LOCALIZATION("Reminder"), LOCALIZATION("DateTime") + ":").toDateTime();
	if (DateTime.isValid())
	{
		ISCalendar::Insert(DateTime.date(), QTime(DateTime.time().hour(), DateTime.time().minute()), LOCALIZATION("CallToOrganization").arg(GetFieldWidget("Name")->GetValue().toString()), QVariant(), GetMetaTable()->GetName(), GetObjectID());
		QMetaObject::invokeMethod(ISMemoryObjects::GetInstance().GetCalendarForm(), "UpdateCurrentMount");
	}
}
//-----------------------------------------------------------------------------
void ISOrganizationObjectForm::Called()
{
	if (GetObjectID())
	{
		ISPhoneEdit *PhoneEdit = dynamic_cast<ISPhoneEdit*>(sender());

		ISQuery qInsertCallHisotory(QI_CALL_HISTORY);
		qInsertCallHisotory.BindValue(":Organization", GetObjectID());
		qInsertCallHisotory.BindValue(":Phone", PhoneEdit->GetValue());
		if (qInsertCallHisotory.ExecuteFirst())
		{
			int ID = qInsertCallHisotory.ReadColumn("clhs_id").toInt();

			ISObjectFormBase *CallHistoryObjectForm = ISCore::CreateObjectForm(ISNamespace::OFT_Edit, ISMetaData::GetInstanse().GetMetaTable("CallHistory"), ID);
			CallHistoryObjectForm->SetParentObjectID(GetObjectID());
			CallHistoryObjectForm->show();
		}
	}
	else
	{
		ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.SaveOrganizationCard"));
	}
}
//-----------------------------------------------------------------------------
void ISOrganizationObjectForm::SearchFromINN()
{
	if (!INNEdit->GetValue().isValid())
	{
		ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.InvalidINN"));
		INNEdit->BlinkRed();
		return;
	}

	DaDataService->SearchFromINN(INNEdit->GetValue().toString());
}
//-----------------------------------------------------------------------------
void ISOrganizationObjectForm::SearchFinished()
{
	if (!GetFieldWidget("Name")->GetValue().isValid())
	{
		GetFieldWidget("Name")->SetValue(DaDataService->GetData().value("name"));
	}

	if (!GetFieldWidget("KPP")->GetValue().isValid())
	{
		GetFieldWidget("KPP")->SetValue(DaDataService->GetData().value("kpp"));
	}

	if (!GetFieldWidget("AddressPhysical")->GetValue().isValid())
	{
		GetFieldWidget("AddressPhysical")->SetValue(DaDataService->GetData().value("address"));
	}

	if (!GetFieldWidget("AddressLegal")->GetValue().isValid())
	{
		GetFieldWidget("AddressLegal")->SetValue(DaDataService->GetData().value("address"));
	}

	if (!GetFieldWidget("Principal")->GetValue().isValid())
	{
		GetFieldWidget("Principal")->SetValue(DaDataService->GetData().value("management"));
	}

	QVariant Okved = DaDataService->GetData().value("okved");
	if (Okved.isValid())
	{
		ISQuery qSelectOkved(QS_OKVED);
		qSelectOkved.BindValue(":Code", Okved);
		if (qSelectOkved.ExecuteFirst())
		{
			QVariant OkvedID = qSelectOkved.ReadColumn("okvd_id").toInt();
			GetFieldWidget("Okved")->SetValue(OkvedID);
		}
	}
}
//-----------------------------------------------------------------------------
