#pragma once
//-----------------------------------------------------------------------------
#include "isinformresource_global.h"
#include "ISInterfaceDialogForm.h"
#include "ISUserEdit.h"
//-----------------------------------------------------------------------------
class ISINFORMRESOURCE_EXPORT ISTransferOrganizationForm : public ISInterfaceDialogForm
{
	Q_OBJECT

public:
	ISTransferOrganizationForm(int OrganizationCount, QWidget *parent = 0);
	virtual ~ISTransferOrganizationForm();

	int GetSelectedUserID() const;
	QString GetSelectedUserName() const;

protected:
	void Select();

private:
	ISUserEdit *UserEdit;

	int SelectedUserID;
};
//-----------------------------------------------------------------------------
