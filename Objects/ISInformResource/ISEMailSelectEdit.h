#pragma once
//-----------------------------------------------------------------------------
#include "isinformresource_global.h"
#include "ISComboEdit.h"
//-----------------------------------------------------------------------------
class ISINFORMRESOURCE_EXPORT ISEMailSelectEdit : public ISComboEdit
{
	Q_OBJECT

public:
	Q_INVOKABLE ISEMailSelectEdit(QObject *MetaField, QWidget *parent);
	Q_INVOKABLE ISEMailSelectEdit(QWidget *parent);
	virtual ~ISEMailSelectEdit();
};
//-----------------------------------------------------------------------------
