#pragma once
//-----------------------------------------------------------------------------
#include "StdAfx.h"
//-----------------------------------------------------------------------------
#ifdef ISINFORMRESOURCE_LIB
# define ISINFORMRESOURCE_EXPORT Q_DECL_EXPORT
#else
# define ISINFORMRESOURCE_EXPORT Q_DECL_IMPORT
#endif
//-----------------------------------------------------------------------------
