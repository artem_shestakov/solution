#pragma once
//-----------------------------------------------------------------------------
#include "isinformresource_global.h"
#include "ISListBaseForm.h"
//-----------------------------------------------------------------------------
class ISINFORMRESOURCE_EXPORT ISOrganizationFreeListForm : public ISListBaseForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISOrganizationFreeListForm(QWidget *parent = 0);
	virtual ~ISOrganizationFreeListForm();

protected:
	void TakeToWork();
};
//-----------------------------------------------------------------------------
