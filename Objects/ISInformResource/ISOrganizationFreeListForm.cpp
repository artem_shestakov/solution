#include "StdAfx.h"
#include "ISOrganizationFreeListForm.h"
#include "ISMetaData.h"
#include "ISLocalization.h"
#include "ISPlugin.h"
#include "ISQuery.h"
#include "ISMetaUser.h"
#include "ISMessageBox.h"
#include "ISProtocol.h"
//-----------------------------------------------------------------------------
static QString QU_ORGANIZATION_USER = PREPARE_QUERY("UPDATE organizations SET orgz_user = :UserID WHERE orgz_id = :ObjectID");
//-----------------------------------------------------------------------------
ISOrganizationFreeListForm::ISOrganizationFreeListForm(QWidget *parent) : ISListBaseForm(ISMetaData::GetInstanse().GetMetaTable("Organizations"), parent)
{
	GetQueryModel()->SetClassFilter("orgz_user IS NULL");

	QAction *ActionTakeToWork = new QAction(GetToolBar());
	ActionTakeToWork->setText(LOCALIZATION("TakeOrganization") + "...");
	ActionTakeToWork->setToolTip(LOCALIZATION("TakeOrganization"));
	ActionTakeToWork->setIcon(ISPlugin::GetInstance().GetPluginInterface()->GetIcon("TakeOrganization"));
	connect(ActionTakeToWork, &QAction::triggered, this, &ISOrganizationFreeListForm::TakeToWork);
	AddAction(ActionTakeToWork, true, true);
}
//-----------------------------------------------------------------------------
ISOrganizationFreeListForm::~ISOrganizationFreeListForm()
{

}
//-----------------------------------------------------------------------------
void ISOrganizationFreeListForm::TakeToWork()
{
	QString OrganizationName = GetCurrentRecordValue("Name").toString();
	int UserID = GetCurrentRecordValueDB("User").toInt();
	if (UserID)
	{
		ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.OrganizationAlreadyInWork").arg(OrganizationName));
		return;
	}

	if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.OrganizationTakeToWork").arg(OrganizationName)))
	{
		ISQuery qUpdate(QU_ORGANIZATION_USER);
		qUpdate.BindValue(":UserID", CURRENT_USER_ID);
		qUpdate.BindValue(":ObjectID", GetCurrentRecordValue("ID"));
		if (qUpdate.Execute())
		{
			ISProtocol::Insert(true, "{075934CA-4E4F-47E6-966F-C3D4991F2B03}", GetMetaTable()->GetName(), GetMetaTable()->GetLocalListName(), GetObjectID(), OrganizationName);

			ISMessageBox::ShowInformation(this, LOCALIZATION("Message.Information.OrganizationTakeToWorkDone").arg(OrganizationName));
			Update();
		}
	}
}
//-----------------------------------------------------------------------------
