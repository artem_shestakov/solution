#include "StdAfx.h"
#include "ISOrganizationItem.h"
#include "EXDefines.h"
#include "ISPushButton.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
//-----------------------------------------------------------------------------
ISOrganizationItem::ISOrganizationItem(int task_id, int organization_id, const QString &organization_name, const QString &task_priority, QWidget *parent) : QWidget(parent)
{
	TaskID = task_id;
	OrganizationID = organization_id;

	QHBoxLayout *Layout = new QHBoxLayout();
	Layout->setContentsMargins(LAYOUT_MARGINS_5_PX);
	setLayout(Layout);

	Label = new QLabel(this);
	Label->setText(organization_name);
	Layout->addWidget(Label);

	Layout->addStretch();

	Layout->addWidget(new QLabel(task_priority, this));

	ISPushButton *ButtonCall = new ISPushButton(this);
	ButtonCall->setText(LOCALIZATION("Call"));
	ButtonCall->setIcon(BUFFER_ICONS("CallPhone"));
	ButtonCall->setCursor(CURSOR_POINTING_HAND);
	Layout->addWidget(ButtonCall);
	connect(ButtonCall, &ISPushButton::clicked, [=]
	{
		emit CallClicked(OrganizationID);
	});

	ISPushButton *ButtonTask = new ISPushButton(this);
	ButtonTask->setText(LOCALIZATION("Task"));
	ButtonTask->setCursor(CURSOR_POINTING_HAND);
	Layout->addWidget(ButtonTask);
	connect(ButtonTask, &ISPushButton::clicked, [=]
	{
		emit TaskClicked(TaskID);
	});
}
//-----------------------------------------------------------------------------
ISOrganizationItem::~ISOrganizationItem()
{

}
//-----------------------------------------------------------------------------
int ISOrganizationItem::GetOrganizationID() const
{
	return OrganizationID;
}
//-----------------------------------------------------------------------------
void ISOrganizationItem::SetFont(const QFont &Font)
{
	Label->setFont(Font);
}
//-----------------------------------------------------------------------------
