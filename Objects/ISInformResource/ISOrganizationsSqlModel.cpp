#include "StdAfx.h"
#include "ISOrganizationsSqlModel.h"
#include "ISQuery.h"
#include "ISSystem.h"
//-----------------------------------------------------------------------------
static QString QS_STATUS_COLOR = PREPARE_QUERY("SELECT stat_name, stat_color FROM organizationstatus");
//-----------------------------------------------------------------------------
ISOrganizationsSqlModel::ISOrganizationsSqlModel(PMetaClassTable *meta_table, QObject *parent) : ISSqlModelCore(meta_table, parent)
{
	ISQuery qSelectColor(QS_STATUS_COLOR);
	if (qSelectColor.Execute())
	{
		while (qSelectColor.Next())
		{
			ColorStatus.insert(qSelectColor.ReadColumn("stat_name").toString(), ISSystem::StringToColor(qSelectColor.ReadColumn("stat_color").toString()));
		}
	}
}
//-----------------------------------------------------------------------------
ISOrganizationsSqlModel::~ISOrganizationsSqlModel()
{

}
//-----------------------------------------------------------------------------
QVariant ISOrganizationsSqlModel::data(const QModelIndex &ModelIndex, int Role) const
{
	if (Role == Qt::TextColorRole)
	{
		if (ModelIndex.column() == GetFieldIndex("Status"))
		{
			QVariant Value = ISSqlModelCore::data(ModelIndex);
			return qVariantFromValue(ColorStatus.value(Value.toString()));
		}
	}

	return ISSqlModelCore::data(ModelIndex, Role);
}
//-----------------------------------------------------------------------------
