#pragma once
//-----------------------------------------------------------------------------
#include "isinformresource_global.h"
#include "ISListViewForm.h"
#include "ISUserEdit.h"
//-----------------------------------------------------------------------------
class ISINFORMRESOURCE_EXPORT ISCallUserStatisticsForm : public ISListViewForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISCallUserStatisticsForm(QWidget *parent = 0);
	virtual ~ISCallUserStatisticsForm();
};
//-----------------------------------------------------------------------------
