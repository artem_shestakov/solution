#pragma once
//-----------------------------------------------------------------------------
#include "isinformresource_global.h"
#include "ISUserEdit.h"
#include "ISCalendarWidget.h"
#include "ISPushButton.h"
#include "ISTextEdit.h"
#include "ISListWidget.h"
#include "ISDesktopInformResource.h"
#include "ISListViewForm.h"
//-----------------------------------------------------------------------------
class ISINFORMRESOURCE_EXPORT ISDesktopInformResource : public QWidget
{
	Q_OBJECT

public:
	Q_INVOKABLE ISDesktopInformResource(QWidget *parent = 0);
	virtual ~ISDesktopInformResource();

protected:
	void CreateLeftPanel();
	void CreateCentralPanel();
	void CreateRightPanel();

	void Reload(const QVariant &value);

	void ReloadOrganizations(); //������������ �����������
	void ReloadCallsOrganization(); //������������ ������� ������� �����������
	
	void ReloadOrganizationNote(); //������������ ���������� �����������
	void ReloadDayNote(); //������������ ���������� ���

	void OrganizationSelectionChanged(); //������� ��������� ��������� �����������
	void DoubleClickedOrganization(QListWidgetItem *ListWidgetItem); //�������� �������� �����������
	void CallOrganization(int OrganizationID);
	void TaskOrganization(int TaskID);
	void SaveOrganizationNote();
	void SaveDayNote();

private:
	int CurrentUserID;

	QVBoxLayout *MainLayout;
	QHBoxLayout *LayoutCentral;
	
	ISListWidget *ListWidgetOrganizations;
	ISUserEdit *UserEdit;
	ISCalendarWidget *CalendarWidget;
	ISPushButton *ButtonSaveNote;
	ISTextEdit *EditOrganizationNote;
	ISPushButton *ButtonSaveDayNote;
	ISTextEdit *EditDayNote;
	QRadioButton *RadioHistoryOrganization;
	QRadioButton *RadioHistoryAll;
	ISListViewForm *ListViewAllCalls;
	QLabel *LabelOrganizationCalls;
	ISListWidget *ListWidgetOrganizationCalls;
};
//-----------------------------------------------------------------------------
