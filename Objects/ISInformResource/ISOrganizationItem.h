#pragma once
//-----------------------------------------------------------------------------
#include "isinformresource_global.h"
//-----------------------------------------------------------------------------
class ISINFORMRESOURCE_EXPORT ISOrganizationItem : public QWidget
{
	Q_OBJECT

signals:
	void CallClicked(int OrganizationID);
	void TaskClicked(int TaskID);

public:
	ISOrganizationItem(int task_id, int organization_id, const QString &organization_name, const QString &task_priority, QWidget *parent = 0);
	virtual ~ISOrganizationItem();

	int GetOrganizationID() const;
	void SetFont(const QFont &Font);

private:
	int TaskID;
	int OrganizationID;

	QLabel *Label;
};
//-----------------------------------------------------------------------------
