#include "StdAfx.h"
#include "ISInformResource.h"
//-----------------------------------------------------------------------------
#include "ISDesktopInformResource.h"
#include "ISOrganizationListForm.h"
#include "ISOrganizationObjectForm.h"
#include "ISOrganizationFreeListForm.h"
#include "ISOrganizationMyListForm.h"
#include "ISCallHistoryObjectForm.h"
#include "ISCallUserStatisticsForm.h"
#include "ISOrganizationsSqlModel.h"
#include "ISDispatchListForm.h"
#include "ISDispatchOrganizationListForm.h"
#include "ISEMailSelectEdit.h"
//-----------------------------------------------------------------------------
ISInformResource::~ISInformResource()
{

}
//-----------------------------------------------------------------------------
void ISInformResource::RegisterMetaTypes() const
{
	qRegisterMetaType<ISDesktopInformResource*>("ISInformResourceDesktop");
	qRegisterMetaType<ISOrganizationObjectForm*>("ISOrganizationObjectForm");
	qRegisterMetaType<ISOrganizationListForm*>("ISOrganizationListForm");
	qRegisterMetaType<ISOrganizationFreeListForm*>("ISOrganizationFreeListForm");
	qRegisterMetaType<ISOrganizationMyListForm*>("ISOrganizationMyListForm");
	qRegisterMetaType<ISCallHistoryObjectForm*>("ISCallHistoryObjectForm");
	qRegisterMetaType<ISCallUserStatisticsForm*>("ISCallUserStatisticsForm");
	qRegisterMetaType<ISOrganizationsSqlModel*>("ISOrganizationsSqlModel");
	qRegisterMetaType<ISDispatchListForm*>("ISDispatchListForm");
	qRegisterMetaType<ISDispatchOrganizationListForm*>("ISDispatchOrganizationListForm");
	qRegisterMetaType<ISEMailSelectEdit*>("ISEMailSelectEdit");
}
//-----------------------------------------------------------------------------
