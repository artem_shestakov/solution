#include "StdAfx.h"
#include "ISDesktopInformResource.h"
#include "EXDefines.h"
#include "EXConstants.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISQuery.h"
#include "ISMetaUser.h"
#include "ISSystem.h"
#include "ISCore.h"
#include "ISMetaData.h"
#include "ISControls.h"
#include "ISOrganizationItem.h"
#include "ISInputDialog.h"
#include "ISMessageBox.h"
#include "ISTelephonyInterface.h"
#include "ISUserRoleEntity.h"
#include "ISTasks.h"
//-----------------------------------------------------------------------------
static QString QS_TASKS = PREPARE_QUERY("SELECT task_id, tspr_name "
										"FROM _tasks "
										"LEFT JOIN _taskpriority ON tspr_uid = task_priority "
										"WHERE NOT task_isdeleted "
										"AND task_executor = :ExecutorID "
										"AND task_status = :StatusUID "
										"AND task_datelimit = :DateLimit "
										"ORDER BY tspr_order DESC");
//-----------------------------------------------------------------------------
static QString QS_TASK_ORGANIZATIONS = PREPARE_QUERY("SELECT tatc_objectid "
													 "FROM _taskattachedcards "
													 "WHERE NOT tatc_isdeleted "
													 "AND tatc_task = :TaskID "
													 "AND tatc_tablename = :TableName "
													 "ORDER BY tatc_id");
//-----------------------------------------------------------------------------
static QString QS_ORGANIZATION = PREPARE_QUERY("SELECT orgz_name FROM organizations WHERE orgz_id = :OrganizationID");
//-----------------------------------------------------------------------------
static QString QS_ORGANIZATION_NOTE = PREPARE_QUERY("SELECT orgz_note FROM organizations WHERE orgz_id = :OrganizationID");
//-----------------------------------------------------------------------------
static QString QU_ORGANIZATION_NOTE = PREPARE_QUERY("UPDATE organizations SET orgz_note = :Note WHERE orgz_id = :OrganizationID");
//-----------------------------------------------------------------------------
static QString QS_ORGANIZATION_PHONE_MAIN = PREPARE_QUERY("SELECT orgz_name, orgz_phonemain FROM organizations WHERE orgz_id = :OrganizationID");
//-----------------------------------------------------------------------------
static QString QS_CALL_HISTORY_ALL = PREPARE_QUERY("SELECT orgz_id, orgz_name, clhs_creationdate, clrs_name, clhs_note "
												   "FROM callhistory "
												   "LEFT JOIN organizations ON orgz_id = clhs_organization "
												   "LEFT JOIN callresult ON clrs_id = clhs_result "
												   "WHERE NOT clhs_isdeleted "
												   "AND clhs_user = :UserID "
												   "AND clhs_creationdate::DATE = :Date "
												   "ORDER BY clhs_id");
//-----------------------------------------------------------------------------
static QString QS_CALL_HISTORY_ORGANIZATION = PREPARE_QUERY("SELECT orgz_id, orgz_name, clhs_creationdate, clrs_name, clhs_note "
															"FROM callhistory "
															"LEFT JOIN organizations ON orgz_id = clhs_organization "
															"LEFT JOIN callresult ON clrs_id = clhs_result "
															"WHERE NOT clhs_isdeleted "
															"AND clhs_user = :UserID "
															"AND clhs_organization = :OrganizationID "
															"AND clhs_creationdate::DATE = :Date "
															"ORDER BY clhs_id");
//-----------------------------------------------------------------------------
static QString QI_CALL_HISTORY = PREPARE_QUERY("INSERT INTO callhistory(clhs_organization, clhs_phone, clhs_result) "
											   "VALUES(:Organization, :Phone, (SELECT clrs_id FROM callresult WHERE clrs_uid = '{5A92BB6B-9D51-4189-8BCE-787792964C99}')) "
											   "RETURNING clhs_id");
//-----------------------------------------------------------------------------
static QString QS_DAY_NOTE = PREPARE_QUERY("SELECT ddnt_note FROM desktopdaynote WHERE ddnt_user = :UserID AND ddnt_date = :Date");
//-----------------------------------------------------------------------------
static QString QS_DAY_NOTE_COUNT = PREPARE_QUERY("SELECT COUNT(*) FROM desktopdaynote WHERE ddnt_user = :UserID AND ddnt_date = :Date");
//-----------------------------------------------------------------------------
static QString QI_DAY_NOTE = PREPARE_QUERY("INSERT INTO desktopdaynote(ddnt_user, ddnt_date, ddnt_note) "
										   "VALUES(:UserID, :Date, :Note)");
//-----------------------------------------------------------------------------
static QString QU_DAY_NOTE = PREPARE_QUERY("UPDATE desktopdaynote SET ddnt_note = :Note WHERE ddnt_user = :UserID AND ddnt_date = :Date");
//-----------------------------------------------------------------------------
ISDesktopInformResource::ISDesktopInformResource(QWidget *parent) : QWidget(parent)
{
	CurrentUserID = CURRENT_USER_ID;

	MainLayout = new QVBoxLayout();
	MainLayout->setContentsMargins(LAYOUT_MARGINS_NULL);
	setLayout(MainLayout);

	LayoutCentral = new QHBoxLayout();
	MainLayout->addLayout(LayoutCentral);

	CreateLeftPanel();
	CreateCentralPanel();
	CreateRightPanel();
}
//-----------------------------------------------------------------------------
ISDesktopInformResource::~ISDesktopInformResource()
{

}
//-----------------------------------------------------------------------------
void ISDesktopInformResource::CreateLeftPanel()
{
	QGroupBox *GroupBoxOrganizations = new QGroupBox(this);
	GroupBoxOrganizations->setTitle(LOCALIZATION("Organizations"));
	GroupBoxOrganizations->setLayout(new QVBoxLayout());
	GroupBoxOrganizations->setFixedWidth(470);
	LayoutCentral->addWidget(GroupBoxOrganizations);

	QToolBar *ToolBarOrganizations = new QToolBar(GroupBoxOrganizations);
	ToolBarOrganizations->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
	GroupBoxOrganizations->layout()->addWidget(ToolBarOrganizations);

	QAction *ActionUpdate = ISControls::CreateActionUpdate(ToolBarOrganizations);
	connect(ActionUpdate, &QAction::triggered, this, &ISDesktopInformResource::ReloadOrganizations);
	ToolBarOrganizations->addAction(ActionUpdate);

	ListWidgetOrganizations = new ISListWidget(this);
	ListWidgetOrganizations->setAlternatingRowColors(true);
	connect(ListWidgetOrganizations, &ISListWidget::itemDoubleClicked, this, &ISDesktopInformResource::DoubleClickedOrganization);
	GroupBoxOrganizations->layout()->addWidget(ListWidgetOrganizations);
}
//-----------------------------------------------------------------------------
void ISDesktopInformResource::CreateCentralPanel()
{
	QVBoxLayout *LayoutCenter = new QVBoxLayout();
	LayoutCentral->addLayout(LayoutCenter);

	if (ISUserRoleEntity::GetInstance().CheckPermissionSpecial("{37491D3A-0261-42EC-9CB2-FB3DAC320D0A}"))
	{
		UserEdit = new ISUserEdit(this);
		connect(UserEdit, &ISUserEdit::ValueChange, this, &ISDesktopInformResource::Reload);
		LayoutCenter->addWidget(UserEdit);
	}

	CalendarWidget = new ISCalendarWidget(this);
	CalendarWidget->setSizePolicy(QSizePolicy::Maximum, CalendarWidget->sizePolicy().verticalPolicy());
	connect(CalendarWidget, &ISCalendarWidget::DateChanged, this, &ISDesktopInformResource::Reload);
	LayoutCenter->addWidget(CalendarWidget, 0, Qt::AlignCenter);

	ButtonSaveNote = new ISPushButton(this);
	ButtonSaveNote->setText(LOCALIZATION("Save"));
	ButtonSaveNote->setIcon(BUFFER_ICONS("Save"));
	connect(ButtonSaveNote, &ISPushButton::clicked, this, &ISDesktopInformResource::SaveOrganizationNote);
	LayoutCenter->addWidget(ButtonSaveNote, 0, Qt::AlignCenter);

	QLabel *LabelOrganizationNote = new QLabel(this);
	LabelOrganizationNote->setText(LOCALIZATION("NoteFromOrganization") + ":");
	LabelOrganizationNote->setFont(FONT_APPLICATION_BOLD);
	LayoutCenter->addWidget(LabelOrganizationNote, 0, Qt::AlignLeft);

	EditOrganizationNote = new ISTextEdit(this);
	EditOrganizationNote->setEnabled(false);
	EditOrganizationNote->SetVisibleClear(false);
	EditOrganizationNote->SetPlaceholderText(LOCALIZATION("InputOrganizationNote"));
	EditOrganizationNote->setSizePolicy(QSizePolicy::Maximum, EditOrganizationNote->sizePolicy().verticalPolicy());
	LayoutCenter->addWidget(EditOrganizationNote);

	ButtonSaveDayNote = new ISPushButton(this);
	ButtonSaveDayNote->setText(LOCALIZATION("Save"));
	ButtonSaveDayNote->setIcon(BUFFER_ICONS("Save"));
	connect(ButtonSaveDayNote, &ISPushButton::clicked, this, &ISDesktopInformResource::SaveDayNote);
	LayoutCenter->addWidget(ButtonSaveDayNote, 0, Qt::AlignCenter);

	QLabel *LabelDayNote = new QLabel(this);
	LabelDayNote->setText(LOCALIZATION("NoteFromDay") + ":");
	LabelDayNote->setFont(FONT_APPLICATION_BOLD);
	LayoutCenter->addWidget(LabelDayNote, 0, Qt::AlignLeft);

	EditDayNote = new ISTextEdit(this);
	EditDayNote->SetVisibleClear(false);
	EditDayNote->SetPlaceholderText(LOCALIZATION("InputDayNote"));
	EditDayNote->setSizePolicy(QSizePolicy::Maximum, EditDayNote->sizePolicy().verticalPolicy());
	LayoutCenter->addWidget(EditDayNote);
}
//-----------------------------------------------------------------------------
void ISDesktopInformResource::CreateRightPanel()
{
	QVBoxLayout *LayoutGroupBox = new QVBoxLayout();
	LayoutGroupBox->setContentsMargins(LAYOUT_MARGINS_5_PX);

	QGroupBox *GroupBoxCallHistory = new QGroupBox(this);
	GroupBoxCallHistory->setTitle(LOCALIZATION("CallHistory"));
	GroupBoxCallHistory->setLayout(LayoutGroupBox);
	LayoutCentral->addWidget(GroupBoxCallHistory);

	ListViewAllCalls = new ISListViewForm(ISMetaData::GetInstanse().GetMetaQuery("CallHistoryAll"), GroupBoxCallHistory);
	ListViewAllCalls->AddCondition(":Date", CalendarWidget->selectedDate());
	ListViewAllCalls->LoadData();
	LayoutGroupBox->addWidget(ListViewAllCalls);

	LabelOrganizationCalls = new QLabel(GroupBoxCallHistory);
	LabelOrganizationCalls->setText(LOCALIZATION("ByOrganization").arg(0));
	LayoutGroupBox->addWidget(LabelOrganizationCalls);

	ListWidgetOrganizationCalls = new ISListWidget(GroupBoxCallHistory);
	ListWidgetOrganizationCalls->setAlternatingRowColors(true);
	ListWidgetOrganizationCalls->setSizePolicy(ListWidgetOrganizationCalls->sizePolicy().horizontalPolicy(), QSizePolicy::Fixed);
	connect(ListWidgetOrganizationCalls, &ISListWidget::itemDoubleClicked, this, &ISDesktopInformResource::DoubleClickedOrganization);
	LayoutGroupBox->addWidget(ListWidgetOrganizationCalls);
}
//-----------------------------------------------------------------------------
void ISDesktopInformResource::Reload(const QVariant &value)
{
	if (value.type() == QVariant::Int) //���� ������� ������������
	{
		ListViewAllCalls->AddCondition(":UserID", value);
		ListViewAllCalls->SetClassFilter("clhs_user = :UserID");
	}
	else if (value.type() == QVariant::Date) //���� �������� ����
	{
		ListViewAllCalls->AddCondition(":Date", value);
	}
	else //��� ������������
	{
		ListViewAllCalls->RemoveCondition(":UserID");
		ListViewAllCalls->ClearClassFilter();
	}

	ListViewAllCalls->Update();
}
//-----------------------------------------------------------------------------
void ISDesktopInformResource::ReloadOrganizations()
{
	ListWidgetOrganizations->Clear();

	ISQuery qSelectTasks(QS_TASKS);
	qSelectTasks.BindValue(":ExecutorID", CurrentUserID);
	qSelectTasks.BindValue(":StatusUID", CONST_UID_TASK_STATUS_OPENED);
	qSelectTasks.BindValue(":DateLimit", CalendarWidget->selectedDate());
	if (qSelectTasks.Execute())
	{
		disconnect(ListWidgetOrganizations, &ISListWidget::itemSelectionChanged, this, &ISDesktopInformResource::ReloadCallsOrganization);
		disconnect(ListWidgetOrganizations, &ISListWidget::itemSelectionChanged, this, &ISDesktopInformResource::ReloadOrganizationNote);
		disconnect(ListWidgetOrganizations, &ISListWidget::itemSelectionChanged, this, &ISDesktopInformResource::OrganizationSelectionChanged);

		while (qSelectTasks.Next())
		{
			int TaskID = qSelectTasks.ReadColumn("task_id").toInt();
			QString TaskPrority = qSelectTasks.ReadColumn("tspr_name").toString();

			ISQuery qSelectOrganizations(QS_TASK_ORGANIZATIONS);
			qSelectOrganizations.BindValue(":TaskID", TaskID);
			qSelectOrganizations.BindValue(":TableName", "Organizations");
			if (qSelectOrganizations.Execute())
			{
				while (qSelectOrganizations.Next())
				{
					int OrganizationID = qSelectOrganizations.ReadColumn("tatc_objectid").toInt();

					ISQuery qSelectOrganization(QS_ORGANIZATION);
					qSelectOrganization.BindValue(":OrganizationID", OrganizationID);
					if (qSelectOrganization.ExecuteFirst())
					{
						QString OrganizationName = qSelectOrganization.ReadColumn("orgz_name").toString();

						QListWidgetItem *ListWidgetItem = new QListWidgetItem(ListWidgetOrganizations);
						ListWidgetItem->setData(Qt::UserRole, OrganizationID);

						ISOrganizationItem *OrganizationItem = new ISOrganizationItem(TaskID, OrganizationID, OrganizationName, TaskPrority, ListWidgetOrganizations);
						connect(OrganizationItem, &ISOrganizationItem::CallClicked, this, &ISDesktopInformResource::CallOrganization);
						connect(OrganizationItem, &ISOrganizationItem::TaskClicked, this, &ISDesktopInformResource::TaskOrganization);
						ListWidgetOrganizations->setItemWidget(ListWidgetItem, OrganizationItem);
						ListWidgetItem->setSizeHint(OrganizationItem->sizeHint());
					}
				}
			}
		}

		connect(ListWidgetOrganizations, &ISListWidget::itemSelectionChanged, this, &ISDesktopInformResource::ReloadCallsOrganization);
		connect(ListWidgetOrganizations, &ISListWidget::itemSelectionChanged, this, &ISDesktopInformResource::ReloadOrganizationNote);
		connect(ListWidgetOrganizations, &ISListWidget::itemSelectionChanged, this, &ISDesktopInformResource::OrganizationSelectionChanged);
	}
}
//-----------------------------------------------------------------------------
void ISDesktopInformResource::ReloadCallsOrganization()
{
	ListWidgetOrganizationCalls->Clear();
	ButtonSaveNote->setEnabled(true);
	EditOrganizationNote->setEnabled(true);

	ISQuery qSelect(QS_CALL_HISTORY_ORGANIZATION);
	qSelect.BindValue(":UserID", CurrentUserID);
	qSelect.BindValue(":OrganizationID", ListWidgetOrganizations->currentItem()->data(Qt::UserRole));
	qSelect.BindValue(":Date", CalendarWidget->selectedDate());
	if (qSelect.Execute())
	{
		while (qSelect.Next())
		{
			int OrganizationID = qSelect.ReadColumn("orgz_id").toInt();
			QString OrganizationName = qSelect.ReadColumn("orgz_name").toString();
			QString CreationTime = qSelect.ReadColumn("clhs_creationdate").toTime().toString(TIME_FORMAT_STRING_V3);
			QString Result = qSelect.ReadColumn("clrs_name").toString();
			QString Note = qSelect.ReadColumn("clhs_note").toString();

			QListWidgetItem *ListWidgetItem = new QListWidgetItem(ListWidgetOrganizationCalls);
			ListWidgetItem->setText(CreationTime + ": " + OrganizationName + " - " + Result);
			ListWidgetItem->setFont(FONT_TAHOMA_10);
			ListWidgetItem->setData(Qt::UserRole, OrganizationID);

			if (Note.length())
			{
				ListWidgetItem->setText(ListWidgetItem->text() + " " + LOCALIZATION("Note") + ": " + Note);
			}
		}

		LabelOrganizationCalls->setText(LOCALIZATION("ByOrganization").arg(ListWidgetOrganizationCalls->count()));
	}
}
//-----------------------------------------------------------------------------
void ISDesktopInformResource::ReloadOrganizationNote()
{
	ISQuery qSelect(QS_ORGANIZATION_NOTE);
	qSelect.BindValue(":OrganizationID", ListWidgetOrganizations->currentItem()->data(Qt::UserRole));
	if (qSelect.ExecuteFirst())
	{
		QString Note = qSelect.ReadColumn("orgz_note").toString();
		EditOrganizationNote->SetValue(Note);
	}
}
//-----------------------------------------------------------------------------
void ISDesktopInformResource::ReloadDayNote()
{
	ISQuery qSelect(QS_DAY_NOTE);
	qSelect.BindValue(":UserID", CurrentUserID);
	qSelect.BindValue(":Date", CalendarWidget->selectedDate());
	if (qSelect.ExecuteFirst())
	{
		QString Note = qSelect.ReadColumn("ddnt_note").toString();
		EditDayNote->SetValue(Note);
	}
	else
	{
		EditDayNote->Clear();
	}
}
//-----------------------------------------------------------------------------
void ISDesktopInformResource::OrganizationSelectionChanged()
{
	for (int i = 0; i < ListWidgetOrganizations->count(); i++)
	{
		dynamic_cast<ISOrganizationItem*>(ListWidgetOrganizations->itemWidget(ListWidgetOrganizations->item(i)))->SetFont(FONT_APPLICATION);
	}

	dynamic_cast<ISOrganizationItem*>(ListWidgetOrganizations->itemWidget(ListWidgetOrganizations->currentItem()))->SetFont(FONT_APPLICATION_BOLD);
}
//-----------------------------------------------------------------------------
void ISDesktopInformResource::DoubleClickedOrganization(QListWidgetItem *ListWidgetItem)
{
	ISCore::CreateObjectForm(ISNamespace::OFT_Edit, ISMetaData::GetInstanse().GetMetaTable("Organizations"), ListWidgetItem->data(Qt::UserRole).toInt())->show();
}
//-----------------------------------------------------------------------------
void ISDesktopInformResource::CallOrganization(int OrganizationID)
{
	ISQuery qSelectPhoneMain(QS_ORGANIZATION_PHONE_MAIN);
	qSelectPhoneMain.BindValue(":OrganizationID", OrganizationID);
	if (qSelectPhoneMain.ExecuteFirst())
	{
		QString OrganizationName = qSelectPhoneMain.ReadColumn("orgz_name").toString();
		QString PhoneMain = qSelectPhoneMain.ReadColumn("orgz_phonemain").toString();
		if (PhoneMain.length())
		{
			ISSystem::SetWaitGlobalCursor(true);
			ISTelephonyInterface::GetInstance().GetPointer()->CallPhone(PhoneMain);
			ISSystem::SetWaitGlobalCursor(false);

			ISQuery qInsertCallHisotory(QI_CALL_HISTORY);
			qInsertCallHisotory.BindValue(":Organization", OrganizationID);
			qInsertCallHisotory.BindValue(":Phone", PhoneMain);
			if (qInsertCallHisotory.ExecuteFirst())
			{
				int ID = qInsertCallHisotory.ReadColumn("clhs_id").toInt();

				ISObjectFormBase *CallHistoryObjectForm = ISCore::CreateObjectForm(ISNamespace::OFT_Edit, ISMetaData::GetInstanse().GetMetaTable("CallHistory"), ID);
				CallHistoryObjectForm->SetParentObjectID(OrganizationID);
				CallHistoryObjectForm->show();

				ReloadCallsOrganization();
			}
		}
		else
		{
			ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.OrganizationPhoneMainNull").arg(OrganizationName));
		}
	}
}
//-----------------------------------------------------------------------------
void ISDesktopInformResource::TaskOrganization(int TaskID)
{
	ISTasks::ShowTaskForm(TaskID);
	//ISCore::CreateObjectForm(ISNamespace::OFT_Edit, ISMetaData::GetInstanse().GetMetaTable("_Tasks"), TaskID)->show();
}
//-----------------------------------------------------------------------------
void ISDesktopInformResource::SaveOrganizationNote()
{
	ISQuery qUpdate(QU_ORGANIZATION_NOTE);
	qUpdate.BindValue(":Note", EditOrganizationNote->GetValue());
	qUpdate.BindValue(":OrganizationID", ListWidgetOrganizations->currentItem()->data(Qt::UserRole));
	qUpdate.Execute();
}
//-----------------------------------------------------------------------------
void ISDesktopInformResource::SaveDayNote()
{
	ISQuery qSelect(QS_DAY_NOTE_COUNT);
	qSelect.BindValue(":UserID", CurrentUserID);
	qSelect.BindValue(":Date", CalendarWidget->selectedDate());
	if (qSelect.ExecuteFirst())
	{
		int Count = qSelect.ReadColumn("count").toInt();
		if (Count)
		{
			ISQuery qUpdate(QU_DAY_NOTE);
			qUpdate.BindValue(":Note", EditDayNote->GetValue());
			qUpdate.BindValue(":UserID", CurrentUserID);
			qUpdate.BindValue(":Date", CalendarWidget->selectedDate());
			qUpdate.Execute();
		}
		else
		{
			ISQuery qInsert(QI_DAY_NOTE);
			qInsert.BindValue(":UserID", CurrentUserID);
			qInsert.BindValue(":Date", CalendarWidget->selectedDate());
			qInsert.BindValue(":Note", EditDayNote->GetValue());
			qInsert.Execute();
		}
	}
}
//-----------------------------------------------------------------------------
