#include "StdAfx.h"
#include "ISOrganizationListForm.h"
#include "ISMetaData.h"
#include "ISQuery.h"
#include "ISLocalization.h"
#include "ISPlugin.h"
#include "ISUserEdit.h"
#include "ISTransferOrganizationForm.h"
#include "ISMessageBox.h"
#include "ISProtocol.h"
#include "ISDaDataService.h"
#include "ISProgressForm.h"
#include "ISNotify.h"
//-----------------------------------------------------------------------------
static QString QU_ORGANIZATION_USER = PREPARE_QUERY("UPDATE organizations SET orgz_user = :UserID WHERE orgz_id = :OrganizationID")
//-----------------------------------------------------------------------------
static QString QU_RESET_EXECUTOR = PREPARE_QUERY("UPDATE organizations SET orgz_user = NULL WHERE orgz_id = :OrganizationID");
//-----------------------------------------------------------------------------
static QString QU_PERCENTAGE = PREPARE_QUERY("UPDATE organizations SET orgz_percentage = :Percentage WHERE orgz_id = :OrganizationID");
//-----------------------------------------------------------------------------
ISOrganizationListForm::ISOrganizationListForm(QWidget *parent) : ISListBaseForm(ISMetaData::GetInstanse().GetMetaTable("Organizations"), parent)
{
	QLabel *LabelUser = new QLabel(LOCALIZATION("User") + ":", GetToolBar());
	GetToolBar()->addWidget(LabelUser);

	ISUserEdit *UserEdit = new ISUserEdit(GetToolBar());
	connect(UserEdit, &ISComboEdit::ValueChange, this, &ISOrganizationListForm::UserChanged);
	GetToolBar()->addWidget(UserEdit);

	QAction *ActionTransfer = new QAction(GetToolBar());
	ActionTransfer->setText(LOCALIZATION("TransferInWork") + "...");
	ActionTransfer->setToolTip(LOCALIZATION("TransferInWork"));
	ActionTransfer->setIcon(ISPlugin::GetInstance().GetPluginInterface()->GetIcon("TransferOrganization"));
	connect(ActionTransfer, &QAction::triggered, this, &ISOrganizationListForm::Transfer);
	AddAction(ActionTransfer, false, true);

	QAction *ActionResetExecutor = new QAction(GetToolBar());
	ActionResetExecutor->setText(LOCALIZATION("ResetExecutor"));
	ActionResetExecutor->setToolTip(LOCALIZATION("ResetExecutor"));
	ActionResetExecutor->setIcon(ISPlugin::GetInstance().GetPluginInterface()->GetIcon("ResetOrganizationExecutor"));
	connect(ActionResetExecutor, &QAction::triggered, this, &ISOrganizationListForm::ResetExecutor);
	AddAction(ActionResetExecutor, true, true);

	QAction *ActionPercentage = new QAction(this);
	ActionPercentage->setText(LOCALIZATION("CalculatePercentageFill"));
	ActionPercentage->setToolTip(LOCALIZATION("CalculatePercentageFill"));
	ActionPercentage->setIcon(ISPlugin::GetInstance().GetPluginInterface()->GetIcon("Percentage"));
	connect(ActionPercentage, &QAction::triggered, this, &ISOrganizationListForm::Percentage);
	AddAction(ActionPercentage, false, true);
}
//-----------------------------------------------------------------------------
ISOrganizationListForm::~ISOrganizationListForm()
{

}
//-----------------------------------------------------------------------------
void ISOrganizationListForm::UserChanged(const QVariant &value)
{
	if (value.isValid())
	{
		GetQueryModel()->SetClassFilter(QString("orgz_user = %1").arg(value.toInt()));
	}
	else
	{
		GetQueryModel()->ClearClassFilter();
	}

	Update();
}
//-----------------------------------------------------------------------------
void ISOrganizationListForm::Transfer()
{
	QVector<int> Objects = GetSelectedIDs();
	if (!Objects.count())
	{
		ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.NotSelectedOrganization"));
		return;
	}

	ISTransferOrganizationForm TransferOrganizationForm(Objects.count());
	if (TransferOrganizationForm.Exec())
	{
		for (int i = 0; i < Objects.count(); i++)
		{
			int ObjectID = Objects.at(i);
			QString OrganizationName = GetRecordValue("Name", GetRowIndex(ObjectID)).toString();
			int UserID = TransferOrganizationForm.GetSelectedUserID();

			if (UserID)
			{
				ISQuery qUpdate(QU_ORGANIZATION_USER);
				qUpdate.BindValue(":UserID", TransferOrganizationForm.GetSelectedUserID());
				qUpdate.BindValue(":OrganizationID", ObjectID);
				if (qUpdate.Execute())
				{
					ISProtocol::Insert(true, "{BB6C46A0-C8E4-48F7-AC19-3A31D8FE7888}", GetMetaTable()->GetName(), GetMetaTable()->GetLocalListName(), GetObjectID(), LOCALIZATION("Protocol.Transfer").arg(OrganizationName).arg(TransferOrganizationForm.GetSelectedUserName()));
					ISNotify::GetInstance().SendNotification("{F436CA43-860B-4728-83E1-0F38A05A6282}", OrganizationName, true, TransferOrganizationForm.GetSelectedUserID());
				}
			}
			else
			{
				ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.NotSelectedUserWithTransfer"));
				return;
			}
		}

		ISMessageBox::ShowInformation(this, LOCALIZATION("Message.Information.TrasferOrganizationUserDone").arg(Objects.count()).arg(TransferOrganizationForm.GetSelectedUserName()));
		Update();
	}
}
//-----------------------------------------------------------------------------
void ISOrganizationListForm::ResetExecutor()
{
	if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.ResetOrganizationExecutor")))
	{
		ISQuery qUpdate(QU_RESET_EXECUTOR);
		qUpdate.BindValue(":OrganizationID", GetObjectID());
		if (qUpdate.Execute())
		{
			ISMessageBox::ShowInformation(this, LOCALIZATION("Message.Information.ResetOrganizationExecutor"));
			Update();
		}
	}
}
//-----------------------------------------------------------------------------
void ISOrganizationListForm::Percentage()
{
	QVector<int> Objects = GetSelectedIDs();
	if (!Objects.count())
	{
		ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.NotSelectedOrganization"));
		return;
	}

	ISProgressForm ProgressForm(0, Objects.count(), this);
	ProgressForm.show();

	for (int i = 0; i < Objects.count(); i++) //����� ���� �����������
	{
		ProgressForm.SetText(LOCALIZATION("CalculatePercentage").arg(i).arg(Objects.count()));
		ProgressForm.setValue(i);

		int CountFill = 0; //���������� ����������� �����
		int CountFields = GetMetaTable()->GetFields().count(); //���������� �����

		for (int j = 0; j < CountFields; j++) //����� �����
		{
			PMetaClassField *MetaField = GetMetaTable()->GetFields().at(j);
			QString FieldName = "orgz_" + MetaField->GetName();

			ISQuery qSelectField(QString("SELECT %1 FROM organizations WHERE orgz_id = :OrganizationID").arg(FieldName));
			qSelectField.BindValue(":OrganizationID", Objects.at(i));
			if (qSelectField.ExecuteFirst())
			{
				QVariant Value = qSelectField.ReadColumn(FieldName);
				if (!Value.isNull())
				{
					CountFill++;
				}
			}
		}

		double Percent = CountFill * 100 / CountFields;

		ISQuery qUpdatePercentage(QU_PERCENTAGE);
		qUpdatePercentage.BindValue(":Percentage", Percent);
		qUpdatePercentage.BindValue(":OrganizationID", Objects.at(i));
		qUpdatePercentage.Execute();

		if (ProgressForm.wasCanceled())
		{
			return;
		}
	}

	ProgressForm.close();
	ISMessageBox::ShowInformation(this, LOCALIZATION("Message.Information.CalculatePercantageOrganizations"));
	Update();
}
//-----------------------------------------------------------------------------
