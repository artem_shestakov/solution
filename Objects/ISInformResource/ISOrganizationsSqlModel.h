#pragma once
//-----------------------------------------------------------------------------
#include "isinformresource_global.h"
#include "ISSqlModelCore.h"
//-----------------------------------------------------------------------------
class ISINFORMRESOURCE_EXPORT ISOrganizationsSqlModel : public ISSqlModelCore
{
	Q_OBJECT

public:
	Q_INVOKABLE ISOrganizationsSqlModel(PMetaClassTable *meta_table, QObject *parent = 0);
	virtual ~ISOrganizationsSqlModel();

	QVariant data(const QModelIndex &ModelIndex, int Role) const;

private:
	QMap<QString, QColor> ColorStatus;
};
//-----------------------------------------------------------------------------
