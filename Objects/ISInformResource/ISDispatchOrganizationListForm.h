#pragma once
//-----------------------------------------------------------------------------
#include "isinformresource_global.h"
#include "ISListBaseForm.h"
//-----------------------------------------------------------------------------
class ISINFORMRESOURCE_EXPORT ISDispatchOrganizationListForm : public ISListBaseForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISDispatchOrganizationListForm(QWidget *parent = 0);
	virtual ~ISDispatchOrganizationListForm();

	void LoadData() override;

protected:
	void Picking();
	void Matched(const QVector<int> Vector);
};
//-----------------------------------------------------------------------------
