#include "StdAfx.h"
#include "ISDispatchOrganizationListForm.h"
#include "ISMetaData.h"
#include "ISControls.h"
#include "ISPickingObjectsListForm.h"
#include "ISQuery.h"
//-----------------------------------------------------------------------------
static QString QS_ORGANIZATION = PREPARE_QUERY("SELECT COUNT(*) FROM dispatchorganizations WHERE dorg_dispatch = :DispatchID AND dorg_organization = :OrganizationID");
//-----------------------------------------------------------------------------
static QString QI_ORGANIZATION = PREPARE_QUERY("INSERT INTO dispatchorganizations(dorg_dispatch, dorg_organization) "
											   "VALUES(:DispatchID, :OrganizationID)");
//-----------------------------------------------------------------------------
ISDispatchOrganizationListForm::ISDispatchOrganizationListForm(QWidget *parent) : ISListBaseForm(ISMetaData::GetInstanse().GetMetaTable("DispatchOrganizations"), parent)
{
	QAction *ActionPicking = ISControls::CreateActionPicking(this);
	connect(ActionPicking, &QAction::triggered, this, &ISDispatchOrganizationListForm::Picking);
	AddAction(ActionPicking, false, true);
}
//-----------------------------------------------------------------------------
ISDispatchOrganizationListForm::~ISDispatchOrganizationListForm()
{

}
//-----------------------------------------------------------------------------
void ISDispatchOrganizationListForm::LoadData()
{
	GetQueryModel()->SetClassFilter(QString("dorg_dispatch = %1").arg(GetParentObjectID()));
	ISListBaseForm::LoadData();
}
//-----------------------------------------------------------------------------
void ISDispatchOrganizationListForm::Picking()
{
	ISPickingObjectsListForm *PickingObjectsListForm = new ISPickingObjectsListForm(ISMetaData::GetInstanse().GetMetaTable("Organizations"));
	connect(PickingObjectsListForm, &ISPickingObjectsListForm::Matched, this, &ISDispatchOrganizationListForm::Matched);
	PickingObjectsListForm->show();
	PickingObjectsListForm->LoadData();
}
//-----------------------------------------------------------------------------
void ISDispatchOrganizationListForm::Matched(const QVector<int> Vector)
{
	for (int OrganizationID : Vector)
	{
		ISQuery qSelect(QS_ORGANIZATION);
		qSelect.BindValue(":DispatchID", GetParentObjectID());
		qSelect.BindValue(":OrganizationID", OrganizationID);
		if (qSelect.ExecuteFirst())
		{
			int Count = qSelect.ReadColumn("count").toInt();
			if (!Count)
			{
				ISQuery qInsert(QI_ORGANIZATION);
				qInsert.BindValue(":DispatchID", GetParentObjectID());
				qInsert.BindValue(":OrganizationID", OrganizationID);
				qInsert.Execute();
			}
		}
	}

	if (Vector.count())
	{
		Update();
	}
}
//-----------------------------------------------------------------------------
