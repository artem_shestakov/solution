#pragma once
//-----------------------------------------------------------------------------
#include "isinformresource_global.h"
#include "ISPluginInterface.h"
//-----------------------------------------------------------------------------
class ISInformResource : public QObject, public ISPluginInterface
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "InformResource" FILE "InformResource.json")
	Q_INTERFACES(ISPluginInterface)

public:
	~ISInformResource();

	void RegisterMetaTypes() const;
};
//-----------------------------------------------------------------------------
