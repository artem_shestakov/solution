#pragma once
//-----------------------------------------------------------------------------
#include "iscenterseven_global.h"
#include "ISPluginInterface.h"
//-----------------------------------------------------------------------------
class ISCenterSeven : public QObject, public ISPluginInterface
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "CenterSeven" FILE "CenterSeven.json")
	Q_INTERFACES(ISPluginInterface)

public:
	~ISCenterSeven();

	void RegisterMetaTypes() const;
};
//-----------------------------------------------------------------------------
