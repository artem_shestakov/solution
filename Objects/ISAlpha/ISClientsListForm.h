#pragma once
//-----------------------------------------------------------------------------
#include "isalpha_global.h"
#include "ISListBaseForm.h"
//-----------------------------------------------------------------------------
class ISALPHA_EXPORT ISClientsListForm : public ISListBaseForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISClientsListForm(QWidget *parent = 0);
	virtual ~ISClientsListForm();
};
//-----------------------------------------------------------------------------
