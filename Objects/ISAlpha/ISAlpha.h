#pragma once
//-----------------------------------------------------------------------------
#include "isalpha_global.h"
#include "ISPluginInterface.h"
//-----------------------------------------------------------------------------
class ISAlpha : public QObject, public ISPluginInterface
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "Alpha" FILE "Alpha.json")
	Q_INTERFACES(ISPluginInterface)

public:
	~ISAlpha();

	void RegisterMetaTypes() const;
};
//-----------------------------------------------------------------------------
