#include "StdAfx.h"
#include "ISAlpha.h"
#include "EXDefines.h"
#include "ISAssert.h"
//-----------------------------------------------------------------------------
#include "ISDeviceObjectForm.h"
#include "ISDeviceListForm.h"
#include "ISClientsListForm.h"
#include "ISDeviceSubSystem.h"
#include "ISClientsSqlModel.h"
//-----------------------------------------------------------------------------
ISAlpha::~ISAlpha()
{

}
//-----------------------------------------------------------------------------
void ISAlpha::RegisterMetaTypes() const
{
	qRegisterMetaType<ISDeviceObjectForm*>("ISDeviceObjectForm");
	qRegisterMetaType<ISDeviceListForm*>("ISDeviceListForm");
	qRegisterMetaType<ISClientsListForm*>("ISClientsListForm");
	qRegisterMetaType<ISDeviceSubSystem*>("ISDeviceSubSystem");
	qRegisterMetaType<ISClientsSqlModel*>("ISClientsSqlModel");
}
//-----------------------------------------------------------------------------
