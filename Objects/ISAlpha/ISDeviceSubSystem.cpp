#include "StdAfx.h"
#include "ISDeviceSubSystem.h"
#include "ISMetaData.h"
//-----------------------------------------------------------------------------
ISDeviceSubSystem::ISDeviceSubSystem(QWidget *parent) : ISListBaseForm(ISMetaData::GetInstanse().GetMetaTable("Device"), parent)
{
	GetQueryModel()->ClearClassFilter();
}
//-----------------------------------------------------------------------------
ISDeviceSubSystem::~ISDeviceSubSystem()
{

}
//-----------------------------------------------------------------------------
