#include "StdAfx.h"
#include "ISClientsSqlModel.h"
//-----------------------------------------------------------------------------
ISClientsSqlModel::ISClientsSqlModel(PMetaClassTable *meta_table, QObject *parent) : ISSqlModelCore(meta_table, parent)
{

}
//-----------------------------------------------------------------------------
ISClientsSqlModel::~ISClientsSqlModel()
{

}
//-----------------------------------------------------------------------------
QVariant ISClientsSqlModel::data(const QModelIndex &ModelIndex, int Role) const
{
	QVariant Value = ISSqlModelCore::data(ModelIndex, Role);

	if (Role == Qt::TextColorRole)
	{
		int CountInWork = GetRecord(ModelIndex.row()).value("CountInWork").toInt();
		if (CountInWork)
		{
			Value = qVariantFromValue(QColor(Qt::red));
		}
		else
		{
			Value = qVariantFromValue(QColor(Qt::darkGreen));
		}
	}

	return Value;
}
//-----------------------------------------------------------------------------
