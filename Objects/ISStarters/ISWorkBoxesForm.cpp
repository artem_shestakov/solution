#include "StdAfx.h"
#include "ISWorkBoxesForm.h"
#include "ISAssert.h"
#include "ISMessageBox.h"
#include "ISQuery.h"
#include "EXDefines.h"
#include "ISCore.h"
#include "ISLocalization.h"
#include "ISAppendTaskForm.h"
#include "ISSystem.h"
#include "ISControls.h"
#include "ISPushButton.h"
//-----------------------------------------------------------------------------
static QString QS_BOXES_COUNT = PREPARE_QUERY("SELECT COUNT(*) FROM boxes WHERE NOT boxs_isdeleted");
//-----------------------------------------------------------------------------
static QString QS_BOXES = PREPARE_QUERY("SELECT b.boxs_id, b.boxs_name "
										"FROM boxes b "
										"WHERE NOT b.boxs_isdeleted "
										"ORDER BY b.boxs_order");
//-----------------------------------------------------------------------------
static QString QS_TASKS = PREPARE_QUERY("SELECT w.wkcr_id, w.wkcr_box, w.wkcr_timestart, w.wkcr_timestop, w.wkcr_allday, w.wkcr_importance, w.wkcr_car, w.wkcr_description "
										"FROM workcalendar w "
										"WHERE NOT w.wkcr_isdeleted "
										"AND w.wkcr_date = :SelectedDate "
										"ORDER BY w.wkcr_box");
//-----------------------------------------------------------------------------
static QString QI_WORK_CALENDAR = PREPARE_QUERY("INSERT INTO workcalendar(wkcr_date, wkcr_box, wkcr_timestart, wkcr_timestop, wkcr_allday, wkcr_importance, wkcr_car, wkcr_description) "
												"VALUES(:Date, :BoxID, :TimeStart, :TimeStop, :AllDay, :Importance, :Car, :Description) "
												"RETURNING wkcr_id");
//-----------------------------------------------------------------------------
static QString QU_WORK_CALENDAR = PREPARE_QUERY("UPDATE workcalendar SET "
												"wkcr_timestart = :TimeStart, "
												"wkcr_timestop = :TimeStop, "
												"wkcr_allday = :AllDay, "
												"wkcr_importance = :Importance, "
												"wkcr_car = :Car, "
												"wkcr_description = :Description "
												"WHERE wkcr_id = :ID");
//-----------------------------------------------------------------------------
ISWorkBoxesForm::ISWorkBoxesForm(QWidget *parent) : ISInterfaceForm(parent)
{
	this->TableTime = nullptr;

	GetMainLayout()->setContentsMargins(LAYOUT_MARGINS_10_PX);

	Layout = new QHBoxLayout();
	GetMainLayout()->addLayout(Layout);

	CreateBoxTable();
	CreateRightPanel();

	CalendarDateChanged();
}
//-----------------------------------------------------------------------------
ISWorkBoxesForm::~ISWorkBoxesForm()
{

}
//-----------------------------------------------------------------------------
void ISWorkBoxesForm::CellDoubleClicked(int Row, int Column)
{
	QWidget *CellWidget = TableTime->cellWidget(Row, Column);
	
	if (CellWidget)
	{
		ISTaskWidget *CellWidgetTask = dynamic_cast<ISTaskWidget*>(CellWidget);
		IS_ASSERT(CellWidgetTask, "Not dynamic_cast ISTaskWidget");
	}
	else
	{
		ISAppendTaskForm AppendClientForm(nullptr, this);
		if (!AppendClientForm.Exec())
		{
			return;
		}

		QString TimeRange = QString();
		QVariant TimeStart = QVariant();
		QVariant TimeStop = QVariant();
		bool AllDay = false;
		QString Importance = AppendClientForm.GetImportance();
		QString Car = AppendClientForm.GetCar();
		QString Description = AppendClientForm.GetDescription();

		int Minute = AppendClientForm.GetMinute();
		int CountCell = 0;

		if (Minute)
		{
			CountCell = Minute / 30;

			TimeStart = TableTime->verticalHeaderItem(Row)->text();
			TimeStop = TableTime->verticalHeaderItem(Row + CountCell)->text();
			TimeRange = TimeStart.toString() + " - " + TimeStop.toString();

			CountCell++;
		}
		else
		{
			TimeRange = LOCALIZATION("AllDay");
			AllDay = true;
		}

		ISQuery qInsert(QI_WORK_CALENDAR);
		qInsert.BindValue(":Date", CalendarWidget->selectedDate());
		qInsert.BindValue(":BoxID", Columns.value(Column));
		qInsert.BindValue(":TimeStart", TimeStart);
		qInsert.BindValue(":TimeStop", TimeStop);
		qInsert.BindValue(":AllDay", AllDay);
		qInsert.BindValue(":Importance", Importance);
		qInsert.BindValue(":Car", Car);
		qInsert.BindValue(":Description", Description);
		if (qInsert.ExecuteFirst())
		{
			int ID = qInsert.ReadColumn("wkcr_id").toInt();

			CreateTaskWidget(Row, Column, CountCell, TimeRange, Minute, Importance, Car, Description, ID);
		}
	}
}
//-----------------------------------------------------------------------------
void ISWorkBoxesForm::CreateTaskWidget(int Row, int Column, int CountCell, const QString &TimeRange, int Minute, const QString &Importance, const QString &Car, const QString Description, int TaskID)
{
	TableTime->setSpan(Row, Column, CountCell, 1);

	ISTaskWidget *TaskWidget = new ISTaskWidget(TableTime);
	//connect(TaskWidget, &ISTaskWidget::Edit, this, &ISWorkBoxesForm::Edit);
	//connect(TaskWidget, &ISTaskWidget::Update, this, &ISWorkBoxesForm::Update);
	TaskWidget->SetTimeRange(TimeRange);
	TaskWidget->SetMinute(Minute);
	TaskWidget->SetImportance(Importance);
	TaskWidget->SetCar(Car);
	TaskWidget->SetDescription(Description);
	TaskWidget->SetTaskID(TaskID);
	TableTime->setCellWidget(Row, Column, TaskWidget);
}
//-----------------------------------------------------------------------------
void ISWorkBoxesForm::CreateBoxTable()
{
	if (TableTime)
	{
		delete TableTime;
		TableTime = nullptr;
	}

	TableTime = new ISTableTimeWidget(30, this);
	TableTime->setCursor(CURSOR_POINTING_HAND);
	connect(TableTime, &ISTableTimeWidget::cellDoubleClicked, this, &ISWorkBoxesForm::CellDoubleClicked);
	Layout->insertWidget(0, TableTime);

	ISQuery qBoxesCount(QS_BOXES_COUNT);
	if (qBoxesCount.ExecuteFirst())
	{
		int BoxesCount = qBoxesCount.ReadColumn("count").toInt();
		if (BoxesCount)
		{
			TableTime->setColumnCount(BoxesCount);
		}
		else
		{
			TableTime->setVisible(false);
			Layout->addStretch();

			ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.BoxesIsNull"));
		}
	}

	ISQuery qSelectBoxes(QS_BOXES);
	if (qSelectBoxes.Execute())
	{
		int Index = 0;

		while (qSelectBoxes.Next())
		{
			int BoxID = qSelectBoxes.ReadColumn("boxs_id").toInt();
			QString BoxName = qSelectBoxes.ReadColumn("boxs_name").toString();

			TableTime->setHorizontalHeaderItem(Index, new QTableWidgetItem(BoxName));
			Columns.insert(Index, BoxID);

			Index++;
		}
	}

	TableTime->resizeColumnsToContents();

	for (int i = 0; i < TableTime->columnCount(); i++)
	{
		int Width = TableTime->columnWidth(i);
		int NewWidth = Width * 2;
		TableTime->setColumnWidth(i, NewWidth);
	}

	for (int i = 0; i < TableTime->rowCount(); i++)
	{
		for (int j = 0; j < TableTime->columnCount(); j++)
		{
			QTableWidgetItem *Item = new QTableWidgetItem();
			Item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
			TableTime->setItem(i, j, Item);
		}
	}
}
//-----------------------------------------------------------------------------
void ISWorkBoxesForm::CreateRightPanel()
{
	QVBoxLayout *RightLayout = new QVBoxLayout();

	QWidget *WidgetRight = new QWidget(this);
	WidgetRight->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Expanding);
	WidgetRight->setLayout(RightLayout);
	Layout->addWidget(WidgetRight);

	QGroupBox *GroupBoxCalendar = new QGroupBox(WidgetRight);
	GroupBoxCalendar->setTitle(LOCALIZATION("Calendar"));
	GroupBoxCalendar->setLayout(new QVBoxLayout());
	RightLayout->addWidget(GroupBoxCalendar);

	CalendarWidget = new QCalendarWidget(GroupBoxCalendar);
	CalendarWidget->setGridVisible(true);
	CalendarWidget->setCursor(CURSOR_POINTING_HAND);
	CalendarWidget->setVerticalHeaderFormat(QCalendarWidget::NoVerticalHeader);
	connect(CalendarWidget, &QCalendarWidget::selectionChanged, this, &ISWorkBoxesForm::CalendarDateChanged);
	GroupBoxCalendar->layout()->addWidget(CalendarWidget);

	LabelCurrentDate = new QLabel(this);
	LabelCurrentDate->setFont(FONT_APPLICATION_BOLD);
	LabelCurrentDate->setText(LOCALIZATION("SelectedDate") + ": " + CalendarWidget->selectedDate().toString(DATE_FORMAT_STRING_V1));
	RightLayout->addWidget(LabelCurrentDate);

	ISPushButton *ButtonCurrentDate = new ISPushButton(this);
	ButtonCurrentDate->setText(LOCALIZATION("ToCurrentDate"));
	connect(ButtonCurrentDate, &ISPushButton::clicked, [=] { CalendarWidget->setSelectedDate(QDate::currentDate()); CalendarWidget->setFocus(); });
	RightLayout->addWidget(ButtonCurrentDate, 0, Qt::AlignRight);

	RightLayout->addWidget(ISControls::CreateHorizontalLine(this));

	QGroupBox *GroupBoxCheckBoxes = new QGroupBox(WidgetRight);
	GroupBoxCheckBoxes->setTitle(LOCALIZATION("VisibleBoxes"));
	GroupBoxCheckBoxes->setLayout(new QVBoxLayout());
	RightLayout->addWidget(GroupBoxCheckBoxes);

	for (int i = 0; i < TableTime->columnCount(); i++)
	{
		QString ColumnName = TableTime->horizontalHeaderItem(i)->text();

		QCheckBox *CheckBox = new QCheckBox(GroupBoxCheckBoxes);
		CheckBox->setChecked(true);
		CheckBox->setText(ColumnName);
		CheckBox->setProperty("Index", i);
		CheckBox->setCursor(CURSOR_POINTING_HAND);
		connect(CheckBox, &QCheckBox::stateChanged, this, &ISWorkBoxesForm::CheckBoxVisibleChanged);
		GroupBoxCheckBoxes->layout()->addWidget(CheckBox);
	}

	RightLayout->addWidget(ISControls::CreateHorizontalLine(this));

	RightLayout->addStretch();
}
//-----------------------------------------------------------------------------
void ISWorkBoxesForm::LoadTasks(const QDate &Date)
{
	CreateBoxTable();

	ISQuery qSelect(QS_TASKS);
	qSelect.BindValue(":SelectedDate", Date);
	if (qSelect.Execute())
	{
		while (qSelect.Next())
		{
			int TaskID = qSelect.ReadColumn("wkcr_id").toInt();
			int BoxID = qSelect.ReadColumn("wkcr_box").toInt();
			QTime TimeStart = qSelect.ReadColumn("wkcr_timestart").toTime();
			QTime TimeStop = qSelect.ReadColumn("wkcr_timestop").toTime();
			bool AllDay = qSelect.ReadColumn("wkcr_allday").toBool();
			QString Inportance = qSelect.ReadColumn("wkcr_importance").toString();
			QString Car = qSelect.ReadColumn("wkcr_car").toString();
			QString Description = qSelect.ReadColumn("wkcr_description").toString();

			int RowIndex = TableTime->GetIndexRowFromTime(TimeStart);
			int CountCell = (ISSystem::TimeFromMinutes(TimeStop) - ISSystem::TimeFromMinutes(TimeStart)) / 30;
			CountCell++;

			int Minute = ISSystem::TimeFromMinutes(TimeStop) - ISSystem::TimeFromMinutes(TimeStart);

			CreateTaskWidget(RowIndex, Columns.key(BoxID), CountCell, TimeStart.toString(TIME_FORMAT_STRING_V1) + " - " + TimeStop.toString(TIME_FORMAT_STRING_V1), Minute, Inportance, Car, Description, TaskID);
		}
	}
}
//-----------------------------------------------------------------------------
void ISWorkBoxesForm::Update()
{
	LoadTasks(CalendarWidget->selectedDate());
}
//-----------------------------------------------------------------------------
void ISWorkBoxesForm::Edit(ISTaskWidget *TaskWidget)
{
	ISAppendTaskForm *AppendTaskForm = new ISAppendTaskForm(TaskWidget, this);
	if (AppendTaskForm->Exec())
	{
		QVariant TimeStart = QVariant();
		QVariant TimeStop = QVariant();
		bool AllDay = false;
		int Minute = AppendTaskForm->GetMinute();
		int CountCell = 0;

		if (Minute)
		{
			int Row = -1;

			for (int i = 0; i < TableTime->rowCount(); i++)
			{
				for (int j = 0; j < TableTime->columnCount(); j++)
				{
					if (dynamic_cast<ISTaskWidget*>(TableTime->cellWidget(i, j)) == TaskWidget)
					{
						Row = i;
						break;
					}
				}

				if (Row != -1)
				{
					break;
				}
			}

			CountCell = Minute / 30;

			TimeStart = TableTime->verticalHeaderItem(Row)->text();
			TimeStop = TableTime->verticalHeaderItem(Row + CountCell)->text();
		}
		else
		{
			AllDay = true;
		}

		ISQuery qUpdate(QU_WORK_CALENDAR);
		qUpdate.BindValue(":TimeStart", TimeStart);
		qUpdate.BindValue(":TimeStop", TimeStop);
		qUpdate.BindValue(":AllDay", AllDay);
		qUpdate.BindValue(":Importance", AppendTaskForm->GetImportance());
		qUpdate.BindValue(":Car", AppendTaskForm->GetCar());
		qUpdate.BindValue(":Description", AppendTaskForm->GetDescription());
		qUpdate.BindValue(":ID", TaskWidget->GetTaskID());
		if (qUpdate.Execute())
		{
			//OnUpdate();
		}
	}
}
//-----------------------------------------------------------------------------
void ISWorkBoxesForm::CalendarDateChanged()
{
	QDate SelectedDate = CalendarWidget->selectedDate();

	LabelCurrentDate->setText(LOCALIZATION("SelectedDate") + ": " + SelectedDate.toString(DATE_FORMAT_STRING_V1));
	LoadTasks(SelectedDate);
}
//-----------------------------------------------------------------------------
void ISWorkBoxesForm::CheckBoxVisibleChanged(int State)
{
	QCheckBox *CheckBox = dynamic_cast<QCheckBox*>(sender());
	if (CheckBox)
	{
		int ColumnIndex = CheckBox->property("Index").toInt();
		if (CheckBox->isChecked())
		{
			TableTime->showColumn(ColumnIndex);
		}
		else
		{
			TableTime->hideColumn(ColumnIndex);
		}
	}
}
//-----------------------------------------------------------------------------
