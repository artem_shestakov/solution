#include "StdAfx.h"
#include "ISCarsObjectForm.h"
#include "EXDefines.h"
#include "ISLocalization.h"
#include "ISCarSelectForm.h"
#include "ISPlugin.h"
//-----------------------------------------------------------------------------
ISCarsObjectForm::ISCarsObjectForm(ISNamespace::ObjectFormType form_type, PMetaClassTable *meta_table, QWidget *parent, int object_id) : ISObjectFormBase(form_type, meta_table, parent, object_id)
{
	EditCarStringName = dynamic_cast<ISLineEdit*>(GetFieldWidget("CarStringName"));
	//SetVisibleField("CarStringName", false);

	EditListMark = dynamic_cast<ISListEdit*>(GetFieldWidget("Mark"));
	EditListMark->setEnabled(false);

	EditListModel = dynamic_cast<ISListEdit*>(GetFieldWidget("Model"));
	EditListModel->setEnabled(false);

	QAction *ActionSelectCar = new QAction(this);
	ActionSelectCar->setText(LOCALIZATION("SelectCar"));
	ActionSelectCar->setToolTip(LOCALIZATION("SelectCar"));
	ActionSelectCar->setIcon(ISPlugin::GetInstance().GetPluginInterface()->GetIcon("CarSelect"));
	connect(ActionSelectCar, &QAction::triggered, this, &ISCarsObjectForm::CarSelect);
	//AddActionToPanelButtons(ActionSelectCar);
}
//-----------------------------------------------------------------------------
ISCarsObjectForm::~ISCarsObjectForm()
{

}
//-----------------------------------------------------------------------------
void ISCarsObjectForm::CarSelect()
{
	ISCarSelectForm CarSelectForm;
	CarSelectForm.SelectMark(EditListMark->GetValue().toInt());
	CarSelectForm.SelectModel(EditListModel->GetValue().toInt());
	if (CarSelectForm.Exec())
	{
		QString MarkName = CarSelectForm.GetMarkName();
		QString ModelName = CarSelectForm.GetModelName();

		EditListMark->SetValue(CarSelectForm.GetMarkID());
		EditListModel->SetValue(CarSelectForm.GetModelID());
		EditCarStringName->SetValue(MarkName + " " + ModelName);
	}
}
//-----------------------------------------------------------------------------
void ISCarsObjectForm::AfterShowEvent()
{
	ISObjectFormBase::AfterShowEvent();

	EditCarStringName->SetValue(GetFieldValue("CarStringName"));
}
//-----------------------------------------------------------------------------
