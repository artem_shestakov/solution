#include "StdAfx.h"
#include "ISUsedPartsObjectForm.h"
#include "ISQuery.h"
#include "ISMessageBox.h"
#include "ISLocalization.h"
//-----------------------------------------------------------------------------
static QString QS_COUNT_NOMENCLATURE = PREPARE_QUERY("SELECT nmcl_stockremainder FROM nomenclature WHERE nmcl_id = :NomenclatureID");
//-----------------------------------------------------------------------------
static QString QU_COUNT_NOMENCLATURE = PREPARE_QUERY("UPDATE nomenclature SET nmcl_stockremainder = (SELECT nmcl_stockremainder - :Count FROM nomenclature WHERE nmcl_id = :NomenclatureID) WHERE nmcl_id = :NomenclatureID");
//-----------------------------------------------------------------------------
static QString QD_USED_PARTS = PREPARE_QUERY("DELETE FROM usedparts WHERE uspr_id = :ObjectID");
//-----------------------------------------------------------------------------
ISUsedPartsObjectForm::ISUsedPartsObjectForm(ISNamespace::ObjectFormType form_type, PMetaClassTable *meta_table, QWidget *parent, int object_id) : ISObjectFormBase(form_type, meta_table, parent, object_id)
{
	EditListNomenclature = dynamic_cast<ISListEdit*>(GetFieldWidget("Nomenclature"));
	EditCount = dynamic_cast<ISIntegerEdit*>(GetFieldWidget("Count"));
}
//-----------------------------------------------------------------------------
ISUsedPartsObjectForm::~ISUsedPartsObjectForm()
{

}
//-----------------------------------------------------------------------------
bool ISUsedPartsObjectForm::Save()
{
	bool Result = false;

	ISQuery qSelect(QS_COUNT_NOMENCLATURE);
	qSelect.BindValue(":NomenclatureID", EditListNomenclature->GetValue());
	if (qSelect.ExecuteFirst())
	{
		int StockRemainder = qSelect.ReadColumn("nmcl_stockremainder").toInt();
		int Count = EditCount->GetValue().toInt();

		if (Count > StockRemainder)
		{
			ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.MuchOutStock").arg(StockRemainder).arg(Count));
		}
		else
		{
			Result = ISObjectFormBase::Save();

			if (Result)
			{
				ISQuery qUpdate(QU_COUNT_NOMENCLATURE);
				qUpdate.BindValue(":Count", Count);
				qUpdate.BindValue(":NomenclatureID", EditListNomenclature->GetValue());
				if (!qUpdate.Execute())
				{
					ISMessageBox::ShowCritical(this, LOCALIZATION("Message.Critical.FailedToWriteNomenclature"));

					ISQuery qDelete(QD_USED_PARTS);
					qDelete.BindValue(":ObjectID", GetObjectID());
					qDelete.Execute();
				}
			}
		}
	}

	return Result;
}
//-----------------------------------------------------------------------------
