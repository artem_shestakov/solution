#include "StdAfx.h"
#include "ISClientsObjectForm.h"
#include "ISQuery.h"
#include "ISMessageBox.h"
#include "ISLocalization.h"
#include "ISCore.h"
#include "ISMemoryObjects.h"
//-----------------------------------------------------------------------------
static QString QS_CLIENT = PREPARE_QUERY("SELECT clts_id FROM clients WHERE clts_surname = :Surname AND clts_name = :Name AND clts_patronymic = :Patronymic");
//-----------------------------------------------------------------------------
ISClientsObjectForm::ISClientsObjectForm(ISNamespace::ObjectFormType form_type, PMetaClassTable *meta_table, QWidget *parent, int object_id) : ISObjectFormBase(form_type, meta_table, parent, object_id)
{
	Cheched = false;

	if (GetFormType() == ISNamespace::ObjectFormType::OFT_New)
	{
		EditSurname = GetFieldWidget("Surname");
		connect(EditSurname, &ISFieldEditBase::ValueChange, this, &ISClientsObjectForm::CheckClient);

		EditName = GetFieldWidget("Name");
		connect(EditName, &ISFieldEditBase::ValueChange, this, &ISClientsObjectForm::CheckClient);

		EditPatronymic = GetFieldWidget("Patronymic");
		connect(EditPatronymic, &ISFieldEditBase::ValueChange, this, &ISClientsObjectForm::CheckClient);
	}
	else
	{
		EditSurname = nullptr;
		EditName = nullptr;
		EditPatronymic = nullptr;
	}
}
//-----------------------------------------------------------------------------
ISClientsObjectForm::~ISClientsObjectForm()
{

}
//-----------------------------------------------------------------------------
void ISClientsObjectForm::CheckClient()
{
	if (!Cheched)
	{
		ISQuery qSelect(QS_CLIENT);
		qSelect.BindValue(":Surname", EditSurname->GetValue());
		qSelect.BindValue(":Name", EditName->GetValue());
		qSelect.BindValue(":Patronymic", EditPatronymic->GetValue());
		if (qSelect.ExecuteFirst())
		{
			if (qSelect.GetCountResultRows())
			{
				int ClientID = qSelect.ReadColumn("clts_id").toInt();
				QString FullName = EditSurname->GetValue().toString() + " " + EditName->GetValue().toString() + " " + EditPatronymic->GetValue().toString();

				if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.ClientAlreadyExist").arg(FullName)))
				{
					ISObjectFormBase *ObjectForm = ISCore::CreateObjectForm(ISNamespace::ObjectFormType::OFT_Edit, GetMetaTable(), ClientID);
					QMetaObject::invokeMethod(ISMemoryObjects::GetInstance().GetWorkspaceForm(), "AddObjectForm", Q_ARG(QWidget *, ObjectForm));
				}

				Cheched = true;
				SetModificationFlag(false);
				close();
			}
		}
	}
}
//-----------------------------------------------------------------------------
