#include "StdAfx.h"
#include "ISStarters.h"
//-----------------------------------------------------------------------------
#include "ISNomenclatureListForm.h"
#include "ISNomenclatureSupplyListObjectForm.h"
#include "ISClientsListForm.h"
#include "ISCarsObjectForm.h"
#include "ISWorkBoxesForm.h"
#include "ISAggregatesObjectForm.h"
#include "ISRemontObjectForm.h"
#include "ISServiceRenderedObjectForm.h"
#include "ISClientsObjectForm.h"
#include "ISUsedPartsObjectForm.h"
#include "ISSupplyListForm.h"
#include "ISRemontListObjectForm.h"
//-----------------------------------------------------------------------------
ISStarters::~ISStarters()
{

}
//-----------------------------------------------------------------------------
void ISStarters::RegisterMetaTypes() const
{
	qRegisterMetaType<ISNomenclatureListForm*>("ISNomenclatureListForm");
	qRegisterMetaType<ISNomenclatureSupplyListObjectForm*>("ISNomenclatureSupplyListObjectForm");
	qRegisterMetaType<ISClientsListForm*>("ISClientsListForm");
	qRegisterMetaType<ISCarsObjectForm*>("ISCarsObjectForm");
	qRegisterMetaType<ISWorkBoxesForm*>("ISWorkBoxesForm");
	qRegisterMetaType<ISAggregatesObjectForm*>("ISAggregatesObjectForm");
	qRegisterMetaType<ISRemontObjectForm*>("ISRemontObjectForm");
	qRegisterMetaType<ISServiceRenderedObjectForm*>("ISServiceRenderedObjectForm");
	qRegisterMetaType<ISClientsObjectForm*>("ISClientsObjectForm");
	qRegisterMetaType<ISUsedPartsObjectForm*>("ISUsedPartsObjectForm");
	qRegisterMetaType<ISSupplyListForm*>("ISSupplyListForm");
	qRegisterMetaType<ISRemontListObjectForm*>("ISRemontListObjectForm");
}
//-----------------------------------------------------------------------------
