#include "StdAfx.h"
#include "ISSupplyListForm.h"
#include "ISLocalization.h"
#include "ISMessageBox.h"
#include "ISQuery.h"
#include "ISPlugin.h"
//-----------------------------------------------------------------------------
static QString QS_DEBIT = PREPARE_QUERY("SELECT nmcl_id, nmcl_name, nmcl_price, nmmv_count, nmmv_cost FROM nomenclaturesupply LEFT JOIN nomenclature ON nmcl_id = nmmv_nomenclature WHERE NOT nmmv_isdeleted AND nmmv_supply = :SupplyID ORDER BY nmmv_id");
//-----------------------------------------------------------------------------
static QString QU_NOMENCLATURE_COUNT = PREPARE_QUERY("UPDATE nomenclature SET nmcl_stockremainder = (SELECT COALESCE(max(nmcl_stockremainder), 0) FROM nomenclature WHERE nmcl_id = :NomenclatureID) + :CountDebit WHERE nmcl_id = :NomenclatureID");
//-----------------------------------------------------------------------------
static QString QU_SUPPLY_DEBIT = PREPARE_QUERY("UPDATE supply SET sply_debit = true");
//-----------------------------------------------------------------------------
static QString QU_NOMENCLATURE_DEBIT = PREPARE_QUERY("UPDATE nomenclaturesupply SET nmmv_debit = true WHERE nmmv_supply = :SupplyID");
//-----------------------------------------------------------------------------
static QString QU_NOMENCLATURE_PRICE = PREPARE_QUERY("UPDATE nomenclature SET nmcl_price = :Price WHERE nmcl_id = :NomenclatureID");
//-----------------------------------------------------------------------------
ISSupplyListForm::ISSupplyListForm(PMetaClassTable *MetaTable, QWidget *parent) : ISListBaseForm(MetaTable, parent)
{
	QAction *ActionDebit = new QAction(GetToolBar());
	ActionDebit->setText(LOCALIZATION("Debit"));
	ActionDebit->setToolTip(LOCALIZATION("Debit"));
	ActionDebit->setIcon(ISPlugin::GetInstance().GetPluginInterface()->GetIcon("Debit"));
	connect(ActionDebit, &QAction::triggered, this, &ISSupplyListForm::Debit);
	AddAction(ActionDebit);
}
//-----------------------------------------------------------------------------
ISSupplyListForm::~ISSupplyListForm()
{

}
//-----------------------------------------------------------------------------
void ISSupplyListForm::Debit()
{
	bool Debit = GetCurrentRecordValue("Debit").toBool();
	if (Debit)
	{
		ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.AlreadyDebit"));
		return;
	}

	if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.Debit")))
	{
		ISQuery qSelectDebit(QS_DEBIT);
		qSelectDebit.BindValue(":SupplyID", GetObjectID());
		if (qSelectDebit.Execute())
		{
			if (qSelectDebit.GetCountResultRows())
			{
				while (qSelectDebit.Next())
				{
					int NomenclatureID = qSelectDebit.ReadColumn("nmcl_id").toInt();
					QString NomenclatureName = qSelectDebit.ReadColumn("nmcl_name").toString();
					double NomenclaturePrice = qSelectDebit.ReadColumn("nmcl_price").toDouble();
					int Count = qSelectDebit.ReadColumn("nmmv_count").toInt();
					double Cost = qSelectDebit.ReadColumn("nmmv_cost").toDouble();

					ISQuery qUpdate(QU_NOMENCLATURE_COUNT);
					qUpdate.BindValue(":NomenclatureID", NomenclatureID);
					qUpdate.BindValue(":CountDebit", Count);
					if (qUpdate.Execute())
					{
						if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.SetupCostNomenclature").arg(NomenclatureName).arg(Cost).arg(NomenclaturePrice)))
						{
							ISQuery qUpdatePrice(QU_NOMENCLATURE_PRICE);
							qUpdatePrice.BindValue(":Price", Cost);
							qUpdatePrice.BindValue(":NomenclatureID", NomenclatureID);
							qUpdatePrice.Execute();
						}
					}
				}

				ISQuery qUpdate(QU_SUPPLY_DEBIT);
				qUpdate.Execute();

				ISQuery qUpdateNomenclSupply(QU_NOMENCLATURE_DEBIT);
				qUpdateNomenclSupply.BindValue(":SupplyID", GetObjectID());
				qUpdateNomenclSupply.Execute();

				Update();
			}
		}
	}
}
//-----------------------------------------------------------------------------
