#include "StdAfx.h"
#include "ISTaskWidget.h"
#include "EXDefines.h"
#include "ISMessageBox.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISCore.h"
#include "ISQuery.h"
#include "ISControls.h"
#include "ISPushButton.h"
//-----------------------------------------------------------------------------
static QString QD_TASK = PREPARE_QUERY("DELETE FROM workcalendar WHERE wkcr_id = :TaskID");
//-----------------------------------------------------------------------------
ISTaskWidget::ISTaskWidget(QWidget *parent) : QWidget(parent)
{
	this->Importance = QString();
	this->Car = QString();
	this->Description = QString();
	this->Minute = 0;
	this->TaskID = 0;

	QVBoxLayout *Layout = new QVBoxLayout();
	Layout->setContentsMargins(LAYOUT_MARGINS_2_PX);
	setLayout(Layout);

	LayoutGroupBox = new QVBoxLayout();
	LayoutGroupBox->setContentsMargins(LAYOUT_MARGINS_5_PX);

	GroupBox = new QGroupBox(this);
	GroupBox->setLayout(LayoutGroupBox);
	Layout->addWidget(GroupBox);

	CreateTitle();

	QFrame *Frame = ISControls::CreateHorizontalLine(GroupBox);
	Frame->setContentsMargins(0, 0, 0, 0);
	LayoutGroupBox->addWidget(Frame);

	CreateBottom();
}
//-----------------------------------------------------------------------------
ISTaskWidget::~ISTaskWidget()
{

}
//-----------------------------------------------------------------------------
void ISTaskWidget::SetTimeRange(const QString &TimeRange)
{
	LabelTime->setText(TimeRange);
}
//-----------------------------------------------------------------------------
QString ISTaskWidget::GetImportance() const
{
	return Importance;
}
//-----------------------------------------------------------------------------
void ISTaskWidget::SetImportance(const QString &importance)
{
	Importance = importance;
	QString ImportanceLocal = QString();

	if (Importance == "Low")
	{
		ImportanceLocal = LOCALIZATION("Importance.Low");
	}
	else if (Importance == "Ordinary")
	{
		ImportanceLocal = LOCALIZATION("Importance.Ordinary");
	}
	else if (Importance == "High")
	{
		ImportanceLocal = LOCALIZATION("Importance.High");
	}

	LabelImportance->setText(LOCALIZATION("Importance") + ": " + ImportanceLocal);
}
//-----------------------------------------------------------------------------
int ISTaskWidget::GetMinute() const
{
	return Minute;
}
//-----------------------------------------------------------------------------
void ISTaskWidget::SetMinute(int minute)
{
	Minute = minute;
}
//-----------------------------------------------------------------------------
QString ISTaskWidget::GetCar() const
{
	return Car;
}
//-----------------------------------------------------------------------------
void ISTaskWidget::SetCar(const QString &car)
{
	Car = car;
	LabelCar->setText(car);
	LabelCar->adjustSize();
}
//-----------------------------------------------------------------------------
QString ISTaskWidget::GetDescription() const
{
	return Description;
}
//-----------------------------------------------------------------------------
void ISTaskWidget::SetDescription(const QString &description)
{
	Description = description;
	LabelDescription->setText(Description);
	LabelDescription->adjustSize();
}
//-----------------------------------------------------------------------------
int ISTaskWidget::GetTaskID() const
{
	return TaskID;
}
//-----------------------------------------------------------------------------
void ISTaskWidget::SetTaskID(int task_id)
{
	TaskID = task_id;
}
//-----------------------------------------------------------------------------
void ISTaskWidget::CreateTitle()
{
	QHBoxLayout *LayoutTime = new QHBoxLayout();
	LayoutGroupBox->addLayout(LayoutTime);

	QLabel *LabelImageTime = new QLabel(GroupBox);
	LabelImageTime->setPixmap(BUFFER_ICONS("Time").pixmap(SIZE_16_16));
	LayoutTime->addWidget(LabelImageTime);

	LabelTime = new QLabel(GroupBox);
	LabelTime->setFont(FONT_APPLICATION_BOLD);
	LayoutTime->addWidget(LabelTime);

	LayoutTime->addStretch();

	ISPushButton *ButtonEdit = new ISPushButton(GroupBox);
	ButtonEdit->setText(LOCALIZATION("Options") + "...");
	LayoutTime->addWidget(ButtonEdit);

	QMenu *Menu = new QMenu(ButtonEdit);
	ButtonEdit->setMenu(Menu);

	QAction *ActionEdit = new QAction(Menu);
	ActionEdit->setText(LOCALIZATION("Edit"));
	//connect(ActionEdit, &QAction::triggered, this, &ISTaskWidget::Edit);
	Menu->addAction(ActionEdit);

	QAction *ActionDelete = new QAction(Menu);
	ActionDelete->setText(LOCALIZATION("Delete"));
	connect(ActionDelete, &QAction::triggered, this, &ISTaskWidget::Delete);
	Menu->addAction(ActionDelete);
}
//-----------------------------------------------------------------------------
void ISTaskWidget::CreateBottom()
{
	LabelImportance = new QLabel(GroupBox);
	LayoutGroupBox->addWidget(LabelImportance);

	LabelCar = new QLabel(this);
	LabelCar->setFont(FONT_APPLICATION_BOLD);
	LayoutGroupBox->addWidget(LabelCar);

	LabelDescription = new QLabel(GroupBox);
	LabelDescription->setWordWrap(true);
	LayoutGroupBox->addWidget(LabelDescription);

	LayoutGroupBox->addStretch();
}
//-----------------------------------------------------------------------------
void ISTaskWidget::Edit()
{
	emit Edit(this);
}
//-----------------------------------------------------------------------------
void ISTaskWidget::Delete()
{
	if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.DeleteTask")))
	{
		ISQuery qDelete(QD_TASK);
		qDelete.BindValue(":TaskID", TaskID);
		if (qDelete.Execute())
		{
			emit Update();
		}
	}
}
//-----------------------------------------------------------------------------
