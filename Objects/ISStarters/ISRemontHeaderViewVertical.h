#pragma once
//-----------------------------------------------------------------------------
#include "isstarters_global.h"
#include "ISSqlModelCore.h"
//-----------------------------------------------------------------------------
class ISSTARTERS_EXPORT ISRemontHeaderViewVertical : public QHeaderView
{
	Q_OBJECT

public:
	ISRemontHeaderViewVertical(ISSqlModelCore *SqlModel, QWidget *parent = 0);
	virtual ~ISRemontHeaderViewVertical();

	QSize sectionSizeFromContents(int LogicalIndex) const;

protected:
	void paintSection(QPainter *Painter, const QRect &Rect, int LogicalIndex) const;

private:
	ISSqlModelCore *Model;

	QPixmap PixmapPay;
	QPixmap PixmapNotPay;
};
//-----------------------------------------------------------------------------
