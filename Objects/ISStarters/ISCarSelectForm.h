#pragma once
//-----------------------------------------------------------------------------
#include "isstarters_global.h"
#include "ISInterfaceDialogForm.h"
#include "ISButtonDialog.h"
#include "ISLineEdit.h"
//-----------------------------------------------------------------------------
class ISSTARTERS_EXPORT ISCarSelectForm : public ISInterfaceDialogForm
{
	Q_OBJECT

public:
	ISCarSelectForm(QWidget *parent = 0);
	virtual ~ISCarSelectForm();

	QString GetMarkName() const;
	int GetMarkID() const;

	QString GetModelName() const;
	int GetModelID() const;

	void SelectMark(int ID);
	void SelectModel(int ID);

protected:
	void SelectedMark(QListWidgetItem *Item);
	void SelectedModel(QListWidgetItem *Item);
	
	void DoubleClickedModel(QListWidgetItem *Item);

	void LoadCars();
	void LoadModels(int MarkID);
	void Select();

protected slots:
	void SearchMark(const QVariant &Value);
	void SearchModel(const QVariant &Value);

private:
	ISLineEdit *EditSearchMark;
	ISLineEdit *EditSearchModel;

	QListWidget *ListWidgetCars;
	QListWidget *ListWidgetModels;
	ISButtonDialog *ButtonDialog;

	QString MarkName;
	int MarkID;

	QString ModelName;
	int ModelID;
};
//-----------------------------------------------------------------------------
