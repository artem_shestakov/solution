#pragma once
//-----------------------------------------------------------------------------
#include "isstarters_global.h"
#include "ISPluginInterface.h"
//-----------------------------------------------------------------------------
class ISStarters : public QObject, public ISPluginInterface
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "Starters" FILE "Starters.json")
	Q_INTERFACES(ISPluginInterface)

public:
	~ISStarters();

	void RegisterMetaTypes() const;
};
//-----------------------------------------------------------------------------
