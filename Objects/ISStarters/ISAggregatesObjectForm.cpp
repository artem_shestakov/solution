#include "StdAfx.h"
#include "ISAggregatesObjectForm.h"
#include "ISListEdit.h"
//-----------------------------------------------------------------------------
ISAggregatesObjectForm::ISAggregatesObjectForm(ISNamespace::ObjectFormType form_type, PMetaClassTable *meta_table, QWidget *parent, int object_id) : ISObjectFormBase(form_type, meta_table, parent, object_id)
{
	
}
//-----------------------------------------------------------------------------
ISAggregatesObjectForm::~ISAggregatesObjectForm()
{

}
//-----------------------------------------------------------------------------
void ISAggregatesObjectForm::AfterShowEvent()
{
	ISListEdit *EditCar = dynamic_cast<ISListEdit*>(GetFieldWidget("Car"));
	EditCar->SetSqlFilter("cars_client = " + QString::number(GetParentObjectID()));

	ISObjectFormBase::AfterShowEvent(); //������� ����� ����� �������� ������ � �����, �.�. ���� ���� ��������� ������ � ������� ����� �������
}
//-----------------------------------------------------------------------------
