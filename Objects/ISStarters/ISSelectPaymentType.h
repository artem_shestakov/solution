#pragma once
//-----------------------------------------------------------------------------
#include "isstarters_global.h"
#include "ISInterfaceDialogForm.h"
#include "ISButtonDialog.h"
//-----------------------------------------------------------------------------
class ISSTARTERS_EXPORT ISSelectPaymentType : public ISInterfaceDialogForm
{
	Q_OBJECT

public:
	ISSelectPaymentType(QWidget *parent = 0);
	virtual ~ISSelectPaymentType();

	int GetType() const;

protected:
	void Apply();
	void ItemClicked();

private:
	QListWidget *ListWidget;
	ISButtonDialog *ButtonDialog;

	QMap<QListWidgetItem*, int> Items;
	int PaymentTypeID;
};
//-----------------------------------------------------------------------------
