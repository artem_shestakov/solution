#include "StdAfx.h"
#include "ISNomenclatureSupplyListObjectForm.h"
//-----------------------------------------------------------------------------
ISNomenclatureSupplyListObjectForm::ISNomenclatureSupplyListObjectForm(PMetaClassTable *MetaTable, int ParentObjectID, QWidget *parent) : ISListObjectForm(MetaTable, ParentObjectID, parent)
{
	GetQueryModel()->SetClassFilter("nmmv_nomenclature = " + QString::number(ParentObjectID));
}
//-----------------------------------------------------------------------------
ISNomenclatureSupplyListObjectForm::~ISNomenclatureSupplyListObjectForm()
{

}
//-----------------------------------------------------------------------------
