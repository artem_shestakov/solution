#pragma once
//-----------------------------------------------------------------------------
#include "isstarters_global.h"
#include "ISInterfaceDialogForm.h"
//#include "ISComboEdit.h"
#include "ISLineEdit.h"
#include "ISTextEdit.h"
#include "ISTaskWidget.h"
//-----------------------------------------------------------------------------
class ISSTARTERS_EXPORT ISAppendTaskForm : public ISInterfaceDialogForm
{
	Q_OBJECT

public:
	ISAppendTaskForm(ISTaskWidget *TaskWidget = nullptr, QWidget *parent = 0);
	virtual ~ISAppendTaskForm();

	QString GetImportance() const;
	int GetMinute() const;
	QString GetCar() const;
	QString GetDescription() const;

protected:
	void Addition();

private:
	QButtonGroup ButtonGroup;
	QRadioButton *RadioLow;
	QRadioButton *RadioOrdinary;
	QRadioButton *RadioHigh;

	//ISComboEdit *EditTime;
	ISLineEdit *EditCar;
	ISTextEdit *EditDescription;
};
//-----------------------------------------------------------------------------
