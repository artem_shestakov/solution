#pragma once
//-----------------------------------------------------------------------------
#include "isstarters_global.h"
#include "ISObjectFormBase.h"
#include "ISListEdit.h"
#include "ISListObjectForm.h"
//-----------------------------------------------------------------------------
class ISSTARTERS_EXPORT ISRemontObjectForm : public ISObjectFormBase
{
	Q_OBJECT

public:
	Q_INVOKABLE ISRemontObjectForm(ISNamespace::ObjectFormType form_type, PMetaClassTable *meta_table, QWidget *parent, int object_id = 0);
	virtual ~ISRemontObjectForm();

protected:
	void AfterShowEvent() override;
	bool Save() override;

private:
	ISListEdit *EditListCar;
	ISListEdit *EditListAggregat;

	QLabel *LabelPayment;

	QTabWidget *TabWidget;
	ISListObjectForm *ServiceListForm;
	ISListObjectForm *UsedPartsListForm;
};
//-----------------------------------------------------------------------------
