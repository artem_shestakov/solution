#pragma once
//-----------------------------------------------------------------------------
#include "isstarters_global.h"
#include "ISListObjectForm.h"
//-----------------------------------------------------------------------------
class ISSTARTERS_EXPORT ISNomenclatureSupplyListObjectForm : public ISListObjectForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISNomenclatureSupplyListObjectForm(PMetaClassTable *MetaTable, int ParentObjectID, QWidget *parent = 0);
	virtual ~ISNomenclatureSupplyListObjectForm();
};
//-----------------------------------------------------------------------------
