#pragma once
//-----------------------------------------------------------------------------
#include "isstarters_global.h"
#include "ISInterfaceForm.h"
#include "ISTableTimeWidget.h"
#include "ISTaskWidget.h"
//-----------------------------------------------------------------------------
class ISSTARTERS_EXPORT ISWorkBoxesForm : public ISInterfaceForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISWorkBoxesForm(QWidget *parent = 0);
	virtual ~ISWorkBoxesForm();

protected:
	void CellDoubleClicked(int Row, int Column);
	void CreateTaskWidget(int Row, int Column, int CountCell, const QString &TimeRange, int Minute, const QString &Importance, const QString &Car, const QString Description, int TaskID);
	void CreateBoxTable();
	void CreateRightPanel();
	void LoadTasks(const QDate &Date);
	void Update();
	void Edit(ISTaskWidget *TaskWidget);

protected slots:
	void CalendarDateChanged();
	void CheckBoxVisibleChanged(int State);

private:
	QHBoxLayout *Layout;
	ISTableTimeWidget *TableTime;
	QMap<int, int> Columns;

	QCalendarWidget *CalendarWidget;
	QLabel *LabelCurrentDate;
};
//-----------------------------------------------------------------------------
