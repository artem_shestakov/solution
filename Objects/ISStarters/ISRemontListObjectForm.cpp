#include "StdAfx.h"
#include "ISRemontListObjectForm.h"
#include "ISLocalization.h"
#include "ISQuery.h"
#include "ISMessageBox.h"
#include "ISSelectPaymentType.h"
#include "ISRemontHeaderViewVertical.h"
#include "EXDefines.h"
#include "ISPlugin.h"
//-----------------------------------------------------------------------------
static QString QU_REMONT_PAYMENT = PREPARE_QUERY("UPDATE remont SET rmnt_payment = :Payment, rmnt_paymenttype = :PaymentType WHERE rmnt_id = :RemontID");
//-----------------------------------------------------------------------------
ISRemontListObjectForm::ISRemontListObjectForm(PMetaClassTable *MetaTable, int ParentObjectID, QWidget *parent) : ISListObjectForm(MetaTable, ParentObjectID, parent)
{
	QAction *ActionPayment = new QAction(GetTableView());
	ActionPayment->setText(LOCALIZATION("Payment"));
	ActionPayment->setToolTip(LOCALIZATION("Payment"));
	ActionPayment->setIcon(ISPlugin::GetInstance().GetPluginInterface()->GetIcon("Payment"));
	connect(ActionPayment, &QAction::triggered, this, &ISRemontListObjectForm::Payment);
	AddAction(ActionPayment);

	QAction *ActionPaymentCancel = new QAction(GetTableView());
	ActionPaymentCancel->setText(LOCALIZATION("PaymentCancel"));
	ActionPaymentCancel->setToolTip(LOCALIZATION("PaymentCancel"));
	ActionPaymentCancel->setIcon(ISPlugin::GetInstance().GetPluginInterface()->GetIcon("PaymentCancel"));
	connect(ActionPaymentCancel, &QAction::triggered, this, &ISRemontListObjectForm::PaymentCancel);
	AddAction(ActionPaymentCancel);

	GetTableView()->setVerticalHeader(new ISRemontHeaderViewVertical(GetSqlModel(), this));
}
//-----------------------------------------------------------------------------
ISRemontListObjectForm::~ISRemontListObjectForm()
{

}
//-----------------------------------------------------------------------------
void ISRemontListObjectForm::Payment()
{
	bool Payment = GetCurrentRecordValue("Payment").toBool();
	if (Payment)
	{
		ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.AlreadyPayment"));
		return;
	}

	if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.RemitPayment")))
	{
		ISSelectPaymentType SelectPaymentType;
		SelectPaymentType.Exec();

		int PaymentType = SelectPaymentType.GetType();
		if (PaymentType)
		{
			UpdateRemont(true, PaymentType, GetObjectID());
		}
	}
}
//-----------------------------------------------------------------------------
void ISRemontListObjectForm::PaymentCancel()
{
	bool Payment = GetCurrentRecordValue("Payment").toBool();
	if (!Payment)
	{
		ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.PaymentNotCancel"));
		return;
	}

	if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.PaymentCencel")))
	{
		UpdateRemont(false, QVariant(), GetObjectID());
	}
}
//-----------------------------------------------------------------------------
void ISRemontListObjectForm::UpdateRemont(bool Payment, const QVariant &PaymentType, int RemontID)
{
	ISQuery qUpdate(QU_REMONT_PAYMENT);
	qUpdate.BindValue(":Payment", Payment);
	qUpdate.BindValue(":PaymentType", PaymentType);
	qUpdate.BindValue(":RemontID", RemontID);
	qUpdate.Execute();

	ISListObjectForm::Update();
}
//-----------------------------------------------------------------------------
