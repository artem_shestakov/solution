#include "StdAfx.h"
#include "ISServiceRenderedObjectForm.h"
//-----------------------------------------------------------------------------
ISServiceRenderedObjectForm::ISServiceRenderedObjectForm(ISNamespace::ObjectFormType form_type, PMetaClassTable *meta_table, QWidget *parent, int object_id) : ISObjectFormBase(form_type, meta_table, parent, object_id)
{
	EditSpecialPrice = GetFieldWidget("SpecialPrice");
	EditSpecialPrice->setEnabled(false);

	EditUseSpecialPrice = GetFieldWidget("UseSpecialPrice");
	connect(EditUseSpecialPrice, &ISFieldEditBase::ValueChange, this, &ISServiceRenderedObjectForm::UseSpecialPriceChanged);
}
//-----------------------------------------------------------------------------
ISServiceRenderedObjectForm::~ISServiceRenderedObjectForm()
{

}
//-----------------------------------------------------------------------------
void ISServiceRenderedObjectForm::UseSpecialPriceChanged(const QVariant &Value)
{
	bool UseSpecialPrice = Value.toBool();
	
	if (UseSpecialPrice)
	{
		EditSpecialPrice->setEnabled(true);
	}
	else
	{
		EditSpecialPrice->setEnabled(false);
		EditSpecialPrice->Clear();
	}
}
//-----------------------------------------------------------------------------
