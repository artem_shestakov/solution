#pragma once
//-----------------------------------------------------------------------------
#include "isstarters_global.h"
#include "ISObjectFormBase.h"
#include "ISListEdit.h"
#include "ISLineEdit.h"
//-----------------------------------------------------------------------------
class ISSTARTERS_EXPORT ISCarsObjectForm : public ISObjectFormBase
{
	Q_OBJECT

public:
	Q_INVOKABLE ISCarsObjectForm(ISNamespace::ObjectFormType form_type, PMetaClassTable *meta_table, QWidget *parent, int object_id = 0);
	virtual ~ISCarsObjectForm();

protected:
	void CarSelect();
	void AfterShowEvent() override;

private:
	ISLineEdit *EditCarStringName;
	ISListEdit *EditListMark;
	ISListEdit *EditListModel;
};
//-----------------------------------------------------------------------------
