#pragma once
//-----------------------------------------------------------------------------
#include "isstarters_global.h"
#include "ISListObjectForm.h"
//-----------------------------------------------------------------------------
class ISSTARTERS_EXPORT ISRemontListObjectForm : public ISListObjectForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISRemontListObjectForm(PMetaClassTable *MetaTable, int ParentObjectID, QWidget *parent = 0);
	virtual ~ISRemontListObjectForm();

protected:
	void Payment();
	void PaymentCancel();
	void UpdateRemont(bool Payment, const QVariant &PaymentType, int RemontID);
};
//-----------------------------------------------------------------------------
