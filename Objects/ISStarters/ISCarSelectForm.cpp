#include "StdAfx.h"
#include "ISCarSelectForm.h"
#include "ISLocalization.h"
#include "EXDefines.h"
#include "ISCore.h"
#include "ISQuery.h"
#include "ISControls.h"
#include "ISPlugin.h"
//-----------------------------------------------------------------------------
static QString QS_MARK = PREPARE_QUERY("SELECT mark_id, mark_name FROM mark WHERE NOT mark_isdeleted ORDER BY mark_name");
//-----------------------------------------------------------------------------
static QString QS_MODEL = PREPARE_QUERY("SELECT modl_id, modl_name FROM model WHERE NOT modl_isdeleted AND modl_mark = (SELECT mark_uid FROM mark WHERE mark_id = :MarkID) ORDER BY modl_name");
//-----------------------------------------------------------------------------
ISCarSelectForm::ISCarSelectForm(QWidget *parent) : ISInterfaceDialogForm(parent)
{
	MarkID = 0;
	ModelID = 0;

	setWindowTitle(LOCALIZATION("SelectCar"));
	setWindowIcon(ISPlugin::GetInstance().GetPluginInterface()->GetIcon("CarSelect"));

	GetMainLayout()->setContentsMargins(LAYOUT_MARGINS_10_PX);

	QHBoxLayout *Layout = new QHBoxLayout();
	GetMainLayout()->addLayout(Layout);

	QGroupBox *GroupBoxCars = new QGroupBox(this);
	GroupBoxCars->setTitle(LOCALIZATION("Marks"));
	GroupBoxCars->setLayout(new QVBoxLayout());
	Layout->addWidget(GroupBoxCars);

	EditSearchMark = new ISLineEdit(this);
	EditSearchMark->SetPlaceholderText(LOCALIZATION("SearchTohMark"));
	connect(EditSearchMark, &ISLineEdit::ValueChange, this, &ISCarSelectForm::SearchMark);
	GroupBoxCars->layout()->addWidget(EditSearchMark);

	ListWidgetCars = new QListWidget(this);
	connect(ListWidgetCars, &QListWidget::itemClicked, this, &ISCarSelectForm::SelectedMark);
	GroupBoxCars->layout()->addWidget(ListWidgetCars);

	Layout->addWidget(ISControls::CreateVerticalLine(this));

	QGroupBox *GroupBoxModels = new QGroupBox(this);
	GroupBoxModels->setTitle(LOCALIZATION("Models"));
	GroupBoxModels->setLayout(new QVBoxLayout());
	Layout->addWidget(GroupBoxModels);

	EditSearchModel = new ISLineEdit(this);
	EditSearchModel->setEnabled(false);
	EditSearchModel->SetPlaceholderText(LOCALIZATION("SearchTohModel"));
	connect(EditSearchModel, &ISLineEdit::ValueChange, this, &ISCarSelectForm::SearchModel);
	GroupBoxModels->layout()->addWidget(EditSearchModel);

	ListWidgetModels = new QListWidget(this);
	connect(ListWidgetModels, &QListWidget::itemClicked, this, &ISCarSelectForm::SelectedModel);
	connect(ListWidgetModels, &QListWidget::itemDoubleClicked, this, &ISCarSelectForm::DoubleClickedModel);
	GroupBoxModels->layout()->addWidget(ListWidgetModels);

	GetMainLayout()->addWidget(ISControls::CreateHorizontalLine(this));

	ButtonDialog = new ISButtonDialog(this, LOCALIZATION("ListForm.Select"));
	ButtonDialog->SetApplyEnabled(false);
	connect(ButtonDialog, &ISButtonDialog::Apply, this, &ISCarSelectForm::Select);
	connect(ButtonDialog, &ISButtonDialog::Close, this, &ISCarSelectForm::close);
	GetMainLayout()->addWidget(ButtonDialog);

	LoadCars();
}
//-----------------------------------------------------------------------------
ISCarSelectForm::~ISCarSelectForm()
{

}
//-----------------------------------------------------------------------------
QString ISCarSelectForm::GetMarkName() const
{
	return MarkName;
}
//-----------------------------------------------------------------------------
int ISCarSelectForm::GetMarkID() const
{
	return MarkID;
}
//-----------------------------------------------------------------------------
QString ISCarSelectForm::GetModelName() const
{
	return ModelName;
}
//-----------------------------------------------------------------------------
int ISCarSelectForm::GetModelID() const
{
	return ModelID;
}
//-----------------------------------------------------------------------------
void ISCarSelectForm::SelectMark(int ID)
{
	if (ID)
	{
		for (int i = 0; i < ListWidgetCars->count(); i++)
		{
			QListWidgetItem *Item = ListWidgetCars->item(i);

			int ItemData = Item->data(Qt::UserRole).toInt();
			if (ItemData == ID)
			{
				ListWidgetCars->itemClicked(Item);
				ListWidgetCars->setItemSelected(Item, true);
				ListWidgetCars->scrollToItem(Item);
				ListWidgetCars->setFocus();
				break;
			}
		}
	}
}
//-----------------------------------------------------------------------------
void ISCarSelectForm::SelectModel(int ID)
{
	if (ID)
	{
		for (int i = 0; i < ListWidgetModels->count(); i++)
		{
			QListWidgetItem *Item = ListWidgetModels->item(i);

			int ItemData = Item->data(Qt::UserRole).toInt();
			if (ItemData == ID)
			{
				ListWidgetModels->itemClicked(Item);
				ListWidgetModels->setItemSelected(Item, true);
				ListWidgetModels->scrollToItem(Item);
				ListWidgetModels->setFocus();
				break;
			}
		}
	}
}
//-----------------------------------------------------------------------------
void ISCarSelectForm::SelectedMark(QListWidgetItem *Item)
{
	MarkName = Item->text();
	MarkID = Item->data(Qt::UserRole).toInt();

	LoadModels(MarkID);
}
//-----------------------------------------------------------------------------
void ISCarSelectForm::SelectedModel(QListWidgetItem *Item)
{
	ModelName = Item->text();
	ModelID = Item->data(Qt::UserRole).toInt();

	ButtonDialog->SetApplyEnabled(true);
}
//-----------------------------------------------------------------------------
void ISCarSelectForm::DoubleClickedModel(QListWidgetItem *Item)
{
	Select();
}
//-----------------------------------------------------------------------------
void ISCarSelectForm::LoadCars()
{
	ISQuery qSelect(QS_MARK);
	if (qSelect.Execute())
	{
		while (qSelect.Next())
		{
			int ID = qSelect.ReadColumn("mark_id").toInt();
			QString Name = qSelect.ReadColumn("mark_name").toString();

			QListWidgetItem *Item = new QListWidgetItem(ListWidgetCars);
			Item->setData(Qt::UserRole, ID);
			Item->setText(Name);
		}
	}
}
//-----------------------------------------------------------------------------
void ISCarSelectForm::LoadModels(int MarkID)
{
	ListWidgetModels->clear();
	EditSearchModel->setEnabled(true);

	ISQuery qSelect(QS_MODEL);
	qSelect.BindValue(":MarkID", MarkID);
	if (qSelect.Execute())
	{
		while (qSelect.Next())
		{
			int ID = qSelect.ReadColumn("modl_id").toInt();
			QString Name = qSelect.ReadColumn("modl_name").toString();

			QListWidgetItem *Item = new QListWidgetItem(ListWidgetModels);
			Item->setData(Qt::UserRole, ID);
			Item->setText(Name);
		}
	}
}
//-----------------------------------------------------------------------------
void ISCarSelectForm::Select()
{
	SetResult(true);
	close();
}
//-----------------------------------------------------------------------------
void ISCarSelectForm::SearchMark(const QVariant &Value)
{
	for (int i = 0; i < ListWidgetCars->count(); i++)
	{
		ListWidgetCars->setItemHidden(ListWidgetCars->item(i), false);
	}

	if (Value.toString().length())
	{
		for (int i = 0; i < ListWidgetCars->count(); i++)
		{
			QListWidgetItem *Item = ListWidgetCars->item(i);
			if (Item->text().toLower().contains(Value.toString()))
			{
				continue;
			}

			ListWidgetCars->setItemHidden(Item, true);
		}
	}
}
//-----------------------------------------------------------------------------
void ISCarSelectForm::SearchModel(const QVariant &Value)
{
	for (int i = 0; i < ListWidgetModels->count(); i++)
	{
		ListWidgetModels->setItemHidden(ListWidgetModels->item(i), false);
	}

	if (Value.toString().length())
	{
		for (int i = 0; i < ListWidgetModels->count(); i++)
		{
			QListWidgetItem *Item = ListWidgetModels->item(i);
			if (Item->text().toLower().contains(Value.toString()))
			{
				continue;
			}

			ListWidgetModels->setItemHidden(Item, true);
		}
	}
}
//-----------------------------------------------------------------------------
