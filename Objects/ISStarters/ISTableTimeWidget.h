#pragma once
//-----------------------------------------------------------------------------
#include "isstarters_global.h"
//-----------------------------------------------------------------------------
class ISSTARTERS_EXPORT ISTableTimeWidget : public QTableWidget
{
	Q_OBJECT

public:
	ISTableTimeWidget(int TimeRange, QWidget *parent = 0);
	virtual ~ISTableTimeWidget();

	int GetIndexRowFromTime(const QTime &Time) const;
};
//-----------------------------------------------------------------------------
