#pragma once
//-----------------------------------------------------------------------------
#include "isstarters_global.h"
#include "ISListBaseForm.h"
//-----------------------------------------------------------------------------
class ISSTARTERS_EXPORT ISClientsListForm : public ISListBaseForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISClientsListForm(PMetaClassTable *MetaTable, QWidget *parent);
	virtual ~ISClientsListForm();
};
//-----------------------------------------------------------------------------
