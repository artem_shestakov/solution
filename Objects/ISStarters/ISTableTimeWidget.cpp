#include "StdAfx.h"
#include "ISTableTimeWidget.h"
#include "EXDefines.h"
#include "ISAssert.h"
//-----------------------------------------------------------------------------
ISTableTimeWidget::ISTableTimeWidget(int TimeRange, QWidget *parent) : QTableWidget(parent)
{
	IS_ASSERT(TimeRange, "TimeRange is null");

	setCornerButtonEnabled(false);
	verticalHeader()->setFont(FONT_TAHOMA_11);

	int RowCount = (60 * 24) / TimeRange;
	setRowCount(RowCount);

	int Hour = 0;
	int Minute = 0;

	for (int i = 0; i < rowCount(); i++)
	{
		QTime Time(Hour, Minute);
		
		QTableWidgetItem *Item = new QTableWidgetItem();
		Item->setText(Time.toString(TIME_FORMAT_STRING_V1));
		setVerticalHeaderItem(i, Item);

		Minute += TimeRange;
		if (Minute == 60)
		{
			Hour++;
			Minute = 0;
		}
	}
}
//-----------------------------------------------------------------------------
ISTableTimeWidget::~ISTableTimeWidget()
{

}
//-----------------------------------------------------------------------------
int ISTableTimeWidget::GetIndexRowFromTime(const QTime &Time) const
{
	for (int i = 0; i < rowCount(); i++)
	{
		QTableWidgetItem *HeaderItem = verticalHeaderItem(i);
		QString StringTime = HeaderItem->text();
		QTime HeaderTime = QTime::fromString(StringTime);
		if (Time == HeaderTime)
		{
			return i;
		}
	}

	IS_ASSERT(false, "Not found index row from time: " + Time.toString(TIME_FORMAT_STRING_V1));
	return -1;
}
//-----------------------------------------------------------------------------
