#pragma once
//-----------------------------------------------------------------------------
#include "isstarters_global.h"
#include "ISObjectFormBase.h"
#include "ISListEdit.h"
#include "ISIntegerEdit.h"
//-----------------------------------------------------------------------------
class ISSTARTERS_EXPORT ISUsedPartsObjectForm : public ISObjectFormBase
{
	Q_OBJECT

public:
	Q_INVOKABLE ISUsedPartsObjectForm(ISNamespace::ObjectFormType form_type, PMetaClassTable *meta_table, QWidget *parent, int object_id = 0);
	virtual ~ISUsedPartsObjectForm();

protected:
	bool Save() override;

private:
	ISListEdit *EditListNomenclature;
	ISIntegerEdit *EditCount;
};
//-----------------------------------------------------------------------------
