#pragma once
//-----------------------------------------------------------------------------
#include "isstarters_global.h"
#include "ISObjectFormBase.h"
//-----------------------------------------------------------------------------
class ISClientsObjectForm : public ISObjectFormBase
{
	Q_OBJECT

public:
	Q_INVOKABLE ISClientsObjectForm(ISNamespace::ObjectFormType form_type, PMetaClassTable *meta_table, QWidget *parent, int object_id = 0);
	virtual ~ISClientsObjectForm();

protected slots:
	void CheckClient();

protected:
	ISFieldEditBase *EditSurname;
	ISFieldEditBase *EditName;
	ISFieldEditBase *EditPatronymic;
	bool Cheched;
};
//-----------------------------------------------------------------------------
