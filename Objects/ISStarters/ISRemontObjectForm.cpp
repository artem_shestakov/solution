#include "StdAfx.h"
#include "ISRemontObjectForm.h"
#include "ISLocalization.h"
#include "EXDefines.h"
#include "ISMessageBox.h"
#include "ISMetaData.h"
#include "ISBuffer.h"
//-----------------------------------------------------------------------------
ISRemontObjectForm::ISRemontObjectForm(ISNamespace::ObjectFormType form_type, PMetaClassTable *meta_table, QWidget *parent, int object_id) : ISObjectFormBase(form_type, meta_table, parent, object_id)
{
	this->EditListCar = nullptr;
	this->EditListAggregat = nullptr;

	//GetLayoutScrollArea()->takeAt(GetLayoutScrollArea()->count() - 1);

	LabelPayment = new QLabel(this);
	LabelPayment->setVisible(false);
	LabelPayment->setText(LOCALIZATION("RemontAlreadyPayment"));
	LabelPayment->setFont(FONT_APPLICATION_BOLD);
	//GetLayoutScrollArea()->addWidget(LabelPayment);

	QGroupBox *GroupBox = new QGroupBox(this);
	GroupBox->setLayout(new QVBoxLayout());
	//GetLayoutScrollArea()->addWidget(GroupBox);

	TabWidget = new QTabWidget(this);
	TabWidget->tabBar()->setCursor(CURSOR_POINTING_HAND);
	TabWidget->setStyleSheet("QTabWidget::tab-bar {	left: 0px; }");
	TabWidget->setDocumentMode(true);
	GroupBox->layout()->addWidget(TabWidget);
}
//-----------------------------------------------------------------------------
ISRemontObjectForm::~ISRemontObjectForm()
{

}
//-----------------------------------------------------------------------------
void ISRemontObjectForm::AfterShowEvent()
{
	EditListCar = dynamic_cast<ISListEdit*>(GetFieldWidget("Car"));
	//EditListCar->OnSetClassFilter("cars_client = " + QString::number(OnGetParentObjectID()));

	EditListAggregat = dynamic_cast<ISListEdit*>(GetFieldWidget("Aggregat"));
	//EditListAggregat->OnSetClassFilter("spps_client = " + QString::number(OnGetParentObjectID()));

	ServiceListForm = new ISListObjectForm(ISMetaData::GetInstanse().GetMetaTable("ServiceRendered"), GetObjectID(), TabWidget);
	//ServiceListForm->OnSetTabsNot(true);
	TabWidget->addTab(ServiceListForm, BUFFER_ICONS("Table"), LOCALIZATION("Services"));

	UsedPartsListForm = new ISListObjectForm(ISMetaData::GetInstanse().GetMetaTable("UsedParts"), GetObjectID(), TabWidget);
	//UsedPartsListForm->OnSetTabsNot(true);
	TabWidget->addTab(UsedPartsListForm, BUFFER_ICONS("Table"), LOCALIZATION("Parts"));

	if (GetFormType() == ISNamespace::OFT_New)
	{
		ServiceListForm->setEnabled(false);
		UsedPartsListForm->setEnabled(false);
	}
	else if (GetFormType() == ISNamespace::OFT_Edit)
	{
		bool Payment = GetFieldValue("Payment").toBool();
		if (Payment)
		{
			//SetEnabledFields(false);

			LabelPayment->setVisible(true);
			ServiceListForm->setEnabled(false);
			UsedPartsListForm->setEnabled(false);
		}
	}

	ServiceListForm->LoadData();
	UsedPartsListForm->LoadData();
}
//-----------------------------------------------------------------------------
bool ISRemontObjectForm::Save()
{
	QVariant ValueCar = EditListCar->GetValue();
	QVariant ValueAggregat = EditListAggregat->GetValue();

	if (!ValueCar.isValid() && !ValueAggregat.isValid())
	{
		ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Warning.CarOrAggregateNotNull"));
		return false;
	}

	bool Saved = ISObjectFormBase::Save();
	
	if (Saved)
	{
		ServiceListForm->setEnabled(true);
		UsedPartsListForm->setEnabled(true);
	}

	return Saved;
}
//-----------------------------------------------------------------------------
