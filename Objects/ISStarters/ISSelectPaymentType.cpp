#include "StdAfx.h"
#include "ISSelectPaymentType.h"
#include "ISLocalization.h"
#include "ISCore.h"
#include "EXDefines.h"
#include "ISQuery.h"
#include "ISControls.h"
#include "ISPlugin.h"
//-----------------------------------------------------------------------------
static QString QS_PAYMENT_TYPE = PREPARE_QUERY("SELECT pmtp_id, pmtp_name, pmtp_icon FROM paymenttype WHERE NOT pmtp_isdeleted ORDER BY pmtp_id");
//-----------------------------------------------------------------------------
ISSelectPaymentType::ISSelectPaymentType(QWidget *parent) : ISInterfaceDialogForm(parent)
{
	this->PaymentTypeID = 0;

	setWindowTitle(LOCALIZATION("PaymentType"));

	GetMainLayout()->setContentsMargins(LAYOUT_MARGINS_10_PX);

	ListWidget = new QListWidget(this);
	connect(ListWidget, &QListWidget::itemClicked, this, &ISSelectPaymentType::ItemClicked);
	GetMainLayout()->addWidget(ListWidget);

	ISQuery qSelect(QS_PAYMENT_TYPE);
	if (qSelect.Execute())
	{
		while (qSelect.Next())
		{
			int ID = qSelect.ReadColumn("pmtp_id").toInt();
			QString Name = qSelect.ReadColumn("pmtp_name").toString();
			QString Icon = qSelect.ReadColumn("pmtp_icon").toString();

			QListWidgetItem *Item = new QListWidgetItem(ListWidget);
			Item->setText(Name);
			Item->setIcon(ISPlugin::GetInstance().GetPluginInterface()->GetIcon(Icon));
			Item->setSizeHint(QSize(Item->sizeHint().width(), 30));
			Items.insert(Item, ID);
		}
	}

	GetMainLayout()->addWidget(ISControls::CreateHorizontalLine(this));

	ButtonDialog = new ISButtonDialog(this, LOCALIZATION("Pay"), LOCALIZATION("Cancel"));
	ButtonDialog->SetApplyEnabled(false);
	connect(ButtonDialog, &ISButtonDialog::Apply, this, &ISSelectPaymentType::Apply);
	connect(ButtonDialog, &ISButtonDialog::Close, this, &ISSelectPaymentType::close);
	GetMainLayout()->addWidget(ButtonDialog);
}
//-----------------------------------------------------------------------------
ISSelectPaymentType::~ISSelectPaymentType()
{

}
//-----------------------------------------------------------------------------
int ISSelectPaymentType::GetType() const
{
	return PaymentTypeID;
}
//-----------------------------------------------------------------------------
void ISSelectPaymentType::Apply()
{
	PaymentTypeID = Items.value(ListWidget->currentItem());
	close();
}
//-----------------------------------------------------------------------------
void ISSelectPaymentType::ItemClicked()
{
	ButtonDialog->SetApplyEnabled(true);
}
//-----------------------------------------------------------------------------
