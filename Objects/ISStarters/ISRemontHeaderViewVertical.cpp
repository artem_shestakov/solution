#include "StdAfx.h"
#include "ISRemontHeaderViewVertical.h"
#include "EXDefines.h"
#include "ISPlugin.h"
//-----------------------------------------------------------------------------
ISRemontHeaderViewVertical::ISRemontHeaderViewVertical(ISSqlModelCore *SqlModel, QWidget *parent) : QHeaderView(Qt::Vertical, parent)
{
	this->Model = SqlModel;

	PixmapPay = ISPlugin::GetInstance().GetPluginInterface()->GetIcon("PaymentFlag").pixmap(SIZE_16_16);
	PixmapNotPay = ISPlugin::GetInstance().GetPluginInterface()->GetIcon("PaymentFlagNot").pixmap(SIZE_16_16);
}
//-----------------------------------------------------------------------------
ISRemontHeaderViewVertical::~ISRemontHeaderViewVertical()
{

}
//-----------------------------------------------------------------------------
QSize ISRemontHeaderViewVertical::sectionSizeFromContents(int LogicalIndex) const
{
	QSize Size = QHeaderView::sectionSizeFromContents(LogicalIndex);
	Size.setWidth(Size.width() * 3);
	return Size;
}
//-----------------------------------------------------------------------------
void ISRemontHeaderViewVertical::paintSection(QPainter *Painter, const QRect &Rect, int LogicalIndex) const
{
	Painter->save();
	QHeaderView::paintSection(Painter, Rect, LogicalIndex);
	Painter->restore();

	QRect DrawRect = Rect;
	DrawRect.setX(sectionSizeFromContents(LogicalIndex).width() - PixmapNotPay.width() - 5);
	DrawRect.setY(Rect.y() + (sectionSizeFromContents(LogicalIndex).height() / 2) - (PixmapNotPay.height() / 2) + 2);
	DrawRect.setWidth(PixmapNotPay.width());
	DrawRect.setHeight(PixmapNotPay.height());

	bool Payment = Model->GetRecord(LogicalIndex).value("Payment").toBool();
	if (Payment)
	{
		Painter->drawPixmap(DrawRect, PixmapPay);
	}
	else
	{
		Painter->drawPixmap(DrawRect, PixmapNotPay);
	}
}
//-----------------------------------------------------------------------------
