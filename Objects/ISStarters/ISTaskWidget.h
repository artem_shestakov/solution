#pragma once
//-----------------------------------------------------------------------------
#include "isstarters_global.h"
//-----------------------------------------------------------------------------
class ISSTARTERS_EXPORT ISTaskWidget : public QWidget
{
	Q_OBJECT

signals:
	void Edit(ISTaskWidget *TaskWidget);
	void Update();

public:
	ISTaskWidget(QWidget *parent = 0);
	virtual ~ISTaskWidget();

	void SetTimeRange(const QString &TimeRange);

	QString GetImportance() const;
	void SetImportance(const QString &Importance);

	int GetMinute() const;
	void SetMinute(int minute);

	QString GetCar() const;
	void SetCar(const QString &car);

	QString GetDescription() const;
	void SetDescription(const QString &description);

	int GetTaskID() const;
	void SetTaskID(int task_id);

protected:
	void CreateTitle();
	void CreateBottom();
	void Edit();
	void Delete();

private:
	QGroupBox *GroupBox;
	QVBoxLayout *LayoutGroupBox;

	QLabel *LabelTime;
	QLabel *LabelImportance;
	QLabel *LabelCar;
	QLabel *LabelDescription;

	QString Importance;
	QString Car;
	QString Description;

	int Minute;
	int TaskID;
};
//-----------------------------------------------------------------------------
