#pragma once
//-----------------------------------------------------------------------------
#include "isstarters_global.h"
#include "ISObjectFormBase.h"
//-----------------------------------------------------------------------------
class ISSTARTERS_EXPORT ISServiceRenderedObjectForm : public ISObjectFormBase
{
	Q_OBJECT

public:
	Q_INVOKABLE ISServiceRenderedObjectForm(ISNamespace::ObjectFormType form_type, PMetaClassTable *meta_table, QWidget *parent, int object_id = 0);
	virtual ~ISServiceRenderedObjectForm();

protected slots:
	void UseSpecialPriceChanged(const QVariant &Value);

private:
	ISFieldEditBase *EditSpecialPrice;
	ISFieldEditBase *EditUseSpecialPrice;
};
//-----------------------------------------------------------------------------
