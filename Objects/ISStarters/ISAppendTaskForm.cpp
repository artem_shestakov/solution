#include "StdAfx.h"
#include "ISAppendTaskForm.h"
#include "EXDefines.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "ISMessageBox.h"
#include "ISButtonDialog.h"
//-----------------------------------------------------------------------------
//static QString QU_WORK_CALENDAR = PREPARE_QUERY("UPDATE workcalendar SET "
//												"wkcr_timestart = :TimeStart, "
//												"wkcr_timestop = :TimeStop, "
//												"wkcr_allday = :AllDay, "
//												"wkcr_Surname = :Surname, "
//												"wkcr_Name = :Name, "
//												"wkcr_patronymic = :Patronymic, "
//												"wkcr_phone = :Phone, "
//												"wkcr_car = :Car, "
//												"wkcr_description = :Description, "
//												"wkcr_importance = :Importance "
//												"WHERE wkcr_id = :ObjectID");
//-----------------------------------------------------------------------------
ISAppendTaskForm::ISAppendTaskForm(ISTaskWidget *TaskWidget, QWidget *parent) : ISInterfaceDialogForm(parent)
{
	GetMainLayout()->setContentsMargins(LAYOUT_MARGINS_10_PX);

	setWindowTitle(LOCALIZATION("AddingTask"));

	QGroupBox *GroupBoxImportance = new QGroupBox(this);
	GroupBoxImportance->setTitle(LOCALIZATION("Importance"));
	GroupBoxImportance->setLayout(new QHBoxLayout());
	GetMainLayout()->addWidget(GroupBoxImportance);

	RadioLow = new QRadioButton(GroupBoxImportance);
	RadioLow->setChecked(true);
	RadioLow->setCursor(CURSOR_POINTING_HAND);
	RadioLow->setText(LOCALIZATION("Importance.Low"));
	RadioLow->setIcon(BUFFER_ICONS("Importance.Low"));
	RadioLow->setObjectName("Low");
	GroupBoxImportance->layout()->addWidget(RadioLow);
	ButtonGroup.addButton(RadioLow);

	RadioOrdinary = new QRadioButton(GroupBoxImportance);
	RadioOrdinary->setCursor(CURSOR_POINTING_HAND);
	RadioOrdinary->setText(LOCALIZATION("Importance.Ordinary"));
	RadioOrdinary->setIcon(BUFFER_ICONS("Importance.Ordinary"));
	RadioOrdinary->setObjectName("Ordinary");
	GroupBoxImportance->layout()->addWidget(RadioOrdinary);
	ButtonGroup.addButton(RadioOrdinary);

	RadioHigh = new QRadioButton(GroupBoxImportance);
	RadioHigh->setCursor(CURSOR_POINTING_HAND);
	RadioHigh->setText(LOCALIZATION("Importance.High"));
	RadioHigh->setIcon(BUFFER_ICONS("Importance.High"));
	RadioHigh->setObjectName("High");
	GroupBoxImportance->layout()->addWidget(RadioHigh);
	ButtonGroup.addButton(RadioHigh);

	QGroupBox *GroupBoxDescription = new QGroupBox(this);
	GroupBoxDescription->setTitle(LOCALIZATION("Description"));
	GroupBoxDescription->setLayout(new QVBoxLayout());
	GetMainLayout()->addWidget(GroupBoxDescription);

	QLabel *LabelInfo = new QLabel(GroupBoxDescription);
	LabelInfo->setText(LOCALIZATION("EnterMarkAndDescription"));
	GroupBoxDescription->layout()->addWidget(LabelInfo);

	QFormLayout *FormLayout = new QFormLayout();
	dynamic_cast<QVBoxLayout*>(GroupBoxDescription->layout())->addLayout(FormLayout);

	QLabel *LabelTime = new QLabel(GroupBoxDescription);
	LabelTime->setFont(FONT_APPLICATION_BOLD);
	LabelTime->setText(LOCALIZATION("Duration") + ":");

	/*EditTime = new ISComboEdit(nullptr, GroupBoxDescription);
	EditTime->OnSetEditable(false);
	EditTime->OnSetVisibleButtonClear(false);
	EditTime->OnAddItem(LOCALIZATION("Time.30_Minute"), 30);
	EditTime->OnAddItem(LOCALIZATION("Time.1_Hour"), 60);
	EditTime->OnAddItem(LOCALIZATION("Time.1,5_Hour"), 90);
	EditTime->OnAddItem(LOCALIZATION("Time.2_Hour"), 120);
	EditTime->OnAddItem(LOCALIZATION("Time.3_Hour"), 180);
	EditTime->OnAddItem(LOCALIZATION("Time.4_Hour"), 240);
	EditTime->OnAddItem(LOCALIZATION("Time.ToEndDay"), 0);*/
	//FormLayout->addRow(LabelTime, EditTime);

	QLabel *LabelCar = new QLabel(GroupBoxDescription);
	LabelCar->setFont(FONT_APPLICATION_BOLD);
	LabelCar->setCursor(CURSOR_WHATS_THIS);
	LabelCar->setToolTip(LOCALIZATION("FieldNotNull"));
	LabelCar->setText(LOCALIZATION("Car") + ":*");

	EditCar = new ISLineEdit(GroupBoxDescription);
	FormLayout->addRow(LabelCar, EditCar);

	QLabel *LabelDescription = new QLabel(GroupBoxDescription);
	LabelDescription->setFont(FONT_APPLICATION_BOLD);
	LabelDescription->setText(LOCALIZATION("Description") + ":");

	EditDescription = new ISTextEdit(GroupBoxDescription);
	FormLayout->addRow(LabelDescription, EditDescription);

	ISButtonDialog *ButtonDialog = new ISButtonDialog(this, LOCALIZATION("Addition"));
	//ButtonDialog->layout()->setContentsMargins(LAYOUT_MARGINS_NULL);
	connect(ButtonDialog, &ISButtonDialog::Apply, this, &ISAppendTaskForm::Addition);
	connect(ButtonDialog, &ISButtonDialog::Close, this, &ISAppendTaskForm::close);
	GetMainLayout()->addWidget(ButtonDialog);

	if (TaskWidget)
	{
		if (TaskWidget->GetImportance() == "Low")
		{
			RadioLow->setChecked(true);
		}
		else if (TaskWidget->GetImportance() == "Ordinary")
		{
			RadioOrdinary->setChecked(true);
		}
		else if (TaskWidget->GetImportance() == "High")
		{
			RadioHigh->setChecked(true);
		}

		//EditTime->OnSetCurrentUserData(TaskWidget->OnGetMinute());
		EditCar->SetValue(TaskWidget->GetCar());
		EditDescription->SetValue(TaskWidget->GetDescription());
	}
}
//-----------------------------------------------------------------------------
ISAppendTaskForm::~ISAppendTaskForm()
{

}
//-----------------------------------------------------------------------------
QString ISAppendTaskForm::GetImportance() const
{
	for (int i = 0; i < ButtonGroup.buttons().count(); i++)
	{
		QRadioButton *RadioButton = dynamic_cast<QRadioButton*>(ButtonGroup.buttons().at(i));
		if (RadioButton->isChecked())
		{
			return RadioButton->objectName();
		}
	}

	return QString();
}
//-----------------------------------------------------------------------------
int ISAppendTaskForm::GetMinute() const
{
	//return EditTime->OnGetCurrentUserData().toInt();
	return 0;
}
//-----------------------------------------------------------------------------
QString ISAppendTaskForm::GetCar() const
{
	return EditCar->GetValue().toString();
}
//-----------------------------------------------------------------------------
QString ISAppendTaskForm::GetDescription() const
{
	return EditDescription->GetValue().toString();
}
//-----------------------------------------------------------------------------
void ISAppendTaskForm::Addition()
{
	if (EditCar->GetValue().toString().length())
	{
		SetResult(true);
		close();
	}
	else
	{
		ISMessageBox::ShowWarning(this, LOCALIZATION("Message.Error.Field.NullValue").arg(LOCALIZATION("Car")));
	}
}
//-----------------------------------------------------------------------------
