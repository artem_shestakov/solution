#pragma once
//-----------------------------------------------------------------------------
#include "isstarters_global.h"
#include "ISListBaseForm.h"
//-----------------------------------------------------------------------------
class ISSTARTERS_EXPORT ISNomenclatureListForm : public ISListBaseForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISNomenclatureListForm(PMetaClassTable *MetaTable, QWidget *parent = 0);
	virtual ~ISNomenclatureListForm();
};
//-----------------------------------------------------------------------------
