#pragma once
//-----------------------------------------------------------------------------
#include "isstarters_global.h"
#include "ISObjectFormBase.h"
//-----------------------------------------------------------------------------
class ISSTARTERS_EXPORT ISAggregatesObjectForm : public ISObjectFormBase
{
	Q_OBJECT

public:
	Q_INVOKABLE ISAggregatesObjectForm(ISNamespace::ObjectFormType form_type, PMetaClassTable *meta_table, QWidget *parent, int object_id = 0);
	virtual ~ISAggregatesObjectForm();

protected:
	void AfterShowEvent() override;
};
//-----------------------------------------------------------------------------
