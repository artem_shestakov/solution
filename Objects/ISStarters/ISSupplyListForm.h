#pragma once
//-----------------------------------------------------------------------------
#include "isstarters_global.h"
#include "ISListBaseForm.h"
//-----------------------------------------------------------------------------
class ISSTARTERS_EXPORT ISSupplyListForm : public ISListBaseForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISSupplyListForm(PMetaClassTable *MetaTable, QWidget *parent = 0);
	virtual ~ISSupplyListForm();

protected:
	void Debit();
};
//-----------------------------------------------------------------------------
