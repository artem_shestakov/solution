#include "StdAfx.h"
#include "ISPatientsListForm.h"
#include "ISMetaData.h"
#include "EXDefines.h"
#include "ISSystem.h"
//-----------------------------------------------------------------------------
ISPatientsListForm::ISPatientsListForm(QWidget *parent) : ISListBaseForm(ISMetaData::GetInstanse().GetMetaTable("Patients"), parent)
{
	QAction *ActionTherapy = new QAction(GetToolBar());
	ActionTherapy->setText(QString::fromLocal8Bit("������� �� �������"));
	connect(ActionTherapy, &QAction::triggered, this, &ISPatientsListForm::Therapy);
	AddAction(ActionTherapy);

	QAction *ActionSurgery = new QAction(GetToolBar());
	ActionSurgery->setText(QString::fromLocal8Bit("������� �� ��������"));
	connect(ActionSurgery, &QAction::triggered, this, &ISPatientsListForm::Surgery);
	AddAction(ActionSurgery);

	QAction *ActionImplantation = new QAction(GetToolBar());
	ActionImplantation->setText(QString::fromLocal8Bit("������� �� �����������"));
	connect(ActionImplantation, &QAction::triggered, this, &ISPatientsListForm::Implantation);
	AddAction(ActionImplantation);
}
//-----------------------------------------------------------------------------
ISPatientsListForm::~ISPatientsListForm()
{

}
//-----------------------------------------------------------------------------
void ISPatientsListForm::Therapy()
{
	QString TemplateName = ":Resources/HistoryTherapy.docx";
	QString CopyName = APPLICATION_TEMP_PATH + "/HistoryTherapy.docx";

	if (!QFile::exists(CopyName))
	{
		QFile::copy(TemplateName, CopyName);
	}

	ISSystem::OpenFile(CopyName);
}
//-----------------------------------------------------------------------------
void ISPatientsListForm::Surgery()
{
	QString TemplateName = ":Resources/HistorySurgery.docx";
	QString CopyName = APPLICATION_TEMP_PATH + "/HistorySurgery.docx";

	if (!QFile::exists(CopyName))
	{
		QFile::copy(TemplateName, CopyName);
	}

	ISSystem::OpenFile(CopyName);
}
//-----------------------------------------------------------------------------
void ISPatientsListForm::Implantation()
{
	QString TemplateName = ":Resources/HistoryImplantation.docx";
	QString CopyName = APPLICATION_TEMP_PATH + "/HistoryImplantation.docx";

	if (!QFile::exists(CopyName))
	{
		QFile::copy(TemplateName, CopyName);
	}

	ISSystem::OpenFile(CopyName);
}
//-----------------------------------------------------------------------------
