#pragma once
//-----------------------------------------------------------------------------
#include "isshakshak_global.h"
#include "ISPluginInterface.h"
//-----------------------------------------------------------------------------
class IShakShak : public QObject, public ISPluginInterface
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "ShakShak" FILE "ShakShak.json")
	Q_INTERFACES(ISPluginInterface)

public:
	~IShakShak();

	void RegisterMetaTypes() const;
};
//-----------------------------------------------------------------------------
