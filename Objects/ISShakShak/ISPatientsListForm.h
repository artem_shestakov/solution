#pragma once
//-----------------------------------------------------------------------------
#include "isshakshak_global.h"
#include "ISListBaseForm.h"
//-----------------------------------------------------------------------------
class ISSHAKSHAK_EXPORT ISPatientsListForm : public ISListBaseForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISPatientsListForm(QWidget *parent = 0);
	virtual ~ISPatientsListForm();

protected:
	void Therapy();
	void Surgery();
	void Implantation();
};
//-----------------------------------------------------------------------------
