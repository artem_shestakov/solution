#include "StdAfx.h"
#include "ISMilex.h"
//-----------------------------------------------------------------------------
#include "ISClientObjectForm.h"
#include "ISClientBlackListForm.h"
#include "ISDeviceBaseListForm.h"
#include "ISDeviceObjectForm.h"
#include "ISDeviceTodayListForm.h"
#include "ISDeviceYesterdayListForm.h"
#include "ISDeviceExpectationListForm.h"
#include "ISDeviceInWorkListForm.h"
#include "ISDeviceDoneListForm.h"
#include "ISDeviceIssuedListForm.h"
#include "ISDeviceCanceledListForm.h"
#include "ISDeviceCurrentUserListForm.h"
//-----------------------------------------------------------------------------
ISMilex::~ISMilex()
{

}
//-----------------------------------------------------------------------------
void ISMilex::RegisterMetaTypes() const
{
	qRegisterMetaType<ISClientBlackListForm*>("ISClientBlackListForm");
	qRegisterMetaType<ISClientObjectForm*>("ISClientObjectForm");
	qRegisterMetaType<ISDeviceBaseListForm*>("ISDeviceBaseListForm");
	qRegisterMetaType<ISDeviceObjectForm*>("ISDeviceObjectForm");
	qRegisterMetaType<ISDeviceTodayListForm*>("ISDeviceTodayListForm");
	qRegisterMetaType<ISDeviceYesterdaylistForm*>("ISDeviceYesterdaylistForm");
	qRegisterMetaType<ISDeviceExpectationListForm*>("ISDeviceExpectationListForm");
	qRegisterMetaType<ISDeviceInWorkListForm*>("ISDeviceInWorkListForm");
	qRegisterMetaType<ISDeviceDoneListForm*>("ISDeviceDoneListForm");
	qRegisterMetaType<ISDeviceIssuedListForm*>("ISDeviceIssuedListForm");
	qRegisterMetaType<ISDeviceCanceledListForm*>("ISDeviceCanceledListForm");
	qRegisterMetaType<ISDeviceCurrentUserListForm*>("ISDeviceCurrentUserListForm");
}
//-----------------------------------------------------------------------------
