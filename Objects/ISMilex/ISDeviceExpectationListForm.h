#pragma once
//-----------------------------------------------------------------------------
#include "ismilex_global.h"
#include "ISDeviceBaseListForm.h"
//-----------------------------------------------------------------------------
class ISMILEX_EXPORT ISDeviceExpectationListForm : public ISDeviceBaseListForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISDeviceExpectationListForm(QWidget *parent = 0);
	virtual ~ISDeviceExpectationListForm();
};
//-----------------------------------------------------------------------------
