#pragma once
//-----------------------------------------------------------------------------
#include "ismilex_global.h"
#include "ISDeviceBaseListForm.h"
//-----------------------------------------------------------------------------
class ISMILEX_EXPORT ISDeviceTodayListForm : public ISDeviceBaseListForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISDeviceTodayListForm(QWidget *parent = 0);
	virtual ~ISDeviceTodayListForm();
};
//-----------------------------------------------------------------------------
