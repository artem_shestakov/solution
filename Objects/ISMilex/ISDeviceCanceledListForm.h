#pragma once
//-----------------------------------------------------------------------------
#include "ismilex_global.h"
#include "ISDeviceBaseListForm.h"
//-----------------------------------------------------------------------------
class ISMILEX_EXPORT ISDeviceCanceledListForm : public ISDeviceBaseListForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISDeviceCanceledListForm(QWidget *parent = 0);
	virtual ~ISDeviceCanceledListForm();
};
//-----------------------------------------------------------------------------
