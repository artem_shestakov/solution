#pragma once
//-----------------------------------------------------------------------------
#include "ismilex_global.h"
#include "ISDeviceBaseListForm.h"
//-----------------------------------------------------------------------------
class ISMILEX_EXPORT ISDeviceInWorkListForm : public ISDeviceBaseListForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISDeviceInWorkListForm(QWidget *parent = 0);
	virtual ~ISDeviceInWorkListForm();
};
//-----------------------------------------------------------------------------
