#include "StdAfx.h"
#include "ISDeviceYesterdayListForm.h"
//-----------------------------------------------------------------------------
ISDeviceYesterdaylistForm::ISDeviceYesterdaylistForm(QWidget *parent) : ISDeviceBaseListForm(parent)
{
	GetQueryModel()->SetClassFilter("dvce_creationdate::DATE = CURRENT_DATE - 1");
}
//-----------------------------------------------------------------------------
ISDeviceYesterdaylistForm::~ISDeviceYesterdaylistForm()
{

}
//-----------------------------------------------------------------------------
