#include "StdAfx.h"
#include "ISDeviceExpectationListForm.h"
#include "ISMilex.h"
//-----------------------------------------------------------------------------
ISDeviceExpectationListForm::ISDeviceExpectationListForm(QWidget *parent) : ISDeviceBaseListForm(parent)
{
	GetQueryModel()->SetClassFilter(QString("dvce_status = \"%1\"").arg(MILEX_CLIENT_STATE_EXPECTATION));
}
//-----------------------------------------------------------------------------
ISDeviceExpectationListForm::~ISDeviceExpectationListForm()
{

}
//-----------------------------------------------------------------------------
