#pragma once
//-----------------------------------------------------------------------------
#include "ismilex_global.h"
#include "ISPluginInterface.h"
//-----------------------------------------------------------------------------
#define MILEX_CLIENT_STATE_IN_WORK QString("761eef2a-4242-4921-8a4d-d8ccd428053f")
#define MILEX_CLIENT_STATE_DONE QString("682ffd50-cc03-4cd1-8f7f-12fe9a833d84")
#define MILEX_CLIENT_STATE_ISSUED QString("7a0fd8da-6d63-4a14-a7cc-231a69612053")
#define MILEX_CLIENT_STATE_CANCELED QString("1eda32b1-6b96-464a-8e78-3f25790e7cba")
#define MILEX_CLIENT_STATE_EXPECTATION QString("f72f58e5-d400-4b15-8cad-85357aa142c9")
//-----------------------------------------------------------------------------
class ISMilex : public QObject, public ISPluginInterface
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "Milex" FILE "Milex.json")
	Q_INTERFACES(ISPluginInterface)

public:
	~ISMilex();

	void RegisterMetaTypes() const;
};
//-----------------------------------------------------------------------------
