#pragma once
//-----------------------------------------------------------------------------
#include "ismilex_global.h"
#include "ISDeviceBaseListForm.h"
//-----------------------------------------------------------------------------
class ISMILEX_EXPORT ISDeviceYesterdaylistForm : public ISDeviceBaseListForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISDeviceYesterdaylistForm(QWidget *parent = 0);
	virtual ~ISDeviceYesterdaylistForm();
};
//-----------------------------------------------------------------------------
