#pragma once
//-----------------------------------------------------------------------------
#include "ismilex_global.h"
#include "ISDeviceBaseListForm.h"
//-----------------------------------------------------------------------------
class ISMILEX_EXPORT ISDeviceCurrentUserListForm : public ISDeviceBaseListForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISDeviceCurrentUserListForm(QWidget *parent = 0);
	virtual ~ISDeviceCurrentUserListForm();
};
//-----------------------------------------------------------------------------
