#include "StdAfx.h"
#include "ISDeviceInWorkListForm.h"
#include "ISMilex.h"
//-----------------------------------------------------------------------------
ISDeviceInWorkListForm::ISDeviceInWorkListForm(QWidget *parent) : ISDeviceBaseListForm(parent)
{
	GetQueryModel()->SetClassFilter(QString("dvce_status = \"%1\"").arg(MILEX_CLIENT_STATE_IN_WORK));
}
//-----------------------------------------------------------------------------
ISDeviceInWorkListForm::~ISDeviceInWorkListForm()
{

}
//-----------------------------------------------------------------------------
