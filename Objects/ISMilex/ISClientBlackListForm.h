#pragma once
//-----------------------------------------------------------------------------
#include "ismilex_global.h"
#include "ISListBaseForm.h"
//-----------------------------------------------------------------------------
class ISMILEX_EXPORT ISClientBlackListForm : public ISListBaseForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISClientBlackListForm(QWidget *parent = 0);
	virtual ~ISClientBlackListForm();
};
//-----------------------------------------------------------------------------
