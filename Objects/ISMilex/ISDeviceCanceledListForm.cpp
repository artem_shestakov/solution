#include "StdAfx.h"
#include "ISDeviceCanceledListForm.h"
#include "ISMilex.h"
//-----------------------------------------------------------------------------
ISDeviceCanceledListForm::ISDeviceCanceledListForm(QWidget *parent) : ISDeviceBaseListForm(parent)
{
	GetQueryModel()->SetClassFilter(QString("dvce_status = \"%1\"").arg(MILEX_CLIENT_STATE_CANCELED));
}
//-----------------------------------------------------------------------------
ISDeviceCanceledListForm::~ISDeviceCanceledListForm()
{

}
//-----------------------------------------------------------------------------
