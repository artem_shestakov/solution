#include "StdAfx.h"
#include "ISClientBlackListForm.h"
#include "ISMetaData.h"
//-----------------------------------------------------------------------------
ISClientBlackListForm::ISClientBlackListForm(QWidget *parent) : ISListBaseForm(ISMetaData::GetInstanse().GetMetaTable("Clients"), parent)
{
	GetQueryModel()->SetClassFilter("clts_blacklist = true");
}
//-----------------------------------------------------------------------------
ISClientBlackListForm::~ISClientBlackListForm()
{

}
//-----------------------------------------------------------------------------
