#pragma once
//-----------------------------------------------------------------------------
#include "ismilex_global.h"
#include "ISDeviceBaseListForm.h"
//-----------------------------------------------------------------------------
class ISMILEX_EXPORT ISDeviceIssuedListForm : public ISDeviceBaseListForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISDeviceIssuedListForm(QWidget *parent = 0);
	virtual ~ISDeviceIssuedListForm();
};
//-----------------------------------------------------------------------------
