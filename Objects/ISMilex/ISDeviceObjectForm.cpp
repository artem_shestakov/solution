#include "StdAfx.h"
#include "ISDeviceObjectForm.h"
#include "ISQuery.h"
#include "ISMessageBox.h"
#include "ISLocalization.h"
#include "ISCore.h"
#include "ISMemoryObjects.h"
//-----------------------------------------------------------------------------
static QString QS_DEVICE_SERIAL_NUMBER = PREPARE_QUERY("SELECT dvce_id FROM device WHERE dvce_serialnumber = :SerialNumber ORDER BY dvce_id");
//-----------------------------------------------------------------------------
ISDeviceObjectForm::ISDeviceObjectForm(ISNamespace::ObjectFormType form_type, PMetaClassTable *meta_table, QWidget *parent, int object_id) : ISObjectFormBase(form_type, meta_table, parent, object_id)
{
	connect(GetFieldWidget("SerialNumber"), &ISFieldEditBase::ValueChange, this, &ISDeviceObjectForm::SerialNumberChanged);
}
//-----------------------------------------------------------------------------
ISDeviceObjectForm::~ISDeviceObjectForm()
{

}
//-----------------------------------------------------------------------------
void ISDeviceObjectForm::SetClientID(int ClientID)
{
	ISFieldEditBase *EditClient = GetFieldWidget("Client");
	EditClient->SetValue(ClientID);
	EditClient->setEnabled(false);
}
//-----------------------------------------------------------------------------
void ISDeviceObjectForm::SerialNumberChanged(const QVariant &value)
{
	if (value.isValid())
	{
		ISQuery qSelect(QS_DEVICE_SERIAL_NUMBER);
		qSelect.BindValue(":SerialNumber", value);
		if (qSelect.ExecuteFirst())
		{
			int DeviceID = qSelect.ReadColumn("dvce_id").toInt();
			if (GetObjectID() == DeviceID)
			{
				return;
			}

			if (DeviceID)
			{
				if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.FoundClientWithDeviceSerialNumber").arg(value.toString())))
				{
					ISObjectFormBase *ObjectForm = ISCore::CreateObjectForm(ISNamespace::OFT_Edit, GetMetaTable(), DeviceID);
					QMetaObject::invokeMethod(ISMemoryObjects::GetInstance().GetWorkspaceForm(), "AddObjectForm", Q_ARG(QWidget *, ObjectForm));
				}
			}
		}
	}
}
//-----------------------------------------------------------------------------
