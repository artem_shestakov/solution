#include "StdAfx.h"
#include "ISDeviceDoneListForm.h"
#include "ISMilex.h"
//-----------------------------------------------------------------------------
ISDeviceDoneListForm::ISDeviceDoneListForm(QWidget *parent) : ISDeviceBaseListForm(parent)
{
	GetQueryModel()->SetClassFilter(QString("dvce_status = \"%1\"").arg(MILEX_CLIENT_STATE_DONE));
}
//-----------------------------------------------------------------------------
ISDeviceDoneListForm::~ISDeviceDoneListForm()
{

}
//-----------------------------------------------------------------------------
