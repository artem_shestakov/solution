#pragma once
//-----------------------------------------------------------------------------
#include "ismilex_global.h"
#include "ISInterfaceDialogForm.h"
#include "ISButtonDialog.h"
//-----------------------------------------------------------------------------
class ISMILEX_EXPORT ISActionDeviceForm : public ISInterfaceDialogForm
{
	Q_OBJECT

public:
	ISActionDeviceForm(int device_id, const QString &ClientStatus, QWidget *parent = 0);
	virtual ~ISActionDeviceForm();

protected:
	void RadioButtonClicked();
	void Apply();

private:
	QButtonGroup ButtonGroup;
	ISButtonDialog *ButtonDialog;
	
	int DeviceID;
};
//-----------------------------------------------------------------------------
