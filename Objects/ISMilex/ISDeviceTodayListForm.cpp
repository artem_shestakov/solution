#include "StdAfx.h"
#include "ISDeviceTodayListForm.h"
//-----------------------------------------------------------------------------
ISDeviceTodayListForm::ISDeviceTodayListForm(QWidget *parent) : ISDeviceBaseListForm(parent)
{
	GetQueryModel()->SetClassFilter("dvce_creationdate::DATE = CURRENT_DATE");
}
//-----------------------------------------------------------------------------
ISDeviceTodayListForm::~ISDeviceTodayListForm()
{

}
//-----------------------------------------------------------------------------
