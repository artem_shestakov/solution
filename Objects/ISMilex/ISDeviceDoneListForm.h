#pragma once
//-----------------------------------------------------------------------------
#include "ismilex_global.h"
#include "ISDeviceBaseListForm.h"
//-----------------------------------------------------------------------------
class ISMILEX_EXPORT ISDeviceDoneListForm : public ISDeviceBaseListForm
{
	Q_OBJECT
	
public:
	Q_INVOKABLE ISDeviceDoneListForm(QWidget *parent = 0);
	virtual ~ISDeviceDoneListForm();
};
//-----------------------------------------------------------------------------
