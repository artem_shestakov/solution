#pragma once
//-----------------------------------------------------------------------------
#include "ismilex_global.h"
#include "ISObjectFormBase.h"
//-----------------------------------------------------------------------------
class ISMILEX_EXPORT ISDeviceObjectForm : public ISObjectFormBase
{
	Q_OBJECT

public:
	Q_INVOKABLE ISDeviceObjectForm(ISNamespace::ObjectFormType form_type, PMetaClassTable *meta_table, QWidget *parent, int object_id = 0);
	virtual ~ISDeviceObjectForm();

	void SetClientID(int ClientID);

protected:
	void SerialNumberChanged(const QVariant &value);
};
//-----------------------------------------------------------------------------
