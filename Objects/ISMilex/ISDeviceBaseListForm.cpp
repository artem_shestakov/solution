#include "StdAfx.h"
#include "ISDeviceBaseListForm.h"
#include "ISMetaData.h"
#include "ISQuery.h"
#include "ISLocalization.h"
#include "ISPlugin.h"
#include "ISActionDeviceForm.h"
//-----------------------------------------------------------------------------
static QString QS_STATUS = PREPARE_QUERY("SELECT dvce_status FROM device WHERE dvce_id = :DeviceID");
//-----------------------------------------------------------------------------
ISDeviceBaseListForm::ISDeviceBaseListForm(QWidget *parent) : ISListBaseForm(ISMetaData::GetInstanse().GetMetaTable("Device"), parent)
{
	QAction *ActionSetStatus = new QAction(GetTableView());
	ActionSetStatus->setText(LOCALIZATION("Action"));
	ActionSetStatus->setToolTip(LOCALIZATION("Action"));
	ActionSetStatus->setIcon(ISPlugin::GetInstance().GetPluginInterface()->GetIcon("Action"));
	connect(ActionSetStatus, &QAction::triggered, this, &ISDeviceBaseListForm::ActionOnDevice);
	AddAction(ActionSetStatus, true, true);
}
//-----------------------------------------------------------------------------
ISDeviceBaseListForm::~ISDeviceBaseListForm()
{

}
//-----------------------------------------------------------------------------
void ISDeviceBaseListForm::ActionOnDevice()
{
	ISQuery qSelectStatus(QS_STATUS);
	qSelectStatus.BindValue(":DeviceID", GetObjectID());
	if (qSelectStatus.ExecuteFirst())
	{
		QString StatusUID = qSelectStatus.ReadColumn("dvce_status").toString();

		ISActionDeviceForm ActionClientForm(GetObjectID(), StatusUID, this);
		if (ActionClientForm.Exec())
		{
			Update();
		}
	}
}
//-----------------------------------------------------------------------------
