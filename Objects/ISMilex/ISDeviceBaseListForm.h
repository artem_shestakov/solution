#pragma once
//-----------------------------------------------------------------------------
#include "ismilex_global.h"
#include "ISListBaseForm.h"
//-----------------------------------------------------------------------------
class ISMILEX_EXPORT ISDeviceBaseListForm : public ISListBaseForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISDeviceBaseListForm(QWidget *parent = 0);
	virtual ~ISDeviceBaseListForm();

protected:
	void ActionOnDevice(); //�������� ��� ��������
};
//-----------------------------------------------------------------------------
