#include "StdAfx.h"
#include "ISActionDeviceForm.h"
#include "ISLocalization.h"
#include "ISBuffer.h"
#include "EXDefines.h"
#include "ISQuery.h"
#include "ISCore.h"
#include "ISControls.h"
#include "ISPlugin.h"
#include "ISProtocol.h"
//-----------------------------------------------------------------------------
static QString QS_STATUSES = PREPARE_QUERY("SELECT stes_uid, stes_name FROM statuses WHERE NOT stes_isdeleted ORDER BY stes_id");
//-----------------------------------------------------------------------------
static QString QU_STATUS = PREPARE_QUERY("UPDATE device SET dvce_status = :StatusUID WHERE dvce_id = :DeviceID");
//-----------------------------------------------------------------------------
ISActionDeviceForm::ISActionDeviceForm(int device_id, const QString &ClientStatus, QWidget *parent) : ISInterfaceDialogForm(parent)
{
	DeviceID = device_id;

	setWindowTitle(LOCALIZATION("Action"));
	setWindowIcon(ISPlugin::GetInstance().GetPluginInterface()->GetIcon("Action"));

	ForbidResize();

	GetMainLayout()->setContentsMargins(LAYOUT_MARGINS_10_PX);

	ISQuery qSelectActions(QS_STATUSES);
	if (qSelectActions.Execute())
	{
		while (qSelectActions.Next())
		{
			QString StatusUID = qSelectActions.ReadColumn("stes_uid").toString();
			QString StatusName = qSelectActions.ReadColumn("stes_name").toString();

			QRadioButton *RadioButton = new QRadioButton(this);
			RadioButton->setProperty("StatusUID", StatusUID);
			RadioButton->setText(StatusName);
			connect(RadioButton, &QRadioButton::clicked, this, &ISActionDeviceForm::RadioButtonClicked);
			GetMainLayout()->addWidget(RadioButton);
			ButtonGroup.addButton(RadioButton);

			if (StatusUID == ClientStatus)
			{
				RadioButton->setChecked(true);
			}
		}
	}

	GetMainLayout()->addWidget(ISControls::CreateHorizontalLine(this));

	ButtonDialog = new ISButtonDialog(this);
	ButtonDialog->SetApplyEnabled(false);
	connect(ButtonDialog, &ISButtonDialog::Apply, this, &ISActionDeviceForm::Apply);
	connect(ButtonDialog, &ISButtonDialog::Close, this, &ISActionDeviceForm::close);
	GetMainLayout()->addWidget(ButtonDialog);
}
//-----------------------------------------------------------------------------
ISActionDeviceForm::~ISActionDeviceForm()
{

}
//-----------------------------------------------------------------------------
void ISActionDeviceForm::RadioButtonClicked()
{
	ButtonDialog->SetApplyEnabled(true);
}
//-----------------------------------------------------------------------------
void ISActionDeviceForm::Apply()
{
	for (int i = 0; i < ButtonGroup.buttons().count(); i++)
	{
		QRadioButton *RadioButton = dynamic_cast<QRadioButton*>(ButtonGroup.buttons().at(i));
		if (RadioButton->isChecked())
		{
			QString StatusUID = RadioButton->property("StatusUID").toString();

			ISQuery qUpdateStatus(QU_STATUS);
			qUpdateStatus.BindValue(":StatusUID", StatusUID);
			qUpdateStatus.BindValue(":DeviceID", DeviceID);
			if (qUpdateStatus.Execute())
			{
				SetResult(true);
				ISProtocol::Insert(true, "{80A5D5E3-9A78-4304-9FD8-71529F3D1A02}", QString(), QString(), DeviceID, RadioButton->text());
			}

			break;
		}
	}

	close();
}
//-----------------------------------------------------------------------------
