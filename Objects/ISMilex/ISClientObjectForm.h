#pragma once
//-----------------------------------------------------------------------------
#include "ismilex_global.h"
#include "ISObjectFormBase.h"
#include "ISDeviceBaseListForm.h"
//-----------------------------------------------------------------------------
class ISMILEX_EXPORT ISClientObjectForm : public ISObjectFormBase
{
	Q_OBJECT

public:
	Q_INVOKABLE ISClientObjectForm(ISNamespace::ObjectFormType form_type, PMetaClassTable *meta_table, QWidget *parent, int object_id = 0);
	virtual ~ISClientObjectForm();

protected:
	void AfterShowEvent() override;
	void FullNameChanged(const QVariant &value);
	void PhoneChanged(const QVariant &value);
	void ShowDeviceObjectForm(QWidget *ObjectForm);

private:
	QGroupBox *GroupBox;
	ISDeviceBaseListForm *DeviceListForm;
};
//-----------------------------------------------------------------------------
