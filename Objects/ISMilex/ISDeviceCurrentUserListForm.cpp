#include "StdAfx.h"
#include "ISDeviceCurrentUserListForm.h"
#include "ISMetaUser.h"
//-----------------------------------------------------------------------------
ISDeviceCurrentUserListForm::ISDeviceCurrentUserListForm(QWidget *parent) : ISDeviceBaseListForm(parent)
{
	GetQueryModel()->SetClassFilter(QString("dvce_productaccepted = %1").arg(CURRENT_USER_ID));
}
//-----------------------------------------------------------------------------
ISDeviceCurrentUserListForm::~ISDeviceCurrentUserListForm()
{

}
//-----------------------------------------------------------------------------
