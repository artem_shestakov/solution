#include "StdAfx.h"
#include "ISClientObjectForm.h"
#include "ISQuery.h"
#include "ISMessageBox.h"
#include "ISLocalization.h"
#include "ISCore.h"
#include "ISMemoryObjects.h"
#include "ISDeviceObjectForm.h"
//-----------------------------------------------------------------------------
static QString QS_CLIENT_FULL_NAME = PREPARE_QUERY("SELECT clts_id FROM clients WHERE clts_fullname = :FullName ORDER BY clts_id");
//-----------------------------------------------------------------------------
static QString QS_CLIENT_PHONE_MOBILE = PREPARE_QUERY("SELECT clts_id FROM clients WHERE clts_phonemobile = :PhoneMobile ORDER BY clts_id");
//-----------------------------------------------------------------------------
ISClientObjectForm::ISClientObjectForm(ISNamespace::ObjectFormType form_type, PMetaClassTable *meta_table, QWidget *parent, int object_id) : ISObjectFormBase(form_type, meta_table, parent, object_id)
{
	connect(GetFieldWidget("FullName"), &ISFieldEditBase::ValueChange, this, &ISClientObjectForm::FullNameChanged);
	connect(GetFieldWidget("PhoneMobile"), &ISFieldEditBase::ValueChange, this, &ISClientObjectForm::PhoneChanged);

	GroupBox = new QGroupBox(this);
	GroupBox->setTitle(LOCALIZATION("Devices"));
	GroupBox->setLayout(new QVBoxLayout());
	GetLayoutWidgetObject()->addWidget(GroupBox);

	DeviceListForm = new ISDeviceBaseListForm(this);
	connect(DeviceListForm, &ISListBaseForm::AddFormFromTab, this, &ISClientObjectForm::ShowDeviceObjectForm);
	GroupBox->layout()->addWidget(DeviceListForm);
}
//-----------------------------------------------------------------------------
ISClientObjectForm::~ISClientObjectForm()
{

}
//-----------------------------------------------------------------------------
void ISClientObjectForm::AfterShowEvent()
{
	ISObjectFormBase::AfterShowEvent();

	if (GetFormType() == ISNamespace::OFT_New)
	{
		GroupBox->setEnabled(false);
	}
	else if (GetFormType() == ISNamespace::OFT_Edit)
	{
		GroupBox->setEnabled(true);

		DeviceListForm->GetQueryModel()->SetClassFilter(QString("dvce_client = %1").arg(GetObjectID()));
		DeviceListForm->LoadData();
	}
}
//-----------------------------------------------------------------------------
void ISClientObjectForm::FullNameChanged(const QVariant &value)
{
	if (value.isValid())
	{
		ISQuery qSelect(QS_CLIENT_FULL_NAME);
		qSelect.BindValue(":FullName", value);
		if (qSelect.ExecuteFirst())
		{
			int ClientID = qSelect.ReadColumn("clts_id").toInt();
			if (GetObjectID() == ClientID)
			{
				return;
			}

			if (ClientID)
			{
				if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.FoundClientWithFullName").arg(value.toString())))
				{
					ISObjectFormBase *ObjectForm = ISCore::CreateObjectForm(ISNamespace::OFT_Edit, GetMetaTable(), ClientID);
					QMetaObject::invokeMethod(ISMemoryObjects::GetInstance().GetWorkspaceForm(), "AddObjectForm", Q_ARG(QWidget *, ObjectForm));
				}
			}
		}
	}
}
//-----------------------------------------------------------------------------
void ISClientObjectForm::PhoneChanged(const QVariant &value)
{
	if (value.isValid())
	{
		ISQuery qSelect(QS_CLIENT_PHONE_MOBILE);
		qSelect.BindValue(":PhoneMobile", value);
		if (qSelect.ExecuteFirst())
		{
			int ClientID = qSelect.ReadColumn("clts_id").toInt();
			if (GetObjectID() == ClientID)
			{
				return;
			}

			if (ClientID)
			{
				if (ISMessageBox::ShowQuestion(this, LOCALIZATION("Message.Question.FoundClientWithPhoneMobile").arg(value.toString())))
				{
					ISObjectFormBase *ObjectForm = ISCore::CreateObjectForm(ISNamespace::OFT_Edit, GetMetaTable(), ClientID);
					QMetaObject::invokeMethod(ISMemoryObjects::GetInstance().GetWorkspaceForm(), "AddObjectForm", Q_ARG(QWidget *, ObjectForm));
				}
			}
		}
	}
}
//-----------------------------------------------------------------------------
void ISClientObjectForm::ShowDeviceObjectForm(QWidget *ObjectForm)
{
	ISDeviceObjectForm *DeviceObjectForm = dynamic_cast<ISDeviceObjectForm*>(ObjectForm);
	DeviceObjectForm->SetClientID(GetObjectID());
	QMetaObject::invokeMethod(ISMemoryObjects::GetInstance().GetWorkspaceForm(), "AddObjectForm", Q_ARG(QWidget *, DeviceObjectForm));

	//ObjectForm->setParent(nullptr);
	//ObjectForm->show();
}
//-----------------------------------------------------------------------------
