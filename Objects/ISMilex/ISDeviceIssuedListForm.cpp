#include "StdAfx.h"
#include "ISDeviceIssuedListForm.h"
#include "ISMilex.h"
//-----------------------------------------------------------------------------
ISDeviceIssuedListForm::ISDeviceIssuedListForm(QWidget *parent) : ISDeviceBaseListForm(parent)
{
	GetQueryModel()->SetClassFilter(QString("dvce_status = \"%1\"").arg(MILEX_CLIENT_STATE_ISSUED));
}
//-----------------------------------------------------------------------------
ISDeviceIssuedListForm::~ISDeviceIssuedListForm()
{

}
//-----------------------------------------------------------------------------
