#pragma once
//-----------------------------------------------------------------------------
#include "isperspective_global.h"
#include "ISPluginInterface.h"
//-----------------------------------------------------------------------------
class ISPerspective : public QObject, public ISPluginInterface
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "Perspective" FILE "Perspective.json")
	Q_INTERFACES(ISPluginInterface)

public:
	~ISPerspective();

	void RegisterMetaTypes() const;
};
//-----------------------------------------------------------------------------
