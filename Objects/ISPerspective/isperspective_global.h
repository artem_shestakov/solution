#pragma once
//-----------------------------------------------------------------------------
#include "StdAfx.h"
//-----------------------------------------------------------------------------
#ifdef ISPERSPECTIVE_LIB
# define ISPERSPECTIVE_EXPORT Q_DECL_EXPORT
#else
# define ISPERSPECTIVE_EXPORT Q_DECL_IMPORT
#endif
//-----------------------------------------------------------------------------
