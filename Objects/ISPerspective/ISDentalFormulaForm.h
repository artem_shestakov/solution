#pragma once
//-----------------------------------------------------------------------------
#include "isperspective_global.h"
#include "ISInterfaceMetaForm.h"
//-----------------------------------------------------------------------------
class ISPERSPECTIVE_EXPORT ISDentalFormulaForm : public ISInterfaceMetaForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISDentalFormulaForm(QWidget *parent = 0);
	virtual ~ISDentalFormulaForm();

	void LoadData() override;
};
//-----------------------------------------------------------------------------
