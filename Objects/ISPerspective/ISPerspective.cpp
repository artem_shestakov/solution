#include "StdAfx.h"
#include "ISPerspective.h"
//-----------------------------------------------------------------------------
#include "ISDentalFormulaForm.h"
//-----------------------------------------------------------------------------
ISPerspective::~ISPerspective()
{

}
//-----------------------------------------------------------------------------
void ISPerspective::RegisterMetaTypes() const
{
	qRegisterMetaType<ISDentalFormulaForm*>("ISDentalFormulaForm");
}
//-----------------------------------------------------------------------------
