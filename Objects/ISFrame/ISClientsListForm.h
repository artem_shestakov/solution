#pragma once
//-----------------------------------------------------------------------------
#include "isframe_global.h"
#include "ISListBaseForm.h"
//-----------------------------------------------------------------------------
class ISFRAME_EXPORT ISClientsListForm : public ISListBaseForm
{
	Q_OBJECT

public:
	Q_INVOKABLE ISClientsListForm(QWidget *parent);
	virtual ~ISClientsListForm();
};
//-----------------------------------------------------------------------------
