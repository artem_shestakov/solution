#pragma once
//-----------------------------------------------------------------------------
#include "isframe_global.h"
#include "ISPluginInterface.h"
//-----------------------------------------------------------------------------
class ISFrame : public QObject, public ISPluginInterface
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "Frame" FILE "Frame.json")
	Q_INTERFACES(ISPluginInterface)

public:
	~ISFrame();

	void RegisterMetaTypes() const;
};
//-----------------------------------------------------------------------------
