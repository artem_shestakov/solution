#include "StdAfx.h"
#include "ISFrame.h"
//-----------------------------------------------------------------------------
#include "ISClientsListForm.h"
#include "ISCurrentAffairsForm.h"
//-----------------------------------------------------------------------------
ISFrame::~ISFrame()
{

}
//-----------------------------------------------------------------------------
void ISFrame::RegisterMetaTypes() const
{
	qRegisterMetaType<ISClientsListForm*>("ISClientsListForm");
	qRegisterMetaType<ISCurrentAffairsForm*>("ISCurrentAffairsForm");
}
//-----------------------------------------------------------------------------
