#include "StdAfx.h"
#include "ISClientsListForm.h"
#include "ISMetaData.h"
//-----------------------------------------------------------------------------
ISClientsListForm::ISClientsListForm(QWidget *parent) : ISListBaseForm(ISMetaData::GetInstanse().GetMetaTable("Clients"), parent)
{
	GetSqlModel()->RemoveColumn(GetMetaTable()->GetField("Call"));
	GetSqlModel()->RemoveColumn(GetMetaTable()->GetField("Meeting"));
	GetSqlModel()->RemoveColumn(GetMetaTable()->GetField("CommercialSentence"));
	GetSqlModel()->RemoveColumn(GetMetaTable()->GetField("Treaty"));
	GetSqlModel()->RemoveColumn(GetMetaTable()->GetField("InWork"));
}
//-----------------------------------------------------------------------------
ISClientsListForm::~ISClientsListForm()
{

}
//-----------------------------------------------------------------------------
